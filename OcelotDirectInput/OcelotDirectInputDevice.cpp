#include "OcelotDirectInputDevice.h"

using namespace Ocelot;

OcelotDirectInputDevice::OcelotDirectInputDevice()
{

}

OcelotDirectInputDevice::~OcelotDirectInputDevice()
{

#if defined(DEBUG) || defined(_DEBUG)
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_WNDW);
#endif
}

HRESULT OcelotDirectInputDevice::getData(InputType type, UNKNOWN pData, DWORD* iNumb)
{
	HRESULT hr = E_FAIL;
	size_t size = 0;

	if(type == Mouse)
	{
		size = sizeof(DIDEVICEOBJECTDATA);

		// buffered method
		hr = m_pDevice->GetDeviceData(size, (DIDEVICEOBJECTDATA*)pData, iNumb, 0);
	}
	else
	{
		// non buffered method
		if(type == Keyboard)
			size = sizeof(CCHAR) * 256;  // size for keyboard
		else
			size = sizeof(DIJOYSTATE);  // size for joystick
		
		hr = m_pDevice->GetDeviceState(size, pData);
	}

	// check if failed
	if(FAILED(hr))
	{
		if((hr == DIERR_NOTACQUIRED) || (hr == DIERR_INPUTLOST))
		{
			hr = m_pDevice->Acquire();
			while(hr == DIERR_INPUTLOST)
				hr = m_pDevice->Acquire();

			// if another app is using the device, restart the next frame.
			if(hr == DIERR_OTHERAPPHASPRIO) return S_OK;

			// if success
			if(SUCCEEDED(hr))
			{
				if(type == Mouse)
					hr = m_pDevice->GetDeviceData(size, (DIDEVICEOBJECTDATA*)pData, iNumb, 0);
				else
					hr = m_pDevice->GetDeviceState(size, pData);
			}

			// another error
			if(FAILED(hr)) return E_FAIL;
		}

		// another error
		else return E_FAIL;
	}

	return S_OK;
}

VVOID    OcelotDirectInputDevice::log(CCHAR* str, ...)
{
	CCHAR  str2[256];
	CCHAR* args;

	args = (CCHAR*)&str + sizeof(str);
	vsprintf(str2, str, args);
	fprintf(m_pLog, "Starting OcelotDIDevice Debug Log: ");
	fprintf(m_pLog, str2);
	fprintf(m_pLog, "\n");

	if(m_bLogFileOpen)
		fflush(m_pLog);
}

VVOID    OcelotDirectInputDevice::create(LPDIRECTINPUT8 pDI, HWND hWnd, FILE* pLog)
{
	m_pDevice      = NULL;
	m_pDirectInput = pDI;
	m_hWnd         = hWnd;
	m_pLog         = pLog;

	if(m_pLog)
		m_bLogFileOpen = true;
}

HRESULT OcelotDirectInputDevice::crankUp(REFGUID rguid, LPCDIDATAFORMAT lpdf)
{
	DWORD iFlags = DISCL_FOREGROUND | DISCL_NONEXCLUSIVE;

	// if the device already exist
	// we need to destroy it.
	ReleaseID(m_pDevice);

	// then create the device
	if(FAILED(m_pDirectInput->CreateDevice(rguid, &m_pDevice, NULL)))
	{
		log("ERROR: Creation of the Input Device FAILED!!!");
		return E_FAIL;
	}

	// define the right data format.
	if(FAILED(m_pDevice->SetDataFormat(lpdf)))
	{
		log("ERROR: Set of the data format FAILED!!!");
		return E_FAIL;
	}

	// and set the cooperative level.
	if(FAILED(m_pDevice->SetCooperativeLevel(m_hWnd, iFlags)))
	{
		log("ERROR: Set of the cooperative level FAILED!!!");
		return E_FAIL;
	}

	return S_OK;
}

VVOID    OcelotDirectInputDevice::release()
{
	ReleaseID(m_pDevice);
	log("Ocelot Input Device Released !!!");
}
