#ifndef DEF_OCELOTKEYBOARD_H
#define DEF_OCELOTKEYBOARD_H

#include "OcelotDirectInputDevice.h"

namespace Ocelot
{
	class OcelotKeyboard : public OcelotDirectInputDevice
	{
	private:

		// Attributes
		CCHAR m_cKeys[256];
		CCHAR m_cKeysOld[256];
		INT  m_iPrevKey;
		UINT m_iCounter;

	public:

		// Constructor & Destructor
		OcelotKeyboard(LPDIRECTINPUT8, HWND, FILE*);
		~OcelotKeyboard();

		// Methods
		HRESULT initialize();
		HRESULT update();
		BBOOL    isPressed(UINT keyID);
		BBOOL    isReleased(UINT keyID);
		BBOOL    isPressedAndLocked(UINT keyID);

		// Accessors

	};
}

#endif