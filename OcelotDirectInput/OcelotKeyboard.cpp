#include "OcelotKeyboard.h"

using namespace Ocelot;

OcelotKeyboard::OcelotKeyboard(LPDIRECTINPUT8 pDI, HWND hWnd, FILE* pLog) :
m_iPrevKey(-1), m_iCounter(0)
{
	create(pDI, hWnd, pLog);
}

OcelotKeyboard::~OcelotKeyboard()
{
	log("Releasing Keyboard !!!");
	release();
}

HRESULT OcelotKeyboard::initialize()
{
	log("Crancking up the Keyboard !!!");
	if(FAILED(crankUp(GUID_SysKeyboard, &c_dfDIKeyboard)))
		return E_FAIL;

	// clear the memory
	memset(m_cKeys, 0, sizeof(m_cKeys));
	memset(m_cKeysOld, 0, sizeof(m_cKeysOld));

	// acquire the device
	m_pDevice->Acquire();
	log("Keyboard connected !!!");

	return S_OK;
}

HRESULT OcelotKeyboard::update()
{
	// Note: keyboard is used as a non-buffered DI device
	// so, need to get data as a single struct into the key state array.
	memcpy(m_cKeysOld, m_cKeys, sizeof(m_cKeys));

	// query status
	if(FAILED(getData(Keyboard, &m_cKeys[0], NULL)))
	{
		log("ERROR: Get Keyboard Data FAILED !!!");
		return E_FAIL;
	}

	return S_OK;
}

BBOOL    OcelotKeyboard::isPressed(UINT keyID)
{
	if(m_cKeys[keyID] & 0x80)
		return true;

	return false;
}

BBOOL    OcelotKeyboard::isReleased(UINT keyID)
{
	if((m_cKeysOld[keyID] & 0x80) && (m_cKeys[keyID] & 0x80))
		return true;

	return false;
}

BBOOL    OcelotKeyboard::isPressedAndLocked(UINT keyID)
{
	if(isPressed(keyID))
	{
		m_iCounter++;

		if(isReleased(keyID) && (m_iCounter > 8))
		{
			m_iCounter = 0;
			return true;
		}
		else
			return false;
	}
	else 
		return false;
}
