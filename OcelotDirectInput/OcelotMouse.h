#ifndef DEF_OCELOTMOUSE_H
#define DEF_OCELOTMOUSE_H

#include "OcelotDirectInputDevice.h"
#define BUFFERSIZE 16

namespace Ocelot
{
	class OcelotMouse : public OcelotDirectInputDevice
	{
	private:

		// Attributes
		RECT   m_cage;
		HANDLE m_hEvent;
		BBOOL   m_bPressed[3];
		BBOOL   m_bReleased[3];

	public:

		// Constructor & Destructor
		OcelotMouse(LPDIRECTINPUT8, HWND, FILE*);
		~OcelotMouse();

		// Methods
		HRESULT initialize();
		HRESULT update();
		BBOOL    isPressed(UINT butID);
		BBOOL    isReleased(UINT butID);
		VVOID    setCage(RECT cage);

		// Accessors

	};
}

#endif