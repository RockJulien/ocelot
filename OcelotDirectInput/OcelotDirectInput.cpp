#include "OcelotDirectInput.h"

using namespace Ocelot;

// exported DLL create function
HRESULT createInputDevice(HINSTANCE hDll, IOcelotInputDevice** pInterface)
{
	if(!*pInterface)
	{
		*pInterface = new OcelotDirectInput(hDll);
		return S_OK;
	}

	return E_FAIL;
}

// exported DLL release function
HRESULT releaseInputDevice(IOcelotInputDevice** pInterface)
{
	if(!*pInterface)
		return E_FAIL;

	//DeletePointer(*pInterface);
	delete (*pInterface);
	(*pInterface) = NULL;
	return S_OK;
}

OcelotDirectInput::OcelotDirectInput(HINSTANCE dllMod)
{
	m_hDLL      = dllMod;
	m_pDI       = NULL;
	m_pKeyboard = NULL;
	m_pJoystick = NULL;
	m_pMouse    = NULL;
	m_bRunning  = false;
	m_pLog      = NULL;

	// open a log file.
	fopen_s(&m_pLog, "OcelotInputDeviceLog.txt", "w");
	log("SUCCESS. Need to initialize now!!!");
}

OcelotDirectInput::~OcelotDirectInput()
{
	this->Stop();

#if defined(DEBUG) || defined(_DEBUG)
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_WNDW);
#endif
}

VVOID OcelotDirectInput::log(CCHAR* str, ...)
{
	CCHAR  str2[256];
	CCHAR* args;

	args = (CCHAR*)&str + sizeof(str);
	vsprintf_s(str2, str, args);
	fprintf(m_pLog, "Starting OcelotDI Debug Log: ");
	fprintf(m_pLog, str2);
	fprintf(m_pLog, "\n");

	if(m_bLogFileOpen)
		fflush(m_pLog);
}

INT  OcelotDirectInput::Initialize(HWND hWnd, const RECT* mouseCage, BBOOL saveLog)
{
	HRESULT hr;
	log("Initialization in process !!!");
	m_hWndMain     = hWnd;
	m_bLogFileOpen = saveLog;

	// create the DirectInput main object.
	hr = DirectInput8Create(m_hDLL, DIRECTINPUT_VERSION, IID_IDirectInput8, (UNKNOWN*)&m_pDI, NULL);
	if(FAILED(hr))
	{
		log("ERROR: Direct Input create8 function FAILED !!!");
		return (INT)E_FAIL;
	}

	// create all input device objects.
	m_pKeyboard = new OcelotKeyboard(m_pDI, m_hWndMain, m_pLog);
	m_pJoystick = new OcelotJoystick(m_pDI, m_hWndMain, m_pLog);
	m_pMouse    = new OcelotMouse(m_pDI, m_hWndMain, m_pLog);

	// initialize all input device object.
	if(FAILED(m_pKeyboard->initialize()))
	{
		DeletePointer(m_pKeyboard);
		log("ERROR: initialization of the keyboard FAILED !!!");
		return (INT)E_FAIL;
	}

	if(FAILED(m_pMouse->initialize()))
	{
		DeletePointer(m_pMouse);
		log("ERROR: initialization of the mouse FAILED !!!");
		return (INT)E_FAIL;
	}

	if(mouseCage)
		m_pMouse->setCage(*mouseCage);

	if(FAILED(m_pJoystick->initialize()))
	{
		DeletePointer(m_pJoystick);
		log("WARNING: no joysticks connected !!!");
		// no need to send a failure if no joysticks.
	}

	log("initialization complete!!!");
	m_bRunning = true;
	return (INT)S_OK;
}

INT  OcelotDirectInput::Update()
{
	if(!IsRunning())
		return (INT)E_FAIL;

	if(m_pKeyboard)
		if(FAILED(m_pKeyboard->update()))
			return (INT)E_FAIL;

	if(m_pJoystick)
		if(FAILED(m_pJoystick->update()))
			return (INT)E_FAIL;

	if(m_pMouse)
		if(FAILED(m_pMouse->update()))
			return (INT)E_FAIL;

	return (INT)S_OK;
}

INT  OcelotDirectInput::PositionOnScreen(InputType type, POINT* pPoint)
{
	// query screen position for the
	// mouse and the joystick.
	if(type == Mouse)
	{
		if(m_pMouse)
			m_pMouse->PositionOnScreen(pPoint);
		return (INT)S_OK;
	}
	else if(type == Joystick)
	{
		if(m_pJoystick)
			m_pJoystick->PositionOnScreen(pPoint);
		else
		{
			(*pPoint).x = 0;
			(*pPoint).y = 0;
		}

		return (INT)S_OK;
	}
	else
		return (INT)E_INVALIDARG;
}

VVOID OcelotDirectInput::Stop()
{
	log("Releasing the Direct Input !!!");
	DeletePointer(m_pKeyboard);
	DeletePointer(m_pJoystick);
	DeletePointer(m_pMouse);
	if(m_pDI)
	{
		m_pDI->Release();
		m_pDI = NULL;
	}

	log("Direct Input Released !!!");
	fclose(m_pLog); // close the log file.
}

BBOOL OcelotDirectInput::IsRunning()
{
	return m_bRunning;
}

BBOOL OcelotDirectInput::HasJoystick(CCHAR* pJoystickName)
{
	if(m_pJoystick)
	{
		if(pJoystickName) 
			m_pJoystick->getName(pJoystickName);
		return true;
	}
	return false;
}

BBOOL OcelotDirectInput::IsPressed(InputType type, UINT keyID)
{
	if((type == Mouse) && (m_pMouse))
		return m_pMouse->isPressed(keyID);
	else if((type == Keyboard) && (m_pKeyboard))
		return m_pKeyboard->isPressed(keyID);
	else if((type == Joystick) && (m_pJoystick))
		return m_pJoystick->isPressed(keyID);
	else
		return false;
}

BBOOL OcelotDirectInput::IsReleased(InputType type, UINT keyID)
{
	if((type == Mouse) && (m_pMouse))
		return m_pMouse->isReleased(keyID);
	else if((type == Keyboard) && (m_pKeyboard))
		return m_pKeyboard->isReleased(keyID);
	else if((type == Joystick) && (m_pJoystick))
		return m_pJoystick->isReleased(keyID);
	else
		return false;
}

BBOOL OcelotDirectInput::IsPressedAndLocked(UINT keyID)
{
	if(m_pKeyboard)
		return m_pKeyboard->isPressedAndLocked(keyID);
	else
		return false;
}
