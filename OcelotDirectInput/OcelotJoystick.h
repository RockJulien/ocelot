#ifndef DEF_OCELOTJOYSTICK_H
#define DEF_OCELOTJOYSTICK_H

#include "OcelotDirectInputDevice.h"

namespace Ocelot
{
	class OcelotJoystick : public OcelotDirectInputDevice
	{
	private:

		// Attributes
		GUID   m_guid;
		CCHAR   m_cName[256];
		BBOOL   m_bJoystickFound;
		BBOOL   m_bPressed[12];
		BBOOL   m_bReleased[12];
		DWORD  m_iNbButtons;

	public:

		// Constructor & Destructor
		OcelotJoystick(LPDIRECTINPUT8, HWND, FILE*);
		~OcelotJoystick();

		// Methods
		HRESULT initialize();
		HRESULT update();
		BBOOL    isPressed(UINT butID);
		BBOOL    isReleased(UINT butID);
		BBOOL    joystickFound();
		BBOOL    enumJoyCallBack(const DIDEVICEINSTANCE* pI);
		VVOID    getName(CCHAR* pJoystickName);

		// Accessors

	};
}

#endif