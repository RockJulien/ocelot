#ifndef DEF_OCELOTDIRECTINPUTDEVICE_H
#define DEF_OCELOTDIRECTINPUTDEVICE_H

#include "..\OcelotInput\OcelotInputUtil.h"
#include <dinput.h>

namespace Ocelot
{
	class OcelotDirectInputDevice
	{
	protected:

			// Attributes
			LPDIRECTINPUTDEVICE8 m_pDevice;
			LPDIRECTINPUT8       m_pDirectInput;
			HWND                 m_hWnd;
			LINT                 m_lX;
			LINT                 m_lY;
			FILE*                m_pLog;
			BBOOL                 m_bLogFileOpen;

	virtual HRESULT getData(InputType type, UNKNOWN pData, DWORD* iNumb);
	        VVOID    log(CCHAR*, ...);

	public:

			// Constructor & Destructor
			OcelotDirectInputDevice();
	virtual ~OcelotDirectInputDevice();

			// Methods
	virtual HRESULT initialize() = 0;
	virtual HRESULT update()     = 0;
	virtual VVOID    create(LPDIRECTINPUT8, HWND, FILE*);
	virtual HRESULT crankUp(REFGUID rguid, LPCDIDATAFORMAT lpdf);
	virtual VVOID    release();

			// Accessors
	inline  VVOID    PositionOnScreen(POINT* pPoint) { (*pPoint).x = m_lX; (*pPoint).y = m_lY;}
	};
}

#endif