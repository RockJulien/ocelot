#ifndef DEF_OCELOTDIRECTINPUT_H
#define DEF_OCELOTDIRECTINPUT_H

#include "..\OcelotInput\IOcelotInputDevice.h"
#include "OcelotJoystick.h"
#include "OcelotKeyboard.h"
#include "OcelotMouse.h"

namespace Ocelot
{
	class OcelotDirectInput : public IOcelotInputDevice
	{
	private:

		// Attributes
		LPDIRECTINPUT8  m_pDI;        // DirectInput main object.
		OcelotJoystick* m_pJoystick;  // one pointer for a potential joystick attached to the system
		OcelotKeyboard* m_pKeyboard;  // one pointer for a potential keyboard attached to the system
		OcelotMouse*    m_pMouse;     // one pointer for a potential mouse attached to the system.
		BBOOL            m_bLogFileOpen;

		// debug log file
		VVOID log(CCHAR*, ...);

	public:

		// Constructor & Destructor
		OcelotDirectInput(HINSTANCE dllMod);
		~OcelotDirectInput();

		// Methods
		INT  Initialize(HWND, const RECT*, BBOOL);
		INT  Update();
		INT  PositionOnScreen(InputType, POINT*);
		VVOID Stop();
		BBOOL IsRunning();
		BBOOL HasJoystick(CCHAR*);
		BBOOL IsPressed(InputType, UINT);
		BBOOL IsReleased(InputType, UINT);
		BBOOL IsPressedAndLocked(UINT); // only for keyboard.

		// Accessors

	};
}

#endif