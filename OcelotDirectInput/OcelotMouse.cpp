#include "OcelotMouse.h"

using namespace Ocelot;

OcelotMouse::OcelotMouse(LPDIRECTINPUT8 pID, HWND hWnd, FILE* pLog)
{
	create(pID, hWnd, pLog);
}

OcelotMouse::~OcelotMouse()
{
	log("Releasing the Mouse !!!");
	release();
}

HRESULT OcelotMouse::initialize()
{
	// the mouse here is buffered, so another
	// pre-set are needed.

	// clear memory
	memset(m_bPressed, 0, sizeof(BBOOL) * 3);
	memset(m_bReleased, 0, sizeof(BBOOL) * 3);
	m_lX = m_lY = 0;

	// init the mouse cage.
	m_cage.left   = -1;
	m_cage.top    = -1;
	m_cage.right  = -1;
	m_cage.bottom = -1;

	log("Crancking up the Mouse !!!");
	if(FAILED(crankUp(GUID_SysMouse, &c_dfDIMouse)))
		return E_FAIL;

	// notify events
	if(!(m_hEvent = CreateEvent(NULL, false, false, NULL)))
	{
		log("ERROR: Create events of the Mouse FAILED !!!");
		return E_FAIL;
	}

	if(FAILED(m_pDevice->SetEventNotification(m_hEvent)))
	{
		log("ERROR: Set Event Notification of the Mouse FAILED !!!");
		return E_FAIL;
	}

	// build mouse buffer
	DIPROPDWORD dipdw;
	dipdw.diph.dwSize		= sizeof(DIPROPDWORD);
	dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	dipdw.diph.dwObj        = 0;
	dipdw.diph.dwHow        = DIPH_DEVICE;
	dipdw.dwData            = BUFFERSIZE;

	if(FAILED(m_pDevice->SetProperty(DIPROP_BUFFERSIZE, &dipdw.diph)))
	{
		log("ERROR: Set Property of the Mouse FAILED !!!");
		return E_FAIL;
	}

	m_pDevice->Acquire();
	log("Mouse connected !!!");

	return S_OK;
}

HRESULT OcelotMouse::update()
{
	DIDEVICEOBJECTDATA objData[BUFFERSIZE];
	DWORD nbElem = BUFFERSIZE;

	// read data from the mouse buffer.
	if(FAILED(getData(Mouse, &objData[0], &nbElem)))
	{
		log("ERROR: Get Data of the Mouse FAILED !!!");
		return E_FAIL;
	}

	m_bReleased[0] = m_bReleased[1] = m_bReleased[2] = false;

	// now there are nbElem mouse events to process.
	for(DWORD currElem = 0; currElem < nbElem; ++currElem)
	{
		switch(objData[currElem].dwOfs)
		{
			// first movements
		case DIMOFS_X:
			{
				m_lX += objData[currElem].dwData;
				if(m_lX < m_cage.left)
					m_lX = m_cage.left;
				else if(m_lX > m_cage.right)
					m_lX = m_cage.right;
			}
			break;

		case DIMOFS_Y:
			{
				m_lY += objData[currElem].dwData;
				if(m_lY < m_cage.top)
					m_lY = m_cage.top;
				else if(m_lY > m_cage.bottom)
					m_lY = m_cage.bottom;
			}
			break;

		case DIMOFS_BUTTON0:
			{
				if(objData[currElem].dwData & 0x80)
					m_bPressed[0] = true;
				else
				{
					if(m_bPressed[0])
						m_bReleased[0] = true;
					m_bPressed[0] = false;
				}
			}
			break;

		case DIMOFS_BUTTON1:
			{
				if(objData[currElem].dwData & 0x80)
					m_bPressed[1] = true;
				else
				{
					if(m_bPressed[1])
						m_bReleased[1] = true;
					m_bPressed[1] = false;
				}
			}
			break;

		case DIMOFS_BUTTON2:
			{
				if(objData[currElem].dwData & 0x80)
					m_bPressed[2] = true;
				else
				{
					if(m_bPressed[2])
						m_bReleased[2] = true;
					m_bPressed[2] = false;
				}
			}
			break;
		}
	}

	return S_OK;
}

BBOOL    OcelotMouse::isPressed(UINT butID)
{
	if(butID < 3) return m_bPressed[butID]; return false;
}

BBOOL    OcelotMouse::isReleased(UINT butID)
{
	if(butID < 3) return m_bReleased[butID]; return false;
}

VVOID    OcelotMouse::setCage(RECT cage)
{
	m_cage = cage;
}
