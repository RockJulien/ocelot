#include "OcelotJoystick.h"

using namespace Ocelot;

OcelotJoystick* g_pInstance = NULL;

BBOOL CALLBACK gEnumJoyCallBack(const DIDEVICEINSTANCE* pInst, UNKNOWN pUserData)
{
	return g_pInstance->enumJoyCallBack(pInst);
}

OcelotJoystick::OcelotJoystick(LPDIRECTINPUT8 pDI, HWND hWnd, FILE* pLog)
{
	create(pDI, hWnd, pLog);
}

OcelotJoystick::~OcelotJoystick()
{
	log("Releasing the Joystick !!!");
	release();
}

HRESULT OcelotJoystick::initialize()
{
	DIPROPRANGE diprg;
	DIDEVCAPS   diCaps;

	// initialize what is needed.
	memset(m_bPressed, 0, sizeof(m_bPressed));
	memset(m_bReleased, 0, sizeof(m_bReleased));
	m_lX = m_lY      = 0;
	g_pInstance      = this;
	m_bJoystickFound = false;

	log("Crancking up the Joystick !!!");
	// enumerate attached joysticks
	m_pDirectInput->EnumDevices(DI8DEVCLASS_GAMECTRL, (LPDIENUMDEVICESCALLBACK)gEnumJoyCallBack, &m_guid, DIEDFL_ATTACHEDONLY);

	// check if something found
	if(!m_bJoystickFound)
	{
		log("No Joysticks Found !!!");
		return E_FAIL;
	}

	// extras settings
	diprg.diph.dwSize       = sizeof(DIPROPRANGE);
	diprg.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	diprg.diph.dwHow        = DIPH_BYOFFSET;
	diprg.lMin              = -1000;
	diprg.lMax              = 1000;

	diprg.diph.dwObj        = DIJOFS_X;
	m_pDevice->SetProperty(DIPROP_RANGE, &diprg.diph);

	diprg.diph.dwObj        = DIJOFS_Y;
	m_pDevice->SetProperty(DIPROP_RANGE, &diprg.diph);

	// number of buttons
	if(SUCCEEDED(m_pDevice->GetCapabilities(&diCaps)))
		m_iNbButtons = diCaps.dwButtons;
	else
		m_iNbButtons = 4;
	log("Joystick connected (%d buttons, \"%s\")", m_iNbButtons, m_cName);

	return S_OK;
}

HRESULT OcelotJoystick::update()
{
	DIJOYSTATE joyState;

	// poll the joystick
	m_pDevice->Poll();

	// get data from the joystick.
	if(FAILED(getData(Joystick, &joyState, NULL)))
	{
		log("ERROR: Get Data of the Joystick FAILED !!!");
		return E_FAIL;
	}

	// joystick buttons.
	for(DWORD currBut = 0; currBut < m_iNbButtons; currBut++)
	{
		m_bReleased[currBut] = false;

		if(joyState.rgbButtons[currBut] & 0x80)
			m_bPressed[currBut] = true;
		else
		{
			if(m_bPressed[currBut])
				m_bReleased[currBut] = true;
			m_bPressed[currBut] = false;
		}
	}

	// position of the stick
	m_lX = joyState.lX;
	m_lY = joyState.lY;

	return S_OK;
}

BBOOL    OcelotJoystick::isPressed(UINT butID)
{
	if(butID < m_iNbButtons) return m_bPressed[butID]; return false;
}

BBOOL    OcelotJoystick::isReleased(UINT butID)
{
	if(butID < m_iNbButtons) return m_bReleased[butID]; return false;
}

BBOOL    OcelotJoystick::joystickFound()
{
	return m_bJoystickFound;
}

BBOOL    OcelotJoystick::enumJoyCallBack(const DIDEVICEINSTANCE* pI)
{
	// try to crank up
	if(SUCCEEDED(crankUp(pI->guidInstance, &c_dfDIJoystick)))
	{
		m_bJoystickFound = true;
		strcpy(m_cName, (CCHAR*)pI->tszProductName);
		return DIENUM_STOP;
	}

	return DIENUM_CONTINUE;
}

VVOID    OcelotJoystick::getName(CCHAR* pJoystickName)
{
	memcpy(pJoystickName, m_cName, sizeof(CCHAR) * 256);
}
