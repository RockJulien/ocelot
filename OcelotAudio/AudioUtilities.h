#ifndef DEF_AUDIOUTILITIES_H
#define DEF_AUDIOUTILITIES_H 

typedef long long      BIGLINT;
typedef long           LINT;
typedef int            BIGINT;
typedef short          SMALLINT;
typedef float          FFLOAT;
typedef bool           BBOOL;
typedef double         DFLOAT;
typedef char           CCHAR;
typedef wchar_t        WCHAR;
typedef unsigned long  ULINT;
typedef unsigned int   UBIGINT;
typedef unsigned short USMALLINT;
typedef unsigned char  UCCHAR;
typedef void           VVOID;
typedef void*          UNKNOWN;

#endif