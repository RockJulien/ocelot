#ifndef DEF_OCELOTAUDIO_H
#define DEF_OCELOTAUDIO_H

#include "IOcelotAudioDevice.h"

namespace Audio
{
	class OcelotAudio
	{
	private:

			// Attributes
			IOcelotAudioDevice* m_pDevice;
			HINSTANCE           m_hInst;
			HMODULE             m_hDllMod;

	public:

			// Constructor & Destructor
			OcelotAudio(HINSTANCE hInst);
			~OcelotAudio();

			// Methods
			HRESULT createDevice();
			VVOID    release();

			// Accessors
	inline  IOcelotAudioDevice* getDevice() const { return m_pDevice;}
	inline  HINSTANCE           getModule() const { return m_hDllMod;}
	};
}

#endif