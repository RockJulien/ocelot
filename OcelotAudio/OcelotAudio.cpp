#include "OcelotAudio.h"

namespace Audio
{
OcelotAudio::OcelotAudio(HINSTANCE hInst) :
m_pDevice(NULL), m_hInst(hInst), m_hDllMod(NULL)
{

}

OcelotAudio::~OcelotAudio()
{
	release();
}

HRESULT OcelotAudio::createDevice()
{
	m_hDllMod = LoadLibraryExA("OcelotDirectAudio.dll", NULL, 0);
	if(!m_hDllMod)
	{
		MessageBoxA(NULL, "OcelotDirectAudio.dll load from lib FAILED !!!", "Ocelot Engine - ERROR", MB_OK | MB_ICONERROR);
		return E_FAIL;
	}

	CREATEAUDIODEVICE AD = NULL;

	// return a pointer to the function in the DLL.
	AD = (CREATEAUDIODEVICE)GetProcAddress(m_hDllMod, "createAudioDevice");

	// call the function for creating device.
	if(FAILED(AD(m_hDllMod, &m_pDevice)))
	{
		MessageBoxA(NULL, "Create Audio Device from lib FAILED !!!", "Ocelot Engine - ERROR", MB_OK | MB_ICONERROR);
		m_pDevice = NULL;
		return E_FAIL;
	}

	// Done.
	return S_OK;
}

VVOID    OcelotAudio::release()
{
	RELEASEAUDIODEVICE RAD = NULL;

	if(m_hDllMod)
		RAD = (RELEASEAUDIODEVICE)GetProcAddress(m_hDllMod, "releaseAudioDevice");

	// then, call the appropriate function pointed for destroying the device.
	if(m_pDevice)
		if(FAILED(RAD(&m_pDevice)))
			m_pDevice = NULL;
}
}