#ifndef DEF_MAIN_H
#define DEF_MAIN_H

#include "DirectXAPI\DXOcelotApp.h"
#include "..\Ocelot Editor\XML\OcelotRapidXML.h"
#include "Camera\OcelotCameraFactory.h"
#include "Geometry\CubeMap.h"
#include "Geometry\Terrain.h"
#include "GameCursor.h"
#include "AudioManager\AudioManager.h"
#include "Physics\ParticleForceGenerators.h"
#include "Particle System\ParticleEffectsFactory.h"
#include "Physics\IParticleContactResolver.h"
#include "Physics\ParticleContacts.h"
#include "Particle System\Lightning.h"
#include <ctime>
#include <vector>

using namespace Ocelot;

BIGINT WINAPI WinMain(  HINSTANCE hInstance,
						HINSTANCE hPrevInstance,
						LPSTR lpCmdLine,
						BIGINT nShowCmd );

class FirstPersonShooterDemo : public DXOcelotApp
{
public:

	// Constructor & Destructor
	FirstPersonShooterDemo();
	~FirstPersonShooterDemo();

	// Methods
	BIGINT  run();
	VVOID   update(FFLOAT deltaTime);
	VVOID   renderFrame();
	VVOID   initApp(HINSTANCE hInstance, BIGINT nShowCmd, LPWSTR windowName);
	VVOID   handleInput();
	VVOID   clean3DDevice();
	VVOID   resize();
	IOcelotMesh* underMouse(Vector2 curs, UBIGINT& pIndex);
	LRESULT AppProc(UBIGINT message, WPARAM wParam, LPARAM lParam);

	// XML Load data methods
	VVOID ResolveContacts(const FFLOAT delta);
	VVOID LoadXMLData();
	VVOID LoadXMLMeshData(OcelotXMLDoc& doc);
	VVOID LoadXMLParticleEffectsData(OcelotXMLDoc& doc);
	VVOID PlayEndSong(const BBOOL end);

private:

	GameCursor*				   mCentralCursor;
	OcelotLight*			   mLight; // Global light.
	OcelotFog*				   mFog;

	Terrain*				   mTerrain;
	TriPlanarBumpMaterial*     mTerrainMaterial;
	OcelotCamera*              mCamera;
	IOcelotCameraController*   mController;
	Vector2					   m_vOldMousePos;

	std::vector<IOcelotMesh*> static_meshes;
	std::vector<IOcelotMesh*> moving_meshes;

	std::vector<ParticleSystem*> effects;
	std::vector<BBOOL> showEffects;

	ParticleSystemInfo fireInfo;

	IOcelotMesh* m_selectedMesh;

	ParticleGravityGenerator gravGen;

	IParticleContactResolver* contactResolver;
	std::vector<IParticleContact*> contacts;

	std::vector<FFLOAT> spawnDepths;
	std::vector<FFLOAT> spawnWidths;

	std::vector<Lightning*> bolts;

	BBOOL gameFinished;

	BIGINT score;
};

#endif
