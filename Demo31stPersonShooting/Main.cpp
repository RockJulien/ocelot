#include "Main.h"
#include "Geometry\Sphere.h"
#include "Geometry\Cube.h"
#include "Geometry\Grid.h"
#include "..\Ocelot Editor\Collisions\AABB.h"
#include "..\Ocelot Editor\Utilities\PickPair.h"

using namespace Ocelot;

BIGINT WINAPI WinMain(  HINSTANCE hInstance, 
						HINSTANCE hPrevInstance, 
						LPSTR lpCmdLine, 
						BIGINT nShowCmd)
{
	FirstPersonShooterDemo app;
	app.initApp(hInstance, SW_HIDE, L"First Person Shooter Demo");

	return app.run();
}

FirstPersonShooterDemo::FirstPersonShooterDemo()
	: DXOcelotApp(), mCentralCursor(nullptr)
{	
	srand(static_cast<UBIGINT>(time(0)));
	mCamera = NULL;
	mController = NULL;
	m_selectedMesh = nullptr;
	score = 0;
	gameFinished = false;
}

FirstPersonShooterDemo::~FirstPersonShooterDemo()
{
	clean3DDevice();

	DeletePointer(mCamera);
	DeletePointer(mController);
	DeletePointer(mCentralCursor);

	if(AudioMng)
		AudioMng->release();
}

BIGINT FirstPersonShooterDemo::run()
{
	MSG msg = {0};

	m_timer.resetTimer();

	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))      
		{												  
			// translate keystroke messages into the right format
			TranslateMessage(&msg);

			// send the message to the WindowProc
			DispatchMessage(&msg);
		}
		else
		{
			if(m_bPaused)
			{
				Sleep(50);
			}
			else
			{
				m_timer.tick();
				update(m_timer.getDeltaTime());
				renderFrame();
			}
		}
	}
	// clean up DirectX COM
	clean3DDevice();
	return msg.wParam;
}

VVOID FirstPersonShooterDemo::update(FFLOAT deltaTime)
{
	DXOcelotApp::update(deltaTime);

	gameFinished = (BIGINT)m_timer.getGameTime() > 60;
	if
		( gameFinished )
	{
		PlayEndSong(gameFinished);
	}

	// update light if needed.
	// if point light??
	FFLOAT radius = 50.0f;
	Vector3 moves;
	moves.x(radius * cosf(m_timer.getGameTime()));
	moves.y(10.0f);
	moves.z(radius * sinf(m_timer.getGameTime()));
	this->mLight->m_vPosition = moves;

	this->handleInput();

	HR(AudioMng->update(deltaTime));

	// refresh frustum planes.
	this->mCamera->computeFrustumPlane();

	// update the camera once for all
	this->mController->update(deltaTime);

	if
		(this->mCentralCursor)
	{
		this->mCentralCursor->update(deltaTime);
	}

	OcelotCameraInfo lInfo = mCamera->Info();
	Vector3 currentCamPos = lInfo.Eye();
	currentCamPos.y(10.0f);
	lInfo.SetEye(currentCamPos);
	mCamera->SetInfo(lInfo);

	PForceRegistry->UpdateForces(deltaTime);

	for(UINT currMesh = 0; currMesh < (UINT)static_meshes.size(); currMesh++)
	{
		static_meshes[currMesh]->update(deltaTime);
	}

	for(UINT currMesh = 0; currMesh < (UINT)moving_meshes.size(); currMesh++)
	{
		IOcelotMesh* currentMesh = moving_meshes[currMesh];
		const AABBMinMax3D* lBounds = dynamic_cast<const AABBMinMax3D*>( currentMesh->LocalBounds() );
		FFLOAT lRadius = 0.0f;
		if 
			( lBounds != NULL )
		{
			Vector3 lPosition = currentMesh->Position();
			lRadius = lBounds->GetRadius();
			//currentMesh->SetVisibility(this->mCamera->cullMesh(lPosition.getX(), lPosition.getY(), lPosition.getZ(), lRadius));
		}

		if (showEffects[currMesh])
		{
			ParticleSystem* currentSystem = effects[currMesh];
			currentSystem->SetEmitterPosition(moving_meshes[currentSystem->GetMeshIndex()]->Position());
			currentSystem->Update(deltaTime, m_timer.getGameTime());
		}

		currentMesh->update(deltaTime);

		FFLOAT lFactor = 50.0f;
		if
			(currentMesh->Position().getY() < -lRadius * lFactor)
		{
			currentMesh->SetPosition(spawnWidths[currMesh], (FFLOAT)(rand() % (BIGINT)lFactor) + lFactor, spawnDepths[currMesh]);
			currentMesh->GetParticle()->SetVelocity(Vector3(0.0f, 0.0f, 0.0f));
			showEffects[currMesh] = false;
		}
	}

	BIGINT numBolts = (BIGINT)bolts.size();
	BIGINT numCubes = (BIGINT)moving_meshes.size();
	for (BIGINT i = 0; i < numBolts; ++i)
	{
		BIGINT rand1 = rand() % (numCubes - 1);
		BIGINT rand2 = rand() % (numCubes - 1);
		
		bolts[i]->Info().SetStartpoint(moving_meshes[rand1]->Position());
		bolts[i]->Info().SetEndpoint(moving_meshes[rand2]->Position());
		bolts[i]->SetTime(m_timer.getGameTime());
		bolts[i]->update(deltaTime);
	}

	this->ResolveContacts(deltaTime);
}

VVOID FirstPersonShooterDemo::renderFrame()
{
	DXOcelotApp::renderFrame();

	m_device->OMSetDepthStencilState(0, 0);
	FFLOAT blendFactor[] = {0.0f, 0.0f, 0.0f, 0.0f};
	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	if
		(this->mCentralCursor)
	{
		this->mCentralCursor->renderMesh();
	}

	std::vector<IOcelotMesh*>::iterator end = static_meshes.end();
	std::vector<IOcelotMesh*>::iterator it;
	for(it = static_meshes.begin(); it != end; ++it)
	{
		(*it)->renderMesh();
	}

	end = moving_meshes.end();
	for(it = moving_meshes.begin(); it != end; ++it)
	{
		(*it)->renderMesh();
	}

	BIGINT numBolts = (BIGINT)bolts.size();
	for(BIGINT i = 0; i < numBolts; ++i)
	{
		bolts[i]->renderMesh();
	}

	std::vector<ParticleSystem*>::iterator it2;
	std::vector<ParticleSystem*>::iterator end2 = effects.end();
	for(it2 = effects.begin(); it2 != end2; ++it2)
	{
		(*it2)->Render();
	}

	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	wostringstream outs;
	outs << "Score: " << score << "\n" << "Time: " << (BIGINT)m_timer.getGameTime();
	std::wstring scoreString = outs.str();

	RECT R = {10,10,0,0};
	m_device->RSSetState(0);
	m_font->DrawTextW(0, scoreString.c_str(), -1, &R, DT_NOCLIP, DXColor::OcelotColToDXCol(OcelotColor::WHITE)); 
	
	m_swapChain->Present(1,0);   
}

VVOID FirstPersonShooterDemo::initApp(HINSTANCE pInstance, BIGINT pShowCmd, LPWSTR pWindowName)
{
	DXOcelotApp::initApp(pInstance, pShowCmd, pWindowName);

	AudioMng->initialize(m_hAppInstance);

	ResFactory->SetDevice(this->m_device);
	ResFactory->CreateTextures();

	// Initialize the camera.
	OcelotCameraInfo lCamInfo(Vector3(0.0f, 10.0f, -10.0f),
							  Vector3(0.0f, 0.0f, 1.0f),
							  (FFLOAT)m_iWindowWidth / m_iWindowHeight,
							  1000.0f);

	this->mCamera = new OcelotCamera(lCamInfo);

	// And create the controller
	this->mController = ControlFact->createController(this->mCamera, CAMERA_CONTROLLER_FLYER); // Finish the walker camera.

	// Create the light  
	this->mLight = new OcelotLight(  Vector3(-300.0f, 100.0f, -180.0f), // eye    
									 Vector3(0.0f, 0.0f, 1.0f),   // dir    
									 Vector3(1.0f, 0.0f, 0.0f),   // atten  
									 OcelotColor::DARKGREY,		  // ambient   
									 OcelotColor::LIGHTGREY,      // diffuse  
									 OcelotColor::GREY,			  // spec  
									 1000.0f,					  // range 
									 64.0f,						  // spotPow 
									 1.0f);						  // specPow

	// Create the fog.
	this->mFog = new OcelotFog(20.0f,
							   300.0f);

	WSTRING lDefTextPath[3];
	lDefTextPath[0] = L"Dirt.jpg";
	lDefTextPath[1] = L"Grass.jpg";
	lDefTextPath[2] = L"Rock.jpg";
	// Create common textures for saving memory (will be the same).
	this->mTerrainMaterial = new TriPlanarBumpMaterial();
	ResFactory->LoadTriPlanarMaterial(lDefTextPath, *this->mTerrainMaterial);

	CubeMapInfo infoCM( this->m_device, 
						this->mCamera, 
						5000.0f, 
						L"WaterHill.dds", 
						L"..\\Effects\\CubeMap.fx" );

	static_meshes.push_back(new CubeMap(infoCM));

	this->LoadXMLData();

	// Create the game central cursor.
	OverlayInfo lCursorInfo(this->m_device,
							this->mCamera,
							L"GameCursor.png",
							Vector2(0.0f, 0.0f),
							Vector2(0.1f, 0.1f));
	this->mCentralCursor = new GameCursor(lCursorInfo);
	this->mCentralCursor->createInstance();

	std::vector<IOcelotMesh*>::iterator it;
	std::vector<IOcelotMesh*>::iterator end = static_meshes.end();
	for(it = static_meshes.begin(); it != end; ++it)
		(*it)->createInstance();

	end = moving_meshes.end();
	for(it = moving_meshes.begin(); it != end; ++it)
	{
		(*it)->createInstance();
	}

	AudioMng->createSound(L"electro.wav", false, true);
	AudioMng->createSound(L"pew.wav", false, false);
	AudioMng->createSound(L"ping.wav", false, false);

	gravGen.SetGravity(Vector3(0.0f, -9.81f, 0.0f));
	for(UINT currMesh = 0; currMesh < (UINT)moving_meshes.size(); currMesh++)
	{
		PForceRegistry->Add(moving_meshes[currMesh]->GetParticle(), &gravGen);
	}

	fireInfo.device = m_device;
	fireInfo.camera = mCamera;
	fireInfo.maxnumparticles = 300;
	fireInfo.spread = 1;
	fireInfo.texture = ResFactory->GetResource(L"flame.dds");

	effects.reserve(moving_meshes.size());
	BIGINT numEffects = (BIGINT)moving_meshes.size();

	showEffects.reserve(moving_meshes.size());
	for(BIGINT i = 0; i < numEffects; ++i)
	{
		showEffects.push_back(false);
	}

	contactResolver = PContactResolver;
	contactResolver->setNumIterations(8);

	LightningInfo lInfo = { m_device, mCamera, L"..\\Effects\\LightningEffect.fx", ResFactory->GetResource(L"lbolt.dds"), moving_meshes[0]->Position(), moving_meshes[1]->Position(), OcelotColor::CYAN, 3, 10.0f, 0.05f };
	for (BIGINT i = 0; i < 1; ++i)
	{
		bolts.push_back(new Lightning(lInfo));
		bolts[i]->createInstance();
		bolts[i]->Info().SetDuration(0.05f);
	}

	// show the window
	ShowWindow(m_mainWindowInstance, SW_SHOW);
}

VVOID FirstPersonShooterDemo::handleInput()
{
	if
		(this->mController != NULL)
	{
		if
			(InMng->isPressed(Keyboard, OCELOT_LEFT))
		{
			this->mController->move(Direction::LeftSide);
		}
		if
			(InMng->isPressed(Keyboard, OCELOT_RIGHT))
		{
			this->mController->move(Direction::RightSide);
		}
		if
			(InMng->isPressed(Keyboard, OCELOT_UP))
		{
			this->mController->move(Direction::Forward);
		}
		if
			(InMng->isPressed(Keyboard, OCELOT_DOWN))
		{
			this->mController->move(Direction::Backward);
		}

		if
			(InMng->isPressed(Keyboard, OCELOT_B))
		{
			for (UINT currMesh = 0; currMesh < (UINT)moving_meshes.size(); currMesh++)
			{
				IOcelotMesh* currentMesh = moving_meshes[currMesh];
				if (currentMesh->IsBounded())
				{
					currentMesh->HideBound();
				}
				else
				{
					currentMesh->ShowBound();
				}
			}
		}
	}
}

VVOID FirstPersonShooterDemo::clean3DDevice()
{
	DXOcelotApp::clean3DDevice();

	std::vector<IOcelotMesh*>::iterator lStatIter = static_meshes.begin();
	for (; lStatIter != static_meshes.end(); lStatIter++)
	{
		DeletePointer((*lStatIter));
	}
	static_meshes.clear();

	std::vector<IOcelotMesh*>::iterator lMovIter = moving_meshes.begin();
	for (; lMovIter != moving_meshes.end(); lMovIter++)
	{
		DeletePointer((*lMovIter));
	}
	moving_meshes.clear();

	std::vector<ParticleSystem*>::iterator lEffIter = effects.begin();
	for (; lEffIter != effects.end(); lEffIter++)
	{
		DeletePointer((*lEffIter));
	}
	effects.clear();
}

VVOID FirstPersonShooterDemo::resize()
{
	DXOcelotApp::resize();

	// refresh the aspect of the camera if the
	// window is resized.
	if
		(this->mCamera)
	{
		FFLOAT lAspect = (FFLOAT)m_iWindowWidth / m_iWindowHeight;
		OcelotCameraInfo lInfo = this->mCamera->Info();
		lInfo.SetAspect(lAspect);  // aspect ratio
		this->mCamera->SetInfo(lInfo);
	}

	if
		(this->mCentralCursor)
	{
		this->mCentralCursor->Info().OnResize(this->m_iWindowWidth, this->m_iWindowHeight);
	}
}

VVOID FirstPersonShooterDemo::LoadXMLData()
{
	OcelotXMLFile xmlfile("XML\\OcelotGameData.xml");
	OcelotXMLDoc doc;
	try
	{
		doc.parse<rapidxml::parse_no_data_nodes>(xmlfile.data());
	}
	catch(rapidxml::parse_error e)
	{}

	LoadXMLMeshData(doc);
	LoadXMLParticleEffectsData(doc);
}

LRESULT FirstPersonShooterDemo::AppProc(UINT message,WPARAM wParam,LPARAM lParam)
{
	Vector2 mousePosition;

	switch(message)
	{
	case WM_LBUTTONDOWN:
		if(wParam & MK_LBUTTON)
		{
			SetCapture(m_mainWindowInstance);

			m_vOldMousePos.x((FFLOAT)LOWORD(lParam));
			m_vOldMousePos.y((FFLOAT)HIWORD(lParam));
		}
		return 0;
		break;

	case WM_RBUTTONDOWN:
		AudioMng->restartASound(L"pew.wav");
		UBIGINT lIndex;
		m_selectedMesh = underMouse(Vector2(this->m_iWindowWidth * 0.5f, this->m_iWindowHeight * 0.5f/*(FFLOAT)LOWORD(lParam), (FFLOAT)HIWORD(lParam)*/), lIndex);
		if(m_selectedMesh)
		{
			Vector3 lEye = this->mCamera->Info().Eye();
			Vector3 lTarget = m_selectedMesh->Position();
			m_selectedMesh->GetParticle()->AddForce((lTarget - lEye).Vec3Normalise() * 1800.0f);
			showEffects[lIndex] = true;
			if(!gameFinished) score += 5;
		}
		return 0;
		break;

	case WM_LBUTTONUP:
		ReleaseCapture();
		return 0;
		break;

	case WM_MOUSEMOVE:
		if(wParam & MK_LBUTTON)
		{
			mousePosition.x((FFLOAT)LOWORD(lParam));
			mousePosition.y((FFLOAT)HIWORD(lParam));

			FFLOAT lToRad = PI / 180.0f;
			// Make each pixel correspond to a quarter of a degree.
			FFLOAT lDx = (0.25f * static_cast<FFLOAT>(mousePosition.getX() - this->m_vOldMousePos.getX())) * lToRad;
			FFLOAT lDy = (0.25f * static_cast<FFLOAT>(mousePosition.getY() - this->m_vOldMousePos.getY())) * lToRad;

			// add extra camera controler stuff here
			this->mController->pitch(lDy);
			this->mController->yaw(lDx);

			m_vOldMousePos = mousePosition;
		}
		return 0;
	}
	return DXOcelotApp::AppProc(message, wParam, lParam);
}

IOcelotMesh* FirstPersonShooterDemo::underMouse(Vector2 curs, UBIGINT& pIndex)
{
	// algorithm which determine which mesh is under 
	// the cursor of the mouse by unprojecting.
	Matrix4x4 lMatProj = mCamera->Proj();
	Matrix4x4 lMatView = mCamera->View();

	Vector3 lEye = this->mCamera->Info().Eye();

	// determine the ray thanks to screen position
	// in view space.
	FFLOAT lDirX = (+2.0f * curs.getX() / m_iWindowWidth - 1.0f) / lMatProj.get_11();
	FFLOAT lDirY = (-2.0f * curs.getY() / m_iWindowHeight + 1.0f) / lMatProj.get_22();

	Vector3 lOrigine(0.0f, 0.0f, 0.0f);
	Vector3 lRayDir(lDirX, lDirY, 1.0f);

	// Get the inverse view for transforming mesh world to local.
	Matrix4x4 lMatViewInverse;
	lMatView.Matrix4x4Inverse(lMatViewInverse);

	const IOcelotMesh* lClosest = NULL;
	// transform from world space to local space meshes
	size_t lCount = this->moving_meshes.size();
	if
		(lCount != 0)
	{
		std::vector<PickPair> lMatches;
		for
			(UBIGINT lCurrMesh = 0; lCurrMesh < (UBIGINT)lCount - 1; lCurrMesh++)
		{
			IOcelotMesh* lCurrMeshInstance = this->moving_meshes[lCurrMesh];

			Matrix4x4 lCurrMeshInvWorld;
			const Matrix4x4* lCurrMeshWorld = lCurrMeshInstance->World();
			lCurrMeshWorld->Matrix4x4Inverse(lCurrMeshInvWorld);

			Matrix4x4 lCurrMeshLocal;
			lMatViewInverse.Matrix4x4Mutliply(lCurrMeshLocal, lCurrMeshInvWorld);

			// apply the local world to origine and rayDir vec.
			Vector3 lLocalOrigine = Vector3::Vec4ToVec3(lOrigine.Vec3VecMatrixProduct(lCurrMeshLocal));
			Vector3 lLocalRayDir = Vector3::Vec4ToVec3(lRayDir.Vec3VecMatrixTransformNormal(lCurrMeshLocal));
			lLocalRayDir.Vec3Normalise();

			Ray lRay(lLocalOrigine, lLocalRayDir);

			// now determine which mesh this ray intersect
			// and return it if exists.
			const IRayIntersectable* lBounds = lCurrMeshInstance->LocalBounds();
			if
				( lBounds != NULL &&
				  lBounds->IntersectsAndFindPoint( lRay ) )
			{
				PickPair lPair = PickPair(lCurrMeshInstance, lRay.GetIntersectionPoint());
				lPair.SetIndex(lCurrMesh);
				lMatches.push_back(lPair);
			}
		}

		FFLOAT lMinDistance = MaxFloat;
		// Retrieve the closest from the camera and return it.
		std::vector<PickPair>::const_iterator lIter = lMatches.begin();
		for
			(; lIter != lMatches.end(); lIter++)
		{
			FFLOAT lCurrDistance = lEye.Vec3DistanceBetween(lIter->IntersectedPoint());
			if
				(lMinDistance > lCurrDistance)
			{
				lMinDistance = lCurrDistance;
				lClosest = lIter->Picked();
				pIndex = lIter->Index();
			}
		}
	}

	return const_cast<IOcelotMesh*>(lClosest);
}

VVOID FirstPersonShooterDemo::ResolveContacts(const FFLOAT deltaTime)
{
	const AABBMinMax3D* lTerrainBounds = dynamic_cast<const AABBMinMax3D*>(this->mTerrain->WorldBounds());
	contacts.clear();
	for
		(BIGINT i = 0; i < (BIGINT)moving_meshes.size() - 1; ++i)
	{
		IOcelotMesh* objA = moving_meshes[i];
		for
			(BIGINT j = i + 1; j < (BIGINT)moving_meshes.size(); ++j)
		{
			IOcelotMesh* objB = moving_meshes[j];

			if (!objA->UsesPhysics() && !objB->UsesPhysics())
			{
				continue;
			}

			const AABBMinMax3D* lBoundsA = dynamic_cast<const AABBMinMax3D*>(objA->WorldBounds());
			const AABBMinMax3D* lBoundsB = dynamic_cast<const AABBMinMax3D*>(objB->WorldBounds());

			if
				( lBoundsA != NULL &&
				  lBoundsB != NULL &&
				  lBoundsA->Intersects( lBoundsB ) )
			{
				Vector3 contactNormal = (objA->Position() - objB->Position()).Vec3Normalise();
				if 
					( !gameFinished )
				{
					score += 1;
				}
				AudioMng->restartASound(L"ping.wav");
				contacts.push_back(new ParticleContact(objA->GetParticle(), objB->GetParticle(), 0.6f, contactNormal, lBoundsA->CalculatePenetration(lBoundsB) * deltaTime));
			}
		}
	}

	// Test moving objects against static ones.
	for
		( BIGINT i = 0; i < (BIGINT)static_meshes.size(); i++ )
	{
		IOcelotMesh* lStatic = static_meshes[i];
		for
			( BIGINT j = 0; j < (BIGINT)moving_meshes.size(); j++ )
		{
			IOcelotMesh* lMoving = moving_meshes[j];

			if (!lStatic->UsesPhysics() && !lMoving->UsesPhysics())
			{
				continue;
			}

			const AABBMinMax3D* lBoundsA = dynamic_cast<const AABBMinMax3D*>(lStatic->WorldBounds());
			const AABBMinMax3D* lBoundsB = dynamic_cast<const AABBMinMax3D*>(lMoving->WorldBounds());

			if
				( lBoundsA != NULL &&
				  lBoundsB != NULL &&
				  lBoundsA->Intersects( lBoundsB ) )
			{
				Vector3 lContactNormal = Vector3(0.0f, 1.0f, 0.0f);
				const Matrix4x4* lWorld = lStatic->World();
				lContactNormal = Vector3::Vec4ToVec3(lContactNormal.Vec3VecMatrixTransformNormal(*lWorld));
				contacts.push_back(new ParticleContact(lStatic->GetParticle(), lMoving->GetParticle(), 0.6f, lContactNormal, lBoundsA->CalculatePenetration(lBoundsB) * deltaTime));
			}
		}
	}

	contactResolver->ResolveContacts(contacts, deltaTime);
}

VVOID FirstPersonShooterDemo::LoadXMLMeshData(OcelotXMLDoc& doc)
{
	OcelotXMLNode *root = doc.first_node("ocelot");
	OcelotXMLNode *meshesData = root->first_node("meshes");

	BBOOL enabled = atoi(meshesData->first_attribute("enabled")->value()) == 1;
	if(!enabled) 
	{
		return;
	}
	
	for
		( OcelotXMLNode *currentMesh = meshesData->first_node(); currentMesh; currentMesh = currentMesh->next_sibling() )
	{
		BBOOL enabled = atoi(currentMesh->first_attribute("enabled")->value()) == 1;
		if(!enabled) 
		{
			continue;
		}

		string type = currentMesh->first_node()->value();
		if
			( type == "cube" )
		{
			Vector3 pos = OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x","y","z");
			Vector3 radius = OcelotXML::ExtractVector3(currentMesh->first_node("radius"), "x", "y", "z");
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			for 
				( BIGINT i = 0; i < 10; i++ ) // Just add more targets by looping on each creation
			{
				pos.z((FFLOAT)i* 5.0f);
				spawnDepths.push_back(pos.getZ());
				spawnWidths.push_back(pos.getX());
				CubeInfo CI(this->m_device,
							this->mCamera,
							fileNameW,
							L"..\\Effects\\BumpMapping.fx",
							this->mLight,
							this->mFog,
							pos,
							radius);
				moving_meshes.push_back(new Cube(CI));
				moving_meshes.back()->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
			}

		}
		else if 
			( type == "grid" )
		{
			Vector3 pos = OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x", "y", "z");
			Vector3 rot = OcelotXML::ExtractVector3(currentMesh->first_node("rotation"), "x", "y", "z");
			BIGINT rows = atoi(currentMesh->first_attribute("rows")->value());
			BIGINT cols = atoi(currentMesh->first_attribute("cols")->value());
			FFLOAT width = (FFLOAT)atof(currentMesh->first_attribute("width")->value());
			FFLOAT depth = (FFLOAT)atof(currentMesh->first_attribute("depth")->value());
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			GridInfo GI( this->m_device,
						 this->mCamera,
						 fileNameW,
						 L"..\\Effects\\BumpMapping.fx",
						 this->mLight,
						 this->mFog,
						 pos,
						 rows,
						 cols,
						 width,
						 depth );

			Grid* lNewGrid = new Grid(GI);
			IMeshEditor* lEditor = lNewGrid->StartEdition();
			lEditor->RotateX(rot.getX());
			lEditor->RotateY(rot.getY());
			lEditor->RotateZ(rot.getZ());
			DeletePointer(lEditor);
			static_meshes.push_back(lNewGrid);
			static_meshes.back()->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
		}
		else if
			( type == "terrain" )
		{
			Vector3 pos  = OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x", "y", "z");
			BIGINT rows  = atoi(currentMesh->first_attribute("rows")->value());
			BIGINT cols  = atoi(currentMesh->first_attribute("cols")->value());
			FFLOAT width = (FFLOAT)atof(currentMesh->first_attribute("width")->value());
			FFLOAT depth = (FFLOAT)atof(currentMesh->first_attribute("depth")->value());
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			TerrainInfo TI( this->m_device,
							this->mCamera,
							L"..\\Effects\\TriPlanarBumpMapping.fx",
							this->mLight,
							this->mFog,
							this->mTerrainMaterial,
							pos,
							rows,
							cols,
							width,
							depth );

			this->mTerrain = new Terrain(TI);
			
			static_meshes.push_back(this->mTerrain);
			static_meshes.back()->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
		} 
	}
}

VVOID FirstPersonShooterDemo::LoadXMLParticleEffectsData(OcelotXMLDoc& doc)
{
	OcelotXMLNode *root = doc.first_node("ocelot");
	OcelotXMLNode *particleSystemsData = root->first_node("particleSystems");

	BBOOL enabled = atoi(particleSystemsData->first_attribute("enabled")->value()) == 1;
	if(!enabled) 
	{
		return;
	}

	for(OcelotXMLNode *currentMesh = particleSystemsData->first_node(); currentMesh; currentMesh = currentMesh->next_sibling())
	{
		BBOOL enabled = atoi(currentMesh->first_attribute("enabled")->value()) == 1;
		if(!enabled) 
		{
			continue;
		}

		ParticleSystemInfo info;

		info.device = m_device;
		info.camera = mCamera;

		info.maxnumparticles = atoi(currentMesh->first_attribute("numParticles")->value());
		info.spread = (FFLOAT)atof(currentMesh->first_attribute("spread")->value());

		std::string fileName = currentMesh->first_attribute("textureFileName")->value();
		std::wstring fileNameW(fileName.begin(), fileName.end());
		info.texture = ResFactory->GetResource(fileNameW);

		std::string typeS = currentMesh->first_node("type")->value();
		BIGINT type;
		if(typeS == "rain")
		{
			type = PE_RAIN;
		} else if(typeS == "snow")
		{
			type = PE_SNOW;
		} else if(typeS == "smoke")
		{
			type = PE_SMOKE;
		} else if(typeS == "fire")
		{
			type = PE_FIRE;
		}

		for (BIGINT i = 0; i < (BIGINT)moving_meshes.size(); ++i)
		{
			effects.push_back(ParticleFXFactory->CreateParticleEffect(ParticleEffectType(type), info));
			IParticleSystem* ps = effects.back();
			ps->SetMeshIndex(i);
		}
	}
}

VVOID FirstPersonShooterDemo::PlayEndSong(const BBOOL end)
{
	static BBOOL isPlayingEndSong = false;

	if(end && !isPlayingEndSong)
	{
		isPlayingEndSong = true;
		AudioMng->stopASound(L"electro.wav");
		AudioMng->createSound(L"endgame.wav", false, true);
	}
}