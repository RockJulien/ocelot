#include <iostream>
#include "SimpleWindow.h"

int WINAPI WinMain( HINSTANCE hInstance,
					HINSTANCE hPrevInstance,  // useless
					LPSTR lpCmdLine,          // useless
					int nShowCmd )
{

	SimpleWindow app;
	app.initApp(hInstance, SW_HIDE, L"The Ocelot Show");
	app.initTest();
	

	return app.run();
}