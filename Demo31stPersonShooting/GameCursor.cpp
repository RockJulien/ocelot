#include "GameCursor.h"
#include "ResourceManager\ResourceFactory.h"

using namespace Ocelot;
using namespace std;

GameCursor::GameCursor() :
mInfo(), m_fxTexture(NULL), m_fxWorldViewProj(NULL), mTransparentBlendState(NULL)
{

}

GameCursor::GameCursor(const OverlayInfo& pInfo) :
mInfo(pInfo), m_fxTexture(NULL), m_fxWorldViewProj(NULL), mTransparentBlendState(NULL)
{

}

GameCursor::~GameCursor()
{
	this->releaseInstance();
}

VVOID  GameCursor::update(FFLOAT deltaTime)
{
	// Update the scaling and translation matrices using
	// the Info screen position and scale.
	Matrix4x4 lIdentity;
	lIdentity.Matrix4x4Scaling( this->m_matScaling,
								this->mInfo.Scale().getX(),
								this->mInfo.Scale().getY(),
								1.0f );

	Vector2 lScreenPos = this->mInfo.ScreenPosition();
	FFLOAT lXPos = lScreenPos.getX() / this->mInfo.WindowSize().getX();
	FFLOAT lYPos = lScreenPos.getY() / this->mInfo.WindowSize().getY();
	lIdentity.Matrix4x4Translation( this->m_matTranslation,
									lXPos,
									lYPos,
									0.0f );

	// Update world matrices in ancestor
	OcelotDXMesh::update(deltaTime);

	// refresh variables in the shader file
	HR(this->updateEffectVariables());
}

BIGINT GameCursor::updateEffectVariables()
{
	HRESULT lHr = S_OK;

	HR(m_fxWorldViewProj->SetMatrix((FFLOAT*)&this->m_matWorld));
	HR(m_fxTexture->SetResource(mInfo.Texture()));

	return (BIGINT)lHr;
}

BIGINT GameCursor::renderMesh()
{
	float lBlendFactor[] = { 0.0f, 0.0f, 0.0f, 0.0f };

	// set the input layout and buffers
	mInfo.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UBIGINT stride = sizeof(SimpleVertex);
	UBIGINT offset = sizeof(LINT); // Skip the VTable
	mInfo.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	mInfo.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	mInfo.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if 
		( this->m_bVisible )
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for (UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			mInfo.Device()->DrawIndexed( this->m_iNbIndices, 0, 0 );
		}
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT GameCursor::initializeLayout()
{
	HRESULT hr = S_OK;

	// Define the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0 },
	};
	UBIGINT nbElements = sizeof(layout) / sizeof(layout[0]);

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(mInfo.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

	return (BIGINT)hr;
}

BIGINT GameCursor::initializeEffect()
{
	// TransparentBS
	D3D10_BLEND_DESC lBlendState;
	ZeroMemory(&lBlendState, sizeof(D3D10_BLEND_DESC));

	lBlendState.AlphaToCoverageEnable = FALSE;
	lBlendState.BlendEnable[0] = TRUE;
	lBlendState.SrcBlend = D3D10_BLEND_SRC_ALPHA;
	lBlendState.DestBlend = D3D10_BLEND_INV_SRC_ALPHA;
	lBlendState.BlendOp = D3D10_BLEND_OP_ADD;
	lBlendState.SrcBlendAlpha = D3D10_BLEND_ONE;
	lBlendState.DestBlendAlpha = D3D10_BLEND_ZERO;
	lBlendState.BlendOpAlpha = D3D10_BLEND_OP_ADD;
	lBlendState.RenderTargetWriteMask[0] = D3D10_COLOR_WRITE_ENABLE_ALL;

	HR(this->mInfo.Device()->CreateBlendState(&lBlendState, &this->mTransparentBlendState));

	return initializeDXEffect( mInfo.Device(), L"..\\Effects\\Overlay.fx", &m_effect);
}

BIGINT GameCursor::initializeVertices()
{
	HRESULT lHr = S_OK;

	this->m_iNbVertices = 4;

	SimpleVertex lVertices[4] = 
	{
		SimpleVertex( -1.0f, -1.0f, 0.0f,
					  0.0f, 0.0f, -1.0f,
					  0.0f, 1.0f ),
		SimpleVertex( -1.0f, +1.0f, 0.0f,
					  0.0f, 0.0f, -1.0f,
					  0.0f, 0.0f ),
		SimpleVertex( +1.0f, +1.0f, 0.0f,
					  0.0f, 0.0f, -1.0f,
					  1.0f, 0.0f ),
		SimpleVertex( +1.0f, -1.0f, 0.0f,
					  0.0f, 0.0f, -1.0f,
					  1.0f, 1.0f )
	};

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage     = D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth = sizeof(SimpleVertex) * this->m_iNbVertices;
	buffDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = lVertices;

	HR( mInfo.Device()->CreateBuffer(&buffDesc, &InitData, &m_vertexBuffer ) );

	return (BIGINT)lHr;
}

BIGINT GameCursor::initializeIndices()
{
	HRESULT lHr = S_OK;

	this->m_iNbIndices = 6;

	UBIGINT lIndices[6] =
	{
		0,
		1,
		2,

		0,
		2,
		3
	};

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage     = D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth = sizeof(UBIGINT) * this->m_iNbIndices;
	buffDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = lIndices;

	HR( mInfo.Device()->CreateBuffer(&buffDesc, &InitData, &m_indiceBuffer ) );

	return (BIGINT)lHr;
}

VVOID  GameCursor::initializeEffectVariables()
{
	m_fxTechnique     = m_effect->GetTechniqueByName( "Overlay" );
	m_fxWorldViewProj = m_effect->GetVariableByName( "gWorldViewProj" )->AsMatrix();
	m_fxTexture       = m_effect->GetVariableByName( "gTexture" )->AsShaderResource();
}

VVOID  GameCursor::releaseInstance()
{
	// release every COM of DirectX
	OcelotDXMesh::releaseInstance();

	DeletePointer(this->mWorldBounds);
	DeletePointer(this->mDrawableBounds);

	m_fxTexture       = NULL;
	m_fxWorldViewProj = NULL;
}

VVOID  GameCursor::RefreshWorldBounds()
{
	// Nothing to do.
}

/*****************************************************************************************************************/
OverlayInfo::OverlayInfo() :
BaseDXData(), mTexture(NULL), mScreenPosition(0.0f), mScale(1.0f)
{
	
}

OverlayInfo::OverlayInfo(const DevPtr pDevice, const CamPtr pCamera, const WSTRING& pTexturePath, Vector2 pScreenPos, Vector2 pScale) :
BaseDXData(pDevice, pCamera), mScreenPosition(pScreenPos), mScale(pScale)
{
	this->mTexture = ResFactory->GetResource( pTexturePath );
}

OverlayInfo::~OverlayInfo()
{

}

VVOID OverlayInfo::OnResize(BIGINT pWidth, BIGINT pHeight)
{
	this->mWindowSize = Vector2((FFLOAT)pWidth, (FFLOAT)pHeight);
}
