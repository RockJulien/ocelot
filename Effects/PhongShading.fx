//--------------------------------------------------------------------------------------
//  File: PhongShading.fx
//   
//  Description: Shader file for passing meshes vertices through
//               the pipeline and being able to draw it with 
//               Phong shading effect.
//
//  Author:      Larbi Julien
//  Version:     1.0
//  Creation:    22/02/2012
//
//  (c) Copyright 2012-2015 OcelotEngine All right reserved.
//--------------------------------------------------------------------------------------

matrix World;
matrix View;
matrix Projection;
matrix WorldInvTrans;

float4 gDiffuseMtrl;
float4 gDiffuseLight;

float4 gAmbientMtrl;
float4 gAmbientLight;

float3 gLightVecW;

float4 gSpecularMtrl;
float4 gSpecularLight;
float  gSpecularPower;

float3 gEyePosW;

Texture2D gTexture;

//--------------------------------------------------------------------------------------
SamplerState gAnisoSampler
{
	Filter = ANISOTROPIC;
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float4 Posl     : POSITION;
	float3 NormalL  : NORMAL;
	//float3 TangentL : TANGENT;
	float2 TexC     : TEXCOORD;
};

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
    float4 Pos      : SV_POSITION;
	float4 PosW     : POSITION;
	float3 NormalW  : NORMAL;
	//float3 TangentW : TANGENT;
	float2 TexC     : TEXCOORD;
};

//--------------------------------------------------------------------------------------
// Vertex Shader 
//--------------------------------------------------------------------------------------
VS_OUTPUT VS( VS_INPUT vIn)
{
    VS_OUTPUT output = (VS_OUTPUT)0;
	output.Pos = mul( vIn.Posl, World);	
	output.PosW = output.Pos;
	
	output.Pos = mul( output.Pos, View );
    output.Pos = mul( output.Pos, Projection );

	// compute the world tangent
	//output.TangentW = mul(float4(vIn.TangentL, 0.0f), WorldInvTrans).xyz;

	// convert from local to world normal
	float3 wNorm = mul(float4(vIn.NormalL, 0.0f), WorldInvTrans).xyz;
	output.NormalW = normalize(wNorm);

	// pass the vertex texture into the pixel stage :-))
	output.TexC = vIn.TexC;

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( VS_OUTPUT input ) : SV_Target
{
	float4 Color  = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 tex    = gTexture.Sample(gAnisoSampler, input.TexC);
	//float3 texNor = gTextureNor.Sample(gAnisoSampler, input.TexC);
	//float4 texSpe = gTextureSpe.Sample(gAnisoSampler, input.TexC);

	// uncompress each component from [0,1] to [-1, 1]
	//texNor = (2.0f * texNor) - 1.0f;

	input.NormalW = normalize(input.NormalW);

	//float3 N = input.NormalW;   // normal
	//float3 T = normalize(input.TangentW - dot(input.TangentW, N) * N);  // tangent
	//float3 B = cross(N, T);     // binormal

	//float3x3 TBN = float3x3(T, B, N);

	// transform from tangent space to world space
	//float3 bumpedNormalW = normalize(mul(texNor, TBN));

	//----------------------compute Color----------------------------

	// Compute the reflection vector.
	float3 r = float3(0.0f, 0.0f, 0.0f);
	r = reflect(-gLightVecW, input.NormalW);   // compute the emissive light regarding to the incident one

	float3 toEye = float3(0.0f, 0.0f, 0.0f);
	toEye = normalize(gEyePosW - input.PosW);

	// Determine how much (if any) specular light makes it
	// into the eye.
	float t = 0.0f;
	t = pow(max(dot(r, toEye), 0.0f), gSpecularPower);

	// Determine the diffuse light intensity that strikes the vertex.
	float s = max(dot(gLightVecW, input.NormalW), 0.0f);

	float3 spec = float3(0.0f, 0.0f, 0.0f);
	float3 diffuse = float3(0.0f, 0.0f, 0.0f);

	if(s > 0.0f)      // if we are behind, no calculation
	{
		// Compute the ambient, diffuse, and specular terms separately.        //****************************
		spec    = t * (gSpecularMtrl * gSpecularLight).rgb;                    //   Fong lighting
		diffuse = s * (tex * gDiffuseLight).rgb;					           //****************************
	}

	float3 ambient = (tex * gAmbientLight).rgb;								   

	if(s > 0.0f)      // if we are behind, no calculation
	{
		// Sum all the terms together and copy over the diffuse alpha.
		Color.rgb = saturate(ambient + diffuse + spec);
	}
	else
	{
		Color.rgb = saturate(ambient);
	}
	Color.a   = gDiffuseMtrl.a;
	
    return Color;
}

RasterizerState rsWireframe 
{ 
	FillMode = Solid;
	CullMode = back;      
};


//--------------------------------------------------------------------------------------
technique10 Render
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, PS()));
		//SetRasterizerState(rsWireframe);
    }
}
