//--------------------------------------------------------------------------------------
//  File: terrain.fx
//   
//  Description: Shader file for passing terrain vertices through
//               the pipeline and being able to draw it.
//
//  Author:      Larbi Julien
//  Version:     1.0
//  Creation:    17/02/2012
//
//  (c) Copyright 2012-2015 OcelotEngine All right reserved.
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------


float4x4 gWorld;
float4x4 gWorldViewProj;
float4   gLightDir;
bool     gWireFrame;
float    gHeightRange;     // if terrain is deformed for multitexturing
float    gHighestHeight;   // if terrain is deformed for multitexturing


Texture2D gTexture[9]; // Works like a texture array.

//--------------------------------------------------------------------------------------
SamplerState gAnisoSampler
{
	Filter = ANISOTROPIC;
};

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float3 positionL : POSITION;
	float3 normalL   : NORMAL;
	float2 texCoord  : TEXCOORD;
};

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
	float4 positionH : SV_POSITION;
	float4 normalW   : NORMAL;
	float2 texCoord  : TEXCOORD0;
	float4 worldPositionDepth : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
VS_OUTPUT VS(VS_INPUT vIn)
{
	VS_OUTPUT dirPixShad;

	// take back the pos for later
	float3 position = vIn.positionL;

	// pass the position from local space to screen space.
	dirPixShad.positionH = mul(float4(vIn.positionL, 1.0f), gWorldViewProj);

	// pass the normal form local space to world space.
	dirPixShad.normalW   = mul(float4(vIn.normalL, 0.0f), gWorld);

	// just pass texture coordinates to the next stage.
	dirPixShad.texCoord  = vIn.texCoord;

	// for multi texturing.
	dirPixShad.worldPositionDepth = float4(mul(gWorld, float4(position, 1.0f)).xyz, -mul(gWorldViewProj, float4(position, 1.0f)).z);

	return dirPixShad;
}

//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT pIn) : SV_Target
{
	float4 shadingResult = float4(0.0f, 0.0f, 0.0f, 1.0f);

	// sample texture one by one
	float4 sampTex1 = gTexture1.Sample(gAnisoSampler, pIn.texCoord);
	float4 sampTex2;
	float4 sampTex3;
	float4 sampTex4;

	if(gMultiTexturing)
	{
		if(gNbTexture > 1)
		{
			if(gNbTexture == 2)
			{
				sampTex2  = gTexture2.Sample(gAnisoSampler, pIn.texCoord);
			}
			else if(gNbTexture == 3)
			{
				sampTex2  = gTexture2.Sample(gAnisoSampler, pIn.texCoord);
				sampTex3  = gTexture3.Sample(gAnisoSampler, pIn.texCoord);
			}
			else if(gNbTexture == 4)
			{
				sampTex2  = gTexture2.Sample(gAnisoSampler, pIn.texCoord);
				sampTex3  = gTexture3.Sample(gAnisoSampler, pIn.texCoord);
				sampTex4  = gTexture4.Sample(gAnisoSampler, pIn.texCoord);
			}
		}
	}

	float total = 0.0f;
	float ranges[4];  // max allowed
	float weights[4]; // max allowed 
	// create trigger variable for heights texture changes.
	if(gNbTexture != 0)
	{
		float levelRange = gHeightRange / gNbTexture;
		for(int currTex = 0; currTex < gNbTexture; currTex++)
		{
			if(currTex == (gNbTexture - 1))
			{
				ranges[currTex] = 0.0f;
			}
			else
			{
				ranges[currTex] = gHighestHeight - (levelRange * (currTex + 1));
			}
		}

		for(int currText = 0; currText < gNbTexture; currText++)
		{
			weights[currText] = clamp(1.0f - abs(pIn.worldPositionDepth.y - ranges[currText]) / levelRange, 0, 1);
			total += weights[currText]; // add weights.
		}
	}

	// calculate shade on terrain regarding to a potential light
	float shade = saturate(max(dot(pIn.normalW, gLightDir), 0.0f) + 0.1f);

	// interpolate texture concentration at the pixel for multi texturing.
	shadingResult += sampTex1;

	if(gMultiTexturing)
	{
		if(gNbTexture > 1)
		{
			// reset the default result.
			shadingResult = float4(0.0f, 0.0f, 0.0f, 1.0f);
			if(gNbTexture == 2)
			{
				shadingResult += sampTex1 * (weights[1] / total);
				shadingResult += sampTex2 * (weights[0] / total);	
			}
			else if(gNbTexture == 3)
			{
				shadingResult += sampTex1 * (weights[2] / total);
				shadingResult += sampTex2 * (weights[1] / total);
				shadingResult += sampTex3 * (weights[0] / total);
			}
			else if(gNbTexture == 4)
			{
				shadingResult += sampTex1 * (weights[3] / total);
				shadingResult += sampTex2 * (weights[2] / total);
				shadingResult += sampTex3 * (weights[1] / total);
				shadingResult += sampTex4 * (weights[0] / total);
			}
		}
	}

	// apply the shade
	shadingResult *= shade;

	// return a RGBA color for the pixel
	return shadingResult;
}

technique10 WearTerrain
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, PS()));
    }
}
