//Rain particle effect

//DEFINES
//As close to an enum you'll get for shaders at the moment! (at least shader model 4 anyway)
#define EMITTER 0
#define FLARE 1
//END DEFINES

cbuffer perFrame 
{
	float4 gEyePosW;  //Eye position in world coordinates
    float4 gEmitPosW; //Emitter position in world coordinates
    float4 gEmitDirW; //Emit direction in world coordinates
    
	float gGameTime; //Total game time, used to sample "random" texture
    float gTimeStep; //Delta time, used to integrate particles
    float gSpread; //How far each particle can stray away from the emitter

    float4x4 gViewProj;

    float4 gWindForce; //Refers to how much the wind affects the motion of the particle
	float4 gAccelerationW; //rate of change of the particles velocity
}

Texture2D gTex; //Texture to apply to the rain
Texture1D gRandomTex; //Texture full of randomized values, need a "random" function but HLSL doesnt have one so this is the alternative

//STATES
SamplerState gTriLinearSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

SamplerState gPointSampler
{
	Filter = MIN_MAG_MIP_POINT;
	AddressU = WRAP;
	AddressV = WRAP;
};

DepthStencilState DisableDepth
{
	DepthEnable = FALSE;
	DepthWriteMask = ZERO;
};

DepthStencilState NoDepthWrites
{
	DepthEnable = TRUE;
	DepthWriteMask = ZERO;
};
//END STATES

//UTILITY FUNCTIONS
//Random point offset from middle of sphere, not normalised
float3 RandomSpherePoint(float translation)
{
	//Explicitely specifiy miplevel 0
	return gSpread * gRandomTex.SampleLevel(gTriLinearSampler, gGameTime + translation, 0).xyz;;
}
//END UTILITY FUNCTIONS

//STREAM OUT SHADER
struct Particle
{
	float3 initialPositionW : POSITION;
	float3 initialVelocityW : VELOCITY;
	float2 size : SIZE;
	float age : AGE;
	uint type : TYPE;
};

Particle StreamVertexShader(Particle vIn)
{
	return vIn;
}

//Geometry shader function that creates new particles from the emitter
//Note that this is not the shader itself. In order to use the stream output
//features, you have an extra line to specify, see below!
[maxvertexcount(10)]
void GeometryStream(point Particle gIn[1], inout PointStream<Particle> particleStream)
{
	gIn[0].age += gTimeStep;

	if(gIn[0].type == EMITTER) //Only create new particles from emitters
	{
		if(gIn[0].age > 0.001f)
		{
			for(int particle = 0; particle < 9; ++particle)
			{
				float3 randomVec = 35.0f * RandomSpherePoint((float)particle/9.0f);
				randomVec.y = gEyePosW.y + 20.0f;

				//Create new particle and fill in info
				Particle p;

				p.size = float2(1.0f, 1.0f);
				p.age = 0.0f;
				p.type = FLARE;

				p.initialPositionW = gEmitPosW.xyz + randomVec;
				p.initialVelocityW = float3(0.0f, -5.0f, 0.0f);

				particleStream.Append(p);
			}
			gIn[0].age = 0.0f;
		}
		particleStream.Append(gIn[0]);
	}
	else
	{
		//Kill condition
		if(gIn[0].age <= 2.5f)
		{
            particleStream.Append(gIn[0]);
        }
	}
}

//In order to use stream out, you run with function, specifying the function above as well as the data being streamed out
GeometryShader StreamGeometryShader = ConstructGSWithSO(CompileShader(gs_4_0, GeometryStream()), "POSITION.xyz; VELOCITY.xyz; SIZE.xy; AGE.x; TYPE.x");

technique10 StreamOutTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, StreamVertexShader()));
		SetGeometryShader(StreamGeometryShader);
		SetPixelShader(NULL); //No pixel shader as nothing is drawn during the particle creation stage

		SetDepthStencilState(DisableDepth, 0);
	}
}
//END STREAM OUT SHADER

//RENDER SHADER
struct RenderVSOut
{
	float3 positionW : POSITION;
	uint type : TYPE;
};

struct RenderGSOut
{
	float4 positionH : SV_Position;
	float2 texC : TEXCOORD;
};

RenderVSOut RenderVertexShader(Particle vIn)
{
	RenderVSOut vOut;

	//Numerically integrate
	float time = vIn.age;
	float3 totalAcc = gAccelerationW.xyz + gWindForce.xyz;
	vOut.positionW = 0.5f * time * time * totalAcc + vIn.initialVelocityW * time + vIn.initialPositionW;
	
	vOut.type = vIn.type;

	return vOut;
}

//Geom shader that expands particle points into lines.
[maxvertexcount(2)]
void RenderGeometryShader(point RenderVSOut gIn[1], inout LineStream<RenderGSOut> lineStream)
{
	if(gIn[0].type != EMITTER)
	{
		//Position of 2nd point is position of first particle + where it will be in one frame (scaled)
		float3 p0 = gIn[0].positionW;
		float3 p1 = gIn[0].positionW + 0.035f * (gAccelerationW.xyz + gWindForce.xyz);

		//The two points that make the line
		RenderGSOut v0;
		v0.positionH = mul(float4(p0, 1.0f), gViewProj); //Transform them into device space 
		v0.texC = float2(0.0f, 0.0f);
		lineStream.Append(v0);

		RenderGSOut v1;
		v1.positionH = mul(float4(p1, 1.0f), gViewProj); //Transform them into device space 
		v1.texC = float2(1.0f, 1.0f);
		lineStream.Append(v1);
	}
}

float4 RenderPixelShader(RenderGSOut pIn) : SV_TARGET
{
	//just simply sample the tex
	return gTex.Sample(gPointSampler, float3(pIn.texC, 0.0f));
}

technique10 ParticleRenderTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, RenderVertexShader()));
		SetGeometryShader(CompileShader(gs_4_0, RenderGeometryShader()));
		SetPixelShader(CompileShader(ps_4_0, RenderPixelShader()));

		SetDepthStencilState(NoDepthWrites, 0);
	}
}
//END RENDER SHADER