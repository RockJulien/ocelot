//--------------------------------------------------------------------------------------
//  File: OBJMesh.fx
//   
//  Description: Shader file for passing meshes vertices through
//               the pipeline and being able to draw it with 
//               Bump mapping effect.
//
//  Author:      Larbi Julien
//  Version:     1.0
//  Creation:    19/03/2012
//
//  (c) Copyright 2012-2015 OcelotEngine All right reserved.
//--------------------------------------------------------------------------------------

#include "EffectHelper.fx"

cbuffer cbPerFrame
{
	OcelotSpotLight gLight;
	OcelotFog gFog;
	float3 gEyePosW;
};

cbuffer cbPerObject
{
	matrix gWorld;
	matrix gView;
	matrix gProjection;
	matrix gWorldInvTrans;
	float3 gMaterialAmbient;
	float3 gMaterialDiffuse;
	float3 gMaterialSpecular;
	float  gMaterialAlpha;
	int    gMaterialShininess;
};

// Non-numeric components.
Texture2D gTexture;

//--------------------------------------------------------------------------------------
SamplerState gAnisoSampler
{
	Filter = ANISOTROPIC;  // avoid fuzzy part 
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float3 Posl     : POSITION;  // local position
	float3 NormalL  : NORMAL;    // local normal
	float3 TangentL : TANGENT;   // local tangent
	float2 TexC     : TEXCOORD;  // U/V texture coordinates.
};

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
    float4 PosS     : SV_POSITION; // screen position
	float4 PosW     : POSITION;    // world position
	float3 NormalW  : NORMAL;      // world normal
	float3 TangentW : TANGENT;     // world tangent
	float2 TexC     : TEXCOORD;    // U/V texture coordinates.
	float  FogLerp  : FOG;         // the fog interpolation value.
};

//--------------------------------------------------------------------------------------
// Vertex Shader 
//--------------------------------------------------------------------------------------
VS_OUTPUT VS( VS_INPUT vIn)
{
    VS_OUTPUT output = (VS_OUTPUT)0;

	// Convert from local to world space, then to sreen space.
	output.PosS = mul( float4(vIn.Posl, 1.0f), gWorld);	
	output.PosW = output.PosS;
	
	output.PosS = mul( output.PosS, gView );
    output.PosS = mul( output.PosS, gProjection );

	// compute the world tangent
	output.TangentW = mul(float4(vIn.TangentL, 0.0f), gWorldInvTrans).xyz;

	// convert from local to world normal
	float3 wNorm = mul(float4(vIn.NormalL, 0.0f), gWorldInvTrans).xyz;
	output.NormalW = normalize(wNorm);

	// pass the vertex texture into the pixel stage :-))
	output.TexC = vIn.TexC;

	/********************************* Add Fog *****************************/
	// Compute the distance from the eye to the pixel
	float dist = distance(output.PosW, gEyePosW);

	// Then, compute the interpolation value for the Fog.
	output.FogLerp = saturate((dist - gFog.start) / gFog.range);

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( VS_OUTPUT input, uniform bool pUseSpecular, uniform bool pUseTexture ) : SV_Target
{
	float4 Color    = float4( 0.0f, 0.0f, 0.0f, 0.0f);
	float4 lAmbient = float4( gMaterialAmbient, gMaterialAlpha );
	if 
		( pUseTexture )
	{
		lAmbient = gTexture.Sample(gAnisoSampler, input.TexC);
	}

	input.NormalW = normalize(input.NormalW);

	float4 lSpecular = float4(1.0f, 1.0f, 1.0f, 1.0f); // Neutral
	if 
		( pUseSpecular )
	{
		// If OBJ file provide a specular, set it instead (it will be multiplied by the light specular)
		lSpecular = float4( gMaterialSpecular, gMaterialAlpha );
	}

	// Create the struct for current pixel.
	OcelotPixel pix = { lAmbient, lSpecular, input.PosW.xyz, input.NormalW };

	float4 tempColor = ComputePointLight(gLight, pix, gEyePosW);
	tempColor.a = gMaterialAlpha;

	float4 foggedColor = lerp(tempColor, gFog.fogColor, input.FogLerp);

	return foggedColor;
}

//--------------------------------------------------------------------------------------
// Technique(s) containing passe(s) to apply.
//--------------------------------------------------------------------------------------
technique10 NoSpecular
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, PS(false, false)));
    }
}

technique10 Specular
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS(true, false)));
	}
}

technique10 TexturedNoSpecular
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS(false, true)));
	}
}

technique10 TexturedSpecular
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS(true, true)));
	}
}