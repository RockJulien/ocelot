//--------------------------------------------------------------------------------------
//  File: TriPlanarBumpMapping.fx
//   
//  Description: Shader file for passing voxels through
//               the pipeline and being able to draw it.
//               Basic Tri-planar texturing will be applied 
//               avoiding stretching of texture on the 
//               plane perpendicular to the one on which
//               coordinates are computed (one texture per
//               plane merged in one applied to the iso-
//               surface) in association with a hacked 
//               version of Bump mapping in the voxel case.     
//
//  Author:      Larbi Julien
//  Version:     1.0
//  Creation:    27/11/2012
//
//  (c) Copyright 2012-2015 OcelotEngine All right reserved.
//--------------------------------------------------------------------------------------

#include "EffectHelper.fx"

cbuffer cbPerFrame
{
	OcelotSpotLight gLight;
	OcelotFog gFog;
	float3 gEyePosW;
	bool   gTorcheOn;
};

cbuffer cbPerObject
{
	matrix gWorld;
	matrix gView;
	matrix gProjection;
	matrix gWorldInvTrans;
};

// Non-numeric components.
Texture2D gTexture[9];

//--------------------------------------------------------------------------------------
SamplerState gAnisoSampler
{
	Filter = ANISOTROPIC;  // avoid fuzzy part (kind of supersampler).
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
struct VoxelVertexShader_INPUT
{
	float3 Posl     : POSITION;  // local position
	float3 NormalL  : NORMAL;    // local normal
};

//--------------------------------------------------------------------------------------
struct VoxelVertexShader_OUTPUT
{
    float4 PosS      : SV_POSITION; // screen position
	float4 PosW      : POSITION;    // world position
	float3 NormalW   : NORMAL;      // world normal
	float3 TexC      : TEXCOORD0;   // texture coordinate.
	float3 Tan1Light : LIGHTTANGENTSPACE1; // Light Tangent Space 1
	float3 Tan1Eye   : EYETANGENTSPACE1;   // Eye Tangent Space 1
	float2 Tan2Light : LIGHTTANGENTSPACE2; // Light Tangent Space 2
	float2 Tan2Eye   : EYETANGENTSPACE2;   // Eye Tangent Space 2
	float  FogLerp   : FOG;         // the fog interpolation value.
};

//--------------------------------------------------------------------------------------
// Vertex Shader 
//--------------------------------------------------------------------------------------
VoxelVertexShader_OUTPUT VS( VoxelVertexShader_INPUT vIn)
{
    VoxelVertexShader_OUTPUT output = (VoxelVertexShader_OUTPUT)0;

	// Convert from local to world space, then to sreen space.
	output.PosS = mul( float4(vIn.Posl, 1.0f), gWorld);
	output.PosW = output.PosS;
	output.PosS = mul( output.PosS, gView );
	output.PosS = mul( output.PosS, gProjection );

	// Texture coordinate for the vertex.
	output.TexC = vIn.Posl / 1.0f;

	// Convert from local to world normal
	float3 wNorm = mul(float4(vIn.NormalL, 0.0f), gWorldInvTrans).xyz;
	output.NormalW = normalize(wNorm);

	// Compute 2 tangent space vecs dir
	float3 toLight = normalize(gLight.position - output.PosW.xyz); // from object to light.
	float3 toEye   = normalize(gEyePosW - output.PosW.xyz);        // from object to camera.

	// Compute the normal magnitude.
	float3 normalMagnitude = output.NormalW * output.NormalW;
	normalMagnitude.xy = normalMagnitude.x + normalMagnitude.yz;
	normalMagnitude.xy = max(normalMagnitude.xy, 0.03125f);

	// Compute the cross product between each vector dir and the normal
	float3 VCrossNLight = cross(toLight, output.NormalW);
	float3 VCrossNEye   = cross(toEye, output.NormalW);

	// Then, apply transform for each vector dir in the two tangent space.
	// NOTE: N dot V will be the same for both tangent space, thus compute
	// once.
	float4 tempLight;
	tempLight.xy = output.NormalW.xz * toLight.yx - output.NormalW.yx * toLight.xz;
	tempLight.zw = output.NormalW.xz * VCrossNLight.yx - output.NormalW.yx * VCrossNLight.xz;
	float4 tempEye;
	tempEye.xy   = output.NormalW.xz * toEye.yx - output.NormalW.yx * toEye.xz;
	tempEye.zw   = output.NormalW.xz * VCrossNEye.yx - output.NormalW.yx * VCrossNEye.xz;

	// tangents needed for diffuse component.
	output.Tan1Light.xy = tempLight.xz * rsqrt(normalMagnitude.x);
	output.Tan1Light.z  = dot(output.NormalW, toLight); // same for both.
	output.Tan2Light    = tempLight.yw * rsqrt(normalMagnitude.y);
	// tangents needed for specular component.
	output.Tan1Eye.xy   = tempEye.xz * rsqrt(normalMagnitude.x);
	output.Tan1Eye.z    = dot(output.NormalW, toEye);   // same for both.
	output.Tan2Eye      = tempEye.yw * rsqrt(normalMagnitude.y);

	/********************************* Add Fog *****************************/
	// Compute the distance from the eye to the pixel
	float dist = distance(output.PosW, gEyePosW);

	// Then, compute the interpolation value for the Fog.
	output.FogLerp = saturate((dist - gFog.start) / gFog.range);

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( VoxelVertexShader_OUTPUT input ) : SV_Target
{
	float4 Color  = float4( 0.0f, 0.0f, 0.0f, 0.0f );

	// Determine the blend weights for the 3 planar projections.
    // N_orig is the vertex-interpolated normal vector.
    float3 blendWeights = abs( input.NormalW );   // Tighten up the blending zone:
    blendWeights  = ( blendWeights - 0.2f ) * 7;
    blendWeights  = max( blendWeights, 0.1f );    // Force weights to sum to 1.0 (very important!)
    blendWeights /= ( blendWeights.x + blendWeights.y + blendWeights.z );
    // Now determine a color value and bump vector for each of the 3
    // projections, blend them, and store blended results in these two
    // vectors:
    float4 blendedColor; // .w hold spec value
	float3 triBump;
	float3 triBump2;
	float4 triSpec;
	float4 triSpec2;
    float texScale = 1.0f / 16.0f;
    {
        // Compute the UV coords for each of the 3 planar projections.
        // texScale (default ~ 1.0) determines how big the textures appear.
        float2 coord1 = input.TexC.yz * texScale;
        float2 coord2 = input.TexC.zx * texScale;
        float2 coord3 = input.TexC.xy * texScale;
        
		// Triplanar sample coords
		//float2 coord1 = float2(input.TexC.z, -input.TexC.y) * texScale; // ZY: Left and Right
		//float2 coord2 = float2(input.TexC.x, -input.TexC.z) * texScale; // XZ: Top and Bottom
		//float2 coord3 = float2(input.TexC.x, -input.TexC.y) * texScale; // XY: Front and Back

		// Compute a vector that holds one for text coord that should
		// be negated (will contain either 0 or 1).
		float3 flipNeeded = float3((input.NormalW.x > 0.0f), (input.NormalW.y <= 0.0f), (input.NormalW.z > 0.0f));

		// Instead of mirroring, wrap the textures around the Y axis (Front, Back, Left, Right)
		// and the Z axis (Back and Front)
		//float3 flipNeeded = sign(input.NormalW);
		if(flipNeeded.x)
			coord1.x *= -flipNeeded.x;
		if(flipNeeded.y)
			coord2.x *= -flipNeeded.y;
		if(flipNeeded.z)
			coord3.x *= -flipNeeded.z;

		// Now, compute diffuse components thanks to per-vertex two tangent space components of lengyel.
		float3 lightTan1 = normalize(input.Tan1Light);
		float3 lightTan2 = normalize(float3(input.Tan2Light.x, input.Tan2Light.y, input.Tan1Light.z));

		/*
		// Calculate tangents for both tangent spaces
		// - Tangent1: wrapped around Y (Valid for XY and ZY projected textures)
		// - Tangent2: wrapped around Z (Valid for XZ projected textures)
		float3 tangent1 = float3(-input.NormalW.z, 0.0f, input.NormalW.x);
		float3 tangent2 = float3(input.NormalW.y, -input.NormalW.x, 0.0f);
		
		// Build TBN matrices for both tangent spaces
		float3x3 TBN1, TBN2;
		TBN1[0] = normalize(lightTan1 - dot(lightTan1, input.NormalW) * input.NormalW); // tangent1;
		TBN1[1] = cross(TBN1[0], input.NormalW);
		TBN1[2] = input.NormalW;
		TBN2[0] = normalize(lightTan2 - dot(lightTan2, input.NormalW) * input.NormalW);//tangent2;
		TBN2[1] = cross(TBN2[0], input.NormalW);
		TBN2[2] = input.NormalW;
		*/

        // Sample color maps for each projection, at those UV coords.
		// Put your texture in the order you want they appear
        float4 col1 = gTexture[6].Sample(gAnisoSampler, coord1); // PosX face
        float4 col2 = gTexture[3].Sample(gAnisoSampler, coord2); // PosY face
        float4 col3 = gTexture[6].Sample(gAnisoSampler, coord3); // PosZ face
        float4 col4 = gTexture[6].Sample(gAnisoSampler, coord1); // NegX face
        float4 col5 = gTexture[3].Sample(gAnisoSampler, coord2); // NegY face
        float4 col6 = gTexture[6].Sample(gAnisoSampler, coord3); // NegZ face

        //col1 *= ( input.NormalW.y + 1.0f ) / 2.0f;
        //col2 *= ( input.NormalW.y + 1.0f ) / 2.0f;
        //col3 *= ( input.NormalW.y + 1.0f ) / 2.0f;

        //col4 *= ( -input.NormalW.y + 1.0f ) / 2.0f;
        //col5 *= ( -input.NormalW.y + 1.0f ) / 2.0f;
        //col6 *= ( -input.NormalW.y + 1.0f ) / 2.0f;

        //col1 += col4;
        //col2 += col5;
        //col3 += col6;
		float4 posCol = col1 * blendWeights.x +
                        col2 * blendWeights.y +
                        col3 * blendWeights.z;

		float4 negCol = col4 * blendWeights.x +
                        col5 * blendWeights.y +
                        col6 * blendWeights.z;

         // Finally, blend the results of the 3 planar projections.
        blendedColor = posCol + negCol;

		// === Normal maps ===
		float3 nor1 = gTexture[7].Sample(gAnisoSampler, coord1).rgb * 2.0f - 1.0f; // according normal map of texture 6 Rock
		float3 nor2 = gTexture[4].Sample(gAnisoSampler, coord2).rgb * 2.0f - 1.0f; // according normal map of texture 3 Grass
		float3 nor3 = gTexture[7].Sample(gAnisoSampler, coord3).rgb * 2.0f - 1.0f; // according normal map of texture 0 Dirt // Changed with rocky one, better.

		// Compute the blend to add between the normal computed and the light dir in tan space
		// for having the diffuse component.
		triBump.x = saturate(dot(nor1, lightTan1));
		triBump.y = saturate(dot(nor2, lightTan2));
		triBump.z = saturate(dot(nor3, lightTan1));
		triBump *= blendWeights;

		/*
		// Transform normals into world space
		float3 bumpNormal1 = normalize(mul(nor1, TBN1));
		float3 bumpNormal2 = normalize(mul(nor2, TBN2));
		float3 bumpNormal3 = normalize(mul(nor3, TBN1));
		
		// Blend together
		triBump = bumpNormal1 * blendWeights.x +           
				  bumpNormal2 * blendWeights.y +           
				  bumpNormal3 * blendWeights.z;
		
		// Normalize the bump.	   
		triBump = normalize(triBump);
		*/

		// === Specular maps ===
		float3 spe1 = gTexture[8].Sample(gAnisoSampler, coord1).rgb; // according spec map of texture 6 Rock
		float3 spe2 = gTexture[5].Sample(gAnisoSampler, coord2).rgb; // according spec map of texture 3 Grass
		float3 spe3 = gTexture[8].Sample(gAnisoSampler, coord3).rgb; // according spec map of texture 0 Dirt // Changed with rocky one, better.

		float3 eyeTan1   = normalize(lightTan1 + normalize(input.Tan1Eye));
		float3 eyeTan2   = normalize(lightTan2 + normalize(float3(input.Tan2Eye.x, input.Tan2Eye.y, input.Tan1Eye.z)));

		// Compute the blend to add between the normal computed and the eye dir in tan space
		// for having the specular component.
		triSpec.x = saturate(dot(spe1, eyeTan1));
		triSpec.y = saturate(dot(spe2, eyeTan2));
		triSpec.z = saturate(dot(spe3, eyeTan1));
		triSpec.rgb *= blendWeights;
		/*
		// Blend together
		triSpec = spe1 * blendWeights.x +           
				  spe2 * blendWeights.y +           
				  spe3 * blendWeights.z;
		*/

		// For the second light which is a spot light with light position equal to eyePos
		// compute new eye tangents not involving the global light and use them as tangents
		// for computing the bump normal for the torch light.
		float3 torchLightTan1 = normalize(input.Tan1Eye);
		float3 torchLightTan2 = normalize(float3(input.Tan2Eye.x, input.Tan2Eye.y, input.Tan1Eye.z));

		float3 torchEyeTan1   = normalize(torchLightTan1 + normalize(input.Tan1Eye));
		float3 torchEyeTan2   = normalize(torchLightTan2 + normalize(float3(input.Tan2Eye.x, input.Tan2Eye.y, input.Tan1Eye.z)));

		triBump2.x = saturate(dot(nor1, torchLightTan1));
		triBump2.y = saturate(dot(nor2, torchLightTan2));
		triBump2.z = saturate(dot(nor3, torchLightTan1));
		triBump2 *= blendWeights;

		triSpec2.x = saturate(dot(spe1, torchEyeTan1));
		triSpec2.y = saturate(dot(spe2, torchEyeTan2));
		triSpec2.z = saturate(dot(spe3, torchEyeTan1));
		triSpec2.rgb *= blendWeights;
    }

	// Create the struct for current pixel.
	OcelotPixel pix  = { blendedColor, triSpec, input.PosW.xyz, triBump};

	// Another one for the spot light response.
	OcelotPixel pix2 = { blendedColor, triSpec2, input.PosW.xyz, triBump2};

    //float angle = saturate(dot( triBump, gLight.direction ));
    float4 tempColor  = saturate(ComputePointLight(gLight, pix, gEyePosW));//blendedColor * ( gLight.diffuse * angle + gLight.ambient );

	float4 tempColor2 = tempColor;
	
	/*if(gTorcheOn)
	{
		tempColor2 = max(saturate(ComputeSpotLight(gTorchLight, pix2, gEyePosW)), tempColor);
	}*/

	// Add fog in last
	Color = lerp( tempColor2, gFog.fogColor, input.FogLerp );

	return Color;
}

//--------------------------------------------------------------------------------------
// Technique containing passe(s) to apply.
//--------------------------------------------------------------------------------------
technique10 TriPlanarTexturing
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, PS()));
    }
}
