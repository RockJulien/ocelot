//=============================================================================
// Overlay.fx by Frank Luna (C) 2008 All Rights Reserved.
//
// Effect used to simply draw a texture in overlay at a screen position
//=============================================================================
 
cbuffer cbPerObject
{
	matrix gWorldViewProj;
};

// Non-numeric components.
Texture2D gTexture;

//--------------------------------------------------------------------------------------
SamplerState gLinearSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
	float2 TexC    : TEXCOORD; // U/V texture coordinates.
};

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
	float4 PosS : SV_POSITION;
	float2 TexC : TEXCOORD;    // U/V texture coordinates.
};
 
//--------------------------------------------------------------------------------------
// Vertex Shader 
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(VS_INPUT vIn)
{
	VS_OUTPUT vOut;

	vOut.PosS = mul( float4(vIn.PosL, 1.0f), gWorldViewProj );
	vOut.TexC = vIn.TexC;
	
	return vOut;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( VS_OUTPUT pIn ) : SV_Target
{
	float4 lTexColor = gTexture.Sample(gLinearSampler, pIn.TexC);

	// discard pixel if texture alpha < 0.1.  note that we do this
	// test as soon as possible so that we can potentially exit the shader 
	// early, thereby skipping the rest of the shader code.
	clip( lTexColor.a - 0.1f);

	return lTexColor;
}

//--------------------------------------------------------------------------------------
// Technique containing passe(s) to apply.
//--------------------------------------------------------------------------------------
technique10 Overlay
{
    pass P0
    {
		SetVertexShader( CompileShader(vs_4_0, VS()) );
        SetGeometryShader( NULL );
		SetPixelShader( CompileShader(ps_4_0, PS()) );
    }
}
