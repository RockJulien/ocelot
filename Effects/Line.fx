//--------------------------------------------------------------------------------------
//  File: Line.fx
//   
//  Description: Shader file for passing line vertices through
//               the pipeline and being able to draw it.
//
//  Author:      Larbi Julien
//  Version:     1.0
//  Creation:    22/02/2012
//
//  (c) Copyright 2012-2015 OcelotEngine All right reserved.
//--------------------------------------------------------------------------------------

float4x4 gWorld;
float4x4 gView;
float4x4 gProj;

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float3 PosL     : POSITION;
	float4 Color    : COLOR;
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
    float4 PosS      : SV_POSITION;
	float4 Color     : COLOR0;
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(VS_INPUT vIn)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	float4 tempPos = float4(vIn.PosL, 1.0f);
	output.PosS  = mul(tempPos, gWorld);
	output.PosS  = mul(output.PosS, gView);
	output.PosS  = mul(output.PosS, gProj);

	output.Color = vIn.Color;

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT pIn) : SV_Target
{
	return pIn.Color;
}

//--------------------------------------------------------------------------------------
technique10 RenderLine
{
	pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, VS()));
        SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, PS()));
    }
}