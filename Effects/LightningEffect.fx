//Lightning bolt effect

matrix gWVP; //Matrix to transform vertices into device space
float4 gEyePosW; //Position of the eye to use in billboarding
Texture2D gTexture; //Texture of the bolt

cbuffer cbFixed
{  
	//Index into this when texturing the quad
	float2 gQuadTexC[4] = 
	{
        float2(0.0f, 1.0f),
		float2(1.0f, 1.0f),
		float2(0.0f, 0.0f),
		float2(1.0f, 0.0f)
	};
};

//Sampler states
SamplerState gLinearSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

//Depth states
DepthStencilState NoDepthWrites
{
    DepthEnable = TRUE;
    DepthWriteMask = ZERO;
};

//Blend states
BlendState AdditiveBlending
{
    AlphaToCoverageEnable = FALSE;
    BlendEnable[0] = TRUE;
    SrcBlend = SRC_ALPHA;
    DestBlend = ONE;
    BlendOp = ADD;
    SrcBlendAlpha = ZERO;
    DestBlendAlpha = ZERO;
    BlendOpAlpha = ADD;
    RenderTargetWriteMask[0] = 0x0F;
};

struct VS_INPUT
{
	float3 PosW : POSITION;
	float4 Color : COLOR0;
};

//Vertex shader just simply outputs the vertex
VS_INPUT VS(VS_INPUT vIn)
{
	return vIn;
}

struct GS_OUTPUT
{
	float4 PosH : SV_Position;
	float4 Color : COLOR0;
	float2 TexC : TEXCOORD;
};

//Geometry shader expands line into quad and billboard aligns the quad
[maxvertexcount(4)]
void GS(line VS_INPUT gIn[2], inout TriangleStream<GS_OUTPUT> triStream)
{
	float3 diff = gIn[1].PosW - gIn[0].PosW;

	//Create billboard matrix
	float3 look = normalize(gEyePosW.xyz - gIn[0].PosW);
	//In the right vector calculation, local "up" is replaced by a vector perpendicular to the relative
	//positions between the cam and line and the relative positions between the two lines. This nifty trick
	//stops the quads from being world-axis aligned by default
	float3 right = normalize(cross(cross(look, diff), look)); 
	float3 up = cross(look, right);

	float3 halfHeight = 0.5f * diff;

	float4x4 world;
	world[0] = float4(right, 0.0f);
	world[1] = float4(up, 0.0f);
	world[2] = float4(look, 0.0f);
	world[3] = float4(gIn[0].PosW + halfHeight, 1.0f); //offset by half height

	float4x4 WorldViewProj = mul(world, gWVP);
		
	//Expand into quad
	float quadWidth  = 0.5f * length(diff);
	float quadHeight = 1.5f * 1.0f;
	float4 v[4];
	v[0] = float4(-quadWidth, -quadHeight, 0.0f, 1.0f);
	v[1] = float4(+quadWidth, -quadHeight, 0.0f, 1.0f);
	v[2] = float4(-quadWidth, +quadHeight, 0.0f, 1.0f);
	v[3] = float4(+quadWidth, +quadHeight, 0.0f, 1.0f);

	GS_OUTPUT gOut;
	[unroll]
	for(int i = 0; i < 4; ++i)
	{
		gOut.PosH = mul(v[i], WorldViewProj); //Transform into device space
		gOut.TexC = gQuadTexC[i];
		gOut.Color = gIn[0].Color;
		triStream.Append(gOut);
	}
}

float4 PS(GS_OUTPUT pIn) : SV_Target
{
	float4 col = gTexture.Sample(gLinearSampler, float3(pIn.TexC, 0.0f)) * pIn.Color; //sample text and modulate with colour
	clip(col.a - 0.15f); //Allow transparency
	return col;
}

technique10 RenderLine
{
	pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, VS()));
        SetGeometryShader(CompileShader(gs_4_0, GS()));
        SetPixelShader(CompileShader(ps_4_0, PS()));

		SetBlendState(AdditiveBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetDepthStencilState(NoDepthWrites, 0);
    }
}