//Fire effect

//As close to an enum you'll get for shaders at the moment! (at least shader model 4 anyway)
#define EMITTER 0
#define FLARE 1

cbuffer perFrame
{
	float4 gEyePosW; //Eye position in world coordinates
	float4 gEmitPosW;//Emitter position in world coordinates
	float4 gEmitDirW;//Emit direction in world coordinates

	float gGameTime; //Total game time, used to sample "random" texture
	float gTimeStep; //Delta time, used to integrate particles
	float gSpread; //How far each particle can stray away from the emitter

	float4x4 gViewProj; 

	float4 gWindForce; //Refers to how much the wind affects the motion of the particle
	float4 gAccelerationW; //rate of change of the particles velocity
}

cbuffer cbFixed
{  
	//Used so we can texture the quad in a loop
	float2 gQuadTexC[4] = 
	{
        float2(0.0f, 1.0f),
		float2(1.0f, 1.0f),
		float2(0.0f, 0.0f),
		float2(1.0f, 0.0f)
	};
};

Texture2D gTex; //Texture to apply to the quads
Texture1D gRandomTex; //Texture full of randomized values, need a "random" function but HLSL doesnt have one so this is the alternative

//Sampler states
SamplerState gTriLinearSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

//Depth stencil states
DepthStencilState DisableDepth
{
    DepthEnable = FALSE;
    DepthWriteMask = ZERO;
};

DepthStencilState NoDepthWrites
{
    DepthEnable = TRUE;
    DepthWriteMask = ZERO;
};

//Blend states
BlendState AdditiveBlending
{
    AlphaToCoverageEnable = FALSE;
    BlendEnable[0] = TRUE;
    SrcBlend = SRC_ALPHA;
    DestBlend = ONE;
    BlendOp = ADD;
    SrcBlendAlpha = ZERO;
    DestBlendAlpha = ZERO;
    BlendOpAlpha = ADD;
    RenderTargetWriteMask[0] = 0x0F;
};

//Returns a random normalised vector. Total game time + offset used to sample the random tex
float3 RandomNormVec(float translation)
{
	float totalOffset = (gGameTime + translation);
	float3 randVec = gRandomTex.SampleLevel(gTriLinearSampler, totalOffset, 0).xyz;
	return normalize(randVec);
}

struct Particle
{
	float3 initialPosW : POSITION;
	float3 initialVelW : VELOCITY;
	float2 sizeW : SIZE;
	float age : AGE;
	uint type : TYPE;
};

Particle StreamOutVertexShader(Particle vIn)
{
	return vIn;
}

//Geometry shader function that creates new particles from the emitter
//Note that this is not the shader itself. In order to use the stream output
//features, you have an extra line to specify, see below!
[maxvertexcount(2)]
void StreamOut(point Particle gIn[1], inout PointStream<Particle> pStream)
{
	gIn[0].age += gTimeStep;
	
	if(gIn[0].type == EMITTER) //Only create new particles from emitters
	{	
		if(gIn[0].age > 0.005f)
		{
			float3 vRandom = RandomNormVec(0.0f);
			vRandom.x *= 0.5f;
			vRandom.z *= 0.5f;
			
			//Create new particle and fill in info
			Particle p;
			p.age = 0.0f;
			p.type = FLARE;
			p.initialVelW = 4.0f*vRandom;
			p.initialPosW = gEmitPosW.xyz;
			p.sizeW = float2(3.0f, 3.0f);
			
			pStream.Append(p);
			
			gIn[0].age = 0.0f; 
		}
		pStream.Append(gIn[0]);
	}
	else
	{
		if(gIn[0].age <= 1.0f ) //Kill condition, each flame particle lasts for 1 second
			pStream.Append(gIn[0]);
	}
}

//In order to use stream out, you run with function, specifying the function above as well as the data being streamed out
GeometryShader StreamOutGeometryShader = ConstructGSWithSO(CompileShader(gs_4_0, StreamOut()), "POSITION.xyz; VELOCITY.xyz; SIZE.xy; AGE.x; TYPE.x");

technique10 StreamOutTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, StreamOutVertexShader()));
		SetGeometryShader(StreamOutGeometryShader);
		SetPixelShader(NULL); //No pixel shader as nothing is drawn during the particle creation stage

		SetDepthStencilState(DisableDepth, 0);
	}
}

//The vertex structure of particles to be rendered
struct VS_OUT
{
	float3 posW : POSITION;
	float2 sizeW : SIZE;
	float4 color : COLOR;
	uint type : TYPE;
};

VS_OUT RenderVertexShader(Particle vIn)
{
	VS_OUT vOut;
	
	//Numerically integrate
	float time = vIn.age;
	vOut.posW = 0.5f*time*time*(gAccelerationW.xyz + gWindForce.xyz) + time*vIn.initialVelW + vIn.initialPosW;
	
	float opacity = 1.0f - smoothstep(0.0f, 1.0f, time/1.0f); //Goes from solid to transparent over time
	vOut.color = float4(1.0f, 1.0f, 1.0f, opacity);
	
	vOut.sizeW = vIn.sizeW;
	vOut.type = vIn.type;
	
	return vOut;
}

struct GS_OUT
{
	float4 posH : SV_Position;
	float4 color : COLOR;
	float2 texC : TEXCOORD;
};

//Geometry shader that expands the particle points into quads and then textures them
[maxvertexcount(4)]
void RenderGeometryShader(point VS_OUT gIn[1], inout TriangleStream<GS_OUT> stream)
{
	if(gIn[0].type != EMITTER)
	{
		//Construct quad
		float4 v[4];
		float halfWidth  = 0.5f * gIn[0].sizeW.x;
		float halfHeight = 0.5f * gIn[0].sizeW.y;
		v[0] = float4(-halfWidth, -halfHeight, 0.0f, 1.0f);
		v[1] = float4(+halfWidth, -halfHeight, 0.0f, 1.0f);
		v[2] = float4(-halfWidth, +halfHeight, 0.0f, 1.0f);
		v[3] = float4(+halfWidth, +halfHeight, 0.0f, 1.0f);
		
		//Create coordinate frame for billboarding
		float3 look = normalize(gEyePosW.xyz - gIn[0].posW);
		float3 right = normalize(cross(float3(0.0f,1.0f,0.0f), look));
		float3 up = cross(look, right);
		
		float4x4 world;
		world[0] = float4(right, 0.0f);
		world[1] = float4(up, 0.0f);
		world[2] = float4(look, 0.0f);
		world[3] = float4(gIn[0].posW, 1.0f);

		float4x4 WorldViewProj = mul(world, gViewProj);

		GS_OUT gOut;
		[unroll]
		for(int i = 0; i < 4; ++i)
		{
			gOut.posH = mul(v[i], WorldViewProj); //Place into device coordinates
			gOut.texC = gQuadTexC[i]; //Sample inorder to get tex coords 
			gOut.color = gIn[0].color; 
			stream.Append(gOut); 
		}	
	}
}

float4 RenderPixelShader(GS_OUT pIn) : SV_TARGET
{
	return gTex.Sample(gTriLinearSampler, float3(pIn.texC, 0.0f)) * pIn.color; //just simply sample the tex and modulate with colour
}

technique10 ParticleRenderTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, RenderVertexShader()));
		SetGeometryShader(CompileShader(gs_4_0, RenderGeometryShader()));
		SetPixelShader(CompileShader(ps_4_0, RenderPixelShader()));

		SetBlendState(AdditiveBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetDepthStencilState(NoDepthWrites, 0);
	}
}