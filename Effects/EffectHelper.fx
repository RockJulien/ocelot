//--------------------------------------------------------------------------------------
//  File: EffectHelper.fx
//   
//  Description: Shader file for providing helper structures
//               and helper functions which could be used in
//               a shading and texturing process.
//
//  Author:      Larbi Julien
//  Version:     1.0
//  Creation:    04/10/2012
//
//  (c) Copyright 2012-2015 OcelotEngine All right reserved.
//--------------------------------------------------------------------------------------

// extern to a shader process.
struct OcelotParallelLight
{
	// put bigger elements before in the C++ code
	// for matching this struct.
	// It will prevent padding issues.
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 direction;
	float  specPower;
};

struct OcelotPointLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 position;
	float3 attenuation;
	float  range;
	float  specPower;
};

struct OcelotSpotLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 position;
	float3 direction;
	float3 attenuation;
	float  range;
	float  spotPower;
	float  specPower;
};

struct OcelotFog
{
	float4 fogColor;
	float  start;
	float  range;
};

// based on the Franck Luna code.
// intern to a shader process.
struct OcelotPixel
{
	float4 diffuse;   // diffuse value for the pixel.
	float4 specular;  // specular value for the pixel.
	float3 positionS; // world position.
	float3 normalW;   // normal (or Bumped Normal if any) in world space for the pixel.
};

float4 ComputeParallelLight(OcelotSpotLight light, OcelotPixel pix, float3 vEye)
{
	//----------------------compute Color----------------------------
	float4 Color  = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Determine the diffuse light intensity that strikes the vertex.
	float diffuseIntensity = max(dot(-light.direction, pix.normalW), 0.0f);

	// Compute the ambient term which will be the default pix color.
	float3 ambient = (pix.diffuse * light.ambient).rgb;		

	// if we are behind, no calculation
	if(diffuseIntensity > 0.0f)
	{
		// Compute the reflection vector.
		float3 vReflection = reflect(light.direction, pix.normalW);   // compute the emissive light regarding to the incident one

		// Compute the eye vector direction.
		float3 toEye = normalize(vEye - pix.positionS);

		// Determine how much (if any) specular light intensity
		// into the eye.
		float specularIntensity = pow(max(dot(vReflection, toEye), 0.0f), light.specPower);

		// Compute the diffuse and specular terms separately.							//****************************
		float3 spec    = specularIntensity * (pix.specular * light.specular).rgb;       //   Fong Shading (Bump maping optional)
		float3 diffuse = diffuseIntensity * (pix.diffuse * light.diffuse).rgb;			//****************************
	
		// Sum all the terms together and copy over the diffuse alpha.
		Color.rgb = saturate(ambient + diffuse + spec);
	}						   
	else
	{
		// Default Color if behind.
		Color.rgb = saturate(ambient);
	}

	// Set the alpha value.
	Color.a   = 1.0f;
	
    return Color;
}

float4 ComputePointLight(OcelotSpotLight light, OcelotPixel pix, float3 vEye)
{
	//----------------------compute Color----------------------------
	float4 Color  = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Compute the direction from pixel to light.
	float3 pixToLight = light.position - pix.positionS;

	// Compute the distance from pixel to light.
	float dist = length(pixToLight);

	// If this distance is greater than the range of the light, stop light.
	if(dist > light.range)
		return Color;

	// Normalize the reflection.
	pixToLight /= dist;

	// Determine the diffuse light intensity that strikes the vertex.
	float diffuseIntensity = max(dot(pixToLight, pix.normalW), 0.0f);

	// Compute the ambient term which will be the default pix color.
	float3 ambient = (pix.diffuse * light.ambient).rgb;

	// if we are behind, no calculation
	if(diffuseIntensity > 0.0f)
	{
		// Compute the reflection vector.
		float3 vReflection = reflect(-pixToLight, pix.normalW);   // compute the emissive light regarding to the incident one

		// Compute the eye vector direction.
		float3 toEye = normalize(vEye - pix.positionS);

		// Determine how much (if any) specular light intensity
		// into the eye.
		float specularIntensity = pow(max(dot(vReflection, toEye), 0.0f), light.specPower);

		// Compute the diffuse and specular terms separately.							//****************************
		float3 spec    = specularIntensity * (pix.specular * light.specular).rgb;       //   Fong Shading (Bump maping optional)
		float3 diffuse = diffuseIntensity * (pix.diffuse * light.diffuse).rgb;			//****************************
	
		// Sum all the terms together and copy over the diffuse alpha.
		Color.rgb = saturate(ambient + diffuse + spec);
	}						   
	else
	{
		// Default Color if behind.
		Color.rgb = saturate(ambient);
	}

	// Attenuate the light with the attenuation vector.
	Color.rgb = Color.rgb / dot(light.attenuation, float3(1.0f, dist, dist * dist));

	// Set the alpha value.
	Color.a   = 1.0f;

	return Color;
}

float4 ComputeSpotLight(OcelotSpotLight light, OcelotPixel pix, float3 vEye)
{
	//----------------------compute Color----------------------------
	// First proceed exactly like for a point light.
	float4 Color = ComputePointLight(light, pix, vEye);

	// Then, particularities of a spot light.
	float3 pixToLight = normalize(light.position - pix.positionS);

	// Compute the spot light factor.
	float spotFactor = pow(max(dot(-pixToLight, light.direction), 0.0f), light.spotPower);

	// Finally, apply the spot factor to the color.
	Color.rgb *= spotFactor;

	// Set the alpha value.
	Color.a = 1.0f;

	return Color;
}
