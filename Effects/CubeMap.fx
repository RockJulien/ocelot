//--------------------------------------------------------------------------------------
//  File: CubeMap.fx
//   
//  Description: Shader file for passing cubeMap vertices through
//               the pipeline and being able to draw it.
//
//  Author:      Larbi Julien
//  Version:     1.0
//  Creation:    27/02/2012
//
//  (c) Copyright 2012-2015 OcelotEngine All right reserved.
//--------------------------------------------------------------------------------------

float4x4 gWorld;
float4x4 gView;
float4x4 gProj;
 
TextureCube gCubeMap;

//--------------------------------------------------------------------------------------
SamplerState gAnisoSampler
{
	Filter = ANISOTROPIC;
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
struct VS_IN
{
	float3 posL : POSITION;
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
struct VS_OUT
{
	float4 posH : SV_POSITION;
    float3 texC : TEXCOORD;
};
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Vertex Shader 
//-------------------------------------------------------------------------------------- 
VS_OUT VS(VS_IN vIn)
{
	VS_OUT vOut;
	
	float4 tempPos = float4(vIn.posL, 1.0f);
	vOut.posH = mul(tempPos, gWorld);
	vOut.posH = mul(vOut.posH, gView);
	// set z = W for insuring that the backgroud stay at the far plane.
	vOut.posH = mul(vOut.posH, gProj).xyww;
	
	// texture the cubeMap with local position
	vOut.texC = vIn.posL;
	
	return vOut;
}

//--------------------------------------------------------------------------------------
// Pixel Shader 
//--------------------------------------------------------------------------------------
float4 PS(VS_OUT pIn) : SV_Target
{
	return gCubeMap.Sample(gAnisoSampler, pIn.texC);
}

RasterizerState DisableCull
{
    CullMode = None;
};

DepthStencilState LessEqual
{
	// Make sure the depth function is LESS_EQUAL and not just LESS.  
	// Otherwise, the normalized depth values at z = 1 (NDC) will 
	// fail the depth test if the depth buffer was cleared to 1.
    DepthFunc = LESS_EQUAL;
};

//--------------------------------------------------------------------------------------
technique10 Background
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, VS()));
        SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, PS()));
        SetRasterizerState(DisableCull);
        SetDepthStencilState(LessEqual, 0);
    }
}