#include "OBB.h"

using namespace Ocelot;

//Constructor
OBB3D::OBB3D()
	: centre(0.0f, 0.0f, 0.0f), extents(0.0f, 0.0f, 0.0f)
{
	localAxis[0] = Vector3(0.0f, 0.0f, 0.0f);
	localAxis[1] = Vector3(0.0f, 0.0f, 0.0f);
	localAxis[2] = Vector3(0.0f, 0.0f, 0.0f);
}

//Constructor 
OBB3D::OBB3D(const Vector3 &_centre, const Vector3 &uX, const Vector3 &uY, const Vector3 &uZ, const Vector3 &_extents)
	: centre(_centre), extents(_extents)
{
	localAxis[0] = uX;
	localAxis[1] = uY;
	localAxis[2] = uZ;
}

//Intersection test
BBOOL OBB3D::Intersects(const IOBB3D *b) const
{
	//Not yet implemented
	return false;
}