#ifndef SPHERESWEPTVOLUMES_H
#define SPHERESWEPTVOLUMES_H

#include "ICapsule.h"

namespace Ocelot
{
	/*
	 *	A capsule shaped bounding volume. Useful for a cheap bounding representation
	 *	of humans. It is represented by a start and end point and a radius to 
	 *	represent the size of the sphere swept from the start point to the end
	 *	point to make the capsule
	 */
	class Capsule : public ICapsule
	{
	public:
		//Constructors
		Capsule();
		Capsule(const Vector3 &_start, const Vector3 &_end, const FFLOAT _radius);
		Capsule(const FFLOAT startX, const FFLOAT startY, const FFLOAT startZ, const FFLOAT endX, const FFLOAT endY, const FFLOAT endZ, const FFLOAT _radius);

		//Intersection tests
		virtual BBOOL Intersects(const BoundingSphere &b) const;
		virtual BBOOL Intersects(const ICapsule &b) const;

		//Setters/getters for variables
		virtual inline Vector3 GetStart() const { return startPoint; }
		virtual inline VVOID SetStart(const Vector3 &_start) { startPoint = _start; }

		virtual inline Vector3 GetEnd() const { return endPoint; }
		virtual inline VVOID SetEnd(const Vector3 &_end) { endPoint = _end; }

		virtual inline FFLOAT GetRadius() const { return sphereRadius; }
		virtual inline VVOID SetRadius(const FFLOAT _radius) { sphereRadius = _radius; }

	private:
		Vector3 startPoint;
		Vector3 endPoint;
		FFLOAT sphereRadius; //the size of the sphere swept from the start point to the end point to make the capsule
	};
}

#endif