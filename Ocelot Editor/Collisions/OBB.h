#ifndef OBB_H
#define OBB_H

#include "IOBB3D.h"

namespace Ocelot
{
	/*
	 *	A 3D oriented bounding box. unlike an aabb, the obb rotates with
	 *	the object it surrounds. The intersection test is more expensive but
	 *	the tighter fit leads to fewer false positives. It is represented by
	 *	a centre point and 3 extent values along each axis stemming from the centre
	 */
	class OBB3D : public IOBB3D
	{
	public:
		//Constructors
		OBB3D();
		OBB3D(const Vector3 &centre, const Vector3 &uX, const Vector3 &uY, const Vector3 &uZ, const Vector3 &_extents);

		//Intersection test
		virtual BBOOL Intersects(const IOBB3D *b) const;

		//Setters/getters for variables
		virtual inline Vector3 GetCentre() const { return centre; }
		virtual inline VVOID SetCentre(const Vector3 &_centre) { centre = _centre; }

		virtual inline Vector3 GetLocalX() const { return localAxis[0]; }
		virtual inline VVOID SetLocalX(const Vector3 &uX) { localAxis[0] = uX; }

		virtual inline Vector3 GetLocalY() const { return localAxis[1]; }
		virtual inline VVOID SetLocalY(const Vector3 &uY) { localAxis[1] = uY; }

		virtual inline Vector3 GetLocalZ() const { return localAxis[2]; }
		virtual inline VVOID SetLocalZ(const Vector3 &uZ) { localAxis[2] = uZ; }

		virtual inline Vector3 GetLocalAxis(const BIGINT index) { return localAxis[index]; }

		virtual inline Vector3 GetExtents() const { return extents; }
		virtual inline VVOID SetExtents(const Vector3 &_extents) { extents = _extents; }

	private:
		Vector3 centre; //Centre point
		Vector3 localAxis[3]; //Local coordinate axis
		Vector3 extents; //Extents along the 3 local axes stemming from the centre point
	};
}

#endif