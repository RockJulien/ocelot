#ifndef AABB_H
#define AABB_H

#include "AABBInterfaces.h"
#include "../Maths/Ray.h"
#include "../DataStructures/MyLinkedList.h"


namespace Ocelot
{
	class Line;

	/*
		2D axis aligned bounding box specified by a minimum and maximum position.
		the minimum is the corner in the lower left (-x, -y)
		and the maximum is the corner in the upper right  (+x, +y)
	*/

	class AABBMinMax2D : public IAABBMinMax2D
	{
	public:
		//Constructors
		AABBMinMax2D();
		AABBMinMax2D(const Vector2 &min, const Vector2 &max);
		AABBMinMax2D(const FFLOAT minX, const FFLOAT minY, const FFLOAT maxX, const FFLOAT maxY);

		//Intersection test
		virtual BBOOL Intersects(const IAABBMinMax2D *b) const;

		//Setters/getters for position vectors (min and max)
		virtual inline Vector2 GetMinimum() const { return minimum; }
		virtual inline VVOID SetMinimum(const Vector2 &min) { minimum = min; }

		virtual inline Vector2 GetMaximum() const { return maximum; }
		virtual inline VVOID SetMaximum(const Vector2 &max) { maximum = max; }

		virtual inline FFLOAT GetMinX() const { return minimum.getX(); }
		virtual inline FFLOAT GetMinY() const { return minimum.getY(); }
		virtual inline FFLOAT GetMaxX() const { return maximum.getX(); }
		virtual inline FFLOAT GetMaxY() const { return maximum.getY(); }

	private:
		Vector2 minimum; //upper right corner
		Vector2 maximum; //lower left corner
	};

	/*
		2D axis aligned bounding box represented by a minimum position
		and two "extent" floats which determine how far along the x and y
		axes the box travels from the minimum position
	*/
	class AABBMinExtent2D : public IAABBMinExtent2D
	{
	public:
		//Constructors
		AABBMinExtent2D();
		AABBMinExtent2D(const Vector2 &min, const FFLOAT extentX, const FFLOAT extentY);
		AABBMinExtent2D(const FFLOAT minX, const FFLOAT minY, const FFLOAT extentX, const FFLOAT extentY);

		//Intersection test
		virtual BBOOL Intersects(const IAABBMinExtent2D *b) const;

		//Setters/getters for variables
		virtual inline Vector2 GetMinimum() const { return minimum; }
		virtual inline VVOID SetMinimum(const Vector2 &min) { minimum = min; }
		
		virtual inline FFLOAT GetMinX() const { return minimum.getX(); }
		virtual inline FFLOAT GetMinY() const { return minimum.getY(); }

		virtual inline FFLOAT GetExtentX() const { return extents[0]; }
		virtual inline VVOID SetExtentX(const FFLOAT extentX) { extents[0] = extentX; }
		virtual inline FFLOAT GetExtentY() const { return extents[1]; }
		virtual inline VVOID SetExtentY(const FFLOAT extentY) { extents[1] = extentY; }
		virtual inline VVOID SetExtents(const FFLOAT extentX, const FFLOAT extentY) { extents[0] = extentX; extents[1] = extentY; }

	private:
		Vector2 minimum; //lower left corner position
		FFLOAT extents[2]; //The distance along each axis from the minimum position
	};

	/*
		A 2D axis alligned bounding box represented by a centre position and a radius for each axis
	*/
	class AABBCentreRadius2D : public IAABBCentreRadius2D
	{
	public:
		//Constructors
		AABBCentreRadius2D();
		AABBCentreRadius2D(const Vector2 &_centre, const FFLOAT xRadius, const FFLOAT yRadius);
		AABBCentreRadius2D(const FFLOAT centreX, const FFLOAT centreY, const FFLOAT xRadius, const FFLOAT yRadius);

		//Intersection test
		virtual BBOOL Intersects(const IAABBCentreRadius2D *b) const;

		//Setters/getters for variables
		virtual inline Vector2 GetCentre() const { return centre; }
		virtual inline VVOID SetCentre(const Vector2& _centre) { centre = _centre; }
		
		virtual inline FFLOAT GetCentreX() const { return centre.getX(); }
		virtual inline FFLOAT GetCentreY() const { return centre.getY(); }

		virtual inline FFLOAT GetXRadius() const { return radii[0]; }
		virtual inline VVOID SetXRadius(const FFLOAT xRadius) { radii[0] = xRadius; }
		virtual inline FFLOAT GetYRadius() const { return radii[1]; }
		virtual inline VVOID SetYRadius(const FFLOAT yRadius) { radii[1] = yRadius; }
		virtual inline VVOID SetRadii(const FFLOAT xRadius, const FFLOAT yRadius) { radii[0] = xRadius; radii[1] = yRadius; }

	private:
		Vector2 centre; //Centre position of the bounding box
		FFLOAT radii[2]; //The radial extents along each axis (a.k.a. half-widths)
	};

	/*
		A 3D axis aligned bounding box represent by a minimum which is the lower left
		corner closest to the user (along -z in left hand coords, along +z for right)
		and a maximum which is the upper right corner furthest away from the user
		(+z for left hand, -z for right hand). This class is also used in Picking and 
		hence implemented the ray intersecting interface
	*/
	class AABBMinMax3D : public IRayIntersectable
	{
	public:
		//Constructors
		AABBMinMax3D();
		AABBMinMax3D(const Vector3 &min, const Vector3 &max);
		AABBMinMax3D(const FFLOAT minX, const FFLOAT minY, const FFLOAT minZ, const FFLOAT maxX, const FFLOAT maxY, const FFLOAT maxZ);
		//Destructor
		~AABBMinMax3D();

		//Intersection tests
		BBOOL Intersects(const AABBMinMax3D &b) const;
		BBOOL Intersects(const AABBMinMax3D *b) const;
		virtual BBOOL Intersects(const Ray& r) const; //Just returns whether it intersected the ray
		virtual BBOOL IntersectsAndFindPoint(Ray& r) const; //returns not only whether it intersected the ray
		//but also at which point along the ray. This point can be found at Ray::GetIntersectionPoint()

		//Create lines to be able to render the bounding box
		VVOID CreateBounds(ID3D10Device* device, OcelotCamera* cam, FFLOAT offset = 0.015f);
		//Update the lines position
		VVOID Update(FFLOAT deltaTime);
		//Render the lines
		VVOID RenderBounds();

		//Calculate how much the 2 boxes are penetrating, to be used in physics calculations
		FFLOAT CalculatePenetration(const AABBMinMax3D& b) const;
		FFLOAT CalculatePenetration(const AABBMinMax3D* b) const;

		//Setters/getters for variables
		inline Vector3 GetMinimum() const { return minimum; }
		inline VVOID SetMinimum(const Vector3 &min) { minimum = min; }
		inline VVOID SetMinimum(const FFLOAT minX, const FFLOAT minY, const FFLOAT minZ) { minimum = Vector3(minX, minY, minZ); }

		inline Vector3 GetMaximum() const { return maximum; }
		inline VVOID SetMaximum(const Vector3 &max) { maximum = max; }
		inline VVOID SetMaximum(const FFLOAT maxX, const FFLOAT maxY, const FFLOAT maxZ) { maximum = Vector3(maxX, maxY, maxZ); }

		inline FFLOAT GetMinX() const { return minimum.getX(); }
		inline FFLOAT GetMinY() const { return minimum.getY(); }
		inline FFLOAT GetMinZ() const { return minimum.getZ(); }
		inline FFLOAT GetMaxX() const { return maximum.getX(); }
		inline FFLOAT GetMaxY() const { return maximum.getY(); }
		inline FFLOAT GetMaxZ() const { return maximum.getZ(); }
		inline FFLOAT GetRadius() const { return m_fRadius;}

		inline Vector3 GetHalfWidths() { return Vector3((GetMinX() + GetMaxX()) * 0.5f, (GetMinY() + GetMaxY()) * 0.5f, (GetMinZ() + GetMaxZ()) * 0.5f); }
		inline Vector3 GetAxis(BIGINT index) { return GetBody()->GetTransMatrix().GetAxisVector(index); }
	private:
		Vector3             minimum; //lower left corner
		Vector3             maximum; //upper right corner
		FFLOAT              m_fRadius; 
		std::vector<Line*>  mBounds;//List of lines to render the bounding box  

		FFLOAT computePenetration(const Vector3& pCenterA, const Vector3& pCenterB) const;

	};

	/*
		A 3D axis aligned bounding box represented by a minimum vector (lower left corner)
		and 3 values ("extents") which specify how far along each axis the box goes from
		the minimum point
	*/
	class AABBMinExtent3D : public IAABBMinExtent3D
	{
	public:
		//Constructor
		AABBMinExtent3D();
		AABBMinExtent3D(const Vector3 &min, const FFLOAT extentX, const FFLOAT extentY, const FFLOAT extentZ);
		AABBMinExtent3D(const FFLOAT minX, const FFLOAT minY, const FFLOAT minZ, const FFLOAT extentX, const FFLOAT extentY, const FFLOAT extentZ);

		//Intersection test
		virtual BBOOL Intersects(const IAABBMinExtent3D *b) const;

		//Setters/getters for variables
		virtual Vector3 GetMinimum() const { return minimum; }
		virtual VVOID SetMinimum(const Vector3 &min) { minimum = min; }

		virtual FFLOAT GetExtentX() const { return extents[0]; }
		virtual VVOID SetExtentX(const FFLOAT extentX) { extents[0] = extentX; }
		virtual FFLOAT GetExtentY() const { return extents[1]; }
		virtual VVOID SetExtentY(const FFLOAT extentY) { extents[1] = extentY; }
		virtual FFLOAT GetExtentZ() const { return extents[2]; }
		virtual VVOID SetExtentZ(const FFLOAT extentZ) { extents[2] = extentZ; }
		virtual VVOID SetExtents(const FFLOAT extentX, const FFLOAT extentY, const FFLOAT extentZ) { extents[0] = extentX; extents[1] = extentY; extents[2] = extentZ; }

	private:
		Vector3 minimum; //Lower left corner of box
		FFLOAT extents[3]; //How far along each axis the box goes from the minimum point
	};

	/*
		A 3D axis aligned bounding box represented by a centre point and 
		3 radius values which denote how far from the centre each axis extends.
		The 3 radius values are also known as half-widths
	*/
	class AABBCentreRadius3D : public IAABBCentreRadius3D
	{
	public:
		//Constructors	
		AABBCentreRadius3D();
		AABBCentreRadius3D(const Vector3 &_centre, const FFLOAT xRadius, const FFLOAT yRadius, const FFLOAT zRadius);
		AABBCentreRadius3D(const FFLOAT centreX, const FFLOAT centreY, const FFLOAT centreZ, const FFLOAT xRadius, const FFLOAT yRadius, const FFLOAT zRadius);

		//Intersection test
		virtual BBOOL Intersects(const IAABBCentreRadius3D *b);

		//Setters/getters for variables
		virtual inline Vector3 GetCentre() const { return centre; }
		virtual inline VVOID SetCentre(const Vector3 &_centre) { centre = _centre; }
		
		virtual inline FFLOAT GetXRadius() const { return radii[0]; }
		virtual inline FFLOAT GetYRadius() const { return radii[1]; }
		virtual inline FFLOAT GetZRadius() const { return radii[2]; }
		virtual inline VVOID SetXRadius(const FFLOAT xRadius) { radii[0] = xRadius; }
		virtual inline VVOID SetYRadius(const FFLOAT yRadius) { radii[1] = yRadius; }
		virtual inline VVOID SetZRadius(const FFLOAT zRadius) { radii[2] = zRadius; }
		virtual inline VVOID SetRadii(const FFLOAT xRadius, const FFLOAT yRadius, const FFLOAT zRadius) { radii[0] = xRadius; radii[1] = yRadius; radii[2] = zRadius; }

	private:
		Vector3 centre; //Centre position of the box
		FFLOAT radii[3]; //The half-width extents along each axis stemming from the centre point
	};
}

#endif