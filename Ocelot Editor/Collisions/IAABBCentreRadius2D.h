#ifndef IAABBCENTRERADIUS2D_H
#define IAABBCENTRERADIUS2D_H

#include "../Maths/Vector2.h"

/*
 * The interface to which all 2D AABBs (represented with centre and a radius) use
 * in the Ocelot engine. 
 * The class itself: A 2D axis aligned bounding box represented by a centre position and a radius for each axis
 */

namespace Ocelot
{
	class IAABBCentreRadius2D
	{
	public:
		//Intersection test
		virtual BBOOL Intersects(const IAABBCentreRadius2D *b) const = 0;

		//Setters/getters for variables
		//Centre position of the bounding box
		virtual Vector2 GetCentre() const = 0;
		virtual VVOID SetCentre(const Vector2& _centre) = 0;
		
		virtual FFLOAT GetCentreX() const = 0;
		virtual FFLOAT GetCentreY() const = 0;

		//The radial extents along each axis (a.k.a. half-widths)
		virtual FFLOAT GetXRadius() const = 0;
		virtual VVOID SetXRadius(const FFLOAT xRadius) = 0;
		virtual FFLOAT GetYRadius() const = 0;
		virtual VVOID SetYRadius(const FFLOAT yRadius) = 0;
		virtual VVOID SetRadii(const FFLOAT xRadius, const FFLOAT yRadius) = 0;
	};
}

#endif