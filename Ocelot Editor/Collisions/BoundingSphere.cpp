#include "BoundingSphere.h"

using namespace Ocelot;

//Constructor
BoundingSphere::BoundingSphere()
	: sphereCentre(0.0f, 0.0f, 0.0f), sphereRadius(0.0f)
{}

//Constructor
BoundingSphere::BoundingSphere(const Vector3 &_centre, const FFLOAT _radius)
	: sphereCentre(_centre), sphereRadius(_radius)
{}

//Constructor
BoundingSphere::BoundingSphere(const FFLOAT centreX, const FFLOAT centreY, const FFLOAT centreZ, const FFLOAT _radius)
	: sphereCentre(centreX, centreY, centreZ), sphereRadius(_radius)
{}

//Intersection test
BBOOL BoundingSphere::Intersects(const BoundingSphere &b) const
{
	//Very simple intersection test, if the distance between their
	//centers is less than the sum of their radii, they intersect
	Vector3 toCentre = sphereCentre - b.GetCentre();

	return toCentre.Vec3Length() <= GetRadius() + b.GetRadius();
}

//Intersection test against a ray
BBOOL BoundingSphere::Intersects(const Ray& r) const
{
	//A vector from the centre to the origin of the ray
	Vector3 toOrigin = r.GetOrigin() - sphereCentre;

	//Distance between this vector and the radius of the sphere.
	//Note a vector "dotted" with itself is equal to its magnitude squared
	FFLOAT relRayOrigin = toOrigin.Vec3DotProduct(toOrigin) - (sphereRadius * sphereRadius);
	if(relRayOrigin <= 0.0f) //If ray origin is inside sphere, then it intersects regardless of ray direction
	{
		return true;
	}

	//Angle between this vector and direction of the ray
	FFLOAT relRayDirection = toOrigin.Vec3DotProduct(r.GetDirection());
	if(relRayDirection > 0.0f) //If > 0 then facing *away* from sphere
	{
		return false;
	}

	FFLOAT discriminant = relRayDirection * relRayDirection - relRayOrigin; //Check for real solutions, imaginary solutions => no intersection
	if(discriminant < 0.0f) //If this is negative, the square root (when working out the quadratic to find t) would give complex numbers
	{
		return false;
	}

	return true;
}

//Intersection test against a ray and stores the intersection point
BBOOL BoundingSphere::IntersectsAndFindPoint(Ray& pFired) const
{
	//A vector from the centre to the origin of the ray
	Vector3 lToOrigin = pFired.GetOrigin() - this->sphereCentre;

	//Distance between this vector and the radius of the sphere.
	//Note a vector "dotted" with itself is equal to its magnitude squared
	FFLOAT relRayOrigin = lToOrigin.Vec3DotProduct(lToOrigin) - SQUARE(this->sphereRadius);
	if (relRayOrigin <= 0.0f) //If ray origin is inside sphere, then it intersects regardless of ray direction
	{
		return true;
	}

	//Angle between this vector and direction of the ray
	FFLOAT relRayDirection = lToOrigin.Vec3DotProduct(pFired.GetDirection());
	if (relRayDirection > 0.0f) //If > 0 then facing *away* from sphere
	{
		return false;
	}

	FFLOAT discriminant = SQUARE(relRayDirection) - relRayOrigin; //Check for real solutions, imaginary solutions => no intersection
	if (discriminant < 0.0f) //If this is negative, the square root (when working out the quadratic to find t) would give complex numbers
	{
		return false;
	}

	return true;

	//Quadratic solution (for the smallest t value)
	FFLOAT t = -relRayDirection - sqrtf(discriminant);
	if(t < 0.0f) //smallest t could be negative to clamp to [0, infinity)
	{
		t = 0.0f;
	}

	//Set intersection point as smallest t
	pFired.SetIntersectionPoint(pFired(t));
	return true;
}

//Create the bounds to be able to draw the bounding volume
VVOID BoundingSphere::CreateBounds(ID3D10Device* device, OcelotCamera* cam, FFLOAT offset)
{
	CircleInfo lInfo( device, 
					  cam, 
					  L"", 
					  L"..\\Effects\\Line.fx", 
					  sphereCentre, 
					  sphereRadius + offset, 
					  15, 
					  Axes::AXIS_X );

	// create circle
	Circle* lCir1 = new Circle(lInfo);
	Circle* lCir2 = new Circle(lInfo);
	IMeshEditor* lEditor = lCir1->StartEdition();
	lEditor->RotateY( 45.0f );
	DeletePointer( lEditor );
	lEditor = lCir2->StartEdition();
	lEditor->RotateY( -45.0f );
	DeletePointer( lEditor );
	this->mBounds.push_back( lCir1 );
	this->mBounds.push_back( lCir2 );

	for
		( UBIGINT lCurrLine = 0; lCurrLine < (UBIGINT)this->mBounds.size(); lCurrLine++ )
	{
		this->mBounds[lCurrLine]->createInstance();
	}
}

//Update the bounding volume to move when the corresponding object it surrounds also moves
VVOID BoundingSphere::Update(FFLOAT deltaTime)
{
	for
		(UBIGINT lCurrCircle = 0; lCurrCircle < (UBIGINT)this->mBounds.size(); lCurrCircle++)
	{
		// update the world regarding to its master mesh (yes master !!!)
		if 
			( lCurrCircle == 0 )
		{
			Matrix4x4 lRotation;
			lRotation.Matrix4x4RotationY(lRotation, 45.0f);
			this->mBounds[lCurrCircle]->SetWorld(this->m_matWorld * lRotation);
		}

		if 
			( lCurrCircle == 1 )
		{
			Matrix4x4 lRotation;
			lRotation.Matrix4x4RotationY(lRotation, -45.0f);
			this->mBounds[lCurrCircle]->SetWorld(this->m_matWorld * lRotation);
		}

		this->mBounds[lCurrCircle]->update(deltaTime);
	}
}

//Draw the bounding volume
VVOID BoundingSphere::RenderBounds()
{
	for
		( UBIGINT lCurrLine = 0; lCurrLine < (UBIGINT)this->mBounds.size(); lCurrLine++ )
	{
		this->mBounds[lCurrLine]->renderMesh();
	}
}

//Constructor
BoundingCircle::BoundingCircle()
	: circleCentre(0.0f, 0.0f), circleRadius(0.0f)
{}

//Constructor
BoundingCircle::BoundingCircle(const Vector2 &_centre, const FFLOAT _radius)
	: circleCentre(_centre), circleRadius(_radius)
{}

//Constructor
BoundingCircle::BoundingCircle(const FFLOAT centreX, const FFLOAT centreY, const FFLOAT _radius)
	: circleCentre(centreX, centreY), circleRadius(_radius)
{}

//Intersection test
BBOOL BoundingCircle::Intersects(const IBoundingCircle *b) const
{
	//Possibly the easiest intersection test ever. Find the distance between the two
	//centres, if this distance is more than the sum of their radii, they intersect.
	Vector2 toCentre = circleCentre - b->GetCentre();

	return toCentre.Vec2Length() <= GetRadius() + b->GetRadius();
}