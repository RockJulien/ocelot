#ifndef IAABBMINEXTEND2D_H
#define IAABBMINEXTEND2D_H

#include "../Maths/Vector2.h"

/*
 * The interface used by all 2D aabbs (represented by a minimum position
 * and 2D halfwidth "extents") used in the Ocelot engine. The class itself
 * can be described as:
 * 2D axis aligned bounding box represented by a minimum position
   and two "extent" floats which determine how far along the x and y
   axes the box travels from the minimum position
 */
namespace Ocelot
{
	class IAABBMinExtent2D
	{
	public:
		//Intersection test
		virtual BBOOL Intersects(const IAABBMinExtent2D *b) const = 0;

		//Setters/getters for variables
		virtual Vector2 GetMinimum() const = 0;
		virtual VVOID SetMinimum(const Vector2 &min) = 0;
		
		virtual FFLOAT GetMinX() const = 0;
		virtual FFLOAT GetMinY() const = 0;

		virtual FFLOAT GetExtentX() const = 0;
		virtual VVOID SetExtentX(const FFLOAT extentX) = 0;
		virtual FFLOAT GetExtentY() const = 0;
		virtual VVOID SetExtentY(const FFLOAT extentY) = 0;
		virtual VVOID SetExtents(const FFLOAT extentX, const FFLOAT extentY) = 0;
	};
}

#endif