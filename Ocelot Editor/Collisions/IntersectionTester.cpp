#include "IntersectionTester.h"

using namespace Ocelot;

Vector3 IntersectionTester::ClosestPointOnLineSegment(const Vector3 &lineStart, const Vector3 &lineEnd, const Vector3 &point)
{
	//Vector from start to end
	Vector3 toEnd = lineEnd - lineStart;
	
	//project point onto line segment
	FFLOAT interpPoint = (point - lineStart).Vec3DotProduct(toEnd) / toEnd.Vec3DotProduct(toEnd);

	//Clamp with boundaries of line segment
	if(interpPoint < 0.0f)
	{
		interpPoint = 0.0f;
	}
	if(interpPoint > 1.0f)
	{
		interpPoint = 1.0f;
	}

	return lineStart + interpPoint * toEnd;
}

Vector3 IntersectionTester::ClosestPointOnAABB(const AABBMinMax3D &aabb, const Vector3 &point)
{
	Vector3 result;

	//Clamp all axes to either a face, vertex or edge Voronoi region
	//This essentially means test for whether the point is inside, outside,
	//or on the AABB and clamp to its surface as neccessary. Repeat for each
	//axis
	// 

	//X-axis
	FFLOAT x = point.getX();
	if(x < aabb.GetMinX())
	{
		x = aabb.GetMinX();
	}
	if(x > aabb.GetMaxX())
	{
		x = aabb.GetMaxX();
	}
	result.x(x);

	//Y-axis
	FFLOAT y = point.getY();
	if(y < aabb.GetMinY())
	{
		y = aabb.GetMinY();
	}
	if(y > aabb.GetMaxY())
	{
		y = aabb.GetMaxY();
	}
	result.y(y);

	//Z-axis
	FFLOAT z = point.getZ();
	if(z < aabb.GetMinZ())
	{
		z = aabb.GetMinZ();
	}
	if(z > aabb.GetMaxZ())
	{
		z = aabb.GetMaxZ();
	}
	result.z(z);

	return result;
}

//Alternative, more-optimized version of the ClosestPointOnLineSegment() function above
Vector3 IntersectionTester::ClosestPointOnLineSegmentOpt(const Vector3 &lineStart, const Vector3 &lineEnd, const Vector3 &point)
{
	//Vector from start to end
	Vector3 toEnd = lineEnd - lineStart;

	//project point onto line segment, but not dividing by ab just yet. 
	//The division is only needed if t is less than the magnitude of ab
	FFLOAT interpPoint = (point - lineStart).Vec3DotProduct(toEnd);

	if(interpPoint <= 0.0f)
	{
		return lineStart; //if t <= 0, it is either on lineStart or lineStart is closest so it would get clamped to there anyway
	} 
	else
	{
		//Now test for whether we actually need to divide and proceed the same
		//way the original ClosestPointOnLineSegment() did
		FFLOAT den = toEnd.Vec3DotProduct(toEnd);
		if(interpPoint >= den)
		{
			return lineEnd;
		}
		else
		{
			interpPoint /= den;
			return lineStart + interpPoint * toEnd;
		}
	}
}

Vector3 IntersectionTester::ClosestPointOnOBB(OBB3D& obb, const Vector3& point)
{
	//Begin result at obb's centre and step away from there
	Vector3 result = obb.GetCentre();

	//Vector from obb's centre to the point
	Vector3 toPoint = point - obb.GetCentre();

	//For each axis
	for(BIGINT i = 0; i < 3; ++i)
	{
		Vector3& localAxis = obb.GetLocalAxis(i); //Get the axis we're testing against

		//project d onto the axis...
		FFLOAT distance = toPoint.Vec3DotProduct(localAxis);
		//and then test for overlap, clamping if its outside the axis ranges
		if(distance > obb.GetExtents()[i])
		{
			distance = obb.GetExtents()[i];
		}
		if(distance < -obb.GetExtents()[i])
		{
			distance = - obb.GetExtents()[i];
		}

		result += distance * localAxis;
	}
	return result;
}

BBOOL IntersectionTester::SphereIntersectAABBTest(const BoundingSphere& sphere, const AABBMinMax3D& aabb)
{
	//Very simple test, find the closest point on the AABB that the sphere's centre lies on
	Vector3 point = ClosestPointOnAABB(aabb, sphere.GetCentre());
	//Get the vector from the centre to that spheres point
	Vector3 v = point - sphere.GetCentre();

	//...and as with most sphere tests, check this distance is less than the sum of radii
	//(in this case, we're checking the square of the distance against the square of the radii)
	//(it saves a square root call)
	return v.Vec3DotProduct(v) <= (sphere.GetRadius() * sphere.GetRadius());
}

BBOOL IntersectionTester::SphereIntersectOBBTest(const BoundingSphere& sphere, OBB3D& obb)
{
	//Very simple test, find the closest point on the OBB that the sphere's centre lies on
	Vector3 point = ClosestPointOnOBB(obb, sphere.GetCentre());

	//Get the vector from the centre to that spheres point
	Vector3 v = point - sphere.GetCentre();

	//...and as with most sphere tests, check this distance is less than the sum of radii
	//(in this case, we're checking the square of the distance against the square of the radii)
	//(it saves a square root call)
	return v.Vec3DotProduct(v) <= (sphere.GetRadius() * sphere.GetRadius());
}

BBOOL IntersectionTester::LineSegmentIntersectAABBTest(const Vector3 &lineStart, const Vector3 &lineEnd, const AABBMinMax3D& aabb)
{
	//Calculate centre of box
	Vector3 boxCentre = (aabb.GetMinimum() + aabb.GetMaximum()) * 0.5f;
	//Calculate its "half-widths"
	Vector3 boxExtents = aabb.GetMaximum() - boxCentre;

	//Calculate centre of line segment
	Vector3 segmentCentre = (lineStart + lineEnd) * 0.5f;
	//Calculate the lines "half-width"
	Vector3 segmentExtent = lineEnd - segmentCentre;
	
	//Make it relative to the box centre (i.e. stick in box's local coord frame)
	segmentCentre -= boxCentre;

	//Test for overlap on each axis
	//X-axis
	FFLOAT segX = fabsf(segmentExtent.getX());
	if(fabsf(segmentCentre.getX()) > boxExtents.getX() + segX)
	{
		return false;
	}

	//Y-axis
	FFLOAT segY = fabsf(segmentExtent.getY());
	if(fabsf(segmentCentre.getY()) > boxExtents.getY() + segY)
	{
		return false;
	}

	//Z-axis
	FFLOAT segZ = fabsf(segmentExtent.getZ());
	if(fabsf(segmentCentre.getZ()) > boxExtents.getZ() + segZ)
	{
		return false;
	}

	//Get rid of possibility of completely parallel line segment to axes
	//(as it would bugger up the following tests)
	segX += FLT_EPSILON;
	segY += FLT_EPSILON;
	segZ += FLT_EPSILON;

	//Test determinant of each axis in line segments coords against determinant in boxes coords
	if(fabsf(segmentCentre.getY() * segmentExtent.getZ() - segmentCentre.getZ() * segmentExtent.getY()) > boxExtents.getY() * segZ + boxExtents.getZ() * segY)
	{
		return false;
	}
	if(fabsf(segmentCentre.getZ() * segmentExtent.getX() - segmentCentre.getX() * segmentExtent.getZ()) > boxExtents.getX() * segZ + boxExtents.getZ() * segX)
	{
		return false;
	}
	if(fabsf(segmentCentre.getX() * segmentExtent.getY() - segmentCentre.getY() * segmentExtent.getX()) > boxExtents.getX() * segY + boxExtents.getY() * segX)
	{
		return false;
	}

	return true;
}