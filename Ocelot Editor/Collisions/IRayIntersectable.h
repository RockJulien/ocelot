#ifndef IRAYINTERSECTABLE_H
#define IRAYINTERSECTABLE_H

#include "../Maths/Ray.h"
#include "../Physics/IRigidBody.h"
#include <D3D10.h>
#include "../Camera/OcelotCamera.h"

/*
 * The interface used by all ray pickable objects in the Ocelot engine.
 * It is not strictly an interface since it defines some variables of its
 * own but can be used in the exact same way and allows the code to follow
 * more of the SOLID principles
 */
namespace Ocelot
{
	class IRayIntersectable
	{
	protected:

				Matrix4x4           m_matWorld;
				IRigidBody*			body;

	public:
		//Intersection tests
		//First test checks for intersection
		virtual BBOOL Intersects(const Ray& r) const               = 0;
		//Second test checks for intersection and stores intersection point in the ray
		virtual BBOOL IntersectsAndFindPoint(Ray& r) const         = 0;
		
		//Create drawable bounds so the bounding volume can be rendered
		virtual VVOID CreateBounds(ID3D10Device*, OcelotCamera*, FFLOAT offset = 0.01f) = 0;
		//Update the position of the bounds as the object it encloses moves
		virtual VVOID Update(FFLOAT)                                = 0;
		//Render the bounding volume
		virtual VVOID RenderBounds()                               = 0;

		//Setters/getters for variables
		inline  VVOID      SetWorld(const Matrix4x4& world) { m_matWorld = world;}
		inline  Matrix4x4 GetWorld() const { return m_matWorld;}

		inline VVOID SetBody(IRigidBody* b) { body = b;}
		inline IRigidBody* GetBody() { return body;}
	};
}

#endif