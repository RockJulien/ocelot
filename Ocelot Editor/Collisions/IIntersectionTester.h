#ifndef IINTERSECTIONTESTER_H
#define IINTERSECTIONTESTER_H

#include "../Maths/Vector3.h"
#include "AABB.h"
#include "BoundingSphere.h"
#include "OBB.h"

/*
 * The interface that all intersection testers use in ocelot engine
 * The class itself should be described as represented as:
 * Class which enables intersection between two different types 
 *	(such as sphere and aabb) and also has tests to determine which
 *	point the collisions intersect at (or nearest point)
 */
namespace Ocelot
{
	class IIntersectionTester
	{
	public:
		//Determine the closest points on various objects
		virtual Vector3 ClosestPointOnLineSegment(const Vector3 &lineStart, const Vector3 &lineEnd, const Vector3 &point) = 0;
		virtual Vector3 ClosestPointOnLineSegmentOpt(const Vector3 &lineStart, const Vector3 &lineEnd, const Vector3 &point) = 0;
		virtual Vector3 ClosestPointOnAABB(const AABBMinMax3D &aabb, const Vector3 &point) = 0;
		virtual Vector3 ClosestPointOnOBB(OBB3D& obb, const Vector3& point) = 0;

		//Determine whether an object of type X intersects an object of type Y for all X != Y
		virtual BBOOL SphereIntersectAABBTest(const BoundingSphere& sphere, const AABBMinMax3D& aabb) = 0;
		virtual BBOOL SphereIntersectOBBTest(const BoundingSphere& sphere, OBB3D& obb) = 0;
		virtual BBOOL LineSegmentIntersectAABBTest(const Vector3 &lineStart, const Vector3 &lineEnd, const AABBMinMax3D& aabb) = 0;
	};
}

#endif