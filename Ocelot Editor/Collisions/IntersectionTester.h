#ifndef INTERSECTIONTESTER_H
#define INTERSECTIONTESTER_H

#include "IIntersectionTester.h"

namespace Ocelot
{
	/*
	 *	Class which enables intersection between two different types 
	 *	(such as sphere and aabb) and also has tests to determine which
	 *	point the collisions intersect at (or nearest point)
	 */
	class IntersectionTester : public IIntersectionTester
	{
	public:
		//Determine the closest points on various objects
		virtual Vector3 ClosestPointOnLineSegment(const Vector3 &lineStart, const Vector3 &lineEnd, const Vector3 &point);
		virtual Vector3 ClosestPointOnLineSegmentOpt(const Vector3 &lineStart, const Vector3 &lineEnd, const Vector3 &point);
		virtual Vector3 ClosestPointOnAABB(const AABBMinMax3D &aabb, const Vector3 &point);
		virtual Vector3 ClosestPointOnOBB(OBB3D& obb, const Vector3& point);
	
		//Determine whether an object of type X intersects an object of type Y for all X != Y
		virtual BBOOL SphereIntersectAABBTest(const BoundingSphere& sphere, const AABBMinMax3D& aabb);
		virtual BBOOL SphereIntersectOBBTest(const BoundingSphere& sphere, OBB3D& obb);
		virtual BBOOL LineSegmentIntersectAABBTest(const Vector3 &lineStart, const Vector3 &lineEnd, const AABBMinMax3D& aabb);
	private:

	};
}

#endif