#ifndef IAABBMINMAX2D_H
#define IAABBMINMAX2D_H

#include "../Maths/Vector2.h"

/*
 *  The interface used by all 2D aabbs (represented by a min and max position) 
 *  used in the ocelot engine. The class itself can be described as:
 *  2D axis aligned bounding box specified by a minimum and maximum position.
	the minimum is the corner in the lower left (-x, -y)
	and the maximum is the corner in the upper right  (+x, +y)
 */

namespace Ocelot
{
	class IAABBMinMax2D
	{
	public:
		//Intersection test
		virtual BBOOL Intersects(const IAABBMinMax2D *b) const = 0;
		
		//Setters/getters for position vectors (min and max)
		virtual Vector2 GetMinimum() const = 0;
		virtual VVOID SetMinimum(const Vector2 &min) = 0;
		
		virtual Vector2 GetMaximum() const = 0;
		virtual VVOID SetMaximum(const Vector2 &max) = 0;

		virtual FFLOAT GetMinX() const = 0;
		virtual FFLOAT GetMinY() const = 0;
		virtual FFLOAT GetMaxX() const = 0;
		virtual FFLOAT GetMaxY() const = 0;
	};
}

#endif