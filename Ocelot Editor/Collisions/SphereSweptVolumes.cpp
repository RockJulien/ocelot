#include "SphereSweptVolumes.h"

using namespace Ocelot;

//Constructor
Capsule::Capsule()
	: startPoint(0.0f, 0.0f, 0.0f), endPoint(0.0f, 0.0f, 0.0f), sphereRadius(0.0f)
{}

//Constructor
Capsule::Capsule(const Vector3 &_start, const Vector3 &_end, const FFLOAT _radius)
	: startPoint(_start), endPoint(_end), sphereRadius(_radius)
{}

//Constructor
Capsule::Capsule(const FFLOAT startX, const FFLOAT startY, const FFLOAT startZ, const FFLOAT endX, const FFLOAT endY, const FFLOAT endZ, const FFLOAT _radius)
	: startPoint(startX, startY, startZ), endPoint(endX, endY, endZ), sphereRadius(_radius)
{}

//Intersection test against a sphere
BBOOL Capsule::Intersects(const BoundingSphere &b) const
{
	return true;
}

//Intersection test against another capsule
BBOOL Capsule::Intersects(const ICapsule &b) const
{
	return true;
}