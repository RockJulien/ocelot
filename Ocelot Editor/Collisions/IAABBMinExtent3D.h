#ifndef IAABBMINEXTENT3D_H
#define IAABBMINEXTENT3D_H

#include "../Maths/Vector3.h"

/*
 * The interface used by all 3D aabbs (represented by a minimum position and
 * 3 extents along each world axis stemming from that minimum) used in the
 * ocelot engine. The minextent aabb itself can be described as:
 * A 3D axis aligned bounding box represented by a minimum vector (lower left corner)
   and 3 values ("extents") which specify how far along each axis the box goes from
   the minimum point
 */
namespace Ocelot
{
	class IAABBMinExtent3D
	{
	public:
		//Intersection test
		virtual BBOOL Intersects(const IAABBMinExtent3D *b) const = 0;

		//Setters/getters for variables
		virtual Vector3 GetMinimum() const = 0;
		virtual VVOID SetMinimum(const Vector3 &min) = 0;

		virtual FFLOAT GetExtentX() const = 0;
		virtual VVOID SetExtentX(const FFLOAT extentX) = 0;
		virtual FFLOAT GetExtentY() const = 0;
		virtual VVOID SetExtentY(const FFLOAT extentY) = 0;
		virtual FFLOAT GetExtentZ() const = 0;
		virtual VVOID SetExtentZ(const FFLOAT extentZ) = 0;
		virtual VVOID SetExtents(const FFLOAT extentX, const FFLOAT extentY, const FFLOAT extentZ) = 0;
	};
}

#endif