#include "AABB.h"
#include "../Geometry/Line.h"

using namespace Ocelot;

//Constructor
AABBMinMax2D::AABBMinMax2D()
	: minimum(0.0f,0.0f), maximum(0.0f,0.0f)
{}

//Constructor
AABBMinMax2D::AABBMinMax2D(const Vector2 &min, const Vector2 &max)
	: minimum(min), maximum(max)
{}

//Constructor
AABBMinMax2D::AABBMinMax2D(const FFLOAT minX, const FFLOAT minY, const FFLOAT maxX, const FFLOAT maxY)
	: minimum(minX,minY), maximum(maxX,maxY)
{}

//Intersection test
BBOOL AABBMinMax2D::Intersects(const IAABBMinMax2D *b) const
{
	//Determines intersection by checking overlap on each axis
	//Exit out early if no intersection is found on a single axis
	 
	//X-axis check
	if(GetMaxX() < b->GetMinX() || GetMinX() > b->GetMaxX())
	{
		return false;
	}

	//Y-axis check
	if(GetMaxY() < b->GetMinY() || GetMinY() > b->GetMaxY())
	{
		return false;
	}

	//If we reach here, there is overlap on all axes, hence the
	//boxes must be overlapping
	return true;
}

//Constructor
AABBMinExtent2D::AABBMinExtent2D()
	: minimum(0.0f,0.0f)
{
	extents[0] = 0.0f;
	extents[1] = 0.0f;
}

//Constructor
AABBMinExtent2D::AABBMinExtent2D(const Vector2 &min, const FFLOAT extentX, const FFLOAT extentY)
	: minimum(min)
{
	extents[0] = extentX;
	extents[1] = extentY;
}

//Constructor
AABBMinExtent2D::AABBMinExtent2D(const FFLOAT minX, const FFLOAT minY, const FFLOAT extentX, const FFLOAT extentY)
	: minimum(minX,minY)
{
	extents[0] = extentX;
	extents[1] = extentY;
}

//Intersection test
BBOOL AABBMinExtent2D::Intersects(const IAABBMinExtent2D *b) const
{
	//Tests for overlap on each axis relative to each extent

	//X-axis
	FFLOAT signum = GetMinX() - b->GetMinX();
	if(signum > b->GetExtentX() || -signum > GetExtentX())
	{
		return false;
	}
	//Y-axis
	signum = GetMinY() - b->GetMinY();
	if(signum > b->GetExtentY() || -signum > GetExtentY())
	{
		return false;
	}

	//If we reach here, there is overlap on each axis and hence
	//the boxes are intersecting
	return true;
}

//Constructor
AABBCentreRadius2D::AABBCentreRadius2D()
	: centre(0.0f,0.0f)
{
	radii[0] = 0.0f;
	radii[1] = 0.0f;
}

//Constructor
AABBCentreRadius2D::AABBCentreRadius2D(const Vector2 &_centre, const FFLOAT xRadius, const FFLOAT yRadius)
	: centre(_centre)
{
	radii[0] = xRadius;
	radii[1] = yRadius;
}

//Constructor
AABBCentreRadius2D::AABBCentreRadius2D(const FFLOAT centreX, const FFLOAT centreY, const FFLOAT xRadius, const FFLOAT yRadius)
	: centre(centreX, centreY)
{
	radii[0] = xRadius;
	radii[1] = yRadius;
}

//Intersection test
BBOOL AABBCentreRadius2D::Intersects(const IAABBCentreRadius2D *b) const
{
	//On each axis, test for overlap by checking the difference between their
	//positions against the sum of their radii. Very similar to a sphere test!
	//Although, because its not a sphere, you have to check each axis at a time
	//and not do just one check (like you would with a sphere)
	
	//X-axis
	if(fabsf(GetCentreX() - b->GetCentreX()) > (GetXRadius() + b->GetXRadius()))
	{
		return false;
	}

	//Y-axis
	if(fabsf(GetCentreY() - b->GetCentreY()) > (GetYRadius() + b->GetYRadius()))
	{
		return false;
	}

	//Overlap on all axes hence they are intersecting
	return true;
}

//Constructor
AABBMinMax3D::AABBMinMax3D() : 
minimum(0.0f), maximum(1.0f), m_fRadius(0.5f)
{}

//Constructor
AABBMinMax3D::AABBMinMax3D(const Vector3 &min, const Vector3 &max) : 
minimum(min), maximum(max), m_fRadius((max - min).Vec3Length() / 2.0f)
{}

//Constructor
AABBMinMax3D::AABBMinMax3D(const FFLOAT minX, const FFLOAT minY, const FFLOAT minZ, const FFLOAT maxX, const FFLOAT maxY, const FFLOAT maxZ) : 
minimum(minX, minY, minZ), maximum(maxX, maxY, maxZ), m_fRadius((maximum - minimum).Vec3Length() / 2.0f)
{}

//Destructor
AABBMinMax3D::~AABBMinMax3D()
{
	if 
		( this->mBounds.size() > 0 )
	{
		vector<Line*>::iterator lLineIter = this->mBounds.begin();
		for 
			( ; lLineIter != this->mBounds.end(); lLineIter++ )
		{
			DeletePointer((*lLineIter));
		}

		this->mBounds.clear();
	}
}

//Calculate how much the 2 boxes are penetrating, to be used in physics calculations
//Ian Millington's penetration code for particles is rubbish! (in fact, it's
//non-existant, meaning the penetration value for his particle simulations are all
//the floats default value (a value very close to zero)), this is why the objects
//jittering on the floor very badly. Now, with particles representing large objects, this
// is gonna happen anyway, but a good penetration calculation can reduce the jittering and
// this code reduces it quite a lot (but not completely, I think it's technically impossible for that
// as you would need an infinite frame rate)
FFLOAT AABBMinMax3D::CalculatePenetration(const AABBMinMax3D& b) const
{
	//Penetrations are checked relative to their centres
	Vector3 centreW = 0.5f * (GetMinimum() + GetMaximum());
	Vector3 bCentreW = 0.5f * (b.GetMinimum() + b.GetMaximum());

	return this->computePenetration(centreW, bCentreW);
}

FFLOAT AABBMinMax3D::CalculatePenetration(const AABBMinMax3D* b) const
{
	//Penetrations are checked relative to their centres
	Vector3 centreW = 0.5f * (GetMinimum() + GetMaximum());
	Vector3 bCentreW = 0.5f * (b->GetMinimum() + b->GetMaximum());

	return this->computePenetration(centreW, bCentreW);
}

FFLOAT AABBMinMax3D::computePenetration(const Vector3& pCenterA, const Vector3& pCenterB) const
{
	FFLOAT smallestX, smallestY, smallestZ;
	FFLOAT largestX, largestY, largestZ;

	//Find the smallest penetration along each axis
	smallestX = min(pCenterA.getX(), pCenterB.getX());
	smallestY = min(pCenterA.getY(), pCenterB.getY());
	smallestZ = min(pCenterA.getZ(), pCenterB.getZ());

	//Find the largest penetration along each axis
	largestX = max(pCenterA.getX(), pCenterB.getX());
	largestY = max(pCenterA.getY(), pCenterB.getY());
	largestZ = max(pCenterA.getZ(), pCenterB.getZ());

	//Get penetration distance on each axis
	FFLOAT penetrationX = smallestX = largestX;
	FFLOAT penetrationY = smallestY = largestY;
	FFLOAT penetrationZ = smallestZ = largestZ;

	//Find the value closest to 0, scaled along the axis this value lies on
	FFLOAT penetration = penetrationX;
	FFLOAT scale = largestX - smallestX;
	if (fabsf(penetrationY) < fabsf(penetration))
	{
		penetration = penetrationY;
		scale = largestY - smallestY;
	}
	if (fabsf(penetrationZ) < fabsf(penetration))
	{
		penetration = penetrationZ;
		scale = largestZ - smallestZ;
	}

	//Thar be your penetration amount in the direction of the collision 
	return penetration * scale;
}

//Intersection test
BBOOL AABBMinMax3D::Intersects(const AABBMinMax3D &b) const
{
	//Determines intersection by checking overlap on each axis
	//Exit out early if no intersection is found on a single axis

	//X-axis check
	if(GetMaxX() < b.GetMinX() || GetMinX() > b.GetMaxX())
	{
		return false;
	}

	//Y-axis check
	if(GetMaxY() < b.GetMinY() || GetMinY() > b.GetMaxY())
	{
		return false;
	}

	//Z-axis check
	if(GetMaxZ() < b.GetMinZ() || GetMinZ() > b.GetMaxZ())
	{
		return false;
	}

	//If we reach here, there is overlap on all axes and hence the
	//two boxes are intersecting
	return true;
}

BBOOL AABBMinMax3D::Intersects(const AABBMinMax3D *b) const
{
	//Determines intersection by checking overlap on each axis
	//Exit out early if no intersection is found on a single axis

	//X-axis check
	if (GetMaxX() < b->GetMinX() || GetMinX() > b->GetMaxX())
	{
		return false;
	}

	//Y-axis check
	if (GetMaxY() < b->GetMinY() || GetMinY() > b->GetMaxY())
	{
		return false;
	}

	//Z-axis check
	if (GetMaxZ() < b->GetMinZ() || GetMinZ() > b->GetMaxZ())
	{
		return false;
	}

	//If we reach here, there is overlap on all axes and hence the
	//two boxes are intersecting
	return true;
}

//Intersection test
BBOOL AABBMinMax3D::Intersects(const Ray& r) const
{
	//The ray is tested against 3 slabs, where a slab is the distance between
	//parallel planes (we have 3 axes hence there will be 3 slabs), if the origin
	//of the ray lies to the side of any of these slabs, there is no intersection.
	//If there is, it will intersect at two places (giving two t's for the ray equation:
	//r = origin + (t * direction). If the minimum of these is larger than than the 
	//maximum, then there is no overlap (technically it means the quadratic solved to
	//obtain t has complex "imaginary" numbers as a solution, which we don't want)
	// 
	FFLOAT interpMin = -FLT_MAX;
	FFLOAT interpMax = FLT_MAX;

	//for each slab
	for(BIGINT slab(0); slab < 3; ++slab)
	{
		if(fabsf(r.GetDirection()[slab]) < FLT_EPSILON)
		{
			//ray is parallel to the slab, no intersection if the ray is
			//not lying within slab
			if(r.GetOrigin()[slab] < minimum[slab] || r.GetOrigin()[slab] > maximum[slab])
			{
				return false;
			}
		} 
		else
		{
			//Find two intersection values
			FFLOAT invDir = 1.0f / r.GetDirection()[slab];

			FFLOAT t1 = (minimum[slab] - r.GetOrigin()[slab]) * invDir;
			FFLOAT t2 = (maximum[slab] - r.GetOrigin()[slab]) * invDir;

			//t1 is the near slab intersection, so it should be swapped with t2
			//if this condition is true
			if(t1 > t2)
			{
				FFLOAT temp = t1;
				t1 = t2;
				t2 = temp;
			}

			interpMin = interpMin > t1 ? interpMin : t1;
			interpMax = interpMax < t2 ? interpMax : t2;

			if(interpMin > interpMax)
			{
				return false; //no slab intersection if tMin > tMax
			}
		}
	}
	return true;
}

//Intersection test that stores point of intersection in ray
BBOOL AABBMinMax3D::IntersectsAndFindPoint(Ray& r) const
{
	//The ray is tested against 3 slabs, where a slab is the distance between
	//parallel planes (we have 3 axes hence there will be 3 slabs), if the origin
	//of the ray lies to the side of any of these slabs, there is no intersection.
	//If there is, it will intersect at two places (giving two t's for the ray equation:
	//r = origin + (t * direction). If the minimum of these is larger than than the 
	//maximum, then there is no overlap (technically it means the quadratic solved to
	//obtain t has complex "imaginary" numbers as a solution, which we don't want)
	// 
	FFLOAT interpMin = -FLT_MAX;
	FFLOAT interpMax = FLT_MAX;

	for(BIGINT slab(0); slab < 3; ++slab)
	{
		if(fabsf(r.GetDirection()[slab]) < FLT_EPSILON)
		{
			//ray is parallel to the slab, no intersection if the ray is
			//not lying within slab
			if(r.GetOrigin()[slab] < minimum[slab] || r.GetOrigin()[slab] > maximum[slab])
			{
				return false;
			}
		} 
		else
		{
			FFLOAT invDir = 1.0f / r.GetDirection()[slab];

			FFLOAT t1 = (minimum[slab] - r.GetOrigin()[slab]) * invDir;
			FFLOAT t2 = (maximum[slab] - r.GetOrigin()[slab]) * invDir;

			//t1 is the near slab intersection, so it should be swapped with t2
			//if this condition is true
			if(t1 > t2)
			{
				FFLOAT temp = t1;
				t1 = t2;
				t2 = temp;
			}

			interpMin = interpMin > t1 ? interpMin : t1;
			interpMax = interpMax < t2 ? interpMax : t2;

			if(interpMin > interpMax)
			{
				return false; //no slab intersection if tMin > tMax
			}
		}
	}
	//Store intersection point in ray
	r.SetIntersectionPoint(r(interpMin));
	return true;
}

//Create lines to be able to render the bounding box
VVOID AABBMinMax3D::CreateBounds(ID3D10Device* device, OcelotCamera* cam, FFLOAT offset)
{
	// compute every point of the cube.
	FFLOAT minX = minimum.getX();
	FFLOAT minY = minimum.getY();
	FFLOAT minZ = minimum.getZ();
	FFLOAT maxX = maximum.getX();
	FFLOAT maxY = maximum.getY();
	FFLOAT maxZ = maximum.getZ();

	m_fRadius = 1.0f + offset;

	// face with minimum as leftBottomMin
	Vector3 rightBottomMin(maxX + offset, minY - offset, minZ - offset);
	Vector3 rightTopMin(maxX + offset, maxY + offset, minZ - offset);
	Vector3 leftTopMin(minX - offset, maxY + offset, minZ - offset);

	// face with maximum as rightTopMax
	Vector3 rightBottomMax(maxX + offset, minY - offset, maxZ + offset);
	Vector3 leftTopMax(minX - offset, maxY + offset, maxZ + offset);
	Vector3 leftBottomMax(minX - offset, minY - offset, maxZ + offset);

	// front face
	LineInfo lInfo(device, cam, L"", L"..\\Effects\\Line.fx", minimum - offset, rightBottomMin);
	Line* line1 = new Line(lInfo);
	lInfo.SetFrom(rightBottomMin);
	lInfo.SetTo(rightTopMin); 
	Line* line2 = new Line(lInfo);
	lInfo.SetFrom(rightTopMin);
	lInfo.SetTo(leftTopMin);
	Line* line3 = new Line(lInfo);
	lInfo.SetFrom(leftTopMin);
	lInfo.SetTo(minimum - offset);
	Line* line4 = new Line(lInfo);

	// right face
	lInfo.SetFrom(rightBottomMin);
	lInfo.SetTo(rightBottomMax);
	Line* line5 = new Line(lInfo);
	lInfo.SetFrom(rightBottomMax);
	lInfo.SetTo(maximum + offset);
	Line* line6 = new Line(lInfo);
	lInfo.SetFrom(maximum + offset);
	lInfo.SetTo(rightTopMin);
	Line* line7 = new Line(lInfo);

	// left face
	lInfo.SetFrom(minimum - offset);
	lInfo.SetTo(leftBottomMax);
	Line* line8 = new Line(lInfo);
	lInfo.SetFrom(leftBottomMax);
	lInfo.SetTo(leftTopMax);
	Line* line9 = new Line(lInfo);
	lInfo.SetFrom(leftTopMax);
	lInfo.SetTo(leftTopMin);
	Line* line10 = new Line(lInfo);

	// back face
	lInfo.SetFrom(maximum + offset);
	lInfo.SetTo(leftTopMax);
	Line* line11 = new Line(lInfo);
	lInfo.SetFrom(rightBottomMax);
	lInfo.SetTo(leftBottomMax);
	Line* line12 = new Line(lInfo);

	this->mBounds.push_back(line1);
	this->mBounds.push_back(line2);
	this->mBounds.push_back(line3);
	this->mBounds.push_back(line4);
	this->mBounds.push_back(line5);
	this->mBounds.push_back(line6);
	this->mBounds.push_back(line7);
	this->mBounds.push_back(line8);
	this->mBounds.push_back(line9);
	this->mBounds.push_back(line10);
	this->mBounds.push_back(line11);
	this->mBounds.push_back(line12);

	for
		( UBIGINT lCurrLine = 0; lCurrLine < (UBIGINT)this->mBounds.size(); lCurrLine++ )
	{
		this->mBounds[lCurrLine]->createInstance();
	}
}

//Update the lines position
VVOID AABBMinMax3D::Update(FFLOAT deltaTime)
{
	for
		( UBIGINT lCurrLine = 0; lCurrLine < (UBIGINT)this->mBounds.size(); lCurrLine++ )
	{
		// update the world regarding to its master mesh (yes master !!!)
		this->mBounds[lCurrLine]->SetWorld(this->m_matWorld);
		this->mBounds[lCurrLine]->update(deltaTime);
	}
}

//Render the lines
VVOID AABBMinMax3D::RenderBounds()
{
	for
		( UBIGINT lCurrLine = 0; lCurrLine < (UBIGINT)this->mBounds.size(); lCurrLine++ )
	{
		this->mBounds[lCurrLine]->renderMesh();
	}
}

//Constructor
AABBMinExtent3D::AABBMinExtent3D()
	: minimum(0.0f,0.0f,0.0f)
{
	extents[0] = 0.0f;
	extents[1] = 0.0f;
	extents[2] = 0.0f;
}

//Constructor
AABBMinExtent3D::AABBMinExtent3D(const Vector3 &min, const FFLOAT extentX, const FFLOAT extentY, const FFLOAT extentZ)
	: minimum(min)
{
	extents[0] = extentX;
	extents[1] = extentY;
	extents[2] = extentZ;
}

//Constructor
AABBMinExtent3D::AABBMinExtent3D(const FFLOAT minX, const FFLOAT minY, const FFLOAT minZ, const FFLOAT extentX, const FFLOAT extentY, const FFLOAT extentZ)
	: minimum(minX,minY,minZ)
{
	extents[0] = extentX;
	extents[1] = extentY;
	extents[2] = extentZ;
}

//Intersection test
BBOOL AABBMinExtent3D::Intersects(const IAABBMinExtent3D *b) const
{
	BIGINT x = 0, y = 1, z = 2;
	//Tests for overlap on each axis relative to each extent

	//X-axis
	FFLOAT signum = minimum[x] - b->GetMinimum()[x];
	if(signum > b->GetExtentX() || -signum > GetExtentX())
	{
		return false;
	}

	//Y-axis
	signum = minimum[y] - b->GetMinimum()[y];
	if(signum > b->GetExtentY() || -signum > GetExtentY())
	{
		return false;
	}
	
	//Z-axis
	signum = minimum[z] - b->GetMinimum()[z];
	if(signum > b->GetExtentZ() || -signum > GetExtentZ())
	{
		return false;
	}

	//if we reach here, there is overlap on each axis and hence the
	//boxes are intersecting
	return true;
}

//Constructor
AABBCentreRadius3D::AABBCentreRadius3D()
	: centre(0.0f, 0.0f, 0.0f)
{
	radii[0] = 0.0f;
	radii[1] = 0.0f;
	radii[2] = 0.0f;
}

//Constructor
AABBCentreRadius3D::AABBCentreRadius3D(const Vector3 &_centre, const FFLOAT xRadius, const FFLOAT yRadius, const FFLOAT zRadius)
	: centre(_centre)
{
	radii[0] = xRadius;
	radii[1] = yRadius;
	radii[2] = zRadius;
}

//Constructor
AABBCentreRadius3D::AABBCentreRadius3D(const FFLOAT centreX, const FFLOAT centreY, const FFLOAT centreZ, const FFLOAT xRadius, const FFLOAT yRadius, const FFLOAT zRadius)
	: centre(centreX, centreY, centreZ)
{
	radii[0] = xRadius;
	radii[1] = yRadius;
	radii[2] = zRadius;
}

//Intersection test
BBOOL AABBCentreRadius3D::Intersects(const IAABBCentreRadius3D *b)
{
	BIGINT x = 0, y = 1, z = 2;
	//On each axis, test for overlap by checking the difference between their
	//positions against the sum of their radii. Very similar to a sphere test!
	//Although, because its not a sphere, you have to check each axis at a time
	//and not do just one check (like you would with a sphere)

	//X-axis
	if(fabsf(centre[x] - b->GetCentre()[x]) > (GetXRadius() + b->GetXRadius()))
	{
		return false;
	}

	//Y-axis
	if(fabsf(centre[y] - b->GetCentre()[y]) > (GetYRadius() + b->GetYRadius()))
	{
		return false;
	}

	//Z-axis
	if(fabsf(centre[z] - b->GetCentre()[z]) > (GetZRadius() + b->GetZRadius()))
	{
		return false;
	}

	//overlap on all axes hence they are intersecting
	return true;
}