#ifndef IAABBCENTRERADIUS3D_H
#define IAABBCENTRERADIUS3D_H

#include "../Maths/Vector3.h"

/*
 * The interface used by all 3D aabbs (represented by a centre position and
 * 3 radial half-widths) used in the Ocelot engine. The description of the
 * class itself:
 * A 3D axis aligned bounding box represented by a centre point and 
   3 radius values which denote how far from the centre each axis extends.
   The 3 radius values are also known as half-widths
 */
namespace Ocelot
{
	class IAABBCentreRadius3D
	{
	public:
		//Intersection test
		virtual BBOOL Intersects(const IAABBCentreRadius3D *b) = 0;

		//Setters/getters for variables
		virtual Vector3 GetCentre() const = 0;
		virtual VVOID SetCentre(const Vector3 &_centre) = 0;
		
		virtual FFLOAT GetXRadius() const = 0;
		virtual FFLOAT GetYRadius() const = 0;
		virtual FFLOAT GetZRadius() const = 0;
		virtual VVOID SetXRadius(const FFLOAT xRadius) = 0;
		virtual VVOID SetYRadius(const FFLOAT yRadius) = 0;
		virtual VVOID SetZRadius(const FFLOAT zRadius) = 0;
		virtual VVOID SetRadii(const FFLOAT xRadius, const FFLOAT yRadius, const FFLOAT zRadius) = 0;
	};
}

#endif