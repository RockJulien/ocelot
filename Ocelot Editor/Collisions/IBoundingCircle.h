#ifndef IBOUNDINGCIRCLE_H
#define IBOUNDINGCIRCLE_H

#include "../Maths/Vector2.h"

/*
 * The interface used by all bounding circles in the ocelot engine.
 * Bounding circles themselves can be described as:
 * A bounding circle! It is represented by a 2D centre position and a radius
 */
namespace Ocelot
{
	class IBoundingCircle
	{
	public:
		//Intersection test
		virtual BBOOL Intersects(const IBoundingCircle *b) const = 0;

		//Setters/getters for variables
		virtual  Vector2 GetCentre() const = 0;
		virtual  VVOID SetCentre(const Vector2 &_centre) = 0;

		virtual  FFLOAT GetRadius() const = 0;
		virtual  VVOID SetRadius(const FFLOAT _radius) = 0;
	};
}

#endif