#ifndef IOBB3D_H
#define IOBB3D_H

#include "../Maths/Vector3.h"

/*
 * The interface used by all 3D OBBs in the Ocelot engine.
 * The OBB class itself should be described as:
 * A 3D oriented bounding box. unlike an aabb, the obb rotates with
 * the object it surrounds. The intersection test is more expensive but
 * the tighter fit leads to fewer false positives. It is represented by
 * a centre point and 3 extent values along each axis stemming from the centre
 */
namespace Ocelot
{
	class IOBB3D
	{
	public:
		//Intersection test
		virtual BBOOL Intersects(const IOBB3D *b) const = 0;

		//Setters/getters for variables
		virtual Vector3 GetCentre() const = 0;
		virtual VVOID SetCentre(const Vector3 &_centre) = 0;

		virtual Vector3 GetLocalX() const = 0;
		virtual VVOID SetLocalX(const Vector3 &uX) = 0;

		virtual Vector3 GetLocalY() const = 0;
		virtual VVOID SetLocalY(const Vector3 &uY) = 0;

		virtual Vector3 GetLocalZ() const = 0;
		virtual VVOID SetLocalZ(const Vector3 &uZ) = 0;

		virtual Vector3 GetLocalAxis(const BIGINT index) = 0;

		virtual VVOID SetExtents(const Vector3 &_extents) = 0;
		virtual Vector3 GetExtents() const = 0;
	};
}

#endif