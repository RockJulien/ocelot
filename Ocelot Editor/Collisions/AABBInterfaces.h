#ifndef AABBINTERFACES_H
#define AABBINTERFACES_H

/*
 * A small utility file that includes all AABB related interfaces so that each 
 * page which requires the AABB stuff won't have a massive clutter of #includes
 * just this one.
 */

#include "IAABBCentreRadius2D.h"
#include "IAABBCentreRadius3D.h"
#include "IAABBMinExtend2D.h"
#include "IAABBMinExtent3D.h"
#include "IAABBMinMax2D.h"
#include "IRayIntersectable.h"

#endif