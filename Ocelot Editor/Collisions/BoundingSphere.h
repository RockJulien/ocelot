#ifndef BOUNDINGSPHERE_H
#define BOUNDINGSPHERE_H

#include "../Maths/Vector3.h"
#include "../Geometry/Circle.h"
#include "../DataStructures/MyLinkedList.h"
#include "IRayIntersectable.h"
#include "IBoundingCircle.h"

namespace Ocelot
{
	/*
	 *	A bounding sphere used as a bounding volume for many meshes as the
	 *	intersection test is very cheap. Is one of the bounding volumes used
	 *	in picking and hence implements the IRayIntersectable interface
	 */
	class BoundingSphere : public IRayIntersectable
	{
	public:
		//Constructors
		BoundingSphere();
		BoundingSphere(const Vector3 &_centre, const FFLOAT _radius);
		BoundingSphere(const FFLOAT centreX, const FFLOAT centreY, const FFLOAT centreZ, const FFLOAT _radius);

		//Intersection tests
		BBOOL Intersects(const BoundingSphere &b) const;
		virtual BBOOL Intersects(const Ray& r) const; //Determines whether it intersects a ray
		virtual BBOOL IntersectsAndFindPoint(Ray& r) const; //Determines intersection and also point of intersection (available via Ray::GetIntersectionPoint())

		//Create lines to render bounding volume
		VVOID CreateBounds(ID3D10Device* device, OcelotCamera* cam, FFLOAT offset = 0.2f);
		//Update position of bounding volume
		VVOID Update(FFLOAT deltaTime);
		//Render bounding volume
		VVOID RenderBounds();

		//Setters/getters for variables
		inline Vector3 GetCentre() const { return sphereCentre; }
		inline VVOID SetCentre(const Vector3 &_centre) { sphereCentre = _centre; }

		inline FFLOAT GetRadius() const { return sphereRadius; }
		inline VVOID SetRadius(const FFLOAT _radius) { sphereRadius = _radius; }

	private:
		Vector3 sphereCentre; //Centre position of sphere
		FFLOAT sphereRadius; //Radius of sphere
		std::vector<Circle*> mBounds; //List of lines to render bounding volume
	};

	/*
	 * A bounding circle! It is represented by a 2D centre position and a radius
	 */
	class BoundingCircle : public IBoundingCircle
	{
	public:
		//Constructors
		BoundingCircle();
		BoundingCircle(const Vector2 &_centre, const FFLOAT _radius);
		BoundingCircle(const FFLOAT centreX, const FFLOAT centreY, const FFLOAT _radius);

		//Intersection test
		virtual BBOOL Intersects(const IBoundingCircle *b) const;

		//Setters/getters for variables
		virtual inline Vector2 GetCentre() const { return circleCentre; }
		virtual inline VVOID SetCentre(const Vector2 &_centre) { circleCentre = _centre; }

		virtual inline FFLOAT GetRadius() const { return circleRadius; }
		virtual inline VVOID SetRadius(const FFLOAT _radius) { circleRadius = _radius; }

	private:
		Vector2 circleCentre; //Centre position of the circle
		FFLOAT circleRadius; //radius of the circle
	};
}

#endif