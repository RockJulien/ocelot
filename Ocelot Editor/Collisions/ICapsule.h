#ifndef ICAPSULE_H
#define ICAPSULE_H

#include "BoundingSphere.h"

/*
 * The interface used by all capsule (sphere swept volumes) bounding
 * volumes in Ocelot engine. A capsule itself can be described as:
 * A capsule shaped bounding volume. Useful for a cheap bounding representation
 * of humans. It is represented by a start and end point and a radius to 
 * represent the size of the sphere swept from the start point to the end
 * point to make the capsule
 */
namespace Ocelot
{
	class ICapsule
	{
	public:
		//Intersection tests
		virtual BBOOL Intersects(const BoundingSphere &b) const = 0;
		virtual BBOOL Intersects(const ICapsule &b) const = 0;

		//Setters/getters for variables
		virtual Vector3 GetStart() const = 0;
		virtual VVOID SetStart(const Vector3 &_start) = 0;

		virtual Vector3 GetEnd() const = 0;
		virtual VVOID SetEnd(const Vector3 &_end) = 0;

		virtual FFLOAT GetRadius() const = 0;
		virtual VVOID SetRadius(const FFLOAT _radius) = 0;
	};
}

#endif