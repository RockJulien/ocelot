#ifndef DEF_SIMPLEWINDOW_H
#define DEF_SIMPLEWINDOW_H

#include "DirectXAPI\DXOcelotApp.h"
#include "Geometry\CubeMap.h"
#include "Geometry\OcelotLight.h"
#include "Geometry\OcelotFog.h"
#include "Camera\OcelotCameraFactory.h"
#include "Particle System/ParticleSystem.h"
#include "XML/OcelotRapidXML.h"
#include "Particle System/Lightning.h"
#include "Physics/ParticleForceGenerators.h"
#include "Physics/ParticleContacts.h"
#include <fstream>
#include "AudioManager\AudioManager.h"
#include "Particle System\ParticleEffectsFactory.h"

using namespace Ocelot;

BIGINT WINAPI WinMain(HINSTANCE hInstance,
					  HINSTANCE hPrevInstance,  // useless
					  LPSTR lpCmdLine,          // useless
					  BIGINT nShowCmd);

class SimpleWindow : public DXOcelotApp
{
private:

	// Attributes
	Vector2						   mOldMousePos;
	IOcelotMesh*				   mSelectedMesh;
	std::vector<IOcelotMesh*>	   mMeshes;
	FFLOAT						   mElapsedTime;
	OcelotCamera*				   mCamera;
	IOcelotCameraController*	   mController;
	CubeMap*					   mCubeMap;
	TriPlanarBumpMaterial*         mTerrainMaterial;
	OcelotLight*			       mLight; // Global light.
	OcelotFog*				       mFog;

	// wireframe mode
	ID3D10RasterizerState*	       mWireFrameMode;
	BBOOL					       mIsWireFrame;

	std::vector<IParticleSystem*>  mParticleSystems;
	Lightning*				       mLightning;
	
	IParticleForceRegistry*        mForceReg;
	IParticleForceGenerator*       mGravGen;
	IParticleContactResolver*      mContactResolver;
	std::vector<IParticleContact*> mContacts;

	BIGINT						   mTerrainIndex;

public:

	// Constructor & Destructor
	SimpleWindow();
	~SimpleWindow();

	// Methods
	BIGINT  run();
	VVOID   update(FFLOAT deltaTime);
	VVOID   renderFrame();
	VVOID   initApp(HINSTANCE hInstance, BIGINT nShowCmd, LPWSTR windowName);
	VVOID   handleInput();
	VVOID   clean3DDevice();
	VVOID   resize();
	IOcelotMesh* underMouse(Vector2 curs);
	LRESULT AppProc(UBIGINT message, WPARAM wParam, LPARAM lParam);

	// XML Load data methods
	VVOID resolveContacts(const FFLOAT deltaTime);
	VVOID LoadXMLData();
	VVOID LoadXMLMeshData(rapidxml::xml_document<>& doc);
	VVOID LoadXMLCameraData(rapidxml::xml_document<>& doc);
	VVOID LoadXMLParticleSystemData(rapidxml::xml_document<>& doc);

	// Accessors
};

#endif