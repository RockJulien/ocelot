#include "SimpleWindow.h"
#include <ctime>
#include "Maths\Vector4.h"
#include "Utilities\PickPair.h"
#include "Collisions\AABB.h"
#include "Geometry\Cylinder.h"
#include "Geometry\Sphere.h"
#include "Geometry\Terrain.h"
#include "Geometry\Cube.h"
#include "Geometry\PyraCylinder.h"

using namespace std;

BIGINT WINAPI WinMain(HINSTANCE hInstance,
					  HINSTANCE hPrevInstance,  // useless
					  LPSTR lpCmdLine,          // useless
					  BIGINT nShowCmd)
{

	SimpleWindow app;
	app.initApp(hInstance, SW_HIDE, L"The Ocelot Show");


	return app.run();
}

SimpleWindow::SimpleWindow() : 
DXOcelotApp(), mCamera(NULL), mController(NULL), mSelectedMesh(NULL), mTerrainMaterial(NULL),
mWireFrameMode(NULL), mIsWireFrame(false), mElapsedTime(0.0f)
{
	srand(static_cast<UBIGINT>(time(0)));
}

SimpleWindow::~SimpleWindow()
{
	this->clean3DDevice();

	DeletePointer(this->mLightning);
	DeletePointer(this->mGravGen);

	if 
		( this->mMeshes.size() > 0 )
	{
		vector<IOcelotMesh*>::iterator lIter = this->mMeshes.begin();
		for
			( ; lIter != this->mMeshes.end(); lIter++ )
		{
			DeletePointer((*lIter))
		}
		this->mMeshes.clear();
	}

	if 
		( this->mParticleSystems.size() != 0 )
	{
		vector<IParticleSystem*>::iterator lIter = this->mParticleSystems.begin();
		for 
			( ; lIter != this->mParticleSystems.end(); lIter++ )
		{
			DeletePointer((*lIter))
		}
		this->mParticleSystems.clear();
	}

	AudioMng->release();
}

BIGINT  SimpleWindow::run()
{
	// the main loop
	MSG lMsg = {0};

	this->m_timer.resetTimer();

	while
		( lMsg.message != WM_QUIT )
	{
		if 
			( PeekMessage( &lMsg, NULL, 0, 0, PM_REMOVE ) )
		{												  

			// translate keystroke messages into the right format
			TranslateMessage( &lMsg );

			// send the message to the WindowProc
			DispatchMessage( &lMsg );
		}
		else
		{
			if
				( this->m_bPaused )
			{
				Sleep(50);
			}
			else
			{
				this->m_timer.tick();
				this->update( this->m_timer.getDeltaTime() );
				this->renderFrame();
			}
		}
	}

	// clean up DirectX COM
	this->clean3DDevice();

	return lMsg.wParam;
}

VVOID SimpleWindow::initApp(HINSTANCE pInstance, BIGINT pShowCmd, LPWSTR pWindowName)
{
	DXOcelotApp::initApp(pInstance, pShowCmd, pWindowName);

	// init the audio device.
	// for now, add whatever song you wnat here. just the short name
	// by putting them first in the Resources folder.
	AudioMng->initialize(m_hAppInstance);

	// init the Resource Manager
	ResFactory->SetDevice( this->m_device );
	ResFactory->CreateTextures();

	// mode wireframe
	D3D10_RASTERIZER_DESC rsDesc;
	ZeroMemory(&rsDesc, sizeof(D3D10_RASTERIZER_DESC));
	rsDesc.FillMode = D3D10_FILL_WIREFRAME;
	rsDesc.CullMode = D3D10_CULL_BACK;
    rsDesc.FrontCounterClockwise = false;
	m_device->CreateRasterizerState(&rsDesc, &mWireFrameMode);

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3(-202.0f, -10.0f, -150.0f),
							   Vector3( 0.0f, 0.0f, 1.0f ),
							   (FFLOAT)m_iWindowWidth / m_iWindowHeight,
							   1000.0f );
	lCamInfo.SetNear( 1.0f );
	this->mCamera = new OcelotCamera( lCamInfo );

	// And create the controller
	this->mController = ControlFact->createController( this->mCamera, CAMERA_CONTROLLER_FLYER );

	WSTRING lDefTextPath[3];
	lDefTextPath[0] = L"Dirt.jpg";
	lDefTextPath[1] = L"Grass.jpg";
	lDefTextPath[2] = L"Rock.jpg";
	// Create common textures for saving memory (will be the same).
	this->mTerrainMaterial = new TriPlanarBumpMaterial();
	ResFactory->LoadTriPlanarMaterial( lDefTextPath, *this->mTerrainMaterial );

	// Create the light  
	mLight = new OcelotLight(Vector3(-300.0f, 100.0f, -180.0f), // eye    
							 Vector3(0.0f, 0.0f, 1.0f),   // dir    
							 Vector3(1.0f, 0.0f, 0.0f),   // atten  
							 OcelotColor::DARKGREY,		  // ambient   
							 OcelotColor::LIGHTGREY,      // diffuse  
							 OcelotColor::GREY,			  // spec  
							 1000.0f,					  // range 
							 64.0f,						  // spotPow 
							 1.0f);						  // specPow
	
	// Create the fog.
	mFog = new OcelotFog(5.0f, 
						 140.0f);

	LoadXMLData();

	CubeMapInfo infoCM(this->m_device, 
					   this->mCamera, 
					   5000.0f, 
					   L"snowMountainMap.dds", 
					   L"..\\Effects\\CubeMap.fx");
	this->mCubeMap = new CubeMap(infoCM);
	this->mMeshes.push_back(this->mCubeMap);
	
	TerrainInfo TI( this->m_device,
					this->mCamera,
					L"..\\Effects\\TriPlanarBumpMapping.fx",
					this->mLight,
					this->mFog,
					this->mTerrainMaterial,
					Vector3(0.0f),
					513,
					513,
					513,
					513 );
	
	Terrain* HM = new Terrain(TI);
	HM->setHeightMapName(L"coastMountain513.raw");
	HM->setHeightMapUse(true);

	this->mMeshes.push_back(HM);
	
	// init default meshes extracted from file
	for 
		( UBIGINT currMesh = 0; currMesh < (UBIGINT)this->mMeshes.size(); currMesh++ )
	{
		this->mMeshes[currMesh]->createInstance(); // template design pattern in action!!! call right methods of every mesh from the abstract class.
	}

	LightningInfo lInfo(this->m_device, 
						this->mCamera, 
						L"..\\Effects\\LightningEffect.fx", 
						ResFactory->GetResource(L"lbolt.dds"), 
						this->mMeshes[1]->Position(), 
						this->mMeshes[4]->Position(), 
						OcelotColor::CYAN, 
						3, 
						10.0f, 
						0.05f);

	this->mLightning = new Lightning(lInfo);
	this->mLightning->createInstance();

	this->mGravGen = new ParticleGravityGenerator(Vector3(0.0f, -9.81f, 0.0f));
	this->mForceReg = PForceRegistry;

	this->mContactResolver = PContactResolver;
	this->mContactResolver->setNumIterations(15);

	for
		( BIGINT i = 0; i < this->mMeshes.size(); i++ )
	{
		if
			( this->mMeshes[i]->UsesPhysics() )
		{
			this->mForceReg->Add( this->mMeshes[i]->GetParticle(), this->mGravGen );
		}
	}

	// show the window
	ShowWindow(m_mainWindowInstance, SW_SHOW);

	// trigger song for the app.
	HR(AudioMng->createSound(L"heli.wav", true, true));
	HR(AudioMng->createSound(L"Techno_1.wav", false, false));
}

IOcelotMesh* SimpleWindow::underMouse(Vector2 curs)
{
	// algorithm which determine which mesh is under 
	// the cursor of the mouse by unprojecting.
	Matrix4x4 lMatProj = mCamera->Proj();
	Matrix4x4 lMatView = mCamera->View();

	Vector3 lEye = this->mCamera->Info().Eye();

	// determine the ray thanks to screen position
	// in view space.
	FFLOAT lDirX = (+2.0f * curs.getX() / m_iWindowWidth  - 1.0f) / lMatProj.get_11();
	FFLOAT lDirY = (-2.0f * curs.getY() / m_iWindowHeight + 1.0f) / lMatProj.get_22();

	Vector3 lOrigine( 0.0f, 0.0f, 0.0f );
	Vector3 lRayDir( lDirX, lDirY, 1.0f );

	// Get the inverse view for transforming mesh world to local.
	Matrix4x4 lMatViewInverse;
	lMatView.Matrix4x4Inverse( lMatViewInverse );

	const IOcelotMesh* lClosest = NULL;
	// transform from world space to local space meshes
	if
		( this->mMeshes.size() != 0 )
	{
		std::vector<PickPair> lMatches;
		for
			( UBIGINT lCurrMesh = 0; lCurrMesh < (UBIGINT)this->mMeshes.size() - 1; lCurrMesh++ )
		{
			IOcelotMesh* lCurrMeshInstance = this->mMeshes[lCurrMesh];

			Matrix4x4 lCurrMeshInvWorld;
			const Matrix4x4* lCurrMeshWorld = lCurrMeshInstance->World();
			lCurrMeshWorld->Matrix4x4Inverse( lCurrMeshInvWorld );

			Matrix4x4 lCurrMeshLocal;
			lMatViewInverse.Matrix4x4Mutliply( lCurrMeshLocal, lCurrMeshInvWorld );

			// apply the local world to origine and rayDir vec.
			Vector3 lLocalOrigine = Vector3::Vec4ToVec3( lOrigine.Vec3VecMatrixProduct( lCurrMeshLocal ) );
			Vector3 lLocalRayDir = Vector3::Vec4ToVec3(lRayDir.Vec3VecMatrixTransformNormal(lCurrMeshLocal));
			lLocalRayDir.Vec3Normalise();

			Ray lRay( lLocalOrigine, lLocalRayDir );

			// now determine which mesh this ray intersect
			// and return it if exists.
			const IRayIntersectable* lBounds = lCurrMeshInstance->LocalBounds();
			if
				( lBounds != NULL &&
				  lBounds->IntersectsAndFindPoint( lRay ) )
			{
				lMatches.push_back( PickPair( lCurrMeshInstance, lRay.GetIntersectionPoint() ) );
			}
		}

		FFLOAT lMinDistance = MaxFloat;
		// Retrieve the closest from the camera and return it.
		std::vector<PickPair>::const_iterator lIter = lMatches.begin();
		for 
			( ; lIter != lMatches.end(); lIter++ )
		{
			const IOcelotMesh* lMesh = lIter->Picked();
			Vector3 lPosition = lIter->IntersectedPoint();
			FFLOAT lCurrDistance = lEye.Vec3DistanceBetween( lPosition );
			if 
				( lMinDistance > lCurrDistance )
			{
				lMinDistance = lCurrDistance;
				lClosest = lMesh;
			}
		}
	}

	return const_cast<IOcelotMesh*>( lClosest );
}

VVOID SimpleWindow::update(FFLOAT deltaTime)
{
	DXOcelotApp::update(deltaTime);
	this->mElapsedTime = deltaTime;

	// update the user impact
	this->handleInput();

	// update the audio device
	HR(AudioMng->update(deltaTime));

	// refresh frustum planes.
	this->mCamera->computeFrustumPlane();

	// update the camera once for all
	this->mController->update( deltaTime );

	// update meshes
	for
		( UBIGINT lCurrMesh = 0; lCurrMesh < (UBIGINT)this->mMeshes.size(); lCurrMesh++ )
	{
		IOcelotMesh* lCurr = this->mMeshes[lCurrMesh];
		const AABBMinMax3D* lBounds = dynamic_cast<const AABBMinMax3D*>( lCurr->LocalBounds() );
		if 
			( lBounds != NULL )
		{
			Vector3 lPosition = lCurr->Position();
			FFLOAT lRadius = lBounds->GetRadius();
			//lCurr->SetVisibility( this->mCamera->cullMesh(lPosition.getX(), lPosition.getY(), lPosition.getZ(), lRadius ) );
		}

		lCurr->update(deltaTime);
	}

	for
		( BIGINT i = 0; i < this->mParticleSystems.size(); i++ )
	{
		IParticleSystem* lCurrentPS = this->mParticleSystems[i];
		BIGINT lIndex = lCurrentPS->GetMeshIndex();
		lCurrentPS->SetEmitterPosition((lIndex == -1 ? mCamera->Info().Eye() : this->mMeshes[lIndex]->Position()) + lCurrentPS->GetEmitterOffset());
		lCurrentPS->Update(deltaTime, this->m_timer.getGameTime());
	}

	this->mLightning->Info().SetStartpoint( this->mMeshes[8]->Position() );
	this->mLightning->Info().SetEndpoint( this->mMeshes[rand() % (this->mMeshes.size() - 1)]->Position() );
	this->mLightning->SetTime( this->m_timer.getGameTime() );
	this->mLightning->update(deltaTime);
	
	this->resolveContacts(deltaTime);
	
	this->mForceReg->UpdateForces( deltaTime);
}

VVOID SimpleWindow::resolveContacts(const FFLOAT deltaTime)
{
	this->mContacts.clear();
	for 
		( BIGINT i = 0; i < this->mMeshes.size() - 1; ++i )
	{
		IOcelotMesh* objA = this->mMeshes[i];
		for(BIGINT j = i + 1; j < this->mMeshes.size(); ++j)
		{
			IOcelotMesh* objB = this->mMeshes[j];

			if(!objA->UsesPhysics() || !objB->UsesPhysics())
			{
				continue;
			}

			/*if 
				( !objA->GetParticle()->HasFiniteMass() || 
				  !objB->GetParticle()->HasFiniteMass()   )
			{
				continue;
			}*/

			/*AABBMinMax3D boundsA(objA->GetMinW(), objA->GetMaxW());
			AABBMinMax3D boundsB(objB->GetMinW(), objB->GetMaxW());*/

			const AABBMinMax3D* lBoundsA = dynamic_cast<const AABBMinMax3D*>(objA->WorldBounds());
			const AABBMinMax3D* lBoundsB = dynamic_cast<const AABBMinMax3D*>(objB->WorldBounds());

			if 
				( lBoundsA != NULL && 
				  lBoundsB != NULL &&
				  lBoundsA->Intersects( lBoundsB ) )
			{
				Vector3 contactNormal = Vector3(0.0f, 1.0f, 0.0f);

				this->mContacts.push_back(new ParticleContact(objA->GetParticle(), objB->GetParticle(), 0.2f, contactNormal, lBoundsA->CalculatePenetration(lBoundsB) * deltaTime));
			}
		}
	}
	this->mContactResolver->ResolveContacts(this->mContacts, deltaTime);
}

VVOID SimpleWindow::renderFrame()
{
	// render basic DirectX elements
	DXOcelotApp::renderFrame();

	if(InMng->isPressedAndLocked(OCELOT_W))
		this->mIsWireFrame = !this->mIsWireFrame;

	m_device->OMSetDepthStencilState(0, 0);
	FFLOAT blendFactor[] = {0.0f, 0.0f, 0.0f, 0.0f};
	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	if(this->mIsWireFrame) m_device->RSSetState(this->mWireFrameMode);

	for(UBIGINT currMesh = 0; currMesh < (UBIGINT)this->mMeshes.size(); currMesh++)
	{
		this->mMeshes[currMesh]->renderMesh();
	}

	this->mLightning->renderMesh();

	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	
	for(BIGINT i = 0; i < this->mParticleSystems.size(); ++i)
		this->mParticleSystems[i]->Render();

	// draw FPS
	/*
	RECT R = {10,10,0,0};
	m_device->RSSetState(0);
	m_font->DrawTextW(  0,												  // no sprite
						m_frameStat.c_str(),							  // string to render
						-1,												  // default value
						&R,												  // size of the rect
						DT_NOCLIP,                                        // do not cull if bigger than rect
						DXColor::OcelotColToDXCol(OcelotColor::WHITE));   // white
	 */
	// display the frame 
	m_swapChain->Present(1,0);                               // swap with the back buffer and sync at the monitors refresh rate
}

VVOID SimpleWindow::resize()
{
	DXOcelotApp::resize();

	// refresh the aspect of the camera if the
	// window is resized.
	if
		( this->mCamera )
	{
		FFLOAT lAspect = (FFLOAT)m_iWindowWidth / m_iWindowHeight;
		OcelotCameraInfo lInfo = this->mCamera->Info();
		lInfo.SetAspect( lAspect );  // aspect ratio
		this->mCamera->SetInfo( lInfo );
	}
}

VVOID SimpleWindow::handleInput()
{
	// handle every input/key pressed.
	// move the mesh
	FFLOAT speed = 2.0f;

	if 
		( this->mController != NULL )
	{
		if 
			( InMng->isPressed( Keyboard, OCELOT_LEFT ) )
		{
			this->mController->move( Direction::LeftSide );
		}
		if
			( InMng->isPressed( Keyboard, OCELOT_RIGHT ) )
		{
			this->mController->move( Direction::RightSide );
		}
		if
			( InMng->isPressed( Keyboard, OCELOT_UP ) )
		{
			this->mController->move( Direction::Forward );
		}
		if
			( InMng->isPressed( Keyboard, OCELOT_DOWN ) )
		{
			this->mController->move( Direction::Backward );
		}
	}

	// move the selected mesh
	if
		( this->mSelectedMesh )
	{
		BBOOL lResetPhysics = false;
		if 
			( this->mSelectedMesh->UsesPhysics() )
		{
			// Stop the physics computation the time we move the entity.
			this->mSelectedMesh->SetPhysicsUse(false);
			lResetPhysics = true;
		}

		IMeshEditor* lEditor = this->mSelectedMesh->StartEdition();
		if(InMng->isPressed(Keyboard, OCELOT_NP6)) 
		{
			lEditor->TranslateX(speed * this->mElapsedTime);
			this->mSelectedMesh->SetPhysicsUse(false);
		}
		else if(InMng->isPressed(Keyboard, OCELOT_NP4))
		{
			lEditor->TranslateX(-speed * this->mElapsedTime);
			this->mSelectedMesh->SetPhysicsUse(false);
		}

		if(InMng->isPressed(Keyboard, OCELOT_NP8)) 
		{
			lEditor->TranslateZ(speed * this->mElapsedTime);
			this->mSelectedMesh->SetPhysicsUse(false);
		}
		else if(InMng->isPressed(Keyboard, OCELOT_NP2))
		{
			lEditor->TranslateZ(-speed * this->mElapsedTime);
			this->mSelectedMesh->SetPhysicsUse(false);
		}

		if(InMng->isPressed(Keyboard, OCELOT_NPPLUS)) 
		{
			lEditor->TranslateY(speed * this->mElapsedTime);
			this->mSelectedMesh->SetPhysicsUse(false);
		} 
		else if(InMng->isPressed(Keyboard, OCELOT_NPMINUS))
		{
			lEditor->TranslateY(-speed * this->mElapsedTime);
			this->mSelectedMesh->SetPhysicsUse(false);
		}

		if(InMng->isPressed(Keyboard, OCELOT_X))
		{
			lEditor->RotateX(RadToDeg(10.0f * this->mElapsedTime));
		}
		else if(InMng->isPressed(Keyboard, OCELOT_C))
		{
			lEditor->RotateX(-RadToDeg(10.0f * this->mElapsedTime));
		}

		if(InMng->isPressed(Keyboard, OCELOT_Y))
		{
			lEditor->RotateY(RadToDeg(10.0f * this->mElapsedTime));
		}
		else if(InMng->isPressed(Keyboard, OCELOT_U))
		{
			lEditor->RotateY(-RadToDeg(10.0f * this->mElapsedTime));
		}

		if(InMng->isPressed(Keyboard, OCELOT_Z))
		{
			lEditor->RotateZ(RadToDeg(10.0f * this->mElapsedTime));
		}
		else if(InMng->isPressed(Keyboard, OCELOT_E))
		{
			lEditor->RotateZ(-RadToDeg(10.0f * this->mElapsedTime));
		}
		DeletePointer(lEditor);

		// Reset the physics is previously deactivated for edition purpose.
		if 
			( lResetPhysics )
		{
			this->mSelectedMesh->SetPhysicsUse(true);
		}
	}

	if(InMng->isPressed(Keyboard, OCELOT_M))
		HR(AudioMng->startASound(L"DragonfightSkyrim.wav"));

	if(InMng->isPressed(Keyboard, OCELOT_L))
		AudioMng->stopASound(L"DragonfightSkyrim.wav");

	if(InMng->isPressedAndLocked(OCELOT_H))
		HR(AudioMng->startASound(L"heli.wav"));

	if(InMng->isPressedAndLocked(OCELOT_G))
		AudioMng->stopASound(L"heli.wav");
}

VVOID SimpleWindow::clean3DDevice()
{
	DXOcelotApp::clean3DDevice();

	DeletePointer(this->mCamera);
	DeletePointer(this->mController);
	this->mSelectedMesh = NULL;
}

LRESULT SimpleWindow::AppProc(UBIGINT message,WPARAM wParam,LPARAM lParam)
{
	Vector2 mousePosition;

	FFLOAT dx = 0.0f;
	FFLOAT dy = 0.0f;

	switch(message)
	{
		case WM_RBUTTONDOWN:
			if (this->mSelectedMesh) this->mSelectedMesh->HideBound();
			this->mSelectedMesh = underMouse(Vector2((FFLOAT)LOWORD(lParam), (FFLOAT)HIWORD(lParam)));
			if(this->mSelectedMesh) this->mSelectedMesh->ShowBound();
			return 0;
			break;

		case WM_LBUTTONDOWN:
			if(wParam & MK_LBUTTON)
			{
				this->mOldMousePos.x((FFLOAT)LOWORD(lParam));
				this->mOldMousePos.y((FFLOAT)HIWORD(lParam));

				SetCapture(m_mainWindowInstance);
			}
			return 0;
			break;

		case WM_LBUTTONUP:
			ReleaseCapture();
			return 0;
			break;

		case WM_MOUSEMOVE:
			if(wParam & MK_LBUTTON)
			{
				mousePosition.x((FFLOAT)LOWORD(lParam)); 
				mousePosition.y((FFLOAT)HIWORD(lParam)); 

				FFLOAT lToRad = PI / 180.0f;
				// Make each pixel correspond to a quarter of a degree.
				FFLOAT lDx = (0.25f * static_cast<FFLOAT>(mousePosition.getX() - this->mOldMousePos.getX())) * lToRad;
				FFLOAT lDy = (0.25f * static_cast<FFLOAT>(mousePosition.getY() - this->mOldMousePos.getY())) * lToRad;

				// add extra camera controler stuff here
				this->mController->pitch( lDy );
				this->mController->yaw( lDx );
			
				this->mOldMousePos = mousePosition;
			}
			return 0;
	}

	return DXOcelotApp::AppProc(message, wParam, lParam);
}

VVOID SimpleWindow::LoadXMLData()
{
	OcelotXMLFile xmlfile("XML\\OcelotGameData.xml");
	OcelotXMLDoc doc;
	try
	{
		doc.parse<rapidxml::parse_no_data_nodes>(xmlfile.data());
	}
	catch(rapidxml::parse_error e)
	{}

	LoadXMLMeshData(doc);
	LoadXMLCameraData(doc);
	LoadXMLParticleSystemData(doc);
}

VVOID SimpleWindow::LoadXMLMeshData(rapidxml::xml_document<>& doc)
{
	OcelotXMLNode *root = doc.first_node("ocelot");
	OcelotXMLNode *meshesData = root->first_node("meshes");

	BBOOL enabled = atoi(meshesData->first_attribute("enabled")->value()) == 1;
	if
		( !enabled ) 
	{
		return;
	}

	BIGINT count = 0;
	for
		( OcelotXMLNode *currentMesh = meshesData->first_node(); 
	                     currentMesh; 
						 currentMesh = currentMesh->next_sibling() )
	{
		BBOOL enabled = atoi(currentMesh->first_attribute("enabled")->value()) == 1;
		if(!enabled) 
		{
			count++;
			continue;
		}

		string type = currentMesh->first_node()->value();
		if
			( type == "sphere" )
		{
			Vector3 pos     = OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x","y","z");
			FFLOAT radius   = (FFLOAT)atof(currentMesh->first_attribute("radius")->value());
			BIGINT slices   = atoi(currentMesh->first_attribute("slices")->value());
			BIGINT rings    = atoi(currentMesh->first_attribute("rings")->value());
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			SphereInfo SI(this->m_device, 
						  this->mCamera, 
						  fileNameW, 
						  L"..\\Effects\\BumpMapping.fx", 
						  this->mLight, 
						  this->mFog, 
						  pos, 
						  radius, 
						  rings, 
						  slices);
			this->mMeshes.push_back(new Sphere(SI));
			this->mMeshes.back()->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
			count++;
		} 
		else if
			( type == "cube" )
		{
			Vector3 pos     = OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x","y","z");
			Vector3 radius  = OcelotXML::ExtractVector3(currentMesh->first_node("radius"),"x","y","z");
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			CubeInfo CI(this->m_device, 
						this->mCamera, 
						fileNameW, 
						L"..\\Effects\\BumpMapping.fx", 
						this->mLight, 
						this->mFog, 
						pos, 
						radius);
			this->mMeshes.push_back(new Cube(CI));
			this->mMeshes.back()->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
			count++;
		} 
		else if
			( type == "cylinder" )
		{
			Vector3 pos     = OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x","y","z");
			Vector3 scale   = OcelotXML::ExtractVector3(currentMesh->first_node("scale"),"sx","sy","sz");
			FFLOAT radius   = (FFLOAT)atof(currentMesh->first_attribute("radius")->value());
			FFLOAT height   = (FFLOAT)atof(currentMesh->first_attribute("height")->value());
			BIGINT slices   = atoi(currentMesh->first_attribute("slices")->value());
			BIGINT rings    = atoi(currentMesh->first_attribute("rings")->value());
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			CylinderInfo CyI(this->m_device,
							 this->mCamera, 
							 fileNameW, 
							 L"..\\Effects\\BumpMapping.fx", 
							 this->mLight, 
							 this->mFog, 
							 pos, 
							 radius,
							 height, 
							 rings, 
							 slices);
			this->mMeshes.push_back(new Cylinder(CyI));
			this->mMeshes.back()->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
			count++;
		}
		else if
			( type == "pyracylinder" )
		{
			Vector3 pos   = OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x", "y", "z");
			Vector3 scale = OcelotXML::ExtractVector3(currentMesh->first_node("scale"), "sx", "sy", "sz");
			FFLOAT radius = (FFLOAT)atof(currentMesh->first_attribute("radius")->value());
			FFLOAT lBottomRadius = (FFLOAT)atof(currentMesh->first_attribute("bottomRadius")->value());
			FFLOAT height = (FFLOAT)atof(currentMesh->first_attribute("height")->value());
			BIGINT slices = atoi(currentMesh->first_attribute("slices")->value());
			BIGINT rings  = atoi(currentMesh->first_attribute("rings")->value());
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			PyraCylinderInfo CyI(this->m_device,
								 this->mCamera,
								 fileNameW,
								 L"..\\Effects\\BumpMapping.fx",
								 this->mLight,
								 this->mFog,
								 pos,
								 radius,
								 lBottomRadius,
								 height,
								 rings,
								 slices);
			this->mMeshes.push_back(new PyraCylinder(CyI));
			this->mMeshes.back()->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
			count++;
		}
		else if
			( type == "terrain" )
		{
			Vector3 pos		= OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x", "y", "z");
			BIGINT rows     = atoi(currentMesh->first_attribute("rows")->value());
			BIGINT cols     = atoi(currentMesh->first_attribute("cols")->value());
			FFLOAT width    = (FFLOAT)atof(currentMesh->first_attribute("width")->value());
			FFLOAT depth    = (FFLOAT)atof(currentMesh->first_attribute("depth")->value());
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			TerrainInfo TI(this->m_device, 
						   this->mCamera, 
						   L"..\\Effects\\TriPlanarBumpMapping.fx",
						   this->mLight, 
						   this->mFog, 
						   this->mTerrainMaterial, 
						   pos, 
						   rows, 
						   cols, 
						   width, 
						   depth);
			this->mMeshes.push_back(new Terrain(TI));
			this->mMeshes.back()->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
			this->mTerrainIndex = count;
			count++;
		}
	}
}

VVOID SimpleWindow::LoadXMLCameraData(rapidxml::xml_document<>& doc)
{
	OcelotXMLNode *root = doc.first_node("ocelot");
	OcelotXMLNode *camerasData = root->first_node("cameras");

	BBOOL enabled = atoi(camerasData->first_attribute("enabled")->value()) == 1;
	if(!enabled) 
	{
		return;
	}
}

VVOID SimpleWindow::LoadXMLParticleSystemData(rapidxml::xml_document<>& doc)
{
	OcelotXMLNode *root = doc.first_node("ocelot");
	OcelotXMLNode *particleSystemsData = root->first_node("particleSystems");

	BBOOL enabled = atoi(particleSystemsData->first_attribute("enabled")->value()) == 1;
	if(!enabled) 
	{
		return;
	}

	for
		( OcelotXMLNode *currentMesh = particleSystemsData->first_node(); 
	                     currentMesh; 
						 currentMesh = currentMesh->next_sibling())
	{
		BBOOL enabled = atoi(currentMesh->first_attribute("enabled")->value()) == 1;
		if(!enabled) 
		{
			continue;
		}

		ParticleSystemInfo info;

		info.device = m_device;
		info.camera = mCamera;

		info.maxnumparticles = atoi(currentMesh->first_attribute("numParticles")->value());
		info.spread = (FFLOAT)atof(currentMesh->first_attribute("spread")->value());

		std::string fileName = currentMesh->first_attribute("textureFileName")->value();
		std::wstring fileNameW(fileName.begin(), fileName.end());
		info.texture = ResFactory->GetResource( fileNameW );

		std::string typeS = currentMesh->first_node("type")->value();
		BIGINT type;
		if(typeS == "rain")
		{
			type = PE_RAIN;
		} else if(typeS == "snow")
		{
			type = PE_SNOW;
		} else if(typeS == "smoke")
		{
			type = PE_SMOKE;
		} else if(typeS == "fire")
		{
			type = PE_FIRE;
		}

		this->mParticleSystems.push_back(ParticleFXFactory->CreateParticleEffect(ParticleEffectType(type), info));

		IParticleSystem* ps = this->mParticleSystems.back();
		ps->SetMeshIndex(atoi(currentMesh->first_node("emitterPosition")->first_attribute("meshIndex")->value()));
		ps->SetEmitterOffset(OcelotXML::ExtractVector3(currentMesh->first_node("emitterPosition")->first_node("offset"), "x", "y", "z"));
		ps->SetAcceleration(OcelotXML::ExtractVector3(currentMesh->first_node("acceleration"), "x","y","z"));
		ps->SetWindAmplitude(OcelotXML::ExtractVector3(currentMesh->first_node("windForce")->first_node("amplitude"), "x", "y", "z"));
		ps->SetWindPeriodicity(OcelotXML::ExtractVector3(currentMesh->first_node("windForce")->first_node("periodicity"), "x", "y", "z"));
	}
}