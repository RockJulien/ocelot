#include "PickPair.h"

// used namespaces
using namespace Ocelot;

PickPair::PickPair(const IOcelotMesh* pMesh, Vector3 pPoint) :
mPicked(pMesh), mIntersectionPoint(pPoint)
{

}