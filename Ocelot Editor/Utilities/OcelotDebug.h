#ifndef DEF_OCELOTDEBUG_H
#define DEF_OCELOTDEBUG_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   OcelotDebug.h
/   
/  Description: This time, I tried to make a DEbug syntax which
/               could debug my program but need to have my own
/               Type as well like HRESULT which are low level.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

#include <Windows.h>
#include <WinBase.h>

namespace Ocelot
{
	namespace Debug
	{
		#define O_PASSED 1
		#define O_FAIL -1
		#define O_FAILED(Result) { if(Result < 0) return true; return false;}
		#define O_SUCCEED(Result) { if(Result > 0) return true; return false;}
		#define OCELOT_TRY(Result) { BIGINT OcelotResult = (BIGINT)Result; if(FAILED(OcelotResult)) OutputDebugStringW("OcelotResult error !!!");}
	}
}

#endif