#ifndef DEF_PICKPAIR_H
#define DEF_PICKPAIR_H

// Includes
#include "..\Maths\Vector3.h"
#include "..\Geometry\IOcelotMesh.h"

// Namespaces
namespace Ocelot
{
	// Class definition
	class PickPair
	{
	private:

		const IOcelotMesh* mPicked;
		Vector3			   mIntersectionPoint;
		UBIGINT			   mIndex;

	public:

		PickPair(const IOcelotMesh* pMesh, Vector3 pPoint);

		// Getters
		inline const IOcelotMesh* Picked() const { return this->mPicked; };
		inline Vector3			  IntersectedPoint() const { return this->mIntersectionPoint; }
		inline UBIGINT			  Index() const { return this->mIndex; };
		inline VVOID			  SetIndex(UBIGINT pIndex) { this->mIndex = pIndex; };
	};
}

#endif