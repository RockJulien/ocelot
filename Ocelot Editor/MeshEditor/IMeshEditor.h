#ifndef DEF_IMESHEDITOR_H
#define DEF_IMESHEDITOR_H

#include "..\DataStructures\DataStructureUtil.h"
#include "..\Maths\Vector3.h"

namespace Ocelot
{
	class IMeshEditor
	{
	public:

		// Constructor & Destructor
		IMeshEditor() {}
		virtual ~IMeshEditor(){}

		// Methods
		virtual VVOID    RotateX(FFLOAT pAngleInDegree) = 0;
		virtual VVOID    RotateY(FFLOAT pAngleInDegree) = 0;
		virtual VVOID    RotateZ(FFLOAT pAngleInDegree) = 0;
		virtual VVOID    Rotate(const Vector3& pRotation) = 0;
		virtual VVOID    TranslateX(FFLOAT pDiff) = 0;
		virtual VVOID    TranslateY(FFLOAT pDiff) = 0;
		virtual VVOID    TranslateZ(FFLOAT pDiff) = 0;
		virtual VVOID	 rescale(FFLOAT pScaleX, FFLOAT pScaleY, FFLOAT pScaleZ) = 0;
		virtual VVOID	 rescale(const Vector3& pScaling) = 0;

	};
}

#endif