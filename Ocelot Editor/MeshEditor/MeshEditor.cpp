#include "MeshEditor.h"
#include "..\Geometry\AOcelotBaseMesh.h"

// used namespaces
using namespace Ocelot;

MeshEditor::MeshEditor(AOcelotBaseMesh* pMesh) :
mEdited(pMesh)
{

}

MeshEditor::~MeshEditor()
{

}

VVOID    MeshEditor::RotateX(FFLOAT pAngleInDegree)
{
	FFLOAT lRadToAdd = DegToRad(pAngleInDegree);
	FFLOAT lOldPitch = this->mEdited->mOrientation.getX();
	lOldPitch += lRadToAdd;
	this->mEdited->mOrientation = Vector3(  lOldPitch,
											this->mEdited->mOrientation.getY(),
											this->mEdited->mOrientation.getZ()  );

	Matrix4x4 lIdentity;
	lIdentity.Matrix4x4RotationGeneralized( this->mEdited->m_matRotation, 
											this->mEdited->mOrientation.getX(),
											this->mEdited->mOrientation.getY(), 
											this->mEdited->mOrientation.getZ());
}

VVOID    MeshEditor::RotateY(FFLOAT pAngleInDegree)
{
	FFLOAT lRadToAdd = DegToRad(pAngleInDegree);
	FFLOAT lOldYaw = this->mEdited->mOrientation.getY();
	lOldYaw += lRadToAdd;
	this->mEdited->mOrientation = Vector3(  this->mEdited->mOrientation.getX(),
											lOldYaw,
											this->mEdited->mOrientation.getZ()  );

	Matrix4x4 lIdentity;
	lIdentity.Matrix4x4RotationGeneralized( this->mEdited->m_matRotation, 
											this->mEdited->mOrientation.getX(),
											this->mEdited->mOrientation.getY(),
											this->mEdited->mOrientation.getZ() );
}

VVOID    MeshEditor::RotateZ(FFLOAT pAngleInDegree)
{
	FFLOAT lRadToAdd = DegToRad(pAngleInDegree);
	FFLOAT lOldRoll  = this->mEdited->mOrientation.getZ();
	lOldRoll += lRadToAdd;
	this->mEdited->mOrientation = Vector3(  this->mEdited->mOrientation.getX(),
											this->mEdited->mOrientation.getY(),
											lOldRoll  );

	Matrix4x4 lIdentity;
	lIdentity.Matrix4x4RotationGeneralized( this->mEdited->m_matRotation, 
											this->mEdited->mOrientation.getX(),
											this->mEdited->mOrientation.getY(),
											this->mEdited->mOrientation.getZ() );
}

VVOID    MeshEditor::Rotate(const Vector3& pRotation)
{
	Matrix4x4 lRotationToAdd;
	FFLOAT lXRadToAdd = DegToRad( pRotation.getX() );
	FFLOAT lYRadToAdd = DegToRad( pRotation.getY() );
	FFLOAT lZRadToAdd = DegToRad( pRotation.getZ() );
	lRotationToAdd.Matrix4x4RotationGeneralized( lRotationToAdd, lXRadToAdd, lYRadToAdd, lZRadToAdd );
	this->mEdited->m_matRotation.Matrix4x4Mutliply( this->mEdited->m_matRotation, lRotationToAdd );
}

VVOID    MeshEditor::TranslateX(FFLOAT pDiff)
{
	Vector3 lPosition = this->mEdited->Position();
	FFLOAT lOldTx = lPosition.getX() + pDiff;

	Matrix4x4 lIdentity;
	lIdentity.Matrix4x4Translation( this->mEdited->m_matTranslation, 
									lOldTx, 
									lPosition.getY(), 
									lPosition.getZ() );
	this->mEdited->m_particle->SetPosition(this->mEdited->m_particle->GetPosition() + pDiff * Vector3(1.0f, 0.0f, 0.0f));
}

VVOID    MeshEditor::TranslateY(FFLOAT pDiff)
{
	Vector3 lPosition = this->mEdited->Position();
	FFLOAT lOldTy = lPosition.getY() + pDiff;

	Matrix4x4 lIdentity;
	lIdentity.Matrix4x4Translation( this->mEdited->m_matTranslation,
									lPosition.getX(),
									lOldTy,
									lPosition.getZ() );
	this->mEdited->m_particle->SetPosition(this->mEdited->m_particle->GetPosition() + pDiff * Vector3(0.0f, 1.0f, 0.0f));
}

VVOID    MeshEditor::TranslateZ(FFLOAT pDiff)
{
	Vector3 lPosition = this->mEdited->Position();
	FFLOAT lOldTz = lPosition.getZ() + pDiff;

	Matrix4x4 lIdentity;
	lIdentity.Matrix4x4Translation( this->mEdited->m_matTranslation,
									lPosition.getX(),
									lPosition.getY(),
									lOldTz );
	this->mEdited->m_particle->SetPosition(this->mEdited->m_particle->GetPosition() + pDiff * Vector3(0.0f, 0.0f, 1.0f));
}

VVOID	 MeshEditor::rescale(FFLOAT pScaleX, FFLOAT pScaleY, FFLOAT pScaleZ)
{
	Matrix4x4 lIdentity;
	lIdentity.Matrix4x4Scaling(this->mEdited->m_matScaling, pScaleX, pScaleY, pScaleZ);
}

VVOID	 MeshEditor::rescale(const Vector3& pScaling)
{
	Matrix4x4 lIdentity;
	lIdentity.Matrix4x4Scaling(this->mEdited->m_matScaling, pScaling.getX(), pScaling.getY(), pScaling.getZ());
}
