#ifndef DEF_MESHEDITOR_H
#define DEF_MESHEDITOR_H

// Includes
#include "IMeshEditor.h"

// Namespaces
namespace Ocelot
{
	// Forward declarations
	class AOcelotBaseMesh;

	// Class definition
	class MeshEditor : public IMeshEditor
	{
	private:

		// Attributes
		AOcelotBaseMesh* mEdited;

	public:

		// Constructor & Destructor
		MeshEditor(AOcelotBaseMesh* pMesh);
		~MeshEditor();

		// Methods
		VVOID    RotateX(FFLOAT pAngleInDegree);
		VVOID    RotateY(FFLOAT pAngleInDegree);
		VVOID    RotateZ(FFLOAT pAngleInDegree);
		VVOID    Rotate(const Vector3& pRotation);
		VVOID    TranslateX(FFLOAT pDiff);
		VVOID    TranslateY(FFLOAT pDiff);
		VVOID    TranslateZ(FFLOAT pDiff);
		VVOID	 rescale(FFLOAT pScaleX, FFLOAT pScaleY, FFLOAT pScaleZ);
		VVOID	 rescale(const Vector3& pScaling);

	};
}

#endif