#ifndef RIGIDBODYCONTACTS_H
#define RIGIDBODYCONTACTS_H

#include "IRigidBodyContactResolver.h"
#include <iostream>

namespace Ocelot
{
	/*
	 * A class that holds the data required for resolving contacts such as the two bodies colliding,
	 * the friction and restitution coefficients. It also handles the resolving of the contacts.
	 */
	class RigidBodyContact : public IRigidBodyContact
	{
	public:
		//Constructor
		RigidBodyContact();

		//Calculate the velocity of the specified body on the point of contact
		virtual Vector3 VelocityAtContact(UBIGINT bodyIndex, const FFLOAT duration);

		//Calculates the impulse (instant change in velocity) required to resolve the collision
		virtual Vector3 CalculateImpulse(Matrix3x3* inverseInertiaTensors);

		//Applies position changes to resolve inter-penetration. Similar to how with particles where the particle
		//with the smaller mass would be moved a larger % of the total movement required to resolve penetration,
		//the rigid bodies move relative to their inertia
		virtual VVOID ResolvePenetration(Vector3 linearChange[2], Vector3 angularChange[2], FFLOAT penetration);

		//Resolves the velocities each body will have after the collision (through conservation of momentum) 
		//As with the position changes, the body with the larger mass/inertia will receive the smaller velocity change
		virtual VVOID ResolveVelocity(Vector3 velocityChange[2], Vector3 rotationChange[2]);

		//Given the point of contact as an origin and the contact normal as an axis, construct the collision coordinate system
		virtual VVOID CreateCollisionBasis();

		//Calculate the speeds required to resolve collisions within the frame
		virtual VVOID UpdateRequiredVelocityChange(const FFLOAT delta);

		//Swap body[0] with body[1] and flip the contact normal, used if calculations relative to one body are easier than the other
		virtual VVOID SwitchOrder();

		//Used at the start of the resolution cycle, it sets up data such as relative velocity, calculates the collision basis and the required velocity difference
		virtual VVOID PrepareInternalData(const FFLOAT delta);

		//Setters/getters for the variables described below in private section
		virtual inline Vector3 GetPointOfContact() const { return pointOfContact; }
		virtual inline VVOID SetPointOfContact(const Vector3& contactPoint) { pointOfContact = contactPoint; }
		
		virtual inline Vector3 GetContactNormal() const { return contactNormal; }
		virtual inline VVOID SetContactNormal(const Vector3& normal) { contactNormal = normal; }

		virtual inline Vector3 GetContactVelocity() const { return contactVelocity; }
		virtual inline VVOID SetContactVelocity(const Vector3& cVel) { contactVelocity = cVel; }

		virtual inline FFLOAT GetPenetrationDepth() const { return penetrationDepth; }
		virtual inline VVOID SetPenetrationDepth(const FFLOAT p) { penetrationDepth = p; }

		virtual inline FFLOAT GetRestitutionCoeff() const { return restitutionCoefficient; }
		virtual inline VVOID SetRestitution(const FFLOAT r) { restitutionCoefficient = r; }

		virtual inline FFLOAT GetFrictionCoeff() const { return frictionCoefficient; }
		virtual inline VVOID SetFriction(const FFLOAT f) { frictionCoefficient = f; }

		virtual inline FFLOAT GetRequiredVelDiff() const { return desiredVelocityDiff; }
		virtual inline VVOID SetRequiredVelDiff(const FFLOAT delVel) { desiredVelocityDiff = delVel; }

		virtual inline IRigidBody* GetRigidBody(const BIGINT index) { return rigidBodies[index]; }
		inline VVOID SetRigidBody(const BIGINT index, IRigidBody* _body) { rigidBodies[index] = _body; }

		virtual inline Vector3 GetRelativeContactPosition(const BIGINT index) { return relPointOfContactPos[index]; }
		virtual inline VVOID SetRelativeContactPosition(const BIGINT index, const Vector3& cPos) { relPointOfContactPos[index] = cPos; }

		virtual inline Matrix3x3* GetContactToWorldMatrix() { return &collisionTransform; }

		//Utility function to reduce function calls, it sets body array to these two bodies and sets the restitution and friction coeffs
		virtual VVOID SetCollisionData(IRigidBody* bOne, IRigidBody* bTwo, const FFLOAT rest, const FFLOAT friction);


	private:
		IRigidBody* rigidBodies[2]; //Pointers to the two rigid bodies potentially colliding together

		Matrix3x3 collisionTransform; //Transformation matrix to transform between world coordinates and collision coordinates
		//The collision coordinate frame is one which has origin at the point of contact and one axis (x in this case) is the contact normal
		//It is used as some calculations are much simpler relative to the point of contact

		Vector3 pointOfContact; //The world coordinates of the point where the two bodies collide
		Vector3 contactNormal; //The direction of the collision. One body will be moved in this direction and other body moved in opposite direction
		Vector3 contactVelocity; //The speed at which they are colliding
		Vector3 relPointOfContactPos[2]; //The relative positions from each body to the pointOfContact vector

		FFLOAT penetrationDepth; //The amount that the two bodies are overlapping
		FFLOAT restitutionCoefficient; //Coefficient of restitution which denotes what % of kinetic energy was converted to potential energy during the collision
		FFLOAT frictionCoefficient; //Friction coefficient which denotes what % of energy is lost as heat and sound due to friction, should be in interval [0,1]
		FFLOAT desiredVelocityDiff; //The speeds required to resolve collisions within the frame
	};

	#define RContactResolver RigidBodyContactResolver::GetInstance()
	class RigidBodyContactResolver : public IRigidBodyContactResolver
	{
	/*
	 *	A contact resolver for rigid bodies that mostly acts as a managing class for multiple
	 *	contact data structures. It is only necessary to have one instance of the resolver so it
	 *	follows the singleton design pattern. It contains methods to resolve the collisions in the contact
	 *	data structures and uses epsilon values where it only resolves velocities and penetrations larger than this epsilon
	 *	to increase stability. It uses several iterations when resolving contacts in case resolving one contact re/introduces
	 *	another collision point
	 */
	public:
		//Get singleton instance
		static RigidBodyContactResolver* GetInstance();

		//Resolves penetration and velocity for the rigid bodies in contact with each other
		virtual VVOID ResolveCollisions(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta);

		//Configures the contact data so that they can be resolved by calling the SetInternals function for each contact 
		virtual VVOID PrepareBodies(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta);

		//Resolve inter-penetrations by calculating the position changes required then applying those changes
		virtual VVOID ResolveInterpenetrations(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta);

		//Resolve after-collision-velocities by calculating the velocity changes required then applying those changes
		virtual VVOID ResolveVelocities(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta);

		//Setters/getters for variables described below in private section
		virtual inline FFLOAT GetPenetrationDelta() const { return minPenetrationDelta; }
		virtual inline VVOID SetPenetrationDelta(const FFLOAT pEpsilon) { minPenetrationDelta = pEpsilon; }

		virtual inline FFLOAT GetVelocityDelta() const { return minVelocityDelta; }
		virtual inline VVOID SetVelocityDelta(const FFLOAT vEpsilon) { minVelocityDelta = vEpsilon; }

		virtual inline UBIGINT GetPenetrationIterations() const { return penetrationIterations; }
		virtual inline VVOID SetPenetrationIterations(const UBIGINT posIterations) { penetrationIterations = posIterations; }

		virtual inline UBIGINT GetVelocityIterations() const { return velocityIterations; }
		virtual inline VVOID SetVelocityIterations(const UBIGINT velIterations) { velocityIterations = velIterations; }

	private:
		UBIGINT penetrationIterations; //The number of iterations used to resolve penetrations
		UBIGINT velocityIterations; //The number of iterations used to resolve velocity

		FFLOAT minPenetrationDelta; //The minimum penetration depth before it will considered necessary to resolve
		FFLOAT minVelocityDelta; //The minimum velocity before it will be considered necessary to resolve

		UBIGINT penetrationIterationsUsed; //The number of iterations used so far.
		UBIGINT velocityIterationsUsed;

		//Made private to manage creation of instances of this class
		RigidBodyContactResolver(){}
		RigidBodyContactResolver(const RigidBodyContactResolver&);
		RigidBodyContactResolver& operator= (const RigidBodyContactResolver&);
	};
}

#endif