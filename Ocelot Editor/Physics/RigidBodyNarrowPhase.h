#ifndef RIGIDBODYNARROWPHASE_H
#define RIGIDBODYNARROWPHASE_H

#include "INarrowCollisionDetector.h"

/*
 *  A class used for detecting collisions between objects and finding more 
 *  specific contact data such as the point of the collision and the collision normal.
 *  Only one collision detector is required so it follows the singleton design pattern
 */

namespace Ocelot
{
	#define NarrowPhase NarrowCollisionDetector::GetInstance()
	class NarrowCollisionDetector : public INarrowCollisionDetector
	{
	public:
		//Get singleton instance
		static NarrowCollisionDetector* GetInstance();

		//Detect collision (and its information) between two spheres
		virtual BBOOL SphereAndSphere(BoundingSphere& one, BoundingSphere& two, CollisionData* data);

		//Detect collision (and its information) between an axis aligned bounding box and a sphere
		virtual BBOOL BoxAndSphere(AABBMinMax3D& box, BoundingSphere& sphere, CollisionData* data);

		//Detect collision (and its information) between two axis aligned bounding boxes
		virtual BBOOL BoxAndBox(AABBMinMax3D& one, AABBMinMax3D& two, CollisionData* data);

	private:
		//All of these lovely functions are purely for the separating axis theorem type tests. 
		//The guys at gamedev said it would be "easy enough to implement". Lies :(
		
		//tests an axis for for smallest penetration, which denotes which axis should be resolved first
		BBOOL TextAxisForPenetration(AABBMinMax3D& one, AABBMinMax3D& two, Vector3& axis, Vector3& toCentre, UBIGINT index, FFLOAT& smallestPenetration, UBIGINT& smallestCase);
		
		//Returns penetration on an axis between two bounding boxes
		FFLOAT GetPenetration(AABBMinMax3D& one, AABBMinMax3D& two, Vector3& axis, Vector3& toCentre);

		//Transform an axis aligned bounding box to be aligned with the chosen axis
		FFLOAT ReallignAABB(AABBMinMax3D& box, Vector3& axis);

		//Calculates point of contact
		Vector3 FindPointOfContact(Vector3 &pOne, Vector3 &dOne, FFLOAT oneSize, Vector3 &pTwo, Vector3 &dTwo, FFLOAT twoSize, BBOOL useOne);

		//Fills out data if the contact point lies on a face of one of the aabbs
		VVOID GeneratePointToFaceData(AABBMinMax3D& one, AABBMinMax3D& two, Vector3& toCentre, CollisionData* data, UBIGINT best, FFLOAT pen);

		//Made private to manage creation of instances
		NarrowCollisionDetector() {}
		NarrowCollisionDetector(const NarrowCollisionDetector&);
		NarrowCollisionDetector& operator= (NarrowCollisionDetector&);
	};
}

#endif