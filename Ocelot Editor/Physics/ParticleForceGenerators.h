#ifndef PARTICLEFORCEGENERATORS_H
#define PARTICLEFORCEGENERATORS_H

#include "../Maths/Vector3.h"
#include "IParticleForceGenerator.h"
#include "IParticle.h"
#include "IParticleForceRegistry.h"

#include <vector>

/*
	Particle force generators, as you can probably guess by the name,
	generate forces for particles that are to be added to the particles
	force accumulation vector. There are many force generators such as gravity,
	air resistance, springs, bouyancy and more and they each follow an
	interface to allow their usage to be polymorphic.
*/

namespace Ocelot
{

	/*
		This class is a registry that holds pointers to particles
		and pointers to force generators acting on those particles.
		The relationship between particles and force generators is:
		[many]particles:[one]force-generator but this is per-generator,
		i.e. a particle can register itself to any number of generators.

		On each update loop, it scrolls through all registrations and gets
		the force generator to calculate a force to add to the particle
	*/
	#define PForceRegistry ParticleForceRegistry::GetInstance()
	class ParticleForceRegistry : public IParticleForceRegistry
	{
	public:
		//Singleton class
		static ParticleForceRegistry* GetInstance();

		//Add a registration (a registration consists of a particle and a force generator)
		virtual VVOID Add(IParticle *particle, IParticleForceGenerator* forceGenerator);

		//Remove a particular registration
		virtual VVOID Remove(IParticle *particle, IParticleForceGenerator *forceGenerator);

		//Remove all registrations
		virtual VVOID Clear();

		//Called on each loop to update all particles registered with force generators
		virtual VVOID UpdateForces(const FFLOAT delta);

	private:
		struct ParticleForceRecord
		{
			IParticle *particle;
			IParticleForceGenerator *forceGenerator;
		};

		//The registry is just a vector of the above struct, woooo.
		typedef std::vector<ParticleForceRecord> Registry;
		Registry registrations;

		//Made private to manage instance creation
		ParticleForceRegistry() {}
		ParticleForceRegistry(const ParticleForceRegistry&);
		ParticleForceRegistry& operator= (const ParticleForceRegistry&);
	};

	/*
		Calculates a gravitational force and applies it to a particle
		The gravity force can be user specified
	*/
	class ParticleGravityGenerator : public IParticleForceGenerator
	{
	public:
		//Constructors
		ParticleGravityGenerator();
		ParticleGravityGenerator(const Vector3 &g);
		
		//Calculate gravitational force
		virtual VVOID UpdateForce(IParticle *particle, const FFLOAT deltaTime);

		//Set and get gravity vector
		inline Vector3 GetGravity() { return gravity; }
		inline VVOID SetGravity(const Vector3& g) { gravity = g; }
		inline VVOID SetGravity(const FFLOAT gx, const FFLOAT gy, const FFLOAT gz) { gravity = Vector3(gx, gy, gz); }

	private:
		Vector3 gravity;
	};

	/*
		Calculates air resistance (drag) and applies to a particle
		Can user specify the 2 drag coefficients
	*/
	class ParticleDragGenerator : public IParticleForceGenerator
	{
	public:
		//Constructors
		ParticleDragGenerator();
		ParticleDragGenerator(const FFLOAT K1, const FFLOAT K2);
		
		//Calculate drag force
		virtual VVOID UpdateForce(IParticle *particle, const FFLOAT deltaTime);

		//Setters/Getters of two drag coefficents.
		//K1 relates to the speed of the particle.
		//K2 relates to the square of the speed
		inline FFLOAT GetSpeedCoefficient() const { return k1; }
		inline FFLOAT GetSpeedSquareCoefficient() const { return k2; }
		inline VVOID SetSpeedCoeff(const FFLOAT K1) { k1 = K1; }
		inline VVOID SetSpeedSquareCoeff(const FFLOAT K2) { k2 = K2; }

	private:
		FFLOAT k1;
		FFLOAT k2;
	};

	/*
		Calculates spring force. The calculations follow Hook's law: f = -kx
		The spring constant k can be user set.
	*/
	class ParticleSpringGenerator : public IParticleForceGenerator
	{
	public:
		//Constructors
		ParticleSpringGenerator();
		ParticleSpringGenerator(IParticle *_other, const FFLOAT _springConstant, const FFLOAT _restLength);
		
		//Calculate spring force
		virtual VVOID UpdateForce(IParticle *particle, const FFLOAT deltaTime);
		
		//Setters/getters for variables
		inline IParticle* GetOther() { return otherParticle; }
		inline VVOID SetOther(IParticle *_other) { otherParticle = _other; }

		inline FFLOAT GetSpringConstant() const { return springConstant; }
		inline VVOID SetSpringConstant(const FFLOAT _springConstant) { springConstant = _springConstant; }

		inline FFLOAT GetRestLength() const { return restLength; }
		inline VVOID SetRestLength(const FFLOAT _restLength) { restLength = _restLength; }

	private:
		IParticle *otherParticle; //The particle at the other end of the spring
		FFLOAT springConstant; //Stiffness of the spring
		FFLOAT restLength; //Equilibrium point along the spring
	};

	/*
		Calculates a spring force just as the class above except now the other
		end of the spring is attached to a static position instead of another particle
	*/
	class ParticleAnchoredSpringGenerator : public IParticleForceGenerator
	{
	public:
		//Constructors
		ParticleAnchoredSpringGenerator();
		ParticleAnchoredSpringGenerator(Vector3 &_anchor, const FFLOAT _springConstant, const FFLOAT _restLength);
		
		//Calculate spring force
		virtual VVOID UpdateForce(IParticle *particle, const FFLOAT deltaTime);

		//Setters/Getters for variables
		inline Vector3 GetAnchor() const { return anchorPosition; }
		inline VVOID SetAnchor(const Vector3 &_anchor) { anchorPosition = _anchor; }

		inline FFLOAT GetSpringConstant() const { return springConstant; }
		inline VVOID SetSpringConstant(const FFLOAT _springConstant) { springConstant = _springConstant; }

		inline FFLOAT GetRestLength() const { return restLength; }
		inline VVOID SetRestLength(const FFLOAT _restLength) { restLength = _restLength; }

	private:
		Vector3 anchorPosition; //Position where the other end of the spring is attached
		FFLOAT springConstant; //Stiffness of the spring
		FFLOAT restLength; //Equilibrium point along the spring
	};

	/*
		Calculates a spring force in such a way that acts like a bungee, i.e. stretches
		but has no compression force. It is similar to the two classes above but with a 
		small check inside the updateforce method. Can be useful in two player games
		where you don't want two players to separate too much, although they can get as close
		as they want to.....in the game I mean.
	*/
	class ParticleBungeeGenerator : public IParticleForceGenerator
	{
	public:
		//Constructor
		ParticleBungeeGenerator();
		ParticleBungeeGenerator(IParticle *_other, const FFLOAT _springConstant, const FFLOAT _restLength);
		
		//Calculates bungee spring force
		virtual VVOID UpdateForce(IParticle *particle, const FFLOAT deltaTime);
	
		//Setters/getters for variables
		inline IParticle* GetOther() { return otherParticle; }
		inline VVOID SetOther(IParticle *_other) { otherParticle = _other; }

		inline FFLOAT GetSpringConstant() const { return springConstant; }
		inline VVOID SetSpringConstant(const FFLOAT _springConstant) { springConstant = _springConstant; }

		inline FFLOAT GetRestLength() const { return restLength; }
		inline VVOID SetRestLength(const FFLOAT _restLength) { restLength = _restLength; }

	private:
		IParticle *otherParticle; //Other particle the bungee end is attached to
		FFLOAT springConstant; //Stiffness of the spring
		FFLOAT restLength; //Equilibrium point along the spring
	};

	/*
		Calculates buoyancy force that pushes floating objects out of water (mostly)
		until they rest (half in water, half out, or whichever ratio has an equal gravity:buoyancy force ratio)
	*/
	class ParticleBuoyancyGenerator : public IParticleForceGenerator
	{
	public:
		//Constructors
		ParticleBuoyancyGenerator();
		ParticleBuoyancyGenerator(const FFLOAT _maxDepth, const FFLOAT _volume, const FFLOAT _waterHeight, const FFLOAT _liquidDensity);
		
		//Calculate buoyancy force
		virtual VVOID UpdateForce(IParticle *particle, const FFLOAT deltaTime);

		//Setters/getters for variables
		inline FFLOAT GetMaxDepth() const { return maxDepth; }
		inline VVOID SetMaxDepth(const FFLOAT _maxDepth) { maxDepth = _maxDepth; }

		inline FFLOAT GetVolume() const { return waterVolume; }
		inline VVOID SetVolume(const FFLOAT _volume) { waterVolume = _volume; }

		inline FFLOAT GetWaterHeight() const { return waterHeight; }
		inline VVOID SetWaterHeight(const FFLOAT _waterHeight) { waterHeight = _waterHeight; }

		inline FFLOAT GetLiquidDensity() const { return waterDensity; }
		inline VVOID SetLiquidDensity(const FFLOAT _liquidDensity) { waterDensity = _liquidDensity; }

	private:
		FFLOAT maxDepth; //Max depth before full buoyancy force is applied
		FFLOAT waterVolume; //Volume of the water
		FFLOAT waterHeight; //The height of the water from ground level
		FFLOAT waterDensity; //The "thickness" of the water
	};
}

#endif