#ifndef IRIGIDBODYCONTACT_H
#define IRIGIDBODYCONTACT_H

#include "IRigidBody.h"

/*
 *	The interface used by all rigid body contact data structures used in the Ocelot engine.
 *	The rigid body contact data structure itself should follow this general description:
 *		A class that holds the data required for resolving contacts such as the two bodies colliding,
 *		the friction and restitution coefficients. It also handles the resolving of the contacts.
 */

namespace Ocelot
{
	class IRigidBodyContact
	{
	public:
		//Calculate the velocity of the specified body on the point of contact
		virtual Vector3 VelocityAtContact(UBIGINT bodyIndex, const FFLOAT duration) = 0;

		//Calculates the impulse (instant change in velocity) required to resolve the collision
		virtual Vector3 CalculateImpulse(Matrix3x3* inverseInertiaTensors) = 0;

		//Applies position changes to resolve inter-penetration. Similar to how with particles where the particle
		//with the smaller mass would be moved a larger % of the total movement required to resolve penetration,
		//the rigid bodies move relative to their inertia
		virtual VVOID ResolvePenetration(Vector3 linearChange[2], Vector3 angularChange[2], FFLOAT penetration) = 0;

		//Resolves the velocities each body will have after the collision (through conservation of momentum) 
		//As with the position changes, the body with the larger mass/inertia will receive the smaller velocity change
		virtual VVOID ResolveVelocity(Vector3 velocityChange[2], Vector3 rotationChange[2]) = 0;

		//Given the point of contact as an origin and the contact normal as an axis, construct the collision coordinate system
		virtual VVOID CreateCollisionBasis() = 0;

		//Calculate the speeds required to resolve collisions within the frame
		virtual VVOID UpdateRequiredVelocityChange(const FFLOAT delta) = 0;

		//Swap body[0] with body[1] and flip the contact normal, used if calculations relative to one body are easier than the other
		virtual VVOID SwitchOrder() = 0;

		//Used at the start of the resolution cycle, it sets up data such as relative velocity, calculates the collision basis and the required velocity difference
		virtual VVOID PrepareInternalData(const FFLOAT delta) = 0;

		//Setters/getters for the required variables
		virtual Vector3 GetPointOfContact() const = 0;
		virtual VVOID SetPointOfContact(const Vector3& contactPoint) = 0;
		
		virtual Vector3 GetContactNormal() const = 0;
		virtual VVOID SetContactNormal(const Vector3& normal) = 0;

		virtual Vector3 GetContactVelocity() const = 0;
		virtual VVOID SetContactVelocity(const Vector3& cVel) = 0;

		virtual FFLOAT GetPenetrationDepth() const = 0;
		virtual VVOID SetPenetrationDepth(const FFLOAT p) = 0;

		virtual FFLOAT GetRestitutionCoeff() const = 0;
		virtual VVOID SetRestitution(const FFLOAT r) = 0;

		virtual FFLOAT GetFrictionCoeff() const = 0;
		virtual VVOID SetFriction(const FFLOAT f) = 0;

		virtual FFLOAT GetRequiredVelDiff() const = 0;
		virtual VVOID SetRequiredVelDiff(const FFLOAT delVel) = 0;

		virtual IRigidBody* GetRigidBody(const BIGINT index) = 0;
		virtual VVOID SetRigidBody(const BIGINT index, IRigidBody* _body) = 0;

		virtual Vector3 GetRelativeContactPosition(const BIGINT index) = 0;
		virtual VVOID SetRelativeContactPosition(const BIGINT index, const Vector3& cPos) = 0;

		virtual Matrix3x3* GetContactToWorldMatrix() = 0;

		//Utility function to reduce function calls, it sets body array to these two bodies and sets the restitution and friction coeffs
		virtual VVOID SetCollisionData(IRigidBody* bOne, IRigidBody* bTwo, const FFLOAT rest, const FFLOAT friction) = 0;
	};
}

#endif