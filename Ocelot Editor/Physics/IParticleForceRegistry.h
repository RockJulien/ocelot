#ifndef IPARTICLEFORCEREGISTRY_H
#define IPARTICLEFORCEREGISTRY_H

/*
	The interface used for particle force registrys in the ocelot engine.

	The particle force registry class is a registry that holds pointers to particles
	and pointers to force generators acting on those particles.
	The relationship between particles and force generators is:
	[many]particles:[one]force-generator but this is per-generator,
	i.e. a particle can register itself to any number of generators.

	On each update loop, it scrolls through all registrations and gets
	the force generator to calculate a force to add to the particle
*/

namespace Ocelot
{
	class IParticleForceRegistry
	{
	public:
		//Add a registration (a registration consists of a particle and a force generator)
		virtual VVOID Add(IParticle *particle, IParticleForceGenerator* forceGenerator) = 0; 

		//Remove a particular registration
		virtual VVOID Remove(IParticle *particle, IParticleForceGenerator *forceGenerator) = 0;

		//Remove all registrations
		virtual VVOID Clear() = 0;

		//Called on each loop to update all particles registered with force generators
		virtual VVOID UpdateForces(const FFLOAT delta) = 0;
	};
}

#endif