#include "RigidBodyNarrowPhase.h"

using namespace Ocelot;

//Get singleton instance
NarrowCollisionDetector* NarrowCollisionDetector::GetInstance()
{
	static NarrowCollisionDetector instance;
	return &instance;
}

//Detect collision (and its information) between two spheres
BBOOL NarrowCollisionDetector::SphereAndSphere(BoundingSphere& sphereOne, BoundingSphere& sphereTwo, CollisionData* contactData)
{
	if (contactData->contactsLeft <= 0) //If contact array is full, return false
	{
		return false;
	}

	//As usual, the sphere-sphere test is incredibly simple.
	//Just check whether the distance between the two spheres against
	//the sum of their radii.
	Vector3 positionOne = sphereOne.GetCentre();
	Vector3 positionTwo = sphereTwo.GetCentre();

	Vector3 middlePoint = positionOne - positionTwo;
	FFLOAT size = middlePoint.Vec3Length();

	if(size <= 0.0f || size >= sphereOne.GetRadius() + sphereTwo.GetRadius())
	{
		return false; //If distance between two is greater than sum of radii, they are not colliding
	}

	//If we reach here, they are colliding, so now the collision data needs to be filled in
	//Collision normal is just the normalized relative position
	IRigidBodyContact* contact = contactData->contacts;
	contact->SetContactNormal(middlePoint / size); 
	contact->SetPointOfContact(positionOne + (middlePoint * 0.5f));
	contact->SetPenetrationDepth(sphereOne.GetRadius() + sphereTwo.GetRadius() - size);
	contact->SetCollisionData(sphereOne.GetBody(), sphereTwo.GetBody(), contactData->restitution, contactData->friction);
	contactData->AddContacts(1);

	return true;
}

//Detect collision (and its information) between an axis aligned bounding box and a sphere
BBOOL NarrowCollisionDetector::BoxAndSphere(AABBMinMax3D& boundingBox, BoundingSphere& boundingSphere, CollisionData* collisionData)
{
	//Similarly to the sphere-sphere test. Get the relative centres and check against the radii
	Vector3 sphereCentre = boundingSphere.GetCentre();
	Vector3 relativeCentre = boundingBox.GetBody()->GetTransMatrix().TransformInverse(sphereCentre);

	//Closest thing to a radius the box has are half widths along each axis
	Vector3 halfWidths = boundingBox.GetHalfWidths();

	//Since any of these half widths could be different, the radius has to be checked against all 3
	if (abs(relativeCentre.getX()) - boundingSphere.GetRadius() > halfWidths.getX() ||
		abs(relativeCentre.getY()) - boundingSphere.GetRadius() > halfWidths.getY() ||
		abs(relativeCentre.getZ()) - boundingSphere.GetRadius() > halfWidths.getZ())
	{
		return false;
	}

	//If we reach here, we have a collision, so now the collision data needs to be filled out.
	//Since the relative centre is relative to the box's centre, we need to clamp each coordinate to the box
	//which goes from (-halfWidth, halfWidth) on each axis
	Vector3 clampedPoint;
	FFLOAT distance;

	//Clamp x-axis
	distance = relativeCentre.getX();
	if(distance > halfWidths.getX())
	{
		distance = halfWidths.getX();
	}
	if(distance < -halfWidths.getX())
	{
		distance = -halfWidths.getX();
	}
	clampedPoint.x(distance);

	//Clamp y-axis
	distance = relativeCentre.getY();
	if(distance > halfWidths.getY())
	{
		distance = halfWidths.getY();
	}
	if(distance < -halfWidths.getY())
	{
		distance = -halfWidths.getY();
	}
	clampedPoint.y(distance);

	//Clamp z-axis
	distance = relativeCentre.getZ();
	if(distance > halfWidths.getZ())
	{
		distance = halfWidths.getZ();
	}
	if(distance < -halfWidths.getZ())
	{
		distance = -halfWidths.getZ();
	}
	clampedPoint.z(distance);

	//Check the clamped contact point against the sphere in the usual way
	//Although this time the distance squared is checked against radius squared
	//to save a square root computation
	distance = (clampedPoint - relativeCentre).Vec3LengthSquared();
	if(distance > boundingSphere.GetRadius() * boundingSphere.GetRadius())
	{
		return false;
	}

	//Transform this closest point from the boxs' local coordinates to world coordinates
	Vector3 closestPointWorldCoords = boundingBox.GetBody()->GetTransMatrix().Transform(clampedPoint);

	//Fill in contact data
	IRigidBodyContact* contact = collisionData->contacts;
	contact->SetContactNormal((closestPointWorldCoords - sphereCentre).Vec3Normalise());
	contact->SetPointOfContact(closestPointWorldCoords);
	contact->SetPenetrationDepth(boundingSphere.GetRadius() - sqrt(distance));
	contact->SetCollisionData(boundingBox.GetBody(), boundingSphere.GetBody(), collisionData->restitution, collisionData->friction);
	collisionData->AddContacts(1);

	return true;
}

//Fills out data if the contact point lies on a face of one of the aabbs
VVOID NarrowCollisionDetector::GeneratePointToFaceData(AABBMinMax3D& firstBox, AABBMinMax3D& secondBox, Vector3& toCentre, CollisionData* collisionData, UBIGINT bestAxis, FFLOAT penetration) 
{
	IRigidBodyContact* currentContact = collisionData->contacts;

	//Work out which of first box's faces are on the colliding axis
	Vector3 collisionNormal = firstBox.GetAxis(bestAxis);
	//Each face has two possible directions, we need the one facing box 2
	if (collisionNormal.Vec3DotProduct(toCentre) > 0.0f)
	{
		collisionNormal = collisionNormal * -1.0f; //box 2 is in the other direction so flip it!
	}

	//Find out which of the 2nd box's vertices are colliding with the above box
	Vector3 vertices = secondBox.GetHalfWidths();
	//All of box 2's vertices can be found with the half widths
	//Test which direction each of the half widths should face by comparing
	//their angle with the contact normal. If facing in the wrong direction, use
	//the vertex on the opposite side of that same axis (for example if the collision
	//was on +z and we test -z, the angle between the two will be too large and so it
	//will be flipped to +z) etc.
	if (secondBox.GetAxis(0).Vec3DotProduct(collisionNormal) < 0.0f) 
	{
		vertices.x(-vertices.getX());
	} 
	if (secondBox.GetAxis(1).Vec3DotProduct(collisionNormal) < 0.0f) 
	{
		vertices.y(-vertices.getY());
	} 
	if (secondBox.GetAxis(2).Vec3DotProduct(collisionNormal) < 0.0f) 
	{
		vertices.z(-vertices.getZ());
	} 

	//Fill in contact data
	currentContact->SetContactNormal(collisionNormal);
	currentContact->SetPenetrationDepth(penetration);
	currentContact->SetPointOfContact(secondBox.GetBody()->GetTransMatrix().Transform(vertices));
	currentContact->SetCollisionData(firstBox.GetBody(), secondBox.GetBody(), collisionData->friction, collisionData->restitution);
	collisionData->AddContacts(1);
}

//Detect collision (and its information) between two axis aligned bounding boxes
BBOOL NarrowCollisionDetector::BoxAndBox(AABBMinMax3D& firstBox, AABBMinMax3D& secondBox, CollisionData* collisionData)
{
	//From centre of first box to centre of second box
	Vector3 toCentre = secondBox.GetAxis(3) - firstBox.GetAxis(3);

	FFLOAT penetrationDepth = FLT_MAX;
	unsigned bestAxis = 0xffffff;

	//Get each axis of the boxes that are to be tested
	Vector3 oneAxisZero = firstBox.GetAxis(0);
	Vector3 oneAxisOne = firstBox.GetAxis(1);
	Vector3 oneAxisTwo = firstBox.GetAxis(2);
	Vector3 twoAxisZero = secondBox.GetAxis(0);
	Vector3 twoAxisOne = secondBox.GetAxis(1);
	Vector3 twoAxisTwo = secondBox.GetAxis(2);

	//Check each axis and find the smallest penetration depth
	//First 6 are testing against the faces of the boxes, there should be 12 tests but 6 are redundant
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisZero, toCentre, 0, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisOne, toCentre, 1, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisTwo, toCentre, 2, penetrationDepth, bestAxis)) return false;

	if(!TextAxisForPenetration(firstBox, secondBox, twoAxisZero, toCentre, 3, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, twoAxisOne, toCentre, 4, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, twoAxisTwo, toCentre, 5, penetrationDepth, bestAxis)) return false;

	UBIGINT bestSingleAxis = bestAxis;

	//Now perform tests for each edge direction. technically 45 tests but all bar 9 are redundant
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisZero.Vec3CrossProduct(twoAxisZero), toCentre, 6, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisZero.Vec3CrossProduct(twoAxisOne), toCentre, 7, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisZero.Vec3CrossProduct(twoAxisTwo), toCentre, 8, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisOne.Vec3CrossProduct(twoAxisZero), toCentre, 9, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisOne.Vec3CrossProduct(twoAxisOne), toCentre, 10, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisOne.Vec3CrossProduct(twoAxisTwo), toCentre, 11, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisTwo.Vec3CrossProduct(twoAxisZero), toCentre, 12, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisTwo.Vec3CrossProduct(twoAxisOne), toCentre, 13, penetrationDepth, bestAxis)) return false;
	if(!TextAxisForPenetration(firstBox, secondBox, oneAxisTwo.Vec3CrossProduct(twoAxisTwo), toCentre, 14, penetrationDepth, bestAxis)) return false;

	//If the smallest penetration was found in the first 3 tests
	//We have a collision on the 2nd box's face
	if(bestAxis < 3)
	{
		GeneratePointToFaceData(firstBox, secondBox, toCentre, collisionData, bestAxis, penetrationDepth);
		return true;
	}
	//If found in the next 3 tests, the collision was
	//found on the 1st box's face
	else if (bestAxis < 6)
	{
		GeneratePointToFaceData(secondBox, firstBox, toCentre*-1.0f, collisionData, bestAxis-3, penetrationDepth);
		return true;
	}
	//Else it is an edge-edge collision between the two boxes
	else
	{
		bestAxis -= 6;
		BIGINT firstBoxAxisIndex = bestAxis / 3;
		BIGINT secondBoxAxisIndex = bestAxis % 3;

		Vector3 firstCollidingAxis = firstBox.GetAxis(firstBoxAxisIndex);
		Vector3 secondCollidingAxis = secondBox.GetAxis(secondBoxAxisIndex);
		Vector3 contactNormal = firstCollidingAxis.Vec3CrossProduct(secondCollidingAxis);
		contactNormal.Vec3Normalise();

		//Swap direction of axis if it doesn't go from 1st box to 2nd box
		if(contactNormal.Vec3DistanceBetween(toCentre) > 0.0f) 
		{
			contactNormal = contactNormal * -1.0f;
		}

		Vector3 contactPointOnFirst = firstBox.GetHalfWidths();
		Vector3 contactPointOnSecond = secondBox.GetHalfWidths();

		//Now need to find which of the 4 possible edges are BIGINT contact
		for(BIGINT i = 0; i < 3; ++i)
		{
			if (i == firstBoxAxisIndex) //Parallel with collision axis
			{
				contactPointOnFirst.set(i, 0.0f);
			}
			else if (firstBox.GetAxis(i).Vec3DotProduct(contactNormal) > 0.0f) 
			{ 
				contactPointOnFirst.set(i, -contactPointOnFirst[i]); //set closest extreme axis
			}

			if (i == secondBoxAxisIndex) //Parallel with collision axis
			{
				contactPointOnSecond.set(i, 0.0f);
			}
			else if (secondBox.GetAxis(i).Vec3DotProduct(contactNormal) < 0.0f)
			{
				contactPointOnSecond.set(i, -contactPointOnSecond[i]); //set closest extreme axis
			}
		}
		//Transform from local box coordinates to world coordinates
		contactPointOnFirst = firstBox.GetBody()->GetTransMatrix().Transform(contactPointOnSecond);
		contactPointOnSecond = secondBox.GetBody()->GetTransMatrix().Transform(contactPointOnSecond);

		//With the two points above forming a line segment, calculate exactly
		//where on this line is the contact point
		Vector3 pointOfContact = FindPointOfContact(contactPointOnFirst, firstCollidingAxis, firstBox.GetHalfWidths()[firstBoxAxisIndex], contactPointOnSecond, secondCollidingAxis, secondBox.GetHalfWidths()[secondBoxAxisIndex], bestSingleAxis > 2);

		//Fill in contact data
		IRigidBodyContact* contact = collisionData->contacts;
		contact->SetPenetrationDepth(penetrationDepth);
		contact->SetContactNormal(contactNormal);
		contact->SetPointOfContact(pointOfContact);
		contact->SetCollisionData(firstBox.GetBody(), secondBox.GetBody(), collisionData->friction, collisionData->restitution);
		collisionData->AddContacts(1);
		return true;
	}
	return false;
}

//Calculates point of contact
Vector3 NarrowCollisionDetector::FindPointOfContact(Vector3 &firstPoint, Vector3 &firstD, FFLOAT oneSize, Vector3 &secondPoint, Vector3 &secondD, FFLOAT twoSize, BBOOL useOne)
{
	FFLOAT firstSquareMagD = firstD.Vec3LengthSquared();
	FFLOAT secondSquareMagD = secondD.Vec3LengthSquared();

	FFLOAT denominator = firstSquareMagD * secondSquareMagD - (secondD.Vec3DotProduct(firstD)) * (secondD.Vec3DotProduct(firstD));

	if (abs(denominator) < 0.0001f) 
	{
		return useOne? firstPoint : secondPoint;
	}

	FFLOAT dpStaOne = firstD.Vec3DotProduct(firstPoint - secondPoint);
	FFLOAT dpStaTwo = secondD.Vec3DotProduct(firstPoint - secondPoint);

	FFLOAT firstProjMag = ((secondD.Vec3DotProduct(firstD)) * dpStaTwo - secondSquareMagD * dpStaOne) / denominator;
	FFLOAT secondProjMag = (firstSquareMagD * dpStaTwo - (secondD.Vec3DotProduct(firstD)) * dpStaOne) / denominator;

	//Contact point is one of the points
	if (firstProjMag > oneSize || firstProjMag < -oneSize || secondProjMag > twoSize || secondProjMag < -twoSize)
	{
		return useOne? firstPoint : secondPoint;
	}
	else
	{
		//Contact point lies between the two points
		return (firstPoint + firstD * firstProjMag) * 0.5 + (secondPoint + secondD * secondProjMag) * 0.5;
	}
}

//Transform an axis aligned bounding box to be aligned with the chosen axis
FFLOAT NarrowCollisionDetector::ReallignAABB(AABBMinMax3D& boundingBox, Vector3& axisToAlignTo) 
{
	Vector3 halfWidths = boundingBox.GetHalfWidths();
	return halfWidths.getX() * abs(axisToAlignTo.Vec3DotProduct(boundingBox.GetAxis(0))) + halfWidths.getY() * abs(axisToAlignTo.Vec3DotProduct(boundingBox.GetAxis(1))) + halfWidths.getZ() * abs(axisToAlignTo.Vec3DotProduct(boundingBox.GetAxis(2)));
}

//Returns penetration on an axis between two bounding boxes
FFLOAT NarrowCollisionDetector::GetPenetration(AABBMinMax3D& firstBox, AABBMinMax3D& secondBox, Vector3& axis, Vector3& toCentre) 
{
	//Project onto the axis to see overlap
	FFLOAT firstProjection = ReallignAABB(firstBox, axis);
	FFLOAT secondProjection = ReallignAABB(secondBox, axis);

	//Calculate and return the amount of overlap they share on this axis
	return firstProjection + secondProjection - abs(toCentre.Vec3DotProduct(axis));
}

//tests an axis for for smallest penetration, which denotes which axis should be resolved first
BBOOL NarrowCollisionDetector::TextAxisForPenetration(AABBMinMax3D& firstBox, AABBMinMax3D& secondBox, Vector3& axis, Vector3& toCentre, UBIGINT index, FFLOAT& smallestPenetration, UBIGINT& bestCase)
{
	//Don't test nearly parallel or actual parallel axes
	//But if the axis is parallel then they can't be overlapping on that axis anyway
	if(axis.Vec3LengthSquared() < FLT_EPSILON) 
	{
		return true;
	}
	axis.Vec3Normalise();

	//Get penetration on that axis
	FFLOAT penetrationDepth = GetPenetration(firstBox, secondBox, axis, toCentre);

	if(penetrationDepth < 0.0f) 
	{
		return false; //no penetration
	}

	//Compare with current smallest penetration
	//and update smallest penetration if need be
	//The smallest penetration is that axis that will
	//get resolved first.
	if(penetrationDepth < smallestPenetration)
	{
		smallestPenetration = penetrationDepth;
		bestCase = index;
	}
	return true;
}