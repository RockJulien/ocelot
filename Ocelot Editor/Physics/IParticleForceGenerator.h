#ifndef IPARTICLEFORCEGENERATOR_H
#define IPARTICLEFORCEGENERATOR_H

#include "IParticle.h"

/*
	The interfaces to be used in all particle force generators in the Ocelot engine
	The force generators follow a polymorphic interface so a single force registry
	can initiate the application of forces to particles regardless of the type of 
	force
*/

namespace Ocelot
{
	class IParticleForceGenerator
	{
	public:
		//Calculate force
		virtual VVOID UpdateForce(IParticle *particle, const FFLOAT delta) = 0;
	};
}

#endif