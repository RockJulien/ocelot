#ifndef IRIGIDBODYFORCEGENERATOR_H
#define IRIGIDBODYFORCEGENERATOR_H

#include "IRigidBody.h"

/*
 * The interface used for all rigid body force generators in the Ocelot engine
 * The only required function is one which applies the necessary force to the 
 * body at the necessary point on the body
 */

namespace Ocelot
{
	class IRigidBodyForceGenerator
	{
	public:
		virtual VVOID ApplyForce(IRigidBody* body, const FFLOAT delta) = 0;
	};
}

#endif