#ifndef PARTICLE_H
#define PARTICLE_H

#include <cassert>
#include <cmath>
#include <cfloat>

#include "../Maths/Vector3.h"
#include "IParticle.h"

/*
	Particle class used in particle physics.
	It holds position, velocity and accleration and uses a 
	force based model (which is accumulated via D'Alemberts principle)
	Has mass, stored as inverse mass to easily simulate "infinite" masses by having a 0 inverse mass
	and also stops things having 0 mass (since this would equate to an infinite inverse mass and 
	computers can't really store infinity)
*/

namespace Ocelot 
{
	class Particle : public IParticle
	{
	public:
		//Constructor
		Particle();

		//1st order Numerical integration
		virtual VVOID EulerIntegrate(const FFLOAT delta);

		//Set accumilated forces to 0
		virtual VVOID ClearAccumulator();

		//Add force to accumulation
		virtual VVOID AddForce(const Vector3 &force);

		//Getters for variables and state, variables are described in the private section below
		virtual inline Vector3 GetForceAccum() const { return forceAccumulation; }
		virtual inline Vector3 GetPosition() const { return particlePosition; }
		virtual inline Vector3 GetVelocity() const { return particleVelocity; }
		virtual inline Vector3 GetAcceleration() const { return particleAcceleration; }
		virtual inline FFLOAT GetDamping() const { return linearDamping; }
		virtual inline FFLOAT GetInverseMass() const { return inverseMass; }
		virtual inline FFLOAT GetMass() const { assert(fabsf(inverseMass) > FLT_EPSILON); return 1.0f / inverseMass; }
		virtual inline BBOOL HasFiniteMass() const { return (fabsf(inverseMass) > FLT_EPSILON); }

		//Setters for variables and state, variables are described in the private section below
		virtual inline VVOID SetPosition(const Vector3 &pos) { previousPosition = particlePosition; particlePosition = pos; }
		virtual inline VVOID SetVelocity(const Vector3 &vel) { particleVelocity = vel; }
		virtual inline VVOID SetAcceleration(const Vector3 &acc) { particleAcceleration = acc; }
		virtual inline VVOID SetDamping(const FFLOAT damp) { linearDamping = damp; }
		virtual inline VVOID SetInverseMass(const FFLOAT iMass) { inverseMass = iMass; }
		virtual inline VVOID SetMass(const FFLOAT mass) { assert(fabsf(mass) > FLT_EPSILON); inverseMass = 1.0f / mass; }

	private:
		Vector3 particlePosition; //A 3D vector storing the particles position in space
		Vector3 previousPosition; //The position of the particle in the previous frame, used to deduce amount of isotropic friction
		Vector3 particleVelocity; //Rate of change of position of the particle
		Vector3 particleAcceleration; //Rate of change of velocity of the particle
		Vector3 forceAccumulation; //Accumulated force which applies an accleration on the particle on each frame

		FFLOAT linearDamping; //How much velocity is naturally lost on each frame.
		FFLOAT inverseMass; //The inverse of how heavy the object is.

		VVOID ApplyFriction(const FFLOAT delta);
	};
}

#endif