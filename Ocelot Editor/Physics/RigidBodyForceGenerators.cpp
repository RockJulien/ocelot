#include "RigidBodyForceGenerators.h"

using namespace Ocelot;

//Constructors
RigidBodyGravityGenerator::RigidBodyGravityGenerator()
{}

//Constructors
RigidBodyGravityGenerator::RigidBodyGravityGenerator(const Vector3 &g)
	: gravity(g)
{}

//Calculate and add force to body
VVOID RigidBodyGravityGenerator::ApplyForce(IRigidBody* body, const FFLOAT delta)
{
	//axis acceleration (usually y-axis) = mass * gravity
	body->AddLinearForce(gravity * body->GetMass());
}