#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include "IRigidBody.h"

/*
 *	A rigid body class which acts similarly to the particle classes but with the addition
 *	of rotation using extended versions of Newton's laws of motion and D'Alembert's principal
 *	to include torque and angular quantities such as orientation and angular velocity.
 */

namespace Ocelot
{
	class RigidBody : public IRigidBody
	{
	public:
		//Constructor
		RigidBody();

		//Update data that can be derived from current state such as the transformation and inertia tensor
		virtual VVOID UpdateState();

		//Add force to accumulator
		virtual VVOID AddLinearForce(const Vector3& force);

		//Add torque to accumulator
		virtual VVOID AddTorque(const Vector3& torque);

		//Adds the force to the point on the body with both being specified in world coordinates
		virtual VVOID AddPointForceWorld(const Vector3 &force, const Vector3 &point);

		//Adds the force to the point on the body with the point being specified in local coordinates
		virtual VVOID AddPointForceLocal(const Vector3 &force, const Vector3 &point);

		//Set accumulated force and torque to zero along each axis
		virtual VVOID ClearAccumulators();

		//Numerically integrate to update linear and angular values for displacement and velocity
		virtual VVOID EulerIntegrate(const FFLOAT delta);

		//Transform a local coordinate to one in world coordinates 
		virtual Vector3 TransformLocalToWorld(const Vector3 &point) const;

		//Setters/getters for variables which are described below in the private section
		virtual inline FFLOAT GetInverseMass() const { return inverseMass; }
		virtual inline VVOID SetInverseMass(const FFLOAT invMass) { inverseMass = invMass; }
		virtual inline FFLOAT GetMass() const { return 1.0f / inverseMass; }
		virtual inline VVOID SetMass(const FFLOAT mass) { inverseMass = 1.0f / mass; }
		virtual inline BBOOL HasFiniteMass() const { return !(fabsf(inverseMass) < FLT_EPSILON); }

		virtual inline FFLOAT GetLinearDamping() const { return linearDamping; }
		virtual inline VVOID SetLinearDamping(const FFLOAT linDamping) { linearDamping = linDamping; }

		virtual inline FFLOAT GetAngularDamping() const { return angularDamping; }
		virtual inline VVOID SetAngularDamping(const FFLOAT angDamp) { angularDamping = angDamp; }

		virtual inline Vector3 GetRigidBodyPosition() const { return rigidBodyPosition; }
		virtual inline VVOID AddPosition(const Vector3 &p) { rigidBodyPosition += p; }
		virtual inline VVOID SetPosition(const Vector3 &p) { rigidBodyPosition = p; }

		virtual inline Vector3 GetRigidBodyVelocity() const { return rigidBodyVelocity; }
		virtual inline VVOID AddVelocity(const Vector3 &v) { rigidBodyVelocity += v; }
		virtual inline VVOID SetVelocity(const Vector3 &v) { rigidBodyVelocity = v; }

		virtual inline Vector3 GetRigidBodyAcceleration() const { return acceleration; }
		virtual inline VVOID SetAcceleration(const Vector3& a) { acceleration = a; }

		virtual inline Vector3 GetPrevAcceleration() const { return prevAcceleration; }
		virtual inline VVOID SetPrevAcceleration(const Vector3& a) { prevAcceleration = a; }

		virtual inline Vector3 GetRigidBodyAngularVelocity() const { return angularVelocity; }
		virtual inline VVOID AddAngularVelocity(const Vector3 &r) { angularVelocity += r; }
		virtual inline VVOID SetAngularVelocity(const Vector3 &v) { angularVelocity = v; }

		virtual inline Quaternion GetRigidBodyOrientation() const { return orientation; }
		virtual inline VVOID SetOrientation(const Quaternion &orient) { orientation = orient; }

		virtual inline Matrix3x4 GetTransMatrix() const { return transMatrix; }
		virtual inline VVOID SetTransMatrix(const Matrix3x4 &t) { transMatrix = t; }

		virtual inline Matrix3x3 GetInverseInertiaTensor() const { return inverseInertiaTensor; }
		virtual inline VVOID SetInverseInertiaTensor(const Matrix3x3& m) { inverseInertiaTensor = m; }
		virtual VVOID SetInertiaTensor(Matrix3x3& inertiaTensor);
		virtual VVOID GetIITWorldCoordinates(Matrix3x3* out);

		//Update transformation matrix for the local-world and world-local coordinate transformations
		virtual inline VVOID UpdateBodyTransform(Matrix3x4& transform, Vector3& _position, Quaternion& _orientation)
		{
			transform.SetOrientationAndPosition(_orientation, _position);
		}	

		//Update inertia tensor with current orientation and rotation
		virtual inline VVOID UpdateInertiaTensor(Matrix3x3 &inverseInertiaTensorWorld, const Quaternion& _orientation, const Matrix3x3& inverseInertiaTensorBody, const Matrix3x4& rotationMatrix)
		{
			FFLOAT t0 = rotationMatrix.get11() * inverseInertiaTensorBody.get_11() + rotationMatrix.get12() * inverseInertiaTensorBody.get_21() + rotationMatrix.get13() * inverseInertiaTensorBody.get_31();
			FFLOAT t1 = rotationMatrix.get11() * inverseInertiaTensorBody.get_12() + rotationMatrix.get12() * inverseInertiaTensorBody.get_22() + rotationMatrix.get13() * inverseInertiaTensorBody.get_32();
			FFLOAT t2 = rotationMatrix.get11() * inverseInertiaTensorBody.get_13() + rotationMatrix.get12() * inverseInertiaTensorBody.get_23() + rotationMatrix.get13() * inverseInertiaTensorBody.get_33();
			FFLOAT t3 = rotationMatrix.get21() * inverseInertiaTensorBody.get_11() + rotationMatrix.get22() * inverseInertiaTensorBody.get_21() + rotationMatrix.get23() * inverseInertiaTensorBody.get_31();
			FFLOAT t4 = rotationMatrix.get21() * inverseInertiaTensorBody.get_12() + rotationMatrix.get22() * inverseInertiaTensorBody.get_22() + rotationMatrix.get23() * inverseInertiaTensorBody.get_32();
			FFLOAT t5 = rotationMatrix.get21() * inverseInertiaTensorBody.get_13() + rotationMatrix.get22() * inverseInertiaTensorBody.get_23() + rotationMatrix.get23() * inverseInertiaTensorBody.get_33();
			FFLOAT t6 = rotationMatrix.get31() * inverseInertiaTensorBody.get_11() + rotationMatrix.get32() * inverseInertiaTensorBody.get_21() + rotationMatrix.get33() * inverseInertiaTensorBody.get_31();
			FFLOAT t7 = rotationMatrix.get31() * inverseInertiaTensorBody.get_12() + rotationMatrix.get32() * inverseInertiaTensorBody.get_22() + rotationMatrix.get33() * inverseInertiaTensorBody.get_32();
			FFLOAT t8 = rotationMatrix.get31() * inverseInertiaTensorBody.get_13() + rotationMatrix.get32() * inverseInertiaTensorBody.get_23() + rotationMatrix.get33() * inverseInertiaTensorBody.get_33();

			inverseInertiaTensorWorld.set_11(t0 * rotationMatrix.get11() + t1 * rotationMatrix.get12() + t2 * rotationMatrix.get13());
			inverseInertiaTensorWorld.set_12(t0 * rotationMatrix.get21() + t1 * rotationMatrix.get22() + t2 * rotationMatrix.get23());
			inverseInertiaTensorWorld.set_13(t0 * rotationMatrix.get31() + t1 * rotationMatrix.get32() + t2 * rotationMatrix.get33());
			inverseInertiaTensorWorld.set_21(t3 * rotationMatrix.get11() + t4 * rotationMatrix.get12() + t5 * rotationMatrix.get13());
			inverseInertiaTensorWorld.set_22(t3 * rotationMatrix.get21() + t4 * rotationMatrix.get22() + t5 * rotationMatrix.get23());
			inverseInertiaTensorWorld.set_23(t3 * rotationMatrix.get31() + t4 * rotationMatrix.get32() + t5 * rotationMatrix.get33());
			inverseInertiaTensorWorld.set_31(t6 * rotationMatrix.get11() + t7 * rotationMatrix.get12() + t8 * rotationMatrix.get13());
			inverseInertiaTensorWorld.set_32(t6 * rotationMatrix.get21() + t7 * rotationMatrix.get22() + t8 * rotationMatrix.get23());
			inverseInertiaTensorWorld.set_33(t6 * rotationMatrix.get31() + t7 * rotationMatrix.get32() + t8 * rotationMatrix.get33());
		}

	private:
		FFLOAT inverseMass; // 1/mass of the object. Inverse is stored to simplify integration and collision resolution (i.e. most calculations involving mass use the inverse of it)
		FFLOAT linearDamping; //The % amount of energy left for/after linear calculations (such as calculating velocity)
		FFLOAT angularDamping; //The % amount of energy left for/after rotational calculations (such as calculating angular velocity)

		Vector3 rigidBodyPosition; //The position of the body in the world space of a 3D Cartesian coordinate frame
		Vector3 rigidBodyVelocity; //The linear velocity of the body, i.e. rate of change of position of the body
		Vector3 acceleration; //The linear acceleration of the body, i.e. rate of change of velocity of the body
		Vector3 prevAcceleration; //The linear acceleration of the body in the last frame
		Quaternion orientation; //Orientation of the body in world space stored as a half angle quaternion, rotational equivalent of position
		Vector3 angularVelocity; //The angular velocity of the body. i.e. the rate of change of orientation. rotational equivalent of velocity
		
		Matrix3x4 transMatrix; //Transforms points from rigid body space to world space and vice versa
		Matrix3x3 inverseInertiaTensor; //The rotational equivalent of inverse mass. It describes the body's resistance to rotation in local coordinates
		Matrix3x3 inverseInertiaTensorWorldCoords; //Above matrix but in world coordinates

		Vector3 forceAccumulation; //D'Alembert's principle. Instead of doing calculations for each force. Accumulate a net force and apply calculations to that instead
		Vector3 torqueAccumulation; //Rotational equivalent of force accumulation
	};
}

#endif