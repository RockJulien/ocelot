#ifndef IPARTICLE_H
#define IPARTICLE_H

class Vector3; 

/*
	The interface that all particles use in the Ocelot engine
*/

namespace Ocelot
{
	class IParticle
	{
	public:
		//1st order Numerical integration
		virtual VVOID EulerIntegrate(const FFLOAT delta) = 0;

		//Set accumilated forces to 0
		virtual VVOID ClearAccumulator() = 0;

		//Add force to accumulation
		virtual VVOID AddForce(const Vector3 &force) = 0;

		//Setters and getters for the required variables: position, velocity, accleration, damping, inverse mass
		//and force accumulation
		virtual Vector3 GetForceAccum() const = 0;
		virtual Vector3 GetPosition() const = 0;
		virtual Vector3 GetVelocity() const = 0;
		virtual Vector3 GetAcceleration() const = 0;
		virtual FFLOAT GetDamping() const = 0;
		virtual FFLOAT GetInverseMass() const = 0;
		virtual FFLOAT GetMass() const = 0;
		virtual BBOOL HasFiniteMass() const = 0;

		virtual VVOID SetPosition(const Vector3 &pos) = 0;
		virtual VVOID SetVelocity(const Vector3 &vel) = 0;
		virtual VVOID SetAcceleration(const Vector3 &acc) = 0;
		virtual VVOID SetDamping(const FFLOAT damp) = 0;
		virtual VVOID SetInverseMass(const FFLOAT iMass) = 0;
		virtual VVOID SetMass(const FFLOAT mass) = 0;
	};
}

#endif