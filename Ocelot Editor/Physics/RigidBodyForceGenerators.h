#ifndef RIGIDBODYFORCEGENERATORS_H
#define RIGIDBODYFORCEGENERATORS_H

#include "IRigidBodyForceGenerator.h"

namespace Ocelot
{
	/*
	 * Generates gravitational force to be added to body
	 */
	class RigidBodyGravityGenerator : public IRigidBodyForceGenerator
	{
	public:
		//Constructors
		RigidBodyGravityGenerator();
		RigidBodyGravityGenerator(const Vector3 &g);

		//Calculate and add force to body
		virtual VVOID ApplyForce(IRigidBody* body, const FFLOAT delta);

		//Setters/getters
		inline Vector3 GetGravity() const { return gravity; }
		inline VVOID SetGravity(const Vector3 &g) { gravity = g; }

	private:
		Vector3 gravity;
	};
}
#endif