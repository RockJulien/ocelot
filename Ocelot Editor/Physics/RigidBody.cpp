#include "RigidBody.h"

using namespace Ocelot;

//Constructor
RigidBody::RigidBody()
{}

//Update data that can be derived from current state such as the transformation and inertia tensor
VVOID RigidBody::UpdateState()
{
	orientation.QuaternionNormalize();

	//Update transformation matrix for the local-world and world-local coordinate transformations
	UpdateBodyTransform(transMatrix, rigidBodyPosition, orientation); 

	//Update inertia tensor with current orientation and rotation
	UpdateInertiaTensor(inverseInertiaTensorWorldCoords, orientation, inverseInertiaTensor, transMatrix);
}

VVOID RigidBody::SetInertiaTensor(Matrix3x3& inertiaTensor)
{
	inverseInertiaTensor.Matrix3x3Inverse(inertiaTensor);
}

//Add force to accumulators
VVOID RigidBody::AddLinearForce(const Vector3& force)
{
	forceAccumulation += force;
}

//Add torque to accumulator
VVOID RigidBody::AddTorque(const Vector3& torque)
{
	torqueAccumulation +=  torque;
}

//Set accumulated force and torque to zero along each axis
VVOID RigidBody::ClearAccumulators() 
{
	torqueAccumulation.Clear();
	forceAccumulation.Clear();
}

//Numerically integrate to update linear and angular values for displacement and velocity
VVOID RigidBody::EulerIntegrate(const FFLOAT delta)
{
	//calculate linear acceleration. f = ma => a = f/m
	prevAcceleration = acceleration + forceAccumulation * inverseMass;
	//update linear velocity from the acceleration values
	rigidBodyVelocity += prevAcceleration * delta;
	//Dampen 
	rigidBodyVelocity *= powf(linearDamping, delta);

	//Calculate angular acceleration
	Vector3 angularAcceleration = inverseInertiaTensorWorldCoords.Matrix3x3Transform(torqueAccumulation);
	//update linear and angular velocity from the acceleration values
	angularVelocity += angularAcceleration * delta;
	//Dampen
	angularVelocity *= powf(angularDamping, delta);

	//Use new velocity values to calculate new position/orientation
	rigidBodyPosition += rigidBodyVelocity * delta;
	orientation += angularVelocity * delta;

	//Update local inertia tensor
	UpdateState();
	//Reset force and torque to 0
	ClearAccumulators();
}

//Adds the force to the point on the body with both being specified in world coordinates
VVOID RigidBody::AddPointForceWorld(const Vector3 &force, const Vector3 &point)
{
	//Add to accumulators
	forceAccumulation += force;
	//torque = force x distance to pivot relative to centre of mass
	torqueAccumulation += (point - rigidBodyPosition).Vec3CrossProduct(force);
}

//Adds the force to the point on the body with the point being specified in local coordinates
VVOID RigidBody::AddPointForceLocal(const Vector3 &force, const Vector3 &point)
{
	//Transform point to world coordinates and
	//delegate to world coordinate version of this function
	AddPointForceWorld(force, TransformLocalToWorld(point));
}

//Transform a local coordinate to one in world coordinates
Vector3 RigidBody::TransformLocalToWorld(const Vector3 &point) const
{
	return transMatrix.Transform(point);
}

VVOID RigidBody::GetIITWorldCoordinates(Matrix3x3* out)
{
	*out = inverseInertiaTensorWorldCoords;
}
