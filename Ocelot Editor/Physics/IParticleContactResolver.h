#ifndef IPARTICLECONTACTRESOLVER_H
#define IPARTICLECONTACTRESOLVER_H

#include <vector>
#include "IParticleContacts.h"

/*
	The interface used by all particle collision resolvers in the Ocelot engine
	It takes an array of possible collisions and resolves the velocities & penetration
	However, since resolving one collision, may place the particles into another collision,
	multiple iterations are used and the "worst" penetration is resolved on each iteration
*/

namespace Ocelot
{
	class IParticleContactResolver
	{
	public:
		//Iterate through the array and resolve the "worst" penetration for each collision
		//Do this for multiple iterations in case one resolution introduces a new penetration
		virtual VVOID ResolveContacts(std::vector<IParticleContact*>& contactArray, const FFLOAT delta) = 0;

		//Setters/getters for variables
		virtual BIGINT GetNumIterations() const = 0; //How many iterations used before assuming the collisions are resolved enough
		virtual VVOID setNumIterations(const BIGINT numIterations) = 0;
	};
}

#endif