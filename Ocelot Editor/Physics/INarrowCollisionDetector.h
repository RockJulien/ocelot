#ifndef INARROWCOLLISIONDETECTOR_H
#define INARROWCOLLISIONDETECTOR_H

#include "IRigidBodyContact.h"
#include "../Collisions/BoundingSphere.h"
#include "../Collisions/AABB.h"

/*
 *	The interface used by all narrow phase collision detectors in the Ocelot engine.
 *	The narrow phase collision detector should follow this description:
 *	A class used for detecting collisions between objects and finding more 
 *  specific contact data such as the point of the collision and the collision normal.
 *  Only one collision detector is required so it follows the singleton design pattern
 */

namespace Ocelot
{
	//A data structure that the collision detector fills out when detecting collisions
	struct CollisionData 
	{
		IRigidBodyContact* contacts;
		BIGINT contactsLeft;
		BIGINT contactCount;
		FFLOAT restitution;
		FFLOAT friction;

		VVOID AddContacts(UBIGINT count)
		{
			contactsLeft -= count;
			contactCount += count;

			contacts += count;
		}
	};

	class INarrowCollisionDetector
	{
	public:
		//Detect collision (and its information) between two spheres
		virtual BBOOL SphereAndSphere(BoundingSphere& one, BoundingSphere& two, CollisionData* data) = 0;

		//Detect collision (and its information) between an axis aligned bounding box and a sphere
		virtual BBOOL BoxAndSphere(AABBMinMax3D& box, BoundingSphere& sphere, CollisionData* data) = 0;

		//Detect collision (and its information) between two axis aligned bounding boxes
		virtual BBOOL BoxAndBox(AABBMinMax3D& one, AABBMinMax3D& two, CollisionData* data) = 0;
	};
}

#endif