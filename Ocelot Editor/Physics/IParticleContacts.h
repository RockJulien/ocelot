#ifndef IPARTICLECONTACTS_H
#define IPARTICLECONTACTS_H

/*
	The interface that all particle contact classes follow for use
	in the collision resolution system for particles.
*/

namespace Ocelot
{
	class IParticleContact
	{
	public:
		//Resolve velocity using impulses and interpenetration
		virtual VVOID Resolve(const FFLOAT delta) = 0;

		//Calculate separating velocity, the speed at which two particles are moving away from eachother
		virtual FFLOAT SeparatingVelocity() const = 0;

		//Setters and getters for variables
		virtual VVOID SetPenetration(const FFLOAT pen) = 0;
		virtual VVOID SetRestitution(const FFLOAT rest) = 0;
		virtual FFLOAT GetPenetration() const = 0; //How much the two particles are penetrating eachother
		virtual FFLOAT GetRestitution() const = 0;//Coefficient of restitution, it corresponds to how much energy is lost in the collision, should always be in interval [0,1]
	};
}

#endif