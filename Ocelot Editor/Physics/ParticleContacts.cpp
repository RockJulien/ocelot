#include "ParticleContacts.h"

using namespace Ocelot;

//Constructor
ParticleContact::ParticleContact(IParticle* partA, IParticle* partB, const FFLOAT restCoeff, const Vector3& cNormal, const FFLOAT pen)
{
	collidingParticle[0] = partA;
	collidingParticle[1] = partB;
	restitutionCoeff = restCoeff;

	penetrationDepth = pen;
	contactNormal = cNormal;
}

//Resolve velocity and interpenetration
VVOID ParticleContact::Resolve(const FFLOAT delta)
{
	ResolvePenetration(delta); //Move particles apart so they are no longer penetrating
	ResolveVelocity(delta); //Resolve how fast the two particles should move after the collision
}

//Calculate separating velocity, the speed at which two particles are moving away from eachother
FFLOAT ParticleContact::SeparatingVelocity() const
{
	//Separating velocity = relative vel dotted with contact normal
	//Its a FFLOAT even though its called velocity and not speed. The sign determines the direction
	Vector3 relativeVelocity = collidingParticle[0]->GetVelocity();
	if(collidingParticle[1])
	{
		relativeVelocity -= collidingParticle[1]->GetVelocity();
	}

	return relativeVelocity.Vec3DotProduct(contactNormal);
}

//Resolve how fast the two particles should move after the collision
VVOID ParticleContact::ResolveVelocity(const FFLOAT delta)
{
	//velocity in the direction of the contact
	FFLOAT separatingVelocity = SeparatingVelocity();

	//If they are already separating, don't need to resolve collision
	if(separatingVelocity > 0.0f)
	{
		return;
	}

	//separatingVelocity is *before* collision, newSeparatingVelocity is *after* collision
	//new vel = old vel * -restitution coeff
	FFLOAT sepVelAfterCollision = -separatingVelocity * restitutionCoeff;

	//Checks velocity build up due to acceleration only, this will happen during resting contact
	CalculateRestBuildUp(delta, sepVelAfterCollision);

	FFLOAT velocityDelta = sepVelAfterCollision - separatingVelocity;

	//Apply impulse with respect to the particles mass (heavier object has smaller change)
	//We find this using the % of the particles mass in relation to the total mass of the two particles
	FFLOAT totalInverseMass = collidingParticle[0]->GetInverseMass();
	if(collidingParticle[1])
	{
		totalInverseMass += collidingParticle[1]->GetInverseMass();
	}

	if(totalInverseMass <= 0.0f) //The two things in contact must be of "infinite" mass and do not need resolving
	{
		return;
	}

	//Calculate how much impulse to apply in the direction of the collision
	FFLOAT impulseIntegral = velocityDelta / totalInverseMass;
	Vector3 impulse = contactNormal * impulseIntegral;

	//Apply impulses, particle[1]'s impulse is specified in the opposite direction to particle[0]s as 
	//detailed in Newton's 3rd law of motion
	//Amount of impulse is proportional to the inverse mass of the particles
	UpdateVelocities(impulse);
}

VVOID ParticleContact::UpdateVelocities(const Vector3& impulse) 
{
	collidingParticle[0]->SetVelocity(collidingParticle[0]->GetVelocity() + impulse * collidingParticle[0]->GetInverseMass());
	if(collidingParticle[1])
	{
		collidingParticle[1]->SetVelocity(collidingParticle[1]->GetVelocity() + impulse * -collidingParticle[1]->GetInverseMass());
	}
}

VVOID ParticleContact::CalculateRestBuildUp(const FFLOAT delta, FFLOAT& sepVelAfterCollision) 
{
	Vector3 restingContactBuildUp = collidingParticle[0]->GetAcceleration();
	if(collidingParticle[1])
	{
		restingContactBuildUp -= collidingParticle[1]->GetAcceleration();
	}

	FFLOAT accCausedSeparatingVelocity = restingContactBuildUp.Vec3DotProduct(contactNormal) * delta;
	if(accCausedSeparatingVelocity < 0.0f) //If we have build up because of resting contact, remove extra force from system
	{
		sepVelAfterCollision += restitutionCoeff * accCausedSeparatingVelocity;
		if(sepVelAfterCollision < 0.0f)
		{
			sepVelAfterCollision = 0.0f;
		}
	}
}

//Move particles apart so they are no longer penetrating
VVOID ParticleContact::ResolvePenetration(const FFLOAT delta)
{
	//If there is no penetration, then exit early
	if(penetrationDepth <= 0.0f)
	{
		return;
	}

	//How far each particle moves to resolve penetration depends on
	//the mass of each particle, heavier objects will move less.

	FFLOAT totalInverseMass = collidingParticle[0]->GetInverseMass();
	if(collidingParticle[1])
	{
		totalInverseMass += collidingParticle[1]->GetInverseMass();
	}

	if(totalInverseMass <= 0.0f)
	{
		return;
	}

	//Calculate how much movement is needed in the direction of the contact normal
	Vector3 movementPerMass = contactNormal * (penetrationDepth / totalInverseMass);

	//Calculate amount of movement proportional to inversemass. particleMovement[1] is specified in the opposite direction than particleMovement[0]
	particleDisplacement[0] = movementPerMass * collidingParticle[0]->GetInverseMass();
	if(collidingParticle[1])
	{
		particleDisplacement[1] = movementPerMass * -collidingParticle[1]->GetInverseMass();
	} 
	else {
		particleDisplacement[1].Clear();
	}

	//Apply movement 
	UpdatePositions();
}

VVOID ParticleContact::UpdatePositions() 
{
	collidingParticle[0]->SetPosition(collidingParticle[0]->GetPosition() + particleDisplacement[0]);
	if(collidingParticle[1])
	{
		collidingParticle[1]->SetPosition(collidingParticle[1]->GetPosition() + particleDisplacement[1]);
	}
}

//Singleton instance
ParticleContactResolver* ParticleContactResolver::GetInstance()
{
	static ParticleContactResolver instance;
	return &instance;
}

//Iterate through the array and resolve the "worst" penetration for each collision
//Do this for multiple iterations in case one resolution introduces a new penetration
VVOID ParticleContactResolver::ResolveContacts(std::vector<IParticleContact*>& contactParticles, const FFLOAT delta)
{
	BIGINT i;
	iterationsUsed = 0;

	while(iterationsUsed < iterations)
	{
		FFLOAT max = FLT_MAX;
		BIGINT numberOfContacts = contactParticles.size();
		BIGINT highestPriorityIndex = numberOfContacts;
		//Loop through and find the worst penetration for each contact
		for(i = 0; i < numberOfContacts; ++i)
		{
			FFLOAT separatingVelocity = contactParticles[i]->SeparatingVelocity();
			if(separatingVelocity < max && (separatingVelocity < 0.0f || contactParticles[i]->GetPenetration() > 0.0f))
			{
				max = separatingVelocity;
				highestPriorityIndex = i;
			}
		}

		if(highestPriorityIndex == numberOfContacts)
		{
			break;
		}

		//Resolve that particular "worst" penetration
		contactParticles[highestPriorityIndex]->Resolve(delta);
		++iterationsUsed; //iterate through again to make sure resolving this penetration didnt introduce more
	}
}