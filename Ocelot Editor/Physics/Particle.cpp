#include "Particle.h"

using namespace Ocelot;

//Constructor
Particle::Particle() 
{}

//1st order Numerical integration
VVOID Particle::EulerIntegrate(const FFLOAT delta)
{
	//If we are integrating things with "infinite" mass, then don't
	//integrate, their position should stay the same
	if(inverseMass <= 0.0f)
	{
		return;
	}

	assert(delta > 0.0f);

	Vector3 currentFrameAcceleration = particleAcceleration;
	//force = mass * acceleration => acceleration = force/mass
	currentFrameAcceleration += forceAccumulation * inverseMass;

	previousPosition = particlePosition; //Store position before integrating
	particlePosition += particleVelocity * delta; //update position
	particleVelocity += currentFrameAcceleration * delta; //update velocity

	particleVelocity *= pow(linearDamping, delta); //Dampen velocity

	//If the particle is sliding along the xz plane, introduce friction along those axes
	ApplyFriction(delta);

	ClearAccumulator(); //Reset accumulated forces
}

//Set accumilated forces to 0
VVOID Particle::ClearAccumulator()
{
	forceAccumulation.Clear();
}

//Add force to accumulation
VVOID Particle::AddForce(const Vector3 &force)
{
	forceAccumulation += force;
}

VVOID Particle::ApplyFriction(const FFLOAT delta) 
{
	if(fabsf(previousPosition.getY() - particlePosition.getY()) < 0.01f)
	{
		particleVelocity.x(particleVelocity.getX() * pow(linearDamping * 0.5f, delta));
		particleVelocity.z(particleVelocity.getZ() * pow(linearDamping * 0.5f, delta));
	}
}