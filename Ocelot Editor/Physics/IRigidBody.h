#ifndef IRIGIDBODY_H
#define IRIGIDBODY_H

#include "../Maths/Vector3.h"
#include "../Maths/Quaternion.h"
#include "../Maths/Matrix3x4.h"
#include "../Maths/Matrix3x3.h"

/*
 *	The interface used by all rigid body classes in the Ocelot engine
 *	The rigid body class itself should follow the following description:
 *		A rigid body class which acts similarly to the particle classes but with the addition
 *		of rotation using extended versions of Newton's laws of motion and D'Alembert's principal
 *		to include torque and angular quantities such as orientation and angular velocity.
 */

namespace Ocelot
{
	class IRigidBody
	{
	public:
		//Update data that can be derived from current state such as the transformation and inertia tensor
		virtual VVOID UpdateState() = 0;

		//Add force to accumulator
		virtual VVOID AddLinearForce(const Vector3& force) = 0;

		//Add torque to accumulator
		virtual VVOID AddTorque(const Vector3& torque) = 0;

		//Adds the force to the point on the body with both being specified in world coordinates
		virtual VVOID AddPointForceWorld(const Vector3 &force, const Vector3 &point) = 0;

		//Adds the force to the point on the body with the point being specified in local coordinates
		virtual VVOID AddPointForceLocal(const Vector3 &force, const Vector3 &point) = 0;

		//Set accumulated force and torque to zero along each axis
		virtual VVOID ClearAccumulators() = 0;

		//Numerically integrate to update linear and angular values for displacement and velocity
		virtual VVOID EulerIntegrate(const FFLOAT delta) = 0;

		//Transform a local coordinate to one in world coordinates 
		virtual Vector3 TransformLocalToWorld(const Vector3 &point) const = 0;

		//Update transformation matrix for the local-world and world-local coordinate transformations
		virtual VVOID UpdateBodyTransform(Matrix3x4& transform, Vector3& _position, Quaternion& _orientation) = 0;	

		//Update inertia tensor with current orientation and rotation
		virtual VVOID UpdateInertiaTensor(Matrix3x3 &iitWorld, const Quaternion& _orientation, const Matrix3x3& iitBody, const Matrix3x4& rotMat) = 0;

		//Setters/getters for required variables
		virtual FFLOAT GetInverseMass() const = 0;
		virtual VVOID SetInverseMass(const FFLOAT invMass) = 0;
		virtual FFLOAT GetMass() const = 0;
		virtual VVOID SetMass(const FFLOAT mass) = 0;
		virtual BBOOL HasFiniteMass() const = 0;

		virtual FFLOAT GetLinearDamping() const = 0;
		virtual VVOID SetLinearDamping(const FFLOAT linDamping) = 0;

		virtual FFLOAT GetAngularDamping() const = 0;
		virtual VVOID SetAngularDamping(const FFLOAT angDamp) = 0;

		virtual Vector3 GetRigidBodyPosition() const = 0;
		virtual VVOID AddPosition(const Vector3 &p) = 0;
		virtual VVOID SetPosition(const Vector3 &p) = 0;

		virtual Vector3 GetRigidBodyVelocity() const = 0;
		virtual VVOID AddVelocity(const Vector3 &v) = 0;
		virtual VVOID SetVelocity(const Vector3 &v) = 0;

		virtual Vector3 GetRigidBodyAcceleration() const = 0;
		virtual VVOID SetAcceleration(const Vector3& a) = 0;

		virtual Vector3 GetPrevAcceleration() const = 0;
		virtual VVOID SetPrevAcceleration(const Vector3& a) = 0;

		virtual Vector3 GetRigidBodyAngularVelocity() const = 0;
		virtual VVOID AddAngularVelocity(const Vector3 &r) = 0;
		virtual VVOID SetAngularVelocity(const Vector3 &v) = 0;

		virtual Quaternion GetRigidBodyOrientation() const = 0;
		virtual VVOID SetOrientation(const Quaternion &orient) = 0;

		virtual Matrix3x4 GetTransMatrix() const = 0;
		virtual VVOID SetTransMatrix(const Matrix3x4 &t) = 0;

		virtual Matrix3x3 GetInverseInertiaTensor() const = 0;
		virtual VVOID SetInverseInertiaTensor(const Matrix3x3& m) = 0;
		virtual VVOID SetInertiaTensor(Matrix3x3& inertiaTensor) = 0;
		virtual VVOID GetIITWorldCoordinates(Matrix3x3* out) = 0;
	};
}

#endif