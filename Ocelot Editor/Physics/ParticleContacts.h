#ifndef PARTICLECONTACTS_H
#define PARTICLECONTACTS_H

#include "Particle.h"
#include "IParticleContacts.h"
#include "IParticleContactResolver.h"
#include <vector>

namespace Ocelot
{
	/*
		A contact data class that contains all the required data to resolve collisions between particles
	*/
	class ParticleContact : public IParticleContact
	{
	public:
		//Constructor
		ParticleContact(IParticle* partA, IParticle* partB, const FFLOAT restCoeff, const Vector3& cNorm, const FFLOAT pen);

		//Resolve velocity using impulses and interpenetration
		virtual VVOID Resolve(const FFLOAT delta);
		//Calculate separating velocity, the speed at which two particles are moving away from eachother
		virtual FFLOAT SeparatingVelocity() const;

		//Setters and getters for variables
		virtual inline VVOID SetPenetration(const FFLOAT pen) { penetrationDepth = pen; }
		virtual inline VVOID SetRestitution(const FFLOAT rest) { restitutionCoeff = rest; }
		virtual inline FFLOAT GetPenetration() const { return penetrationDepth; }
		virtual inline FFLOAT GetRestitution() const { return restitutionCoeff; }

	private:
		IParticle* collidingParticle[2]; //The two particles in contact
		FFLOAT restitutionCoeff; //Coefficient of restitution, it corresponds to how much energy is lost in the collision, should always be in interval [0,1]
		FFLOAT penetrationDepth; //How much the two particles are penetrating eachother
		Vector3 contactNormal; //The direction of the collision
		Vector3 particleDisplacement[2]; //How much each particle will move in order to separate. The heavier the object, the less it moves

		VVOID ResolveVelocity(const FFLOAT delta); //Resolve how fast the two particles should move after the collision using impulses
		VVOID ResolvePenetration(const FFLOAT delta); //Move particles apart so they are no longer penetrating
		VVOID UpdateVelocities(const Vector3& impulse);
		VVOID UpdatePositions();
		VVOID CalculateRestBuildUp(const FFLOAT delta, FFLOAT& sepVelAfterCollision);
	};

	#define PContactResolver ParticleContactResolver::GetInstance()
	class ParticleContactResolver : public IParticleContactResolver
	{
	public:
		//Singleton instance
		static ParticleContactResolver* GetInstance();

		//Setters/getters for variables
		virtual inline BIGINT GetNumIterations() const { return iterations; }
		virtual inline VVOID setNumIterations(const BIGINT numIterations) { iterations = numIterations; }
		inline BIGINT getNumIterationsUsed() const { return iterationsUsed; }

		//Iterate through the array and resolve the "worst" penetration for each collision
		//Do this for multiple iterations in case one resolution introduces a new penetration
		virtual VVOID ResolveContacts(std::vector<IParticleContact*>& contactArray, const FFLOAT delta);

	private:
		BIGINT iterations; //How many iterations used before assuming the collisions are resolved enough
		BIGINT iterationsUsed; //How many iterations have been used so far

		//Made private to manage creation of instances
		ParticleContactResolver(){}
		ParticleContactResolver(const ParticleContactResolver&);
		ParticleContactResolver& operator= (const ParticleContactResolver&);
	};
}

#endif