#include "RigidBodyContacts.h"

using namespace Ocelot;

//Constructor
RigidBodyContact::RigidBodyContact()
{}

//Utility function to reduce function calls, it sets body array to these two bodies and sets the restitution and friction coeffs
VVOID RigidBodyContact::SetCollisionData(IRigidBody* firstBody, IRigidBody* secondBody, const FFLOAT rest, const FFLOAT _friction)
{
	rigidBodies[0] = firstBody;
	rigidBodies[1] = secondBody;
	restitutionCoefficient = rest;
	frictionCoefficient = _friction;
}

//Given the point of contact as an origin and the contact normal as an axis, construct the collision coordinate system
VVOID RigidBodyContact::CreateCollisionBasis()
{
	Vector3 contactAxis[2];

	/*
	 *	First check whether the new z-axis is closer to the contact normals x or y axis
	 *	Then for each result:
	 *	calculate a scale which will normalize the vector, but save it in a FFLOAT so
	 *	we don't have to keep recalculating the length
	 *	The new x-axis is at right angles to the either the world up or world right vectors 
	 *	(hence the two routes the if statement could go) and is found using a 2D determinant
	 *	The new y-axis is found by the cross product between the z-axis and the new x-axis
	 */
	if(abs(contactNormal.getX()) > abs(contactNormal.getY()))
	{
		FFLOAT inverseMagnitude = 1.0f/sqrtf(contactNormal.getZ()*contactNormal.getZ() + contactNormal.getX()*contactNormal.getX());

		contactAxis[0] = Vector3(contactNormal.getZ() * inverseMagnitude, 0.0f, -contactNormal.getX() * inverseMagnitude);
		contactAxis[1] = Vector3(contactNormal.getY() * contactAxis[0].getX(), contactNormal.getZ() * contactAxis[0].getX() - contactNormal.getX() * contactAxis[0].getZ(), -contactNormal.getY() * contactAxis[0].getX());
	} 
	else
	{
		FFLOAT inverseMagnitude = 1.0f/sqrtf(contactNormal.getZ()*contactNormal.getZ() + contactNormal.getY()*contactNormal.getY());

		contactAxis[0] = Vector3(0.0f, -contactNormal.getZ() * inverseMagnitude, contactNormal.getY() * inverseMagnitude);
		contactAxis[1] = Vector3(contactNormal.getY() * contactAxis[0].getZ() - contactNormal.getZ() * contactAxis[0].getY(), -contactNormal.getX() * contactAxis[0].getZ(), contactNormal.getX() * contactAxis[0].getY());
	}

	//Set these new vectors to the matrix with the contact normal as the x-axis in collision coordinates
	collisionTransform.Matrix3x3SetBasis(contactNormal, contactAxis[0], contactAxis[1]);
}

//Used at the start of the resolution cycle, it sets up data such as relative velocity, calculates the collision basis and the required velocity difference
VVOID RigidBodyContact::PrepareInternalData(const FFLOAT delta)
{
	//Create collision coordinate frame between the two bodies
	CreateCollisionBasis();

	//Find relative position from the point of contact to each body
	relPointOfContactPos[0] = pointOfContact - rigidBodies[0]->GetRigidBodyPosition();
	relPointOfContactPos[1] = pointOfContact - rigidBodies[1]->GetRigidBodyPosition();
	
	//Calculate relative velocity of the bodies at the point of contact
	contactVelocity = VelocityAtContact(0, delta) - VelocityAtContact(1, delta);

	//Given the relative positions and velocity, update the required velocities to resolve the collision
	UpdateRequiredVelocityChange(delta);
}

//Calculate the speeds required to resolve collisions within the frame
VVOID RigidBodyContact::UpdateRequiredVelocityChange(const FFLOAT delta)
{
	FFLOAT velocityLimit = 0.25f;

	//Relative velocity accrued from resting contacts
	FFLOAT velocityFromAcc = (rigidBodies[0]->GetPrevAcceleration() * delta).Vec3DotProduct(contactNormal) - (rigidBodies[1]->GetPrevAcceleration() * delta).Vec3DotProduct(contactNormal);

	//If this relative velocity is too small, temporarily set restitution to 0 to remove all energy from the system
	//This stops some of the resting contact "jitter"
	FFLOAT currentRestitution = restitutionCoefficient;
	if (abs(contactVelocity.getX()) < velocityLimit)
	{
		currentRestitution = 0.0f;
	}

	//required velocity is in the opposite direction of the contact basis and resting contact acceleration buildup is removed
	desiredVelocityDiff = -contactVelocity.getX() - currentRestitution * (contactVelocity.getX() - velocityFromAcc);
}

//Swap body[0] with body[1] and flip the contact normal, used if calculations relative to one body are easier than the other
VVOID RigidBodyContact::SwitchOrder()
{
	std::swap(rigidBodies[0], rigidBodies[1]);
	contactNormal *= -1;
}

//Calculate the velocity of the specified body on the point of contact
Vector3 RigidBodyContact::VelocityAtContact(UBIGINT bodyIndex, const FFLOAT duration)
{
	//Velocity of the body along an axis perpendicular to the contact normal relative to the bodies current velocity.
	//It can be thought of as the velocity produced at the contact point if the body collided there (which it either is or will do soon)
	Vector3 vel = rigidBodies[bodyIndex]->GetRigidBodyAngularVelocity().Vec3CrossProduct(relPointOfContactPos[bodyIndex]) + rigidBodies[bodyIndex]->GetRigidBodyVelocity();
	//Transform to the collision coordinate frame and return
	return collisionTransform.Matrix3x3TransformTranspose(vel);
}

//Applies position changes to resolve inter-penetration. Similar to how with particles where the particle
//with the smaller mass would be moved a larger % of the total movement required to resolve penetration,
//the rigid bodies move relative to their inertia
VVOID RigidBodyContact::ResolvePenetration(Vector3 linearDelta[2], Vector3 angularDelta[2], FFLOAT penetrationDepth)
{
	FFLOAT inertiaAlongCentre[2]; //Along centre of mass can be used as a linear particle
	FFLOAT angularInertia[2];

	FFLOAT cumulativeInertia = 0.0f;
	//Calculate the total amount of inertia (i.e. "resistance to rotation") that will
	//take place in order to resolve penetration. The axis along which the inertia is calculated
	//is the collision basis' x-axis, which is equal to the contact normal
	for(BIGINT body = 0; body < 2; ++body)
	{
		if(rigidBodies[body])
		{
			Matrix3x3 inverseInertiaTensor;
			rigidBodies[body]->GetIITWorldCoordinates(&inverseInertiaTensor);

			//Resistance to rotation along axes that don't fall through the centre of mass
			Vector3 angularInertiaWorld = relPointOfContactPos[body].Vec3CrossProduct(contactNormal);
			angularInertiaWorld = inverseInertiaTensor.Matrix3x3Transform(angularInertiaWorld);
			angularInertiaWorld = angularInertiaWorld.Vec3CrossProduct(relPointOfContactPos[body]);
			//Only interested in the inertia along the contact normal
			angularInertia[body] = angularInertiaWorld.Vec3DotProduct(contactNormal);

			inertiaAlongCentre[body] = rigidBodies[body]->GetInverseMass(); //Inertia through centre of mass

			cumulativeInertia += inertiaAlongCentre[body] + angularInertia[body];
		}
	}

	//Unlike particles which can only translate, for rigid bodies you have to also include how much the bodies need to
	//rotate to resolve penetration
	FFLOAT requiredAngularMovement[2];
	FFLOAT requiredLinearMovement[2];
	//Now loop through to calculate how much each object should move
	for (BIGINT body = 0; body < 2; ++body)
	{
		FFLOAT direction = (body == 0) ? 1.0f : -1.0f;

		//Determine what % of the total inertia needs to be allotted to rotation and the rest goes to linear movement
		requiredAngularMovement[body] = direction * penetrationDepth * (angularInertia[body] / cumulativeInertia);
		requiredLinearMovement[body] = direction * penetrationDepth * (inertiaAlongCentre[body] / cumulativeInertia);

		//Limit the movement for when mass is large but inertia is small
		Vector3 projection = relPointOfContactPos[body];
		projection += contactNormal * (-relPointOfContactPos[body].Vec3DotProduct(contactNormal));

		//Clamp the values to fit in this projected limit
		FFLOAT angularLimit = 0.2f;
		FFLOAT maximumMag = angularLimit * projection.Vec3Length();
		if(requiredAngularMovement[body] < -maximumMag)
		{
			FFLOAT totalMovement = requiredAngularMovement[body] + requiredLinearMovement[body];
			requiredAngularMovement[body] = -maximumMag;
			requiredLinearMovement[body] = totalMovement - requiredAngularMovement[body];
		}
		else if(requiredAngularMovement[body] > maximumMag)
		{
			FFLOAT totalMovement = requiredAngularMovement[body] + requiredLinearMovement[body];
			requiredAngularMovement[body] = maximumMag;
			requiredLinearMovement[body] = totalMovement - requiredAngularMovement[body];
		}

		//The required direction to rotate in
		Vector3 requiredDirection = relPointOfContactPos[body].Vec3CrossProduct(contactNormal);

		Matrix3x3 inverseInertiaTensor;
		rigidBodies[body]->GetIITWorldCoordinates(&inverseInertiaTensor);

		//Calculate the amount of rotation need to be facing that direction, scaled by how much of the movement is rotation and by how much
		//rotational inertia there is
		angularDelta[body] = inverseInertiaTensor.Matrix3x3Transform(requiredDirection) * (requiredAngularMovement[body] / angularInertia[body]);


		//We're only interested in the linear movement along the contact normal
		linearDelta[body] = contactNormal.Vec3DotProduct(requiredLinearMovement[body]);

		//Apply the new position
		rigidBodies[body]->SetPosition(rigidBodies[body]->GetRigidBodyPosition() + (contactNormal * requiredLinearMovement[body]));

		//Apply the new orientation
		rigidBodies[body]->SetOrientation(rigidBodies[body]->GetRigidBodyOrientation() + angularDelta[body]);
	}
}

//Resolves the velocities each body will have after the collision (through conservation of momentum) 
//As with the position changes, the body with the larger mass/inertia will receive the smaller velocity change
VVOID RigidBodyContact::ResolveVelocity(Vector3 velocityDelta[2], Vector3 rotationDelta[2])
{
	//Cache inverse inertia matrices instead of calling the function many times.
	Matrix3x3 inverseInertias[2];
	inverseInertias[0] = rigidBodies[0]->GetInverseInertiaTensor();
	inverseInertias[1] = rigidBodies[1]->GetInverseInertiaTensor();

	//Holds the magnitude of the impulse to be applied along each axis in the collision basis (at the point of collision)
	Vector3 impulseInCollisionBasis = CalculateImpulse(inverseInertias);
	
	//Transform the impulse from collision basis coordinates to world coordinates (so it can be applied to the bodies)
	Vector3 impulseWorld = collisionTransform.Matrix3x3Transform(impulseInCollisionBasis);

	//Angular velocity goes away from the contact position for body 0, towards for body 1
	Vector3 impulsiveTorque0 = relPointOfContactPos[0].Vec3CrossProduct(impulseWorld);
	Vector3 impulsiveTorque1 = impulseWorld.Vec3CrossProduct(relPointOfContactPos[1]);
	
	//Update the bodies
	//Linear velocity change is just instantaneous force over mass, it's very similar to how you would treat acceleration
	rigidBodies[0]->AddVelocity(impulseWorld * rigidBodies[0]->GetInverseMass());
	rigidBodies[1]->AddVelocity(impulseWorld * -rigidBodies[1]->GetInverseMass()); //In opposite direction
	//Rotational change = impulse/inertia. similar to force/mass.
	rigidBodies[0]->AddAngularVelocity(inverseInertias[0].Matrix3x3Transform(impulsiveTorque0));
	rigidBodies[1]->AddAngularVelocity(inverseInertias[1].Matrix3x3Transform(impulsiveTorque1));
}

//Calculates the impulse (instant change in velocity) required to resolve the collision
Vector3 RigidBodyContact::CalculateImpulse(Matrix3x3* inverseInertiaTensors)
{
	Vector3 impulseCollisionBasis;

	//The change in velocity in world coordinates for each unit of impulse
	Vector3 velocityChange = relPointOfContactPos[0].Vec3CrossProduct(contactNormal);
	velocityChange = inverseInertiaTensors[0].Matrix3x3Transform(velocityChange);
	velocityChange = velocityChange.Vec3CrossProduct(relPointOfContactPos[0]);

	//The change in rotation about the contact normal and the change in linear position about the contact normal
	FFLOAT deltaVelocity = velocityChange.Vec3DotProduct(contactNormal) + rigidBodies[0]->GetInverseMass();

	//And the same for the other body
	velocityChange = relPointOfContactPos[1].Vec3CrossProduct(contactNormal);
	velocityChange = inverseInertiaTensors[1].Matrix3x3Transform(velocityChange);
	velocityChange = velocityChange.Vec3CrossProduct(relPointOfContactPos[1]);

	deltaVelocity += velocityChange.Vec3DotProduct(contactNormal) + rigidBodies[1]->GetInverseMass();

	//Impulse is completely applied along contact normal. None is dissipated along other axes due to lack of friction
	impulseCollisionBasis.x(desiredVelocityDiff / deltaVelocity);
	impulseCollisionBasis.y(0.0f);
	impulseCollisionBasis.z(0.0f);
	return impulseCollisionBasis;
}

//Get singleton instance
RigidBodyContactResolver* RigidBodyContactResolver::GetInstance()
{
	static RigidBodyContactResolver instance;
	return &instance;
}

//Resolves penetration and velocity for the rigid bodies in contact with each other
VVOID RigidBodyContactResolver::ResolveCollisions(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta)
{
	if(numContacts <= 0) //No contacts to resolve, so exit out
	{
		return;
	}

	//Configures the contact data so that they can be resolved by calling the SetInternals function for each contact 
	PrepareBodies(contacts, numContacts, delta);

	//Resolve inter-penetrations by calculating the position changes required then applying those changes
	ResolveInterpenetrations(contacts, numContacts, delta);

	//Resolve after-collision-velocities by calculating the velocity changes required then applying those changes
	ResolveVelocities(contacts, numContacts, delta);
}

//Configures the contact data so that they can be resolved by calling the SetInternals function for each contact 
VVOID RigidBodyContactResolver::PrepareBodies(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta)
{
	for(IRigidBodyContact* contact = contacts; contact < contacts + numContacts; contact++)
	{
		contact->PrepareInternalData(delta);
	}
}

//Resolve inter-penetrations by calculating the position changes required then applying those changes
VVOID RigidBodyContactResolver::ResolveInterpenetrations(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta)
{
	penetrationIterationsUsed = 0;

	BIGINT penetrationIndex;
	FFLOAT largestPenetration;
	
	Vector3 deltaPosition;

	//Store how much each should change their position and orientation
	Vector3 positionChanges[2];
	Vector3 orientationChanges[2];

	//Multiple iterations are used in case resolving one collision introduces another
	while(penetrationIterationsUsed < penetrationIterations)
	{
		largestPenetration = minPenetrationDelta;
		penetrationIndex = numContacts;

		//Loop through each and find the most penetrating contact
		for(BIGINT i = 0; i < (BIGINT)numContacts; ++i)
		{
			if(contacts[i].GetPenetrationDepth() > largestPenetration)
			{
				largestPenetration = contacts[i].GetPenetrationDepth();
				penetrationIndex = i;
			}
		}
		//No penetrating contacts found
		if(penetrationIndex == numContacts) 
		{
			break;
		}

		//Resolve inter-penetration
		contacts[penetrationIndex].ResolvePenetration(positionChanges, orientationChanges, largestPenetration);

		//Update contacts position and penetration data to show this new resolution
		for(BIGINT i = 0; i < (BIGINT)numContacts; ++i) 
		{
			for(BIGINT j = 0; j < 2; ++j)
			{
				if(contacts[i].GetRigidBody(j))
				{
					for(BIGINT k = 0; k < 2; ++k)
					{
						if(contacts[i].GetRigidBody(j) == contacts[penetrationIndex].GetRigidBody(k))
						{
							deltaPosition = positionChanges[k] + orientationChanges[k].Vec3CrossProduct(contacts[i].GetRelativeContactPosition(j));
							contacts[i].SetPenetrationDepth(contacts[i].GetPenetrationDepth() + deltaPosition.Vec3DotProduct(contacts[i].GetContactNormal())* (j ? 1.0f : -1.0f));
						}
					}
				}
			}
		}
		penetrationIterationsUsed++;
	}
}

//Resolve after-collision-velocities by calculating the velocity changes required then applying those changes
VVOID RigidBodyContactResolver::ResolveVelocities(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta)
{
	//Store how much each body should change their velocity and angular velocity
	Vector3 linearVelocityChanges[2];
	Vector3 angularVelocityChanges[2];
	Vector3 deltaVel;

	velocityIterationsUsed = 0;
	//Using multiple iterations as solving one contact may introduce another
	while(velocityIterationsUsed < velocityIterations)
	{
		FFLOAT maxRequiredVelChange = minVelocityDelta;
		BIGINT maxIndex = numContacts;

		//Find the contact the requires the largest change in velocity
		for(BIGINT i = 0; i < (BIGINT)numContacts; ++i)
		{
			if(contacts[i].GetRequiredVelDiff() > maxRequiredVelChange)
			{
				maxRequiredVelChange = contacts[i].GetRequiredVelDiff();
				maxIndex = i;
			}
		}

		//All contacts don't need a change in velocity
		if(maxIndex == numContacts) 
		{
			break;
		}

		//Resolve velocity of contact
		contacts[maxIndex].ResolveVelocity(linearVelocityChanges, angularVelocityChanges);

		//Update the contacts contact velocity and required velocity change to reflect the new resolution
		for(BIGINT i = 0; i < (BIGINT)numContacts; ++i)
		{
			for(BIGINT j = 0; j < 2; ++j)
			{
				if(contacts[i].GetRigidBody(j))
				{
					for(BIGINT k = 0; k < 2; ++k)
					{
						if(contacts[i].GetRigidBody(j) == contacts[maxIndex].GetRigidBody(k))
						{
							deltaVel = linearVelocityChanges[k] + angularVelocityChanges[k].Vec3DotProduct(contacts[i].GetRelativeContactPosition(j));
							contacts[i].SetContactVelocity(contacts[i].GetContactVelocity() + contacts[i].GetContactToWorldMatrix()->Matrix3x3TransformTranspose(deltaVel) * (j? -1.0f : 1.0f));
							contacts[i].UpdateRequiredVelocityChange(delta);
						}
					}
				}
			}
		}
		velocityIterationsUsed++;
	}
}