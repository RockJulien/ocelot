#include "ParticleForceGenerators.h"

using namespace Ocelot;

//Singleton class
ParticleForceRegistry* ParticleForceRegistry::GetInstance()
{
	static ParticleForceRegistry instance;
	return &instance;
}

//Add a registration (a registration consists of a particle and a force generator)
VVOID ParticleForceRegistry::Add(IParticle *particle, IParticleForceGenerator *forceGenerator)
{
	ParticleForceRecord p = {particle, forceGenerator};
	registrations.push_back(p);
}

//Remove a particular registration
VVOID ParticleForceRegistry::Remove(IParticle *particle, IParticleForceGenerator *forceGenerator)
{
	std::vector<ParticleForceRecord>::iterator it;
	for(it = registrations.begin(); it != registrations.end(); ++it)
	{
		if((*it).particle == particle && (*it).forceGenerator == forceGenerator)
		{
			registrations.erase(it);
		}
	}
}

//Remove all registrations
VVOID ParticleForceRegistry::Clear()
{
	registrations.clear();
}

//Called on each loop to update all particles registered with force generators
VVOID ParticleForceRegistry::UpdateForces(const FFLOAT deltaTime)
{
	Registry::iterator itEnd = registrations.end();
	for(Registry::iterator it = registrations.begin();  it != itEnd; ++it)
	{
		//Each force generator follows an interface with this pure virtual function
		it->forceGenerator->UpdateForce(it->particle, deltaTime);
	}
}

//Constructor
ParticleGravityGenerator::ParticleGravityGenerator()
	: gravity(0.0f, 0.0f, 0.0f)
{}

//Constructor
ParticleGravityGenerator::ParticleGravityGenerator(const Vector3 &g)
	: gravity(g)
{}

VVOID ParticleGravityGenerator::UpdateForce(IParticle *particle, const FFLOAT deltaTime)
{
	//Don't add force to anything with "infinite" mass
	if(!particle->HasFiniteMass())
	{
		return;
	}

	//add gravity to accumulated force
	particle->AddForce(gravity * particle->GetMass());
}

//Constructor
ParticleDragGenerator::ParticleDragGenerator()
	: k1(0.0f), k2(0.0f)
{}

//Constructor
ParticleDragGenerator::ParticleDragGenerator(const FFLOAT K1, const FFLOAT K2)
	: k1(K1), k2(K2)
{}

//Calculate drag force
VVOID ParticleDragGenerator::UpdateForce(IParticle *particle, const FFLOAT deltaTime)
{
	//The faster the particle is going, the more drag it receives, so we first need its current velocity
	Vector3 dragForce = particle->GetVelocity();

	//drag force = (k1*speed + k2*speed^2) in the opposite direction of velocity 
	FFLOAT dragCoefficient = dragForce.Vec3Length();
	dragCoefficient = k1 * dragCoefficient + k2 * dragCoefficient * dragCoefficient;
	dragForce = dragForce.Vec3Normalise();
	dragForce *= -dragCoefficient; //minus sign is there because drag acts in the opposite direction of motion

	particle->AddForce(dragForce);
}

//Constructor
ParticleSpringGenerator::ParticleSpringGenerator()
	: otherParticle(nullptr), restLength(0.0f), springConstant(0.0f)
{}

//Constructor
ParticleSpringGenerator::ParticleSpringGenerator(IParticle *_other, const FFLOAT _springConstant, const FFLOAT _restLength)
	: otherParticle(_other), restLength(_restLength), springConstant(_springConstant)
{}

//Calculate spring force
VVOID ParticleSpringGenerator::UpdateForce(IParticle *particle, const FFLOAT deltaTime)
{
	//Calculate relative position
	Vector3 springForce = particle->GetPosition();
	springForce -= otherParticle->GetPosition();

	//f = -kx, x = difference between current relative position and rest length 
	//and placed in the direction of their relative position
	FFLOAT springForceMagnitude = springForce.Vec3Length();
	springForceMagnitude = fabsf(springForceMagnitude - restLength);
	springForceMagnitude *= springConstant;

	springForce = springForce.Vec3Normalise();
	springForce *= springForceMagnitude;

	//Apply spring force
	particle->AddForce(springForce);
}

//Constructor
ParticleAnchoredSpringGenerator::ParticleAnchoredSpringGenerator()
	: anchorPosition(0.0f, 0.0f, 0.0f), restLength(0.0f), springConstant(0.0f)
{}

//Constructor
ParticleAnchoredSpringGenerator::ParticleAnchoredSpringGenerator(Vector3 &_anchor, const FFLOAT _springConstant, const FFLOAT _restLength)
	: anchorPosition(_anchor), restLength(_restLength), springConstant(_springConstant)
{}

//Calculate spring force
VVOID ParticleAnchoredSpringGenerator::UpdateForce(IParticle *particle, const FFLOAT deltaTime)
{
	//Calculate relative position
	Vector3 springForce = particle->GetPosition();
	springForce -= anchorPosition;

	//f = -kx, x = difference between current relative position and rest length 
	//and placed in the direction of their relative position
	FFLOAT springForceMagnitude = springForce.Vec3Length();
	springForceMagnitude = (restLength - springForceMagnitude) * springConstant;
	springForce = springForce.Vec3Normalise();
	springForce *= springForceMagnitude;

	//Apply spring force
	particle->AddForce(springForce);
}

//Constructor
ParticleBungeeGenerator::ParticleBungeeGenerator()
	: otherParticle(nullptr), restLength(0.0f), springConstant(0.0f)
{}

//Constructor
ParticleBungeeGenerator::ParticleBungeeGenerator(IParticle *_other, const FFLOAT _springConstant, const FFLOAT _restLength)
	: otherParticle(_other), restLength(_restLength), springConstant(_springConstant)
{}

//Calculate bungee spring force
VVOID ParticleBungeeGenerator::UpdateForce(IParticle *particle, const FFLOAT deltaTime)
{
	//Calculate relative position
	Vector3 bungeeForce = particle->GetPosition();
	bungeeForce -= otherParticle->GetPosition();

	FFLOAT forceMagnitude = bungeeForce.Vec3Length();
	if(forceMagnitude <= restLength) //If the current distance between the two is less than the rest length, apply no compression (i.e. no force at all!)
	{
		return;
	}

	//else the code is the same as the spring classes above
	forceMagnitude = springConstant * (restLength - forceMagnitude);

	bungeeForce = bungeeForce.Vec3Normalise();
	bungeeForce *= forceMagnitude;

	particle->AddForce(bungeeForce);
}

//Constructor
ParticleBuoyancyGenerator::ParticleBuoyancyGenerator()
	: maxDepth(0.0f), waterVolume(0.0f), waterHeight(0.0f), waterDensity(0.0f)
{}

//Constructor
ParticleBuoyancyGenerator::ParticleBuoyancyGenerator(const FFLOAT _maxDepth, const FFLOAT _volume, const FFLOAT _waterHeight, const FFLOAT _liquidDensity)
	: maxDepth(_maxDepth), waterVolume(_volume), waterHeight(_waterHeight), waterDensity(_liquidDensity)
{}

VVOID ParticleBuoyancyGenerator::UpdateForce(IParticle *particle, const FFLOAT deltaTime)
{
	FFLOAT particleDepth = particle->GetPosition().getY(); //current depth of the particle

	if(particleDepth >= waterHeight + maxDepth) //If we're already mostly out of the water, no need to apply any more forces
	{
		return;
	}

	Vector3 force(0.0f, 0.0f, 0.0f);

	//If we're fully submerged, then apply full force
	if(particleDepth <= waterHeight - maxDepth)
	{
		force.y(waterDensity * waterVolume);
		particle->AddForce(force);
		return;
	}

	//If not fully submerged, then apply a scaled amount of the full force (scales with depth)
	force.y(waterDensity * waterVolume * (particleDepth - maxDepth - waterHeight) / 2.0f * maxDepth);
	particle->AddForce(force);
}