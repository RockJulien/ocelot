#ifndef IRIGIDBODYCONTACTRESOLVER_H
#define IRIGIDBODYCONTACTRESOLVER_H

#include "IRigidBodyContact.h"

/*
 * The interface used by all rigid body contact resolvers in the ocelot engine.
 * The rigid body contact resolver object itself should follow this description:
 *	A contact resolver for rigid bodies that mostly acts as a managing class for multiple
 *	contact data structures. It is only necessary to have one instance of the resolver so it
 *	follows the singleton design pattern. It contains methods to resolve the collisions in the contact
 *	data structures and uses epsilon values where it only resolves velocities and penetrations larger than this epsilon
 *	to increase stability. It uses several iterations when resolving contacts in case resolving one contact re/introduces
 *	another collision point
 */

namespace Ocelot
{
	class IRigidBodyContactResolver
	{
	public:
		//Setters/getters for required variables
		virtual FFLOAT GetPenetrationDelta() const = 0;
		virtual VVOID SetPenetrationDelta(const FFLOAT pEpsilon) = 0;

		virtual FFLOAT GetVelocityDelta() const = 0;
		virtual VVOID SetVelocityDelta(const FFLOAT vEpsilon) = 0;

		virtual UBIGINT GetPenetrationIterations() const = 0;
		virtual VVOID SetPenetrationIterations(const UBIGINT posIterations) = 0;

		virtual UBIGINT GetVelocityIterations() const = 0;
		virtual VVOID SetVelocityIterations(const UBIGINT velIterations) = 0;

		//Resolves penetration and velocity for the rigid bodies in contact with each other
		virtual VVOID ResolveCollisions(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta) = 0;

		//Configures the contact data so that they can be resolved by calling the SetInternals function for each contact 
		virtual VVOID PrepareBodies(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta) = 0;

		//Resolve inter-penetrations by calculating the position changes required then applying those changes
		virtual VVOID ResolveInterpenetrations(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta) = 0;

		//Resolve after-collision-velocities by calculating the velocity changes required then applying those changes
		virtual VVOID ResolveVelocities(IRigidBodyContact* contacts, UBIGINT numContacts, FFLOAT delta) = 0;
	};
}

#endif