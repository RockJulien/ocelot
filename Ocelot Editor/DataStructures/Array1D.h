#ifndef DEF_ARRAY1D_H
#define DEF_ARRAY1D_H

#include "DataStructureUtil.h"

template<class TheType>
class Array1D
{
protected:

	// Attributes
	TheType* m_uData;
	BIGINT   m_iNbData;
	BBOOL    m_bCopy;

public:

	// Constructor & Destructor
	Array1D();
	Array1D(BIGINT iSize);
	Array1D(const Array1D<TheType>& cpy);
	Array1D<TheType>& operator= (const Array1D<TheType>& assgm);
	virtual ~Array1D();

	// Methods
	VVOID map(BIGINT size);
	VVOID unmap();
	VVOID resize(BIGINT newSize);
	VVOID insert(TheType uData, BIGINT iIndex);
	VVOID remove(BIGINT iIndex);
	BBOOL save(const CCHAR* cFileName);
	BBOOL load(const CCHAR* cFileName);

	// Access Operator
	TheType& operator[] (BIGINT iIndex);

	BBOOL    operator == (const Array1D<TheType>& array1D) const;
	BBOOL    operator != (const Array1D<TheType>& array1D) const;

	// Conversion Operator
	operator TheType* ();

	// Accessors
	inline TheType* Array() const { return m_uData;}
	inline VVOID    SetArray(TheType* t) { m_uData = t;}
	inline VVOID    SetCopy(BBOOL state) { m_bCopy = state;}
	inline BIGINT   Size() const { return m_iNbData;}
};

template<class TheType>
Array1D<TheType>::Array1D() :
m_uData(NULL), m_bCopy(false)
{
	map(10);
}

template<class TheType>
Array1D<TheType>::Array1D(BIGINT iSize) :
m_bCopy(false)
{
	map(iSize);
}

template<class TheType>
Array1D<TheType>::Array1D(const Array1D<TheType>& cpy) :
m_bCopy(true)
{
	m_iNbData = cpy.Size();
	m_uData   = cpy.Array();
}

template<class TheType>
Array1D<TheType>& Array1D<TheType>::operator= (const Array1D<TheType>& assgm)
{
	if(m_uData)
	{
		free( m_uData );
		m_uData = 0;
	}

	m_iNbData = assgm.Size();
	m_uData   = assgm.Array();
	m_bCopy   = true;
		
	return *this;
}

template<class TheType>
Array1D<TheType>::~Array1D()
{
	unmap();
}

template<class TheType>
VVOID Array1D<TheType>::map(BIGINT size)
{
	m_iNbData = size;
	m_uData   = (TheType*)malloc(m_iNbData * sizeof(TheType));
	//memset(m_uData, 0, m_iNbData);
}

template<class TheType>
VVOID Array1D<TheType>::unmap()
{
	if(m_uData && !m_bCopy)
	{
		free( m_uData );
		m_uData = 0;
	}

	m_iNbData = 0;
}

template<class TheType>
VVOID Array1D<TheType>::resize(BIGINT newSize)
{
	// avoid to process if same size passed.
	if(newSize == m_iNbData)
		return;

	TheType* newOne = (TheType*)malloc(newSize * sizeof(TheType));//new TheType[newSize];

	if(!newOne)
		return;

	BIGINT smaller;
	if(newSize < m_iNbData)
		smaller = newSize;
	else
		smaller = m_iNbData;

	// copy data respecting the new size.
	for(BIGINT index = 0; index < smaller; index++)
		newOne[index] = m_uData[index];

	// store the new size
	m_iNbData = newSize;

	// destroy the previous one.
	if(m_uData)
	{
		free( m_uData );
		m_uData = 0;
	}

	// then, point to the new one.
	m_uData = newOne;
}

template<class TheType>
VVOID Array1D<TheType>::insert(TheType uData, BIGINT iIndex)
{
	for(BIGINT index = (m_iNbData - 1); index > iIndex; index--)
		m_uData[index] = m_uData[index - 1];

	m_uData[iIndex] = uData;
}

template<class TheType>
VVOID Array1D<TheType>::remove(BIGINT iIndex)
{
	// if we want to remove the last one.
	//if(iIndex == (m_iNbData - 1))

	for(BIGINT index = (iIndex + 1); index < m_iNbData; index++)
		m_uData[index - 1] = m_uData[index];
}

template<class TheType>
BBOOL Array1D<TheType>::save(const CCHAR* cFileName)
{
	FILE* output = 0;
	BIGINT written = 0;

	fopen_s(&output, cFileName, "wb");
	if(!output)
		return false;

	written = fwrite(m_uData, sizeof(TheType), m_iNbData, output);

	fclose(output);
	if(written != m_iNbData)
		return false;

	return true;
}

template<class TheType>
BBOOL Array1D<TheType>::load(const CCHAR* cFileName)
{
	FILE* input = 0;
	BIGINT loaded = 0;

	fopen_s(&input, cFileName, "rb");
	if(!input)
		return false;

	loaded = fread(m_uData, sizeof(TheType), m_iNbData, input);

	fclose(input);
	if(loaded != m_iNbData)
		return false;

	return true;
}

template<class TheType>
TheType& Array1D<TheType>::operator[] (BIGINT iIndex)
{
	return m_uData[iIndex];
}

template<class TheType>
BBOOL    Array1D<TheType>::operator == (const Array1D<TheType>& array1D) const
{
	// insure they have the same size, or else automatically different.
	if(Size() != array1D.Size())
		return false;

	for(UBIGINT i = 0; i < (UBIGINT)array1D.Size(); i++)
	{
		if(NotEqual<TheType>(array1D.Array()[i], Array()[i]))
			return false;
	}
	
	return true;
}

template<class TheType>
BBOOL    Array1D<TheType>::operator != (const Array1D<TheType>& array1D) const
{
	// insure they have the same size, or else automatically different.
	if(Size() != array1D.Size())
		return true;

	for(UBIGINT i = 0; i < (UBIGINT)array1D.Size(); i++)
	{
		if(NotEqual<TheType>(array1D.Array()[i], Array()[i]))
			return true;
	}

	return false;
}

template<class TheType>
Array1D<TheType>::operator TheType* ()
{
	return m_uData;
}

#endif
