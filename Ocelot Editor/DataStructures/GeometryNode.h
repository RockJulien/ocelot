#ifndef DEF_GEOMETRYNODE_H
#define DEF_GEOMETRYNODE_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   GeometryNode.h/.cpp
/
/ Description: This file provide a way to only update an entity in 
/              the scene graph accordingly to its transformations
/              due to parent world matrix and its own transformations.
/              An entity of the game is expected as parameter and 
/              will launch its update method every frame (time dependant).
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "SceneNode.h"
#include "Geometry\IOcelotMesh.h"

class GeometryNode : public SceneNode  // if we want to render the entity via the scene graph
{
private:

	Ocelot::IOcelotMesh*  m_mIEntity;
	Matrix4x4             m_matFinalWorld;

public:

	// Constructor & Destructor
	GeometryNode();
	GeometryNode(Ocelot::IOcelotMesh* entity);
	~GeometryNode();

	// Methods
	void	 update(float deltaTime);  // this method should update the geometry of the entity
	void	 applyScale(float sX, float sY, float sZ){}
	void	 applyTrans(float tX, float tY, float tZ){}
	void	 applyRotat(float rX, float rY, float rZ){}

	// Accessors
	inline Ocelot::IOcelotMesh* getEntity() const{ return m_mIEntity;};
};

#endif