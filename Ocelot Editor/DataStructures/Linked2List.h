#ifndef DEF_LINKED2LIST_H
#define DEF_LINKED2LIST_H

#include "List2Data.h"
#include "List2Iterator.h"

template<class TheType>
class Linked2List
{
protected:

	// Attributes
	BIGINT              m_iNbCells;
	List2Data<TheType>* m_dFirst;
	List2Data<TheType>* m_dLast;

public:

	// Constructor & Destructor
	Linked2List();
	virtual ~Linked2List();

	// Methods
	VVOID putBACK(TheType uData);
	VVOID putFRONT(TheType uData);
	VVOID insert(List2Iterator<TheType>& iter, TheType uData);
	VVOID remove(List2Iterator<TheType>& iter);
	VVOID removeBACK();
	VVOID removeFRONT();
	List2Iterator<TheType> iterator();
	BBOOL save(char* cFileName);
	BBOOL load(char* cFileName);
	VVOID release();
	VVOID clear();

	TheType  researchByIndex(BIGINT index);

	// Accessors
	inline BIGINT              Size() const { return m_iNbCells;}
	inline VVOID               SetSize(BIGINT size) { m_iNbCells = size;}
	inline List2Data<TheType>* First() { return m_dFirst; }
	inline TheType FirstData() { return m_dFirst->Data();}
	inline List2Data<TheType>* Last() { return m_dLast; }
	inline TheType LastData() { return m_dLast->Data();}
};

template<class TheType>
Linked2List<TheType>::Linked2List() :
m_iNbCells(0), m_dFirst(0), m_dLast(0)
{

}

template<class TheType>
Linked2List<TheType>::~Linked2List()
{
	release();
}

template<class TheType>
VVOID Linked2List<TheType>::putBACK(TheType uData)
{
	List2Data<TheType>* newData = new List2Data<TheType>(uData);

	if(!newData)
		return;

	// first check if nothing in the list yet.
	if(!m_dFirst)
	{
		m_dFirst = newData;
		m_dLast  = newData;
	}
	else // else add to the back by updating accordingly.
	{
		m_dLast->SetNext(newData);
		newData->SetPrevious(m_dLast);
		m_dLast = newData;
	}

	// add to the count.
	m_iNbCells++;
}

template<class TheType>
VVOID Linked2List<TheType>::putFRONT(TheType uData)
{
	List2Data<TheType>* newData = new List2Data<TheType>(uData);

	if(!newData)
		return;

	if(!m_dFirst)
	{
		m_dFirst = m_dLast = newData;
	}
	else
	{
		m_dFirst->SetPrevious(newData);
		newData->SetNext(m_dFirst);
		m_dFirst = newData;
	}

	// add to the count.
	m_iNbCells++;
}

template<class TheType>
VVOID Linked2List<TheType>::insert(List2Iterator<TheType>& iter, TheType uData)
{
	// first, check if the iterator belong to this list
	if(this != iter.List())
		return;

	// check if something in the list and it is valid as well.
	if(iter.notEnd())
	{
		if(iter.CurrCell() == m_dFirst)
		{
			if(m_dFirst == m_dLast)
				putBACK(uData);
			else
			{
				// insert normally between two (left and right) nodes.
				List2Data<TheType>* newData = new List2Data<TheType>(uData);

				// link accordingly
				newData->SetNext(iter.CurrCell()->Next());
				newData->SetPrevious(iter.CurrCell());
				iter.CurrCell()->Next()->SetPrevious(newData);
				iter.CurrCell()->SetNext(newData);

				// add to the total.
				m_iNbCells++;
			}
		}
		else if(iter.CurrCell() == m_dLast)
		{
			// just normally put the data at the end.
			putBACK(uData);
		}
		else
		{
			// insert normally between two (left and right) nodes.
			List2Data<TheType>* newData = new List2Data<TheType>(uData);

			// link accordingly
			newData->SetNext(iter.CurrCell()->Next());
			newData->SetPrevious(iter.CurrCell());
			iter.CurrCell()->Next()->SetPrevious(newData);
			iter.CurrCell()->SetNext(newData);

			// add to the total.
			m_iNbCells++;
		}
	}
	else
		// in the case where the iterator is invalid and the list contain nothing
		// just insert the node.
		putBACK(uData);
}

template<class TheType>
VVOID Linked2List<TheType>::remove(List2Iterator<TheType>& iter)
{
	// First, check if the iterator belong to this list
	if(iter.List() != this)
		return;

	// Second, check if there is something in the list
	if(!iter.notEnd())
		return;

	// else, keep a track of the first for iteration
	List2Data<TheType>* temp = m_dFirst;

	// if this is the first we try to delete.
	if(iter.CurrCell() == m_dFirst)
	{
		// iterate once and delete the first.
		iter.next();
		removeFRONT();
	}
	else
	{
		// else, iterate through the list until we reach
		// the previous to the one we want to delete.
		while(temp->Next() != iter.CurrCell())
			temp = temp->Next();

		// now the tempo one is the previous one
		// iterate the iterator once
		iter.next();

		// then, check if this is the tail and update
		// accordingly and delete the node.
		if(temp->Next() == m_dLast)
			m_dLast = temp;

		List2Data<TheType>* toDelete = temp->Next();
		DeletePointer(toDelete);
		// finally, restore the linked list.
		temp->SetNext(iter.CurrCell());
	}

	// subtract to the count.
	m_iNbCells--;
}

template<class TheType>
VVOID Linked2List<TheType>::removeBACK()
{
	List2Data<TheType>* temp = 0;

	if(m_dLast)
	{
		// temporary keep pointer to the next
		temp = m_dLast->Previous();

		// delete the last.
		DeletePointer(m_dLast);

		if(temp)
		{
			// then, restore the next one as the last
			m_dLast = temp;
			m_dLast->SetNext(NULL);
		}
		else
		{
			// if only one cell was present, set the first to NULL as well.
			//if(!m_dLast)
			m_dFirst = 0;
		}

		// and subtract the number of cell.
		m_iNbCells--;
	}
}

template<class TheType>
VVOID Linked2List<TheType>::removeFRONT()
{
	List2Data<TheType>* temp = 0;

	if(m_dFirst)
	{
		// temporary keep pointer to the next
		temp = m_dFirst->Next();

		// delete the first.
		DeletePointer(m_dFirst);

		if(temp)
		{
			// then, restore the next one as the first
			m_dFirst = temp;
			m_dFirst->SetPrevious(NULL);
		}
		else
		{
			// if only one cell was present, set the last to NULL as well.
			//if(!m_dFirst)
			m_dLast = 0;
		}

		// and subtract the number of cell.
		m_iNbCells--;
	}
}

template<class TheType>
List2Iterator<TheType> Linked2List<TheType>::iterator()
{
	return List2Iterator<TheType>(this, m_dFirst);
}

template<class TheType>
BBOOL Linked2List<TheType>::save(char* cFileName)
{
	FILE* output = 0;
	List2Data<TheType>* iterator = m_dFirst;

	// open the file for writing as binary stream.
	output = fopen(cFileName, "wb");

	if(!output)
		return false;

	// write the number of cells of the list.
	fwrite(&m_iNbCells, sizeof(BIGINT), 1, output);

	// then, iterate through the list for writing 
	// data in.
	while(iterator)
	{
		fwrite(&(iterator->Data()), sizeof(TheType), 1, output);
		iterator = iterator->Next();
	}

	// okay, then close the file.
	fclose(output);
	return true;
}

template<class TheType>
BBOOL Linked2List<TheType>::load(char* cFileName)
{
	FILE* input = 0;
	TheType buffer;
	BIGINT nbCells = 0;

	// we open the file prepared to be read as a binary stream.
	input = fopen(cFileName, "rb");

	if(!input)
		return false;

	// read first the number of cell for the list.
	fread(&nbCells, sizeof(BIGINT), 1, input);

	// then, start the loop creation process for every cells.
	while(nbCells != 0)
	{
		fread(&buffer, sizeof(TheType), 1, input);
		putBACK(buffer);
		nbCells--;
	}

	// then, close the file, over.
	fclose(input);
	return true;
}

template<class TheType>
VVOID Linked2List<TheType>::release()
{
	// If the destructor is not called (everything is possible
	// sometimes), just call it yourself.
	if(m_iNbCells != 0)
	{
		List2Data<TheType>* iter = m_dFirst;
		List2Data<TheType>* next = 0;

		// destroy the list
		while(iter)
		{
			next = iter->Next();
			DeletePointer(iter);
			iter = next;
		}

		m_iNbCells = 0;
	}
}

template<class TheType>
VVOID Linked2List<TheType>::clear()
{
	if(m_iNbCells != 0)
	{
		List2Data<TheType>* iter = m_dFirst;
		List2Data<TheType>* next = 0;

		// destroy the list
		while(iter)
		{
			next = iter->Next();
			delete iter->Data();
			DeletePointer(iter);
			iter = next;
		}

		m_iNbCells = 0;
	}
}

template<class TheType>
TheType  Linked2List<TheType>::researchByIndex(BIGINT index)
{
	List2Data<TheType>* temp = First();
	if (temp == NULL) cout << " You can't research in an empty list !!!" << endl;

	// traverse the list
	int lCounter = 0;
	while (temp != NULL && 
		   lCounter != index)
	{
		temp = temp->Next();
		lCounter++;
	}

	if (temp == NULL)
	{
		cout << " This object doesn't exist :-(( " << endl;
		return NULL;
	}
	else
		cout << " I got it :-)) " << endl;

	return temp->Data();
}

#endif