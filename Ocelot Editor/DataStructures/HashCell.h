#ifndef DEF_HASHCELL_H
#define DEF_HASHCELL_H

template<class KeyType, class DataType>
class HashCell
{
private:

	// Attributes
	KeyType  m_uKey;
	DataType m_uData;

public:

	// Constructor & Destructors
	HashCell();
	HashCell(KeyType uKey, DataType uData);
	~HashCell();

	// Methods


	// Accessors
	inline KeyType  Key() const { return m_uKey;}
	inline DataType Data() const { return m_uData;}
};

template<class KeyType, class DataType>
HashCell<KeyType, DataType>::HashCell()
{
	
}

template<class KeyType, class DataType>
HashCell<KeyType, DataType>::HashCell(KeyType uKey, DataType uData) : 
m_uKey(uKey), m_uData(uData)
{

}

template<class KeyType, class DataType>
HashCell<KeyType, DataType>::~HashCell()
{

}

#endif