#ifndef DEF_LISTDATA_H
#define DEF_LISTDATA_H

#include "DataStructureUtil.h"

template<class TheType>
class ListData
{
private:

	// Attributes
	TheType   m_uData;
	ListData* m_dNext;

public:

	// Constructor & Destructor
	ListData();
	ListData(TheType uData);
	~ListData();

	// Methods
	SMALLINT insertData(TheType uData);

	// Accessors
	inline TheType   Data() const { return m_uData;}
	inline ListData* Next() const { return m_dNext;}
	inline VVOID     SetNext(ListData* dNext) { m_dNext = dNext;}
};

template<class TheType>
ListData<TheType>::ListData() : 
m_dNext(0)
{

}

template<class TheType>
ListData<TheType>::ListData(TheType uData) : 
m_uData(uData), m_dNext(0)
{
	
}

template<class TheType>
ListData<TheType>::~ListData()
{

}

template<class TheType>
SMALLINT ListData<TheType>::insertData(TheType uData)
{
	// Create the new Node by inserting the data in.
	ListData<TheType>* newData = new ListData<TheType>(uData);

	if(!newData)
		return BAD;

	// because we insert the new one straight after the head one,
	// the new one point to the one which was previously the next.
	newData->m_dNext = m_dNext;

	// then the next one point to the new node.
	m_dNext = newData;

	return GOOD;
}

#endif