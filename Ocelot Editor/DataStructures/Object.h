#ifndef DEF_OBJECT_H
#define DEF_OBJECT_H

/***************************************************************************
/ File name:	Object.h
/ Description:	This is a header file of a template class !!!
/				This class is needed by the linkedList class which is also 
/               a template for allowing us to put whatever we want in the 
/               linkedlist. This class has also a pointer toward the previous
/               and the next Object of the list. which allow us to traverse
/               the list in whatever direction for increasing the speed
/               of a search for example.
/
/ Author: 		Larbi Julien
/ Version		1.0
/ Date:		    13/12/85
/***************************************************************************/

#include <iostream>
#include <cassert>
#include "..\Maths\Utility.h"

template<class object> class Object
{

private:

		BIGINT             m_iIndex;
		object          m_object;
		Object<object>* m_next;
		Object<object>* m_previous;

public:

		// Constructor & Destructor
		Object();
		~Object();

		// Getters & Setters
		VVOID			setIndex(BIGINT index);
		VVOID			setObject(object newOne);
		VVOID			setNext(Object<object>* newNext);
		VVOID			setPrevious(Object<object>* newPrevious);
		BIGINT             getIndex() const;
		object			getObject() const;
		Object<object>* getNext() const;
		Object<object>* getPrevious() const;
};

#endif

template<class object>
Object<object>::Object(): m_object(0), m_next(0)
{

}

template<class object>
Object<object>::~Object()
{
	
}

template<class object>
VVOID Object<object>::setIndex(BIGINT index)
{
	m_iIndex = index;
}

template<class object>
VVOID Object<object>::setObject(object newOne)
{
	m_object = newOne;
}

template<class object>
VVOID Object<object>::setNext(Object<object>* newNext)
{
	m_next = newNext;
}

template<class object>
VVOID Object<object>::setPrevious(Object<object>* newPrevious)
{
	m_previous = newPrevious;
}

template<class object>
BIGINT Object<object>::getIndex() const
{
	return m_iIndex;
}

template<class object>
object Object<object>::getObject() const
{
	return m_object;
}

template<class object>
Object<object>* Object<object>::getNext() const
{
	return m_next;
}

template<class object>
Object<object>* Object<object>::getPrevious() const
{
	return m_previous;
}