#ifndef DEF_AGRAPH_H
#define DEF_AGRAPH_H

#include "GraphNode.h"
#include "GraphBridge.h"
#include "Array1D.h"
#include "LinkedQueue.h"

template<class NodeType, class BridgeType>
class AGraph
{
public:

	// Re-Definition
	typedef GraphNode<NodeType, BridgeType>   Node;
	typedef GraphBridge<NodeType, BridgeType> Bridge;

private:

	// Attributes
	UBIGINT        m_iNbNodes;
	Array1D<Node*> m_aNodes;
	// The need in a graph to add or delete a node should not be very often
	// and because an array has a faster elements access, it will be better.

public:

	// Constructor & Destructor
	AGraph(BIGINT aSize);
	~AGraph();

	// Methods
	BBOOL   addNode(NodeType uData, UBIGINT iIndex);
	VVOID   remNode(UBIGINT iIndex);
	BBOOL   addBridge(UBIGINT iFrom, UBIGINT iTo, BridgeType uCost);
	VVOID   remBridge(UBIGINT iFrom, UBIGINT iTo);
	Bridge* isWay(UBIGINT iFrom, UBIGINT iTo);
	VVOID   clearCheck(); // useful when starting a new search.
	// Search algo!!!
	VVOID   depthFS(Node* node, VVOID (*functionToApply)(Node*));
	VVOID   breadthFS(Node* node, VVOID (*functionToApply)(Node*));

	// Accessors
	inline UBIGINT        NodeCount() const { return m_iNbNodes;}
	inline Array1D<Node*> Nodes() const { return m_aNodes;}
};

template<class NodeType, class BridgeType>
AGraph<NodeType, BridgeType>::AGraph(BIGINT aSize) :
m_aNodes(aSize), m_iNbNodes(0)
{
	for(UBIGINT i = 0; i < aSize; i++)
		m_aNodes[i] = 0;
}

template<class NodeType, class BridgeType>
AGraph<NodeType, BridgeType>::~AGraph()
{
	// destroy every nodes of the array
	for(UBIGINT i = 0; i < m_aNodes.Size(); i++)
		if(m_aNodes[i])
			DeletePointer(m_aNodes[i]);
}

template<class NodeType, class BridgeType>
BBOOL   AGraph<NodeType, BridgeType>::addNode(NodeType uData, UBIGINT iIndex)
{
	// if there is already something, return false.
	if(m_aNodes[iIndex])
		return false;

	// else create a new Node which will be stored
	// at the index.
	m_aNodes[iIndex] = new Node(uData);
	// then, add to the count.
	m_iNbNodes++;
	return true;
}

template<class NodeType, class BridgeType>
VVOID   AGraph<NodeType, BridgeType>::remNode(UBIGINT iIndex)
{
	// if nothing at the index, return.
	if(!m_aNodes[iIndex])
		return;

	Bridge* bridge;
	for(UBIGINT currNode = 0; currNode < m_aNodes.Size(); currNode++)
		if(m_aNodes[currNode])
		{
			// check if a bridge exist between each node and the node we want to delete.
			bridge = m_aNodes[currNode]->isWay(m_aNodes[iIndex]);
			// if there is one, delete it.
			if(bridge)
				remBridge(currNode, iIndex);
		}

	// once every bridges are deleted
	// the node could be deleted.
	DeletePointer(m_aNodes[iIndex]);
	// and subtract a node to the count.
	m_iNbNodes--;
}

template<class NodeType, class BridgeType>
BBOOL   AGraph<NodeType, BridgeType>::addBridge(UBIGINT iFrom, UBIGINT iTo, BridgeType uCost)
{
	// if one of the two nodes between which we want to add a bridge 
	// does not exist, return false.
	if(!m_aNodes[iFrom] || !m_aNodes[iTo])
		return false;

	// if there is already a bridge between these two nodes already
	// no need to add the same, return false.
	if(m_aNodes[iFrom]->isWay(m_aNodes[iTo]))
		return false;

	// if everything is okay, add the bridge between both.
	m_aNodes[iFrom]->addSConn(m_aNodes[iTo], uCost);
	// and return true.
	return true;
}

template<class NodeType, class BridgeType>
VVOID   AGraph<NodeType, BridgeType>::remBridge(UBIGINT iFrom, UBIGINT iTo)
{
	// check if the two nodes exist.
	if(!m_aNodes[iFrom] && !m_aNodes[iTo])
		return;

	// if they exist, remove the bridge
	m_aNodes[iFrom]->remSConn(m_aNodes[iTo]);
}

template<class NodeType, class BridgeType>
GraphBridge<NodeType, BridgeType>* AGraph<NodeType, BridgeType>::isWay(UBIGINT iFrom, UBIGINT iTo)
{
	// again, check if nodes exist, else return NULL.
	if(!m_aNodes[iFrom] || !m_aNodes[iTo])
		return 0;

	// if exist, return the bridge pointer if there is one or a NULL pointer.
	return m_aNodes[iFrom]->isWay(m_aNodes[iTo]);
}

template<class NodeType, class BridgeType>
VVOID   AGraph<NodeType, BridgeType>::clearCheck()
{
	for(UBIGINT i = 0; i < m_aNodes.Size(); i++)
		if(m_aNodes[i])
			m_aNodes[i]->NotChecked();
}

template<class NodeType, class BridgeType>
VVOID   AGraph<NodeType, BridgeType>::depthFS(Node* node, VVOID (*functionToApply)(Node*))
{
	// check if the node is not NULL.
	if(!node)
		return;

	// then, process the node and mark it as checked.
	functionToApply(node);
	node->Checked();
	// create an iterator for iterating through every bridges.
	List2Iterator<Bridge> iterator = node->Bridges().iterator();
	for(iterator.begin(); iterator.notEnd(); iterator.next())
		if(!iterator.data().Connection()->isChecked())
			depthFS(iterator.data().Connection(), functionToApply);
}

template<class NodeType, class BridgeType>
VVOID   AGraph<NodeType, BridgeType>::breadthFS(Node* node, VVOID (*functionToApply)(Node*))
{
	// check if the node is not NULL.
	if(!node)
		return;

	// prepare a queue for storing nodes checked
	// and an iterator through the bridges.
	LinkedQueue<Node*>    queue;
	List2Iterator<Bridge> iterator;
	// first, put in the queue the first node and mark it as checked.
	queue.pushBACK(node);
	node->Checked();

	// then, enter into the while loop which will finish only
	// if the queue is empty.
	while(queue.Size() != 0)
	{
		// apply the function to the node
		functionToApply(queue.poleValue());
		iterator = queue.poleValue()->Bridges().iterator();
		// then, iterate through the list of bridges
		for(iterator.begin(); iterator.notEnd(); iterator.next())
			if(!iterator.data().Connection()->isChecked())
			{
				iterator.data().Connection()->Checked();
				queue.pushBACK(iterator.data().Connection());
			}

		// and pop the previous first node which has been processed
		// for passing to the next one in the queue.
		queue.popFRONT();
	}
}

#endif