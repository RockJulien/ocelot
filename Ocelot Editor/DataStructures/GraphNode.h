#ifndef DEF_GRAPHNODE_H
#define DEF_GRAPHNODE_H

#include "GraphBridge.h"
#include "Linked2List.h"

template<class NodeType, class BridgeType>
class GraphNode
{
public:

	// Re-Definition
	typedef GraphNode<NodeType, BridgeType>   Node;
	typedef GraphBridge<NodeType, BridgeType> Bridge;

private:

	// Attributes
	NodeType            m_uData;
	BBOOL               m_bChecked;
	Linked2List<Bridge> m_lBridges;

public:

	// Constructor & Destructor
	GraphNode(NodeType uData);
	~GraphNode();

	// Methods
	VVOID   addSConn(Node* nodeToConn, BridgeType uCost);
	VVOID   remSConn(Node* to);
	Bridge* isWay(Node* to);

	// Accessors
	inline NodeType            Data() const { return m_uData;}
	inline BBOOL               isChecked() const { return m_bChecked;}
	inline Linked2List<Bridge> Bridges() const { return m_lBridges;}
	inline VVOID               NotChecked() { m_bChecked = false;}
	inline VVOID               Checked() { m_bChecked = true;}
};

template<class NodeType, class BridgeType>
GraphNode<NodeType, BridgeType>::GraphNode(NodeType uData) :
m_uData(uData), m_bChecked(false)
{

}

template<class NodeType, class BridgeType>
GraphNode<NodeType, BridgeType>::~GraphNode()
{

}

template<class NodeType, class BridgeType>
VVOID   GraphNode<NodeType, BridgeType>::addSConn(Node* nodeToConn, BridgeType uCost)
{
	// create a new bridge/edge/arc whatever we call it.
	Bridge bridge(uCost);
	// set the connection (could be added in the constructor)
	bridge.SetConnection(nodeToConn);
	// then, add it to the list.
	m_lBridges.putBACK(bridge);
}

template<class NodeType, class BridgeType>
VVOID   GraphNode<NodeType, BridgeType>::remSConn(Node* to)
{
	// Create an iterator for iterate through bridges of this one.
	List2Iterator<Bridge> iterator = m_lBridges.iterator();
	// iterate through it.
	for(iterator.begin(); iterator.notEnd(); iterator.next())
		if(iterator.data().Connection() == to)
		{
			m_lBridges.remove(iterator);
			return;
		}
}

template<class NodeType, class BridgeType>
GraphBridge<NodeType, BridgeType>* GraphNode<NodeType, BridgeType>::isWay(Node* to)
{
	// Create an iterator for iterate through bridges of this one.
	List2Iterator<Bridge> iterator = m_lBridges.iterator();
	// iterate through it.
	for(iterator.begin(); iterator.notEnd(); iterator.next())
		if(iterator.data().Connection() == to)
			return &(iterator.data());
	// if no bridge between, return NULL.
	return 0;
}

#endif