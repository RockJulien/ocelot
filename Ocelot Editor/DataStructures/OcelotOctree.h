#ifndef DEF_OCELOTOCTREE_H
#define DEF_OCELOTOCTREE_H

#include "TreeNode.h"

template<class TheType, class SearchType>
class OcelotOctree : public TreeNode<TheType, SearchType>
{
private:

	// Attributes

public:

	// Constructor & Destructor
	OcelotOctree();
	OcelotOctree(TheType newOne);
	~OcelotOctree();

	// Methods
	VVOID   addChild(TreeNode<TheType, SearchType>* newChild, SMALLINT index = -1); // limit index to 0-7. 8 children.
	VVOID   remove(SMALLINT index);                                     // limit index to 0-7. 8 children.

	// Accessors

};

template<class TheType, class SearchType>
OcelotOctree<TheType, SearchType>::OcelotOctree() :
TreeNode<TheType, SearchType>()
{
	
}

template<class TheType, class SearchType>
OcelotOctree<TheType, SearchType>::OcelotOctree(TheType newOne) : 
TreeNode<TheType, SearchType>(newOne)
{
	
}

template<class TheType, class SearchType>
OcelotOctree<TheType, SearchType>::~OcelotOctree()
{
	//destroy();
}

template<class TheType, class SearchType>
VVOID   OcelotOctree<TheType, SearchType>::addChild(TreeNode<TheType, SearchType>* newChild, SMALLINT index)
{
	// Assert that if index is different from -1, it is between 0 and 7 (maximum children count in an octree).
	if(index > 7)
		return;

	// If full octree: 8 and index = -1 meaning to add the newChild as a new cell.
	if((m_lChildren->Size() == 8) && (index < 0))
	{
		DeletePointer(newChild); // destroy avoiding mem. leaks because will not be inserted in the list.
		return;
	}

	TreeNode<TheType, SearchType>::addChild(newChild, index);
}

template<class TheType, class SearchType>
VVOID   OcelotOctree<TheType, SearchType>::remove(SMALLINT index)
{
	// Assert that if index is different from -1, it is between 0 and 7 (maximum children count in an octree).
	if(index > 7)
		return;

	TreeNode<TheType, SearchType>::remove(index);
}

#endif