#ifndef DEF_VECTOREXT_H
#define DEF_VECTOREXT_H

// Includes
#include "VectorIterator.h"

// Namespaces
namespace Ocelot
{
	// Class definition
	template<class TYPE>
	class VectorExt
	{
	public:

		typedef BBOOL (*AnyDelegate)(TYPE pOther);

	private:

		// Attributes
		std::vector<TYPE> mVector;

	public:

		// Constructors & Destructor
		VectorExt();
		~VectorExt();

		// Methods
		VVOID  Add(const TYPE& pValue);
		VVOID  Insert(BIGINT pIndex, const TYPE& pValue);
		VVOID  SetAt(BIGINT pIndex, const TYPE& pValue);
		TYPE   GetAt(BIGINT pIndex) const;
		BIGINT Size() const;
		BBOOL  Any() const;
		BBOOL  Any(AnyDelegate pDelegate);
		VVOID  Remove(BIGINT pIndex);
		VVOID  RemoveAll();

		// Iterator
		VectorIter<TYPE> Iterator();
		VectorIter<TYPE> Iterator() const;

		// Operators
		TYPE& operator [](BIGINT pIndex);
		const TYPE& operator [](BIGINT pIndex) const;

		// Extension Methods
		BBOOL  Contains(const TYPE& pValue) const;
		BIGINT IndexOf(const TYPE& pValue) const;
		BIGINT IndexOf(const TYPE& pValue, BIGINT pStart) const;
		BIGINT IndexOf(const TYPE& pValue, BIGINT pIndex, BIGINT pNumElements) const;
		BIGINT LastIndexOf(const TYPE& pValue) const;
		BIGINT LastIndexOf(const TYPE& pValue, BIGINT pIndex) const;
		BIGINT LastIndexOf(const TYPE& pValue, BIGINT pIndex, BIGINT pNumElements) const;

		friend class VectorIter<TYPE>;
	};

template<class TYPE>
VectorExt<TYPE>::VectorExt() :
mVector()
{

}

template<class TYPE>
VectorExt<TYPE>::~VectorExt()
{
	this->mVector.clear();
}

template<class TYPE>
VVOID VectorExt<TYPE>::Add(const TYPE& pValue)
{
	this->mVector.push_back( pValue );
}

template<class TYPE>
VVOID VectorExt<TYPE>::Insert(BIGINT pIndex, const TYPE& pValue)
{
	if 
		( pIndex < 0 ||
		  pIndex >= this->mVector.size() )
	{
		return;
	}

	this->mVector.insert( this->mVector.begin() + pIndex );
}

template<class TYPE>
VVOID VectorExt<TYPE>::SetAt(BIGINT pIndex, const TYPE& pValue)
{
	// Validate arguments
	if (pIndex < 0 ||
		pIndex >= this->Size())
	{
		return;
	}

	this->mVector[pIndex] = pValue;
}

template<class TYPE>
TYPE VectorExt<TYPE>::GetAt(BIGINT pIndex) const
{
	return this->mVector[pIndex];
}

template<class TYPE>
BIGINT VectorExt<TYPE>::Size() const
{
	return (BIGINT)this->mVector.size();
}

template<class TYPE>
BBOOL  VectorExt<TYPE>::Any() const
{
	return this->Size() > 0;
}

template<class TYPE>
BBOOL  VectorExt<TYPE>::Any(AnyDelegate pDelegate)
{
	std::vector<TYPE>::iterator lIter = this->mVector.begin();
	for 
		( ; lIter != this->mVector.end(); lIter++ )
	{
		if 
			( pDelegate((*lIter)) )
		{
			return true;
		}
	}

	return false;
}

template<class TYPE>
VVOID VectorExt<TYPE>::Remove(BIGINT pIndex)
{
	if 
		( pIndex < 0 || 
		  pIndex >= this->Size() )
	{
		return;
	}

	this->mVector.erase( this->mVector.begin() + pIndex );
}

template<class TYPE>
VVOID VectorExt<TYPE>::RemoveAll()
{
	// Make sure to release pointers if any before.
	this->mVector.clear();
}

template<class TYPE>
VectorIter<TYPE> VectorExt<TYPE>::Iterator()
{
	return VectorIter<TYPE>(this);
}

template<class TYPE>
VectorIter<TYPE> VectorExt<TYPE>::Iterator() const
{
	return VectorIter<TYPE>( this );
}

template<class TYPE>
TYPE& VectorExt<TYPE>::operator [](BIGINT pIndex)
{
	return this->mVector[pIndex];
}

template<class TYPE>
const TYPE& VectorExt<TYPE>::operator [](BIGINT pIndex) const
{
	return this->mVector[pIndex];
}

template<class TYPE>
BBOOL  VectorExt<TYPE>::Contains(const TYPE& pValue) const
{
	return (-1 != this->IndexOf(pValue));
}

template<class TYPE>
BIGINT VectorExt<TYPE>::IndexOf(const TYPE& pValue) const
{
	return (this->Size() > 0) ? this->IndexOf(pValue, 0, this->Size()) : -1;
}

template<class TYPE>
BIGINT VectorExt<TYPE>::IndexOf(const TYPE& pValue, BIGINT pStart) const
{
	return this->IndexOf(pValue, pStart, this->Size() - pStart);
}

template<class TYPE>
BIGINT VectorExt<TYPE>::IndexOf(const TYPE& pValue, BIGINT pIndex, BIGINT pNumElements) const
{
	// Validate arguments
	if (pIndex < 0 ||
		pIndex >= this->Size() ||
		pNumElements < 0 ||
		pIndex + pNumElements > this->Size())
	{
		return -1;
	}

	// Search
	for (BIGINT i = pIndex; i < (pIndex + pNumElements); i++)
	{
		if (pValue == this->mVector[i])
		{
			return i;
		}
	}

	// Not found
	return -1;
}

template<class TYPE>
BIGINT VectorExt<TYPE>::LastIndexOf(const TYPE& pValue) const
{
	return (this->Size() > 0) ? this->LastIndexOf(pValue, this->Size() - 1, this->Size()) : -1;
}

template<class TYPE>
BIGINT VectorExt<TYPE>::LastIndexOf(const TYPE& pValue, BIGINT pIndex) const
{
	return this->LastIndexOf(pValue, pIndex, pIndex + 1);
}

template<class TYPE>
BIGINT VectorExt<TYPE>::LastIndexOf(const TYPE& pValue, BIGINT pIndex, BIGINT pNumElements) const
{
	// Validate arguments
	if (pIndex < 0 ||
		pIndex >= this->Size() ||
		pNumElements < 0 ||
		pIndex - pNumElements < 0)
	{
		return -1;
	}

	// Search
	for (BIGINT i = pIndex; i >(pIndex - pNumElements); i--)
	{
		if (pValue == this->mVector[i])
		{
			return i;
		}
	}

	// Not found
	return -1;
}

}

#endif