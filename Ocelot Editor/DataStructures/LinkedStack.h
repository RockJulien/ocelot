#ifndef DEF_LINKEDSTACK_H
#define DEF_LINKEDSTACK_H

#include "Linked2List.h"

template<class TheType>
class LinkedStack : public Linked2List<TheType>
{
private:

	// Attributes

public:

	// Constructor & Destructor
	LinkedStack();
	~LinkedStack();

	// Methods
	VVOID   push(TheType uData);
	VVOID   pop();
	TheType top();

	// Accessors

};

template<class TheType>
LinkedStack<TheType>::LinkedStack() :
Linked2List<TheType>()
{

}

template<class TheType>
LinkedStack<TheType>::~LinkedStack()
{
	Linked2List<TheType>::~Linked2List<TheType>();
}

template<class TheType>
VVOID   LinkedStack<TheType>::push(TheType uData)
{
	putBACK(uData);
}

template<class TheType>
VVOID   LinkedStack<TheType>::pop()
{
	removeBACK();
}

template<class TheType>
TheType LinkedStack<TheType>::top()
{
	return m_dLast->Data();
}

#endif