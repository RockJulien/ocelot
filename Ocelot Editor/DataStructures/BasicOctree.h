#ifndef DEF_BASICOCTREE_H
#define DEF_BASICOCTREE_H

#include "DataStructureUtil.h"

namespace Ocelot
{
	template<typename TheType>
	class BasicOctree
	{
	private:

		typedef BasicOctree<TheType> Node;

		// Attributes
		TheType m_uData;
		Node*   m_nChildren[8];

	public:

		// Constructor & Destructor
		BasicOctree();
		BasicOctree(TheType uData);
		~BasicOctree();

		// Methods
		VVOID applyToTree(VVOID (*functionToApply)(Node*));
		VVOID updateTree(VVOID (*updateFunction)(Node*, FFLOAT deltaTime), FFLOAT deltaTime);
		VVOID destroy();

		// Accessors
		inline TheType Data() { return m_uData;}
		inline Node**  Child(USMALLINT sibInd) { if(sibInd < 8) return &m_nChildren[sibInd]; return NULL;}
		inline VVOID   SetData(TheType uData) { m_uData = uData;}
		inline VVOID   SetChild(USMALLINT sibInd, Node* sibling) { if(sibInd < 8) m_nChildren[sibInd] = sibling;}
	};

	template<typename TheType>
	BasicOctree<TheType>::BasicOctree()
	{
		for(USMALLINT currChild = 0; currChild < 8; currChild++)
		{
			m_nChildren[currChild] = NULL;
		}
	}

	template<typename TheType>
	BasicOctree<TheType>::BasicOctree(TheType uData) :
	m_uData(uData)
	{
		for(USMALLINT currChild = 0; currChild < 8; currChild++)
		{
			m_nChildren[currChild] = NULL;
		}
	}

	template<typename TheType>
	BasicOctree<TheType>::~BasicOctree()
	{
		destroy();
	}

	template<typename TheType>
	VVOID BasicOctree<TheType>::applyToTree(VVOID (*functionToApply)(Node*))
	{
		functionToApply(this);

		for(USMALLINT currChild = 0; currChild < 8; currChild++)
		{
			if(m_nChildren[currChild])
			{
				m_nChildren[currChild]->applyToTree(functionToApply);
			}
		}
	}

	template<typename TheType>
	VVOID BasicOctree<TheType>::updateTree(VVOID (*updateFunction)(Node*, FFLOAT deltaTime), FFLOAT deltaTime)
	{
		updateFunction(this, deltaTime);

		for(USMALLINT currChild = 0; currChild < 8; currChild++)
		{
			if(m_nChildren[currChild])
			{
				m_nChildren[currChild]->updateTree(updateFunction, deltaTime);
			}
		}
	}
	
	template<typename TheType>
	VVOID BasicOctree<TheType>::destroy()
	{
		// Destroy octree's nodes.
		for(USMALLINT currChild = 0; currChild < 8; currChild++)
		{
			DeletePointer(m_nChildren[currChild]);
		}
	}
}

#endif