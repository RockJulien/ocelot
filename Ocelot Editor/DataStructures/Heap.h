#ifndef DEF_HEAP_H
#define DEF_HEAP_H

#include "Array1D.h"

// Heap priority queue using array instead of linked list
// for faster algo (*6 speed)

template<class TheType>
class Heap : public Array1D<TheType>
{
private:

	// Attributes
	BIGINT  m_iNbCell;
	BIGINT  (*m_compare)(TheType, TheType);

public:

	// Constructor & Destructor
	Heap(BIGINT size, BIGINT (*compare)(TheType, TheType));
	~Heap();

	// Methods
	VVOID putBACK(TheType uData);
	VVOID replaceUP(BIGINT index);
	VVOID popFRONT();
	VVOID replaceDOWN(BIGINT index);

	// Accessors
	inline TheType Root() const { return m_uData[1];}
};

template<class TheType>
Heap<TheType>::Heap(BIGINT size, BIGINT (*compare)(TheType, TheType)) :
Array1D<TheType>(size + 1), m_iNbCell(0), m_compare(compare)
{

}

template<class TheType>
Heap<TheType>::~Heap()
{
	Array1D<TheType>::~Array1D<TheType>();
}

template<class TheType>
VVOID Heap<TheType>::putBACK(TheType uData)
{
	// increment first for avoiding to use 0.
	m_iNbCell++;
	// check if the new one does not overflow
	// the heap. if it does, rezise.
	if(m_iNbCell >= m_iNbData)
		resize(m_iNbData * 2);
	// then, store the new data.
	m_uData[m_iNbCell] = uData;
	// then, trigger the replaceUP algo for
	// insuring that the new data is properly
	// place between a bigger parent and a smaller child.
	replaceUP(m_iNbCell);
}

template<class TheType>
VVOID Heap<TheType>::replaceUP(BIGINT index)
{
	BIGINT parentIndex = index * 0.5f; // way to find the parent index in an arrayed binary tree.
	BIGINT childIndex  = index;
	TheType tempo      = m_uData[childIndex];

	// then, iterate up till there is a valid parent
	while(parentIndex > 0)
		// and compare the new data with the parent one for swapping if it is necessary
		if(m_compare(tempo, m_uData[parentIndex]) > 0)
		{
			// swap only the parent instead of the child
			m_uData[childIndex] = m_uData[parentIndex];
			// reset indexes to the new right value for keeping testing if need to go up.
			childIndex  = parentIndex;
			parentIndex *= 0.5f;
		}
		else // else, if parents are no more bigger than the new data, break it is over.
			break;

	//then, if no need to swap anymore, put the child which went up to its last and final position.
	m_uData[childIndex] = tempo;
}

template<class TheType>
VVOID Heap<TheType>::popFRONT()
{
	// this dequeue function just overwrite
	// the root cell. (Will need another function for accessing the root before deleting).
	if(m_iNbCell >= 1)
	{
		// we know that the root is at the position 1 (nothing at 0 but can correct that by making minus 1 to every indexes).
		m_uData[1] = m_uData[m_iNbCell]; // overwrite the root with the bottom-right-most.
		replaceDOWN(1);
		m_iNbCell--; // minus 1 to the total.
	}
}

template<class TheType>
VVOID Heap<TheType>::replaceDOWN(BIGINT index)
{
	// this time the index in parameter is the parent one.
	BIGINT parentIndex = index;
	// and the trick for having the child one regarding to this one is the following.
	BIGINT childIndex  = index * 2;
	// put the one to change in a tempo var.
	TheType tempo = m_uData[parentIndex];

	// then, iterate through the children
	while(childIndex < m_iNbCell)
	{
		if(childIndex < (m_iNbCell - 1))
			// compare the left and right child for knowing which one is the bigger.
			if(m_compare(m_uData[childIndex], m_uData[childIndex + 1]) < 0)
				childIndex++;

		if(m_compare(tempo, m_uData[childIndex]) < 0)
		{
			// swap only upward the child instead of its parent if needed.
			m_uData[parentIndex] = m_uData[childIndex];
			// reset the indexes accordingly (DOWN a level).
			parentIndex = childIndex;
			childIndex *= 2;
		}
		else
			break;
	}

	// finally, once finished, store the tempo which one the one to move down
	// at its final position.
	m_uData[parentIndex] = tempo;
}

#endif