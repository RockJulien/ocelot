#ifndef DEF_ARRAY3D_H
#define DEF_ARRAY3D_H

#include "DataStructureUtil.h"

template<class TheType>
class Array3D
{
private:

	// Attributes
	TheType* m_uData;
	BIGINT   m_iWidth;
	BIGINT   m_iHeight;
	BIGINT   m_iDepth;

public:

	// Constructor & Destructor
	Array3D(BIGINT iWidth, BIGINT iHeight, BIGINT iDepth);
	~Array3D();

	// Methods
	VVOID    resize(BIGINT iNewWidth, BIGINT iNewHeight, BIGINT iNewDepth);
	VVOID    insert(TheType uData, BIGINT iIndex);
	VVOID    remove(BIGINT iIndex);
	TheType& value(BIGINT iX, BIGINT iY, BIGINT iZ);
	VVOID    setValue(TheType uData, BIGINT iX, BIGINT iY, BIGINT iZ);

	// Accessors
	BIGINT   Size() const { return (m_iWidth * m_iHeight * m_iDepth);}
};

template<class TheType>
Array3D<TheType>::Array3D(BIGINT iWidth, BIGINT iHeight, BIGINT iDepth) :
m_iWidth(iWidth), m_iHeight(iHeight), m_iDepth(iDepth)
{
	m_uData = new TheType[iWidth * iHeight * iDepth];
}

template<class TheType>
Array3D<TheType>::~Array3D()
{
	if(m_uData)
		DeleteArray1D(m_uData);
}

template<class TheType>
VVOID    Array3D<TheType>::resize(BIGINT iNewWidth, BIGINT iNewHeight, BIGINT iNewDepth)
{
	TheType* newOne = new TheType[iNewWidth * iNewHeight * iNewDepth];
	if(!newOne)
		return;

	BIGINT t1, t2, t3, t4;
	// Determine the smallest number for all dimensions.
	BIGINT smallerX = (iNewWidth < m_iWidth ? iNewWidth : m_iWidth);
	BIGINT smallerY = (iNewHeight < m_iHeight ? iNewHeight : m_iHeight);
	BIGINT smallerZ = (iNewDepth < m_iDepth ? iNewDepth : m_iDepth);

	// enter in copying process
	for(BIGINT z = 0; z < smallerZ; z++)
	{
		// pre-compute the outer term (z) of the access algo.
		t1 = z * iNewWidth * iNewHeight;
		t2 = z * m_iWidth * m_iHeight;

		for(BIGINT y = 0; y < smallerY; y++)
		{
			// pre-compute the mid term (y) of the access algo.
			t3 = y * iNewWidth;
			t4 = y * m_iWidth;

			for(BIGINT x = 0; x < smallerX; x++)
				// move data to the new array.
				newOne[t1 + t3 + x] = m_uData[t2 + t4 + x];
		}
	}

	// then, delete the old array and point to the new one.
	if(m_uData)
		DeleteArray1D(m_uData);
	m_uData   = newOne;
	m_iWidth  = iNewWidth;
	m_iHeight = iNewHeight;
	m_iDepth  = iNewDepth;
}

template<class TheType>
VVOID    Array3D<TheType>::insert(TheType uData, BIGINT iIndex)
{
	for(BIGINT index = ((m_iWidth * m_iHeight * m_iDepth) - 1); index > iIndex; index--)
		m_uData[index] = m_uData[index - 1];

	m_uData[iIndex] = uData;
}

template<class TheType>
VVOID    Array3D<TheType>::remove(BIGINT iIndex)
{
	for(BIGINT index = (iIndex + 1); index < (m_iWidth * m_iHeight * m_iDepth); index++)
		m_uData[index - 1] = m_uData[index];
}

template<class TheType>
TheType& Array3D<TheType>::value(BIGINT iX, BIGINT iY, BIGINT iZ)
{
	return m_uData[(iZ * m_iWidth * m_iHeight) + (iY * m_iWidth) + iX];
}

template<class TheType>
VVOID    Array3D<TheType>::setValue(TheType uData, BIGINT iX, BIGINT iY, BIGINT iZ)
{
	m_uData[(iZ * m_iWidth * m_iHeight) + (iY * m_iWidth) + iX] = uData;
}

#endif
