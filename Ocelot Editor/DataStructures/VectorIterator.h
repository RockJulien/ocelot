#ifndef DEF_VECTORITERATOR_H
#define DEF_VECTORITERATOR_H

// Includes
#include <vector>
#include "DataStructures\DataStructureUtil.h"

// Namespaces
namespace Ocelot
{
	// Forward declarations
	template<class TYPE>
	class VectorExt;

	// Class definition
	template<class TYPE>
	class VectorIter
	{
	public:

		// Typedefs
		typedef std::vector<TYPE> NativeList;
		typedef typename NativeList::iterator Iter;

	private:

		// Attributes
		Iter             mIter;
		VectorExt<TYPE>* mInstance;

		// Forbidden
		VectorIter();

	public:

		// Constructors & Destructor
		VectorIter(const VectorExt<TYPE>* pVector);
		~VectorIter();

		// Methods
		BBOOL NotEnd();
		BBOOL MoveNext();
		VVOID Reset();

		// Accessors
		TYPE Current() const;

	};

template<class TYPE>
VectorIter<TYPE>::VectorIter() :
mInstance(NULL)
{

}

template<class TYPE>
VectorIter<TYPE>::VectorIter(const VectorExt<TYPE>* pVector) :
mInstance(const_cast<VectorExt<TYPE>*>(pVector))
{
	this->mIter = this->mInstance->mVector.begin();
}

template<class TYPE>
VectorIter<TYPE>::~VectorIter()
{

}

template<class TYPE>
BBOOL VectorIter<TYPE>::NotEnd()
{
	return this->mIter != this->mInstance->mVector.end();
}

template<class TYPE>
BBOOL VectorIter<TYPE>::MoveNext()
{
	if 
		( this->NotEnd() )
	{
		this->mIter++;
		return true;
	}

	return false;
}

template<class TYPE>
VVOID VectorIter<TYPE>::Reset()
{
	this->mIter = this->mInstance->mVector.begin();
}

template<class TYPE>
TYPE  VectorIter<TYPE>::Current() const
{
	return (*this->mIter);
}

}

#endif