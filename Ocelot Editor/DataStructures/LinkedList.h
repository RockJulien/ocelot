#ifndef DEF_LINKEDLIST_H
#define DEF_LINKEDLIST_H

#include "ListData.h"
#include "ListIterator.h"

template<class TheType>
class LinkedList
{
protected:

	// Attributes
	BIGINT             m_iNbCells;
	ListData<TheType>* m_dFirst;
	ListData<TheType>* m_dLast;

public:

	// Constructor & Destructor
	LinkedList();
	~LinkedList();

	// Methods
	SMALLINT putBACK(TheType uData);
	SMALLINT putFRONT(TheType uData);
	VVOID    insert(ListIterator<TheType>& iter, TheType uData);
	VVOID    removeBACK();
	VVOID    removeFRONT();
	VVOID    remove(ListIterator<TheType>& iter);
	ListIterator<TheType> iterator();
	BBOOL    save(char* cFileName);
	BBOOL    load(char* cFileName);

	// Accessors
	inline BIGINT Size() const { return m_iNbCells;}
	inline ListData<TheType>* First() const { return m_dFirst;}
	inline ListData<TheType>* Last() const { return m_dLast;}
};

template<class TheType>
LinkedList<TheType>::LinkedList() :
m_iNbCells(0), m_dFirst(0), m_dLast(0)
{

}

template<class TheType>
LinkedList<TheType>::~LinkedList()
{
	ListData<TheType>* iter = m_dFirst;
	ListData<TheType>* next = 0;

	// destroy every cell.
	while(iter)
	{
		next = iter->Next();
		DeletePointer(iter);
		iter = next;
	}
}

template<class TheType>
SMALLINT LinkedList<TheType>::putBACK(TheType uData)
{
	// if nothing one the list yet.
	if(!m_dFirst)
	{
		// create a new one which will be the first.
		m_dFirst = m_dLast = new ListData<TheType>(uData);
		if(!m_dFirst)
			return BAD;
	}
	else
	{
		// insert the new one between the first and the last.
		m_dLast->insertData(uData);
		m_dLast = m_dLast->Next();
	}

	m_iNbCells++;
	return GOOD;
}

template<class TheType>
SMALLINT LinkedList<TheType>::putFRONT(TheType uData)
{
	// create the new cell.
	ListData<TheType>* newOne = new ListData<TheType>(uData);
	if(!newOne)
		return BAD;

	newOne->SetNext(m_dFirst);
	m_dFirst = newOne;
	// if it is the first one, set the last one equal to teh first.
	if(!m_dLast)
		m_dLast = m_dFirst;

	m_iNbCells++;
	return GOOD;
}

template<class TheType>
VVOID    LinkedList<TheType>::insert(ListIterator<TheType>& iter, TheType uData)
{
	// first, check if the iterator belong to this list
	if(this != iter.List())
		return;

	// check if something in the list and it is valid as well.
	if(iter.notEnd())
	{
		// then insert the new data at the iterator position
		iter.CurrCell()->insertData(uData);
		// check in the case where the currCell is the last
		// for updating pointer in that case.
		if(iter.CurrCell() == m_dLast)
			m_dLast = iter.CurrCell()->Next();
		// and add to the counter.
		m_iNbCells++;
	}
	else
		// in the case where the iterator is invalid and the list contain nothing
		// just insert the node.
		putBACK(uData);
}

template<class TheType>
VVOID    LinkedList<TheType>::remove(ListIterator<TheType>& iter)
{
	// First, check if the iterator belong to this list
	if(iter.List() != this)
		return;

	// Second, check if there is something in the list
	if(!iter.notEnd())
		return;

	// else, keep a track of the first for iteration
	ListData<TheType>* temp = m_dFirst;

	// if this is the first we try to delete.
	if(iter.CurrCell() == m_dFirst)
	{
		// iterate once and delete the first.
		iter.next();
		removeFRONT();
	}
	else
	{
		// else, iterate through the list until we reach
		// the previous to the one we want to delete.
		while(temp->Next() != iter.CurrCell())
			temp = temp->Next();

		// now the tempo one is the previous one
		// iterate the iterator once
		iter.next();

		// then, check if this is the tail and update
		// accordingly and delete the node.
		if(temp->Next() == m_dLast)
			m_dLast = temp;

		DeletePointer(temp->Next());
		// finally, restore the linked list.
		temp->SetNext(iter.CurrCell());
	}

	// subtract to the count.
	m_iNbCells--;
}

template<class TheType>
VVOID    LinkedList<TheType>::removeBACK()
{
	// As we're forced to iterate through the list
	// we start from the first one for finding the
	// previous one of the last for deleting it.
	ListData<TheType>* temp = m_dFirst;
	// if something in the list.
	if(m_dFirst)
	{
		// if only one cell, remove it and reset the list.
		if(m_dFirst == m_dLast)
		{
			DeletePointer(m_dFirst);
			m_dLast = 0;
		}
		else
		{
			// iterate through the list until the previous to the last.
			while(temp->Next() != m_dLast)
				temp = temp->Next();

			// now we have the right one, set the previous one
			// as the last one and delete the old last one.
			m_dLast = temp;
			DeletePointer(temp->Next());
		}

		// finally, subtract a cell.
		m_iNbCells--;
	}
}

template<class TheType>
VVOID    LinkedList<TheType>::removeFRONT()
{
	ListData<TheType>* temp = 0;

	if(m_dFirst)
	{
		// temporary keep pointer to the next
		temp = m_dFirst->Next();
		// delete the first.
		DeletePointer(m_dFirst);
		// then, restore the next one as the first
		m_dFirst = temp;
		// if only one cell was present, set the last to NULL as well.
		if(!m_dFirst)
			m_dLast = 0;
		// and subtract the number of cell.
		m_iNbCells--;
	}
}

template<class TheType>
ListIterator<TheType> LinkedList<TheType>::iterator()
{
	return ListIterator<TheType>(this, m_dFirst);
}

template<class TheType>
BBOOL    LinkedList<TheType>::save(char* cFileName)
{
	FILE* output = 0;
	ListData<TheType>* iterator = m_dFirst;

	// open the file for writing as binary stream.
	output = fopen(cFileName, "wb");

	if(!output)
		return false;

	// write the number of cells of the list.
	fwrite(&m_iNbCells, sizeof(BIGINT), 1, output);

	// then, iterate through the list for writing 
	// data in.
	while(iterator)
	{
		fwrite(&(iterator->Data()), sizeof(TheType), 1, output);
		iterator = iterator->Next();
	}

	// okay, then close the file.
	fclose(output);
	return true;
}

template<class TheType>
BBOOL    LinkedList<TheType>::load(char* cFileName)
{
	FILE* input = 0;
	TheType buffer;
	BIGINT nbCells = 0;

	// we open the file prepared to be read as a binary stream.
	input = fopen(cFileName, "rb");

	if(!input)
		return false;

	// read first the number of cell for the list.
	fread(&nbCells, sizeof(BIGINT), 1, input);

	// then, start the loop creation process for every cells.
	while(nbCells != 0)
	{
		fread(&buffer, sizeof(TheType), 1, input);
		putBACK(buffer);
		nbCells--;
	}

	// then, close the file, over.
	fclose(input);
	return true;
}

#endif