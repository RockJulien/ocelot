#ifndef DEF_DATASTRUCTUREUTIL_H
#define DEF_DATASTRUCTUREUTIL_H

#pragma warning (disable: 4005)

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#if defined(DEBUG) || (_DEBUG)
// Will provide the line and the file when memory leaks occur
// because of diverse mem alloc.
#define OcelotMalloc(s)     _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#define OcelotCalloc(c, s)  _calloc_dbg(c, s, _NORMAL_BLOCK, __FILE__, __LINE__)
#define OcelotRealloc(p, s) _realloc_dbg(p, s, _NORMAL_BLOCK, __FILE__, __LINE__)
#define OcelotExpand(p, s)  _expand_dbg(p, s, _NORMAL_BLOCK, __FILE__, __LINE__)
#define OcelotFree(p)       _free_dbg(p, _NORMAL_BLOCK)
#define OcelotMsize(p)      _msize_dbg(p, _NORMAL_BLOCK)
#else
#define OcelotMalloc(s)     malloc
#define OcelotCalloc(c, s)  calloc
#define OcelotRealloc(p, s) realloc
#define OcelotExpand(p, s)  _expand
#define OcelotFree(p)       free
#define OcelotMsize(p)      _msize
#endif

#define DeletePointer(X) {if(X){delete X; X = 0;}}
#define DeleteArray1D(X) {if(X){delete[] X; X = 0;}}
#define DeleteArray2D(X, rows) { for(UBIGINT currRow = 0; currRow < rows; currRow++){ if(X[currRow]){ delete[] X[currRow];}} delete[] X;}
#define DeleteStatic(X, rows) { for(UBIGINT currRow = 0; currRow < rows; currRow++){ if(X[currRow]){ delete[] X[currRow];}}}
#define DeletePointersNPointed(X, rows, cols) { for(UBIGINT currRow = 0; currRow < rows; currRow++){ for(UBIGINT currCol = 0; currCol < cols; currCol++){ DeletePointer(X[rows][cols]);}} DeleteArray1D(X);}
typedef long long DLINT;
typedef long      LINT;
typedef int       BIGINT;
typedef short     SMALLINT;
typedef float     FFLOAT;
typedef bool      BBOOL;
typedef double    DFLOAT;
typedef char      CCHAR;
typedef wchar_t   WCHAR;
typedef unsigned long int ULINT;
typedef unsigned int UBIGINT;
typedef unsigned short USMALLINT;
typedef unsigned char UCCHAR;
typedef void   VVOID;
typedef void*  UNKNOWN;

// Debug utilities
#define GOOD 1;
#define BAD -1;
//BBOOL CHECK(X) {if(X < 0){ return BFALSE;} else return BTRUE;}

template<class T>
inline BBOOL Equality(T a, T b)
{
	if(a == b) 
		return true;
	return false;
}

template<class F>
inline BBOOL NotEqual(F a, F b)
{
	if(a != b) 
		return true;
	return false;
}

// Simple type quick sort.
template<class type>
VVOID SQuickSort(type* toSort, BIGINT lowIndex, BIGINT stopIndex)
{
	// the sort algorithm will sort from lowIndex to stopIndex
	// avoiding to sort the rest if any.
	BIGINT currLow  = lowIndex;
	BIGINT currStop = stopIndex;
	type tempVal; // only for swapping values.
	type toComp     = toSort[(lowIndex + stopIndex) / 2];

	// Partition.
	do
	{
		// search for a smallest value
		while(toSort[currLow] < toComp)
			currLow++;
		while(toSort[currStop] > toComp)
			currStop--;

		// if the smallest value is after the bigger one, swap.
		if(currLow <= currStop)
		{
			tempVal          = toSort[currLow];
			toSort[currLow]  = toSort[currStop];
			toSort[currStop] = tempVal;
			currLow++;
			currStop--;
		}
	}
	while(currLow <= currStop);

	// Recursion.
	if(lowIndex < currStop)
		SQuickSort(toSort, lowIndex, currStop);
	if(currLow < stopIndex)
		SQuickSort(toSort, currLow, stopIndex);
}

// Double type quick sort regarding to the second type.
template<class type1, class type2>
VVOID DQuickSort(type1* toSort1, type2* toSort2, BIGINT lowIndex, BIGINT stopIndex)
{
	// the sort algorithm will sort from lowIndex to stopIndex
	// avoiding to sort the rest if any.
	BIGINT currLow  = lowIndex;
	BIGINT currStop = stopIndex;
	type1 tempValType1; // only for swapping values.
	type2 tempValType2; // only for swapping values.
	type2 toComp    = toSort2[(lowIndex + stopIndex) / 2];

	// Partition.
	do
	{
		// search for a smallest value
		while(toSort2[currLow] < toComp)
			currLow++;
		while(toSort2[currStop] > toComp)
			currStop--;

		// if the smallest value is after the bigger one, swap.
		if(currLow <= currStop)
		{
			// Swap first (the important one)
			tempValType2      = toSort2[currLow];
			toSort2[currLow]  = toSort2[currStop];
			toSort2[currStop] = tempValType2;
			// Swap second
			tempValType1      = toSort1[currLow];
			toSort1[currLow]  = toSort1[currStop];
			toSort1[currStop] = tempValType1;
			currLow++;
			currStop--;
		}
	}
	while(currLow <= currStop);

	// Recursion.
	if(lowIndex < currStop)
		DQuickSort(toSort1, toSort2, lowIndex, currStop);
	if(currLow < stopIndex)
		DQuickSort(toSort1, toSort2, currLow, stopIndex);
}

#endif
