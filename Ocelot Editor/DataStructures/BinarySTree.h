#ifndef DEF_BINARYSTREE_H
#define DEF_BINARYSTREE_H

#include "BinaryTree.h"

template<class TheType>
class BinarySTree
{
public:

	// Re-Definition
	typedef BinaryTree<TheType> Node;

private:

	// Attributes
	Node*  m_nRoot;
	BIGINT (*m_functionToApply)(TheType, TheType);

public:

	// Constructor & Destructor
	BinarySTree(BIGINT (*functionToApply)(TheType, TheType));
	~BinarySTree();

	// Methods
	VVOID insert(TheType uData);
	Node* search(TheType uData);
	// need a remove method.

	// Accessors
	inline Node* Root() const { return m_nRoot;}
};

template<class TheType>
BinarySTree<TheType>::BinarySTree(BIGINT (*functionToApply)(TheType, TheType)) :
m_nRoot(0), m_functionToApply(functionToApply)
{

}

template<class TheType>
BinarySTree<TheType>::~BinarySTree()
{
	DeletePointer(m_nRoot);
}

template<class TheType>
VVOID BinarySTree<TheType>::insert(TheType uData)
{
	// create a tempo iterator.
	Node* currIter = m_nRoot;
	// if nothing yet, create the root.
	if(!m_nRoot)
		m_nRoot = new Node(uData);
	else
		while(currIter)
		{
			// if result of the function is negative.
			if(m_functionToApply(uData, currIter->Data()) < 0)
			{
				// check if there is already a left node
				// regarding to the currNode.
				if(!currIter->Left())
				{
					// add to the left.
					currIter->SetLeft(new Node(uData));
					currIter->Left()->SetParent(currIter);
					// stop the loop.
					currIter = NULL;
				}
				else // else, iterate for reaching the next left
					currIter = currIter->Left();
			}
			else // else, the result is positive
			{
				// check if there is already a right node
				// regarding to the currNode.
				if(!currIter->Right())
				{
					currIter->SetRight(new Node(uData));
					currIter->Right()->SetParent(currIter);
					// stop the loop.
					currIter = NULL;
				}
				else // else, iterate for reaching the next right
					currIter = currIter->Right();
			}
		}
}

template<class TheType>
BinaryTree<TheType>* BinarySTree<TheType>::search(TheType uData)
{
	Node* currIter = m_nRoot;

	BIGINT tempRes = 0;

	while(currIter)
	{
		tempRes = m_functionToApply(uData, currIter->Data());
		if(tempRes == 0)
			return currIter;
		if(tempRes < 0)
			currIter = currIter->Left();
		else
			currIter = currIter->Right();
	}

	// if nothing found, return NULL.
	return 0;
}

#endif