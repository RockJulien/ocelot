#ifndef DEF_SCALINGNODE_H
#define DEF_SCALINGNODE_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   ScalingNode.h/.cpp
/
/ Description: This file provide a way to create a scaling node 
/              which will change the scale of an entity regarding to its 
/              parent scale for allowing complex transformations 
/              and association of different entities in the scene.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "SceneNode.h"

class ScalingNode : public SceneNode
{
private:

	// Attributes of the scaling node
	float      m_fScaleX;
	float      m_fScaleY;
	float      m_fScaleZ;
	Matrix4x4  m_matScaling;

public:

	// Constructor & Destructor
	ScalingNode();
	ScalingNode(float scaleXYZ);
	ScalingNode(float scaleX, float scaleY, float scaleZ);
	ScalingNode(const Vector3& scale);
	~ScalingNode();

	// Methods
	void      update(float deltaTime);
	void	  applyScale(float sX, float sY, float sZ);
	void	  applyTrans(float tX, float tY, float tZ);
	void	  applyRotat(float rX, float rY, float rZ);

	// Accessors
	inline float     ScaleX() const { return m_fScaleX;}
	inline float     ScaleY() const { return m_fScaleY;}
	inline float     ScaleZ() const { return m_fScaleZ;}
	inline Vector3   ScaleXYZ() const { return Vector3(m_fScaleX, m_fScaleY, m_fScaleZ);}
	inline void      setScale(float scaleXYZ) { m_fScaleX = scaleXYZ; m_fScaleY = scaleXYZ; m_fScaleZ = scaleXYZ;}
	inline void      setScale(float scaleX, float scaleY, float scaleZ) { m_fScaleX = scaleX; m_fScaleY = scaleY; m_fScaleZ = scaleZ;}
	inline void      setScale(Vector3& scale) { m_fScaleX = scale.getX(); m_fScaleY = scale.getY(); m_fScaleZ = scale.getZ();}
};

#endif