#ifndef DEF_ROTATIONNODE_H
#define DEF_ROTATIONNODE_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   RotationNode.h/.cpp
/
/ Description: This file provide a way to create a rotation node 
/              which will rotate an entity regarding to its 
/              parent direction for allowing complex transformations 
/              and association of different entities in the scene.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "SceneNode.h"

class RotationNode : public SceneNode
{
private:

	// Attributes
	float				 m_fRotationX;
	float                m_fRotationY;
	float                m_fRotationZ;
	Matrix4x4            m_matRotation;

public:

	// Constructor & Destructor
	RotationNode();
	RotationNode(float rotationXYZ);
	RotationNode(float rotationX, float rotationY, float rotationZ);
	RotationNode(const Vector3& rotation);
	~RotationNode();

	// Methods
	void	 update(float deltaTime);
	void	 applyScale(float sX, float sY, float sZ);
	void	 applyTrans(float tX, float tY, float tZ);
	void	 applyRotat(float rX, float rY, float rZ);

	// Accessors
	inline float		RotationX() const { return m_fRotationX;}
	inline float		RotationY() const { return m_fRotationY;}
	inline float		RotationZ() const { return m_fRotationZ;}
	inline Vector3      RotationXYZ() const { return Vector3(m_fRotationX, m_fRotationY, m_fRotationZ);}
	inline void         setRotation(float rotationXYZ) { m_fRotationX = rotationXYZ; m_fRotationY = rotationXYZ; m_fRotationZ = rotationXYZ;}
	inline void         setRotation(float rotationX, float rotationY, float rotationZ) { m_fRotationX = rotationX; m_fRotationY = rotationY; m_fRotationZ = rotationZ;}
	inline void         setRotation(const Vector3& rotation) { m_fRotationX = rotation.getX(); m_fRotationY = rotation.getY(); m_fRotationZ = rotation.getZ();}
};

#endif