#ifndef DEF_MEMORYUTIL_H
#define DEF_MEMORYUTIL_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   MemoryUtil.h
/
/ Description: This header provide different way to free memory
/              for different kind of array if we do not want to use
/              STL contents.
/              Do your own data structures and become better!!!
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        19/02/2012
/****************************************************************************/

inline BIGINT initMemory2D(UINT nbRows, UINT nbCols, FFLOAT** &m)
{
	// allocate column
	m = (FFLOAT**)malloc(sizeof(FFLOAT*) * nbRows);

	if(m == NULL)
		return -1; // problem when allocating memory

	// allocate each row
	for(UINT i = 0; i < nbRows; ++i)
	{
		m[i] = (FFLOAT*)malloc(sizeof(FFLOAT) * nbCols);
	}

	if(m[nbRows - 1] == NULL)
		return -1;  // problem when allocating memory

	// if everything is OKAY
	return 1;
}

inline BIGINT exitMemory2D(UINT nbRows, FFLOAT** &m)
{
	if(m)
	{
		for(UINT i = 0; i < nbRows; ++i)
		{
			free(m[i]);
		}

		// then deallocate last layer of pointer
		free(m);

		return 1;
	}

	// if the array was already empty
	// send error message for either
	// tell to the user to stop trying
	// destroy a memory already destroy
	// or that there is a problem with
	// its array because nothing inside :-).
	return -1;
}

#endif