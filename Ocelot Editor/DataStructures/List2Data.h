#ifndef DEF_LIST2DATA_H
#define DEF_LIST2DATA_H

#include "DataStructureUtil.h"

template<class TheType>
class List2Data
{
private:

	// Attributes
	TheType    m_uData;
	List2Data* m_dNext;
	List2Data* m_dPrevious;

public:

	// Constructor & Destructor
	List2Data();
	List2Data(TheType uData);
	~List2Data();

	// Methods


	// Accessors
	inline TheType    Data() { return m_uData;}
	inline List2Data* Next() { return m_dNext;}
	inline List2Data* Previous() { return m_dPrevious;}
	inline VVOID      SetData(TheType uData) { m_uData = uData;}
	inline VVOID      SetNext(List2Data* dNext)  { m_dNext = dNext;}
	inline VVOID      SetPrevious(List2Data* dPrevious) { m_dPrevious = dPrevious;}
};

template<class TheType>
List2Data<TheType>::List2Data() : 
m_dNext(0), m_dPrevious(0)
{

}

template<class TheType>
List2Data<TheType>::List2Data(TheType uData) : 
m_dNext(0), m_dPrevious(0), m_uData(uData)
{

}

template<class TheType>
List2Data<TheType>::~List2Data()
{

}

#endif