#ifndef DEF_STOKEN_H
#define DEF_STOKEN_H

#include "DataStructureUtil.h"

enum TOKENTYPE
{
	NUMBER,
	OPERATOR,
	VARIABLE,
	LEFTPAR,
	RIGHTPAR
};

class SToken
{
private:

	// Attributes
	TOKENTYPE m_type;
	FFLOAT    m_fNumber;
	BIGINT    m_iVariable;
	BIGINT    m_iOperator;

public:

	// Constructor & Destructor
	SToken();
	~SToken();

	// Methods


	// Accessors

};

#endif