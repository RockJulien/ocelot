#ifndef DEF_ARRAY2D_H
#define DEF_ARRAY2D_H

#include "DataStructureUtil.h"

template<class TheType>
class Array2D
{
private:

	// Attributes
	TheType* m_uData;
	BIGINT   m_iWidth;
	BIGINT   m_iHeight;

public:

	// Constructor & Destructor
	Array2D(BIGINT iWidth, BIGINT iHeight);
	~Array2D();

	// Methods
	VVOID    resize(BIGINT iNewWidth, BIGINT iNewHeight);
	VVOID    insert(TheType uData, BIGINT iIndex);
	VVOID    remove(BIGINT iIndex);
	TheType& value(BIGINT iX, BIGINT iY);
	VVOID    setValue(TheType uData, BIGINT iX, BIGINT iY);

	// Accessors
	inline BIGINT Size() const { return (m_iWidth * m_iHeight);}
};

template<class TheType>
Array2D<TheType>::Array2D(BIGINT iWidth, BIGINT iHeight) :
m_iWidth(iWidth), m_iHeight(iHeight)
{
	m_uData = new TheType[iWidth * iHeight];
}

template<class TheType>
Array2D<TheType>::~Array2D()
{
	if(m_uData)
		DeleteArray1D(m_uData);
}

template<class TheType>
VVOID    Array2D<TheType>::resize(BIGINT iNewWidth, BIGINT iNewHeight)
{
	TheType* newOne = new TheType[iNewWidth * iNewHeight];
	if(!newOne)
		return;

	BIGINT t1, t2;
	BIGINT smallerX = (iNewWidth < m_iWidth ? iNewWidth : m_iWidth);
	BIGINT smallerY = (iNewHeight < m_iHeight ? iNewHeight : m_iHeight);

	for(BIGINT y = 0; y < smallerY; y++)
	{
		t1 = y * iNewWidth;
		t2 = y * m_iWidth;

		for(BIGINT x = 0; x < smallerX; x++)
			newOne[t1 + x] = m_uData[t2 + x];
	}

	// then, free the old one and point to the new one.
	if(m_uData)
		DeleteArray1D(m_uData);
	m_uData   = newOne;
	m_iWidth  = iNewWidth;
	m_iHeight = iNewHeight;
}

template<class TheType>
VVOID    Array2D<TheType>::insert(TheType uData, BIGINT iIndex)
{
	for(BIGINT index = ((m_iWidth * m_iHeight) - 1); index > iIndex; index--)
		m_uData[index] = m_uData[index - 1];

	m_uData[iIndex] = uData;
}

template<class TheType>
VVOID    Array2D<TheType>::remove(BIGINT iIndex)
{
	for(BIGINT index = (iIndex + 1); index < (m_iWidth * m_iHeight); index++)
		m_uData[index - 1] = m_uData[index];
}

template<class TheType>
TheType& Array2D<TheType>::value(BIGINT iX, BIGINT iY)
{
	return m_uData[iY * m_iWidth + iX];
}

template<class TheType>
VVOID    Array2D<TheType>::setValue(TheType uData, BIGINT iX, BIGINT iY)
{
	m_uData[iY * m_iWidth + iX] = uData;
}

#endif