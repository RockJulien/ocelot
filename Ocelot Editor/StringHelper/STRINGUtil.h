#ifndef DEF_STRINGUTIL_H
#define DEF_STRINGUTIL_H

#include <string>
#include <stdlib.h>
#include <cstdio>
#include <string>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <fstream>
#include "..\DataStructures\Array1D.h"
#include "..\DataStructures\Linked2List.h"

typedef std::string          STRING;
typedef std::wstring         WSTRING;
typedef Array1D<char>        CSTRING;
typedef Linked2List<STRING>  LSSTRING;
typedef Linked2List<WSTRING> LWSTRING;

enum strType
{
	WC,
	CC
};

#endif