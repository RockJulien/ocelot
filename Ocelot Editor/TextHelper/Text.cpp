#include "Text.h"

using namespace Ocelot;

Text::Text() :
m_iIndex(0)
{

}

Text::~Text()
{
	
}

VVOID Text::addLine(WSTRING newStringOnLine)
{
	// Add the new string as a new line.
	m_sLines.putBACK(newStringOnLine);
}

VVOID Text::appendToLine(WSTRING stringToAppend, UBIGINT line)
{
	// Add the new string to an already
	// in line.
	if(line >= (UBIGINT)m_sLines.Size())
	{
		// If too high index, put as new line
		// Add the new string as a new line.
		m_sLines.putBACK(stringToAppend);
	}
	else
	{
		// search and append
		UBIGINT counter = 0;
		List2Iterator<WSTRING> iter = m_sLines.iterator();
		for(iter.begin(); iter.notEnd(); iter.next(), counter++)
		{
			if(counter == line)
			{
				WSTRING lineString = iter.data();
				lineString.append(stringToAppend);
				iter.setData(lineString);
			}
		}
	}
}

VVOID Text::refresh(WSTRING toChangeWith, UBIGINT line)
{
	if(line >= (UBIGINT)m_sLines.Size())
		return;

	// search and refresh
	UBIGINT counter = 0;
	List2Iterator<WSTRING> iter = m_sLines.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
		if(counter == line)
			iter.setData(toChangeWith);
}

VVOID Text::eraseLine(UBIGINT line)
{
	if(line >= (UBIGINT)m_sLines.Size())
		return;

	// Erase the linked node containing the line
	// and its string(s).
	UBIGINT counter = 0;
	List2Iterator<WSTRING> iter = m_sLines.iterator();
	for(iter.begin(); iter.notEnd(); iter.next(), counter++)
	{
		if(counter == line)
			m_sLines.remove(iter);
	}
}
