#include "OcelotDXText.h"

using namespace Ocelot;

OcelotDXText::OcelotDXText() :
m_info(), m_font(NULL), m_textSprite(NULL), 
m_3DTextMesh(NULL), m_type(TEXT_TYPE_DRAW_SINGLE)
{
	// Default: Single simple text.
	CreateDXFont();
}

OcelotDXText::OcelotDXText(OcelotDXTextInfo info, TextDrawType type) :
m_info(info), m_font(NULL), m_textSprite(NULL), 
m_3DTextMesh(NULL), m_type(type)
{
	switch(m_type)
	{
	case TEXT_TYPE_DRAW_SINGLE:
		{
			CreateDXFont();
		}
		break;
	case TEXT_TYPE_DRAW_MULTI:
		{
			CreateDXFont();
			CreateSprite();
		}
		break;
	case TEXT_TYPE_DRAW_3D:
		{
			CreateD3DXTextMesh();
		}
		break;
	}
}

OcelotDXText::~OcelotDXText()
{
	ReleaseCOM(m_font);
	ReleaseCOM(m_textSprite);
	ReleaseCOM(m_3DTextMesh);
}

VVOID OcelotDXText::CreateD3DXTextMesh()
{
	/*if(m_3DTextMesh)
		ReleaseCOM(m_3DTextMesh);

	HDC hdc = CreateCompatibleDC(NULL);
	if(!hdc)
		HR(E_OUTOFMEMORY);

	BIGINT height = -MulDiv(m_info.Height(), GetDeviceCaps(hdc, LOGPIXELSY), 72);
	HFONT hFont;
	HFONT hFontOld;

	hFont = CreateFont(height, 
					   0, 
					   0, 
					   0, 
					   m_info.Weight(), 
					   m_info.IsItalic(), 
					   false, 
					   false, 
					   m_info.CharSet(), 
					   OUT_DEFAULT_PRECIS, 
					   CLIP_DEFAULT_PRECIS, 
					   m_info.Quality(), 
					   m_info.PitchNName(), 
					   m_info.TextStyle().c_str());

	hFontOld = (HFONT)SelectObject(hdc, hFont);
	
	// Build the entire text for the 3D mesh.
	WSTRING toDraw;
	List2Iterator<WSTRING> iter = m_sLines.iterator();
	for(iter.begin(); iter.notEnd(); iter.next())
	{
		toDraw += iter.data() + L"\n";
	}

	HR(D3DXCreateText((LPDIRECT3DDEVICE9)m_info.Device(), hdc, toDraw.c_str(), 0.001f, 0.1f, &m_3DTextMesh, NULL, NULL));

	SelectObject(hdc, hFontOld);
	DeleteObject(hFont);
	DeleteDC(hdc);*/
}

VVOID OcelotDXText::Update(FFLOAT deltaTime)
{

}

VVOID OcelotDXText::Render(BIGINT leftPos, BIGINT topPos, BIGINT windowWidth, BIGINT windowHeight)
{
	// NOTE: windowWidth and Height will serve to wrap text
	// when resizing the window
	RECT rc;

	switch(m_type)
	{
	case TEXT_TYPE_DRAW_SINGLE:
		{
			WSTRING toDraw;
			WSTRING toLine = L"\n";
			List2Iterator<WSTRING> iter = m_sLines.iterator();
			for(iter.begin(); iter.notEnd(); iter.next())
			{
				toDraw.append(iter.data());
				toDraw.append(toLine);
			}
			SetRect( &rc, leftPos, topPos, windowWidth, windowHeight );
			m_font->DrawTextW(NULL, 
							  toDraw.c_str(), 
							  -1, 
							  &rc, 
							  /*DT_NOCLIP*/DT_LEFT | DT_WORDBREAK | DT_EXPANDTABS, 
							  DXColor::OcelotColToDXCol(m_info.TextColor()));
		}
		break;
	case TEXT_TYPE_DRAW_MULTI:
		{
			m_textSprite->Begin( D3DX10_SPRITE_SORT_DEPTH_FRONT_TO_BACK );
			UBIGINT lineCount = m_sLines.Size();
			List2Iterator<WSTRING> iter = m_sLines.iterator();
			for(iter.begin(); iter.notEnd(); iter.next(), lineCount--)
			{
				SetRect( &rc, leftPos, topPos - m_info.Height() * lineCount, windowWidth, windowHeight );
				m_font->DrawTextW(m_textSprite, 
								  iter.data().c_str(), 
								  -1, 
								  &rc, /*DT_NOCLIP*/DT_LEFT | DT_WORDBREAK | DT_EXPANDTABS, 
								  DXColor::OcelotColToDXCol(m_info.TextColor()));
			}
			m_textSprite->End();
		}
		break;
	case TEXT_TYPE_DRAW_3D:
		{
			/*D3DMATERIAL9 mtrl; // For 3D text Color.
			ZeroMemory( &mtrl, sizeof( D3DMATERIAL9 ) );
			Color col = m_info.TextColor();
			mtrl.Diffuse.r = col.getR();
			mtrl.Diffuse.g = col.getG();
			mtrl.Diffuse.b = col.getB();
			mtrl.Diffuse.a = col.getA();
			mtrl.Ambient.r = col.getR() * 0.5f;
            mtrl.Ambient.g = col.getG() * 0.5f;
            mtrl.Ambient.b = col.getB() * 0.5f;
            mtrl.Ambient.a = col.getA();
			((LPDIRECT3DDEVICE9)m_info.Device())->SetMaterial(&mtrl);
			m_3DTextMesh->DrawSubset( 0 );*/
		}
		break;
	}
}

VVOID OcelotDXText::OnResize()
{
	//if(m_font)
		//HR(m_font->OnResetDevice());
	//if(m_textSprite)
		//HR(m_textSprite->OnResetDevice());

	if(m_type == TEXT_TYPE_DRAW_3D)
	{
		// First destroy, then re-create.
		ReleaseCOM(m_3DTextMesh);
		CreateD3DXTextMesh();
	}
}

/*VVOID OcelotDXText::OnLostDevice()
{
	if(m_font)
		HR(m_font->OnLostDevice());
	if(m_textSprite)
		HR(m_textSprite->OnLostDevice());
	ReleaseCOM(m_3DTextMesh);
}*/

VVOID OcelotDXText::CreateDXFont()
{
	HR(D3DX10CreateFont(m_info.Device(), 
					    m_info.Height(), 
					    m_info.Width(), 
					    m_info.Weight(), 
					    m_info.MipLevel(), 
					    m_info.IsItalic(), 
					    m_info.CharSet(), 
					    m_info.Precision(), 
					    m_info.Quality(), 
					    m_info.PitchNName(), 
					    m_info.TextStyle().c_str(), 
					    &m_font));
}

VVOID OcelotDXText::CreateSprite()
{
	HR(D3DX10CreateSprite(m_info.Device(), 
						  1,
						  &m_textSprite));
}

VVOID OcelotDXText::CreateText3D()
{

}

OcelotDXTextInfo::OcelotDXTextInfo() :
BaseDXData(), m_iHeight(12), m_iWidth(0), m_iWeight(FW_BOLD), m_iMipLevel(1), m_bItalic(false), m_iCharSet(DEFAULT_CHARSET), m_iPrecision(OUT_DEFAULT_PRECIS), 
m_iQuality(DEFAULT_QUALITY), m_iPitchNFam(DEFAULT_PITCH | FF_DONTCARE), m_sTextStyle(L"Arial"), m_textColor(OcelotColor::NAVYBLUE)
{

}

OcelotDXTextInfo::OcelotDXTextInfo(DevPtr device, CamPtr camera, Color color, BIGINT height, UBIGINT width, UBIGINT weight, UBIGINT mipLevel, BBOOL italic, UBIGINT charSet, UBIGINT precision, UBIGINT quality, UBIGINT pitchNFam, WSTRING textStyle) :
BaseDXData(device, camera), m_iHeight(height), m_iWidth(width), m_iWeight(weight), m_iMipLevel(mipLevel), m_bItalic(italic), m_iCharSet(charSet), m_iPrecision(precision), 
m_iQuality(quality), m_iPitchNFam(pitchNFam), m_sTextStyle(textStyle), m_textColor(color)
{

}

OcelotDXTextInfo::~OcelotDXTextInfo()
{

}
