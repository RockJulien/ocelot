#ifndef DEF_INPUTMANAGER_H
#define DEF_INPUTMANAGER_H

#include "..\DirectXAPI\DXUtility.h"
#include "OcelotInput.h"

#if defined(DEBUG) || defined(_DEBUG)
#pragma comment(lib, "..\\Debug\\OcelotInput.lib")
#else
#pragma comment(lib, "..\\Release\\OcelotInput.lib")
#endif

#define InMng Ocelot::InputManager::instance()

namespace Ocelot
{
						struct InfoInput
						{
							HWND      hWnd;
							HINSTANCE hInst;
							UINT      widthWindow;
							UINT      heightWindow;
						};

	class InputManager
	{
	private:

			// Attributes
			// input device
			OcelotInput*        m_pInput;
			IOcelotInputDevice* m_pDevice;

			// Constructor
			InputManager(){}

			// cpy cst & assgmt
			InputManager(const InputManager&);
			InputManager& operator = (const InputManager&);

	public:

			// Destructor
			~InputManager();

			// Singleton
	static  InputManager* instance();

			// Methods
			VVOID initialize(InfoInput inf);
			VVOID update();
			BBOOL isPressed(InputType type, UINT keyID);
			BBOOL isReleased(InputType type, UINT keyID);
			BBOOL isPressedAndLocked(UINT keyID);
			VVOID release();

			// Accessors
	inline  IOcelotInputDevice* Device() const { return m_pDevice; }
	};
}

#endif