#include "InputManager.h"

using namespace Ocelot;

InputManager::~InputManager()
{

}

InputManager* InputManager::instance()
{
	static InputManager instance;

	return &instance;
}

VVOID InputManager::initialize(InfoInput inf)
{
	// determine the screen cage for the mouse
	RECT cage;
	cage.left   = 0;
	cage.top    = 0;
	cage.right  = inf.widthWindow - 1;
	cage.bottom = inf.heightWindow - 1;

	// create the input object
	m_pInput = new OcelotInput(inf.hInst);

	HR(m_pInput->createDevice());

	// get the device
	m_pDevice = m_pInput->GetDevice();

	// initialize it.
	HR(m_pDevice->Initialize(inf.hWnd, &cage, true));
}

VVOID InputManager::update()
{
	HR(m_pDevice->Update());
}

BBOOL InputManager::isPressed(InputType type, UINT keyID)
{
	return m_pDevice->IsPressed(type, keyID);
}

BBOOL InputManager::isReleased(InputType type, UINT keyID)
{
	return m_pDevice->IsReleased(type, keyID);
}

BBOOL InputManager::isPressedAndLocked(UINT keyID)
{
	return m_pDevice->IsPressedAndLocked(keyID);
}

VVOID InputManager::release()
{
	DeletePointer(m_pInput);
}
