#ifndef DEF_IMESHLOADER_H
#define DEF_IMESHLOADER_H

// Includes
#include "DirectXAPI\DXUtility.h"
#include "Geometry\IOcelotMesh.h"
#include "StringHelper\STRINGHelpers.h"

// Namespaces
namespace Ocelot
{
	// Interface defintion
	class IMeshLoader
	{
	public:

				// Constructors & Destructor
				IMeshLoader() {}
		virtual ~IMeshLoader() {}

				// Methods
		virtual BBOOL		 CanLoad(const STRING& pFileName) = 0;
		virtual IOcelotMesh* LoadFromFile(const DevPtr pDevice, 
										  const CamPtr pCamera, 
										  const STRING& pFileName) = 0;

	};
}

#endif