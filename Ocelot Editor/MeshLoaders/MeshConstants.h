
#pragma once

/** @def MAX_VERTICES
*  Maximum number of vertices per mesh.  */

#ifndef MAX_VERTICES
#	define MAX_VERTICES 0x7fffffff
#endif

/** @def MAX_FACES
*  Maximum number of faces per mesh. */

#ifndef MAX_FACES
#	define MAX_FACES 0x7fffffff
#endif

/** @def MAX_FACE_INDICES
*  Maximum number of indices per face (polygon). */

#ifndef MAX_FACE_INDICES 
#	define MAX_FACE_INDICES 0x7fff
#endif

/** @def MAX_BONE_WEIGHTS
*  Maximum number of indices per face (polygon). */

#ifndef MAX_BONE_WEIGHTS
#	define MAX_BONE_WEIGHTS 0x7fffffff
#endif

/** @def MAX_COLOR_SETS
*  Supported number of vertex color sets per mesh. */

#ifndef MAX_COLOR_SETS
#	define MAX_COLOR_SETS 0x8
#endif

/** @def MAX_TEXTURECOORDS
*  Supported number of texture coord sets (UV(W) channels) per mesh */

#ifndef MAX_TEXTURECOORDS
#	define MAX_TEXTURECOORDS 0x8
#endif