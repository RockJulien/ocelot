
#pragma once

/**
 * Includes
 */
#include "IMeshLoader.h"
#include "Maths/Vector2.h"
#include "Maths/Vector3.h"
#include "Geometry/PrimitiveTypes.h"
#include "DataStructures\VectorExt.h"

/**
 * Namespace
 */
namespace Loader
{
	/**
	 * Base abstract mesh loader
	 */
	class AMeshLoader : public Ocelot::IMeshLoader
	{
	private:

		/**
		 * Attribute(s)
		 */

	protected:

		/**
		 * Attribute(s)
		 */

		/**
		 * Constructor
		 */
		AMeshLoader();

		/**
		 * Method(s)
		 */
		STRING GetExtension(const STRING& pFileName);

		bool CheckExtension(const STRING& pFileName, const Ocelot::VectorExt<STRING>& pExtensionMatches);

	public:

		/**
		 * Destructor
		 */
		virtual ~AMeshLoader();

		/**
		 * Method(s)
		 */
		virtual BBOOL				 CanLoad(const STRING& pFileName) = 0;

		virtual Ocelot::IOcelotMesh* LoadFromFile(const Ocelot::DevPtr pDevice, 
												  const Ocelot::CamPtr pCamera, 
												  const STRING& pFileName) = 0;
	};
}