
#include "AMeshLoader.h"

using namespace std;
using namespace Ocelot;
using namespace Loader;

AMeshLoader::AMeshLoader()
{

}

AMeshLoader::~AMeshLoader()
{

}

STRING AMeshLoader::GetExtension(const STRING& pFileName)
{
	string::size_type pos = pFileName.find_last_of('.');

	// no file extension at all
	if (pos == STRING::npos)
	{
		return "";
	}

	STRING ret = pFileName.substr(pos + 1);
	transform(ret.begin(), ret.end(), ret.begin(), ::tolower); // thanks to Andy Maloney for the hint
	return ret;
}

bool AMeshLoader::CheckExtension(const STRING& pFileName, const VectorExt<STRING>& pExtensionMatches)
{
	WSTRING::size_type pos = pFileName.find_last_of('.');

	// no file extension - can't read
	if (pos == WSTRING::npos)
	{
		return false;
	}

	const CHAR* ext_real = &pFileName[pos + 1];
	for 
		( VectorIter<STRING> lIter = pExtensionMatches.Iterator(); 
							 lIter.NotEnd(); 
							 lIter.MoveNext() )
	{
		if 
			( !_stricmp(ext_real, lIter.Current().c_str()) )
		{
			return true;
		}
	}

	return false;
}
