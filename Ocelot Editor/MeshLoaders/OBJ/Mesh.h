
#pragma once

/**
 * Includes
 */
#include "DataStructures/VectorExt.h"
#include "../MeshConstants.h"

/**
 * Namespace
 */
namespace OBJLoader
{
	/**
	 * Forward declaration(s)
	 */
	class Face;
	class Material;

	/**
	 * Mesh class definition
	 */
	class Mesh
	{
	public:

		/**
		 * Attribute(s)
		 */
		static const unsigned int NoMaterial = ~0u;

	private:

		///	Array with pointer to all stored faces
		Ocelot::VectorExt<Face*> mFaces;
		///	Assigned material
		Material*			mMaterial;
		///	Number of stored indices.
		unsigned int		mIndiceCount;
		/// Number of UV
		unsigned int		mUVCoordinates[ MAX_TEXTURECOORDS ];
		///	Material index.
		unsigned int		mMaterialIndex;
		///	True, if normals are stored.
		bool				mHasNormals;

	public:

		/**
		 * Constructor(s) & Destructor
		 */
		Mesh();
		~Mesh();

		/**
		 * Methods
		 */
		void AddFace(const Face* pNewFace);

		void RemoveFace(const Face* pFace);

		inline unsigned int GetUVCoordinate(int pIndex)
		{
			if ( pIndex < 0 || 
				 pIndex >= MAX_TEXTURECOORDS )
			{
				throw std::exception("Mesh::GetUVCoordinate IndexOutOfBounds");
			}

			return this->mUVCoordinates[ pIndex ];
		}

		inline void SetUVCoordinate(int pIndex, unsigned int pValue)
		{
			if 
				( pIndex < 0 ||
				  pIndex >= MAX_TEXTURECOORDS )
			{
				throw std::exception("Mesh::SetUVCoordinate IndexOutOfBounds");
			}

			this->mUVCoordinates[pIndex] = pValue;
		}

		inline const Material* GetMaterial() const
		{
			return this->mMaterial;
		}

		inline void SetMaterial(Material* pMaterial)
		{
			this->mMaterial = pMaterial;
		}

		inline unsigned int IndiceCount() const
		{
			return this->mIndiceCount;
		}

		inline void SetIndiceCount(unsigned int pIndiceCount)
		{
			this->mIndiceCount = pIndiceCount;
		}

		inline unsigned int MaterialIndex() const
		{
			return this->mMaterialIndex;
		}

		inline void SetMaterialIndex(unsigned int pMaterialIndex)
		{
			this->mMaterialIndex = pMaterialIndex;
		}

		inline const Ocelot::VectorExt<Face*>* Faces() const
		{
			return &this->mFaces;
		}

		inline bool HasNormals() const
		{
			return this->mHasNormals;
		}

		inline void SetHasNormal(bool pHasNormals)
		{
			this->mHasNormals = pHasNormals;
		}
	};
}
