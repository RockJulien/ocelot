
#pragma once

/**
 * Includes
 */
#include <string>
#include "DataStructures/VectorExt.h"
#include "ObjectType.h"
#include "Maths/Matrix4x4.h"

/**
 * Namespace
 */
namespace OBJLoader
{
	/**
	 * Object storing all objects contained in a .obj file class definition
	 */
	class Object
	{
	private:

		/**
		 * Attribute(s)
		 */
		//!	Object name
		std::string mName;
		//!	Transformation matrix, stored in OpenGL format
		Matrix4x4   mTransformation;
		//!	All sub-objects referenced by this object
		Ocelot::VectorExt<Object*> mSubObjects;
		///	Assigned meshes
		Ocelot::VectorExt<unsigned int> mMeshes;

	public:

		/**
		 * Constructor(s) & Destructor
		 */
		Object();
		~Object();

		/**
		 * Methods
		 */
		inline void AddMesh(unsigned int pNewMesh)
		{
			this->mMeshes.Add(pNewMesh);
		}

		inline void RemoveMesh(unsigned int pMesh)
		{
			BIGINT lIndex = this->mMeshes.IndexOf(pMesh);
			this->mMeshes.Remove(lIndex);
		}

		inline void AddSubObject(const Object* pNewObject)
		{
			this->mSubObjects.Add(const_cast<Object*>(pNewObject) );
		}

		inline void RemoveSubObject(const Object* pObject)
		{
			BIGINT lIndex = this->mSubObjects.IndexOf( const_cast<Object*>(pObject) );
			this->mSubObjects.Remove(lIndex);
		}

		inline const std::string& Name() const
		{
			return this->mName;
		}

		inline void SetName(const std::string& pName)
		{
			this->mName = pName;
		}

		inline Matrix4x4* EditTransformation()
		{
			return &this->mTransformation;
		}

		inline const Matrix4x4* Transformation() const
		{
			return &this->mTransformation;
		}

		inline const Ocelot::VectorExt<unsigned int>* Meshes() const
		{
			return &this->mMeshes;
		}

		inline const Ocelot::VectorExt<Object*>* SubObjects() const
		{
			return &this->mSubObjects;
		}
	};
}
