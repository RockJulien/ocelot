
#pragma once

/**
 * Includes
 */
#include "DataStructures/VectorExt.h"
#include "Geometry/PrimitiveTypes.h"

/**
 * Namespace
 */
namespace OBJLoader
{
	/**
	 * Forward declaration(s)
	 */
	class Material;

	/**
	 * Object-face class definition
	 */
	class Face
	{
	public:

		/**
		 * Typedef(s)
		 */
		typedef Ocelot::VectorExt<unsigned int> IndexArray;

	private:

		/**
		 * Attribute(s)
		 */
		//!	Primitive type
		PrimitiveTypes::Type mPrimitiveType;
		//!	Vertex indices
		IndexArray* mVertices;
		//!	Normal indices
		IndexArray* mNormals;
		//!	Texture coordinates indices
		IndexArray* mTexCoords;
		//!	Pointer to assigned material
		Material*   mMaterial;

	public:

		/**
		 * Constructor(s) & Destructor
		 */
		Face(IndexArray* pVertices,
			 IndexArray* pNormals,
			 IndexArray* pTexCoords,
			 PrimitiveTypes::Type pPrimitive = PrimitiveTypes::ePOLYGON);
		~Face();

		/**
		 * Methods
		 */
		inline PrimitiveTypes::Type PrimitiveType() const
		{
			return this->mPrimitiveType;
		}

		inline const IndexArray* Vertices() const
		{
			return this->mVertices;
		}

		inline const IndexArray* Normals() const
		{
			return this->mNormals;
		}

		inline const IndexArray* TexCoords() const
		{
			return this->mTexCoords;
		}

		inline const Material* GetMaterial() const
		{
			return this->mMaterial;
		}

		inline void SetMaterial(Material* pMaterial)
		{
			this->mMaterial = pMaterial;
		}
	};
}
