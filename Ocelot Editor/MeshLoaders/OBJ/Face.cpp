
/**
 * Includes
 */
#include "Face.h"

/**
 * Usings
 */
using namespace std;
using namespace OBJLoader;

/********************************************************************************************/
Face::Face( IndexArray* pVertices,
			IndexArray* pNormals,
			IndexArray* pTexCoords,
			PrimitiveTypes::Type pPrimitive ) :
			mPrimitiveType(pPrimitive), mVertices(pVertices), mNormals( pNormals),
			mTexCoords(pTexCoords), mMaterial( nullptr)
{

}

/********************************************************************************************/
Face::~Face()
{
	delete this->mVertices;
	this->mVertices = NULL;

	delete this->mNormals;
	this->mNormals = NULL;

	delete this->mTexCoords;
	this->mTexCoords = NULL;
}
