
/**
 * Includes
 */
#include "Object.h"

/**
 * Usings
 */
using namespace std;
using namespace Ocelot;
using namespace OBJLoader;

Object::Object() :
mName("")
{

}

Object::~Object()
{
	for 
		( VectorIter<Object*> lIter = this->mSubObjects.Iterator();
							  lIter.NotEnd(); 
							  lIter.MoveNext() )
	{
		delete lIter.Current();
	}
	this->mSubObjects.RemoveAll();
}
