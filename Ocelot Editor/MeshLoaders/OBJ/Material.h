
#pragma once

/**
 * Includes
 */
#include <string>
#include "TextureTypes.h"
#include "Maths/Vector3.h"

/**
 * Namespace
 */
namespace OBJLoader
{
	/**
	 * Object-material class definition
	 */
	class Material
	{
	private:

		/**
		 * Attribute(s)
		 */
		bool mTypesMap[TextureTypes::eCount];

		//!	Name of material description
		std::string mMaterialName;

		//!	Texture names
		std::string mTexture;
		std::string mSpecular;
		std::string mAmbient;
		std::string mEmissive;
		std::string mBump;
		std::string mNormal;
		std::string mSpecularity;
		std::string mOpacity;
		std::string mDisp;

		//!	Ambient color 
		Vector3 mAmbientRGB;
		//!	Diffuse color
		Vector3 mDiffuseRGB;
		//!	Specular color
		Vector3 mSpecularRGB;
		//!	Emissive color
		Vector3 mEmissiveRGB;
		//!	Alpha value
		float mAlpha;
		//!	Shineness factor
		float mShininess;
		//!	Illumination model 
		int mIlluminationModel;
		//! Index of refraction
		float mRefractionIndex;

	public:

		/**
		 * Constructor(s) & Destructor
		 */
		Material();
		~Material();

		/**
		 * Methods
		 */
		inline void AddType(TextureTypes::Type pType)
		{
			this->mTypesMap[ pType ] = true;
		}

		inline void RemoveType(TextureTypes::Type pType)
		{
			this->mTypesMap[ pType ] = false;
		}

		inline bool HasType(TextureTypes::Type pType) const
		{
			return this->mTypesMap[ pType ];
		}

		inline const std::string& MaterialName() const
		{
			return this->mMaterialName;
		}

		inline void SetMaterialName(const std::string& pMaterialName)
		{
			this->mMaterialName = pMaterialName;
		}

		inline const std::string& Texture() const
		{
			return this->mTexture;
		}

		inline void SetTexture(const std::string& pTexture)
		{
			this->mTexture = pTexture;
		}

		inline const std::string& Specular() const
		{
			return this->mSpecular;
		}

		inline void SetSpecular(const std::string& pSpecular)
		{
			this->mSpecular = pSpecular;
		}

		inline const std::string& Ambient() const
		{
			return this->mAmbient;
		}

		inline void SetAmbient(const std::string& pAmbient)
		{
			this->mAmbient = pAmbient;
		}

		inline const std::string& Emissive() const
		{
			return this->mEmissive;
		}

		inline void SetEmissive(const std::string& pEmissive)
		{
			this->mEmissive = pEmissive;
		}

		inline const std::string& Bump() const
		{
			return this->mBump;
		}

		inline void SetBump(const std::string& pBump)
		{
			this->mBump = pBump;
		}

		inline const std::string& Normal() const
		{
			return this->mNormal;
		}

		inline void SetNormal(const std::string& pNormal)
		{
			this->mNormal = pNormal;
		}

		inline const std::string& Specularity() const
		{
			return this->mSpecularity;
		}

		inline void SetSpecularity(const std::string& pSpecularity)
		{
			this->mSpecularity = pSpecularity;
		}

		inline const std::string& Opacity() const
		{
			return this->mOpacity;
		}

		inline void SetOpacity(const std::string& pOpacity)
		{
			this->mOpacity = pOpacity;
		}

		inline const std::string& Disp() const
		{
			return this->mDisp;
		}

		inline void SetDisp(const std::string& pDisp)
		{
			this->mDisp = pDisp;
		}

		inline Vector3 AmbientRGB() const
		{
			return this->mAmbientRGB;
		}

		inline void SetAmbientRGB(Vector3 pAmbientRGB)
		{
			this->mAmbientRGB = pAmbientRGB;
		}

		inline Vector3 DiffuseRGB() const
		{
			return this->mDiffuseRGB;
		}

		inline void SetDiffuseRGB(Vector3 pDiffuseRGB)
		{
			this->mDiffuseRGB = pDiffuseRGB;
		}

		inline Vector3 SpecularRGB() const
		{
			return this->mSpecularRGB;
		}

		inline void SetSpecularRGB(Vector3 pSpecularRGB)
		{
			this->mSpecularRGB = pSpecularRGB;
		}

		inline Vector3 EmissiveRGB() const
		{
			return this->mEmissiveRGB;
		}

		inline void SetEmissiveRGB(Vector3 pEmissiveRGB)
		{
			this->mEmissiveRGB = pEmissiveRGB;
		}

		inline float Alpha() const
		{
			return this->mAlpha;
		}

		inline void SetAlpha(float pAlpha)
		{
			this->mAlpha = pAlpha;
		}

		inline float Shininess() const
		{
			return this->mShininess;
		}

		inline void SetShininess(float pShininess)
		{
			this->mShininess = pShininess;
		}

		inline int IlluminationModel() const
		{
			return this->mIlluminationModel;
		}

		inline void SetIlluminationModel(int pIlluminationModel)
		{
			this->mIlluminationModel = pIlluminationModel;
		}

		inline float RefractionIndex() const
		{
			return this->mRefractionIndex;
		}

		inline void SetRefractionIndex(float pRefractionIndex)
		{
			this->mRefractionIndex = pRefractionIndex;
		}
	};
}
