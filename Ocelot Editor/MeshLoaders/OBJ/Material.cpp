
/**
 * Includes
 */
#include "Material.h"

/**
 * Usings
 */
using namespace std;
using namespace OBJLoader;

/********************************************************************************************/
Material::Material() : 
mDiffuseRGB(0.6f, 0.6f, 0.6f)
, mAlpha(1.f)
, mShininess(0.0f)
, mIlluminationModel(1)
, mRefractionIndex(1.f)
{
	for 
		(size_t lCurr = 0; lCurr < TextureTypes::eCount; ++lCurr)
	{
		this->mTypesMap[lCurr] = false;
	}
}

/********************************************************************************************/
Material::~Material()
{

}
