
#include "Face.h"
#include "Mesh.h"
#include "Model.h"
#include "Object.h"
#include "Material.h"
#include <exception>
#include "OBJUtilities.h"
#include "OBJMaterialLoader.h"
#include "StringHelper/STRINGHelpers.cpp"
#include "OBJParser.h"

using namespace std;
using namespace Ocelot;
using namespace OBJLoader;

OBJParser::OBJParser(const std::string& pModelName) :
mDataIt(),
mDataItEnd(),
mModel(nullptr),
mCurrentLine(0)
{
	fill_n( this->mBuffer, cBUFFERSIZE, 0 );

	this->mModel = new Model();
	this->mModel->SetModelName( pModelName );

	Material* lDefaultMaterial = new Material();
	lDefaultMaterial->SetMaterialName( "Default" );
	this->mModel->SetDefaultMaterial( lDefaultMaterial );
	this->mModel->AddMaterial( lDefaultMaterial );
	this->mModel->AddMaterialToLib( lDefaultMaterial );
}

OBJParser::~OBJParser()
{
	if 
		( this->mModel != nullptr )
	{
		delete this->mModel;
		this->mModel = nullptr;
	}
}

bool OBJParser::Parse(DataArray& pData)
{
	this->mDataIt    = pData.begin();
	this->mDataItEnd = pData.end();

	return this->InternalParseFile();
}

bool OBJParser::InternalParseFile()
{
	if
		(mDataIt == mDataItEnd)
	{
		return false;
	}

	while
		(mDataIt != mDataItEnd)
	{
		switch
			(*mDataIt)
		{
		case 'v': // Parse a vertex texture coordinate
		{
			++mDataIt;
			if
				(*mDataIt == ' ' ||
				*mDataIt == '\t')
			{
				// read in vertex definition
				this->GetVector3(this->mPositions);
			}
			else if
				(*mDataIt == 't')
			{
				// read in texture coordinate ( 2D or 3D )
				++mDataIt;
				this->GetVector(this->mTexCoords);
			}
			else if
				(*mDataIt == 'n')
			{
				// Read in normal vector definition
				++mDataIt;
				this->GetVector3(this->mNormals);
			}
		}
		break;
		case 'p': // Parse a face, line or point statement
		case 'l':
		case 'f':
		{
			this->GetFace(*mDataIt == 'f' ? PrimitiveTypes::ePOLYGON : (*mDataIt == 'l'
				? PrimitiveTypes::eLINE : PrimitiveTypes::ePOINT));
		}
		break;
		case '#': // Parse a comment
		{
			this->GetComment();
		}
		break;
		case 'u': // Parse a material desc. setter
		{
			this->GetMaterialDesc();
		}
		break;
		case 'm': // Parse a material library or merging group ('mg')
		{
			if
				(*(mDataIt + 1) == 'g')
			{
				this->GetGroupNumberAndResolution();
			}
			else
			{
				this->GetMaterialLib();
			}
		}
		break;
		case 'g': // Parse group name
		{
			this->GetGroupName();
		}
		break;
		case 's': // Parse group number
		{
			this->GetGroupNumber();
		}
		break;
		case 'o': // Parse object name
		{
			this->GetObjectName();
		}
		break;
		default:
		{
			this->mDataIt = OBJUtilities::skipLine<DataArrayIt>(this->mDataIt, this->mDataItEnd, this->mCurrentLine);
		}
		break;
		}
	}

	return true;
}

void OBJParser::CopyNextWord(CCHAR* pBuffer, size_t pLength)
{
	size_t index = 0;
	this->mDataIt = OBJUtilities::getNextWord<DataArrayIt>(mDataIt, mDataItEnd);
	while
		( mDataIt != mDataItEnd &&
		  OBJUtilities::isSeparator(*mDataIt) == false)
	{
		pBuffer[index] = *mDataIt;
		index++;
		if 
			( index == pLength - 1)
		{
			break;
		}
		++mDataIt;
	}

	assert(index < pLength);
	pBuffer[index] = '\0';
}

void OBJParser::CopyNextLine(CCHAR* pBuffer, size_t pLength)
{
	size_t index = 0u;

	// some OBJ files have line continuations using \ (such as in C++ et al)
	bool lHasContinuation = false;
	for
		(  ;
			mDataIt != mDataItEnd && index < pLength - 1;
			++mDataIt )
	{
		const char c = *mDataIt;
		if (c == '\\')
		{
			lHasContinuation = true;
			continue;
		}

		if (c == '\n' || c == '\r')
		{
			if (lHasContinuation)
			{
				pBuffer[index++] = ' ';
				continue;
			}
			break;
		}

		lHasContinuation = false;
		pBuffer[index++] = c;
	}

	assert(index < pLength);
	pBuffer[index] = '\0';
}

void OBJParser::GetVector(Ocelot::VectorExt<Vector3>& pVectorArray)
{
	size_t lComponentCount(0);
	DataArrayIt tmp(mDataIt);
	while 
		( OBJUtilities::IsLineEnd(*tmp) == false )
	{
		if (*tmp == ' ')
		{
			++lComponentCount;
		}
		tmp++;
	}

	float x, y, z;
	if
		( lComponentCount == 2)
	{
		this->CopyNextWord(mBuffer, cBUFFERSIZE);
		x = (float)atof(mBuffer);

		this->CopyNextWord(mBuffer, cBUFFERSIZE);
		y = (float)atof(mBuffer);
		z = 0.0;
	}
	else if
		( lComponentCount == 3)
	{
		this->CopyNextWord(mBuffer, cBUFFERSIZE);
		x = (float)atof(mBuffer);

		this->CopyNextWord(mBuffer, cBUFFERSIZE);
		y = (float)atof(mBuffer);

		this->CopyNextWord(mBuffer, cBUFFERSIZE);
		z = (float)atof(mBuffer);
	}
	else
	{
		assert(!"Invalid number of components");
	}
	pVectorArray.Add(Vector3(x, y, z));
	mDataIt = OBJUtilities::skipLine<DataArrayIt>(mDataIt, mDataItEnd, mCurrentLine);
}

void OBJParser::GetVector3(Ocelot::VectorExt<Vector3>& pVector3Array)
{
	float x, y, z;
	this->CopyNextWord(mBuffer, cBUFFERSIZE);
	x = (float)atof(mBuffer);

	this->CopyNextWord(mBuffer, cBUFFERSIZE);
	y = (float)atof(mBuffer);

	this->CopyNextWord(mBuffer, cBUFFERSIZE);
	z = (float)atof(mBuffer);

	pVector3Array.Add(Vector3(x, y, z));
	mDataIt = OBJUtilities::skipLine<DataArrayIt>(mDataIt, mDataItEnd, mCurrentLine);
}

void OBJParser::GetVector2(Ocelot::VectorExt<Vector2>& pVector2Array)
{
	float x, y;
	this->CopyNextWord(mBuffer, cBUFFERSIZE);
	x = (float)atof(mBuffer);

	this->CopyNextWord(mBuffer, cBUFFERSIZE);
	y = (float)atof(mBuffer);

	pVector2Array.Add(Vector2(x, y));

	mDataIt = OBJUtilities::skipLine<DataArrayIt>(mDataIt, mDataItEnd, mCurrentLine);
}

void OBJParser::GetFace(PrimitiveTypes::Type pType)
{
	this->CopyNextLine( this->mBuffer, cBUFFERSIZE );
	if 
		( this->mDataIt == this->mDataItEnd )
	{
		return;
	}

	CCHAR* lPtr = this->mBuffer;
	CCHAR* lEnd = &lPtr[ cBUFFERSIZE ];
	lPtr = OBJUtilities::getNextToken<CCHAR*>( lPtr, lEnd );
	if 
		( lPtr == lEnd || 
		  *lPtr == '\0' )
	{
		return;
	}

	VectorExt<UBIGINT>* lIndices  = new VectorExt<UBIGINT>();
	VectorExt<UBIGINT>* lTexID    = new VectorExt<UBIGINT>();
	VectorExt<UBIGINT>* lNormalID = new VectorExt<UBIGINT>();
	bool lHasNormal = false;

	const BIGINT lVertexCount   = this->mModel->Vectices()->Size();
	const BIGINT lTexCoordCount = this->mModel->TextureCoords()->Size();
	const BIGINT lNormalCount   = this->mModel->Normals()->Size();

	const BBOOL lAnyTexCoords = this->mModel->TextureCoords()->Any();
	const BBOOL lAnyNormals   = this->mModel->Normals()->Any();
	BIGINT lStep = 0, lPos = 0;
	while 
		( lPtr != lEnd )
	{
		lStep = 1;

		if 
			( OBJUtilities::IsLineEnd( *lPtr ) )
		{
			break;
		}

		if 
			( *lPtr == '/' )
		{
			if 
				( pType == PrimitiveTypes::ePOINT )
			{
				throw new exception("OBJParser::GetFace: Separator unexpected in point statement");
			}

			if 
				( lPos == 0)
			{
				//if there are no texture coordinates in the file, but normals
				if 
					( lAnyTexCoords == false && 
					  lAnyNormals ) 
				{
					lPos = 1;
					lStep++;
				}
			}
			lPos++;
		}
		else if 
			( OBJUtilities::isSeparator( *lPtr ) )
		{
			lPos = 0;
		}
		else
		{
			//OBJ USES 1 Base ARRAYS!!!!
			const BIGINT lVal = atoi( lPtr );

			// increment iStep position based off of the sign and # of digits
			BIGINT lTemp = lVal;
			if 
				( lVal < 0 )
			{
				++lStep;
			}

			while 
				( (lTemp = lTemp / 10) != 0 )
			{
				++lStep;
			}

			if 
				( lVal > 0 )
			{
				// Store parsed index
				if 
					( lPos == 0 )
				{
					lIndices->Add( lVal - 1);
				}
				else if 
					( lPos == 1 )
				{
					lTexID->Add( lVal - 1 );
				}
				else if 
					( lPos == 2 )
				{
					lNormalID->Add( lVal - 1 );
					lHasNormal = true;
				}
				else
				{
					this->ReportErrorTokenInFace();
				}
			}
			else if 
				( lVal < 0 )
			{
				// Store relatively index
				if 
					( lPos == 0 )
				{
					lIndices->Add( lVertexCount + lVal );
				}
				else if 
					( lPos == 1 )
				{
					lTexID->Add( lTexCoordCount + lVal );
				}
				else if 
					( lPos == 2 )
				{
					lNormalID->Add( lNormalCount + lVal );
					lHasNormal = true;
				}
				else
				{
					this->ReportErrorTokenInFace();
				}
			}
		}
		lPtr += lStep;
	}

	if 
		( lIndices->Any() == false )
	{
		//DefaultLogger::get()->error("Obj: Ignoring empty face");
		this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
		return;
	}

	Face* lFace = new Face( lIndices, lNormalID, lTexID, pType );

	const Material* lCurrentMaterial = this->mModel->CurrentMaterial();
	// Set active material, if one set
	if 
		( lCurrentMaterial != nullptr )
	{
		lFace->SetMaterial( const_cast<Material*>(lCurrentMaterial) );
	}
	else
	{
		lFace->SetMaterial( const_cast<Material*>(this->mModel->DefaultMaterial()) );
	}

	const Object* lCurrentObject = this->mModel->CurrentObject();
	// Create a default object, if nothing is there
	if 
		( lCurrentObject == nullptr )
	{
		this->CreateObject( "defaultobject" );
	}

	const Mesh* lCurrentMesh = this->mModel->CurrentMesh();
	// Assign face to mesh
	if 
		( lCurrentMesh == nullptr )
	{
		this->CreateMesh();
	}

	Mesh* lMesh = const_cast<Mesh*>( this->mModel->CurrentMesh() );
	// Store the face
	lMesh->AddFace( lFace );
	lMesh->SetIndiceCount( lMesh->IndiceCount() + lFace->Vertices()->Size() );
	lMesh->SetUVCoordinate( 0, lMesh->GetUVCoordinate( 0 ) + lFace->TexCoords()->Size() );
	if 
		( lMesh->HasNormals() == false && 
		  lHasNormal )
	{
		lMesh->SetHasNormal( true );
	}

	// Skip the rest of the line
	this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
}

void OBJParser::GetMaterialDesc()
{
	// Each material request a new object.
	// Sometimes the object is already created (see 'o' tag by example), but it is not initialized !
	// So, we create a new object only if the current on is already initialized !
	const Object* lCurrentObject = this->mModel->CurrentObject();
	if ( lCurrentObject != nullptr &&
		(lCurrentObject->Meshes()->Size() > 1 ||
		(lCurrentObject->Meshes()->Size() == 1 && (*this->mModel->Meshes())[ (*lCurrentObject->Meshes())[0] ]->Faces()->Size() != 0 ) ) )
	{
		this->mModel->SetCurrentObject( nullptr );
	}

	// Get next data for material data
	this->mDataIt = OBJUtilities::getNextToken<DataArrayIt>( this->mDataIt, this->mDataItEnd );
	if 
		( this->mDataIt == this->mDataItEnd )
	{
		return;
	}

	CCHAR* lStart = &(*this->mDataIt);
	while 
		( this->mDataIt != this->mDataItEnd && 
		  OBJUtilities::isSeparator(*this->mDataIt) == false )
	{
		++this->mDataIt;
	}

	// Get name
	string lMaterialName( lStart, &(*this->mDataIt) );
	if 
		( lMaterialName.empty() )
	{
		return;
	}

	// Search for material
	const Material* lMaterial = this->mModel->GetMaterialByName( lMaterialName );
	if 
		( lMaterial == nullptr )
	{
		// Not found, use default material
		this->mModel->SetCurrentMaterial( const_cast<Material*>( this->mModel->DefaultMaterial() ) );
		//DefaultLogger::get()->error("OBJ: failed to locate material " + strName + ", skipping");
	}
	else
	{
		// Found, using detected material
		this->mModel->SetCurrentMaterial( const_cast<Material*>( lMaterial ) );
		if 
			( this->NeedsNewMesh( lMaterialName ) )
		{
			this->CreateMesh();
		}

		const_cast<Mesh*>( this->mModel->CurrentMesh() )->SetMaterialIndex( this->GetMaterialIndex( lMaterialName ) );
	}

	// Skip rest of line
	this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
}

void OBJParser::GetComment()
{
	while
		(mDataIt != mDataItEnd)
	{
		if
			('\n' == (*mDataIt))
		{
			++mDataIt;
			break;
		}
		else
		{
			++mDataIt;
		}
	}
}

void OBJParser::GetMaterialLib()
{
	// Translate tuple
	this->mDataIt = OBJUtilities::getNextToken<DataArrayIt>( this->mDataIt, this->mDataItEnd );
	if 
		( this->mDataIt == this->mDataItEnd )
	{
		return;
	}

	CCHAR* lStart = &(*this->mDataIt);
	while 
		( this->mDataIt != this->mDataItEnd && 
		  OBJUtilities::isNewLine(*this->mDataIt) == false )
	{
		this->mDataIt++;
	}

	// Check for existence
	const string lMaterialName( lStart, &(*this->mDataIt) );
	ifstream lInFile(lMaterialName);

	if 
		(!lInFile)
	{
		//DefaultLogger::get()->error("OBJ: Unable to locate material file " + lMaterialName );
		this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
		return;
	}

	// Import material library data from file
	OBJMaterialLoader::DataArray lBuffer;
	OBJParser::TextFileToBuffer( &lInFile, lBuffer );
	lInFile.close();

	// Importing the material library 
	OBJMaterialLoader lMaterialLoader( lBuffer, lMaterialName, this->mModel );
}

void OBJParser::GetNewMaterial()
{
	mDataIt = OBJUtilities::getNextToken<DataArrayIt>( mDataIt, mDataItEnd );
	mDataIt = OBJUtilities::getNextWord<DataArrayIt>( mDataIt, mDataItEnd );
	
	if 
		( mDataIt == mDataItEnd)
	{
		return;
	}

	CCHAR* lStart = &(*mDataIt);
	string lMaterialName( lStart, *mDataIt );
	while 
		( mDataIt != mDataItEnd && 
		  OBJUtilities::isSeparator(*mDataIt) )
	{
		mDataIt++;
	}

	const Material* lMaterial = this->mModel->GetMaterialByName( lMaterialName );
	if 
		( lMaterial == nullptr )
	{
		// Show a warning, if material was not found
		//DefaultLogger::get()->warn("OBJ: Unsupported material requested: " + strMat);
		this->mModel->SetCurrentMaterial( const_cast<Material*>( this->mModel->DefaultMaterial() ) );
	}
	else
	{
		// Set new material
		if 
			( this->NeedsNewMesh( lMaterialName ) )
		{
			this->CreateMesh();
		}

		const_cast<Mesh*>( this->mModel->CurrentMesh() )->SetMaterialIndex( this->GetMaterialIndex( lMaterialName ) );
	}

	mDataIt = OBJUtilities::skipLine<DataArrayIt>( mDataIt, mDataItEnd, mCurrentLine );
}

void OBJParser::GetGroupName()
{
	string lGroupName;

	mDataIt = OBJUtilities::getName<DataArrayIt>(mDataIt, mDataItEnd, lGroupName);
	if
		( OBJUtilities::isEndOfBuffer(mDataIt, mDataItEnd) )
	{
		return;
	}

	// Change active group, if necessary
	if 
		( this->mModel->ActiveGroupName() != lGroupName )
	{
		// Search for already existing entry
		Model::ConstGroupMapIt lGroupIter = this->mModel->Groups()->find( lGroupName );

		// We are mapping groups into the object structure
		this->CreateObject( lGroupName );

		// New group name, creating a new entry
		if 
			( lGroupIter == this->mModel->Groups()->end() )
		{
			Ocelot::VectorExt<UBIGINT>* lGroup = this->mModel->AddGroup( lGroupName );
			this->mModel->SetGroupFaceIDs( lGroup );
		}
		else
		{
			this->mModel->SetGroupFaceIDs( lGroupIter->second );
		}
		this->mModel->SetActiveGroupName( lGroupName );
	}
	mDataIt = OBJUtilities::skipLine<DataArrayIt>(mDataIt, mDataItEnd, mCurrentLine);
}

void OBJParser::GetGroupNumber()
{
	// Not used

	mDataIt = OBJUtilities::skipLine<DataArrayIt>(mDataIt, mDataItEnd, mCurrentLine);
}

void OBJParser::GetGroupNumberAndResolution()
{
	// Not used

	mDataIt = OBJUtilities::skipLine<DataArrayIt>(mDataIt, mDataItEnd, mCurrentLine);
}

int  OBJParser::GetMaterialIndex(const string& pMaterialName)
{
	int lIndex = -1;
	if 
		( pMaterialName.empty() )
	{
		return lIndex;
	}

	for 
		( BIGINT lCurr = 0; 
				 lCurr < this->mModel->MaterialLib()->Size(); 
				 lCurr++ )
	{
		if 
			( pMaterialName == this->mModel->MaterialLib()->GetAt( lCurr ) )
		{
			lIndex = lCurr;
			break;
		}
	}

	return lIndex;
}

void OBJParser::GetObjectName()
{
	mDataIt = OBJUtilities::getNextToken<DataArrayIt>(mDataIt, mDataItEnd);
	if 
		( mDataIt == mDataItEnd)
	{
		return;
	}

	CCHAR* lStart = &(*mDataIt);
	while 
		( mDataIt != mDataItEnd && 
		  OBJUtilities::isSeparator(*mDataIt) == false )
	{
		++mDataIt;
	}

	string lObjectName( lStart, &(*mDataIt) );
	if 
		( lObjectName.empty() == false )
	{
		// Reset current object
		this->mModel->SetCurrentObject( nullptr );

		// Search for actual object
		for 
			( VectorIter<Object*> lIter = this->mModel->Objects()->Iterator();
								  lIter.NotEnd();
								  lIter.MoveNext() )
		{
			if 
				( lObjectName == lIter.Current()->Name() )
			{
				this->mModel->SetCurrentObject( lIter.Current() );
				break;
			}
		}

		// Allocate a new object, if current one was not found before
		if 
			( this->mModel->CurrentObject() == nullptr )
		{
			this->CreateObject( lObjectName );
		}
	}

	mDataIt = OBJUtilities::skipLine<DataArrayIt>( mDataIt, mDataItEnd, mCurrentLine );
}

void OBJParser::CreateObject(const string& pObjectName)
{
	assert( this->mModel != nullptr );

	Object* lNewObject = new Object();
	lNewObject->SetName( pObjectName );
	this->mModel->SetCurrentObject( lNewObject );
	this->mModel->AddObject( lNewObject );

	this->CreateMesh();

	const Material* lCurrentMaterial = this->mModel->CurrentMaterial();
	if 
		( lCurrentMaterial != nullptr )
	{
		Mesh* lCurrentMesh = const_cast<Mesh*>( this->mModel->CurrentMesh() );
		lCurrentMesh->SetMaterialIndex( this->GetMaterialIndex( lCurrentMaterial->MaterialName() ) );
		lCurrentMesh->SetMaterial( const_cast<Material*>( this->mModel->CurrentMaterial() ) );
	}
}

void OBJParser::CreateMesh()
{
	assert( this->mModel != nullptr );

	Mesh* lNewMesh = new Mesh();
	this->mModel->SetCurrentMesh( lNewMesh );
	this->mModel->AddMesh( lNewMesh );
	UBIGINT lMeshId = this->mModel->Meshes()->Size() - 1;

	const Object* lCurrentObject = this->mModel->CurrentObject();
	if 
		( lCurrentObject != nullptr )
	{
		const_cast<Object*>( lCurrentObject )->AddMesh( lMeshId );
	}
	else
	{
		throw exception( "OBJParser::CreateMesh: No object detected to attach a new mesh instance." );
	}
}

bool OBJParser::NeedsNewMesh(const string& pMaterialName)
{
	const Mesh* lCurrentMesh = this->mModel->CurrentMesh();
	if 
		( lCurrentMesh == nullptr )
	{
		return true;
	}

	bool lNeedMaterial = false;
	int lIndex = this->GetMaterialIndex( pMaterialName );
	int lCurrentMaterialIndex = lCurrentMesh->MaterialIndex();
	if 
		( lCurrentMaterialIndex != int(Mesh::NoMaterial) || 
		  lCurrentMaterialIndex != lIndex )
	{
		// New material -> only one material per mesh, so we need to create a new 
		// material
		lNeedMaterial = true;
	}

	return lNeedMaterial;
}

void OBJParser::ReportErrorTokenInFace()
{
	mDataIt = OBJUtilities::skipLine<DataArrayIt>( mDataIt, mDataItEnd, mCurrentLine );
	// TO DO : Report error.
}

void OBJParser::TextFileToBuffer(std::ifstream* pStream, std::vector<char>& pData)
{
	assert(pStream != nullptr);

	if
		(pStream->eof())
	{
		throw exception("File is empty");
	}

	// get length of file:
	pStream->seekg(0, pStream->end);
	BIGINT lFileSize = (BIGINT)pStream->tellg();
	pStream->seekg(0, pStream->beg);

	pData.reserve(lFileSize + 1);
	pData.resize(lFileSize);
	pStream->read(&pData[0], lFileSize);
	if
		(!pStream)
	{
		BIGINT lReadCount = (BIGINT)pStream->gcount();
		string lError = "Error: only ";
		toString(lReadCount, lError);
		lError.append(" could be read");
		throw exception(lError.c_str());
	}

	//ConvertToUTF8(pData);

	// append a binary zero to simplify string parsing
	pData.push_back(0);
}