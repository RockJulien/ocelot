
#include "Model.h"
#include "Mesh.h"
#include "Face.h"
#include "Object.h"
#include "Material.h"

using namespace std;
using namespace Ocelot;
using namespace OBJLoader;

Model::Model() :
mModelName(""),
mCurrentObject(nullptr),
mCurrentMaterial(nullptr),
mDefaultMaterial(nullptr),
mGroupFaceIDs(nullptr),
mActiveGroupName(""),
mCurrentMesh(nullptr)
{

}

Model::~Model()
{
	for 
		( VectorIter<Object*> lIter = this->mObjects.Iterator(); 
							  lIter.NotEnd(); 
							  lIter.MoveNext() )
	{
		delete lIter.Current();
	}
	this->mObjects.RemoveAll();

	for
		( VectorIter<Mesh*> lIter = this->mMeshes.Iterator();
							lIter.NotEnd();
							lIter.MoveNext() )
	{
		delete lIter.Current();
	}
	this->mMeshes.RemoveAll();

	for ( GroupMapIt it = this->mGroups.begin(); 
		             it != this->mGroups.end(); 
					 ++it)
	{
		delete it->second;
	}
	this->mGroups.clear();

	for 
		( std::map<std::string, Material*>::iterator it = this->mMaterialMap.begin(); 
													 it != this->mMaterialMap.end(); 
													 ++it) 
	{
		delete it->second;
	}
	this->mMaterialMap.clear();
}

Ocelot::VectorExt<unsigned int>* Model::AddGroup(const std::string& pGroupName)
{
	Ocelot::VectorExt<unsigned int>* lNewGroup = new Ocelot::VectorExt<unsigned int>();
	this->mGroups.insert( std::pair<std::string, Ocelot::VectorExt<unsigned int>*>( pGroupName, lNewGroup ) );
	return lNewGroup;
}

void Model::AddToGroup(const std::string& pGroupName, unsigned int pToAdd)
{
	map<std::string, Ocelot::VectorExt<unsigned int>*>::iterator lIter = this->mGroups.find( pGroupName );
	if 
		( lIter != this->mGroups.end() )
	{
		lIter->second->Add( pToAdd );
	}
}

void Model::RemoveGroup(const std::string& pGroupName)
{
	map<std::string, Ocelot::VectorExt<unsigned int>*>::iterator lIter = this->mGroups.find( pGroupName );
	if 
		( lIter != this->mGroups.end() )
	{
		this->mGroups.erase( lIter );
	}
}

void Model::RemoveFromGroup(const std::string& pGroupName, int pIndex)
{
	map<std::string, Ocelot::VectorExt<unsigned int>*>::iterator lIter = this->mGroups.find(pGroupName);
	if
		( lIter != this->mGroups.end() )
	{
		lIter->second->Remove( pIndex );
	}
}

void Model::AddObject(const Object* pNewObject)
{
	this->mObjects.Add( const_cast<Object*>(pNewObject) );
}

void Model::RemoveObject(const Object* pObject)
{
	BIGINT lIndex = this->mObjects.IndexOf( const_cast<Object*>(pObject) );
	this->mObjects.Remove( lIndex );
}

void Model::AddMesh(const Mesh* pNewMesh)
{
	this->mMeshes.Add( const_cast<Mesh*>(pNewMesh) );
}

void Model::RemoveMesh(const Mesh* pMesh)
{
	BIGINT lIndex = this->mMeshes.IndexOf( const_cast<Mesh*>(pMesh) );
	this->mMeshes.Remove( lIndex );
}

void Model::AddVertex(const Vector3& pVertex)
{
	this->mVertices.Add( pVertex );
}

void Model::RemoveVertex(int pIndex)
{
	this->mVertices.Remove( pIndex );
}

void Model::AddNormal(const Vector3& pNormal)
{
	this->mNormals.Add( pNormal );
}

void Model::RemoveNormal(int pIndex)
{
	this->mNormals.Remove( pIndex );
}

void Model::AddTexCoord(const Vector3& pTexCoord)
{
	this->mTextureCoords.Add( pTexCoord );
}

void Model::RemoveTexCoord(int pIndex)
{
	this->mTextureCoords.Remove( pIndex );
}

void Model::AddMaterial(const Material* pMaterial)
{
	this->mMaterialMap.insert( std::pair<string, Material*>( pMaterial->MaterialName(), const_cast<Material*>(pMaterial) ) );
}

void Model::AddMaterialToLib(const Material* pMaterial)
{
	this->mMaterialLib.Add( pMaterial->MaterialName() );
}

void Model::RemoveMaterial(const std::string& pName)
{
	std::map<string, Material*>::iterator lIter = this->mMaterialMap.find( pName );
	if 
		( lIter != this->mMaterialMap.end() )
	{
		this->mMaterialMap.erase( lIter );
	}
}
