
#pragma once

/**
 * Includes
 */
#include "Maths/Vector2.h"
#include "Maths/Vector3.h"
#include "Geometry/PrimitiveTypes.h"
#include "DataStructures\VectorExt.h"

/**
 * Namespace
 */
namespace OBJLoader
{
	/**
	 * Forward declaration(s)
	 */
	class Model;

	/**
	 * OBJ parser class definition
	 */
	class OBJParser
	{
	public:

		/**
		 * Constant(s)
		 */
		static const size_t cBUFFERSIZE = 4096;

		/**
		 * Typedef(s)
		 */
		typedef std::vector<CCHAR> DataArray;
		typedef std::vector<CCHAR>::iterator DataArrayIt;
		typedef std::vector<CCHAR>::const_iterator ConstDataArrayIt;

	private:

		/**
		 * Attribute(s)
		 */
		///	Default material name
		static const std::string cDEFAULT_MATERIAL;
		//!	Iterator to current position in buffer
		DataArrayIt				mDataIt;
		//!	Iterator to end position of buffer
		DataArrayIt				mDataItEnd;
		//!	Pointer to model instance
		Model*					mModel;
		//!	Current line (for debugging)
		UBIGINT					mCurrentLine;
		//!	Helper buffer
		CCHAR					mBuffer[ cBUFFERSIZE ];

		//! Temp structs
		Ocelot::VectorExt<Vector3>  mPositions;
		Ocelot::VectorExt<Vector3>  mNormals;
		Ocelot::VectorExt<Vector3>	mTexCoords;

		/**
		 * Private Methods
		 */
		///	Parse the loaded file
		bool InternalParseFile();
		///	Method to copy the new delimited word in the current line.
		void CopyNextWord(CCHAR* pBuffer, size_t pLength);
		///	Method to copy the new line.
		void CopyNextLine(CCHAR* pBuffer, size_t pLength);
		/// Stores the vector 
		void GetVector(Ocelot::VectorExt<Vector3>& pVectorArray);
		///	Stores the following 3d vector.
		void GetVector3(Ocelot::VectorExt<Vector3>& pVector3Array);
		///	Stores the following 3d vector.
		void GetVector2(Ocelot::VectorExt<Vector2>& pVector2Array);
		///	Stores the following face.
		void GetFace(PrimitiveTypes::Type pType);
		/// Reads the material description.
		void GetMaterialDesc();
		///	Gets a comment.
		void GetComment();
		/// Gets a a material library.
		void GetMaterialLib();
		/// Creates a new material.
		void GetNewMaterial();
		/// Gets the group name from file.
		void GetGroupName();
		/// Gets the group number from file.
		void GetGroupNumber();
		/// Gets the group number and resolution from file.
		void GetGroupNumberAndResolution();
		/// Returns the index of the material. Is -1 if not material was found.
		int  GetMaterialIndex(const std::string& pMaterialName);
		/// Parse object name
		void GetObjectName();
		/// Creates a new object.
		void CreateObject(const std::string& pObjectName);
		///	Creates a new mesh.
		void CreateMesh();
		///	Returns true, if a new mesh instance must be created.
		bool NeedsNewMesh(const std::string& pMaterialName);
		///	Error report in token
		void ReportErrorTokenInFace();

	public:

		/**
		 * Constructor(s) & Destructor
		 */
		OBJParser(const std::string& pModelName);
		~OBJParser();

		/**
		 * Methods
		 */
		bool Parse(DataArray& pData);

		static void TextFileToBuffer(std::ifstream* pStream, std::vector<char>& pData);

		inline const Model* GetModel() const
		{
			return this->mModel;
		}
	};
}