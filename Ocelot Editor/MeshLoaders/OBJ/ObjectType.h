
#pragma once

/**
 * Namespace
 */
namespace OBJLoader
{
	namespace ObjectType
	{
		/**
		 * Object type enumeration definition
		 */
		enum Type
		{
			// Object
			eObject = 0,

			// Group
			eGroup
		};
	}
}
