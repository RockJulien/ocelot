
#pragma once

/**
 * Includes
 */
#include "Maths/Vector2.h"
#include "Maths/Vector3.h"
#include "Geometry/PrimitiveTypes.h"
#include "DataStructures\VectorExt.h"

/**
 * Namespace
 */
namespace OBJLoader
{
	/**
	 * Forward declaration(s)
	 */
	class Model;

	/**
	 * OBJ Material loader class definition
	 */
	class OBJMaterialLoader
	{
	public:

		/**
		 * Constant(s)
		 */
		static const size_t cBUFFERSIZE = 2048;

		/**
		 * Typedef(s)
		 */
		typedef std::vector<CCHAR> DataArray;
		typedef std::vector<CCHAR>::iterator DataArrayIt;
		typedef std::vector<CCHAR>::const_iterator ConstDataArrayIt;

	private:

		/**
		 * Attribute(s)
		 */
		//!	Absolute pathname
		std::string		mAbsPath;
		//!	Data iterator showing to the current position in data buffer
		DataArrayIt		mDataIt;
		//!	Data iterator to end of buffer
		DataArrayIt		mDataItEnd;
		//!	USed model instance
		Model*			mModel;
		//!	Current line in file
		UBIGINT			mCurrentLine;
		//!	Helper buffer
		CCHAR			mBuffer[ cBUFFERSIZE ];

		/**
		 * Private Method(s)
		 */
		///	Load the whole material description
		void Load();
		///	Get color data.
		void GetColorRGBA(Vector3* pColor);
		///	Get illumination model from loaded data
		void GetIlluminationModel(BIGINT& pIllumModel);
		///	Gets a float value from data.	
		void GetFloatValue(float& pValue);
		///	Creates a new material from loaded data.
		void CreateMaterial();
		///	Get texture name from loaded data.
		void GetTexture();
		///	Get the flag indicating whether the texture has a known type or not.
		void GetTextureOption(bool& pHasType);

	public:
		
		/**
		 * Constructor(s) & Destructor
		 */
		explicit OBJMaterialLoader(DataArray& pBuffer, const std::string &pAbsPath, Model* pModel);
		~OBJMaterialLoader();

		/**
		 * Methods
		 */

	};
}