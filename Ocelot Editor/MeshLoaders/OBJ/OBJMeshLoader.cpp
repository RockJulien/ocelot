#include "OBJMeshLoader.h"
#include "ResourceManager\ResourceFactory.h"
#include "StringHelper\STRINGHelpers.h"

// Namespaces used
using namespace std;
using namespace Ocelot;

OBJMeshLoader::OBJMeshLoader()
{

}

OBJMeshLoader::~OBJMeshLoader()
{

}

BBOOL		 OBJMeshLoader::CanLoad(const STRING& pFileName)
{
	LSSTRING lRes;
	// split the extension.
	splitWithDelimiter(pFileName, lRes, '.');
	STRING lExtension = lRes.Last()->Data();
	if 
		( lExtension == "obj" ||
		  lExtension == "OBJ" ) // TO DO : Add a tolower function for having one test.
	{
		return true;
	}

	return false;
}

IOcelotMesh* OBJMeshLoader::LoadFromFile(const DevPtr pDevice, const CamPtr pCamera, const STRING& pFileName)
{
	STRING lStrMaterialFilename;
	this->mCurrentSubsetId = 0;
	this->mCurrentSubsetVertexCount = 0;
	this->mCurrentSubsetFaceCount   = 0;

	STRING lDir;
	LSSTRING lRes;
	splitWithDelimiter( pFileName, lRes, '\\' );
	List2Iterator<STRING> lIter = lRes.iterator();
	for 
		( lIter.begin(); lIter.CurrCell() != lRes.Last(); lIter.next() )
	{
		lDir += lIter.data() + "\\";
	}

	// File input
	CHAR lStrCommand[256] = { 0 };
	ifstream lInFile(pFileName);
	if 
		(!lInFile)
	{
		return NULL;
	}

	// Loop over the file content until the end.
	for 
		(;;)
	{
		lInFile >> lStrCommand;
		if 
			(!lInFile)
		{
			break;
		}

		if 
			(0 == strcmp(lStrCommand, "#"))
		{
			// Comment
		}
		else if 
			(0 == strcmp(lStrCommand, "v"))
		{
			// Vertex Position
			this->LoadPosition( lInFile );
		}
		else if 
			(0 == strcmp(lStrCommand, "vt"))
		{
			// Vertex TexCoord
			this->LoadTextureCoord( lInFile );
		}
		else if 
			(0 == strcmp(lStrCommand, "vn"))
		{
			// Vertex Normal
			this->LoadNormal( lInFile );
		}
		else if 
			(0 == strcmp(lStrCommand, "f"))
		{
			this->LoadTriangle( lInFile );
		}
		else if 
			(0 == strcmp(lStrCommand, "mtllib"))
		{
			// Material library
			lInFile >> lStrMaterialFilename;
		}
		else if 
			(0 == strcmp(lStrCommand, "usemtl"))
		{
			// Material
			this->PrepareMaterial( lInFile );
		}
		else
		{
			// Unimplemented or unrecognized command
		}

		lInFile.ignore(1000, '\n');
	}

	// Cleanup
	lInFile.close();
	this->ReleaseData();

	// If an associated material file was found, read that in as well.
	if 
		( lStrMaterialFilename[0] )
	{
		this->LoadMaterials( lDir + lStrMaterialFilename );
	}

	// Create the mesh and return it.
	MeshInfo lInfo( pDevice, 
		            pCamera, 
					NULL, 
					NULL, 
					L"..\\Effects\\OBJMesh.fx", 
					Vector3(0.0f) );
	lInfo.SetVertices( this->mVertices );
	lInfo.SetIndices( this->mIndices );
	lInfo.SetSubsetTable( this->mSubsets );
	lInfo.SetMaterials( this->mMaterials );
	return new MeshGeometry( lInfo );
}

VVOID OBJMeshLoader::LoadTextureCoord(ifstream& pStream)
{
	// Vertex TexCoord
	FFLOAT lU, lV;
	pStream >> lU >> lV;
	this->mTexCoords.Add( Vector2( lU, lV ) );
}

VVOID OBJMeshLoader::LoadPosition(ifstream& pStream)
{
	// Vertex Position
	FFLOAT lX, lY, lZ;
	pStream >> lX >> lY >> lZ;
	this->mPositions.Add( Vector3( lX, lY, lZ ) );
}

VVOID OBJMeshLoader::LoadNormal(ifstream& pStream)
{
	// Vertex Normal
	FFLOAT lX, lY, lZ;
	pStream >> lX >> lY >> lZ;
	this->mNormals.Add( Vector3( lX, lY, lZ ) );
}

VVOID OBJMeshLoader::LoadTriangle(ifstream& pStream)
{
	// Face
	UBIGINT lPosition, lTexCoord, lNormal;
	EnhancedVertex lVertex;

	for 
		( UBIGINT iFace = 0; iFace < 3; iFace++ )
	{
		ZeroMemory(&lVertex, sizeof(EnhancedVertex));

		// OBJ format uses 1-based arrays
		pStream >> lPosition;
		lVertex.Position = this->mPositions[ lPosition - 1 ];

		if ('/' == pStream.peek())
		{
			pStream.ignore();

			if ('/' != pStream.peek())
			{
				// Optional texture coordinate
				pStream >> lTexCoord;
				lVertex.TexCoord = this->mTexCoords[ lTexCoord - 1 ];
			}

			if ('/' == pStream.peek())
			{
				pStream.ignore();

				// Optional vertex normal
				pStream >> lNormal;
				lVertex.Normal = this->mNormals[ lNormal - 1 ];
			}
		}

		// If a duplicate vertex doesn't exist, add this vertex to the Vertices
		// list. Store the index in the Indices array. The Vertices and Indices
		// lists will eventually become the Vertex Buffer and Index Buffer for
		// the mesh.
		UBIGINT lIndex = this->AddVertex( lPosition, &lVertex );
		if 
			( lIndex == (UBIGINT)-1 )
		{
			return;
		}

		mIndices.Add( lIndex );
	}

	// Incremente the face count
	this->mCurrentSubsetFaceCount++;
}

VVOID OBJMeshLoader::LoadMaterials(const STRING& pFileName)
{
	// File input
	CHAR lStrCommand[256] = { 0 };
	ifstream InFile( pFileName );
	if 
		( !InFile )
	{
		return;
	}

	OBJMaterial* lMaterial = NULL;
	for 
		(;;)
	{
		InFile >> lStrCommand;
		if 
			( !InFile )
		{
			break;
		}

		if 
			( 0 == strcmp(lStrCommand, "newmtl") )
		{
			// Switching active materials
			STRING lStrName;
			InFile >> lStrName;

			lMaterial = NULL;
			for 
				( BIGINT i = 0; i < this->mMaterials.Size(); i++ )
			{
				OBJMaterial* lCurMaterial = this->mMaterials[ i ];
				if 
					( 0 == strcmp(lCurMaterial->Name().c_str(), lStrName.c_str()) )
				{
					lMaterial = lCurMaterial;
					break;
				}
			}
		}

		// The rest of the commands rely on an active material
		if 
			( lMaterial == NULL )
		{
			continue;
		}

		if 
			( 0 == strcmp(lStrCommand, "#") )
		{
			// Comment
		}
		else if 
			( 0 == strcmp(lStrCommand, "Ka") )
		{
			// Ambient color
			FFLOAT lRed, lGreen, lBlue;
			InFile >> lRed >> lGreen >> lBlue;
			lMaterial->SetAmbient( Vector3( lRed, lGreen, lBlue ) );
		}
		else if 
			( 0 == strcmp(lStrCommand, "Kd") )
		{
			// Diffuse color
			FFLOAT lRed, lGreen, lBlue;
			InFile >> lRed >> lGreen >> lBlue;
			lMaterial->SetDiffuse( Vector3( lRed, lGreen, lBlue ) );
		}
		else if 
			( 0 == strcmp(lStrCommand, "Ks") )
		{
			// Specular color
			FFLOAT lRed, lGreen, lBlue;
			InFile >> lRed >> lGreen >> lBlue;
			lMaterial->SetSpecular( Vector3( lRed, lGreen, lBlue ) );
		}
		else if 
			( 0 == strcmp(lStrCommand, "d") ||
			  0 == strcmp(lStrCommand, "Tr") )
		{
			// Alpha
			FFLOAT lAlpha;
			InFile >> lAlpha;
			lMaterial->SetAlpha( lAlpha );
		}
		else if 
			( 0 == strcmp(lStrCommand, "Ns") )
		{
			// Shininess
			BIGINT lShininess;
			InFile >> lShininess;
			lMaterial->SetShininess( lShininess );
		}
		else if 
			( 0 == strcmp(lStrCommand, "illum") )
		{
			// Specular on/off
			BIGINT lIllumination;
			InFile >> lIllumination;
			lMaterial->SetHasSpecular( (lIllumination == 2) );
		}
		else if 
			( 0 == strcmp(lStrCommand, "map_Kd") )
		{
			// Texture
			WSTRING lWideStr;
			STRING lTextureName;
			InFile >> lTextureName;
			toUString(lTextureName, lWideStr);
			lMaterial->SetTexture( ResFactory->GetResource( lWideStr ) );
		}
		else
		{
			// Unimplemented or unrecognized command
		}

		InFile.ignore(1000, L'\n');
	}

	InFile.close();
}

UBIGINT OBJMeshLoader::AddVertex(UBIGINT pHash, const EnhancedVertex* pVertex)
{
	// If this vertex doesn't already exist in the Vertices list, create a new entry.
	// Add the index of the vertex to the Indices list.
	BBOOL lFoundInList = false;
	UBIGINT lIndex = 0;

	// Since it's very slow to check every element in the vertex list, a hashtable stores
	// vertex indices according to the vertex position's index as reported by the OBJ file
	if 
		((UBIGINT)mVertexCache.Size() > pHash)
	{
		CacheEntry* lEntry = mVertexCache[pHash];
		while 
			( lEntry != NULL )
		{
			EnhancedVertex* lCacheVertex = &mVertices[0] + lEntry->Index;

			// If this vertex is identical to the vertex already in the list, simply
			// point the index buffer to the existing vertex
			if 
				( 0 == memcmp(pVertex, lCacheVertex, sizeof(EnhancedVertex)) )
			{
				lFoundInList = true;
				lIndex = lEntry->Index;
				break;
			}

			lEntry = lEntry->Next;
		}
	}

	// Vertex was not found in the list. Create a new entry, both within the Vertices list
	// and also within the hashtable cache
	if 
		( lFoundInList == false )
	{
		// Add to the Vertices list
		lIndex = mVertices.Size();
		mVertices.Add(*pVertex);
		this->mCurrentSubsetVertexCount++;

		// Add this to the hashtable
		CacheEntry* lNewEntry = new CacheEntry;
		if 
			( lNewEntry == NULL )
		{
			return (UBIGINT)-1;
		}

		lNewEntry->Index = lIndex;
		lNewEntry->Next  = NULL;

		// Grow the cache if needed
		while 
			( (UBIGINT)mVertexCache.Size() <= pHash )
		{
			mVertexCache.Add( NULL );
		}

		// Add to the end of the linked list
		CacheEntry* lCurEntry = mVertexCache[pHash];
		if 
			( lCurEntry == NULL )
		{
			// This is the head element
			mVertexCache.SetAt( pHash, lNewEntry );
		}
		else
		{
			// Find the tail
			while 
				( lCurEntry->Next != NULL )
			{
				lCurEntry = lCurEntry->Next;
			}

			lCurEntry->Next = lNewEntry;
		}
	}

	return lIndex;
}

VVOID   OBJMeshLoader::PrepareMaterial(ifstream& pStream)
{
	// Fill the previous subset counts with the incremented data.
	if 
		( this->mSubsets.Size() > this->mCurrentSubsetId )
	{
		Subset* lPreviousSubset = this->mSubsets[ this->mCurrentSubsetId ];
		lPreviousSubset->SetVertexCount( this->mCurrentSubsetVertexCount );
		lPreviousSubset->SetFaceCount( this->mCurrentSubsetFaceCount );
	}

	STRING lStrName;
	pStream >> lStrName;

	BBOOL lFound = false;
	for 
		( BIGINT lMaterial = 0; lMaterial < mMaterials.Size(); lMaterial++ )
	{
		OBJMaterial* lCurMaterial = this->mMaterials[ lMaterial ];
		if 
			( 0 == strcmp(lCurMaterial->Name().c_str(), lStrName.c_str()) )
		{
			lFound = true;
			this->mCurrentSubsetId = lMaterial;
			break;
		}
	}

	if 
		( lFound == false )
	{
		OBJMaterial* lNewMaterial = new OBJMaterial();
		if 
			( lNewMaterial == NULL )
		{
			return;
		}

		this->mCurrentSubsetId = this->mMaterials.Size();

		lNewMaterial->SetName( STRING( lStrName ) );

		this->mMaterials.Add( lNewMaterial );

		// Prepare the corresponding subset this material will be used for.
		if
			( this->mCurrentSubsetId == this->mSubsets.Size() )
		{
			// Next set starts where the previous one ends.
			this->mSubsets.Add( new Subset( this->mCurrentSubsetId, this->mCurrentSubsetVertexCount, 0, this->mCurrentSubsetFaceCount, 0 ) );
			this->mCurrentSubsetVertexCount = 0;
			this->mCurrentSubsetFaceCount   = 0;
		}
	}
}

VVOID OBJMeshLoader::ReleaseData()
{
	this->mPositions.RemoveAll();
	this->mTexCoords.RemoveAll();
	this->mNormals.RemoveAll();

	// Iterate through all the elements in the cache and subsequent linked lists
	for 
		( BIGINT i = 0; i < this->mVertexCache.Size(); i++ )
	{
		CacheEntry* lEntry = mVertexCache[ i ];
		while 
			( lEntry != NULL )
		{
			CacheEntry* lNext = lEntry->Next;
			DeletePointer( lEntry );
			lEntry = lNext;
		}
	}

	this->mVertexCache.RemoveAll();
}
