
#pragma once

/**
 * Includes
 */
#include <map>
#include "Maths/Vector3.h"
#include "DataStructures/VectorExt.h"
#include "../MeshConstants.h"

/**
 * Namespace
 */
namespace OBJLoader
{
	/**
	 * Forward declaration(s)
	 */
	class Mesh;
	class Face;
	class Object;
	class Material;

	/**
	 * Model class definition to store all objects data
	 */
	class Model
	{
	public:

		/**
		 * Typedef(s)
		 */
		typedef std::map<std::string, Ocelot::VectorExt<unsigned int>* > GroupMap;
		typedef std::map<std::string, Ocelot::VectorExt<unsigned int>* >::iterator GroupMapIt;
		typedef std::map<std::string, Ocelot::VectorExt<unsigned int>* >::const_iterator ConstGroupMapIt;

	private:

		/**
		 * Attribute(s)
		 */
		//!	Model name
		std::string				    mModelName;
		//!	List ob assigned objects
		Ocelot::VectorExt<Object*>  mObjects;
		//!	Pointer to current object
		Object*						mCurrentObject;
		//!	Pointer to current material
		Material*					mCurrentMaterial;
		//!	Pointer to default material
		Material*					mDefaultMaterial;
		//!	Vector with all generated materials
		Ocelot::VectorExt<std::string>  mMaterialLib;
		//!	Vector with all generated group
		Ocelot::VectorExt<std::string>  mGroupLib;
		//!	Vector with all generated vertices
		Ocelot::VectorExt<Vector3>		mVertices;
		//!	vector with all generated normals
		Ocelot::VectorExt<Vector3>		mNormals;
		//!	Group map
		GroupMap						mGroups;
		//!	Group to face id assignment
		Ocelot::VectorExt<unsigned int>* mGroupFaceIDs;
		//!	Active group
		std::string					mActiveGroupName;
		//!	Vector with generated texture coordinates
		Ocelot::VectorExt<Vector3>	mTextureCoords;
		//!	Current mesh instance
		Mesh*						mCurrentMesh;
		//!	Vector with stored meshes
		Ocelot::VectorExt<Mesh*>	mMeshes;
		//!	Material map
		std::map<std::string, Material*> mMaterialMap;

	public:

		/**
		 * Constructor(s) & Destructor
		 */
		Model();
		~Model();

		/**
		 * Method(s)
		 */
		Ocelot::VectorExt<unsigned int>* AddGroup(const std::string& pGroupName);

		void AddToGroup(const std::string& pGroupName, unsigned int pToAdd);

		void RemoveGroup(const std::string& pGroupName);

		void RemoveFromGroup(const std::string& pGroupName, int pIndex);

		void AddObject(const Object* pNewObject);

		void RemoveObject(const Object* pObject);

		void AddMesh(const Mesh* pNewMesh);

		void RemoveMesh(const Mesh* pMesh);

		inline const Ocelot::VectorExt<Mesh*>* Meshes() const
		{
			return &this->mMeshes;
		}

		void AddVertex(const Vector3& pVertex);

		void RemoveVertex(int pIndex);

		inline const Ocelot::VectorExt<Vector3>* Vectices() const
		{
			return &this->mVertices;
		}

		void AddNormal(const Vector3& pNormal);

		void RemoveNormal(int pIndex);

		inline const Ocelot::VectorExt<Vector3>* Normals() const
		{
			return &this->mNormals;
		}

		void AddTexCoord(const Vector3& pTexCoord);

		void RemoveTexCoord(int pIndex);

		inline const Ocelot::VectorExt<Vector3>* TextureCoords() const
		{
			return &this->mTextureCoords;
		}

		void AddMaterial(const Material* pMaterial);

		void AddMaterialToLib(const Material* pMaterial);

		void RemoveMaterial(const std::string& pName);

		inline const Material* GetMaterialByName(const std::string& pName) const
		{
			std::map<std::string, Material*>::const_iterator lIter = this->mMaterialMap.find(pName);
			if 
				( lIter != this->mMaterialMap.end() )
			{
				return lIter->second;
			}

			return nullptr;
		}

		inline const Mesh* CurrentMesh() const
		{
			return this->mCurrentMesh;
		}

		inline void SetCurrentMesh(Mesh* pMesh)
		{
			this->mCurrentMesh = pMesh;
		}

		inline const Object* CurrentObject() const
		{
			return this->mCurrentObject;
		}

		inline void SetCurrentObject(Object* pObject)
		{
			this->mCurrentObject = pObject;
		}

		inline const Material* CurrentMaterial() const
		{
			return this->mCurrentMaterial;
		}

		inline void SetCurrentMaterial(Material* pMaterial)
		{
			this->mCurrentMaterial = pMaterial;
		}

		inline const Material* DefaultMaterial() const
		{
			return this->mDefaultMaterial;
		}

		inline void SetDefaultMaterial(Material* pDefaultMaterial)
		{
			this->mDefaultMaterial = pDefaultMaterial;
		}

		inline const std::string& ActiveGroupName() const
		{
			return this->mActiveGroupName;
		}

		inline void SetActiveGroupName(const std::string& pActiveGroupName)
		{
			this->mActiveGroupName = pActiveGroupName;
		}

		inline const std::string& ModelName() const
		{
			return this->mModelName;
		}

		inline void SetModelName(const std::string& pName)
		{
			this->mModelName = pName;
		}

		inline const Ocelot::VectorExt<Object*>* Objects() const
		{
			return &this->mObjects;
		}

		inline const GroupMap* Groups() const
		{
			return &this->mGroups;
		}

		inline const Ocelot::VectorExt<unsigned int>* GetGroupByName(const std::string& pGroupName)
		{
			GroupMapIt lIter = this->mGroups.find( pGroupName );
			if 
				( lIter != this->mGroups.end() )
			{
				lIter->second;
			}

			return nullptr;
		}

		inline const Ocelot::VectorExt<std::string>* MaterialLib() const
		{
			return &this->mMaterialLib;
		}

		inline const Ocelot::VectorExt<std::string>* GroupLib() const
		{
			return &this->mGroupLib;
		}

		inline const Ocelot::VectorExt<unsigned int>* GroupFaceIds() const
		{
			return this->mGroupFaceIDs;
		}

		inline void SetGroupFaceIDs(Ocelot::VectorExt<unsigned int>* pGroupFaceIDs)
		{
			this->mGroupFaceIDs = pGroupFaceIDs;
		}
	};
}
