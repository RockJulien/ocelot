
#include <string>
#include "Model.h"
#include "Material.h"
#include "OBJUtilities.h"
#include "StringHelper/STRINGHelpers.h"
#include "OBJMaterialLoader.h"

using namespace std;
using namespace Ocelot;
using namespace OBJLoader;

namespace OBJLoader
{
	// Material specific token
	static const string DiffuseTexture = "map_kd";
	static const string AmbientTexture = "map_ka";
	static const string SpecularTexture = "map_ks";
	static const string OpacityTexture = "map_d";
	static const string BumpTexture1 = "map_bump";
	static const string BumpTexture2 = "map_Bump";
	static const string BumpTexture3 = "bump";
	static const string NormalTexture = "map_Kn";
	static const string DisplacementTexture = "disp";
	static const string SpecularityTexture = "map_ns";

	// texture option specific token
	static const string BlendUOption = "-blendu";
	static const string BlendVOption = "-blendv";
	static const string BoostOption = "-boost";
	static const string ModifyMapOption = "-mm";
	static const string OffsetOption = "-o";
	static const string ScaleOption = "-s";
	static const string TurbulenceOption = "-t";
	static const string ResolutionOption = "-texres";
	static const string ClampOption = "-clamp";
	static const string BumpOption = "-bm";
	static const string ChannelOption = "-imfchan";
	static const string TypeOption = "-type";
}

OBJMaterialLoader::OBJMaterialLoader(DataArray& pBuffer, const string &pAbsPath, Model* pModel) :
mDataIt(pBuffer.begin()),
mDataItEnd(pBuffer.end()),
mModel(pModel),
mCurrentLine(0)
{
	assert( this->mModel != nullptr );

	const Material* lDefaultMaterial = this->mModel->DefaultMaterial();
	if 
		( lDefaultMaterial == nullptr )
	{
		Material* lDefaultMaterial = new Material();
		lDefaultMaterial->SetMaterialName( "default" );
		this->mModel->SetDefaultMaterial( lDefaultMaterial );
	}

	this->Load();
}

OBJMaterialLoader::~OBJMaterialLoader()
{

}

void OBJMaterialLoader::Load()
{
	if 
		( this->mDataIt == this->mDataItEnd )
	{
		return;
	}

	Material* lCurrentMaterial = const_cast<Material*>( this->mModel->CurrentMaterial() );
	while 
		( this->mDataIt != this->mDataItEnd )
	{
		switch 
			( *this->mDataIt )
		{
		case 'K':
		{
			++this->mDataIt;
			if 
				( *this->mDataIt == 'a' ) // Ambient color
			{
				++this->mDataIt;

				Vector3 lAmbient;
				this->GetColorRGBA( &lAmbient );
				lCurrentMaterial->SetAmbientRGB( lAmbient );
			}
			else if 
				( *this->mDataIt == 'd' )	// Diffuse color
			{
				++this->mDataIt;

				Vector3 lDiffuse;
				this->GetColorRGBA( &lDiffuse );
				lCurrentMaterial->SetDiffuseRGB( lDiffuse );
			}
			else if 
				( *this->mDataIt == 's' )
			{
				++this->mDataIt;

				Vector3 lSpecular;
				this->GetColorRGBA( &lSpecular );
				lCurrentMaterial->SetSpecularRGB( lSpecular );
			}
			else if 
				( *this->mDataIt == 'e' )
			{
				++this->mDataIt;

				Vector3 lEmissive;
				this->GetColorRGBA( &lEmissive );
				lCurrentMaterial->SetEmissiveRGB( lEmissive );
			}

			this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
		}
		break;
		case 'd':	// Alpha value
		{
			++this->mDataIt;

			float lAlpha;
			this->GetFloatValue( lAlpha );
			lCurrentMaterial->SetAlpha( lAlpha );
			this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
		}
		break;
		case 'N':	// Shineness
		{
			++this->mDataIt;
			switch (*this->mDataIt)
			{
			case 's':
				{
					++this->mDataIt;

					float lShininess;
					this->GetFloatValue( lShininess );
					lCurrentMaterial->SetShininess( lShininess );
				}
				break;
			case 'i': //Index Of refraction
				{
					++this->mDataIt;

					float lRefractionIndex;
					this->GetFloatValue( lRefractionIndex );
					lCurrentMaterial->SetRefractionIndex( lRefractionIndex );
				}
				break;
			}
			this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
			break;
		}
		break;
		case 'm':	// Texture
		case 'b':   // quick'n'dirty - for 'bump' sections
		{
			this->GetTexture();
			this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
		}
		break;
		case 'n':	// New material name
		{
			this->CreateMaterial();
			this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
		}
		break;
		case 'i':	// Illumination model
		{
			this->mDataIt = OBJUtilities::getNextToken<DataArrayIt>(this->mDataIt, this->mDataItEnd);
			BIGINT lIlluminationModel;
			this->GetIlluminationModel( lIlluminationModel );
			lCurrentMaterial->SetIlluminationModel( lIlluminationModel );
			this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
		}
		break;
		default:
		{
			this->mDataIt = OBJUtilities::skipLine<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mCurrentLine );
		}
		break;
		}
	}
}

void OBJMaterialLoader::GetColorRGBA(Vector3* pColor)
{
	assert( pColor != nullptr );

	float lRed;
	float lGreen;
	float lBlue;
	this->mDataIt = OBJUtilities::getFloat<DataArrayIt>( this->mDataIt, this->mDataItEnd, lRed );
	pColor->x( lRed );

	this->mDataIt = OBJUtilities::getFloat<DataArrayIt>( this->mDataIt, this->mDataItEnd, lGreen );
	pColor->y( lGreen );

	this->mDataIt = OBJUtilities::getFloat<DataArrayIt>( this->mDataIt, this->mDataItEnd, lBlue );
	pColor->z( lBlue );
}

void OBJMaterialLoader::GetIlluminationModel(BIGINT& pIllumModel)
{
	this->mDataIt = OBJUtilities::CopyNextWord<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mBuffer, cBUFFERSIZE );
	pIllumModel = atoi( this->mBuffer );
}

void OBJMaterialLoader::GetFloatValue(float& pValue)
{
	this->mDataIt = OBJUtilities::CopyNextWord<DataArrayIt>( this->mDataIt, this->mDataItEnd, this->mBuffer, cBUFFERSIZE );
	pValue = (float)atof( this->mBuffer );
}

void OBJMaterialLoader::CreateMaterial()
{
	string lLine("");
	while 
		( OBJUtilities::isNewLine( *this->mDataIt ) == false )
	{
		lLine += *this->mDataIt;
		++this->mDataIt;
	}

	vector<std::string> lTokens;
	const unsigned int lTokenCount = OBJUtilities::tokenize<string>( lLine, lTokens, " " );
	string lName("");
	if 
		( lTokenCount == 1 )
	{
		lName = "Default";
	}
	else 
	{
		lName = lTokens[ 1 ];
	}

	const Material* lMaterial = this->mModel->GetMaterialByName( lName );
	if 
		( lMaterial == nullptr ) 
	{
		// New Material created
		Material* lNewMaterial = new Material();
		lNewMaterial->SetMaterialName( lName );
		this->mModel->SetCurrentMaterial( lNewMaterial );
		this->mModel->AddMaterialToLib( lNewMaterial );
		this->mModel->AddMaterial( lNewMaterial );
	}
	else 
	{
		// Use older material
		this->mModel->SetCurrentMaterial( const_cast<Material*>(lMaterial) );
	}
}

void OBJMaterialLoader::GetTexture()
{
	string* lOut(nullptr);
	TextureTypes::Type lTextureType;

	Material* lCurrentMaterial = const_cast<Material*>( this->mModel->CurrentMaterial() );

	const CCHAR* lPtr(&(*this->mDataIt));
	if (!_strincmp(lPtr, DiffuseTexture.c_str(), DiffuseTexture.size())) 
	{
		// Diffuse texture
		lOut = const_cast<string*>( &lCurrentMaterial->Texture() );
		lTextureType = TextureTypes::eDiffuse;
	}
	else if (!_strincmp(lPtr, AmbientTexture.c_str(), AmbientTexture.size())) 
	{
		// Ambient texture
		lOut = const_cast<string*>( &lCurrentMaterial->Ambient() );
		lTextureType = TextureTypes::eAmbient;
	}
	else if (!_strincmp(lPtr, SpecularTexture.c_str(), SpecularTexture.size())) 
	{
		// Specular texture
		lOut = const_cast<string*>( &lCurrentMaterial->Specular() );
		lTextureType = TextureTypes::eSpecular;
	}
	else if (!_strincmp(lPtr, OpacityTexture.c_str(), OpacityTexture.size())) 
	{
		// Opacity texture
		lOut = const_cast<string*>( &lCurrentMaterial->Opacity() );
		lTextureType = TextureTypes::eOpacity;
	}
	else if (!_strincmp(lPtr, "map_ka", 6)) 
	{
		// Ambient texture
		lOut = const_cast<string*>( &lCurrentMaterial->Ambient() );
		lTextureType = TextureTypes::eAmbient;
	}
	else if (!_strincmp(&(*this->mDataIt), "map_emissive", 6)) 
	{
		// Emissive texture
		lOut = const_cast<string*>( &lCurrentMaterial->Emissive() );
		lTextureType = TextureTypes::eEmissive;
	}
	else if (!_strincmp(lPtr, BumpTexture1.c_str(), BumpTexture1.size()) ||
		!_strincmp(lPtr, BumpTexture2.c_str(), BumpTexture2.size()) ||
		!_strincmp(lPtr, BumpTexture3.c_str(), BumpTexture3.size())) 
	{
		// Bump texture 
		lOut = const_cast<string*>( &lCurrentMaterial->Bump() );
		lTextureType = TextureTypes::eBump;
	}
	else if (!_strincmp(lPtr, NormalTexture.c_str(), NormalTexture.size())) 
	{
		// Normal map
		lOut = const_cast<string*>( &lCurrentMaterial->Normal() );
		lTextureType = TextureTypes::eNormal;
	}
	else if (!_strincmp(lPtr, DisplacementTexture.c_str(), DisplacementTexture.size())) 
	{
		// Displacement texture
		lOut = const_cast<string*>( &lCurrentMaterial->Disp() );
		lTextureType = TextureTypes::eDisp;
	}
	else if (!_strincmp(lPtr, SpecularityTexture.c_str(), SpecularityTexture.size())) 
	{
		// Specularity scaling (glossiness)
		lOut = const_cast<string*>( &lCurrentMaterial->Specularity() );
		lTextureType = TextureTypes::eSpecularity;
	}
	else 
	{
		throw exception("OBJ/MTL: Encountered unknown texture type");
		return;
	}

	bool lHasType = false;
	this->GetTextureOption( lHasType );
	if 
		( lHasType )
	{
		lCurrentMaterial->AddType( lTextureType );
	}
	else
	{
		lCurrentMaterial->RemoveType( lTextureType );
	}

	string lTextureName;
	this->mDataIt = OBJUtilities::getName<DataArrayIt>( this->mDataIt, this->mDataItEnd, lTextureName );
	*lOut = lTextureName;
}

void OBJMaterialLoader::GetTextureOption(bool& pHasType)
{
	this->mDataIt = OBJUtilities::getNextToken<DataArrayIt>(this->mDataIt, this->mDataItEnd);

	//If there is any more texture option
	while (!OBJUtilities::isEndOfBuffer(this->mDataIt, this->mDataItEnd) && *this->mDataIt == '-')
	{
		const CCHAR* lPtr(&(*this->mDataIt));
		//skip option key and value
		int lSkipToken = 1;

		if (!_strincmp(lPtr, ClampOption.c_str(), ClampOption.size()))
		{
			DataArrayIt it = OBJUtilities::getNextToken<DataArrayIt>(this->mDataIt, this->mDataItEnd);
			char value[3];
			OBJUtilities::CopyNextWord(it, this->mDataItEnd, value, sizeof(value) / sizeof(*value));
			if (!_strincmp(value, "on", 2))
			{
				pHasType = true;
			}

			lSkipToken = 2;
		}
		else if (!_strincmp(lPtr, BlendUOption.c_str(), BlendUOption.size())
			|| !_strincmp(lPtr, BlendVOption.c_str(), BlendVOption.size())
			|| !_strincmp(lPtr, BoostOption.c_str(), BoostOption.size())
			|| !_strincmp(lPtr, ResolutionOption.c_str(), ResolutionOption.size())
			|| !_strincmp(lPtr, BumpOption.c_str(), BumpOption.size())
			|| !_strincmp(lPtr, ChannelOption.c_str(), ChannelOption.size())
			|| !_strincmp(lPtr, TypeOption.c_str(), TypeOption.size()))
		{
			lSkipToken = 2;
		}
		else if (!_strincmp(lPtr, ModifyMapOption.c_str(), ModifyMapOption.size()))
		{
			lSkipToken = 3;
		}
		else if (!_strincmp(lPtr, OffsetOption.c_str(), OffsetOption.size())
			|| !_strincmp(lPtr, ScaleOption.c_str(), ScaleOption.size())
			|| !_strincmp(lPtr, TurbulenceOption.c_str(), TurbulenceOption.size())
			)
		{
			lSkipToken = 4;
		}

		for 
			( int i = 0; i < lSkipToken; ++i )
		{
			this->mDataIt = OBJUtilities::getNextToken<DataArrayIt>(this->mDataIt, this->mDataItEnd);
		}
	}
}
