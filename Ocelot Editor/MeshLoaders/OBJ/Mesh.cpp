
#include "Face.h"
#include "Mesh.h"

using namespace Ocelot;
using namespace OBJLoader;

Mesh::Mesh() :
mMaterial(nullptr),
mIndiceCount(0),
mMaterialIndex(NoMaterial),
mHasNormals(false)
{
	memset(mUVCoordinates, 0, sizeof(unsigned int) * MAX_TEXTURECOORDS);
}

Mesh::~Mesh()
{
	for
		( VectorIter<Face*> lIter = this->mFaces.Iterator(); 
	                        lIter.NotEnd(); 
							lIter.MoveNext() )
	{
		delete lIter.Current();
	}
	this->mFaces.RemoveAll();
}

void Mesh::AddFace(const Face* pNewFace)
{
	this->mFaces.Add(const_cast<Face*>(pNewFace));
}

void Mesh::RemoveFace(const Face* pFace)
{
	BIGINT lIndex = this->mFaces.IndexOf(const_cast<Face*>(pFace));
	this->mFaces.Remove(lIndex);
}