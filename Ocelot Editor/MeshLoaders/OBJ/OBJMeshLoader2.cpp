#include "OBJMeshLoader2.h"
#include "ResourceManager\ResourceFactory.h"
#include "StringHelper\STRINGHelpers.h"
#include "OBJUtilities.h"
#include "OBJParser.h"

// Namespaces used
using namespace std;
using namespace Ocelot;
using namespace OBJLoader;

static const unsigned int sObjMinSize = 16;

OBJMeshLoader2::OBJMeshLoader2()
{

}

OBJMeshLoader2::~OBJMeshLoader2()
{

}

BBOOL		 OBJMeshLoader2::CanLoad(const STRING& pFileName)
{
	VectorExt<STRING> lMatches;
	lMatches.Add("obj");
	lMatches.Add("Obj");
	lMatches.Add("OBJ");
	return CheckExtension( pFileName, lMatches );
}

IOcelotMesh* OBJMeshLoader2::LoadFromFile(const DevPtr pDevice, const CamPtr pCamera, const STRING& pFileName)
{
	// Read file into memory
	const string lMode = "rb";
	ifstream lStream(pFileName);
	if 
		(!lStream)
	{
		throw exception("OBJMeshLoader2::LoadFromFile Failed to open file.");
	}

	// Get the file-size and validate it, throwing an exception when fails
	// get length of file:
	lStream.seekg(0, lStream.end);
	BIGINT lFileSize = (BIGINT)lStream.tellg();
	lStream.seekg(0, lStream.beg);
	if 
		(lFileSize < sObjMinSize)
	{
		throw exception("OBJMeshLoader2::LoadFromFile The file is too small.");
	}

	// Allocate buffer and read file into it
	OBJParser::TextFileToBuffer( &lStream, this->mBuffer );

	// Get the model name
	string  lModelName;
	string::size_type lPos = pFileName.find_last_of("\\/");
	if 
		(lPos != string::npos)
	{
		lModelName = pFileName.substr(lPos + 1, pFileName.size() - lPos - 1);
	}
	else
	{
		lModelName = pFileName;
	}

	// process all '\'
	vector<char> ::iterator iter = this->mBuffer.begin();
	while 
		(iter != this->mBuffer.end())
	{
		if 
			(*iter == '\\')
		{
			// remove '\'
			iter = this->mBuffer.erase(iter);
			// remove next character
			while (*iter == '\r' || *iter == '\n')
			{
				iter = this->mBuffer.erase(iter);
			}
		}
		else
		{
			++iter;
		}
	}

	OBJParser lParser( lModelName );

	bool lResult = lParser.Parse( this->mBuffer );

	IOcelotMesh* lMesh = this->CreateDataFromImport( lParser.GetModel() );

	this->mBuffer.clear();

	return lMesh;
}

IOcelotMesh* OBJMeshLoader2::CreateDataFromImport(const Model* pModel)
{
	if 
		( pModel == nullptr )
	{
		return nullptr;
	}

	//// Create the mesh and return it.
	//MeshInfo lInfo(pDevice,
	//	pCamera,
	//	NULL,
	//	NULL,
	//	L"..\\Effects\\OBJMesh.fx",
	//	Vector3(0.0f));
	//lInfo.SetVertices(this->mVertices);
	//lInfo.SetIndices(this->mIndices);
	//lInfo.SetSubsetTable(this->mSubsets);
	//lInfo.SetMaterials(this->mMaterials);
	//return new MeshGeometry(lInfo);

	return nullptr;
}