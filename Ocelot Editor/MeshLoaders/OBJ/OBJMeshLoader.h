#ifndef DEF_OBJMESHLOADER_H
#define DEF_OBJMESHLOADER_H

// Includes
#include "../IMeshLoader.h"
#include "Geometry\MeshGeometry.h"
#include "DataStructures\VectorExt.h"
#include "Geometry\OcelotDXMaterial.h"

// Namespaces 
namespace Ocelot
{
	// Class definition
	class OBJMeshLoader : public IMeshLoader
	{
	private:

		// Used for a hashtable vertex cache when creating the mesh from a .obj file
		struct CacheEntry
		{
			UBIGINT     Index;
			CacheEntry* Next;
		};

		// Attributes

		// Temp structures
		VectorExt<CacheEntry*>    mVertexCache;
		VectorExt<Vector3>        mPositions;
		VectorExt<Vector3>        mNormals;
		VectorExt<Vector2>		  mTexCoords;

		// Final structures
		MeshInfo::MeshVertices    mVertices;
		MeshInfo::MeshIndices     mIndices;
		MeshInfo::MeshMaterials   mMaterials;
		MeshInfo::MeshSubsetTable mSubsets;

		BIGINT					  mCurrentSubsetId;
		UBIGINT					  mCurrentSubsetVertexCount;
		UBIGINT					  mCurrentSubsetFaceCount;

		// Private Methods
		VVOID LoadTextureCoord(ifstream& pStream);
		VVOID LoadPosition(ifstream& pStream);
		VVOID LoadNormal(ifstream& pStream);
		VVOID LoadTriangle(ifstream& pStream);
		VVOID LoadMaterials(const STRING& pFileName);
		UBIGINT AddVertex(UBIGINT pHash, const EnhancedVertex* pVertex);
		VVOID   PrepareMaterial(ifstream& pStream);
		VVOID   ReleaseData();

	public:

		// Constructors & Destructor
		OBJMeshLoader();
		~OBJMeshLoader();

		// Methods
		BBOOL		 CanLoad(const STRING& pFileName);
		IOcelotMesh* LoadFromFile(const DevPtr pDevice, const CamPtr pCamera, const STRING& pFileName);

	};
}

#endif