
#pragma once

/**
 * Namespace
 */
namespace OBJLoader
{
	namespace TextureTypes
	{
		/**
		 * Texture type enumeration definition
		 */
		enum Type
		{
			// Diffuse map
			eDiffuse = 0,

			// Specular map
			eSpecular,

			// Ambient map
			eAmbient,

			// Emissive map
			eEmissive,

			// Bump map
			eBump,

			// Normal map
			eNormal,

			// Specularity map
			eSpecularity,

			// Opacity map
			eOpacity,

			// Displasment map
			eDisp,

			// Count
			eCount
		};
	}
}
