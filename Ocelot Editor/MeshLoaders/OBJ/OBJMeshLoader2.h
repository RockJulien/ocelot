
#pragma once

// Includes
#include "../AMeshLoader.h"
#include "Geometry/PrimitiveTypes.h"
#include "DataStructures\VectorExt.h"

// Namespaces 
namespace OBJLoader
{
	/**
	 * Forward declaration(s)
	 */
	class Model;
	class Object;

	// Class definition
	class OBJMeshLoader2 : public Loader::AMeshLoader
	{
	private:

		//!	Data buffer
		std::vector<char>	mBuffer;
		//!	Pointer to root object instance
		Object*				mRootObject;
		//!	Absolute pathname of model in file system
		string				mAbsPath;

		//!	\brief	Create the data from imported content.
		Ocelot::IOcelotMesh* CreateDataFromImport(const Model* pModel);

	public:

		// Constructors & Destructor
		OBJMeshLoader2();
		~OBJMeshLoader2();

		// Methods
		BBOOL				 CanLoad(const STRING& pFileName);
		Ocelot::IOcelotMesh* LoadFromFile(const Ocelot::DevPtr pDevice, 
										  const Ocelot::CamPtr pCamera, 
										  const STRING& pFileName);

	};
}