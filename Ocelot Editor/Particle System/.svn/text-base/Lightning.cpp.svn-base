#include "Lightning.h"

using namespace Ocelot;

//Constructor and initial variable settings
Lightning::Lightning()
	: IDXBaseMesh(), startPoint(0.0f), endPoint(0.0f), numIterations(1)
{
	m_iNbVertices = (int)powf(2.0f, (float)numIterations) + 1;
	arcRange = 6.0f;
	vertices.resize(m_iNbVertices);
	m_fScale = 2.0f;
	m_sEffectFileName  = L".\\Effects\\LightningEffect.fx";
	m_sTextureFileName = L"";
	color = OcelotColor::CYAN;

	srand((unsigned int)time(0));
}

//Constructor and initial variable settings
Lightning::Lightning(LightningInfo& info)
	: IDXBaseMesh(info.data), startPoint(info.startpoint), endPoint(info.endpoint), numIterations(info.numiterations), arcRange(info.range)
{
	m_iNbVertices = (int)powf(2.0f, (float)numIterations) + 1;
	vertices.resize(m_iNbVertices);
	m_fScale = 2.0f;
	m_sEffectFileName  = L".\\Effects\\LightningEffect.fx";
	m_sTextureFileName = L"";
	tex = info.tex;
	color = info.color;
	duration = info.duration;

	srand((unsigned int)time(0));
}

//Destructor
Lightning::~Lightning()
{
	releaseMesh();
}

//Regenerates a new lightning bolt if enough time has passedd
void Lightning::update(float deltaTime)
{
	static float t_base = 0.0f;

	if(totalTime - t_base >= duration) //If true, we generate a new lightning bolt
	{
		GenerateNewBolt(t_base, deltaTime);
	}
}

//Render the lightning bolt
INT Lightning::renderMesh()
{
	HRESULT hr = S_OK;

	HR(transfertEffectVariables());

	m_device->IASetInputLayout(m_inputLayout);

	UINT stride = sizeof(::BasicVertex);
	UINT offset = 0;
	m_device->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	m_device->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	m_device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINELIST);

	HR(IDXBaseMesh::renderMesh());

	return (INT)hr;
}

//Crate layout description for use in shader
INT Lightning::initializeLayout()
{
	HRESULT hr = S_OK;

	// create the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] = 
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,12, D3D10_INPUT_PER_VERTEX_DATA, 0},
	};
	//Only position and colour are needed, normals aren't needed since the bolts just get
	//billboarded and no expensive lighting calculations are used for them (just a texture sample)
	UINT nbElements = sizeof(layout)/sizeof(layout[0]);

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(m_device->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

	return (INT)hr;
}

//Initialize shader
INT Lightning::createEffect()
{
	return IDXBaseMesh::createEffect();
}

//Initialise variables to be used in shader
void Lightning::initializeEffectVariables()
{
	m_fxTechnique = m_effect->GetTechniqueByName("RenderLine");
	m_fxWorldViewProj = m_effect->GetVariableByName("gWVP")->AsMatrix();
	pEyePosVar = m_effect->GetVariableByName("gEyePosW")->AsVector();
	texVar = m_effect->GetVariableByName("gTexture")->AsShaderResource();
}

//Update shader variables with new values
INT Lightning::transfertEffectVariables()
{
	HRESULT hr = S_OK;

	Matrix4x4 matV, matP, worldViewProj;
	matV = m_camera->getViewMat();
	matP = m_camera->getProjMat();

	m_matWorld.Matrix4x4Mutliply(worldViewProj, matV, matP);

	m_fxWorldViewProj->SetMatrix((float*)worldViewProj);
	pEyePosVar->SetFloatVector((float*)&eyePositionW);
	texVar->SetResource(tex);

	return (INT)hr;
}

//Create the vertex information and fill vertex buffer
INT Lightning::initializeVertices()
{
	HRESULT hr = S_OK;

	//Start vertex
	vertices[0].Color = color;
	vertices[0].Position = startPoint;
	
	//Middle vertices
	GenerateMiddleVertices();

	//End vertex
	vertices[m_iNbVertices - 1].Color = color;
	vertices[m_iNbVertices - 1].Position = endPoint;

	CreateVertexBuffer();

	return (INT)hr;
}

//Create indices and fill index buffer
INT Lightning::initializeIndices()
{
	HRESULT hr = S_OK;

	m_iNbIndices  = (UINT)powf(2.0f, (float)numIterations + 1);

	std::vector<DWORD> indices;
	indices.resize(m_iNbIndices);

	for (UINT i = 0; i < m_iNbIndices; ++i)
	{
		indices[i] = (DWORD)ceil(0.5f * (float)i);
	}
	
	CreateIndexBuffer(indices);

	return (INT)hr;
}

void Lightning::GenerateNewBolt(float& t_base, const float deltaTime) 
{
	ReleaseCOM(m_vertexBuffer); //Delete current vertices
	initializeVertices(); //Re-calculate new vertices and set in buffer
	SetEyePosition(m_camera->getEyePos()); //Get most current camera position
	IDXBaseMesh::update(deltaTime); //Parents update function
	t_base += duration; //Update t_base
}

void Lightning::GenerateMiddleVertices() 
{
	float mid = 1.0f / powf(2.0f, (float)numIterations);
	int numMidVertices = (int)pow(2.0f, numIterations) - 1;
	Vector3 toEnd = endPoint - startPoint;

	float range = arcRange;
	for (int i = 1; i <= numMidVertices; ++i)
	{
		vertices[i].Color = color;
		vertices[i].Position = startPoint + ((i * mid) * toEnd);
		float offset = (float)(rand() % 10 - 5);
		offset /= 10.0f;
		offset *= range;
		vertices[i].Position.y(vertices[i].Position.getY() + offset);
		range /= 1.25f;
	}
}

void Lightning::CreateVertexBuffer() 
{
	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage = D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth = sizeof(BasicVertex) * m_iNbVertices;
	buffDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = &vertices[0];

	HR(m_device->CreateBuffer(&buffDesc, &InitData, &m_vertexBuffer));
}

void Lightning::CreateIndexBuffer(std::vector<DWORD>& indices) 
{
	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage = D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth = sizeof(DWORD) * m_iNbIndices;
	buffDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = &indices[0];

	HR(m_device->CreateBuffer(&buffDesc, &InitData, &m_indiceBuffer));
}