#include "ParticleEffectsFactory.h"

using namespace Ocelot;

//Get single static instance of class
ParticleEffectsFactory* ParticleEffectsFactory::GetInstance()
{
	static ParticleEffectsFactory instance;
	return &instance;
}

//Factory function
ParticleSystem* ParticleEffectsFactory::CreateParticleEffect(ParticleEffectType type, ParticleSystemInfo& info)
{
	switch(type)
	{
	case PE_SMOKE:
		info.filename = L"..\\Effects\\SmokeEffect.fx";
		break;
	case PE_FIRE:
		info.filename = L"..\\Effects\\FireEffect.fx";
		break;
	case PE_SNOW:
		info.filename = L"..\\Effects\\SnowEffect.fx";
		break;
	case PE_RAIN:
		info.filename = L"..\\Effects\\RainEffect.fx";
		break;
	default:
		return nullptr;
	}
	return new ParticleSystem(info);
}