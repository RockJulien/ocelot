#ifndef IPARTICLESYSTEM_H
#define IPARTICLESYSTEM_H

#include "../Camera/OcelotCamera.h"
#include "../ResourceManager/ResourceFactory.h"

/*
	The interface that all particle systems follow.
*/

namespace Ocelot
{
	class IParticleSystem
	{
	public:
		//Initialises some variables, generates a random texture for the shader, 
		//builds effect file, initializes shader variables, builds buffers and builds inputlayout.
		virtual VVOID Init(ID3D10Device* device, ID3D10ShaderResourceView* texArrayRV, OcelotCamera* cam, std::wstring fxfilename, const BIGINT maxNumParticles) = 0;
		
		//Resets particle system to initial state
		virtual VVOID ResetParticleSystem() = 0;
		
		//Updates camera position, generates current wind force, updates gameTime, timeStep and variables
		virtual VVOID Update(const FFLOAT delta, const FFLOAT gameTime) = 0;
		
		//Draws the particle system
		virtual VVOID Render() = 0;

		//Create link to shader file
		virtual ID3D10Effect* CreateFX(ID3D10Device* device, ::std::wstring filename) = 0;

		virtual BIGINT GetMeshIndex() const = 0; //The mesh the particle system surrounds (can be camera, player, object, anything that has an index)
		virtual VVOID SetEmitterPosition(const Vector3& emitPos) = 0; //The position where particles are emitted.
		virtual VVOID SetMeshIndex(const BIGINT index) = 0;
		virtual VVOID SetEmitterOffset(const Vector3& offset) = 0; //The offset from the emitter position
		virtual VVOID SetAcceleration(const Vector3& acc) = 0; //The rate of change of velocity for the particles
		virtual VVOID SetWindAmplitude(const Vector3& amp) = 0; //How much the wind affects the motion of the particles
		virtual VVOID SetWindPeriodicity(const Vector3& omega) = 0; //How often the wind affects the motion of the particles
		virtual Vector3 GetEmitterOffset() const = 0;
		virtual Vector3 GetEmitterPosition() const = 0;
	};
}

#endif