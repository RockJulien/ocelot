#ifndef PARTICLEEFFECTSFACTORY_H
#define PARTICLEEFFECTSFACTORY_H

#include "IParticleEffectFactory.h"

/*
	Factory pattern class to ease and manage the creation of the
	different types of particle effects.
	You pass in the type of particle system you want and the factory
	handles the rest.
*/

namespace Ocelot
{
	#define ParticleFXFactory ParticleEffectsFactory::GetInstance()
	class ParticleEffectsFactory : public IParticleEffectFactory
	{
	public:
		//Only one particle effects factory is required so it is made singleton.
		static ParticleEffectsFactory* GetInstance();

		//The factory pattern function
		virtual ParticleSystem* CreateParticleEffect(ParticleEffectType type, ParticleSystemInfo& info);

	private:
		//Made private to limit creation of instances of this class
		ParticleEffectsFactory() {}
		ParticleEffectsFactory(const ParticleEffectsFactory&);
		ParticleEffectsFactory& operator= (const ParticleEffectsFactory&);
	};
}

#endif