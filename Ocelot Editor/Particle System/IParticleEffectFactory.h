#ifndef IPARTICLEFFECTFACTORY_H
#define IPARTICLEFFECTFACTORY_H

#include "ParticleSystem.h"

/*
	The interfaces that particle effect factories use to manage creation of particle effect systems
*/

namespace Ocelot
{
	//Enumeration denoting the different types supported
	enum ParticleEffectType
	{
		PE_FIRE,
		PE_SNOW,
		PE_RAIN,
		PE_SMOKE
	};

	class IParticleEffectFactory
	{
	public:
		//Return a particle system pointer depending on the type specified
		IParticleSystem* CreateParticleEffect(ParticleEffectType type, ParticleSystemInfo& info);
	};
}

#endif