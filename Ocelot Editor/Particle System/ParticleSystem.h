#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

/*
	Creates a particle system that runs and is managed on the GPU using shaders.
	Different effect files and different settings set in the ParticleSystemInfo
	struct determine what type of particle system is used (fire, snow, etc.)
	The particle system is one of the types of object that can be read from XML.

	The particle system makes use of a "random texture" technique. This is because
	the particles are managed on the GPU but also require randomness, something which
	the shader cannot supply. So instead a texure is generated CPU and filled in with
	random values, the GPU then samples this texture using the total game time and an
	offset to index in.
*/

#include "IParticleSystem.h"

namespace Ocelot
{
	//An information struct, used so we don't have to pass in a large number of arguments
	struct ParticleSystemInfo
	{
		FFLOAT spread;
		BIGINT maxnumparticles;
		
		ID3D10Device* device;
		OcelotCamera* camera;
		ID3D10ShaderResourceView* texture;

		std::wstring filename;

		Vector3 acceleration;
		Vector3 windforce;
		Vector3 eyeposition;
		Vector3 emitterposition;
		Vector3 emitdirection;
	};

	class ParticleSystem : public IParticleSystem
	{
	public:
		//Constructors
		ParticleSystem();
		ParticleSystem(ParticleSystemInfo& info);
		//Destructor
		~ParticleSystem();

		//Initialises some variables, generates a random texture for the shader, 
		//builds effect file, initializes shader variables, builds buffers and builds inputlayout.
		virtual VVOID Init(ID3D10Device* device, ID3D10ShaderResourceView* texArrayRV, OcelotCamera* cam, std::wstring fxfilename, const BIGINT maxNumParticles);
		
		//Resets particle system
		virtual VVOID ResetParticleSystem();

		//Updates camera position, generates current wind force, updates gameTime, timeStep and variables
		virtual VVOID Update(const FFLOAT delta, const FFLOAT gameTime);
		
		//Draws the particle system
		virtual VVOID Render();

		//Create link to shader file
		virtual ID3D10Effect* CreateFX(ID3D10Device* device, ::std::wstring filename);

		//Return the maximum number of alive/spawned particles this system can have (i.e. how large the particle pool is)
		inline BIGINT     GetMaxParticles() const { return maxParticles; }

		//Return how far away from emitter position the particles are allow to go
		inline FFLOAT   GetSpread() const { return spread; }

		//Return the ages of the particle system
		inline FFLOAT   GetAge() const { return age; }

		//Various setters/getters for variables/state
		inline Vector4 GetEyePosition() const { return eyePositionWorld; }
		inline Vector4 GetEmitDirection() const { return emitDirectionWorld; }
		inline Vector4 GetWindForce() const { return windForceWorld; }
		inline Vector4 GetAcceleration() const { return acceleration; }
		inline Vector3 GetWindAmplitude() const { return windAmplitude; }
		inline Vector3 GetWindPeriodicity() const { return windPeriodicity; }

		inline VVOID SetDevice(ID3D10Device* device) { pDevice = device; }
		inline VVOID SetMaxParticles(const BIGINT maxNumParticles) { maxParticles = maxNumParticles; }
		inline VVOID SetEyePosition(const Vector3& eyePos) { eyePositionWorld = Vector4(eyePos.getX(), eyePos.getY(), eyePos.getZ(), 1.0f); }
		inline VVOID SetEmitDirection(const Vector3& emitDir) { emitDirectionWorld = Vector4(emitDir.getX(), emitDir.getY(), emitDir.getZ(), 0.0f); }
		inline VVOID SetWindForce(const Vector3& wForce) { windForceWorld = Vector4(wForce.getX(), wForce.getY(), wForce.getZ(), 0.0f); }
		inline VVOID SetSpread(const FFLOAT spr) { spread = spr; }

		//Below are the functions implemented from the IParticleSystem interface. They are the minimal
		//functions require to start up a particle system (along with the virtual methods up above too of course!)

		//Returns which mesh (if any) the emitter is attached to. -1 denotes the camera position.
		virtual inline BIGINT  GetMeshIndex() const { return meshIndex; }

		//Set the position of the emitter
		virtual inline VVOID SetEmitterPosition(const Vector3& emitPos) { emitterPositionWorld = Vector4(emitPos.getX(), emitPos.getY(), emitPos.getZ(), 1.0f); }
		
		//Set how much velocity changes per second for the particles
		virtual inline VVOID SetAcceleration(const Vector3& acc) { acceleration = Vector4(acc.getX(), acc.getY(), acc.getZ(), 0.0f); }
		
		//Return the offset from the emitter position
		virtual inline Vector3 GetEmitterOffset() const { return emitterOffset; }
		
		//Set the strength of the wind
		virtual inline VVOID SetWindAmplitude(const Vector3& amp) { windAmplitude = amp; }
		
		//Set how often wind direction changes
		virtual inline VVOID SetWindPeriodicity(const Vector3& omega) { windPeriodicity = omega; }
		
		//Set which mesh the emitter is attached to (or rather which meshes *position*, so as to not couple an emitter with a mesh)
		virtual inline VVOID SetMeshIndex(const BIGINT index) { meshIndex = index; }
		
		//Set an offset from the emitter position, useful if you want to start an effect not in the middle of a mesh.
		virtual inline VVOID SetEmitterOffset(const Vector3& offset) { emitterOffset = offset; }

		virtual inline Vector3 GetEmitterPosition() const { return Vector3(emitterPositionWorld.getX(), emitterPositionWorld.getY(), emitterPositionWorld.getZ()); }

	private:
		//The max number of particles alive at any one moment
		BIGINT maxParticles;
		
		FFLOAT age; //The age of the particle system
		FFLOAT gameTime; //How much time as passed in the simulation (i.e. the age of the application running the particle system, not the age of the particle system itself, which can be reset)
		FFLOAT timeStep; //The delta time between frames
		FFLOAT spread; //How spread out from the emitter position the particles can be

		Vector3 windAmplitude; //Strength of the wind
		Vector3 windPeriodicity; //How often the wind direction changes

		BIGINT meshIndex; //Which mesh the particle system is attached to

		Vector4 eyePositionWorld;
		Vector4 emitterPositionWorld; 
		Vector4 emitDirectionWorld;
		Vector4 windForceWorld;
		Vector4 acceleration;
		Vector3 emitterOffset;
		Matrix4x4 matRot;

		BBOOL isFirstRun;

		//Direct X stuff, this is why I created the IParticleSystem interface, so its not relying solely 
		//on the DirectX things. The graphics library specific things should be done solely in the concrete
		//implementations like here, so different implementations of the IParticleSystem running different
		//graphics library code can be swapped in/out at runtime without having to change the underlying code.
		OcelotCamera*						pCamera;

		ID3D10Device*						pDevice;
		ID3D10Buffer*						pInitializationBuffer;
		ID3D10Buffer*						pStreamOutBuffer;
		ID3D10Buffer*						pRenderBuffer;
		ID3D10ShaderResourceView*			pTextureArrayRV;
		ID3D10ShaderResourceView*			pRandomRV;
		ID3D10EffectTechnique*				pStreamOutTech;
		ID3D10EffectTechnique*				pRenderTech;
		ID3D10EffectMatrixVariable*			pViewProjVar;
		ID3D10EffectMatrixVariable*         pMatRotVar;
		ID3D10EffectScalarVariable*			pGameTimeVar;
		ID3D10EffectScalarVariable*			pTimeStepVar;
		ID3D10EffectScalarVariable*			pSpreadVar;
		ID3D10EffectVectorVariable*			pEyePosVar;
		ID3D10EffectVectorVariable*			pEmitPosVar;
		ID3D10EffectVectorVariable*			pEmitDirVar;
		ID3D10EffectVectorVariable*			pWindForceVar;
		ID3D10EffectVectorVariable*		    pAccelerationVariable;
		ID3D10EffectShaderResourceVariable* pTextureArrayVar;
		ID3D10EffectShaderResourceVariable* pRandomVar;
		ID3D10InputLayout*					pInputLayout;
		ID3D10Effect*					    pParticleFX; 

		VVOID BuildVertexBuffers(); //Generate vertex data and fill buffer
		VVOID BuildInputLayout(); //Create input layout the shader uses
		VVOID BuildParticleFX(std::wstring& filename); //Create link to shader file
		VVOID SetEffectVariables(Matrix4x4& view, Matrix4x4& proj); //Update and set shader-side variables
		VVOID InitToNull(); //Initialise all pointers to null
		VVOID InitEffectVariables(); //Initialise links to shader variables

		
		ParticleSystem(const ParticleSystem& rhs);
		ParticleSystem& operator=(const ParticleSystem& rhs);
	};
}

#endif