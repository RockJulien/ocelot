#ifndef ILIGHTNING_H
#define ILIGHTNING_H

#include "..\Maths\Vector3.h"
#include "..\DataStructures\DataStructureUtil.h"

/*
	The interface for classes that produce lightning bolt effects.
	The bolt itself is abstracted simply as having a start point,
	an end point, some number of subdivisions between these points,
	the colour of the bolt and the duration of time between each
	bolt gen.
*/

namespace Ocelot
{
	class ILightning
	{
	public:
		//The bolts are billboarded and hence need an eye position, most often of which is the camera position
		//It should be in world coordinates
		virtual VVOID SetEyePosition(const Vector3& eyePos) = 0;

		//Getters for the lightning bolt's variables
		virtual Vector3 GetStartPoint() const = 0; //Where the lightning bolt begins
		virtual Vector3 GetEndPoint() const = 0;   //Where the lightning bolt ends
		virtual BIGINT GetNumIterations() const = 0;  //The number of times the bolt is subdivided
		virtual Color GetColor() const = 0; //The colour of the bolt
		virtual FFLOAT GetDuration() const = 0; //The duration between one bolt and a new bolt

		//Setters for the lightning bolts variables, descriptions of which are in comments above
		virtual VVOID SetStartPoint(const Vector3& start) = 0;
		virtual VVOID SetEndPoint(const Vector3& end) = 0;
		virtual VVOID SetNumIterations(const BIGINT iterations) = 0;
		virtual VVOID SetColor(const Color& col) = 0;
		virtual VVOID SetTime(const FFLOAT time) = 0;
		virtual VVOID SetDuration(const FFLOAT secs) = 0;
	};
}

#endif