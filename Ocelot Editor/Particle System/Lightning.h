#ifndef LIGHTNING_H
#define LIGHTNING_H

/*
	Creates a lightning bolt effect that on each update loop is
	regenerated. The duration between each new bolt generation can be
	set and changed at runtime, the color of the bolt, how many times it
	is subdivded and the difference in height offsets for each
	subdivision can all be changed.
*/

#include "..\Geometry\OcelotDXMesh.h"
#include <ctime>

namespace Ocelot
{
								//Lightning information struct used as a parameter in the constructor instead of a large number of params.
								class LightningInfo : public BaseDXData
								{
								private:

									WSTRING     mEffectPath;
									ResViewPtr	mTexture;
									Vector3		mStartpoint;
									Vector3		mEndpoint;
									Color		mColor;
									BIGINT		mNumiterations;
									FFLOAT		mRange;
									FFLOAT		mDuration;

								public:

									// Constructor & Destructor
									LightningInfo();
									LightningInfo(DevPtr pDevice, CamPtr pCamera, WSTRING pEffectPath, ResViewPtr pTexture, Vector3	pStartpoint, Vector3 pEndpoint, Color pColor, BIGINT pNumiterations, FFLOAT	pRange, FFLOAT pDuration);
									~LightningInfo();

									// Accessors
									inline ResViewPtr Texture() const { return mTexture; };
									inline Vector3	  Startpoint() const { return mStartpoint; }
									inline Vector3	  Endpoint() const { return mEndpoint; }
									inline Color	  GetColor() const { return mColor; }
									inline BIGINT	  Numiterations() const { return mNumiterations; }
									inline FFLOAT	  Range() const { return mRange; }
									inline FFLOAT	  Duration() const { return mDuration; }
									inline WSTRING    EffectPath() const { return mEffectPath; }
									inline VVOID      SetTexture(ResViewPtr pTexture) { mTexture = pTexture; };
									inline VVOID	  SetStartpoint(const Vector3& pStartPoint) { mStartpoint = pStartPoint; }
									inline VVOID	  SetEndpoint(const Vector3& pEndpoint) { mEndpoint = pEndpoint; }
									inline VVOID	  SetColor(const Color& pColor) { mColor = pColor; }
									inline VVOID	  SetNumiterations(BIGINT pIterationCount) { mNumiterations = pIterationCount; }
									inline VVOID	  SetRange(FFLOAT pRange) { mRange = pRange; }
									inline VVOID	  SetDuration(FFLOAT pDuration) { mDuration = pDuration; }
									inline VVOID      SetEffectPath(WSTRING pEffPath) { mEffectPath = pEffPath; }
								};

	class Lightning : public OcelotDXMesh
	{
	public:

		//Constructors & Destructor
		Lightning();
		Lightning(const LightningInfo& info);
		~Lightning();

		// Methods per frame.
		VVOID   update(FFLOAT deltaTime); //Regenerates a new lightning bolt if enough time has passed
		BIGINT  updateEffectVariables(); //Update shader variables with new values
		BIGINT  renderMesh(); //Render the lightning bolt

		// Methods
		BIGINT  initializeLayout();
		BIGINT  initializeEffect();
		BIGINT  initializeVertices();
		BIGINT  initializeIndices();
		VVOID   initializeEffectVariables();

		// Accessors
		inline LightningInfo& Info() { return mInfo; }
		inline VVOID          SetTime(const FFLOAT time) { mTotalTime = time; }

	private:

		// Attributes
		LightningInfo mInfo;

		MatPtr		  m_fxWorldViewProj;

		//Its corresponding shader variable
		VecPtr		  mEyePosVar;

		//Its corresponding shader variable
		TexPtr		  mTexVar;
		
		//How much time has passed in the lightning bolt simulation
		FFLOAT		  mTotalTime;

		//The vertices that create the bolt effect
		std::vector<BasicVertex> mVertices;

		// private methods
		VVOID RefreshWorldBounds();
		VVOID GenerateNewBolt(FFLOAT& t_base, const FFLOAT delta);
		VVOID GenerateMiddleVertices();
		VVOID CreateVertexBuffer();
		VVOID CreateIndexBuffer(std::vector<UBIGINT>& pIndices);
	};
}

#endif