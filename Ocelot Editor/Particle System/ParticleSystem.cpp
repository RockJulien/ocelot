#include "ParticleSystem.h"
#include "..\Geometry\VertexTypes.h"

using namespace Ocelot;

//Constructors
ParticleSystem::ParticleSystem()
{
	InitToNull();

	isFirstRun = true;
	gameTime = 0.0f;
	timeStep = 0.0f;
	age = 0.0f;
	spread = 1.0f;

	eyePositionWorld = Vector4(0.0f, 0.0f, 0.0f, 1.0f);
	emitterPositionWorld = Vector4(0.0f, 0.0f, 0.0f, 1.0f);
	emitDirectionWorld = Vector4(0.0f, 1.0f, 0.0f, 0.0f);
	windForceWorld = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
	acceleration = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
}

ParticleSystem::ParticleSystem(ParticleSystemInfo &info)
{
	InitToNull();

	isFirstRun = true;
	gameTime = 0.0f;
	timeStep = 0.0f;
	age = 0.0f;
	spread = info.spread;

	SetEyePosition(info.eyeposition);
	SetEmitterPosition(info.emitterposition);
	SetEmitDirection(info.emitdirection);
	SetWindForce(info.windforce);
	SetAcceleration(info.acceleration);
	Init(info.device, info.texture, info.camera, info.filename, info.maxnumparticles);
}

ParticleSystem::~ParticleSystem()
{
	ReleaseCOM(pInitializationBuffer)
	ReleaseCOM(pRenderBuffer)
	ReleaseCOM(pStreamOutBuffer)
	ReleaseCOM(pInputLayout);
	ReleaseCOM(pParticleFX);
}

//Initialises some variables, generates a random texture for the shader, builds effect file, initializes shader variables, builds buffers and builds inputlayout
VVOID ParticleSystem::Init(ID3D10Device* device, ID3D10ShaderResourceView* texArrayRV, OcelotCamera* cam, std::wstring filename, const int maxNumParticles)
{
	pDevice = device;
	maxParticles = maxNumParticles;
	pCamera = cam;

	pTextureArrayRV = texArrayRV;
	pRandomRV = ResFactory->GenerateRandomTex();

	BuildParticleFX(filename);

	InitEffectVariables();

	BuildVertexBuffers();

	BuildInputLayout();
}

//Resets particle system
VVOID ParticleSystem::ResetParticleSystem()
{
	isFirstRun = true;
	age = 0.0f;
}

//Updates camera position, generates current wind force, updates gameTime, timeStep and variables
VVOID ParticleSystem::Update(const float delta, const float _gameTime)
{
	SetEyePosition(pCamera->Info().Eye());
	SetWindForce(Vector3(windAmplitude.getX() * cosf(windPeriodicity.getX() * _gameTime), windAmplitude.getY() * windPeriodicity.getY(), windAmplitude.getZ() * windPeriodicity.getZ()));
	gameTime = _gameTime;
	timeStep = delta;

	static FFLOAT rotation = 0.0f;

	// Update the rotation variable each frame.
	rotation += 0.0174532925f * 4.0f;
	if(rotation > 360.0f)
	{
		rotation -= 360.0f;
	}

	// Update the rotation.
	matRot.Matrix4x4Identity();
	matRot.Matrix4x4RotationY(matRot, rotation);
	matRot.Matrix4x4RotationZ(matRot, rotation);

	age += delta;
}

//Initialize the 3 buffers required. They are bound at different stages
//of the render pipeline and are initially empty.
VVOID ParticleSystem::BuildVertexBuffers()
{
	D3D10_BUFFER_DESC bufferDescription;
	bufferDescription.CPUAccessFlags = 0;
	bufferDescription.MiscFlags = 0;
	bufferDescription.Usage = D3D10_USAGE_DEFAULT;
	bufferDescription.ByteWidth = sizeof(LightParticleVertex) * 1;
	bufferDescription.BindFlags = D3D10_BIND_VERTEX_BUFFER;

	LightParticleVertex initialParticle;
	ZeroMemory(&initialParticle, sizeof(LightParticleVertex));
	initialParticle.Age = 0.0f;
	initialParticle.Type = 0;

	D3D10_SUBRESOURCE_DATA initialisedData;
	initialisedData.pSysMem = &initialParticle;

	HRESULT hr = pDevice->CreateBuffer(&bufferDescription, &initialisedData, &pInitializationBuffer);
	if(FAILED(hr))
	{
		MessageBox(0, L"Create init buffer failed", 0, 0);
	}

	bufferDescription.ByteWidth = sizeof(LightParticleVertex) * maxParticles;
	bufferDescription.BindFlags = D3D10_BIND_VERTEX_BUFFER | D3D10_BIND_STREAM_OUTPUT;

	hr = pDevice->CreateBuffer(&bufferDescription, 0, &pRenderBuffer);
	if(FAILED(hr))
	{
		MessageBox(0, L"Create render buffer failed", 0, 0);
	}
	hr = pDevice->CreateBuffer(&bufferDescription, 0, &pStreamOutBuffer);
	if(FAILED(hr))
	{
		MessageBox(0, L"Create stream out buffer failed", 0, 0);
	}
}

//Draws the particle system
VVOID ParticleSystem::Render()
{
	Matrix4x4 view = pCamera->View();
	Matrix4x4 proj = pCamera->Proj();
	
	SetEffectVariables(view, proj);

	pDevice->IASetInputLayout(pInputLayout);
	pDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);

	UINT strideSize = sizeof(LightParticleVertex);
	UINT offsetSize = 0;

	//if is first run, run shader code that creates and streams out the vertices required to draw particles
	//else, the particles have already been streamed, so render them
	if(isFirstRun)
	{
		pDevice->IASetVertexBuffers(0, 1, &pInitializationBuffer, &strideSize, &offsetSize);
	}
	else
	{
		pDevice->IASetVertexBuffers(0, 1, &pRenderBuffer, &strideSize, &offsetSize);
	}

	pDevice->SOSetTargets(1, &pStreamOutBuffer, &offsetSize);

	D3D10_TECHNIQUE_DESC techniqueDescription;
	pStreamOutTech->GetDesc(&techniqueDescription);
	for(UINT p = 0; p < techniqueDescription.Passes; ++p)
	{
		pStreamOutTech->GetPassByIndex(p)->Apply(0);

		if(isFirstRun)
		{
			pDevice->Draw(1, 0);
			isFirstRun = false;
		}
		else
		{
			pDevice->DrawAuto();
		}
	}

	ID3D10Buffer* bArray[1] = {0};
	pDevice->SOSetTargets(1, bArray, &offsetSize);

	std::swap(pRenderBuffer, pStreamOutBuffer);

	pDevice->IASetVertexBuffers(0, 1, &pRenderBuffer, &strideSize, &offsetSize);

	pRenderTech->GetDesc(&techniqueDescription);
	for(UINT p = 0; p < techniqueDescription.Passes; ++p)
	{
		pRenderTech->GetPassByIndex(p)->Apply(0);
		pDevice->DrawAuto();
	}
}

//Build the input layout which the shader file will use as input to its vertex shader.
VVOID ParticleSystem::BuildInputLayout()
{
	D3D10_PASS_DESC passDescription;

	D3D10_INPUT_ELEMENT_DESC particleLayoutDescription[] =
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"VELOCITY", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"SIZE",     0, DXGI_FORMAT_R32G32_FLOAT,    0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"AGE",      0, DXGI_FORMAT_R32_FLOAT,       0, 32, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"TYPE",     0, DXGI_FORMAT_R32_UINT,        0, 36, D3D10_INPUT_PER_VERTEX_DATA, 0},
	};
	
	pParticleFX->GetTechniqueByName("StreamOutTech")->GetPassByIndex(0)->GetDesc(&passDescription);
	HRESULT hr = pDevice->CreateInputLayout(particleLayoutDescription, 5, passDescription.pIAInputSignature, passDescription.IAInputSignatureSize, &pInputLayout);
	if(FAILED(hr))
	{
		MessageBox(0, L"CreateInputLayout for particle layout failed", 0, 0);
	}
}

//Create link to shader file
ID3D10Effect* ParticleSystem::CreateFX(ID3D10Device* device, ::std::wstring filename)
{
	DWORD shaderFlags = D3D10_SHADER_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif

	ID3D10Blob* compilationErrors = 0;
	HRESULT hr = 0;
	ID3D10Effect* fx = 0;
	hr = D3DX10CreateEffectFromFile(filename.c_str(), 0, 0, "fx_4_0", shaderFlags, 0, device, 0, 0, &fx, &compilationErrors, 0);
	if(FAILED(hr))
	{
		if(compilationErrors)
		{
			MessageBoxA(0, (CCHAR*)compilationErrors->GetBufferPointer(), 0, 0);
			if(compilationErrors)
			{
				compilationErrors->Release();
				compilationErrors = nullptr;
			}
		}
	}

	return fx;
}

//Create link to shader file
VVOID ParticleSystem::BuildParticleFX(std::wstring& filename)
{
	pParticleFX = CreateFX(pDevice, filename);
}

//Updates the variables (that get sent to the shader) with the most up-to-date values
VVOID ParticleSystem::SetEffectVariables(Matrix4x4& view, Matrix4x4& proj) 
{
	pViewProjVar->SetMatrix((float*)&(view * proj));
	pMatRotVar->SetMatrix((float*)&matRot);
	pGameTimeVar->SetFloat(gameTime);
	pTimeStepVar->SetFloat(timeStep);
	pEyePosVar->SetFloatVector((float*)&eyePositionWorld);
	pEmitPosVar->SetFloatVector((float*)&emitterPositionWorld);
	pEmitDirVar->SetFloatVector((float*)&emitDirectionWorld);
	pWindForceVar->SetFloatVector((float*)&windForceWorld);
	pAccelerationVariable->SetFloatVector((float*)&acceleration);
	pSpreadVar->SetFloat(spread);
	pTextureArrayVar->SetResource(pTextureArrayRV);
	pRandomVar->SetResource(pRandomRV);
}

//Initialize all pointers to null. Just a helpful class so the two constructors aren't 
//full of duplication and also to make them neater
VVOID ParticleSystem::InitToNull() 
{
	pDevice				  = nullptr;
	pInitializationBuffer = nullptr;
	pStreamOutBuffer	  = nullptr;
	pRenderBuffer		  = nullptr;
	pTextureArrayRV		  = nullptr;
	pRandomRV			  = nullptr;
	pStreamOutTech		  = nullptr;
	pRenderTech			  = nullptr;
	pViewProjVar		  = nullptr;
	pMatRotVar            = nullptr;
	pGameTimeVar		  = nullptr;
	pTimeStepVar		  = nullptr;
	pEyePosVar			  = nullptr;
	pEmitPosVar			  = nullptr;
	pEmitDirVar			  = nullptr;
	pTextureArrayVar      = nullptr;
	pRandomVar		      = nullptr;
	pCamera				  = nullptr;
	pInputLayout		  = nullptr;
	pParticleFX			  = nullptr;
	pWindForceVar		  = nullptr;
	pAccelerationVariable = nullptr;
	pSpreadVar			  = nullptr;
}

//Link CPU-side variables with GPU-side variables
VVOID ParticleSystem::InitEffectVariables() 
{
	pStreamOutTech = pParticleFX->GetTechniqueByName("StreamOutTech");
	pRenderTech = pParticleFX->GetTechniqueByName("ParticleRenderTech");	
	pViewProjVar = pParticleFX->GetVariableByName("gViewProj")->AsMatrix();
	pMatRotVar   = pParticleFX->GetVariableByName("gMatRot")->AsMatrix();
	pGameTimeVar = pParticleFX->GetVariableByName("gGameTime")->AsScalar();
	pTimeStepVar = pParticleFX->GetVariableByName("gTimeStep")->AsScalar();
	pEyePosVar = pParticleFX->GetVariableByName("gEyePosW")->AsVector();
	pEmitPosVar = pParticleFX->GetVariableByName("gEmitPosW")->AsVector();
	pEmitDirVar = pParticleFX->GetVariableByName("gEmitDirW")->AsVector();
	pWindForceVar = pParticleFX->GetVariableByName("gWindForce")->AsVector();
	pSpreadVar = pParticleFX->GetVariableByName("gSpread")->AsScalar();
	pAccelerationVariable = pParticleFX->GetVariableByName("gAccelerationW")->AsVector();
	pTextureArrayVar = pParticleFX->GetVariableByName("gTex")->AsShaderResource();
	pRandomVar = pParticleFX->GetVariableByName("gRandomTex")->AsShaderResource();
}