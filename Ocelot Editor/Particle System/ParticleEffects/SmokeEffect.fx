//Smoke effect

//As close to an enum you'll get for shaders at the moment! (at least shader model 4 anyway)
#define EMITTER 0
#define FLARE 1

cbuffer perFrame 
{
	float4 gEyePosW;  //Eye position in world coordinates
    float4 gEmitPosW; //Emitter position in world coordinates
    float4 gEmitDirW; //Emit direction in world coordinates
    
	float gGameTime; //Total game time, used to sample "random" texture
    float gTimeStep; //Delta time, used to integrate particles
    float gSpread; //How far each particle can stray away from the emitter

    float4x4 gViewProj;

    float4 gWindForce; //Refers to how much the wind affects the motion of the particle
	float4 gAccelerationW; //rate of change of the particles velocity
}

cbuffer cbFixed
{  
	//Used so we can texture the quad in a loop
	float2 gQuadTexC[4] = 
	{
        float2(0.0f, 1.0f),
		float2(1.0f, 1.0f),
		float2(0.0f, 0.0f),
		float2(1.0f, 0.0f)
	};
};

Texture2D gTex; //Texture to apply to the quads
Texture1D gRandomTex; //Texture full of randomized values, need a "random" function but HLSL doesnt have one so this is the alternative

//Sampler states
SamplerState gTriLinearSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

//Depth states
DepthStencilState DisableDepth
{
    DepthEnable = FALSE;
    DepthWriteMask = ZERO;
};

DepthStencilState NoDepthWrites
{
    DepthEnable = TRUE;
    DepthWriteMask = ZERO;
};

//Blend state, combination gives a nice build up of smoke
BlendState SubtractiveBlending
{
    AlphaToCoverageEnable = FALSE;
    BlendEnable[0] = TRUE;
    SrcBlend = SRC_ALPHA;
    DestBlend = INV_SRC_ALPHA;
    BlendOp = ADD;
    SrcBlendAlpha = ZERO;
    DestBlendAlpha = ZERO;
    BlendOpAlpha = ADD;
    RenderTargetWriteMask[0] = 0x0F;
};

//Returns a random normalised vector. Total game time + offset used to sample the random tex
float3 RandomNormVec(float translation)
{
	float totalOffset = (gGameTime + translation);
	float3 randVec = gRandomTex.SampleLevel(gTriLinearSampler, totalOffset, 0);
	return normalize(randVec);
}

struct Particle
{
	float3 initialPosW : POSITION;
	float3 initialVelW : VELOCITY;
	float2 sizeW : SIZE;
	float age : AGE;
	uint type : TYPE;
};

Particle StreamOutVertexShader(Particle vIn)
{
	return vIn;
}

//Geometry shader function that creates new particles from the emitter
//Note that this is not the shader itself. In order to use the stream output
//features, you have an extra line to specify, see below!
[maxvertexcount(2)]
void StreamOut(point Particle gIn[1], inout PointStream<Particle> stream)
{
	gIn[0].age += gTimeStep;
	
	if(gIn[0].type == EMITTER) //Only create new particles from emitters
	{	
		if(gIn[0].age > 0.005f)
		{
			float3 vRandom = RandomNormVec(0.0f);
			vRandom.y = abs(vRandom.y);
			vRandom.x *= 0.85f;
			vRandom.z *= 0.85f;

			//Create new particle and fill in info
			Particle p;

			p.sizeW = float2(1.0f, 1.0f);
			p.age = 0.0f;
			p.type = FLARE;

			p.initialPosW = gEmitPosW.xyz;
			p.initialPosW.x + vRandom.x;
			p.initialPosW.z + vRandom.z;

			p.initialVelW = 0.5f * vRandom;
			
			stream.Append(p);
			
			gIn[0].age = 0.0f;
		}
		stream.Append(gIn[0]);
	}
	else
	{
		if(gIn[0].age <= 4.0f ) //Kill condition
			stream.Append(gIn[0]);
	}
}

//In order to use stream out, you run with function, specifying the function above as well as the data being streamed out
GeometryShader StreamOutGeometryShader = ConstructGSWithSO(CompileShader(gs_4_0, StreamOut()), "POSITION.xyz; VELOCITY.xyz; SIZE.xy; AGE.x; TYPE.x");

technique10 StreamOutTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, StreamOutVertexShader()));
		SetGeometryShader(StreamOutGeometryShader);
		SetPixelShader(NULL); //No pixel shader as nothing is drawn during the particle creation stage

		SetDepthStencilState(DisableDepth, 0);
	}
}

//The vertex structure of particles to be rendered
struct VS_OUT
{
	float3 posW : POSITION;
	float2 sizeW : SIZE;
	float4 color : COLOR;
	uint type : TYPE;
	float age : AGE;
};

VS_OUT RenderVertexShader(Particle vIn)
{
	VS_OUT vOut;
	
	//Numerically integrate
	float time = vIn.age;
	vOut.posW = 0.5f*time*time*gWindForce.xyz + time*vIn.initialVelW + vIn.initialPosW;
	
	float opacity = 1.0f - smoothstep(0.0f, 1.0f, time/4.0f); //Goes from solid to transparent over time
	vOut.color = float4(1.0f, 1.0f, 1.0f, opacity); //Various opacities of white, the blend state makes it look more gray as it builds up :)
	
	vOut.sizeW = vIn.sizeW;
	vOut.type = vIn.type;
	vOut.age = vIn.age;
	
	return vOut;
}

struct GS_OUT
{
	float4 posH : SV_Position;
	float4 color : COLOR;
	float2 texC : TEXCOORD;
};

//Geometry shader that expands the particle points into quads and then textures them
[maxvertexcount(4)]
void RenderGeometryShader(point VS_OUT gIn[1], inout TriangleStream<GS_OUT> stream)
{
	if(gIn[0].type != EMITTER)
	{		
		//Construct quad
		float halfWidth = 0.5f * gIn[0].sizeW.x * gIn[0].age;
		float halfHeight = 0.5f * gIn[0].sizeW.y * gIn[0].age;
		float4 v[4];
		v[0] = float4(-halfWidth, -halfHeight, 0.0f, 1.0f);
		v[1] = float4(+halfWidth, -halfHeight, 0.0f, 1.0f);
		v[2] = float4(-halfWidth, +halfHeight, 0.0f, 1.0f);
		v[3] = float4(+halfWidth, +halfHeight, 0.0f, 1.0f);

		//Create coordinate frame for billboarding
		float3 look = normalize(gEyePosW.xyz - gIn[0].posW);
		float3 right = normalize(cross(float3(0,1,0), look));
		float3 up = cross(look, right);
		
		float4x4 world;
		world[0] = float4(right, 0.0f);
		world[1] = float4(up, 0.0f);
		world[2] = float4(look, 0.0f);
		world[3] = float4(gIn[0].posW, 1.0f);

		float4x4 WorldViewProj = mul(world, gViewProj);
		
		GS_OUT gOut;
		[unroll]
		for(int i = 0; i < 4; ++i)
		{
			gOut.posH = mul(v[i], WorldViewProj); //Place into device coordinates
			gOut.texC = gQuadTexC[i];
			gOut.color = gIn[0].color;
			stream.Append(gOut);
		}	
	}
}

float4 RenderPixelShader(GS_OUT pIn) : SV_TARGET
{
	//just simply sample the tex and modulate with colour
	return gTex.Sample(gTriLinearSampler, float3(pIn.texC, 0.0f)) * pIn.color;
}

technique10 ParticleRenderTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, RenderVertexShader()));
		SetGeometryShader(CompileShader(gs_4_0, RenderGeometryShader()));
		SetPixelShader(CompileShader(ps_4_0, RenderPixelShader()));

		SetBlendState(SubtractiveBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xffffffff);
		SetDepthStencilState(NoDepthWrites, 0);
	}
}