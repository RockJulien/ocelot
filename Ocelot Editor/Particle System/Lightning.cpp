#include "Lightning.h"
#include "..\DirectXAPI\DXHelperFunctions.h"

using namespace Ocelot;
using namespace std;

//Constructor and initial variable settings
Lightning::Lightning() : 
OcelotDXMesh(), mInfo()
{
	m_iNbVertices = (BIGINT)powf(2.0f, (FFLOAT)mInfo.Numiterations()) + 1;
	this->mVertices.resize(m_iNbVertices);

	srand((UBIGINT)time(0));
}

//Constructor and initial variable settings
Lightning::Lightning(const LightningInfo& info) : 
OcelotDXMesh(), mInfo(info)
{
	m_iNbVertices = (BIGINT)powf(2.0f, (FFLOAT)mInfo.Numiterations()) + 1;
	this->mVertices.resize(m_iNbVertices);

	srand((UBIGINT)time(0));
}

//Destructor
Lightning::~Lightning()
{
	releaseInstance();
}

//Regenerates a new lightning bolt if enough time has passedd
VVOID Lightning::update(FFLOAT deltaTime)
{
	static FFLOAT t_base = 0.0f;

	if
		( this->mTotalTime - t_base >= this->mInfo.Duration() ) //If true, we generate a new lightning bolt
	{
		this->GenerateNewBolt(t_base, deltaTime);
	}

	// refresh variables in the shader file
	HR(this->updateEffectVariables());

	return OcelotDXMesh::update(deltaTime);
}

//Render the lightning bolt
BIGINT Lightning::renderMesh()
{
	HRESULT hr = S_OK;

	mInfo.Device()->IASetInputLayout(m_inputLayout);

	UINT stride = sizeof(::BasicVertex);
	UINT offset = 0;
	mInfo.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	mInfo.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	mInfo.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINELIST);

	if (m_bVisible)
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for (UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			mInfo.Device()->DrawIndexed(m_iNbIndices, 0, 0);
		}
	}

	return OcelotDXMesh::renderMesh();
}

//Crate layout description for use in shader
BIGINT Lightning::initializeLayout()
{
	HRESULT hr = S_OK;

	// create the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] = 
	{
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0},
		{"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,12, D3D10_INPUT_PER_VERTEX_DATA, 0},
	};
	//Only position and colour are needed, normals aren't needed since the bolts just get
	//billboarded and no expensive lighting calculations are used for them (just a texture sample)
	UINT nbElements = sizeof(layout)/sizeof(layout[0]);

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(mInfo.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

	return (INT)hr;
}

//Initialize shader
BIGINT Lightning::initializeEffect()
{
	return initializeDXEffect(mInfo.Device(), mInfo.EffectPath(), &m_effect);
}

//Initialise variables to be used in shader
VVOID Lightning::initializeEffectVariables()
{
	this->m_fxTechnique     = this->m_effect->GetTechniqueByName("RenderLine");
	this->m_fxWorldViewProj = this->m_effect->GetVariableByName("gWVP")->AsMatrix();
	this->mEyePosVar		= this->m_effect->GetVariableByName("gEyePosW")->AsVector();
	this->mTexVar			= this->m_effect->GetVariableByName("gTexture")->AsShaderResource();
}

//Update shader variables with new values
BIGINT Lightning::updateEffectVariables()
{
	HRESULT hr = S_OK;

	Matrix4x4 lMatV, lMatP, lWorldViewProj;
	lMatV = mInfo.GetCamera()->View();
	lMatP = mInfo.GetCamera()->Proj();
	Vector3 lEye = this->mInfo.GetCamera()->Info().Eye();
	Vector4 lHEye(lEye.getX(), lEye.getY(), lEye.getZ(), 1.0f);

	this->m_matWorld.Matrix4x4Mutliply(lWorldViewProj, lMatV, lMatP);

	this->m_fxWorldViewProj->SetMatrix((FFLOAT*)lWorldViewProj);
	this->mEyePosVar->SetFloatVector((FFLOAT*)&lHEye);
	this->mTexVar->SetResource(this->mInfo.Texture());

	return (INT)hr;
}

//Create the vertex information and fill vertex buffer
BIGINT Lightning::initializeVertices()
{
	HRESULT hr = S_OK;

	//Start vertex
	this->mVertices[0].Color = this->mInfo.GetColor();
	this->mVertices[0].Position = this->mInfo.Startpoint();
	
	//Middle this->mVertices
	this->GenerateMiddleVertices();

	//End vertex
	this->mVertices[m_iNbVertices - 1].Color = this->mInfo.GetColor();
	this->mVertices[m_iNbVertices - 1].Position = this->mInfo.Endpoint();

	this->CreateVertexBuffer();

	return (INT)hr;
}

//Create indices and fill index buffer
BIGINT Lightning::initializeIndices()
{
	HRESULT hr = S_OK;

	this->m_iNbIndices = (UINT)powf(2.0f, (FFLOAT)mInfo.Numiterations() + 1);

	vector<UBIGINT> lIndices;
	lIndices.resize(this->m_iNbIndices);

	for 
		( UBIGINT i = 0; i < this->m_iNbIndices; i++ )
	{
		lIndices[i] = (UBIGINT)ceil(0.5f * (FFLOAT)i);
	}
	
	this->CreateIndexBuffer( lIndices );

	return (INT)hr;
}

VVOID Lightning::GenerateNewBolt(FFLOAT& t_base, const FFLOAT deltaTime) 
{
	ReleaseCOM(m_vertexBuffer); //Delete current this->mVertices
	this->initializeVertices(); //Re-calculate new this->mVertices and set in buffer

	OcelotDXMesh::update(deltaTime); //Parents update function

	t_base += mInfo.Duration(); //Update t_base
}

VVOID Lightning::GenerateMiddleVertices() 
{
	FFLOAT lMid = 1.0f / powf(2.0f, (FFLOAT)mInfo.Numiterations());
	BIGINT lNumMidVertices = (BIGINT)pow(2.0f, mInfo.Numiterations()) - 1;
	Vector3 lToEnd = mInfo.Endpoint() - mInfo.Startpoint();

	FFLOAT lRange = mInfo.Range();
	for 
		( BIGINT i = 1; i <= lNumMidVertices; i++ )
	{
		this->mVertices[i].Color = mInfo.GetColor();
		Vector3 lNewPosition = mInfo.Startpoint() + ((i * lMid) * lToEnd);
		FFLOAT lOffset = (FFLOAT)(rand() % 10 - 5);
		lOffset /= 10.0f;
		lOffset *= lRange;
		lNewPosition.y( lNewPosition.getY() + lOffset );
		this->mVertices[i].Position = lNewPosition;
		lRange /= 1.25f;
	}
}

VVOID Lightning::CreateVertexBuffer() 
{
	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage = D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth = sizeof(BasicVertex) * m_iNbVertices;
	buffDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = &this->mVertices[0];

	HR(mInfo.Device()->CreateBuffer(&buffDesc, &InitData, &m_vertexBuffer));
}

VVOID Lightning::CreateIndexBuffer(std::vector<UBIGINT>& pIndices)
{
	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage = D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth = sizeof(UBIGINT) * m_iNbIndices;
	buffDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = &pIndices[0];

	HR(mInfo.Device()->CreateBuffer(&buffDesc, &InitData, &m_indiceBuffer));
}

VVOID Lightning::RefreshWorldBounds()
{
	this->mWorldBounds = NULL;
}

/************************************************************************************************************************************************************************************************/
LightningInfo::LightningInfo() :
BaseDXData(), mEffectPath(L"..\\Effects\\LightningEffect.fx"), mTexture(NULL), mStartpoint(0.0f), 
mEndpoint(10.0f), mColor(OcelotColor::CYAN), mNumiterations(3), mRange(10.0f), mDuration(1.0f)
{

}

LightningInfo::LightningInfo(DevPtr pDevice, CamPtr pCamera, WSTRING pEffectPath, ResViewPtr pTexture, Vector3	pStartpoint, Vector3 pEndpoint, Color pColor, BIGINT pNumiterations, FFLOAT	pRange, FFLOAT pDuration) :
BaseDXData(pDevice, pCamera), mEffectPath(pEffectPath), mTexture(pTexture), mStartpoint(pStartpoint), 
mEndpoint(pEndpoint), mColor(pColor), mNumiterations(pNumiterations), mRange(pRange), mDuration(pDuration)
{

}

LightningInfo::~LightningInfo()
{

}
