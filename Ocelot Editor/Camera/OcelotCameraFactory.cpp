#include "OcelotCameraFactory.h"

using namespace Ocelot;

OcelotCameraFactory::~OcelotCameraFactory()
{

}

OcelotCameraFactory* OcelotCameraFactory::instance()
{
	static OcelotCameraFactory instance;

	return &instance;
}

IOcelotCameraController* OcelotCameraFactory::createController(OcelotCamera* camera, OcelotControllerType type, FFLOAT speed)
{
	// release the previous camera for
	// avoiding memory leaks
	DeletePointer(m_currController);

	// assign another pointer toward a new type of
	// camera. (Strategy Factory Pattern).
	switch(type)
	{
	case CAMERA_CONTROLLER_ORBITER:
				m_iType = type;
				m_currController = new OcelotOrbiterController(camera);
				break;
	case CAMERA_CONTROLLER_FLYER:
				m_iType = type;
				m_currController = new OcelotFlyerController(camera, speed);
				break;
	default:
		m_currController = NULL;
	}

	// if no match
	return m_currController;
}
