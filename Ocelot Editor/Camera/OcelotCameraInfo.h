#ifndef DEF_OCELOTCAMERAINFO_H
#define DEF_OCELOTCAMERAINFO_H

#include "..\Maths\Vector3.h"

namespace Ocelot
{
	class OcelotCameraInfo
	{
	private:

		// Attributes
		// View's attributes.
		Vector3              m_vEyePos;
		Vector3              m_vLAtPos;
		Vector3              m_vUpVect;

		// Projection's attributes.
		FFLOAT               m_fFovy;
		FFLOAT               m_fAspect;
		FFLOAT               m_fNearPlane;
		FFLOAT               m_fFarPlane;

	public:

		// Constructor & Destructor
		OcelotCameraInfo();
		OcelotCameraInfo(Vector3 eyePos, Vector3 lookAt, FFLOAT aspect, FFLOAT fFar, Vector3 up = Vector3(0.0f, 1.0f, 0.0f), FFLOAT fov = PI * 0.25f, FFLOAT fNear = 0.1f);
		~OcelotCameraInfo();

		// Methods


		// Accessors
		inline Vector3 Eye() const { return m_vEyePos;}
		inline Vector3 LookAt() const { return m_vLAtPos;}
		inline Vector3 Up() const { return m_vUpVect;}
		inline FFLOAT  Fov() const { return m_fFovy;}
		inline FFLOAT  Aspect() const { return m_fAspect;}
		inline FFLOAT  Near() const { return m_fNearPlane;}
		inline FFLOAT  Far() const { return m_fFarPlane;}
		inline VVOID   SetEye(Vector3 eyePos) { m_vEyePos = eyePos;}
		inline VVOID   SetLookAt(Vector3 lookAt) { m_vLAtPos = lookAt;}
		inline VVOID   SetUp(Vector3 up) { m_vUpVect = up;}
		inline VVOID   SetFov(FFLOAT fov) { m_fFovy = fov;}
		inline VVOID   SetAspect(FFLOAT aspect) { m_fAspect = aspect;}
		inline VVOID   SetNear(FFLOAT fNear) { m_fNearPlane = fNear;}
		inline VVOID   SetFar(FFLOAT fFar) { m_fFarPlane = fFar;}
	};
}

#endif