#include "OcelotXMLSplitter.h"

using namespace Ocelot;
using namespace std;

OcelotXMLSplitter::OcelotXMLSplitter()
{}

OcelotXMLSplitter::~OcelotXMLSplitter()
{}

//Split string where the delimiter is whitespace
VVOID OcelotXMLSplitter::splitWithBlanks()
{
	// test ways to split a string 
	m_sTest = "Il �tait une fois la grande bleue !!!";
	istringstream iSS(m_sTest);
	copy(istream_iterator<string>(iSS), istream_iterator<string>(), back_inserter<vector<string>>(m_vStrings));
}

//Split string where the delimiter is a specified character
VVOID OcelotXMLSplitter::splitWithDelimiter(vector<string>& result, const string& toSplit, const CCHAR& delimiter)
{
	string::size_type start = 0;
	string::size_type stop  = toSplit.find(delimiter);

	while(stop != string::npos)
	{
		result.push_back(toSplit.substr(start, (stop - start)));
		// reset start and stop at the new right value
		// and restart the process to the next delimiter
		// encountered while string is not finished.
		start = ++stop;
		stop  = toSplit.find(delimiter, stop);
		// then if break loop test encountered, set 
		// the rest in the array end Ciaoooooo.
		if(stop == string::npos)
			result.push_back(toSplit.substr(start, toSplit.length()));
	}
}

//Erase whitespace of the passed string
VVOID OcelotXMLSplitter::eraseBlanks(string& noBlankIn)
{
	noBlankIn.erase(remove(noBlankIn.begin(), noBlankIn.end(), ' '), noBlankIn.end());
}
