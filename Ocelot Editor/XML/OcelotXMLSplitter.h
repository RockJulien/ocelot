#ifndef DEF_OCELOTXMLSPLITTER_H
#define DEF_OCELOTXMLSPLITTER_H

/*
	Utility class used to split strings read from XML files
*/

#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>
#include "IOcelotXMLSplitter.h"

namespace Ocelot
{
	class OcelotXMLSplitter : public IOcelotXMLSplitter
	{
	private:

		std::string              m_sTest;
		std::vector<std::string> m_vStrings;

	public:

		OcelotXMLSplitter();
		~OcelotXMLSplitter();

		//Split string where the delimiter is whitespace
		virtual VVOID splitWithBlanks();
		//Split string where the delimiter is a specified character
		virtual VVOID splitWithDelimiter(std::vector<std::string>& result, const std::string& toSplit, const CCHAR& delimiter);
		//Erase whitespace of the passed string
		virtual VVOID eraseBlanks(std::string& noBlankIn);

	};
}

#endif