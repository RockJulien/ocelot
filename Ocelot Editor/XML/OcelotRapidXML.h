#ifndef OCELOTRAPIDXML_H
#define OCELOTRAPIDXML_H

/*
This file holds all the #includes needed to use the rapidxml XML parser.
It also has utility methods that ease the extraction of data from XML files.
*/

#include "rapidxml.hpp"
#include "rapidxml_iterators.hpp"
#include "rapidxml_print.hpp"
#include "rapidxml_utils.hpp"

#include "../Maths/Vector3.h"

/*
Typedefs used so that if rapidxml changes, then these typedefs will only need changing in order to work in Ocelot Engine
Instead of going around the implementation files and looking for every instance of rapidxml object that needs changing.
*/
typedef rapidxml::xml_node<> OcelotXMLNode;
typedef rapidxml::file<> OcelotXMLFile;
typedef rapidxml::xml_document<> OcelotXMLDoc;

class OcelotXML
{
public:
	/*
		This utlility function extracts a 3D vector from a specific XML node
		The 3 values that are to be extracted from the XML file (and placed into
		the resultant Vector3) are specified by the CCHAR* params.

		This means that instead of seeing code the 3 lines (the ones in function 
		below that do the actual extracting) all over the place, they can be replaced
		with one line (the call to this function with the required params), making 
		the code neater.
	*/
	static Vector3 ExtractVector3(OcelotXMLNode * node, CCHAR* x, CCHAR* y, CCHAR* z)
	{
		Vector3 result;
		result.x((FFLOAT)(atof(node->first_attribute(x)->value())));
		result.y((FFLOAT)(atof(node->first_attribute(y)->value())));
		result.z((FFLOAT)(atof(node->first_attribute(z)->value())));
		return result;
	}

	/*
		This is very similar to the function above, except this extracts a 
		2D vector instead of a 3D vector.
	*/
	static Vector2 ExtractVector2(OcelotXMLNode * node, CCHAR* x, CCHAR* y)
	{
		Vector2 result;
		result.x((FFLOAT)(atof(node->first_attribute(x)->value())));
		result.y((FFLOAT)(atof(node->first_attribute(y)->value())));
		return result;
	}
};


#endif