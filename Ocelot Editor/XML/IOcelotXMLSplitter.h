#ifndef IOCELOTXMLSPLITTER_H
#define IOCELOTXMLSPLITTER_H

#include <string>
#include "..\Maths\Utility.h"

/*
	Interface of the XML splitter/delimiter.
	As LINT as all XML splitter implementations follow this interface, then an instance of IOcelotXMLSplitter* can be swapped with
	any of these instances at runtime without having to make any changes.
*/

namespace Ocelot
{
	class IOcelotXMLSplitter
	{
	public:
		//Split string where the delimiter is whitespace
		virtual VVOID splitWithBlanks() = 0;
		//Split string where the delimiter is a specified character
		virtual VVOID splitWithDelimiter(std::vector<std::string>& result, const std::string& toSplit, const CCHAR& delimiter) = 0;
		//Erase whitespace of the passed string
		virtual VVOID eraseBlanks(std::string& noBlankIn) = 0;
	};
}

#endif