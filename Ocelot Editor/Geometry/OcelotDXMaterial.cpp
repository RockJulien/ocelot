#include "OcelotDXMaterial.h"

using namespace Ocelot;
using namespace Ocelot::OcelotColor;

PhongMaterial::PhongMaterial() :
m_cAmbient(DARKGREY), m_cDiffuse(DARKGREY), m_cSpecular(DARKGREY), m_rvTexture(NULL)
{

}

PhongMaterial::PhongMaterial(Color ambient, Color diffuse, Color specular, ID3D10ShaderResourceView* texture) :
m_cAmbient(ambient), m_cDiffuse(diffuse), m_cSpecular(specular), m_rvTexture(texture)
{
	
}

PhongMaterial::~PhongMaterial()
{
	//ReleaseCOM(m_rvTexture);
}

VVOID PhongMaterial::releaseCOM()
{
	ReleaseCOM(m_rvTexture);
}

BumpMaterial::BumpMaterial() :
m_rvTexture(NULL), m_rvNormalMap(NULL), m_rvSpecularMap(NULL)
{

}

BumpMaterial::BumpMaterial(ID3D10ShaderResourceView* texture, ID3D10ShaderResourceView* normalMap, ID3D10ShaderResourceView* specularMap) :
m_rvTexture(texture), m_rvNormalMap(normalMap), m_rvSpecularMap(specularMap)
{

}

BumpMaterial::~BumpMaterial()
{
	
}

VVOID BumpMaterial::releaseCOM()
{
	ReleaseCOM(m_rvTexture);
	ReleaseCOM(m_rvNormalMap);
	ReleaseCOM(m_rvSpecularMap);
}

OBJMaterial::OBJMaterial() :
mAmbient(0.2f, 0.2f, 0.2f), mDiffuse(0.8f, 0.8f, 0.8f), mSpecular(1.0f), mTexture(NULL),
mName("Default"), mShininess(0), mAlpha(1.0f), mHasSpecular(false), mTechnique(NULL)
{

}

OBJMaterial::OBJMaterial(Vector3 pAmbient, Vector3 pDiffuse, Vector3 pSpecular, ResViewPtr pTexture, const STRING& pName, BIGINT pShininess, FFLOAT pAlpha, BBOOL pHasSpec) :
mAmbient(pAmbient), mDiffuse(pDiffuse), mSpecular(pSpecular), mTexture(pTexture),
mName(pName), mShininess(pShininess), mAlpha(pAlpha), mHasSpecular(pHasSpec), mTechnique(NULL)
{

}

OBJMaterial::~OBJMaterial()
{

}

VVOID OBJMaterial::releaseCOM()
{
	ReleaseCOM(mTexture);
}

TriPlanarBumpMaterial::TriPlanarBumpMaterial()
{
	for(USMALLINT i = 0; i < 9; i++)
		m_rvTriPlanarTextures[i] = NULL;
}

TriPlanarBumpMaterial::~TriPlanarBumpMaterial()
{

}

VVOID TriPlanarBumpMaterial::releaseCOM()
{
	for(USMALLINT i = 0; i < 9; i++)
		ReleaseCOM(m_rvTriPlanarTextures[i]);
}

TriPlanarTexturePalette::TriPlanarTexturePalette() :
m_rvPalette4096x4096(NULL)
{

}

TriPlanarTexturePalette::~TriPlanarTexturePalette()
{

}

VVOID TriPlanarTexturePalette::releaseCOM()
{
	ReleaseCOM(m_rvPalette4096x4096);
}
