#ifndef DEF_AOCELOTBASEMESH_H
#define DEF_AOCELOTBASEMESH_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   OcelotBaseMesh.h/cpp
/   
/  Description: This object is a base for every object.
/               Contains common attributes none API-dependent
/               for being accessed by children.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

// Includes
#include "..\MeshEditor\MeshEditor.h"
#include "IOcelotMesh.h"
#include "..\StringHelper\STRINGHelpers.h"

// Namespaces
namespace Ocelot
{
	// Class definition
	class AOcelotBaseMesh : public IOcelotMesh
	{
	protected:

			// Attributes
			Matrix4x4 m_matWorld;
			Matrix4x4 m_matTranslation;
			Matrix4x4 m_matRotation;
			Matrix4x4 m_matScaling;
			Matrix4x4 mTextureTransform;
			Vector3   mOrientation;
			IRayIntersectable* mDrawableBounds;
			IRayIntersectable* mWorldBounds;
			UBIGINT   m_iNbVertices;
			UBIGINT   m_iNbIndices;
			FFLOAT    m_fDeltaTime;
			BBOOL     m_bRenderBounds;
			BBOOL     m_bVisible;
			BBOOL	  mHasTextureTransform;

			BBOOL   	m_bUsePhysics;
			IParticle*	m_particle;

			// Constructor
			AOcelotBaseMesh();

			// Private methods
			virtual VVOID RefreshWorldBounds() = 0;
			VVOID ClearPhysics();

	public:

			// Destructor
	virtual ~AOcelotBaseMesh();

			// Factory Methods
	virtual IMeshEditor* StartEdition();

			// methods
	virtual VVOID  update(FFLOAT deltaTime);
	virtual BIGINT renderMesh();
	virtual VVOID  releaseInstance();

			// Accessors
	inline  const IRayIntersectable* LocalBounds() const { return this->mDrawableBounds; };
	inline  const IRayIntersectable* WorldBounds() const { return this->mWorldBounds; };
	inline  const Matrix4x4*		 World() const { return &m_matWorld;}
	inline  UBIGINT   VertexNb() const { return m_iNbVertices;}
	inline  UBIGINT   IndiceNb() const { return m_iNbIndices;}
	inline  FFLOAT    DeltaTime() const { return m_fDeltaTime;}
	inline  BBOOL     IsBounded() const { return m_bRenderBounds;}
	inline  BBOOL     IsVisible() const { return m_bVisible;}
	inline  VVOID     SetWorld(const Matrix4x4& newWorld) { m_matWorld = newWorld;}
	inline  VVOID     SetVisibility(BBOOL pIsVisible) { m_bVisible = pIsVisible; }
	inline  VVOID     ShowBound() { m_bRenderBounds = true;}
	inline  VVOID     HideBound() { m_bRenderBounds = false;}
	inline  VVOID     Visible() { m_bVisible = true;}
	inline  VVOID     NotVisible() { m_bVisible = false;}
	inline  VVOID     SetVisible(BBOOL state) { m_bVisible = state;}
	inline  VVOID	  SetPhysicsUse(bool pUsePhysics) { m_bUsePhysics = pUsePhysics; this->ClearPhysics(); }
	inline  BBOOL	  UsesPhysics() const { return m_bUsePhysics; }
	inline IParticle* GetParticle() const { return m_particle; }
	inline  Vector3	  Position() const { return Vector3(this->m_matTranslation.get_41(), this->m_matTranslation.get_42(), this->m_matTranslation.get_43()); }
	inline  VVOID	  SetPosition(FFLOAT pX, FFLOAT pY, FFLOAT pZ) { Matrix4x4 lIdentity;  lIdentity.Matrix4x4Translation(this->m_matTranslation, pX, pY, pZ); m_particle->SetPosition(Vector3(this->m_matTranslation.get_41(), this->m_matTranslation.get_42(), this->m_matTranslation.get_43())); }
	inline  Vector3   Orientation() const { return this->mOrientation; }
	inline  VVOID	  SetTextureTransform(const Matrix4x4& pTransform)
	{
		this->mTextureTransform = pTransform;
	}
	inline  VVOID     SetUseTransform(BBOOL pUseTextureTransform)
	{
		this->mHasTextureTransform = pUseTextureTransform;
	}

	friend class MeshEditor;

	};
}

#endif
