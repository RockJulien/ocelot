#include "MeshGeometry.h"
#include "DataStructures\VectorIterator.h"

// Namespaces used
using namespace Ocelot;

MeshGeometry::MeshGeometry(const MeshInfo& pMeshInfo) :
OcelotDXMesh(), mInfo(pMeshInfo)
{

}

MeshGeometry::~MeshGeometry()
{
	this->releaseInstance();
}

VVOID  MeshGeometry::update(FFLOAT deltaTime)
{
	// refresh variables in the shader file
	HR(updateEffectVariables());

	OcelotDXMesh::update(deltaTime);
}

BIGINT MeshGeometry::updateEffectVariables()
{
	HRESULT lHR = S_OK;

	// Compute the world inverse transpose matrix.
	Matrix4x4 matV, matP, matWorldInvTrans;
	matV = mInfo.GetCamera()->View();
	matP = mInfo.GetCamera()->Proj();
	m_matWorld.Matrix4x4Inverse(matWorldInvTrans);
	matWorldInvTrans.Matrix4x4Transpose(matWorldInvTrans);

	HR(m_fxLight->SetRawValue(mInfo.Light(), 0, sizeof(OcelotLight)));
	HR(m_fxFog->SetRawValue(mInfo.Fog(), 0, sizeof(OcelotFog)));
	HR(m_fxWorld->SetMatrix((FFLOAT*)&m_matWorld));
	HR(m_fxView->SetMatrix((FFLOAT*)&matV));
	HR(m_fxProj->SetMatrix((FFLOAT*)&matP));
	HR(m_fxWorldInvTrans->SetMatrix((FFLOAT*)&matWorldInvTrans));
	HR(m_fxEyePosition->SetRawValue(&mInfo.GetCamera()->Info().Eye(), 0, sizeof(Vector3)));

	// Cannot update material there as it is linked to the subset being drawn
	// so must be done before the draw call.

	return (BIGINT)lHR;
}

BIGINT MeshGeometry::renderMesh()
{
	// set the input layout and buffers
	mInfo.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UBIGINT stride = sizeof(EnhancedVertex);
	UBIGINT offset = 0;
	mInfo.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	mInfo.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	mInfo.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	for 
		( BIGINT lCurrSubset = 0; 
				 lCurrSubset < this->mInfo.SubsetTable().Size(); 
				 lCurrSubset++ )
	{
		this->RenderSubset( lCurrSubset );
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT MeshGeometry::initializeLayout()
{
	HRESULT hr = S_OK;

	// Define the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D10_INPUT_PER_VERTEX_DATA, 0 },
	};
	UBIGINT nbElements = sizeof(layout) / sizeof(layout[0]);

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(mInfo.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

	return (BIGINT)hr;
}

BIGINT MeshGeometry::initializeEffect()
{
	return initializeDXEffect(mInfo.Device(), mInfo.EffectPath(), &m_effect);
}

BIGINT MeshGeometry::initializeVertices()
{
	HRESULT lHR = S_OK;

	const MeshInfo::MeshVertices& lVertices = this->mInfo.Vertices();

	m_iNbVertices = lVertices.Size();

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage = D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth = sizeof(EnhancedVertex) * m_iNbVertices;
	buffDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = &lVertices[0];

	HR(mInfo.Device()->CreateBuffer(&buffDesc, &InitData, &m_vertexBuffer));

	return (BIGINT)lHR;
}

BIGINT MeshGeometry::initializeIndices()
{
	HRESULT lHR = S_OK;

	const MeshInfo::MeshIndices& lIndices = this->mInfo.Indices();

	this->m_iNbIndices = lIndices.Size();

	// then pass the indices array
	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage = D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth = sizeof(UBIGINT) * m_iNbIndices;
	buffDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags = 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = &lIndices[0];

	// Create indice buffer
	HR(mInfo.Device()->CreateBuffer(&buffDesc, &InitData, &m_indiceBuffer));

	return (BIGINT)lHR;
}

VVOID  MeshGeometry::initializeEffectVariables()
{
	for 
		( UBIGINT lCurrMaterial = 0; 
	              lCurrMaterial < (UBIGINT)this->mInfo.Materials().Size(); 
				  lCurrMaterial++ )
	{
		const CCHAR* lStrTechnique = "";
		OBJMaterial* lMaterial = this->mInfo.Materials()[ lCurrMaterial ];
		if 
			( lMaterial->Texture() && 
			  lMaterial->HasSpecular() )
		{
			lStrTechnique = "TexturedSpecular";
		}
		else if 
			( lMaterial->Texture() && 
			  !lMaterial->HasSpecular() )
		{
			lStrTechnique = "TexturedNoSpecular";
		}
		else if 
			( !lMaterial->Texture() && 
			  lMaterial->HasSpecular() )
		{
			lStrTechnique = "Specular";
		}
		else if 
			( !lMaterial->Texture() && 
			  !lMaterial->HasSpecular() )
		{
			lStrTechnique = "NoSpecular";
		}

		// Just for layout initialization.
		this->m_fxTechnique = m_effect->GetTechniqueByName( lStrTechnique );
		// Just for layout initialization.

		lMaterial->SetTechnique( this->m_fxTechnique );
	}

	m_fxWorld	      = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView	      = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj	      = m_effect->GetVariableByName("gProjection")->AsMatrix();
	m_fxWorldInvTrans = m_effect->GetVariableByName("gWorldInvTrans")->AsMatrix();
	m_fxLight	      = m_effect->GetVariableByName("gLight");
	m_fxFog		      = m_effect->GetVariableByName("gFog");
	m_fxEyePosition   = m_effect->GetVariableByName("gEyePosW");
	m_fxTexture       = m_effect->GetVariableByName("gTexture")->AsShaderResource();
	mMaterialAmbient  = m_effect->GetVariableByName("gMaterialAmbient")->AsVector();
	mMaterialDiffuse  = m_effect->GetVariableByName("gMaterialDiffuse")->AsVector();
	mMaterialSpecular = m_effect->GetVariableByName("gMaterialSpecular")->AsVector();
	mMaterialAlpha    = m_effect->GetVariableByName("gMaterialAlpha")->AsScalar();
	mMaterialShininess = m_effect->GetVariableByName("gMaterialShininess")->AsScalar();
}

VVOID  MeshGeometry::releaseInstance()
{
	// release every COM of DirectX
	OcelotDXMesh::releaseInstance();

	DeletePointer( this->mWorldBounds );
	DeletePointer( this->mDrawableBounds );

	this->m_fxTexture = NULL;
	this->m_fxWorldInvTrans = NULL;
	this->m_fxLight = NULL;
	this->m_fxFog = NULL;
	this->m_fxEyePosition = NULL;
	this->m_fxTechnique = NULL;

	this->mInfo.ReleaseData();
}

VVOID  MeshGeometry::RefreshWorldBounds()
{

}

VVOID  MeshGeometry::RenderSubset(UBIGINT pSubset)
{
	const MeshInfo::MeshMaterials& lMaterials = this->mInfo.Materials();
	const MeshInfo::MeshSubsetTable& lSubsets = this->mInfo.SubsetTable();

	if 
		( pSubset < 0 ||
		  pSubset >= lMaterials.Size() )
	{
		return;
	}

	OBJMaterial* lMaterial = lMaterials[ pSubset ];
	Subset* lSubset = lSubsets[ pSubset ];

	if 
		( lMaterial->Texture() != NULL )
	{
		HR( m_fxTexture->SetResource( lMaterial->Texture() ) );
	}

	HR( mMaterialAmbient->SetFloatVector( lMaterial->Ambient() ) );
	HR( mMaterialDiffuse->SetFloatVector( lMaterial->Diffuse() ) );
	HR( mMaterialSpecular->SetFloatVector( lMaterial->Specular() ) );
	HR( mMaterialAlpha->SetFloat( lMaterial->Alpha() ) );
	HR( mMaterialShininess->SetInt( lMaterial->Shininess() ) );

	if
		( m_bVisible )
	{
		TecPtr lTechnique = lMaterial->Technique();
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR( lTechnique->GetDesc(&techDesc) );

		// for every pass in the shader file.
		for 
			( UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++ )
		{
			HR( lTechnique->GetPassByIndex(p)->Apply(0) );
			mInfo.Device()->DrawIndexed( lSubset->FaceCount() * 3, 
										 lSubset->FaceStart() * 3, 
										 0 );
		}
	}
}

/**************************************************************************************************************************************/
MeshInfo::MeshInfo() :
BaseDXData(), mLight(NULL), mFog(NULL), mEffectPath(L""), mPosition(0.0f)
{

}

MeshInfo::MeshInfo(DevPtr pDevice, CamPtr pCamera, OcelotLight* pLight, OcelotFog* pFog, const WSTRING& pEffectPath, const Vector3& pPosition) :
BaseDXData(pDevice, pCamera), mLight(pLight), mFog(pFog), mEffectPath(pEffectPath), mPosition(pPosition)
{

}

MeshInfo::~MeshInfo()
{

}

VVOID MeshInfo::ReleaseData()
{
	this->mVertices.RemoveAll();
	this->mIndices.RemoveAll();
	this->mSubsetTable.RemoveAll();

	VectorIter<OBJMaterial*> lIter = this->mMaterials.Iterator();
	while 
		( lIter.NotEnd() )
	{
		OBJMaterial* lMat = lIter.Current();
		DeletePointer( lMat );
		lIter.MoveNext();
	}
}
