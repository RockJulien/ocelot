#include "AOcelotBaseMesh.h"

using namespace Ocelot;

AOcelotBaseMesh::AOcelotBaseMesh() :
m_iNbVertices(0), m_iNbIndices(0), mDrawableBounds(NULL), mWorldBounds(NULL), m_bRenderBounds(false), 
m_bVisible(true), m_bUsePhysics(false), mOrientation(0.0f), mHasTextureTransform(false)
{
	m_particle = new Particle();
	m_particle->SetMass(1.0f);
	m_particle->SetDamping(0.98f);
}

AOcelotBaseMesh::~AOcelotBaseMesh()
{
	
}

IMeshEditor* AOcelotBaseMesh::StartEdition()
{
	return new MeshEditor(this);
}

VVOID  AOcelotBaseMesh::update(FFLOAT deltaTime)
{
	// Refresh the world matrix
	this->m_matWorld.Matrix4x4Identity();
	this->m_matScaling.Matrix4x4Mutliply(this->m_matWorld, this->m_matRotation, this->m_matTranslation);

	if
		( this->mDrawableBounds != NULL )
	{
		this->mDrawableBounds->SetWorld(this->m_matWorld);
		this->mDrawableBounds->Update(deltaTime);
	}

	this->RefreshWorldBounds();

	if 
		( this->m_bUsePhysics )
	{
		this->m_particle->EulerIntegrate(deltaTime);
		Vector3 lPosition = m_particle->GetPosition();
		this->SetPosition(lPosition.getX(), lPosition.getY(), lPosition.getZ());
	}
	else
	{
		this->ClearPhysics();
	}
}

BIGINT AOcelotBaseMesh::renderMesh()
{
	if
		(this->mDrawableBounds != NULL &&
		 this->m_bVisible && this->m_bRenderBounds )
	{
		this->mDrawableBounds->RenderBounds();
	}

	return 1;
}

VVOID  AOcelotBaseMesh::releaseInstance()
{
	if
		( m_particle )
	{
		DeletePointer(m_particle);
	}

	if 
		( this->mDrawableBounds != NULL )
	{
		DeletePointer(this->mDrawableBounds);
	}
}

VVOID  AOcelotBaseMesh::ClearPhysics()
{
	m_particle->ClearAccumulator(); 
	m_particle->SetVelocity(Vector3(0.0f, 0.0f, 0.0f));
}
