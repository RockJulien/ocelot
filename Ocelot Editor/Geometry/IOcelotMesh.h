#ifndef DEF_IOCELOTMESH_H
#define DEF_IOCELOTMESH_H

/*****************************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   IOcelotMesh.h
/   
/  Description: This object is an interface for every object which
/               could be implemented for drawing through an
/               Graphic API and its pipeline.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    30/09/2012
/*****************************************************************************/

#include "VertexTypes.h"
#include "DataStructures\DataStructureUtil.h"
#include "Maths\Matrix4x4.h"
#include "Physics\Particle.h"
#include "MeshEditor\IMeshEditor.h"
#include "Collisions\AABB.h"

namespace Ocelot
{
	class IOcelotMesh
	{
	public:

			// Constructor & Destructor
			IOcelotMesh() {}
	virtual ~IOcelotMesh(){}

			// Factory methods
	virtual IMeshEditor* StartEdition() = 0;

			// Methods per frame.
	virtual VVOID  update(FFLOAT deltaTime)    = 0;
	virtual BIGINT updateEffectVariables()     = 0;
	virtual BIGINT renderMesh()                = 0;

			// Methods to use once.
	virtual BIGINT createInstance()            = 0;
	virtual BIGINT initializeLayout()          = 0;
	virtual BIGINT initializeEffect()          = 0;
	virtual BIGINT initializeVertices()        = 0;
	virtual BIGINT initializeIndices()         = 0;
	virtual VVOID  initializeEffectVariables() = 0;
	virtual VVOID  releaseInstance()           = 0;

			// Accessors
	virtual const IRayIntersectable* LocalBounds() const = 0;
	virtual const IRayIntersectable* WorldBounds() const = 0;
	virtual const Matrix4x4*		 World() const = 0;
	virtual IParticle* GetParticle() const = 0;
	virtual VVOID	   SetPhysicsUse(bool pUsePhysics) = 0;
	virtual BBOOL      UsesPhysics() const = 0;
	virtual BBOOL      IsBounded() const = 0;
	virtual BBOOL      IsVisible() const = 0;
	virtual Vector3	   Position() const = 0;
	virtual Vector3    Orientation() const = 0;
	virtual VVOID      ShowBound() = 0;
	virtual VVOID      HideBound() = 0;
	virtual VVOID      SetWorld(const Matrix4x4& pNewWorld) = 0;
	virtual VVOID      SetVisibility(BBOOL pIsVisible) = 0;
	virtual VVOID	   SetPosition(FFLOAT pX, FFLOAT pY, FFLOAT pZ) = 0;

	};
}

#endif
