#include "Cube.h"
#include "..\Collisions\AABB.h"
#include "..\ResourceManager\ResourceFactory.h"

using namespace Ocelot;

Cube::Cube() :
OcelotDXMesh(), m_info(), m_fxAmbient(NULL), m_fxDiffuse(NULL), m_fxSpecular(NULL), 
m_fxWorldInvTrans(NULL), m_fxLight(NULL), m_fxFog(NULL), m_fxEyePosition(NULL)
{
	// load textures.
	BumpMaterial lMat;
	ResFactory->LoadBumpMaterial( this->m_info.TexturePath(), lMat );
	this->m_info.SetMaterial( lMat );
}

Cube::Cube(CubeInfo info) :
OcelotDXMesh(), m_info(info), m_fxAmbient(NULL), m_fxDiffuse(NULL), m_fxSpecular(NULL), 
m_fxWorldInvTrans(NULL), m_fxLight(NULL), m_fxFog(NULL), m_fxEyePosition(NULL)
{
	// load textures.
	BumpMaterial lMat;
	ResFactory->LoadBumpMaterial( this->m_info.TexturePath(), lMat );
	this->m_info.SetMaterial( lMat );

	Vector3 lPosition = this->m_info.Position();
	this->SetPosition(lPosition.getX(), lPosition.getY(), lPosition.getZ());
}

Cube::~Cube()
{
	releaseInstance();
}

VVOID  Cube::update(FFLOAT deltaTime)
{
	// refresh variables in the shader file
	HR(updateEffectVariables());

	OcelotDXMesh::update(deltaTime);
}

BIGINT Cube::updateEffectVariables()
{
	HRESULT hr = S_OK;

	// Compute the world inverse transpose matrix.
	Matrix4x4 matV, matP, matWorldInvTrans;
	matV = m_info.GetCamera()->View();
	matP = m_info.GetCamera()->Proj();
	m_matWorld.Matrix4x4Inverse(matWorldInvTrans);
	matWorldInvTrans.Matrix4x4Transpose(matWorldInvTrans);

	// set mesh's attributes to the shader's attributes.
	HR(m_fxLight->SetRawValue(m_info.Light(), 0, sizeof(OcelotLight)));
	HR(m_fxFog->SetRawValue(m_info.Fog(), 0, sizeof(OcelotFog)));
	HR(m_fxWorld->SetMatrix((FFLOAT*)&m_matWorld));
	HR(m_fxView->SetMatrix((FFLOAT*)&matV));
	HR(m_fxProj->SetMatrix((FFLOAT*)&matP));
	HR(m_fxWorldInvTrans->SetMatrix((FFLOAT*)&matWorldInvTrans));
	HR(m_fxEyePosition->SetRawValue(&m_info.GetCamera()->Info().Eye(), 0, sizeof(Vector3)));
	HR(m_fxAmbient->SetResource(m_info.Material().Ambient()));
	HR(m_fxDiffuse->SetResource(m_info.Material().Diffuse()));
	HR(m_fxSpecular->SetResource(m_info.Material().Specular()));

	if
		(this->mHasTextureTransform)
	{
		HR(m_fxTextureTransform->SetMatrix((FFLOAT*)&mTextureTransform));
	}

	return (BIGINT)hr;
}

BIGINT Cube::renderMesh()
{
	// set the input layout and buffers
	m_info.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UBIGINT stride = sizeof(EnhancedVertex);
	UBIGINT offset = 0;
	m_info.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	m_info.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	m_info.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if(m_bVisible)
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for(UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			m_info.Device()->DrawIndexed(m_iNbIndices, 0, 0);
		}
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT Cube::initializeLayout()
{
	HRESULT hr = S_OK;

	// Define the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 36, D3D10_INPUT_PER_VERTEX_DATA, 0 },
	};
	UBIGINT nbElements = sizeof( layout ) / sizeof( layout[0] );

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(m_info.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

	return (BIGINT)hr;
}

BIGINT Cube::initializeEffect()
{
	return initializeDXEffect(m_info.Device(), m_info.EffectPath(), &m_effect);
}

BIGINT Cube::initializeVertices()
{
	HRESULT hr = S_OK;

	m_iNbVertices = 24;  // 4 vertices per face (6 * 4)

	// cube vertices
	Vector3 P1 = Vector3( -m_info.Radius().getX(),  m_info.Radius().getY(), -m_info.Radius().getZ() ); // scale to the chosen pos.
	Vector3 P2 = Vector3(  m_info.Radius().getX(),  m_info.Radius().getY(), -m_info.Radius().getZ() );
	Vector3 P3 = Vector3(  m_info.Radius().getX(),  m_info.Radius().getY(),  m_info.Radius().getZ() );
	Vector3 P4 = Vector3( -m_info.Radius().getX(),  m_info.Radius().getY(),  m_info.Radius().getZ() );
	Vector3 P5 = Vector3( -m_info.Radius().getX(), -m_info.Radius().getY(), -m_info.Radius().getZ() );
	Vector3 P6 = Vector3(  m_info.Radius().getX(), -m_info.Radius().getY(), -m_info.Radius().getZ() );
	Vector3 P7 = Vector3(  m_info.Radius().getX(), -m_info.Radius().getY(),  m_info.Radius().getZ() );
	Vector3 P8 = Vector3( -m_info.Radius().getX(), -m_info.Radius().getY(),  m_info.Radius().getZ() );

	this->mDrawableBounds = new AABBMinMax3D(P5, P3);
	this->mDrawableBounds->CreateBounds(this->m_info.Device(), this->m_info.GetCamera(), 0.015f);

	// Compute cube's normals.
	Vector3 norm1;
	Vector3 norm2;
	Vector3 norm3;
	Vector3 norm4;
	Vector3 norm5;
	Vector3 norm6;
	Vector3 norm7;
	Vector3 norm8;
	calculateNormalAverage(P1, P2, P5, P4, norm1);
	calculateNormalAverage(P2, P3, P6, P1, norm2); 
	calculateNormalAverage(P3, P4, P7, P2, norm3);
	calculateNormalAverage(P4, P1, P8, P3, norm4); 
	calculateNormalAverage(P5, P8, P1, P6, norm5);
	calculateNormalAverage(P6, P5, P2, P7, norm6);
	calculateNormalAverage(P7, P6, P3, P8, norm7);
	calculateNormalAverage(P8, P7, P4, P5, norm8);

	// Compute cube's tangents.
	Vector3 tan1 = P2 - P1;  // Front surface.
	Vector3 tan2 = P4 - P3;  // Back surface.
	Vector3 tan3 = P2 - P1;  // Top surface.
	Vector3 tan4 = P4 - P3;  // Bottom surface.
	Vector3 tan5 = P1 - P4;  // Left surface.
	Vector3 tan6 = P3 - P2;  // Right surface.

	// Then, tangent average per vertex and normalize.
	Vector3 P1Tan = (tan1 + tan3 + tan5) / 3;
	P1Tan.Vec3Normalise();
	Vector3 P2Tan = (tan1 + tan3 + tan6) / 3;
	P2Tan.Vec3Normalise();
	Vector3 P3Tan = (tan2 + tan3 + tan6) / 3;
	P3Tan.Vec3Normalise();
	Vector3 P4Tan = (tan2 + tan3 + tan5) / 3;
	P4Tan.Vec3Normalise();
	Vector3 P5Tan = (tan1 + tan4 + tan5) / 3;
	P5Tan.Vec3Normalise();
	Vector3 P6Tan = (tan1 + tan4 + tan6) / 3;
	P6Tan.Vec3Normalise();
	Vector3 P7Tan = (tan2 + tan4 + tan6) / 3;
	P7Tan.Vec3Normalise();
	Vector3 P8Tan = (tan2 + tan4 + tan5) / 3;
	P8Tan.Vec3Normalise();

	EnhancedVertex vertices[24] =
	{
		// front face
		EnhancedVertex(P5, norm5, P5Tan, Vector2(0.0f, 1.0f)),
		EnhancedVertex(P1, norm1, P1Tan, Vector2(0.0f, 0.0f)),
		EnhancedVertex(P2, norm2, P2Tan, Vector2(1.0f, 0.0f)),
		EnhancedVertex(P6, norm6, P6Tan, Vector2(1.0f, 1.0f)),
		// back face
		EnhancedVertex(P8, norm8, P8Tan, Vector2(1.0f, 1.0f)),
		EnhancedVertex(P7, norm7, P7Tan, Vector2(0.0f, 1.0f)),
		EnhancedVertex(P3, norm3, P3Tan, Vector2(0.0f, 0.0f)),
		EnhancedVertex(P4, norm4, P4Tan, Vector2(1.0f, 0.0f)),
		// top face
		EnhancedVertex(P1, norm1, P1Tan, Vector2(0.0f, 1.0f)),
		EnhancedVertex(P4, norm4, P4Tan, Vector2(0.0f, 0.0f)),
		EnhancedVertex(P3, norm3, P3Tan, Vector2(1.0f, 0.0f)),
		EnhancedVertex(P2, norm2, P2Tan, Vector2(1.0f, 1.0f)),
		// bottom face
		EnhancedVertex(P5, norm5, P5Tan, Vector2(1.0f, 1.0f)),
		EnhancedVertex(P6, norm6, P6Tan, Vector2(0.0f, 1.0f)),
		EnhancedVertex(P7, norm7, P7Tan, Vector2(0.0f, 0.0f)),
		EnhancedVertex(P8, norm8, P8Tan, Vector2(1.0f, 0.0f)),
		// left face
		EnhancedVertex(P8, norm8, P8Tan, Vector2(0.0f, 1.0f)),
		EnhancedVertex(P4, norm4, P4Tan, Vector2(0.0f, 0.0f)),
		EnhancedVertex(P1, norm1, P1Tan, Vector2(1.0f, 0.0f)),
		EnhancedVertex(P5, norm5, P5Tan, Vector2(1.0f, 1.0f)),
		// right Face
		EnhancedVertex(P6, norm6, P6Tan, Vector2(0.0f, 1.0f)),
		EnhancedVertex(P2, norm2, P2Tan, Vector2(0.0f, 0.0f)),
		EnhancedVertex(P3, norm3, P3Tan, Vector2(1.0f, 0.0f)),
		EnhancedVertex(P7, norm7, P7Tan, Vector2(1.0f, 1.0f))
	};

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(EnhancedVertex) * m_iNbVertices;
	buffDesc.BindFlags		= D3D10_BIND_VERTEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= &vertices;

	HR(m_info.Device()->CreateBuffer(&buffDesc, &InitData, &m_vertexBuffer));
	
	return (BIGINT)hr;
}

BIGINT Cube::initializeIndices()
{
	HRESULT hr = S_OK;

	m_iNbIndices  = 36;  // 6 indices per face (6 * 6) -> 3 per triangle

	// Create index buffer
	UBIGINT indices[36] =
	{
		// front face
		0,1,2,
		0,2,3,
		// back face
		4,5,6,
		4,6,7,
		// top face
		8,9,10,
		8,10,11,
		// bottom face
		12,13,14,
		12,14,15,
		// left face
		16,17,18,
		16,18,19,
		// right face
		20,21,22,
		20,22,23
	};

	D3D10_BUFFER_DESC buffDesc;
	buffDesc.Usage			= D3D10_USAGE_IMMUTABLE;
	buffDesc.ByteWidth		= sizeof(UBIGINT) * m_iNbIndices;
	buffDesc.BindFlags		= D3D10_BIND_INDEX_BUFFER;
	buffDesc.CPUAccessFlags = 0;
	buffDesc.MiscFlags		= 0;

	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem		= &indices;

	HR(m_info.Device()->CreateBuffer(&buffDesc, &InitData, &m_indiceBuffer));
	
	return (BIGINT)hr;
}

VVOID  Cube::initializeEffectVariables()
{
	m_fxTechnique     = m_effect->GetTechniqueByName("BumpMapping");
	m_fxWorld         = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView          = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj          = m_effect->GetVariableByName("gProjection")->AsMatrix();
	m_fxWorldInvTrans = m_effect->GetVariableByName("gWorldInvTrans")->AsMatrix();
	m_fxLight         = m_effect->GetVariableByName("gLight");
	m_fxFog           = m_effect->GetVariableByName("gFog");
	m_fxEyePosition   = m_effect->GetVariableByName("gEyePosW");
	m_fxAmbient       = m_effect->GetVariableByName("gTexture")->AsShaderResource();
	m_fxDiffuse       = m_effect->GetVariableByName("gTextureNor")->AsShaderResource();
	m_fxSpecular      = m_effect->GetVariableByName("gTextureSpe")->AsShaderResource();

	if (this->mHasTextureTransform)
	{
		m_fxTextureTransform = m_effect->GetVariableByName("gSunTextureTransform")->AsMatrix();
	}
}

VVOID  Cube::releaseInstance()
{
	// release every COM of DirectX
	OcelotDXMesh::releaseInstance();

	DeletePointer(this->mWorldBounds);
	DeletePointer(this->mDrawableBounds);

	m_fxAmbient       = NULL;
	m_fxDiffuse       = NULL;
	m_fxSpecular      = NULL;
	m_fxWorldInvTrans = NULL;
	m_fxLight         = NULL;
	m_fxFog           = NULL;
	m_fxEyePosition   = NULL;
}

VVOID  Cube::calculateNormalAverage(const Vector3& origine, const Vector3& X, const Vector3& Y, const Vector3& Z, Vector3& out)
{
	Vector3 temp1;
	Vector3 temp2;
	Vector3 temp3;

	// in front of
	Vector3 u11 = X - origine;
	Vector3 u12 = Y - origine;
	temp1 = u11.Vec3CrossProduct(u12);
	temp1.Vec3Normalise();

	// above
	Vector3 u21 = X - origine;
	Vector3 u22 = Z - origine;
	temp2 = u22.Vec3CrossProduct(u21);
	temp2.Vec3Normalise();

	// beside
	Vector3 u31 = Z - origine;
	Vector3 u32 = Y - origine;
	temp3 = u32.Vec3CrossProduct(u31);
	temp3.Vec3Normalise();

	// compute the average of normal for this origine point
	out = (temp1 + temp2 + temp3) / 3;
	out.Vec3Normalise();
}

VVOID  Cube::RefreshWorldBounds()
{
	DeletePointer(this->mWorldBounds);

	Vector3 lMaxL = Vector3(this->m_info.Radius());
	Vector3 lMinL = Vector3(-this->m_info.Radius());

	Matrix4x4 lWorld = this->m_matWorld;

	Vector4 lMaxH = lWorld.Transform(Vector4(lMaxL.getX(), lMaxL.getY(), lMaxL.getZ(), 1.0f));
	Vector4 lMinH = lWorld.Transform(Vector4(lMinL.getX(), lMinL.getY(), lMinL.getZ(), 1.0f));

	this->mWorldBounds = new AABBMinMax3D( Vector3( lMinH.getX(), lMinH.getY(), lMinH.getZ() ), 
										   Vector3( lMaxH.getX(), lMaxH.getY(), lMaxH.getZ() ) );
}

/******************************** Circle Info Class ***********************************/
CubeInfo::CubeInfo() :
BaseDXData(), m_sEffectPath(L"..\\Effects\\BumpMapping.fx"), m_sTexturePath(L".\\Resources\\StealWoodBox.jpg"), m_vPosition(0.0f), m_vRadius(1.0f),
m_light(NULL), m_fog(NULL)
{
	
}

CubeInfo::CubeInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, OcelotLight* light, OcelotFog* fog, Vector3 position, Vector3 radius) :
BaseDXData(device, camera), m_sEffectPath(effectPath), m_sTexturePath(texturePath), m_light(light), m_fog(fog), m_vPosition(position), m_vRadius(radius)
{
	
}

CubeInfo::~CubeInfo()
{

}