#ifndef DEF_GRID_H
#define DEF_GRID_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Grid.h/cpp
/
/  Description: This file provides a class for creating simple
/               grid by giving the row and column count.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    03/10/2012
/*****************************************************************/

#include "OcelotDXMesh.h"
#include "OcelotLight.h"
#include "OcelotFog.h"

namespace Ocelot
{
									class GridInfo : public BaseDXData
									{
									private:

										OcelotLight*    m_light;
										OcelotFog*      m_fog;
										BumpMaterial    m_material;
										UBIGINT         m_nbRows;
										UBIGINT         m_nbCols;
										FFLOAT          m_terrainWidth;
										FFLOAT			m_terrainDepth;
										WSTRING         m_sTexturePath;
										WSTRING         m_sEffectPath;
										Vector3         m_vPosition;

									public:

										// Constructor & Destructor
										GridInfo();
										GridInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, OcelotLight* light, OcelotFog* fog, Vector3 position, UBIGINT pNbRows, UBIGINT pNbCols, FFLOAT pTerrainWidth, FFLOAT pTerrainDepth);
										~GridInfo();

										// Methods


										// Accessors
										inline OcelotLight*    Light() const { return m_light; }
										inline OcelotFog*      Fog() const { return m_fog; }
										inline BumpMaterial    Material() const { return m_material; }
										inline UBIGINT         RowCount() const { return m_nbRows; }
										inline UBIGINT         ColCount() const { return m_nbCols; }
										inline FFLOAT          TerrainWidth() const { return m_terrainWidth; }
										inline FFLOAT		   TerrainDepth() const { return m_terrainDepth; }
										inline WSTRING         TexturePath() const { return m_sTexturePath; }
										inline WSTRING         EffectPath() const { return m_sEffectPath; }
										inline Vector3         Position() const { return m_vPosition; }
										inline VVOID           SetLight(OcelotLight* light) { m_light = light; }
										inline VVOID           SetFog(OcelotFog* fog) { m_fog = fog; }
										inline VVOID           SetMaterial(const BumpMaterial& material) { m_material = material; }
										inline VVOID           RowCount(UBIGINT pRowCount) { m_nbRows = pRowCount; }
										inline VVOID           ColCount(UBIGINT pColCount) { m_nbCols = pColCount; }
										inline VVOID           TerrainWidth(FFLOAT pWidth) { m_terrainWidth = pWidth; }
										inline VVOID 		   TerrainDepth(FFLOAT pDepth) { m_terrainDepth = pDepth; }
										inline VVOID           SetTexturePath(WSTRING texPath) { m_sTexturePath = texPath; }
										inline VVOID           SetEffectPath(WSTRING effPath) { m_sEffectPath = effPath; }
										inline VVOID           SetPosition(Vector3 position) { m_vPosition = position; }
									};

	class Grid : public OcelotDXMesh
	{
	private:

		// Attributes
		GridInfo  mInfo;
		FFLOAT	  m_fCellWidth;
		FFLOAT    m_fCellDepth;
		VarPtr	  m_fxLight;
		VarPtr	  m_fxFog;
		MatPtr	  m_fxWorldInvTrans;
		VarPtr	  m_fxEyePosition;
		TexPtr    m_fxAmbient;
		TexPtr    m_fxDiffuse;
		TexPtr    m_fxSpecular;

		VVOID RefreshWorldBounds();

	public:

		// Constructor & Destructor
		Grid();
		Grid(const GridInfo& inf);
		~Grid();

		// Methods per frame.
		VVOID   update(FFLOAT deltaTime);
		BIGINT  updateEffectVariables();
		BIGINT  renderMesh();

		// Methods
		BIGINT  initializeLayout();
		BIGINT  initializeEffect();
		BIGINT  initializeVertices();
		BIGINT  initializeIndices();
		VVOID   initializeEffectVariables();
		VVOID   releaseInstance();

		// Accessors
		inline GridInfo& Info() { return mInfo; }
		inline FFLOAT	 getCellWidth() const { return m_fCellWidth; }
		inline FFLOAT	 getCellDepth() const { return m_fCellDepth; }
		inline VVOID     setInfo(const GridInfo& pInfo) { mInfo = pInfo; }
	};
}

#endif