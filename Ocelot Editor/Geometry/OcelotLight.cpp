#include "OcelotLight.h"

using namespace Ocelot;
using namespace Ocelot::OcelotColor;

OcelotParallelLight::OcelotParallelLight() :
m_cAmbient(GREY), m_cDiffuse(LIGHTGREY), m_cSpecular(DARKGREY), m_vDirection(0.0f, -0.8f, -1.0f), m_fSpecularPower(5.0f)
{

}

OcelotParallelLight::OcelotParallelLight(Vector3 direction, Color ambient, Color diffuse, Color specular, FFLOAT power) :
m_cAmbient(ambient), m_cDiffuse(diffuse), m_cSpecular(specular), m_vDirection(direction), m_fSpecularPower(power)
{

}

OcelotParallelLight::~OcelotParallelLight()
{

}

OcelotPointLight::OcelotPointLight() :
m_cAmbient(GREY), m_cDiffuse(LIGHTGREY), m_cSpecular(DARKGREY), m_vPosition(0.0f, 50.0f, 0.0f), 
m_vAttenuation(0.0f, 1.0f, 0.0f), m_fRange(50.0f), m_fSpecularPower(5.0f)
{

}

OcelotPointLight::OcelotPointLight(Vector3 position, Vector3 attenuation, Color ambient, Color diffuse, Color specular, FFLOAT range, FFLOAT power) :
m_cAmbient(ambient), m_cDiffuse(diffuse), m_cSpecular(specular), m_vPosition(position), 
m_vAttenuation(attenuation), m_fRange(range), m_fSpecularPower(power)
{

}

OcelotPointLight::~OcelotPointLight()
{

}

OcelotSpotLight::OcelotSpotLight() :
m_cAmbient(GREY), m_cDiffuse(LIGHTGREY), m_cSpecular(DARKGREY), m_vPosition(0.0f, 50.0f, 50.0f), m_vDirection(Vector3(0.0f, 0.0f, 0.0f) - m_vPosition), 
m_vAttenuation(0.0f, 1.0f, 0.0f), m_fRange(50.0f), m_fSpotPower(50.0f), m_fSpecularPower(5.0f)
{
	
}

OcelotSpotLight::OcelotSpotLight(Vector3 position, Vector3 direction, Vector3 attenuation, Color ambient, Color diffuse, Color specular, FFLOAT range, FFLOAT spotPower, FFLOAT specPower) :
m_cAmbient(ambient), m_cDiffuse(diffuse), m_cSpecular(specular), m_vPosition(position), m_vDirection(direction), 
m_vAttenuation(attenuation), m_fRange(range), m_fSpotPower(spotPower), m_fSpecularPower(specPower)
{

}

OcelotSpotLight::~OcelotSpotLight()
{

}
