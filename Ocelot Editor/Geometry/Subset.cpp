#include "Subset.h"

// Namespaces used
using namespace Ocelot;

Subset::Subset() :
mId(-1), mVertexStart(0), mVertexCount(0), 
mFaceStart(0), mFaceCount(0)
{

}

Subset::Subset(UBIGINT pId, UBIGINT pVertexStart, UBIGINT pVertexCount, UBIGINT pFaceStart, UBIGINT pFaceCount) :
mId(pId), mVertexStart(pVertexStart), mVertexCount(pVertexCount),
mFaceStart(pFaceStart), mFaceCount(pFaceCount)
{

}

Subset::~Subset()
{

}
