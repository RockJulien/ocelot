#ifndef DEF_CYLINDER_H
#define DEF_CYLINDER_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Cylinder.h/cpp
/   
/  Description: This file provides a class for creating cylinder.
/               The mesh will do bump mapping
/               on the surface, so make sure that the texture
/               path is the same for the three needed texture in
/               "resources" but adding _NRM and _SPEC to proper
/               textures (Normal map and Spec map) before the
/               extension.
/               (e.g. test.jpg      => diffuse.
/                     test_NRM.jpg  => ambient.
/                     test_SPEC.jpg => specular.)
/               The loadBumpMaterial() in DXHelper will do the rest.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    03/10/2012
/*****************************************************************/

#include "OcelotDXMesh.h"
#include "OcelotLight.h"
#include "OcelotFog.h"

namespace Ocelot
{
							class CylinderInfo : public BaseDXData
							{
							protected:

								// Attributes
								OcelotLight*    m_light;
								OcelotFog*      m_fog;
								BumpMaterial    m_material;
								WSTRING         m_sEffectPath;
								WSTRING         m_sTexturePath;
								Vector3         m_vPosition;
								UBIGINT         m_iRadius;
								UBIGINT         m_iHeight;
								UBIGINT         m_iNbRings;
								UBIGINT         m_iNbSlices;

							public:

								// Constructor & Destructor
								CylinderInfo();
								CylinderInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, OcelotLight* light, OcelotFog* fog, Vector3 position, UBIGINT radius = 2, UBIGINT height = 10, UBIGINT rings = 10, UBIGINT slices = 10);
								virtual ~CylinderInfo();

								// Methods


								// Accessors
								inline OcelotLight*  Light() const { return m_light;}
								inline OcelotFog*    Fog() const { return m_fog;}
								inline BumpMaterial  Material() const { return m_material;}
								inline WSTRING       EffectPath() const { return m_sEffectPath;}
								inline WSTRING       TexturePath() const { return m_sTexturePath;}
								inline Vector3       Position() const { return m_vPosition;}
								inline UBIGINT       Radius() const { return m_iRadius;}
								inline UBIGINT       Height() const { return m_iHeight;}
								inline UBIGINT       Rings() const { return m_iNbRings;}
								inline UBIGINT       Slices() const { return m_iNbSlices;}
								inline VVOID         SetLight(OcelotLight* light) { m_light = light;}
								inline VVOID         SetFog(OcelotFog* fog) { m_fog = fog;}
								inline VVOID         SetMaterial(BumpMaterial material) { m_material = material;}
								inline VVOID         SetEffectPath(WSTRING effectPath) { m_sEffectPath = effectPath;}
								inline VVOID         SetTexturePath(WSTRING texturePath) { m_sTexturePath = texturePath;}
								inline VVOID         SetPosition(Vector3 position) { m_vPosition = position;}
								inline VVOID         SetRadius(UBIGINT radius) { m_iRadius = radius;}
								inline VVOID         SetHeight(UBIGINT height) { m_iHeight = height;}
								inline VVOID         SetRings(UBIGINT rings) { m_iNbRings = rings;}
								inline VVOID         SetSlices(UBIGINT slices) { m_iNbSlices = slices;}
							};

	class Cylinder : public OcelotDXMesh
	{
	private:

		// Attributes
		CylinderInfo m_info;
		TexPtr       m_fxAmbient;
		TexPtr       m_fxDiffuse;
		TexPtr       m_fxSpecular;
		MatPtr       m_fxWorldInvTrans;
		VarPtr       m_fxLight;
		VarPtr       m_fxFog;
		VarPtr       m_fxEyePosition;

		// only useful for building indices.
		// for building top/bottom hats.
		UBIGINT      m_iTopBaseIndex;
		UBIGINT      m_iTopCenterIndex;
		UBIGINT      m_iBottomBaseIndex;
		UBIGINT      m_iBottomCenterIndex;

		VVOID RefreshWorldBounds();

	public:

		// Constructor & Destructor
		Cylinder();
		Cylinder(CylinderInfo info);
		~Cylinder();

		// Methods per frame.
		VVOID  update(FFLOAT deltaTime);
		BIGINT updateEffectVariables();
		BIGINT renderMesh();

		// Methods to use once.
		BIGINT initializeLayout();
		BIGINT initializeEffect();
		BIGINT initializeVertices();
		BIGINT initializeIndices();
		VVOID  initializeEffectVariables();
		VVOID  releaseInstance();

		// Accessors
		inline CylinderInfo Info() const { return m_info;}
		inline VVOID        SetInfo(CylinderInfo info) { m_info = info;}
	};
}

#endif