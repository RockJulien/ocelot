#ifndef DEF_OCELOTDXMATERIAL_H
#define DEF_OCELOTDXMATERIAL_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   OcelotDXMaterial.h/cpp
/   
/  Description: This file provides a classes for storing Material's
/               data for texturing a mesh. Two different classes
/               for simple Phong shading and Bump mapping involving
/               a normal and specular maps.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    03/10/2012
/*****************************************************************/

#include "DirectXAPI\DXHelperFunctions.h"
#include "DataStructures\DataStructureUtil.h"
#include "Maths\Vector3.h"
#include "Maths\Color.h"

namespace Ocelot
{
	class PhongMaterial
	{
	private:

		// Attributes
		Color                     m_cAmbient;
		Color                     m_cDiffuse;
		Color                     m_cSpecular;
		ID3D10ShaderResourceView* m_rvTexture;

	public:

		// Constructor & Destructor 
		PhongMaterial();
		PhongMaterial(Color ambient, Color diffuse, Color specular, ID3D10ShaderResourceView* texture);
		~PhongMaterial();

		// Methods
		VVOID releaseCOM();

		// Accessors
		inline Color					 Ambient() const { return m_cAmbient;}
		inline Color					 Diffuse() const { return m_cDiffuse;}
		inline Color					 Specular() const { return m_cSpecular;}
		inline ID3D10ShaderResourceView* Texture() const { return m_rvTexture;}
		inline VVOID					 SetAmbient(Color ambient) { m_cAmbient = ambient;}
		inline VVOID					 SetDiffuse(Color diffuse) { m_cDiffuse = diffuse;}
		inline VVOID					 SetSpecular(Color specular) { m_cSpecular = specular;}
		inline VVOID					 SetTexture(ID3D10ShaderResourceView* texture) { m_rvTexture = texture;}
	};

	class BumpMaterial
	{
	private:

		// Attributes
		ID3D10ShaderResourceView* m_rvTexture;
		ID3D10ShaderResourceView* m_rvNormalMap;   // rv = resource view, thus texture.
		ID3D10ShaderResourceView* m_rvSpecularMap;

	public:

		// Constructor & Destructor
		BumpMaterial();
		BumpMaterial(ID3D10ShaderResourceView* texture, ID3D10ShaderResourceView* normalMap, ID3D10ShaderResourceView* specularMap);
		~BumpMaterial();

		// Methods
		VVOID releaseCOM();

		// Accessors
		inline ID3D10ShaderResourceView* Diffuse() const { return m_rvTexture;}
		inline ID3D10ShaderResourceView* Ambient() const { return m_rvNormalMap;}
		inline ID3D10ShaderResourceView* Specular() const { return m_rvSpecularMap;}
		inline VVOID					 SetDiffuse(ID3D10ShaderResourceView* texture) { m_rvTexture = texture;}
		inline VVOID					 SetAmbient(ID3D10ShaderResourceView* normalMap) { m_rvNormalMap = normalMap;}
		inline VVOID					 SetSpecular(ID3D10ShaderResourceView* specularMap) { m_rvSpecularMap = specularMap;}
	};

	class OBJMaterial
	{
	private:

		// Attributes
		Vector3 mAmbient;
		Vector3 mDiffuse;
		Vector3 mSpecular;
		ResViewPtr mTexture;
		TecPtr	mTechnique;
		STRING mName;
		BIGINT  mShininess;
		FFLOAT  mAlpha;
		BBOOL   mHasSpecular;

	public:

		// Constructor & Destructor 
		OBJMaterial();
		OBJMaterial(Vector3 pAmbient, Vector3 pDiffuse, Vector3 pSpecular, ResViewPtr pTexture, const STRING& pName, BIGINT pShininess, FFLOAT pAlpha, BBOOL pHasSpec);
		~OBJMaterial();

		// Methods
		VVOID releaseCOM();

		// Accessors
		inline Vector3	  Ambient() const { return mAmbient; }
		inline Vector3	  Diffuse() const { return mDiffuse; }
		inline Vector3	  Specular() const { return mSpecular; }
		inline ResViewPtr Texture() const { return mTexture; }
		inline TecPtr	  Technique() const { return mTechnique; }
		inline STRING     Name() const { return mName; }
		inline BIGINT     Shininess() const { return mShininess; }
		inline FFLOAT     Alpha() const { return mAlpha; }
		inline BBOOL      HasSpecular() const { return mHasSpecular; }
		inline VVOID	  SetAmbient(const Vector3& ambient) { mAmbient = ambient; }
		inline VVOID	  SetDiffuse(const Vector3& diffuse) { mDiffuse = diffuse; }
		inline VVOID	  SetSpecular(const Vector3& specular) { mSpecular = specular; }
		inline VVOID	  SetTexture(ResViewPtr texture) { mTexture = texture; }
		inline VVOID 	  SetTechnique(TecPtr pTechnique) { mTechnique = pTechnique; }
		inline VVOID      SetName(const STRING& pName) { mName = pName; }
		inline VVOID      SetShininess(BIGINT pShininess) { mShininess = pShininess; }
		inline VVOID      SetAlpha(FFLOAT pAlpha) { mAlpha = pAlpha; }
		inline VVOID      SetHasSpecular(BBOOL pHasSpec) { mHasSpecular = pHasSpec; }
	};

	class TriPlanarBumpMaterial
	{
	private:

		// Attributes
		ID3D10ShaderResourceView* m_rvTriPlanarTextures[9];

	public:

		// Constructor & Destructor
		TriPlanarBumpMaterial();
		~TriPlanarBumpMaterial();

		// Methods
		VVOID releaseCOM();

		// Accessors
		inline ID3D10ShaderResourceView** TextureArray() { return m_rvTriPlanarTextures;}
		inline VVOID                      SetTexture(ID3D10ShaderResourceView* textRV, USMALLINT index) { m_rvTriPlanarTextures[index] = textRV;}
	};

	class TriPlanarTexturePalette
	{
	private:

		// Attributes
		ID3D10ShaderResourceView* m_rvPalette4096x4096;

	public:

		// Constructor & Destructor
		TriPlanarTexturePalette();
		~TriPlanarTexturePalette();

		// Methods
		VVOID releaseCOM();

		// Accessors
		inline ID3D10ShaderResourceView* Palette() { return m_rvPalette4096x4096;}
		inline VVOID                     SetPalette(ID3D10ShaderResourceView* palette) { m_rvPalette4096x4096 = palette;}
	};
}

#endif