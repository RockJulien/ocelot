#ifndef DEF_OCELOTDXMESH_H
#define DEF_OCELOTDXMESH_H

/*****************************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   OcelotDXMesh.h/cpp
/   
/  Description: This object is a base class for every object which
/               could be implemented for drawing through the
/               DirectX API and its pipeline.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    30/09/2012
/*****************************************************************************/

#include "BaseDXData.h"
#include "AOcelotBaseMesh.h"
#include "OcelotDXMaterial.h"

namespace Ocelot
{
	class OcelotDXMesh : public AOcelotBaseMesh
	{
	protected:

			// Attributes
			EffPtr  m_effect;
			TecPtr  m_fxTechnique;
			LayPtr  m_inputLayout;
			BufPtr  m_vertexBuffer;
			BufPtr  m_indiceBuffer;
			MatPtr  m_fxWorld;  // will let the GPU compute
			MatPtr  m_fxView;   // the world/view/projection.
			MatPtr  m_fxProj;
			MatPtr  m_fxTextureTransform;

			// Constructor
			OcelotDXMesh();

	public:

			// Destructor
	virtual ~OcelotDXMesh();

			// Methods per frame
	virtual VVOID  update(FFLOAT deltaTime);
	virtual BIGINT updateEffectVariables();
	virtual BIGINT renderMesh();

			// Methods to use once.
	virtual BIGINT createInstance();
	virtual BIGINT initializeLayout();
	virtual BIGINT initializeEffect();
	virtual BIGINT initializeVertices();
	virtual BIGINT initializeIndices();
	virtual VVOID  initializeEffectVariables();
	virtual VVOID  releaseInstance();

			// Accessors
	inline  BufPtr VBuffer() const { return m_vertexBuffer;}
	inline  VVOID  SetVBuffer(BufPtr newBuf) { m_vertexBuffer = newBuf;}
	};
}

#endif
