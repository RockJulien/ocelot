#ifndef DEF_SUBSET_H
#define DEF_SUBSET_H

// Includes
#include "DataStructures\VectorExt.h"
#include "DataStructures\DataStructureUtil.h"

// Namespaces 
namespace Ocelot
{
	// Class definition
	class Subset
	{
	public:

		// Typedefs
		typedef VectorExt<Subset*> SubsetTable;

	private:

		// Attributes
		UBIGINT mId;
		UBIGINT mVertexStart;
		UBIGINT mVertexCount;
		UBIGINT mFaceStart;
		UBIGINT mFaceCount;

	public:

		// Constructors & Destructor
		Subset();
		Subset(UBIGINT pId, UBIGINT pVertexStart, UBIGINT pVertexCount, UBIGINT pFaceStart, UBIGINT pFaceCount);
		~Subset();

		// Accessors
		inline UBIGINT ID() const { return mId; }
		inline UBIGINT VertexStart() const { return mVertexStart; }
		inline UBIGINT VertexCount() const { return mVertexCount; }
		inline UBIGINT FaceStart() const { return mFaceStart; }
		inline UBIGINT FaceCount() const { return mFaceCount; }
		inline VVOID   SetID(UBIGINT pId) { mId = pId; }
		inline VVOID   SetVertexStart(UBIGINT pVStart) { mVertexStart = pVStart; }
		inline VVOID   SetVertexCount(UBIGINT pVCount) { mVertexCount = pVCount; }
		inline VVOID   SetFaceStart(UBIGINT pFStart) { mFaceStart = pFStart; }
		inline VVOID   SetFaceCount(UBIGINT pFCount) { mFaceCount = pFCount; }
	};
}

#endif