#include "Grid.h"
#include "..\ResourceManager\ResourceFactory.h"
#include "..\Collisions\AABB.h"

using namespace Ocelot;
using namespace std;

Grid::Grid() :
OcelotDXMesh(), mInfo(), m_fCellWidth(1.0f), m_fCellDepth(1.0f),
m_fxLight(NULL), m_fxFog(NULL), m_fxAmbient(NULL), m_fxDiffuse(NULL), m_fxSpecular(NULL),
m_fxWorldInvTrans(NULL), m_fxEyePosition(NULL)
{
	// load textures.
	BumpMaterial lMat;
	ResFactory->LoadBumpMaterial(this->mInfo.TexturePath(), lMat);
	this->mInfo.SetMaterial(lMat);
}

Grid::Grid(const GridInfo& inf) :
OcelotDXMesh(), mInfo(inf), m_fCellWidth(1.0f), m_fCellDepth(1.0f),
m_fxLight(NULL), m_fxFog(NULL), m_fxAmbient(NULL), m_fxDiffuse(NULL), m_fxSpecular(NULL),
m_fxWorldInvTrans(NULL), m_fxEyePosition(NULL)
{
	// load textures.
	BumpMaterial lMat;
	ResFactory->LoadBumpMaterial(this->mInfo.TexturePath(), lMat);
	this->mInfo.SetMaterial(lMat);

	Vector3 lPosition = this->mInfo.Position();
	this->SetPosition(lPosition.getX(), lPosition.getY(), lPosition.getZ());
}

Grid::~Grid()
{
	releaseInstance();
}

VVOID   Grid::update(FFLOAT deltaTime)
{
	// refresh variables in the shader file
	HR(updateEffectVariables());

	OcelotDXMesh::update(deltaTime);
}

BIGINT  Grid::updateEffectVariables()
{
	HRESULT hr = S_OK;

	// Compute the world inverse transpose matrix.
	Matrix4x4 matV, matP, matWorldInvTrans;
	matV = mInfo.GetCamera()->View();
	matP = mInfo.GetCamera()->Proj();
	m_matWorld.Matrix4x4Inverse(matWorldInvTrans);
	matWorldInvTrans.Matrix4x4Transpose(matWorldInvTrans);

	// set mesh's attributes to the shader's attributes.
	HR(m_fxLight->SetRawValue(mInfo.Light(), 0, sizeof(OcelotLight)));
	HR(m_fxFog->SetRawValue(mInfo.Fog(), 0, sizeof(OcelotFog)));
	HR(m_fxWorld->SetMatrix((FFLOAT*)&m_matWorld));
	HR(m_fxView->SetMatrix((FFLOAT*)&matV));
	HR(m_fxProj->SetMatrix((FFLOAT*)&matP));
	HR(m_fxWorldInvTrans->SetMatrix((FFLOAT*)&matWorldInvTrans));
	HR(m_fxEyePosition->SetRawValue(&mInfo.GetCamera()->Info().Eye(), 0, sizeof(Vector3)));
	HR(m_fxAmbient->SetResource(mInfo.Material().Ambient()));
	HR(m_fxDiffuse->SetResource(mInfo.Material().Diffuse()));
	HR(m_fxSpecular->SetResource(mInfo.Material().Specular()));

	if
		(this->mHasTextureTransform)
	{
		HR(m_fxTextureTransform->SetMatrix((FFLOAT*)&mTextureTransform));
	}

	return (BIGINT)hr;
}

BIGINT  Grid::renderMesh()
{
	// set the input layout and buffers
	mInfo.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UBIGINT stride = sizeof(EnhancedVertex);
	UBIGINT offset = 0;
	mInfo.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	mInfo.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	mInfo.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if (m_bVisible)
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for (UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			mInfo.Device()->DrawIndexed(m_iNbIndices, 0, 0);
		}
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT  Grid::initializeLayout()
{
	HRESULT hr = S_OK;

	// Define the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D10_INPUT_PER_VERTEX_DATA, 0 },
	};
	UBIGINT nbElements = sizeof(layout) / sizeof(layout[0]);

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(mInfo.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

	return (BIGINT)hr;
}

BIGINT  Grid::initializeEffect()
{
	return initializeDXEffect(mInfo.Device(), mInfo.EffectPath(), &m_effect);
}

BIGINT  Grid::initializeVertices()
{
	HRESULT hr = S_OK;

	this->m_iNbVertices = this->mInfo.RowCount() * this->mInfo.ColCount();

	vector<EnhancedVertex> lVertices(this->m_iNbVertices);

	//
	// Create the vertices.
	//
	FFLOAT lHalfWidth = 0.5f * this->mInfo.TerrainWidth();
	FFLOAT lHalfDepth = 0.5f * this->mInfo.TerrainDepth();

	FFLOAT lDx = this->mInfo.TerrainWidth() / (this->mInfo.ColCount() - 1);
	FFLOAT lDz = this->mInfo.TerrainDepth() / (this->mInfo.RowCount() - 1);

	FFLOAT lDu = 1.0f / (this->mInfo.ColCount() - 1);
	FFLOAT lDv = 1.0f / (this->mInfo.RowCount() - 1);

	for 
		( UBIGINT lCurrRow = 0; lCurrRow < this->mInfo.RowCount(); lCurrRow++ )
	{
		FFLOAT z = lHalfDepth - lCurrRow * lDz;
		for 
			( UBIGINT lCurrColumn = 0; lCurrColumn < this->mInfo.ColCount(); lCurrColumn++ )
		{
			FFLOAT x = -lHalfWidth + lCurrColumn * lDx;

			lVertices[lCurrRow * this->mInfo.ColCount() + lCurrColumn].Position = Vector3(x, 0.0f, z);
			lVertices[lCurrRow * this->mInfo.ColCount() + lCurrColumn].Normal   = Vector3(0.0f, 1.0f, 0.0f);
			lVertices[lCurrRow * this->mInfo.ColCount() + lCurrColumn].Tangent  = Vector3(1.0f, 0.0f, 0.0f);

			// Stretch texture over grid.
			lVertices[lCurrRow * this->mInfo.ColCount() + lCurrColumn].TexCoord.x(lCurrColumn * lDu);
			lVertices[lCurrRow * this->mInfo.ColCount() + lCurrColumn].TexCoord.y(lCurrRow * lDv);
		}
	}

	// store extremities for BB.
	FFLOAT sizeTempX = mInfo.TerrainWidth() * 0.5f;
	FFLOAT sizeTempZ = mInfo.TerrainDepth() * 0.5f;
	Vector3 P1 = Vector3(-sizeTempX, 0.0f, -sizeTempZ);
	Vector3 P2 = Vector3(sizeTempX, 0.0f, sizeTempZ);

	this->mDrawableBounds = new AABBMinMax3D(P1, P2);
	this->mDrawableBounds->CreateBounds(mInfo.Device(), mInfo.GetCamera());

	// now transfert vertices in the vertex buffer
	D3D10_BUFFER_DESC VBDesc;
	VBDesc.Usage = D3D10_USAGE_IMMUTABLE;  // change with dynamic if it could change on the fly
	VBDesc.ByteWidth = sizeof(EnhancedVertex) * this->m_iNbVertices;
	VBDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	VBDesc.CPUAccessFlags = 0;
	VBDesc.MiscFlags = 0;

	// subresource creation
	D3D10_SUBRESOURCE_DATA  InitData;
	InitData.pSysMem = &lVertices[0];

	// create vertex buffer
	HR(mInfo.Device()->CreateBuffer(&VBDesc, &InitData, &m_vertexBuffer));

	return (BIGINT)hr;
}

BIGINT  Grid::initializeIndices()
{
	HRESULT hr = S_OK;

	UBIGINT lFaceCount = (this->mInfo.RowCount() - 1) * (this->mInfo.ColCount() - 1) * 2;
	this->m_iNbIndices = lFaceCount * 3;

	//
	// Create the indices.
	//
	vector<UBIGINT> lIndices(this->m_iNbIndices); // 3 indices per face

	// Iterate over each quad and compute indices.
	UBIGINT lIndex = 0;
	for 
		( UBIGINT lCurrRow = 0; lCurrRow < this->mInfo.RowCount() - 1; lCurrRow++ )
	{
		for 
			( UBIGINT lCurrColumn = 0; lCurrColumn < this->mInfo.ColCount() - 1; lCurrColumn++ )
		{
			lIndices[lIndex]     = lCurrRow * this->mInfo.ColCount() + lCurrColumn;
			lIndices[lIndex + 1] = lCurrRow * this->mInfo.ColCount() + lCurrColumn + 1;
			lIndices[lIndex + 2] = (lCurrRow + 1) * this->mInfo.ColCount() + lCurrColumn;

			lIndices[lIndex + 3] = (lCurrRow + 1) * this->mInfo.ColCount() + lCurrColumn;
			lIndices[lIndex + 4] = lCurrRow * this->mInfo.ColCount() + lCurrColumn + 1;
			lIndices[lIndex + 5] = (lCurrRow + 1) * this->mInfo.ColCount() + lCurrColumn + 1;

			lIndex += 6; // next quad
		}
	}

	// now transfert indices in the index buffer
	D3D10_BUFFER_DESC IBDesc;
	IBDesc.Usage = D3D10_USAGE_IMMUTABLE;  // change with dynamic if it could change on the fly
	IBDesc.ByteWidth = sizeof(UBIGINT) * this->m_iNbIndices;
	IBDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	IBDesc.CPUAccessFlags = 0;
	IBDesc.MiscFlags = 0;

	// subresource creation
	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = &lIndices[0];

	// create the index buffer
	HR(mInfo.Device()->CreateBuffer(&IBDesc, &InitData, &m_indiceBuffer));

	return (BIGINT)hr;
}

VVOID   Grid::initializeEffectVariables()
{
	m_fxTechnique = m_effect->GetTechniqueByName("BumpMapping");
	m_fxWorld = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView  = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj  = m_effect->GetVariableByName("gProjection")->AsMatrix();
	m_fxWorldInvTrans = m_effect->GetVariableByName("gWorldInvTrans")->AsMatrix();
	m_fxLight = m_effect->GetVariableByName("gLight");
	m_fxFog   = m_effect->GetVariableByName("gFog");
	m_fxEyePosition = m_effect->GetVariableByName("gEyePosW");
	m_fxAmbient  = m_effect->GetVariableByName("gTexture")->AsShaderResource();
	m_fxDiffuse  = m_effect->GetVariableByName("gTextureNor")->AsShaderResource();
	m_fxSpecular = m_effect->GetVariableByName("gTextureSpe")->AsShaderResource();

	if (this->mHasTextureTransform)
	{
		m_fxTextureTransform = m_effect->GetVariableByName("gSunTextureTransform")->AsMatrix();
	}
}

VVOID   Grid::releaseInstance()
{
	// release every COM of DirectX
	OcelotDXMesh::releaseInstance();

	DeletePointer(this->mWorldBounds);
	DeletePointer(this->mDrawableBounds);

	m_fxAmbient = NULL;
	m_fxDiffuse = NULL;
	m_fxSpecular = NULL;
	m_fxWorldInvTrans = NULL;
	m_fxLight = NULL;
	m_fxFog = NULL;
	m_fxEyePosition = NULL;
}

VVOID   Grid::RefreshWorldBounds()
{
	DeletePointer(this->mWorldBounds);

	FFLOAT sizeTempX = mInfo.TerrainWidth() * 0.5f;
	FFLOAT sizeTempZ = mInfo.TerrainDepth() * 0.5f;
	Vector3 lMaxL = Vector3(sizeTempX, 0.0f, sizeTempZ);
	Vector3 lMinL = Vector3(-sizeTempX, 0.0f, -sizeTempZ);

	Matrix4x4 lWorld = this->mDrawableBounds->GetWorld();

	Vector4 lMaxH = lWorld.Transform(Vector4(lMaxL.getX(), lMaxL.getY(), lMaxL.getZ(), 1.0f));
	Vector4 lMinH = lWorld.Transform(Vector4(lMinL.getX(), lMinL.getY(), lMinL.getZ(), 1.0f));

	this->mWorldBounds = new AABBMinMax3D( Vector3( lMinH.getX(), lMinH.getY(), lMinH.getZ() ),
										   Vector3( lMaxH.getX(), lMaxH.getY(), lMaxH.getZ() ) );
}

/**********************************************************************************************************************************************************/
GridInfo::GridInfo() :
BaseDXData(), m_sEffectPath(L"..\\Effects\\BumpMapping.fx"), m_sTexturePath(L".\\Resources\\StealWoodBox.jpg"), m_vPosition(0.0f),
m_light(NULL), m_fog(NULL), m_nbRows(10), m_nbCols(10), m_terrainWidth(100.0f), m_terrainDepth(100.0f)
{

}

GridInfo::GridInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, OcelotLight* light, OcelotFog* fog, Vector3 position, UBIGINT pNbRows, UBIGINT pNbCols, FFLOAT pTerrainWidth, FFLOAT pTerrainDepth) :
BaseDXData(device, camera), m_sTexturePath(texturePath), m_sEffectPath(effectPath), m_light(light), m_fog(fog), m_vPosition(position), m_nbRows(pNbRows), m_nbCols(pNbCols), m_terrainWidth(pTerrainWidth), m_terrainDepth(pTerrainDepth)
{

}

GridInfo::~GridInfo()
{

}
