#ifndef DEF_TERRAIN_H
#define DEF_TERRAIN_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Terrain.h/cpp
/   
/  Description: This object is to create a terrain or simply a
/               plateform or floor.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    19/02/2012
/*****************************************************************/

#include "OcelotDXMesh.h"
#include "OcelotLight.h"
#include "OcelotFog.h"

namespace Ocelot
{
									class TerrainInfo : public BaseDXData
									{
									private:

										OcelotLight*    m_light;
										OcelotFog*      m_fog;
										TriPlanarBumpMaterial* m_material;
										UBIGINT         m_nbRows;
										UBIGINT         m_nbCols;
										FFLOAT          m_terrainWidth;
										FFLOAT			m_terrainDepth;
										WSTRING         m_sEffectPath;
										Vector3         m_vPosition;

									public:

										// Constructor & Destructor
										TerrainInfo();
										TerrainInfo(DevPtr device, CamPtr camera, WSTRING effectPath, OcelotLight* light, OcelotFog* fog, TriPlanarBumpMaterial* pMaterial, Vector3 position, UBIGINT pNbRows, UBIGINT pNbCols, FFLOAT pTerrainWidth, FFLOAT pTerrainDepth);
										~TerrainInfo();

										// Methods


										// Accessors
										inline OcelotLight*    Light() const { return m_light; }
										inline OcelotFog*      Fog() const { return m_fog; }
										inline TriPlanarBumpMaterial* Material() const { return m_material; }
										inline UBIGINT         RowCount() const { return m_nbRows; }
										inline UBIGINT         ColCount() const { return m_nbCols; }
										inline FFLOAT          TerrainWidth() const { return m_terrainWidth; }
										inline FFLOAT		   TerrainDepth() const { return m_terrainDepth; }
										inline WSTRING         EffectPath() const { return m_sEffectPath; }
										inline Vector3         Position() const { return m_vPosition; }
										inline VVOID           SetLight(OcelotLight* light) { m_light = light; }
										inline VVOID           SetFog(OcelotFog* fog) { m_fog = fog; }
										inline VVOID           SetMaterial(TriPlanarBumpMaterial* material) { m_material = material; }
										inline VVOID           RowCount(UBIGINT pRowCount) { m_nbRows = pRowCount; }
										inline VVOID           ColCount(UBIGINT pColCount) { m_nbCols = pColCount; }
										inline VVOID           TerrainWidth(FFLOAT pWidth) { m_terrainWidth = pWidth; }
										inline VVOID 		   TerrainDepth(FFLOAT pDepth) { m_terrainDepth = pDepth; }
										inline VVOID           SetEffectPath(WSTRING effPath) { m_sEffectPath = effPath; }
										inline VVOID           SetPosition(Vector3 position) { m_vPosition = position; }
									};

	class Terrain : public OcelotDXMesh
	{
	private:

		// Attributes
		TerrainInfo				    mInfo;
		FFLOAT						m_fCellWidth;
		FFLOAT                      m_fCellDepth;
		FFLOAT						m_fHeightRange;
		FFLOAT						m_fHighestHeight;
		FFLOAT						m_fLowestHeight;
		VarPtr						m_fxLight;
		VarPtr						m_fxFog;
		MatPtr						m_fxWorldInvTrans;
		VarPtr						m_fxEyePosition;
		ScaPtr						m_fxTorchOn;
		WSTRING                     mHeightMapName;
		std::vector<FFLOAT>         m_fHeightMap;
		BBOOL                       m_bHeightMapUse;
		std::vector<VoxelVertex>    m_vertices;

		TexPtr						m_fxTextureArray;

		VVOID RefreshWorldBounds();

	public:

		// Constructor & Destructor
		Terrain();
		Terrain(TerrainInfo inf);
		~Terrain();

		// Methods per frame.
		VVOID  update(FFLOAT deltaTime);
		BIGINT updateEffectVariables();
		BIGINT renderMesh();

		// Methods
		BIGINT  initializeLayout();
		BIGINT  initializeEffect();
		BIGINT  initializeVertices();
		BIGINT  initializeIndices();
		VVOID   initializeEffectVariables();
		VVOID   loadHeightMap(const WSTRING& pFileName);  // just for an outdoor terrain
		VVOID   smoothTerrain();                          // just for an outdoor terrain
		FFLOAT  averageHeight(UINT row, UINT col);        // just for an outdoor terrain
		BBOOL   inTerrain(UINT row, UINT col);            // just for an outdoor terrain
		FFLOAT  heightAtPosition(const FFLOAT posX, const FFLOAT posZ) const ; // for walking accross the terrain
		Vector3 normalAtPosition(const FFLOAT posX, const FFLOAT posZ) const;
		VVOID   releaseInstance();

		// Accessors
		inline TerrainInfo& Info() { return mInfo;}
		inline FFLOAT    getTerrainWidth() const { return this->mInfo.TerrainWidth(); }
		inline FFLOAT    getTerrainDepth() const { return this->mInfo.TerrainDepth(); }
		inline FFLOAT	 getCellWidth() const { return m_fCellWidth;}
		inline FFLOAT	 getCellDepth() const { return m_fCellDepth;}
		inline BBOOL     isHeightMapUse() const { return m_bHeightMapUse;}
		inline VVOID     setInfo(const TerrainInfo& pInfo) { mInfo = pInfo; }
		inline VVOID     setHeightMapUse(BBOOL use) { m_bHeightMapUse = use;}
		inline VVOID     setHeightMapName(const WSTRING& pHeightMapName) { mHeightMapName = pHeightMapName; }
	};
}

#endif