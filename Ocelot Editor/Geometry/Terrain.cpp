#include "Terrain.h"
#include <Windows.h>
#include "..\ResourceManager\ResourceFactory.h"
#include "..\Collisions\AABB.h"

using namespace Ocelot;
using namespace std;

// default constructor (no device)
Terrain::Terrain() : 
OcelotDXMesh(), mInfo(), m_fCellWidth(1.0f), m_fCellDepth(1.0f), m_fHeightRange(1.0f), m_fHighestHeight(0.0f), 
m_fLowestHeight(0.0f), m_fxLight(NULL), m_fxFog(NULL),
mHeightMapName(L""), m_fHeightMap(), m_bHeightMapUse(false), 
m_vertices(), m_fxWorldInvTrans(NULL), m_fxEyePosition(NULL), m_fxTorchOn(NULL)
{
	// tempo
	m_particle->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
}

// Constructor overload
Terrain::Terrain(TerrainInfo inf) :
OcelotDXMesh(), mInfo(inf), m_fCellWidth(1.0f), m_fCellDepth(1.0f), m_fHeightRange(1.0f), m_fHighestHeight(0.0f),
m_fLowestHeight(0.0f), m_fxLight(NULL), m_fxFog(NULL),
mHeightMapName(L""), m_fHeightMap(), m_bHeightMapUse(false),
m_vertices(), m_fxWorldInvTrans(NULL), m_fxEyePosition(NULL), m_fxTorchOn(NULL)
{
	Vector3 lPosition = this->mInfo.Position();
	this->SetPosition(lPosition.getX(), lPosition.getY(), lPosition.getZ());
	m_particle->SetInverseMass(0.0f);
}

// Destructor
Terrain::~Terrain()
{
	releaseInstance();
}

// update matrices and set dynamic variables through
// the pipeline
VVOID Terrain::update(FFLOAT deltaTime)
{
	// refresh variables in the shader file
	HR(updateEffectVariables());

	OcelotDXMesh::update(deltaTime);
}

// render the terrain
BIGINT  Terrain::renderMesh()
{
	HRESULT hr = S_OK;

	// set the input layout and buffers
	mInfo.Device()->IASetInputLayout(m_inputLayout);

	// vertex buffer
	UBIGINT stride = sizeof(VoxelVertex);
	UBIGINT offset = 0;
	mInfo.Device()->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// index buffer
	mInfo.Device()->IASetIndexBuffer(m_indiceBuffer, DXGI_FORMAT_R32_UINT, 0);

	// primitive topology
	mInfo.Device()->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	if (m_bVisible)
	{
		// render Mesh
		D3D10_TECHNIQUE_DESC techDesc;
		HR(m_fxTechnique->GetDesc(&techDesc));

		// for every pass in the shader file.
		for (UBIGINT p = 0; p < (UBIGINT)techDesc.Passes; p++)
		{
			HR(m_fxTechnique->GetPassByIndex(p)->Apply(0));
			mInfo.Device()->DrawIndexed(m_iNbIndices, 0, 0);
		}
	}

	return OcelotDXMesh::renderMesh();
}

// initialize terrain's vertices
BIGINT  Terrain::initializeVertices()
{
	HRESULT hr = S_OK;

	FFLOAT ref  = 1.0f;
	FFLOAT ref2 = 1000.0f;

	Vector3 P1, P2;

	// compute the number of vertices
	m_iNbVertices    = mInfo.RowCount() * mInfo.ColCount();

	// compute the size of a cell with th esize of the terrain
	m_fCellWidth = mInfo.TerrainWidth() / ((FFLOAT)(mInfo.ColCount() - 1));
	m_fCellDepth = mInfo.TerrainDepth() / ((FFLOAT)(mInfo.RowCount() - 1));

	m_vertices.resize(m_iNbVertices);

	// check if heightMap asked
	if(m_bHeightMapUse)
	{
		// load the heightMap
		loadHeightMap( ResFactory->cResourceFolderPath + this->mHeightMapName);

		// smooth the terrain
		smoothTerrain();
	}

	FFLOAT dU = 1.0f / (FFLOAT)mInfo.ColCount() - 1.0f;
	FFLOAT dV = 1.0f / (FFLOAT)mInfo.RowCount() - 1.0f;

	for (UBIGINT currRow = 0; currRow < mInfo.RowCount(); currRow++)
	{
		for (UBIGINT currCol = 0; currCol < mInfo.ColCount(); currCol++)
		{
			// compute index once for storing vertices
			UBIGINT index = currRow * mInfo.ColCount() + currCol;

			// position on X
			FFLOAT posX = (FFLOAT)((FFLOAT)currCol * m_fCellWidth + (-mInfo.TerrainWidth() * 0.5f));
			
			// position on Y
			FFLOAT posY = 0.0f;

			// load the height map values as Y components if heightMap mode.
			if(m_bHeightMapUse)
			{
				posY = m_fHeightMap[index];
			}

			// find the highest height of the terrain for bounding box generation
			// and multi-texturing as well.
			if(posY > ref)
			{
				ref = posY;
				m_fHighestHeight = posY;
			}

			// find the lowest height of the terrain for computing the HeightRange
			// later on for the multi-texturing process.
			if(posY < ref2)
			{
				ref2 = posY;
				m_fLowestHeight = posY;
			}
			
			// position on Z
			FFLOAT posZ = (FFLOAT)(-((FFLOAT)currRow * m_fCellDepth) + (mInfo.TerrainDepth() * 0.5f));

			// compute normal if the terrain stay flat
			if(!m_bHeightMapUse)
			{
				m_vertices[index].m_vNormal = Vector3(0.0f, 1.0f, 0.0f);
			}

			// texture coordinate on U
			FFLOAT TexU = (FFLOAT)currCol * dU;

			// texture coordinate on V
			FFLOAT TexV = (FFLOAT)currRow * dV;

			// store them in the array
			m_vertices[index].m_vPosition = Vector3(posX, posY, posZ);
			//m_vertices[index].SetTexCoord(Vector2(TexU, TexV));
		}
	}

	// compute the heightRange
	m_fHeightRange = m_fHighestHeight - m_fLowestHeight;

	// compute normals if deformations
	if(m_bHeightMapUse)
	{
		FFLOAT TwoCellWidthInv = 1.0f / (2.0f * m_fCellWidth);
		FFLOAT TwoCellDepthInv = 1.0f / (2.0f * m_fCellDepth);
		// create a temporary array for storing generated normal for then using it
		// for setting normals in the right order which are here shifted by 2.
		vector<VoxelVertex> tempVertData(m_iNbVertices);
		for (UBIGINT currCellZ = 2; currCellZ < (mInfo.RowCount() - 1); currCellZ++)
		{
			for (UBIGINT currCellX = 2; currCellX < (mInfo.ColCount() - 1); currCellX++)
			{
				FFLOAT t = m_fHeightMap[(currCellZ - 1) * mInfo.ColCount() + currCellX];
				FFLOAT b = m_fHeightMap[(currCellZ + 1) * mInfo.ColCount() + currCellX];
				FFLOAT l = m_fHeightMap[currCellZ * mInfo.ColCount() + currCellX - 1];
				FFLOAT r = m_fHeightMap[currCellZ * mInfo.ColCount() + currCellX + 1];

				// compute tangent to the surface on X and Z
				Vector3 tanZ(0.0f, (t - b) * TwoCellDepthInv, 1.0f);
				Vector3 tanX(1.0f, (r - l) * TwoCellWidthInv, 0.0f);

				Vector3 lNormal;
				lNormal = tanZ.Vec3CrossProduct(tanX);
				lNormal.Vec3Normalise();

				UBIGINT index = currCellZ * mInfo.ColCount() + currCellX;
				tempVertData[index].m_vNormal = lNormal;
				//vertices[currCellZ * m_iNbCols + currCellX].Normal = normal;
			}
		}

		// pass normals in the right array in the right order starting from 0.
		for
			( UBIGINT index = 0; index < m_iNbVertices; index++ )
		{
			Vector3 temp = tempVertData[index].m_vNormal;
			if(temp == Vector3(0.0f))
				m_vertices[index].m_vNormal = Vector3(0.0f, 1.0f, 0.0f);
			else
				m_vertices[index].m_vNormal = temp;
		}
	}

	// store extremities for BB.
	FFLOAT sizeTempX = mInfo.TerrainWidth() * 0.5f;
	FFLOAT sizeTempZ = mInfo.TerrainDepth() * 0.5f;
	P1 = Vector3(-sizeTempX, m_fHighestHeight, -sizeTempZ);
	P2 = Vector3(sizeTempX, m_fLowestHeight, sizeTempZ);

	this->mDrawableBounds = new AABBMinMax3D(P1, P2);
	this->mDrawableBounds->CreateBounds(mInfo.Device(), mInfo.GetCamera());

	// now transfert vertices in the vertex buffer
	D3D10_BUFFER_DESC VBDesc;
	VBDesc.Usage		  = D3D10_USAGE_IMMUTABLE;  // change with dynamic if it could change on the fly
	VBDesc.ByteWidth      = sizeof(VoxelVertex) * m_iNbVertices;
	VBDesc.BindFlags	  = D3D10_BIND_VERTEX_BUFFER;
	VBDesc.CPUAccessFlags = 0;
	VBDesc.MiscFlags      = 0;

	// subresource creation
	D3D10_SUBRESOURCE_DATA  InitData;
	InitData.pSysMem      = &m_vertices[0];

	// create vertex buffer
	HR(mInfo.Device()->CreateBuffer(&VBDesc, &InitData, &m_vertexBuffer));
	
	return (BIGINT)hr;
}

// initialize terrain's indices
BIGINT  Terrain::initializeIndices()
{
	HRESULT hr = S_OK;

	std::vector<UBIGINT> indices;

	// compute the number of indices
	UBIGINT nbCells     = (mInfo.RowCount() - 1) * (mInfo.ColCount() - 1);
	UBIGINT nbTriangles = nbCells * 2;
	m_iNbIndices     = nbTriangles * 3;

	indices.resize(m_iNbIndices, 0);

	int index = 0;
	for (UBIGINT currRow = 0; currRow < (mInfo.RowCount() - 1); currRow++)
	{
		for (UBIGINT currCol = 0; currCol < (mInfo.ColCount() - 1); currCol++)
		{
			// indices for a cell

			// first triangle
			indices[index]	   = currRow       * mInfo.ColCount() + currCol;
			indices[index + 1] = currRow       * mInfo.ColCount() + (currCol + 1);
			indices[index + 2] = (currRow + 1) * mInfo.ColCount() + currCol;

			// second triangle
			indices[index + 3] = (currRow + 1) * mInfo.ColCount() + currCol;
			indices[index + 4] = currRow       * mInfo.ColCount() + (currCol + 1);
			indices[index + 5] = (currRow + 1) * mInfo.ColCount() + (currCol + 1);
			
			index += 6;  // next cell
		}
	}

	// now transfert indices in the index buffer
	D3D10_BUFFER_DESC IBDesc;
	IBDesc.Usage		  = D3D10_USAGE_IMMUTABLE;  // change with dynamic if it could change on the fly
	IBDesc.ByteWidth      = sizeof(UBIGINT) * m_iNbIndices;
	IBDesc.BindFlags      = D3D10_BIND_INDEX_BUFFER;
	IBDesc.CPUAccessFlags = 0;
	IBDesc.MiscFlags      = 0;

	// subresource creation
	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem      = &indices[0];

	// create the index buffer
	HR(mInfo.Device()->CreateBuffer(&IBDesc, &InitData, &m_indiceBuffer));
	
	return (BIGINT)hr;
}

BIGINT  Terrain::initializeLayout()
{
	HRESULT hr = S_OK;

	// Define the input layout
	D3D10_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D10_INPUT_PER_VERTEX_DATA, 0 }
	};
	UBIGINT nbElements = sizeof(layout) / sizeof(layout[0]);

	D3D10_PASS_DESC PassDesc;
	HR(m_fxTechnique->GetPassByIndex(0)->GetDesc(&PassDesc));
	HR(mInfo.Device()->CreateInputLayout(layout, nbElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_inputLayout));

	return (BIGINT)hr;
}

// set the effect
BIGINT Terrain::initializeEffect()
{
	return initializeDXEffect(mInfo.Device(), mInfo.EffectPath(), &m_effect);
}

// initialize shader variables
VVOID Terrain::initializeEffectVariables()
{
	m_fxTechnique     = m_effect->GetTechniqueByName("TriPlanarTexturing");
	m_fxWorld		  = m_effect->GetVariableByName("gWorld")->AsMatrix();
	m_fxView		  = m_effect->GetVariableByName("gView")->AsMatrix();
	m_fxProj		  = m_effect->GetVariableByName("gProjection")->AsMatrix();
	m_fxWorldInvTrans = m_effect->GetVariableByName("gWorldInvTrans")->AsMatrix();
	m_fxEyePosition   = m_effect->GetVariableByName("gEyePosW");
	m_fxTorchOn       = m_effect->GetVariableByName("gTorcheOn")->AsScalar();
	m_fxLight	      = m_effect->GetVariableByName("gLight");
	m_fxFog			  = m_effect->GetVariableByName("gFog");
	m_fxTextureArray  = m_effect->GetVariableByName("gTexture")->AsShaderResource();

	if (this->mHasTextureTransform)
	{
		m_fxTextureTransform = m_effect->GetVariableByName("gSunTextureTransform")->AsMatrix();
	}
}

// transfert into the pipeline
BIGINT  Terrain::updateEffectVariables()
{

	HRESULT hr = S_OK;

	Matrix4x4 matV, matP, matWorldInvTrans;
	matV = mInfo.GetCamera()->View();
	matP = mInfo.GetCamera()->Proj();

	m_matWorld.Matrix4x4Inverse(matWorldInvTrans);
	matWorldInvTrans.Matrix4x4Transpose(matWorldInvTrans);

	HR(m_fxLight->SetRawValue(mInfo.Light(), 0, sizeof(OcelotLight)));
	HR(m_fxFog->SetRawValue(mInfo.Fog(), 0, sizeof(OcelotFog)));
	HR(m_fxWorld->SetMatrix((FFLOAT*)&m_matWorld));
	HR(m_fxView->SetMatrix((FFLOAT*)&matV));
	HR(m_fxProj->SetMatrix((FFLOAT*)&matP));
	HR(m_fxTorchOn->SetBool(false));
	HR(m_fxWorldInvTrans->SetMatrix((FFLOAT*)&matWorldInvTrans));
	HR(m_fxEyePosition->SetRawValue(&mInfo.GetCamera()->Info().Eye(), 0, sizeof(Vector3)));
	HR(m_fxTextureArray->SetResourceArray(mInfo.Material()->TextureArray(), 0, 9));

	if
		(this->mHasTextureTransform)
	{
		HR(m_fxTextureTransform->SetMatrix((FFLOAT*)&mTextureTransform));
	}

	return (BIGINT)hr;
}

// load a heightMap
VVOID    Terrain::loadHeightMap(const WSTRING& pFileName)
{
	UBIGINT lCount = mInfo.RowCount() * mInfo.ColCount();
	std::vector<unsigned char> input(lCount);

	std::ifstream inputStream;
	inputStream.open(pFileName, std::ios_base::binary);

	if(inputStream)
	{
		inputStream.read((char*)&input[0], (std::streamsize)input.size());
		inputStream.close();
	}

	m_fHeightMap.resize(lCount, 0.0f);
	for 
		( UBIGINT currHei = 0; currHei < lCount; currHei++ )
	{
		m_fHeightMap[currHei] = 0.35f * (FFLOAT)input[currHei] - 20.0f;
	}
}

// for smoothing the terrain
VVOID    Terrain::smoothTerrain()
{
	vector<FFLOAT> smoothy(m_fHeightMap.size(), 0.0f);

	for(UBIGINT currRow = 0; currRow < mInfo.RowCount(); currRow++)
	{
		for(UBIGINT currCol = 0; currCol < mInfo.ColCount(); currCol++)
		{
			FFLOAT newHeight = averageHeight(currRow, currCol);
			smoothy[(currRow * mInfo.ColCount()) + currCol] = newHeight;
		}
	}

	// if everything is okay, insert new heights
	m_fHeightMap = smoothy;
}

// for computing an average of terrain's heights
FFLOAT   Terrain::averageHeight(UBIGINT currRow, UBIGINT currCol)
{
	FFLOAT heightAverage = 0.0f;
	UBIGINT  nbNeighBors   = 0;

	for(UBIGINT m = currRow-1; m <= currRow+1; m++)
	{
		for(UBIGINT currentCol = currCol-1; currentCol <= currCol+1; currentCol++)
		{
			if(inTerrain(m, currentCol))  // check if the point is in the heightMap, so the terrain.
			{
				heightAverage += m_fHeightMap[(m * mInfo.ColCount()) + currentCol];
				nbNeighBors++;
			}
		}
	}

	// make the average between all summed value and the number of value
	return heightAverage / (FFLOAT)nbNeighBors;
}

// for checking if pixel are in terrain's bounds
BBOOL    Terrain::inTerrain(UBIGINT row, UBIGINT col)
{
	return row >= 0 && row < mInfo.RowCount() && col >= 0 && col < mInfo.ColCount();
}

// for taking back terrain's height allowing to walk
// accross it properly
FFLOAT   Terrain::heightAtPosition(FFLOAT posX, FFLOAT posZ) const
{
	FFLOAT heightAtPos;

	if(m_bHeightMapUse)
	{
		// determine the cell space coordinates
		FFLOAT c = (posX + (0.5f * mInfo.TerrainWidth())) / m_fCellWidth;
		FFLOAT d = (posZ - (0.5f * mInfo.TerrainDepth())) / -m_fCellDepth;

		// get the row and the column where the player is.
		int row = (int)floorf(d);
		int col = (int)floorf(c);

		// take back the height where the player is
		FFLOAT TopLeftCorner	 = m_fHeightMap[row * mInfo.ColCount() + col];
		FFLOAT TopRightCorner    = m_fHeightMap[row * mInfo.ColCount() + col + 1];
		FFLOAT BottomLeftCorner  = m_fHeightMap[(row + 1) * mInfo.ColCount() + col];
		FFLOAT BottomRightCorner = m_fHeightMap[(row + 1) * mInfo.ColCount() + col + 1];

		// now we know the cell we are, let's find in which triangle
		// of the cell we are as well
		FFLOAT s = c - (FFLOAT)col;
		FFLOAT t = d - (FFLOAT)row;

		// if upper triangle
		if(s + t <= 1.0f)
		{
			FFLOAT Uy = TopRightCorner - TopLeftCorner;
			FFLOAT Vy = BottomLeftCorner - TopLeftCorner;

			return TopLeftCorner + s * Uy + t * Vy;
		}
		else // lower triangle
		{
			FFLOAT Uy = BottomLeftCorner - BottomRightCorner;
			FFLOAT Vy = TopRightCorner - BottomRightCorner;

			return BottomRightCorner + (1.0f - s) * Uy + (1.0f - t) * Vy;
		}

	}
	else
	{
		// if flat terrain always return same value
		// so we return the world Y coordinate of the terrain.
		heightAtPos = m_matWorld.get_42();
	}

	return heightAtPos;
}

Vector3 Terrain::normalAtPosition(const FFLOAT posX, const FFLOAT posZ) const
{
	FFLOAT col = floor((posX + (0.5f * mInfo.TerrainWidth())) / m_fCellWidth);
	FFLOAT row = floor((posZ - (0.5f * mInfo.TerrainDepth())) / -m_fCellDepth);

	return m_vertices[(int)col * mInfo.ColCount() + (int)row].m_vNormal;
}

VVOID   Terrain::releaseInstance()
{
	// release every COM of DirectX
	OcelotDXMesh::releaseInstance();

	DeletePointer(this->mWorldBounds);
	DeletePointer(this->mDrawableBounds);

	m_fxLight = NULL;
	m_fxFog = NULL;
	m_fxWorldInvTrans = NULL;
	m_fxEyePosition = NULL;
	m_fxTorchOn = NULL;
	m_fxTextureArray = NULL;
}

VVOID   Terrain::RefreshWorldBounds()
{
	DeletePointer(this->mWorldBounds);

	FFLOAT sizeTempX = mInfo.TerrainWidth() * 0.5f;
	FFLOAT sizeTempZ = mInfo.TerrainDepth() * 0.5f;
	Vector3 lMaxL = Vector3(sizeTempX, m_fLowestHeight, sizeTempZ);
	Vector3 lMinL = Vector3(-sizeTempX, m_fHighestHeight, -sizeTempZ);

	Matrix4x4 lWorld = this->mDrawableBounds->GetWorld();

	Vector4 lMaxH = lWorld.Transform(Vector4(lMaxL.getX(), lMaxL.getY(), lMaxL.getZ(), 1.0f));
	Vector4 lMinH = lWorld.Transform(Vector4(lMinL.getX(), lMinL.getY(), lMinL.getZ(), 1.0f));

	this->mWorldBounds = new AABBMinMax3D( Vector3( lMinH.getX(), lMinH.getY(), lMinH.getZ() ),
										   Vector3( lMaxH.getX(), lMaxH.getY(), lMaxH.getZ() ) );
}

/********************************************************************************************************************/
TerrainInfo::TerrainInfo() :
BaseDXData(), m_sEffectPath(L"..\\Effects\\BumpMapping.fx"), m_vPosition(0.0f), m_material(NULL),
m_light(NULL), m_fog(NULL), m_nbRows(10), m_nbCols(10), m_terrainWidth(100.0f), m_terrainDepth(100.0f)
{

}

TerrainInfo::TerrainInfo(DevPtr device, CamPtr camera, WSTRING effectPath, OcelotLight* light, OcelotFog* fog, TriPlanarBumpMaterial* pMaterial, Vector3 position, UBIGINT pNbRows, UBIGINT pNbCols, FFLOAT pTerrainWidth, FFLOAT pTerrainDepth) :
BaseDXData(device, camera), m_sEffectPath(effectPath), m_light(light), m_fog(fog), m_vPosition(position), m_material(pMaterial), m_nbRows(pNbRows), m_nbCols(pNbCols), m_terrainWidth(pTerrainWidth), m_terrainDepth(pTerrainDepth)
{

}

TerrainInfo::~TerrainInfo()
{

}