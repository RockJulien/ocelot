#ifndef DEF_BASEMESHDATA_H
#define DEF_BASEMESHDATA_H

/*****************************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   BaseMeshData.h/cpp
/   
/  Description: This object is a container structure for 
/               DirectX useful dat for drawing 3D Objects.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    30/09/2012
/*****************************************************************************/

#include "..\DataStructures\DataStructureUtil.h"
#include "..\Camera\OcelotCamera.h"
#include "..\DirectXAPI\DXUtility.h"

namespace Ocelot
{
	class BaseDXData
	{
	protected:

				// Attributes
				DevPtr m_device;
				CamPtr m_camera;

	public:

				// Constructor & Destructor
				BaseDXData();
				BaseDXData(DevPtr device, CamPtr camera);
		virtual ~BaseDXData();

		// Accessors
		inline DevPtr Device() const { return m_device;}
		inline CamPtr GetCamera() const { return m_camera;}
		inline VVOID  SetDevice(DevPtr device) { m_device = device;}
		inline VVOID  SetCamera(CamPtr camera) { m_camera = camera;}
	};
}

#endif
