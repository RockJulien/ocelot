#ifndef DEF_MESHGEOMETRY_H
#define DEF_MESHGEOMETRY_H

// Includes
#include "OcelotDXMesh.h"
#include "Subset.h"
#include "OcelotLight.h"
#include "OcelotFog.h"

// Namespaces
namespace Ocelot
{
								class MeshInfo : public BaseDXData
								{
								public:

									// Typedefs
									typedef Subset::SubsetTable		  MeshSubsetTable;
									typedef VectorExt<EnhancedVertex> MeshVertices;
									typedef VectorExt<UBIGINT>        MeshIndices;
									typedef VectorExt<OBJMaterial*>   MeshMaterials;

								private:

									MeshSubsetTable mSubsetTable;
									MeshVertices    mVertices;
									MeshIndices		mIndices;
									MeshMaterials   mMaterials;
									OcelotLight*    mLight;
									OcelotFog*      mFog;
									WSTRING         mEffectPath;
									Vector3         mPosition;

								public:

									MeshInfo();
									MeshInfo(DevPtr pDevice, CamPtr pCamera, OcelotLight* pLight, OcelotFog* pFog, const WSTRING& pEffectPath, const Vector3& pPosition);
									~MeshInfo();

									// Methods
									VVOID ReleaseData();

									// Accessors
									inline OcelotLight* Light() const { return mLight; }
									inline OcelotFog*   Fog() const { return mFog; }
									inline WSTRING      EffectPath() const { return mEffectPath; }
									inline Vector3      Position() const { return mPosition; }
									inline const MeshVertices&    Vertices() const { return mVertices; }
									inline const MeshIndices&     Indices() const { return mIndices; }
									inline const MeshSubsetTable& SubsetTable() const { return mSubsetTable; }
									inline const MeshMaterials&   Materials() const { return mMaterials; }
									inline VVOID		SetLight(OcelotLight* pLight) { mLight = pLight; }
									inline VVOID        SetFog(OcelotFog* pFog) { mFog = pFog; }
									inline VVOID        SetEffectPath(const WSTRING& pEffectPath) { mEffectPath = pEffectPath; }
									inline VVOID        SetPosition(const Vector3& pPosition) { mPosition = pPosition; }
									inline VVOID		SetVertices(const MeshVertices& pVertices) { this->mVertices = pVertices; }
									inline VVOID		SetIndices(const MeshIndices& pIndices) { this->mIndices = pIndices; }
									inline VVOID		SetSubsetTable(const MeshSubsetTable& pSubsetTable) { this->mSubsetTable = pSubsetTable; }
									inline VVOID		SetMaterials(const MeshMaterials& pMaterials) { this->mMaterials = pMaterials; }
								};

	// Class definition
	class MeshGeometry : public OcelotDXMesh
	{
	private:

				// Attributes
				MeshInfo   mInfo;
				MatPtr     m_fxWorldInvTrans;
				TexPtr     m_fxTexture;
				VarPtr     m_fxLight;
				VarPtr     m_fxFog;
				VarPtr     m_fxEyePosition;
				VecPtr	   mMaterialAmbient;
				VecPtr	   mMaterialDiffuse;
				VecPtr	   mMaterialSpecular;
				ScaPtr	   mMaterialAlpha;
				ScaPtr	   mMaterialShininess;

				// Private methods
				VVOID RefreshWorldBounds();
				VVOID RenderSubset(UBIGINT pSubset);

	public:

				// Constructors & Destructors
				MeshGeometry(const MeshInfo& pMeshInfo);
				~MeshGeometry();

				// Methods per frame.
				VVOID  update(FFLOAT deltaTime);
				BIGINT updateEffectVariables();
				BIGINT renderMesh();

				// Methods to use once.
				BIGINT initializeLayout();
				BIGINT initializeEffect();
				BIGINT initializeVertices();
				BIGINT initializeIndices();
				VVOID  initializeEffectVariables();
				VVOID  releaseInstance();

				// Accessors
		inline  MeshInfo& Info() { return this->mInfo; }

	};
}

#endif