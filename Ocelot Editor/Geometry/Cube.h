#ifndef DEF_CUBE_H
#define DEF_CUBE_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Cube.h/cpp
/   
/  Description: This file provides a class for creating boxes,
/               walls or planes by changing the Radius Vec on
/               the according axis. The mesh will do bump mapping
/               on the surface, so make sure that the texture
/               path is the same for the three needed texture in
/               "resources" but adding _NRM and _SPEC to proper
/               textures (Normal map and Spec map) before the
/               extension.
/               (e.g. test.jpg      => diffuse.
/                     test_NRM.jpg  => ambient.
/                     test_SPEC.jpg => specular.)
/               The loadBumpMaterial() in DXHelper will do the rest.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    03/10/2012
/*****************************************************************/

#include "OcelotDXMesh.h"
#include "OcelotLight.h"
#include "OcelotFog.h"

namespace Ocelot
{

						class CubeInfo : public BaseDXData
						{
						private:

							// Attributes
							OcelotLight*    m_light;
							OcelotFog*      m_fog;
							BumpMaterial    m_material;
							WSTRING         m_sEffectPath;
							WSTRING         m_sTexturePath;
							Vector3         m_vPosition;
							Vector3         m_vRadius;

						public:

							// Constructor & Destructor
							CubeInfo();
							CubeInfo(DevPtr device, CamPtr camera, WSTRING texturePath, WSTRING effectPath, OcelotLight* light, OcelotFog* fog, Vector3 position, Vector3 radius = Vector3(1.0f));
							~CubeInfo();

							// Methods


							// Accessors
							inline OcelotLight*    Light() const { return m_light;}
							inline OcelotFog*      Fog() const { return m_fog;}
							inline BumpMaterial    Material() const { return m_material;}
							inline WSTRING         TexturePath() const { return m_sTexturePath;}
							inline WSTRING         EffectPath() const { return m_sEffectPath;}
							inline Vector3         Position() const { return m_vPosition;}
							inline Vector3         Radius() const { return m_vRadius;}
							inline VVOID           SetLight(OcelotLight* light) { m_light = light;}
							inline VVOID           SetFog(OcelotFog* fog) { m_fog = fog;}
							inline VVOID           SetMaterial(BumpMaterial material) { m_material = material;}
							inline VVOID           SetTexturePath(WSTRING texPath) { m_sTexturePath = texPath;}
							inline VVOID           SetEffectPath(WSTRING effPath) { m_sEffectPath = effPath;}
							inline VVOID           SetPosition(Vector3 position) { m_vPosition = position;}
							inline VVOID           SetRadius(Vector3 radius) { m_vRadius = radius;}
						};

	class Cube : public OcelotDXMesh
	{
	private:

		// Accessors
		CubeInfo   m_info;
		TexPtr     m_fxAmbient;
		TexPtr     m_fxDiffuse;
		TexPtr     m_fxSpecular;
		MatPtr     m_fxWorldInvTrans;
		VarPtr     m_fxLight;
		VarPtr     m_fxFog;
		VarPtr     m_fxEyePosition;

		// Private Methods
		VVOID calculateNormalAverage(const Vector3& origine, const Vector3& X, const Vector3& Y, const Vector3& Z, Vector3& out);
		VVOID RefreshWorldBounds();

	public:

		// Constructor & Destructor
		Cube();
		Cube(CubeInfo info);
		~Cube();

		// Methods per frame.
		VVOID  update(FFLOAT deltaTime);
		BIGINT updateEffectVariables();
		BIGINT renderMesh();

		// Methods to use once.
		BIGINT initializeLayout();
		BIGINT initializeEffect();
		BIGINT initializeVertices();
		BIGINT initializeIndices();
		VVOID  initializeEffectVariables();
		VVOID  releaseInstance();

		// Accessors
		inline CubeInfo Info() const { return m_info;}
		inline VVOID    SetInfo(CubeInfo info) { m_info = info;}
	};
}

#endif