#ifndef DEF_CLASSATTRIBUTETYPE_H
#define DEF_CLASSATTRIBUTETYPE_H

/*****************************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   ClassAttributeType.h
/   
/  Description: This file contains only an enumeration for
/               attribute of a classes which cannot be 
/               accessed when using an interface from which
/               other classes inherit and will be used for 
/               creating instances.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    30/09/2012
/*****************************************************************************/

namespace Ocelot
{
	enum ClassAttributeType
	{
		CLASS_ATTRIBUTETYPE_DEVICE,
		CLASS_ATTRIBUTETYPE_CAMERA,
		CLASS_ATTRIBUTETYPE_SCALE,
		CLASS_ATTRIBUTETYPE_RADIUSPLUS,
		CLASS_ATTRIBUTETYPE_HEIGHT,
		CLASS_ATTRIBUTETYPE_POSITION,
		CLASS_ATTRIBUTETYPE_EFFECTPATH,
		CLASS_ATTRIBUTETYPE_TEXTUREPATH,
		CLASS_ATTRIBUTETYPE_VERTEXBUFFER,
		CLASS_ATTRIBUTETYPE_TESSELATION,
		CLASS_ATTRIBUTETYPE_SPEED,
		CLASS_ATTRIBUTETYPE_LINE_FROM,
		CLASS_ATTRIBUTETYPE_LINE_TO,
		CLASS_ATTRIBUTETYPE_LINE_COLOR,
		CLASS_ATTRIBUTETYPE_CIRCLE_SLICES,
		CLASS_ATTRIBUTETYPE_CYLINDER_RINGS,
		CLASS_ATTRIBUTETYPE_LIGHT,
		CLASS_ATTRIBUTETYPE_MATERIAL,
		CLASS_ATTRIBUTETYPE_CIRCLE_AXIS
	};
}

#endif