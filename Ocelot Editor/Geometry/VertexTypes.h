#ifndef DEF_VERTEXTYPES_H
#define DEF_VERTEXTYPES_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   VertexTypes.h/cpp
/   
/  Description: This file provides multiple class for vertices 
/               of different kind of objects and effect intended.
/               Going from simple shape colored with simple Colors
/               to more complex ones with normal and tangent for 
/               Phong shading processing for example.
/               Particular structure for particles is provided
/               as well.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

#include "..\Maths\Color.h"
#include "..\Maths\Vector3.h"
#include "..\Maths\Vector2.h"
#include "..\DataStructures\DataStructureUtil.h"

namespace Ocelot
{
	// Special vertex class for CubeMap
	// which could serve as Base vertex
	// class.
	class DomeVertex
	{
	protected:

			// Attributes
			Vector3 m_vPosition;

	public:

			// Constructor & Destructor
			DomeVertex();
			DomeVertex(Vector3 position);
			DomeVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ);
	virtual ~DomeVertex();

			// Methods


			// Accessors
	inline  Vector3 Position() const { return m_vPosition;}
	inline  VVOID   SetPosition(Vector3 position) { m_vPosition = position;}
	};

	// Vertex structure with only position
	// and color components.
	struct BasicVertex
	{
		// constructors
		BasicVertex(){}
		BasicVertex(Vector3 pos, Color col) :
			Position(pos), Color(col){}
		BasicVertex(float posX, float posY, float posZ, Color col) :
			Position(posX, posY, posZ), Color(col){}
		BasicVertex(float posX, float posY, float posZ, float red, float green, float blue, float alpha) :
			Position(posX, posY, posZ), Color(red, green, blue, alpha){}

		// attributes
		Vector3 Position;
		Color   Color;
	};

	// Simple enhanced vertex class with position, 
	// normal and texture coordinates allowing 
	// basic lighting and texturization.
	class SimpleVertex : public DomeVertex
	{
	protected:

			// Attributes
			Vector3 m_vNormal;
			Vector2 m_vTexCoord;

	public:

			// Constructor & Destructor
			SimpleVertex();
			SimpleVertex(Vector3 position, Vector3 normal, Vector2 texCoord);
			SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 normal, Vector2 texCoord);
			SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ, Vector2 texCoord);
			SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ, FFLOAT texU, FFLOAT texV);
	virtual ~SimpleVertex();

			// Methods


			// Accessors
	inline  Vector3 Normal() const { return m_vNormal;}
	inline  Vector2 TexCoord() const { return m_vTexCoord;}
	inline  VVOID   SetNormal(Vector3 normal) { m_vNormal = normal;}
	inline  VVOID   SetTexCoord(Vector2 texCoord) { m_vTexCoord = texCoord;}
	};

	// Particular vertex class for Particules.
	// Can be used for Fire, Rain, Snow, etc...
	// Extracted from the example of Franck LUNA.
	struct LightParticleVertex
	{
		// constructors
		LightParticleVertex(){}
		LightParticleVertex(Vector3 pos, Vector3 vel, Vector2 scale, FFLOAT age, BIGINT type) :
			Position(pos), Velocity(vel), Scale(scale), Age(age), Type(type){}
		LightParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 vel, Vector2 scale, FFLOAT age, BIGINT type) :
			Position(posX, posY, posZ), Velocity(vel), Scale(scale), Age(age), Type(type){}
		LightParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT velX, FFLOAT velY, FFLOAT velZ, Vector2 scale, FFLOAT age, BIGINT type) :
			Position(posX, posY, posZ), Velocity(velX, velY, velZ), Scale(scale), Age(age), Type(type){}
		LightParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT velX, FFLOAT velY, FFLOAT velZ, FFLOAT width, FFLOAT height, FFLOAT age, BIGINT type) :
			Position(posX, posY, posZ), Velocity(velX, velY, velZ), Scale(width, height), Age(age), Type(type){}

		// attributes
		Vector3 Position;
		Vector3 Velocity;
		Vector2 Scale;
		FFLOAT  Age;
		BIGINT  Type;
	};

	// Enhanced vertex structure with position, normal,
	// tangent and texture coordinates allowing
	// bump mapping enhancing lighting and texturing.
	struct EnhancedVertex
	{
		// constructors
		EnhancedVertex(){}
		EnhancedVertex(Vector3 pos, Vector3 norm, Vector3 tang, Vector2 tex) :
		Position(pos), Normal(norm), Tangent(tang), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, Vector3 norm, Vector3 tan, Vector2 tex) :
		Position(posX, posY, posZ), Normal(norm), Tangent(tan), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, Vector3 tan, Vector2 tex) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tan), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float tanX, float tanY, float tanZ, Vector2 tex) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tanX, tanY, tanZ), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float tanX, float tanY, float tanZ, float texU, float texV) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tanX, tanY, tanZ), TexCoord(texU, texV){}

		// attributes
		Vector3 Position;
		Vector3 Normal;
		Vector3 Tangent;
		Vector2 TexCoord;
	};

	// Particle vertex structure.
	struct ParticleVertex
	{
		// Constructors.
		ParticleVertex(){}
		ParticleVertex(Vector3 position, Vector2 tex, Vector2 lifeNRot, UBIGINT color) :
		Position(position), TexCoord(tex), LifeNRot(lifeNRot), Color(color) {}
		ParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector2 tex, Vector2 lifeNRot, UBIGINT color) :
		Position(posX, posY, posZ), TexCoord(tex), LifeNRot(lifeNRot), Color(color) {}
		ParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT texU, FFLOAT texV, Vector2 lifeNRot, UBIGINT color) :
		Position(posX, posY, posZ), TexCoord(texU, texV), LifeNRot(lifeNRot), Color(color) {}
		ParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT texU, FFLOAT texV, FFLOAT life, FFLOAT rot, UBIGINT color) :
		Position(posX, posY, posZ), TexCoord(texU, texV), LifeNRot(life, rot), Color(color) {}

		// Attributes.
		Vector3 Position;
		Vector2 TexCoord;
		Vector2 LifeNRot;
		UBIGINT Color;
	};

	struct VoxelVertex
	{
		VoxelVertex() :
		m_vPosition(0.0f), m_vNormal(1.0f) {}
		VoxelVertex(Vector3 position, Vector3 normal) :
		m_vPosition(position), m_vNormal(normal) {}
		VoxelVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 normal) :
		m_vPosition(posX, posY, posZ), m_vNormal(normal) {}
		VoxelVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ) :
		m_vPosition(posX, posY, posZ), m_vNormal(normX, normY, normZ) {}

		Vector3 m_vPosition;
		Vector3 m_vNormal;
	};
}

#endif