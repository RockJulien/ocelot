#include "VertexTypes.h"

using namespace Ocelot;

DomeVertex::DomeVertex() :
m_vPosition(0.0f)
{

}

DomeVertex::DomeVertex(Vector3 position) :
m_vPosition(position)
{

}

DomeVertex::DomeVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ) :
m_vPosition(posX, posY, posZ)
{

}

DomeVertex::~DomeVertex()
{

}

SimpleVertex::SimpleVertex() :
DomeVertex(), m_vNormal(0.0f), m_vTexCoord(0.0f)
{

}

SimpleVertex::SimpleVertex(Vector3 position, Vector3 normal, Vector2 texCoord) :
DomeVertex(position), m_vNormal(normal), m_vTexCoord(texCoord)
{

}

SimpleVertex::SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 normal, Vector2 texCoord) :
DomeVertex(posX, posY, posZ), m_vNormal(normal), m_vTexCoord(texCoord)
{

}

SimpleVertex::SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ, Vector2 texCoord) :
DomeVertex(posX, posY, posZ), m_vNormal(normX, normY, normZ), m_vTexCoord(texCoord)
{

}

SimpleVertex::SimpleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ, FFLOAT texU, FFLOAT texV) :
DomeVertex(posX, posY, posZ), m_vNormal(normX, normY, normZ), m_vTexCoord(texU, texV)
{

}

SimpleVertex::~SimpleVertex()
{
	
}
