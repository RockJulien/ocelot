#include "OcelotDXMesh.h"

using namespace Ocelot;

OcelotDXMesh::OcelotDXMesh() :
AOcelotBaseMesh(), m_effect(NULL), m_fxTechnique(NULL), m_inputLayout(NULL),
m_vertexBuffer(NULL), m_indiceBuffer(NULL), m_fxWorld(NULL), m_fxView(NULL), m_fxProj(NULL)
{

}

OcelotDXMesh::~OcelotDXMesh()
{
	
}

VVOID  OcelotDXMesh::update(FFLOAT deltaTime)
{
	// Add extra stuff

	return AOcelotBaseMesh::update(deltaTime);
}

BIGINT OcelotDXMesh::updateEffectVariables()
{
	// do not know for now
	return (BIGINT)S_OK;
}

BIGINT OcelotDXMesh::renderMesh()
{
	// Add extra stuff

	return AOcelotBaseMesh::renderMesh();
}

BIGINT OcelotDXMesh::createInstance()
{
	HRESULT hr = S_OK;

	/**************************************Initialize D3D*****************************************/
	// create effect object
	HR(initializeEffect());

	// initialize effect Variables
	initializeEffectVariables();

	/**************************************Initialize World***************************************/
	// create the input layout
	HR(initializeLayout());

	// compute vertices
	HR(initializeVertices());

	// compute indices
	HR(initializeIndices());

	return (BIGINT)hr;
}

BIGINT OcelotDXMesh::initializeLayout()
{
	// do not know for now
	return (BIGINT)S_OK;
}

BIGINT OcelotDXMesh::initializeEffect()
{
	return (BIGINT)S_OK;
}

BIGINT OcelotDXMesh::initializeVertices()
{
	// do not know for now
	return (BIGINT)S_OK;
}

BIGINT OcelotDXMesh::initializeIndices()
{
	// do not know for now
	return (BIGINT)S_OK;
}

VVOID  OcelotDXMesh::initializeEffectVariables()
{
	// do not know for now
}

VVOID  OcelotDXMesh::releaseInstance()
{
	// release every COM of DirectX
	ReleaseCOM(m_effect);
	ReleaseCOM(m_inputLayout);
	ReleaseCOM(m_vertexBuffer);
	ReleaseCOM(m_indiceBuffer);
	m_fxTechnique = NULL;
	m_fxWorld     = NULL;
	m_fxView      = NULL;
	m_fxProj      = NULL;

	AOcelotBaseMesh::releaseInstance();
}
