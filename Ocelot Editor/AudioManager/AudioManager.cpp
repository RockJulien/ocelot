#include "AudioManager.h"

using namespace Ocelot;
using namespace Audio;
using namespace std;

AudioManager::~AudioManager()
{

}

AudioManager* AudioManager::instance()
{
	static AudioManager instance;

	return &instance;
}

VVOID    AudioManager::initialize(HINSTANCE hInst)
{
	m_hInst = hInst;
}

HRESULT AudioManager::createSound(const wstring& shortName, BBOOL b3DSound, BBOOL bLoop)
{
	for(UINT currName = 0; currName < m_sNames.size(); currName++)
		if(m_sNames[currName] == shortName)
			return S_OK; // already exist !!!

	// else create an audio object.
	OcelotAudio* HoplaBoom = new OcelotAudio(m_hInst);

	if(FAILED(HoplaBoom->createDevice()))
		return E_FAIL;

	// Get the pointer on that new Device.
	IOcelotAudioDevice* dev = HoplaBoom->getDevice();
	if(!dev)
		return E_FAIL;

	// then, init the audio device.
	if(FAILED(dev->initialize(b3DSound)))
		return E_FAIL;

	// and load the sound ready to be played.
	if(FAILED(dev->loadSound(wstringToCharPointer(shortName.c_str()), bLoop)))
		return E_FAIL;

	// if everything is OKAY, store everything in its array accordingly.
	m_sNames.push_back(shortName);
	m_pAudios.push_back(HoplaBoom);
	m_pDevices.push_back(dev);

	return S_OK;
}

HRESULT AudioManager::update(FFLOAT deltaTime)
{
	HRESULT hr = S_OK;

	for(UINT currSound = 0; currSound < m_pDevices.size(); currSound++)
	{
		if(m_pDevices[currSound]->isRunning())
		{
			hr = m_pDevices[currSound]->playSound(0, deltaTime);
			if(FAILED(hr))
				return hr;
		}
	}

	return hr;
}

HRESULT AudioManager::startASound(wstring shortName)
{
	HRESULT hr = S_OK;

	for(UINT currSound = 0; currSound < m_sNames.size(); currSound++)
	{
		if(m_sNames[currSound] == shortName)
		{
			hr = m_pDevices[currSound]->startSound();
			if(FAILED(hr))
				return hr;
		}
	}

	return hr;
}

HRESULT AudioManager::restartASound(wstring shortName)
{
	HRESULT hr = S_OK;

	for(UINT currSound = 0; currSound < m_sNames.size(); currSound++)
	{
		if(m_sNames[currSound] == shortName)
		{
			hr = m_pDevices[currSound]->trigger();
			if(FAILED(hr))
				return hr;
		}
	}

	return hr;
}

VVOID    AudioManager::stopASound(wstring shortName)
{
	for(UINT currSound = 0; currSound < m_sNames.size(); currSound++)
	{
		if(m_sNames[currSound] == shortName)
			m_pDevices[currSound]->stopSound();
	}
}

HRESULT AudioManager::setReverbForAll(ReverbEffect effect)
{
	HRESULT hr = S_OK;

	for(UINT currSound = 0; currSound < m_pDevices.size(); currSound++)
	{
		hr = m_pDevices[currSound]->setReverb(effect);
		if(FAILED(hr))
			return hr;
	}

	return hr;
}

HRESULT AudioManager::setReverbForOne(ReverbEffect effect, wstring shortName)
{
	HRESULT hr = S_OK;

	for(UINT currSound = 0; currSound < m_sNames.size(); currSound++)
	{
		if(m_sNames[currSound] == shortName)
			hr = m_pDevices[currSound]->setReverb(effect);
	}

	return hr;
}

CCHAR*   AudioManager::wstringToCharPointer(const wchar_t *lpwstr)
{
	// compute the length of the wstring for resizing the array of CCHAR.
	BIGINT size = WideCharToMultiByte(CP_UTF8, 0, lpwstr, -1, NULL, 0, NULL, NULL);

	// prepare the array and store the result in it.
	CCHAR* buffer = new CCHAR[ size+1];
	WideCharToMultiByte(CP_UTF8, 0, lpwstr, -1, buffer, size, NULL, NULL);

	// return the result.
	return buffer;
}

VVOID    AudioManager::release()
{
	for(UINT currDevice = 0; currDevice < m_pAudios.size(); currDevice++)
	{
		if(m_pAudios[currDevice])
		{
			delete m_pAudios[currDevice];
			m_pAudios[currDevice] = NULL;
		}
	}

	m_sNames.clear();
	m_pAudios.clear();
	m_pDevices.clear();
}
