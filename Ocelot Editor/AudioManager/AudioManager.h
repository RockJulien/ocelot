#ifndef DEF_AUDIOMANAGER_H
#define DEF_AUDIOMANAGER_H

#include "..\Utilities\OcelotUtility.h"
#include "OcelotAudio.h"

#if defined(DEBUG) || defined(_DEBUG)
#pragma comment(lib, "..\\Debug\\OcelotAudio.lib")
#else
#pragma comment(lib, "..\\Release\\OcelotAudio.lib")
#endif

#define AudioMng Ocelot::AudioManager::instance()

namespace Ocelot
{
	class AudioManager
	{
	public:

	typedef std::vector<Audio::OcelotAudio*>        Channels;
	typedef std::vector<Audio::IOcelotAudioDevice*> Musics;
	typedef std::vector<std::wstring>				Names;

	private:

			// Attributes
			// Audio Device.
			HINSTANCE m_hInst;
			Channels  m_pAudios;
			Musics    m_pDevices;
			Names     m_sNames;

			// Constructor
			AudioManager(){}

			AudioManager(const AudioManager&);
			AudioManager& operator = (const AudioManager&);

	public:

			// Destructor
			~AudioManager();

			// Singleton
	static  AudioManager* instance();

			// Methods
			VVOID    initialize(HINSTANCE);
			HRESULT  createSound(const std::wstring&, BBOOL, BBOOL);
			HRESULT  update(FFLOAT);
			HRESULT  startASound(std::wstring shortName);
			HRESULT  restartASound(std::wstring shortName);
			VVOID    stopASound(std::wstring shortName);
			HRESULT  setReverbForAll(Audio::ReverbEffect);
			HRESULT  setReverbForOne(Audio::ReverbEffect, std::wstring shortName);
			CCHAR*    wstringToCharPointer(const wchar_t *lpwstr);
			VVOID    release();

			// Accessors
	inline  Musics Device() const { return m_pDevices;}
	};
}

#endif