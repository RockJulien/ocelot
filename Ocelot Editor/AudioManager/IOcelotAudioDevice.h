#ifndef DEF_IOCELOTAUDIODEVICE_H
#define DEF_IOCELOTAUDIODEVICE_H

#include <windows.h>
#include "RevEnum.h"
#include <stdio.h>
#include "AudioUtilities.h"

// Forward declarations
class Vector3;
class Vector4;

// Namespaces
namespace Audio
{
	// Audio Interface definition
	class IOcelotAudioDevice
	{
	protected:

			// Attributes
			HWND      m_hMainWindow;
			HINSTANCE m_hDllMod;
			BBOOL      m_bRunning;
			BBOOL      m_bLogFileOpen;
			FILE*     m_pLog;

	public:

			// Constructor & Destructor
			IOcelotAudioDevice(){}
	virtual ~IOcelotAudioDevice(){}

			// Methods
	virtual HRESULT initialize(BBOOL)                   = 0;  // for initializing the device
	virtual HRESULT startSound()                        = 0;  // for stopping all sounds
	virtual HRESULT loadSound(const CCHAR*, BBOOL)       = 0;  // load a sound
	virtual HRESULT playSound(UINT, FFLOAT delta)       = 0;  // play a loaded sound
	virtual VVOID    stopSound()                        = 0;  // stop a sound's playing
	virtual HRESULT setReverb(ReverbEffect)             = 0;  // set the reverb effect
	virtual HRESULT trigger()                           = 0;  // trig a non looped sound
	virtual VVOID    setListener(Vector3 pos, Vector3 dir, Vector3 up, Vector3 v) = 0;  // set listener parameter in 3D world
	virtual VVOID    setSoundPosition(Vector3, UINT)    = 0;            // set parameters for sound source.
	virtual VVOID    setSoundDirection(Vector3, Vector3 v, UINT)     = 0;
	virtual VVOID    setSoundMaxDistance(const FFLOAT& dist, UINT s) = 0;
	virtual VVOID    release()                          = 0;  // for releasing the device

			// Accessors
	inline BBOOL isRunning() const { return m_bRunning;}
	};

	extern "C"
	{
		HRESULT createAudioDevice(HINSTANCE hDllMod, IOcelotAudioDevice** pInterface);
		typedef HRESULT (*CREATEAUDIODEVICE)(HINSTANCE hDllMod, IOcelotAudioDevice** pInterface);

		HRESULT releaseAudioDevice(IOcelotAudioDevice** pInterface);
		typedef HRESULT (*RELEASEAUDIODEVICE)(IOcelotAudioDevice** pInterface);
	}
}

#endif