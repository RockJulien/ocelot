#ifndef DEF_TIMER_H
#define DEF_TIMER_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Timer.h/.cpp
/   
/  Description: This header file allows to create a timer
/               for any game and provides us the famous
/               deltaTime so useful for time-dependant 
/               animations.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

#include <Windows.h>
#include <iostream>
#include "..\Maths\Utility.h"

class Timer
{

private:

	_int64 m_baseTime;
	_int64 m_pauseTime;
	_int64 m_stopTime;
	_int64 m_currTime;
	_int64 m_prevTime;

	DFLOAT m_secPerCount;
	DFLOAT m_deltaTime;

	BBOOL m_stopped;

public:

	Timer();

	VVOID resetTimer();   // called before your msg loop
	VVOID startTimer();   // called when unpaused
	VVOID stopTimer();    // called when paused
	VVOID tick();         // called every frame

	FFLOAT getGameTime() const;   // in sec
	FFLOAT getDeltaTime() const;  // in sec

};

#endif