#include "Timer.h"

Timer::Timer(): m_secPerCount(0.0), m_deltaTime(-1.0), m_baseTime(0), m_pauseTime(0), m_prevTime(0), m_currTime(0), m_stopped(false)
{
	_int64 countsPerSec;  // needed to create my secPerCount
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
	m_secPerCount = 1.0 / (DFLOAT)countsPerSec;
}

VVOID Timer::tick()
{
	if( m_stopped)
	{
		m_deltaTime = 0.0;
		return;
	}

	// get the time of the frame
	_int64 currentTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);
	m_currTime = currentTime;

	// here is the delta between two frame
	m_deltaTime = (m_currTime - m_prevTime)*m_secPerCount;

	// Prepare for the next frame
	m_prevTime = m_currTime;

	if(m_deltaTime < 0.0)
	{
		m_deltaTime = 0.0;
	}
}

FFLOAT Timer::getDeltaTime() const
{
	return (FFLOAT)m_deltaTime;
}

VVOID Timer::resetTimer()
{
	_int64 currentTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);

	m_baseTime = currentTime;
	m_prevTime = currentTime;
	m_stopTime = 0;
	m_stopped  = false;
}

VVOID Timer::stopTimer()
{

	// if we are already stopped, don't do anything
	if(!m_stopped)
	{
		_int64 currentTime;
		QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);

		// otherwise, save the time we stopped at, and put the boolean to true.
		m_stopTime = currentTime;
		m_stopped = true;
	}

}

VVOID Timer::startTimer()
{

	_int64 startTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&startTime);

	// Accumulate the time elapsed between stop and start pairs
	//
	//			    |<-----d-------->|
	// -------------*----------------*------------------> time
	//          m_stopTime         startTime

	// if we are resuming the timer from a stopped state
	if (m_stopped)
	{
		// then accumulate the paused time
		m_pauseTime += (startTime - m_stopTime);

		// Since we are starting the timer back up, the previous is not valid because of pause mode
		// so we have to reset it to the current value
		m_prevTime = startTime;

		// reset stop variables
		m_stopTime = 0;
		m_stopped  = false;
	}
}

FFLOAT Timer::getGameTime() const
{

	// if we're stopped, don't count the time that has passed since we stopped
	//
	//--------*-----------------*--------------------------------*---------> time
	//     m_baseTime        m_stopTime                      m_currTime

	if(m_stopped)
	{
		return (FFLOAT)((m_stopTime - m_baseTime)* m_secPerCount);
	}

	// the distance m_currTime - m_baseTime includes pauseTime,
	// which we don't want to count. To correct this, we can substract
	// pauseTime from m_currTime
	//
	//  (m_currTime - m_pauseTime) - m_baseTime
	//
	//                        |<--------d--------->|
	//--------*---------------*--------------------*--------------*--------> time
	//      m_baseTime     m_stopTime          startTime      m_currTime

	else
	{
		return (FFLOAT)(((m_currTime - m_pauseTime)- m_baseTime)* m_secPerCount);
	}
}
