#ifndef DEF_DXHELPERFUNCTIONS_H
#define DEF_DXHELPERFUNCTIONS_H

#include "..\DataStructures\DataStructureUtil.h"
#include "..\StringHelper\STRINGHelpers.h"
#include "..\DirectXAPI\DXUtility.h"

namespace Ocelot
{
	// load a 2D texture as a DirectX resource view texture.
	inline ResViewPtr loadTexture(DevPtr pDevice, const WSTRING& pTexturePath)
	{
		ID3D10ShaderResourceView* newOne = NULL;

		if 
			(pTexturePath.size() != 0)
		{
			STRING charPath;
			toAString(pTexturePath, charPath);

			// create it and store it.
			ID3D10Texture2D* texture2D = ResMng::getTexture2DFromFile(pDevice, charPath.c_str());

			if(texture2D != NULL)
			{
				D3D10_TEXTURE2D_DESC desc;
				texture2D->GetDesc(&desc);

				D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
				ZeroMemory(&SRVDesc, sizeof(SRVDesc));

				SRVDesc.Format				= desc.Format;
				SRVDesc.ViewDimension       = D3D10_SRV_DIMENSION_TEXTURE2D;
				SRVDesc.Texture2D.MipLevels = desc.MipLevels;

				HR(pDevice->CreateShaderResourceView(texture2D, &SRVDesc, &newOne));

				// release 2DTexture
				texture2D->Release();
			}
		}

		return newOne;
	}

	inline ResViewPtr loadRV(DevPtr pDevice, const WSTRING& pTexturePath)
	{
		ID3D10ShaderResourceView* newOne = NULL;

		D3DX10CreateShaderResourceViewFromFile(pDevice, pTexturePath.c_str(), 0, 0, &newOne, NULL);

		return newOne;
	}

	// load a 2D texture as a DirectX cube map texture.
	inline ResViewPtr loadCubeMap(DevPtr pDevice, const WSTRING& pTexturePath)
	{
		ID3D10ShaderResourceView* newOne = NULL;

		if 
			(pTexturePath.size() != 0)
		{
			// create it and store it.
			// Particular way to load the texture for a cube map!!!
			// just set the flag to D3D10_RESOURCE_MISC_TEXTURECUBE.
			D3DX10_IMAGE_LOAD_INFO Info;
			Info.MiscFlags = D3D10_RESOURCE_MISC_TEXTURECUBE;

			ID3D10Texture2D* C_M = 0;
			HR(D3DX10CreateTextureFromFile(pDevice, 
										   pTexturePath.c_str(),
										   &Info, 
										   NULL, 
										   (ID3D10Resource**)&C_M, 
										   NULL));

			if(C_M != NULL)
			{
				D3D10_TEXTURE2D_DESC desc;
				C_M->GetDesc(&desc);

				D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
				ZeroMemory(&SRVDesc, sizeof(SRVDesc));

				SRVDesc.Format				= desc.Format;
				SRVDesc.ViewDimension       = D3D10_SRV_DIMENSION_TEXTURECUBE;
				SRVDesc.Texture2D.MipLevels = desc.MipLevels;

				HR(pDevice->CreateShaderResourceView(C_M, &SRVDesc, &newOne));

				// release 2DTexture
				C_M->Release();
			}
		}

		return newOne;
	}

	inline BIGINT     initializeDXEffect(DevPtr pDevice, const WSTRING& pEffectPath, EffPtr* pEffect)
	{
		HRESULT hr = S_OK;

		// Create the effect
		DWORD dwShaderFlags = D3D10_SHADER_ENABLE_STRICTNESS;

		#if defined( DEBUG ) || defined( _DEBUG )
		// Set the D3D10_SHADER_DEBUG flag to embed debug information in the shaders.
		dwShaderFlags |= D3D10_SHADER_DEBUG;
		#endif

		// Gather effect compilation errors
		ID3D10Blob *	pFXcompileErrors = NULL;

		hr = D3DX10CreateEffectFromFile(pEffectPath.c_str(), NULL, NULL, "fx_4_0", dwShaderFlags, 0, pDevice, NULL, NULL, pEffect, &pFXcompileErrors, NULL);
		if(FAILED(hr))
		{
			if(hr == D3D10_ERROR_FILE_NOT_FOUND)
					MessageBoxA(NULL, "The FX file cannot be located. Please run this executable from the parent of the directory (named 'Effects') which contains the FX file.", "ERROR", MB_OK);
			else
			{
				MessageBoxA(NULL, "Failed to compile the FX file.  Please debug the FX code.", "ERROR", MB_OK );

				#if defined(DEBUG) || defined(_DEBUG)
				// Display effect compile errors (if any) on debugging console
				if ( pFXcompileErrors != 0 && pFXcompileErrors->GetBufferPointer() != 0)
				{
					OutputDebugStringA("\n--------------------------------------------------------------------------\n");
					OutputDebugStringA("                           Effect Compilation Errors\n");
					OutputDebugStringA("--------------------------------------------------------------------------\n\n");
					OutputDebugStringA((CCHAR *)pFXcompileErrors->GetBufferPointer());
					OutputDebugStringA("\n--------------------------------------------------------------------------\n\n");
				}
				#endif // defined( DEBUG ) || defined( _DEBUG )
			}
			return (BIGINT)hr;
		} // end if failed

		return (BIGINT)hr;
	}
}

#endif