#ifndef DEF_DXOCELOTAPP_H
#define DEF_DXOCELOTAPP_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   DXOcelotApp.h/.cpp
/   
/  Description: This is a basic Windows DirectX framework from
/               which every concrete app will inherit.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    26/09/2011
/*****************************************************************/

#include <iostream>
#include "DXUtility.h"
#include "Timer.h"

#define RES_Width 800  // our window's resolution
#define RES_Height 700

class DXOcelotApp
{

protected:

		 // Declaration of global variables
		 ID3D10Device*           m_device;
		 ID3D10RenderTargetView* m_renderTargetView;
		 D3D10_VIEWPORT          m_viewPort;
		 ID3D10DepthStencilView* m_depthStencilView;
		 IDXGISwapChain*         m_swapChain;
		 ID3D10Texture2D*		 m_depthStencilBuff;
		 ID3DX10Font*			 m_font;
		 Timer                   m_timer;

		 // some boolean
		 BBOOL					 m_bPaused;
		 BBOOL					 m_bResize;
		 BBOOL				     m_bMinize;
		 BBOOL					 m_bMaxize;

		 std::wstring			 m_frameStat;

		 HWND					 m_mainWindowInstance;
		 HINSTANCE				 m_hAppInstance;
		 BIGINT                     m_iWindowWidth;
		 BIGINT                     m_iWindowHeight;

public:

	     // Constructor & Destructor
		 DXOcelotApp();
		 ~DXOcelotApp();

		 // Methods
		 VVOID initApp(HINSTANCE hInstance, BIGINT nShowCmd, LPCWSTR windowName);
		 VVOID init3DDevice();
virtual  VVOID update(FFLOAT deltaTime);
virtual  VVOID renderFrame();
virtual  VVOID clean3DDevice();
virtual  VVOID resize();
virtual  LRESULT AppProc(UINT message,WPARAM wParam,LPARAM lParam);

};

#endif
