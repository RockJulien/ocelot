#include "DXOcelotApp.h"

#include <sstream>

using namespace std;
using namespace Ocelot;

LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

	static DXOcelotApp* myApp = NULL;

	switch(msg)
	{
	    case WM_CREATE:
			CREATESTRUCT* cs = (CREATESTRUCT*)lParam;
			myApp = (DXOcelotApp*) cs->lpCreateParams;
			return 0;
	}

	if(myApp)
		return myApp->AppProc(msg, wParam, lParam);
	else 
		return DefWindowProc(hWnd, msg, wParam, lParam);

}

DXOcelotApp::DXOcelotApp() : m_bPaused(false), m_bResize(false), m_bMinize(false), m_bMaxize(false), m_iWindowWidth(RES_Width), m_iWindowHeight(RES_Height), m_device(NULL), m_renderTargetView(NULL), m_depthStencilView(NULL), m_swapChain(NULL), m_depthStencilBuff(NULL), m_font(NULL), m_mainWindowInstance(NULL)
{

}

DXOcelotApp::~DXOcelotApp()
{
	// destroy everything
}

VVOID DXOcelotApp::initApp(HINSTANCE hInstance, BIGINT nShowCmd, LPCWSTR windowName)
{
	 WNDCLASS wc;

	 m_hAppInstance = hInstance;

	 wc.style         = CS_HREDRAW | CS_VREDRAW;
	 wc.lpfnWndProc   = WindowProc;
	 wc.cbClsExtra    = 0;
	 wc.cbWndExtra    = 0;
	 wc.hInstance     = m_hAppInstance;
	 wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	 wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
	 wc.hbrBackground = (HBRUSH) GetStockObject(NULL_BRUSH);
	 wc.lpszMenuName  = 0;
	 wc.lpszClassName = L"STD";

	 RegisterClass(&wc);  // register the window class 

	 RECT R = { 0, 0, m_iWindowWidth, m_iWindowHeight };
     AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
	 BIGINT Width  = R.right - R.left;
	 BIGINT Height = R.bottom - R.top;

	 m_mainWindowInstance = CreateWindow(  L"STD",
								windowName,
								WS_OVERLAPPEDWINDOW,
								0,       // x pos
								0,	     // y pos
								Width,   // width
								Height,  // height
								NULL,
								NULL,
								m_hAppInstance,
								this);

	 // init the input device.
	 InfoInput lInfo;
	 lInfo.hInst = m_hAppInstance;
	 lInfo.hWnd  = m_mainWindowInstance;
	 lInfo.widthWindow  = m_iWindowWidth;
	 lInfo.heightWindow = m_iWindowHeight;
	 InMng->initialize( lInfo );

	// display the window on the screen
	ShowWindow(m_mainWindowInstance, nShowCmd);
	UpdateWindow(m_mainWindowInstance);

	// initialize our 3D device
	init3DDevice();
}

VVOID DXOcelotApp::init3DDevice()
{
	 HRESULT hr;
	 DXGI_SWAP_CHAIN_DESC scd;  // swapchain description

	 ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));                   // set the entire scd struct to NULL to prepare it

	 scd.BufferCount       = 1;
	 scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;               // UNORM means that the color go from 0.0 to 1.0 meaning that 0.0 is no color and 1.0 is full color
	 scd.BufferDesc.Width  = m_iWindowWidth;                                // width of the back buffer must be the same of the window
	 scd.BufferDesc.Height = m_iWindowHeight;                               // height idem
	 scd.BufferDesc.RefreshRate.Numerator   = 60;					   // freq 60Hz
	 scd.BufferDesc.RefreshRate.Denominator = 1;
	 scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	 scd.BufferDesc.Scaling          = DXGI_MODE_SCALING_UNSPECIFIED;
	 scd.BufferUsage       = DXGI_USAGE_RENDER_TARGET_OUTPUT;          // to be used as a render target
	 scd.OutputWindow      = m_mainWindowInstance;                                     // our window
	
	 // sampling
	 scd.SampleDesc.Count   = 1;                                        // 1 means that you'll have stairs on the draw, so aliasing.... basic render
	 scd.SampleDesc.Quality = 0;
	 
	 scd.Windowed          = true;
	 scd.SwapEffect        = DXGI_SWAP_EFFECT_DISCARD;
	 scd.Flags             = 0;

	 	UINT createDeviceFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)  
    createDeviceFlags |= D3D10_CREATE_DEVICE_DEBUG;
#endif

	 hr = D3D10CreateDeviceAndSwapChain(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, D3D10_SDK_VERSION, &scd, &m_swapChain, &m_device);
	 if(FAILED(hr)) return;

	 // fonction resize
	 resize();   // only because this part need to be in another part of the code for being able to resize everything when we resize the window

	 D3DX10_FONT_DESC fontDesc;
	 fontDesc.Height		  = 30;
	 fontDesc.Width			  = 0;
	 fontDesc.Weight		  = 0;
	 fontDesc.MipLevels		  = 1;
	 fontDesc.Italic		  = false;
	 fontDesc.CharSet		  = DEFAULT_CHARSET;
	 fontDesc.OutputPrecision = OUT_DEFAULT_PRECIS;
	 fontDesc.Quality         = DEFAULT_QUALITY;
	 fontDesc.PitchAndFamily  = DEFAULT_PITCH | FF_DONTCARE;
	 //wcscpy(fontDesc.FaceName, L"Times New Roman");

	 D3DX10CreateFontIndirect(m_device, &fontDesc, &m_font); 

}

VVOID DXOcelotApp::resize()
{
	 HRESULT hr;
	 ReleaseCOM(m_renderTargetView);
	 ReleaseCOM(m_depthStencilView);
	 ReleaseCOM(m_depthStencilBuff);

	 // get the address of the back buffer and use it to create the render target
	 hr = m_swapChain->ResizeBuffers(1, m_iWindowWidth, m_iWindowHeight, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
	 if(FAILED(hr)) return;

	 ID3D10Texture2D* pBackBuffer;
	 hr = m_swapChain->GetBuffer(0, __uuidof(ID3D10Texture2D), reinterpret_cast<UNKNOWN*>(&pBackBuffer));
	 if(FAILED(hr)) return;
	 hr = m_device->CreateRenderTargetView(pBackBuffer, NULL, &m_renderTargetView);
	 if(FAILED(hr)) return;
	 pBackBuffer->Release();

	 D3D10_TEXTURE2D_DESC dsd;                 // description of the depth/stencil buffer
	 dsd.Width		        = m_iWindowWidth;   // needs to be equal to the window size and other buffers
	 dsd.Height             = m_iWindowHeight;
	 dsd.MipLevels          = 1;
	 dsd.ArraySize          = 1;
	 dsd.Format             = DXGI_FORMAT_D24_UNORM_S8_UINT;
	 dsd.SampleDesc.Count   = 1;      // needs to be equal with swapchain values
	 dsd.SampleDesc.Quality = 0;	  // for multisampling
	 dsd.Usage		        = D3D10_USAGE_DEFAULT;
	 dsd.BindFlags          = D3D10_BIND_DEPTH_STENCIL;
	 dsd.CPUAccessFlags     = 0;
	 dsd.MiscFlags			= 0;

	 hr = m_device->CreateTexture2D(&dsd, 0, &m_depthStencilBuff);
	 if(FAILED(hr)) return;
	 hr = m_device->CreateDepthStencilView(m_depthStencilBuff, 0, &m_depthStencilView);
	 if(FAILED(hr)) return;

	 // set the render target as the back buffer
	 m_device->OMSetRenderTargets(1, &m_renderTargetView, m_depthStencilView);

	 // Let's set the viewport      // create the structure
	 ZeroMemory(&m_viewPort, sizeof(D3D10_VIEWPORT));         // prepare the space and set all to NULL

	 m_viewPort.TopLeftX = 0;
	 m_viewPort.TopLeftY = 0;
	 m_viewPort.Width    = m_iWindowWidth;       // it has to be equal to the size of the window
	 m_viewPort.Height   = m_iWindowHeight;
	 m_viewPort.MinDepth = 0.0f;
	 m_viewPort.MaxDepth = 1.0f;

	 m_device->RSSetViewports(1, &m_viewPort);   // set the viewport
}

VVOID DXOcelotApp::renderFrame()
{
	 // set the window with a blue
	m_device->ClearRenderTargetView(m_renderTargetView, DXColor::BLUE);  
	m_device->ClearDepthStencilView(m_depthStencilView, D3D10_CLEAR_DEPTH | D3D10_CLEAR_STENCIL, 1.0f, 0);
}

VVOID DXOcelotApp::clean3DDevice()
{
	// Unregister the window class.
	UnregisterClass(L"STD", m_hAppInstance);

	// switch to windowed mode (avoid to crash if the app is close when in full screen).
	if (m_swapChain)
		HR(m_swapChain->SetFullscreenState(false, NULL));

	 // Release COM
	 ReleaseCOM(m_device);									 // close & release the 3D Device
	 ReleaseCOM(m_renderTargetView);						 // close & release the render target view
	 ReleaseCOM(m_depthStencilView);
	 ReleaseCOM(m_swapChain);								 // close & release the swapchain
	 ReleaseCOM(m_depthStencilBuff);
	 ReleaseCOM(m_font);

	 // Release the input device.
	 if (InMng)
		 InMng->release();
}

VVOID DXOcelotApp::update(FFLOAT deltaTime)
{
	// code which computes the average of frames/s, and also the average time it takes to render one frame
	static BIGINT frameCnt = 0;
	static FFLOAT t_divi = 0.0f;

	frameCnt++;

	if((m_timer.getGameTime() - t_divi) >= 1.0f)
	{
		FFLOAT FPS = (FFLOAT)frameCnt;   // FPS = frameCnt/1
		FFLOAT milliSecPerFrame = 1000.0f / FPS;

		wostringstream outs;
		outs << L"Ocelot: " << L"frame/sec: " << FPS << L"   ||   ms/frame: "  << milliSecPerFrame;

		m_frameStat = outs.str();

		SetWindowText(m_mainWindowInstance, m_frameStat.c_str());

		frameCnt = 0;
		t_divi  += 1.0f;
	}

	// update the input device
	InMng->update();
}

LRESULT DXOcelotApp::AppProc(UINT message,WPARAM wParam,LPARAM lParam)
 {


	 switch(message)
	 {

	 case WM_ACTIVATE:
		 if(LOWORD(wParam) == WA_INACTIVE)
		 {
			 m_bPaused = true;
			 m_timer.stopTimer();
		 }
		 else
		 {
			 m_bPaused = false;
			 m_timer.startTimer();
		 }
		 return 0;
		 break;
	 case WM_SIZE:
		 m_iWindowWidth  = LOWORD(lParam);
		 m_iWindowHeight = HIWORD(lParam);
		 if(m_device)
		 {
			if(wParam == SIZE_MINIMIZED)
			{
				m_bPaused = true;
				m_bMinize = true;
				m_bMaxize = false;
			}
			else if(wParam == SIZE_MAXIMIZED)
			{
				m_bPaused = false;
				m_bMinize = false;
				m_bMaxize = true;
				resize();
			}
			else if(wParam == SIZE_RESTORED)
			{
				if(m_bMinize)
				{
					m_bPaused = false;
					m_bMinize = false;
					resize();
				}
				else if(m_bMaxize)
				{
					m_bPaused = false;
					m_bMaxize = false;
					resize();
				}
				else if(m_bResize)
				{
					// do nothing
				}
				else
				{
					resize();
				}
			}
		 }
		 return 0;
		 break;
	 case WM_ENTERSIZEMOVE:
		 m_bPaused = true;
		 m_bResize = true;
		 m_timer.stopTimer();
		 return 0;
		 break;
	 case WM_EXITSIZEMOVE:
		 m_bPaused = false;
		 m_bResize = false;
		 m_timer.startTimer();
		 resize();
		 return 0;
		 break;
		 // this msg is read when the window is closed
	 case WM_DESTROY:
		 // close the app entirely
		 PostQuitMessage(0);
		 return 0;
		 break;
		 // sent when a menu is active and the user presses a key
		 // not corresponding to any mnemonic or accelerator key
	 case WM_MENUCHAR:
		 return MAKELRESULT(0, MNC_CLOSE);
		 break;
		 // prevent the window from becoming too small
	 case WM_GETMINMAXINFO:
		 ((MINMAXINFO*)lParam)->ptMinTrackSize.x = 200;
		 ((MINMAXINFO*)lParam)->ptMinTrackSize.y = 200;
		 return 0;
		 break;
	 case WM_KEYDOWN:
		 if(GetAsyncKeyState('P') & 0x8000)
		 {
			 if(m_bPaused)
			 {
				 m_bPaused = false;
				 m_timer.startTimer();
				 return 0;
			 }
			 else
			 {
				 m_bPaused = true;
				 m_timer.stopTimer();
				 return 0;
			 }
		 }
		 else if(wParam == VK_ESCAPE)
		 {
			 PostQuitMessage(0);
			 return 0;
		 }
		 break;
	 }


	 return DefWindowProc(m_mainWindowInstance, message, wParam, lParam);
 }