#ifndef SHADOWCASTER_H
#define SHADOWCASTER_H

#include <D3D10.h>
#include "..\Maths\Utility.h"

namespace Ocelot
{
	class ShadowCaster
	{
	public:
		ShadowCaster();
		ShadowCaster(ID3D10Device* pDevice, UBIGINT sWidth, UBIGINT sHeight);
		~ShadowCaster();

		VVOID Init(ID3D10Device* _pDevice, UBIGINT sWidth, UBIGINT sHeight);
	private:
		UBIGINT width;
		UBIGINT height;

		ID3D10Device* pDevice;
	};
}
#endif