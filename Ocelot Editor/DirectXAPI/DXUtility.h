#ifndef DEF_DXUTILITY_H
#define DEF_DXUTILITY_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   DXUtility.h
/   
/  Description: This utility file just contains divers contents
/               pro Microsoft API for convenience.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

#include <D3D10.h>
#include <D3DX10.h>
#include <dxerr.h>
#include <cassert>
#include "..\Maths\Color.h"
#include "..\Maths\Vector2.h"
#include "..\InputManager\InputManager.h"
#include "..\DataStructures\DataStructureUtil.h"

#define BUFFCOUNT (256)

void FormatOutput(LPCWSTR formatstring, ...);

#if defined(DEBUG) || defined(_DEBUG)
	#ifndef D3D_DEBUG_INFO
	#define D3D_DEBUG_INFO
	#endif
#endif

#pragma warning(disable : 4005)

#if defined(DEBUG) || defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
// Will provide the line and the file when memory leaks occur
// because of a "new".
//#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
//#define new DEBUG_NEW
#endif

#define ReleaseCOM(x) {if(x){x->Release(); x = NULL;}}
#define DeletePointer(p) {if(p){delete p; p = NULL;}}
#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define KEY_UP(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 0 : 1)
#define WAS_PRESSED(vk_code) ((GetKeyState(vk_code) & 0x8000) != 0)

#if defined(DEBUG) | defined(_DEBUG)
	#ifndef HR
	#define HR(x){HRESULT hr = (x); if(FAILED(hr)) FormatOutput( L"%ls %d %d %ls\n", __FILE__, (DWORD)__LINE__, hr, L#x);}
	#endif
#else
	#ifndef HR
	#define HR(x) (x)
	#endif
#endif

namespace Ocelot
{
	// Typedefs
	typedef ID3D10Device* DevPtr;
	typedef ID3D10Effect* EffPtr;
	typedef ID3D10Buffer* BufPtr;
	typedef ID3D10BlendState*  BlendPtr;
	typedef ID3D10InputLayout* LayPtr;
	typedef ID3D10EffectTechnique* TecPtr;
	typedef ID3D10EffectVariable*  VarPtr;
	typedef ID3D10EffectMatrixVariable* MatPtr;
	typedef ID3D10EffectScalarVariable* ScaPtr;
	typedef ID3D10EffectVectorVariable* VecPtr;
	typedef ID3D10ShaderResourceView* ResViewPtr;
	typedef ID3D10EffectShaderResourceVariable* TexPtr;

	namespace WinUtil
	{
		inline Vector2 POINTSToVector(const POINTS& point)
		{
			return Vector2((FFLOAT)point.x, (FFLOAT)point.y);
		}

		inline Vector2 POINTToVector(const POINT& point)
		{
			return Vector2((FFLOAT)point.x, (FFLOAT)point.y);
		}

		inline Vector2 GetClientCursorPosition()
		{
		  POINT MousePos;

		  GetCursorPos(&MousePos);

		  ScreenToClient(GetActiveWindow(), &MousePos);

		  return POINTToVector(MousePos);
		}
 
		inline Vector2 GetClientCursorPosition(HWND hwnd)
		{
		  POINT MousePos;

		  GetCursorPos(&MousePos);

		  ScreenToClient(hwnd, &MousePos);

		  return POINTToVector(MousePos);
		}

		inline POINTS VectorToPOINTS(const Vector2& v)
		{
		  POINTS p;
		  p.x = (short)v.getX();
		  p.y = (short)v.getY();

		  return p;
		}

		inline POINT VectorToPOINT(const Vector2& v)
		{
		  POINT p;
		  p.x = (LINT)v.getX();
		  p.y = (LINT)v.getY();

		  return p;
		}
	}

	namespace ResMng
	{
		inline ID3D10Texture2D* getTexture2DFromFile(ID3D10Device* device, const CCHAR* fileName)
		{
			ID3D10Resource*  resource      = NULL;
			ID3D10Texture2D* tempTexture2D = NULL;

			HR(D3DX10CreateTextureFromFileA(device, fileName, NULL, NULL, &resource, NULL)); 

			HR(resource->QueryInterface(__uuidof(ID3D10Texture2D), (LPVOID*)&tempTexture2D));
			resource->Release();

			return tempTexture2D;
		}
	}

	namespace DXColor
	{
		
		static const D3DXCOLOR WHITE(1.0f, 1.0f, 1.0f, 1.0f);
		static const D3DXCOLOR BLACK(0.0f, 0.0f, 0.0f, 1.0f);
		static const D3DXCOLOR RED(1.0f, 0.0f, 0.0f, 1.0f);
		static const D3DXCOLOR GREEN(0.0f, 1.0f, 0.0f, 1.0f);
		static const D3DXCOLOR SKYBLUE(0.0f, 0.8f, 1.0f, 1.0f);
		static const D3DXCOLOR LIGHTBLUE(0.0f, 0.5f, 1.0f, 1.0f);
		static const D3DXCOLOR BLUE(0.0f, 0.0f, 1.0f, 1.0f);
		static const D3DXCOLOR VIOLET(0.5f, 0.0f, 1.0f, 1.0f);
		static const D3DXCOLOR YELLOW(1.0f, 1.0f, 0.0f, 1.0f);
		static const D3DXCOLOR CYAN(0.0f, 1.0f, 1.0f, 1.0f);
		static const D3DXCOLOR MAGENTA(1.0f, 0.0f, 1.0f, 1.0f);

		static const D3DXCOLOR BEACH_SAND(1.0f, 0.96f, 0.62f, 1.0f);
		static const D3DXCOLOR LIGHT_YELLOW_GREEN(0.48f, 0.77f, 0.46f, 1.0f);
		static const D3DXCOLOR DARK_YELLOW_GREEN(0.1f, 0.48f, 0.19f, 1.0f);
		static const D3DXCOLOR DARKBROWN(0.45f, 0.39f, 0.34f, 1.0f);

		inline D3DXCOLOR OcelotColToDXCol(Color col)
		{
			D3DXCOLOR newOne(col.getR(), col.getG(), col.getB(), col.getA());

			return newOne;
		}

		inline Color     DXColToOcelotCol(D3DXCOLOR col)
		{
			Color     newOne(col.r, col.g, col.b, col.a);

			return newOne;
		}

	}
}

#endif