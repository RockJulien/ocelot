#ifndef DEF_RESOURCEFACTORY_H
#define DEF_RESOURCEFACTORY_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   ResourceFactory.h/.cpp
/   
/  Description: This class allows to store texture resources
/               properly and allows to have fast and easy access
/               to them. It will force the texture to be loaded
/               once only.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    15/03/2012
/*****************************************************************/

#include "..\DirectXAPI\DXHelperFunctions.h"
#include "..\Geometry\OcelotDXMaterial.h"
#include "..\StringHelper\STRINGHelpers.cpp"
#include <fstream>
#include <map>

#define ResFactory ResourceFactory::instance()

namespace Ocelot
{
						enum ResourceType
						{
							eNormalTex = 0,
							eCubMapTex
						};

	class ResourceFactory
	{
	public:

			typedef std::map<WSTRING, ResViewPtr> ResourceViewMap;
			typedef std::map<UBIGINT, ResourceViewMap*> ResourceTypeMap;

			static const WSTRING cResourceFolderPath;
			static const WSTRING cResourceFilePath;

	private:

			// Attributes
			UBIGINT		     mNbResources;
			DevPtr           mDevice;
			ResourceTypeMap  mResourceViewsMap;

			// private constructors & methods
			ResourceFactory(){}

			ResourceFactory(const ResourceFactory&);             // copy cst
			ResourceFactory& operator= (const ResourceFactory&); // assgm cst

			BIGINT  loadNormalResource(const WSTRING& pFileName);
			BIGINT	loadCubeMapResource(const WSTRING& pFileName);
			VVOID	loadResourceFile();
			VVOID	saveResourceFile();

	public:

			// Destructor
			~ResourceFactory();

		    // Singleton
	static  ResourceFactory* instance();

			// Methods
			BIGINT     CreateTextures();
			BIGINT	   AddResource(const WSTRING& pFileName, ResourceType pType);
			ResViewPtr GetResource(const WSTRING& pResourceName, ResourceType pType = ResourceType::eNormalTex);
			// <-- Must be destroyed by hand afterward.
			ResViewPtr GenerateRandomTex();
			VVOID      LoadBumpMaterial(const WSTRING& pTextureName, BumpMaterial& pMaterial);
			VVOID      LoadTriPlanarMaterial(const WSTRING pTextureNames[3], TriPlanarBumpMaterial& pMaterial);

			// Accessors
	inline  UBIGINT    NbResources() const { return mNbResources;}
	inline  VVOID      SetDevice(DevPtr pDevice) { mDevice = pDevice; }

	};
}

#endif