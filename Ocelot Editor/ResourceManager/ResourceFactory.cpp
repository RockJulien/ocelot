#include "ResourceFactory.h"

using namespace Ocelot;
using namespace std;

const wstring ResourceFactory::cResourceFolderPath = L"..\\Resources\\";
const wstring ResourceFactory::cResourceFilePath   = L"Resources\\OcelotResources.ocelot";

ResourceFactory::~ResourceFactory()
{
	this->saveResourceFile();

	ResourceTypeMap::iterator lTypeMapIter = this->mResourceViewsMap.begin();
	for
		( ; lTypeMapIter != this->mResourceViewsMap.end(); lTypeMapIter++ )
	{
		ResourceViewMap::iterator lResourceMapIter = lTypeMapIter->second->begin();
		for
			( ; lResourceMapIter != lTypeMapIter->second->end(); lResourceMapIter++ )
		{
			ReleaseCOM( lResourceMapIter->second );
		}
		DeletePointer( lTypeMapIter->second );
	}
}

ResourceFactory* ResourceFactory::instance()
{
	static ResourceFactory instance;

	return &instance;
}

VVOID	   ResourceFactory::loadResourceFile()
{
	wifstream inputStream(cResourceFilePath);

	// create a buffer for putting useless
	// info inside.
	wstring tempBuffer;
	tempBuffer.resize(50);

	// extract number of textures first.
	inputStream >> tempBuffer >> mNbResources; // ignore //NumberOfTextures:

	// prepare the list of name for extracting
	// textures data.
	inputStream >> tempBuffer; // ignore //TextureNames:
	
	UBIGINT lCurrType;
	WSTRING lCurrName;
	for 
		(UINT currRes = 0; currRes < mNbResources; currRes++)
	{
		inputStream >> lCurrType >> lCurrName;

		ResViewPtr lNewResource = NULL;
		if 
			( lCurrType == ResourceType::eNormalTex )
		{
			lNewResource = loadRV( this->mDevice, cResourceFolderPath + lCurrName );
		}
		else if 
			( lCurrType == ResourceType::eCubMapTex )
		{
			lNewResource = loadCubeMap( this->mDevice, cResourceFolderPath + lCurrName );
		}

		if 
			( lNewResource != NULL )
		{
			ResourceTypeMap::iterator lMapIter;
			if
				((lMapIter = this->mResourceViewsMap.find(lCurrType)) != this->mResourceViewsMap.end())
			{
				lMapIter->second->insert( pair<WSTRING, ResViewPtr>( lCurrName, lNewResource ) );
			}
			else // The type category does not exist yet, so create it.
			{
				ResourceViewMap* lResourceMap = new ResourceViewMap();
				lResourceMap->insert( pair<WSTRING, ResViewPtr>( lCurrName, lNewResource ) );
				this->mResourceViewsMap.insert( pair<UBIGINT, ResourceViewMap*>( lCurrType, lResourceMap ) );
			}
		}
	}
}

VVOID	   ResourceFactory::saveResourceFile()
{
	// automatically cleared thanks to "wofstream"
	wofstream outputStream(cResourceFilePath);

	// store number of textures first.
	outputStream << "//NumberOfTextures:" << endl << mNbResources << endl;

	// re-write previous ones + new ones added
	// for loading them next time and save time.
	outputStream << endl << "//TextureNames:" << endl;

	ResourceTypeMap::iterator lTypeMapIter = this->mResourceViewsMap.begin();
	for 
		( ; lTypeMapIter != this->mResourceViewsMap.end(); lTypeMapIter++ )
	{
		ResourceViewMap::iterator lResourceMapIter = lTypeMapIter->second->begin();
		for 
			( ; lResourceMapIter != lTypeMapIter->second->end(); lResourceMapIter++ )
		{
			outputStream << lTypeMapIter->first << " " << lResourceMapIter->first << endl;
		}
	}
}

BIGINT     ResourceFactory::CreateTextures()
{
	HRESULT hr = S_OK;

	// load textures from Ocelot file
	// default textures provided with
	// the engine for instance.
	loadResourceFile();

	return (INT)hr;
}

BIGINT	   ResourceFactory::AddResource(const WSTRING& pFileName, ResourceType pType)
{
	HRESULT hr = -1;

	// Get the regular texture map list.
	ResourceViewMap* lResourceMap = NULL;
	ResourceTypeMap::iterator lTypeMapIter = this->mResourceViewsMap.find(pType);
	if 
		( lTypeMapIter != this->mResourceViewsMap.end() )
	{
		lResourceMap = lTypeMapIter->second;
	}
	else
	{
		lResourceMap = new ResourceViewMap();
		this->mResourceViewsMap.insert( pair<UBIGINT, ResourceViewMap*>( pType, lResourceMap ) );
	}

	ResourceViewMap::iterator lResIter = lResourceMap->begin();
	// if already exist ?
	for (; lResIter != lResourceMap->end(); lResourceMap++)
		if (lResourceMap->find(pFileName) != lResourceMap->end())
			return hr = S_OK;

	ResViewPtr lNewResource = NULL;
	// add new texture by the user on the fly.
	if
		(pType == eNormalTex)
	{
		lNewResource = loadRV( this->mDevice, pFileName );
	}
	else if
		(pType == eCubMapTex)
	{
		lNewResource = loadCubeMap( this->mDevice, pFileName );
		mNbResources++;
	}

	if 
		( lNewResource )
	{
		lResourceMap->insert( pair<WSTRING, ResViewPtr>( pFileName, lNewResource ) );
		this->mNbResources++;
	}

	return (INT)hr;
}

VVOID      ResourceFactory::LoadBumpMaterial(const WSTRING& pTextureName, BumpMaterial& pMaterial)
{
	// Get the regular texture map list.
	ResourceTypeMap::iterator lTypeMap = this->mResourceViewsMap.find( ResourceType::eNormalTex );

	WSTRING checker = L"";
	if 
		( !(pTextureName == checker) )
	{
		LWSTRING lRes;
		// split the extension.
		splitWithDelimiter( pTextureName, lRes, '.' );

		WSTRING lDiffuseName  = lRes.First()->Data() + L"_NRM." + lRes.Last()->Data();  // normal map
		WSTRING lSpecularName = lRes.First()->Data() + L"_SPEC." + lRes.Last()->Data(); // specular map

		ResViewPtr lAmbientRes  = NULL;
		ResViewPtr lDiffuseRes  = NULL;
		ResViewPtr lSpecularRes = NULL;
		ResourceViewMap::iterator lResourceIter;
		lResourceIter = lTypeMap->second->find( pTextureName );
		if 
			( lResourceIter != lTypeMap->second->end() )
		{
			lAmbientRes = lResourceIter->second;
		}
		else // Support the case where the texture is not loaded yet.
		{
			lAmbientRes = loadRV( this->mDevice, cResourceFolderPath + pTextureName );
			// Add it to the map.
			lTypeMap->second->insert( pair<WSTRING, ResViewPtr>( pTextureName, lAmbientRes ) );
		}

		lResourceIter = lTypeMap->second->find( lDiffuseName );
		if
			( lResourceIter != lTypeMap->second->end() )
		{
			lDiffuseRes = lResourceIter->second;
		}
		else // Support the case where the texture is not loaded yet.
		{
			lDiffuseRes = loadRV( this->mDevice, cResourceFolderPath + lDiffuseName );
			// Add it to the map.
			lTypeMap->second->insert( pair<WSTRING, ResViewPtr>( lDiffuseName, lDiffuseRes ) );
		}

		lResourceIter = lTypeMap->second->find( lSpecularName );
		if
			( lResourceIter != lTypeMap->second->end() )
		{
			lSpecularRes = lResourceIter->second;
		}
		else // Support the case where the texture is not loaded yet.
		{
			lSpecularRes = loadRV( this->mDevice, cResourceFolderPath + lSpecularName );
			// Add it to the map.
			lTypeMap->second->insert( pair<WSTRING, ResViewPtr>( lSpecularName, lSpecularRes ) );
		}

		pMaterial.SetAmbient( lAmbientRes );
		pMaterial.SetDiffuse( lDiffuseRes );
		pMaterial.SetSpecular( lSpecularRes );
	}
}

VVOID      ResourceFactory::LoadTriPlanarMaterial(const WSTRING pTextureNames[3], TriPlanarBumpMaterial& pMaterial)
{
	WSTRING checker = L"";
	USMALLINT lTextCounter = 0;

	// Get the regular texture map list.
	ResourceTypeMap::iterator lTypeMap = this->mResourceViewsMap.find( ResourceType::eNormalTex );

	for 
		( USMALLINT lCurrName = 0; lCurrName < 3; lCurrName++ )
	{
		if 
			( !(pTextureNames[lCurrName] == checker) )
		{
			LWSTRING lRes;

			// split the extension.
			splitWithDelimiter( pTextureNames[lCurrName], lRes, '.' );

			WSTRING lDiffuseName  = lRes.First()->Data() + L"_NRM." + lRes.Last()->Data();  // normal map
			WSTRING lSpecularName = lRes.First()->Data() + L"_SPEC." + lRes.Last()->Data(); // specular map

			ResViewPtr lAmbientRes  = NULL;
			ResViewPtr lDiffuseRes  = NULL;
			ResViewPtr lSpecularRes = NULL;
			ResourceViewMap::iterator lResourceIter;
			lResourceIter = lTypeMap->second->find( pTextureNames[lCurrName] );
			if
				( lResourceIter != lTypeMap->second->end() )
			{
				lAmbientRes = lResourceIter->second;
			}
			else // Support the case where the texture is not loaded yet.
			{
				lAmbientRes = loadRV( this->mDevice, cResourceFolderPath + pTextureNames[lCurrName] );
				// Add it to the map.
				lTypeMap->second->insert( pair<WSTRING, ResViewPtr>( pTextureNames[lCurrName], lAmbientRes ) );
			}

			lResourceIter = lTypeMap->second->find( lDiffuseName );
			if
				( lResourceIter != lTypeMap->second->end() )
			{
				lDiffuseRes = lResourceIter->second;
			}
			else // Support the case where the texture is not loaded yet.
			{
				lDiffuseRes = loadRV( this->mDevice, cResourceFolderPath + lDiffuseName );
				// Add it to the map.
				lTypeMap->second->insert( pair<WSTRING, ResViewPtr>( lDiffuseName, lDiffuseRes ) );
			}

			lResourceIter = lTypeMap->second->find( lSpecularName );
			if
				( lResourceIter != lTypeMap->second->end() )
			{
				lSpecularRes = lResourceIter->second;
			}
			else // Support the case where the texture is not loaded yet.
			{
				lSpecularRes = loadRV( this->mDevice, cResourceFolderPath + lSpecularName );
				// Add it to the map.
				lTypeMap->second->insert( pair<WSTRING, ResViewPtr>( lSpecularName, lSpecularRes ) );
			}

			pMaterial.SetTexture( lAmbientRes,  lTextCounter );     // Diffuse
			pMaterial.SetTexture( lDiffuseRes,  lTextCounter + 1 ); // Ambient
			pMaterial.SetTexture( lSpecularRes, lTextCounter + 2 ); // Specular
			lTextCounter += 3;
		}
	}
}

ResViewPtr ResourceFactory::GetResource(const WSTRING& pResourceName, ResourceType pType)
{
	ResViewPtr lResource = NULL;
	ResourceTypeMap::iterator lTypeIter;
	if
		( ( lTypeIter = this->mResourceViewsMap.find( pType ) ) != this->mResourceViewsMap.end() )
	{
		ResourceViewMap::iterator lResourceIter = lTypeIter->second->find( pResourceName );
		if
			( lResourceIter != lTypeIter->second->end() )
		{
			lResource = lResourceIter->second;
		}
	}

	return lResource;
}

ResViewPtr ResourceFactory::GenerateRandomTex()
{
	HRESULT hr;
	ResViewPtr randomRV = 0;
	Vector4 randomValues[1024];

	for(BIGINT i = 0; i < 1024; ++i)
	{
		randomValues[i].x(RandBetween(-1.0f, 1.0f));
		randomValues[i].y(RandBetween(-1.0f, 1.0f));
		randomValues[i].z(RandBetween(-1.0f, 1.0f));
		randomValues[i].w(RandBetween(-1.0f, 1.0f));
	}

	D3D10_SUBRESOURCE_DATA initData;
	initData.pSysMem = randomValues;
	initData.SysMemPitch = 1024*sizeof(Vector4);
	initData.SysMemSlicePitch = 1024*sizeof(Vector4);

	D3D10_TEXTURE1D_DESC texDesc;
	texDesc.Width = 1024;
	texDesc.MipLevels = 1;
	texDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	texDesc.Usage = D3D10_USAGE_IMMUTABLE;
	texDesc.BindFlags = D3D10_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;
	texDesc.ArraySize = 1;

	ID3D10Texture1D* randomTex = 0;
	hr = mDevice->CreateTexture1D(&texDesc, &initData, &randomTex);
	if(FAILED(hr))
	{
		MessageBox(0, L"CreateTexture in GenerateRandomTexture failed", 0, 0);
	}

	D3D10_SHADER_RESOURCE_VIEW_DESC viewDesc;
	viewDesc.Format = texDesc.Format;
	viewDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE1D;
	viewDesc.Texture1D.MipLevels = texDesc.MipLevels;
	viewDesc.Texture1D.MostDetailedMip = 0;

	hr = mDevice->CreateShaderResourceView(randomTex, &viewDesc, &randomRV);
	if(FAILED(hr))
	{
		MessageBox(0, L"CreateShaderResourceView in GenerateRandomTexture failed", 0, 0);
	}

	if
		(randomTex)
	{
		randomTex->Release();
		randomTex = nullptr;
	}

	return randomRV;
}