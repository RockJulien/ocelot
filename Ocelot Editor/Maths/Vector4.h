#ifndef DEF_VECTOR4_H
#define DEF_VECTOR4_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Vector4.h/.cpp
/
/ Description: This file provide a way to create a 4D vector with its
/              basic contents Dot, Cross products and extras such as
/              four interpolation function between vectors and product
/              with matrixes as well.
/              It will serve especially to pass in homogeneous coordinates
/              for calculating world coordinates by applying to the 4 by 4
/              world matrix for example.
/              Note: The "w" parameter which allow transformation or not
/              will be 1 for a point and 0 for a vector allowing a point
/              to be translated and not a vector representing a direction
/              for example.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Utility.h"
#include "Matrix4x4.h"

class Matrix4x4;
class Vector3;

class Vector4
{
private:

	// attributes
	FFLOAT m_fX;
	FFLOAT m_fY;
	FFLOAT m_fZ;
	FFLOAT m_fW;

public:

	// Constructors & Destructor
	Vector4();                                        // default cst
	Vector4(const Vector4&);                          // copy cst
	Vector4& operator = (const Vector4&);             // assignment cst
	Vector4(FFLOAT xyz, FFLOAT w);                      // overload cst
	Vector4(DFLOAT xyz, DFLOAT w);                    // overload cst
	Vector4(FFLOAT  x, FFLOAT  y, FFLOAT  z, FFLOAT  w);  // overload cst
	Vector4(DFLOAT x, DFLOAT y, DFLOAT z, DFLOAT w);  // overload cst
	~Vector4();

	// Methods
	FFLOAT   Vec4Length();
	FFLOAT   Vec4LengthSqrt();
	FFLOAT   Vec4DistanceBetween(const Vector4& target);
	Vector4 Vec4Normalise();
	FFLOAT   Vec4DotProduct(const Vector4& vec);
	Vector4 Vec4CrossProduct(const Vector4& adjVec1, const Vector4& adjVec2);
	Vector4 Vec4Addition(const Vector4& vecToAdd);
	Vector4 Vec4Subtraction(const Vector4& vecToSub);
	Vector4 Vec4LinearInterpolation(const Vector4& vecToJoin, FFLOAT var);
	Vector4 Vec4HermiteSplineInterpolation(const Vector4& tangStartPoint, const Vector4& vecToJoin, const Vector4& tangVecToJoin, FFLOAT var);
	Vector4 Vec4CatmullSplineInterpolation(const Vector4& vecControl1, const Vector4& vecToJoin, const Vector4& vecControl2, FFLOAT var);
	Vector4 Vec4BarycentricInterpolation(const Vector4& vecAtBar, const Vector4& vecToJoin, FFLOAT baryVar1, FFLOAT baryVar2);
	Vector4 Vec4VecMatrixProduct(const Matrix4x4& mat);

	// Accessors
	inline FFLOAT getX() const { return m_fX; }
	inline FFLOAT getY() const { return m_fY; }
	inline FFLOAT getZ() const { return m_fZ; }
	inline FFLOAT getW() const { return m_fW; }
	inline VVOID  x(const FFLOAT X) { m_fX = X; }
	inline VVOID  y(const FFLOAT Y) { m_fY = Y; }
	inline VVOID  z(const FFLOAT Z) { m_fZ = Z; }
	inline VVOID  w(const FFLOAT W) { m_fW = W; }

	// operators overload
	/*********************Cast Operators**************************/
	operator FFLOAT* ();              // for being able to pass it through the shader as a (FFLOAT*)
	operator const FFLOAT* () const;  // constant version
	/*********************** unary operators **********************/
	Vector4  operator + () const;
	Vector4  operator - () const;
	/*********************** assgm operators **********************/
	Vector4& operator += (const Vector4&);
	Vector4& operator -= (const Vector4&);
	Vector4& operator *= (const FFLOAT& val);
	Vector4& operator /= (const FFLOAT& val);
	/*********************** binary operators *********************/
	Vector4  operator +  (const Vector4&) const;
	Vector4  operator -  (const Vector4&) const;
	Vector4  operator *  (const FFLOAT& val) const;
	Vector4  operator /  (const FFLOAT& val) const;
	BBOOL     operator == (const Vector4&) const;
	BBOOL     operator != (const Vector4&) const;
	friend Vector4 operator * (FFLOAT, const Vector4&);
};

#endif