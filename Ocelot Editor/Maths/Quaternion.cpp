#include "Quaternion.h"

Quaternion::Quaternion() :
m_fX(0.0f), m_fY(0.0f), m_fZ(0.0f), m_fW(0.0f)
{
	QuaternionIdentity();
}

Quaternion::Quaternion(const Quaternion& q) :
m_fX(q.m_fX), m_fY(q.m_fY), m_fZ(q.m_fZ), m_fW(q.m_fW)
{

}

Quaternion& Quaternion::operator = (const Quaternion& q)
{
	m_fX = q.m_fX;
	m_fY = q.m_fY;
	m_fZ = q.m_fZ;
	m_fW = q.m_fW;

	return *this;
}

Quaternion::Quaternion(FFLOAT xyzw) :
m_fX(xyzw), m_fY(xyzw), m_fZ(xyzw), m_fW(xyzw)
{

}

Quaternion::Quaternion(DFLOAT xyzw) :
m_fX((FFLOAT)xyzw), m_fY((FFLOAT)xyzw), m_fZ((FFLOAT)xyzw), m_fW((FFLOAT)xyzw)
{

}

Quaternion::Quaternion(FFLOAT x, FFLOAT y, FFLOAT z, FFLOAT w) :
m_fX(x), m_fY(y), m_fZ(z), m_fW(w)
{

}

Quaternion::Quaternion(DFLOAT x, DFLOAT y, DFLOAT z, DFLOAT w) :
m_fX((FFLOAT)x), m_fY((FFLOAT)y), m_fZ((FFLOAT)z), m_fW((FFLOAT)w)
{

}

// constructors for constructing a quaternion from euler angles
Quaternion::Quaternion(FFLOAT headingAngle, FFLOAT attitudeAngle, FFLOAT bankAngle)
{
	// compute cosine and sine result in radian.
	FFLOAT angle = headingAngle * 0.5f;
	FFLOAT CosRow   = cos(angle);
	FFLOAT SinRow   = sin(angle);

	angle = attitudeAngle * 0.5f;
	FFLOAT CosPitch = cos(angle);
	FFLOAT SinPitch = sin(angle);

	angle = bankAngle * 0.5f;
	FFLOAT CosYaw   = cos(angle);
	FFLOAT SinYaw   = sin(angle);

	// limit number of calculation just for the CPU
	FFLOAT CosRowPitch = CosRow * CosPitch;
	FFLOAT SinRowPitch = SinRow * SinPitch;

	// then create the quaternion
	m_fX = (CosRowPitch * SinYaw) + (SinRowPitch * CosYaw);
	m_fY = (SinRow * CosPitch * CosYaw) + (CosRow * SinPitch * SinYaw);
	m_fZ = (CosRow * SinPitch * CosYaw) - (SinRow * CosPitch * SinYaw);
	m_fW = (CosRowPitch * CosYaw) - (SinRowPitch * SinYaw);
}

Quaternion::Quaternion(DFLOAT headingAngle, DFLOAT attitudeAngle, DFLOAT bankAngle)
{
	// compute cosine and sine result in radian.
	FFLOAT angle = (FFLOAT)headingAngle * 0.5f;
	FFLOAT CosRow   = cos(angle);
	FFLOAT SinRow   = sin(angle);

	angle = (FFLOAT)attitudeAngle * 0.5f;
	FFLOAT CosPitch = cos(angle);
	FFLOAT SinPitch = sin(angle);

	angle = (FFLOAT)bankAngle * 0.5f;
	FFLOAT CosYaw   = cos(angle);
	FFLOAT SinYaw   = sin(angle);

	// limit number of calculation just for the CPU
	FFLOAT CosRowPitch = CosRow * CosPitch;
	FFLOAT SinRowPitch = SinRow * SinPitch;

	// then create the quaternion
	m_fX = (CosRowPitch * SinYaw) + (SinRowPitch * CosYaw);
	m_fY = (SinRow * CosPitch * CosYaw) + (CosRow * SinPitch * SinYaw);
	m_fZ = (CosRow * SinPitch * CosYaw) - (SinRow * CosPitch * SinYaw);
	m_fW = (CosRowPitch * CosYaw) - (SinRowPitch * SinYaw);
}

Quaternion::Quaternion(const Vector3& vec)
{
	// constructor which convert euler angle to quaternion
	// by simply take back X, Y and Z components from the 
	// vector and assign them to same named components of 
	// a quaternion.

	// compute cosine and sine result in radian.
	FFLOAT angle = vec.getX() * 0.5f;
	FFLOAT CosRow   = cos(angle);
	FFLOAT SinRow   = sin(angle);

	angle = vec.getY() * 0.5f;
	FFLOAT CosPitch = cos(angle);
	FFLOAT SinPitch = sin(angle);

	angle = vec.getZ() * 0.5f;
	FFLOAT CosYaw   = cos(angle);
	FFLOAT SinYaw   = sin(angle);

	// limit number of calculation just for the CPU
	FFLOAT CosRowPitch = CosRow * CosPitch;
	FFLOAT SinRowPitch = SinRow * SinPitch;

	// then create the quaternion
	m_fX = (CosRowPitch * SinYaw) + (SinRowPitch * CosYaw);
	m_fY = (SinRow * CosPitch * CosYaw) + (CosRow * SinPitch * SinYaw);
	m_fZ = (CosRow * SinPitch * CosYaw) - (SinRow * CosPitch * SinYaw);
	m_fW = (CosRowPitch * CosYaw) - (SinRowPitch * SinYaw);
}

Quaternion::Quaternion(const Matrix4x4& mat)
{
	FFLOAT sumDiag = mat.get_11() + mat.get_22() + mat.get_33();

	if(sumDiag > 0.0f)
	{
		// Scale = 4 * m_fW of the quaternion
		FFLOAT scale = sqrt(sumDiag + 1.0f) * 2.0f;

		m_fX = (mat.get_32() - mat.get_23()) / scale;
		m_fY = (mat.get_13() - mat.get_31()) / scale;
		m_fZ = (mat.get_21() - mat.get_12()) / scale;
		m_fW = 0.25f * scale;
	}
	else if(mat.get_11() > mat.get_22() && mat.get_11() > mat.get_33())
	{
		// scale = 4 * m_fX of the quaternion
		FFLOAT scale = sqrt(mat.get_11() - mat.get_22() - mat.get_33() + 1.0f) * 2.0f;

		m_fX = 0.25f * scale;
		m_fY = (mat.get_12() + mat.get_21()) / scale;
		m_fZ = (mat.get_13() + mat.get_31()) / scale;
		m_fW = (mat.get_32() - mat.get_23()) / scale;
	}
	else if(mat.get_22() > mat.get_33())
	{
		// Scale = 4 * m_fY of the quaternion
		FFLOAT scale = sqrt(mat.get_22() - mat.get_11() - mat.get_33() + 1.0f) * 2.0f;

		m_fX = (mat.get_12() + mat.get_21()) / scale;
		m_fY = 0.25f * scale;
		m_fZ = (mat.get_23() + mat.get_32()) / scale;
		m_fW = (mat.get_13() - mat.get_31()) / scale;
	}
	else
	{
		// Scale = 4 * m_fZ of the quaternion
		FFLOAT scale = sqrt(mat.get_33() - mat.get_11() - mat.get_22() + 1.0f) * 2.0f;

		m_fX = (mat.get_13() + mat.get_31()) / scale;
		m_fY = (mat.get_23() + mat.get_32()) / scale;
		m_fZ = 0.25f * scale;
		m_fW = (mat.get_21() - mat.get_12()) / scale;
	}

	// don't forget to normalize if I didn't add the function here.
}

Quaternion::Quaternion(const Matrix3x3& mat)
{
	FFLOAT sumDiag = mat.get_11() + mat.get_22() + mat.get_33();

	if(sumDiag > 0.0f)
	{
		// Scale = 4 * m_fW of the quaternion
		FFLOAT scale = sqrt(sumDiag + 1.0f) * 2.0f;

		m_fX = (mat.get_32() - mat.get_23()) / scale;
		m_fY = (mat.get_13() - mat.get_31()) / scale;
		m_fZ = (mat.get_21() - mat.get_12()) / scale;
		m_fW = 0.25f * scale;
	}
	else if(mat.get_11() > mat.get_22() && mat.get_11() > mat.get_33())
	{
		// scale = 4 * m_fX of the quaternion
		FFLOAT scale = sqrt(mat.get_11() - mat.get_22() - mat.get_33() + 1.0f) * 2.0f;

		m_fX = 0.25f * scale;
		m_fY = (mat.get_12() + mat.get_21()) / scale;
		m_fZ = (mat.get_13() + mat.get_31()) / scale;
		m_fW = (mat.get_32() - mat.get_23()) / scale;
	}
	else if(mat.get_22() > mat.get_33())
	{
		// Scale = 4 * m_fY of the quaternion
		FFLOAT scale = sqrt(mat.get_22() - mat.get_11() - mat.get_33() + 1.0f) * 2.0f;

		m_fX = (mat.get_12() + mat.get_21()) / scale;
		m_fY = 0.25f * scale;
		m_fZ = (mat.get_23() + mat.get_32()) / scale;
		m_fW = (mat.get_13() - mat.get_31()) / scale;
	}
	else
	{
		// Scale = 4 * m_fZ of the quaternion
		FFLOAT scale = sqrt(mat.get_33() - mat.get_11() - mat.get_22() + 1.0f) * 2.0f;

		m_fX = (mat.get_13() + mat.get_31()) / scale;
		m_fY = (mat.get_23() + mat.get_32()) / scale;
		m_fZ = 0.25f * scale;
		m_fW = (mat.get_21() - mat.get_12()) / scale;
	}

	// don't forget to normalize if I didn't add the function here.
}

Quaternion::~Quaternion()
{

}

Quaternion  Quaternion::QuaternionIdentity()
{
	// set the real part of the quaternion to 1.
	m_fX = 0.0f;
	m_fY = 0.0f;
	m_fZ = 0.0f;
	m_fW = 1.0f;

	return *this;
}

FFLOAT      Quaternion::QuaternionLength()
{
	// compute the length of the quaternion
	return sqrt((SQUARE(m_fX)) + (SQUARE(m_fY)) + (SQUARE(m_fZ)) + (SQUARE(m_fW)));
}

FFLOAT      Quaternion::QuaternionLengthSqrt()
{
	// compute the length square of the quaternion
	return (SQUARE(m_fX)) + (SQUARE(m_fY)) + (SQUARE(m_fZ)) + (SQUARE(m_fW));
}

Quaternion Quaternion::QuaternionNormalize()
{
	// Normalize the quaternion (same as vectors)
	FFLOAT magnitude = QuaternionLength();

	if(fabsf(magnitude) > FLT_EPSILON)  // prevent from dividing by 0
	{
		this->m_fX /= magnitude;
		this->m_fY /= magnitude;
		this->m_fZ /= magnitude;
		this->m_fW /= magnitude;
	}

	return Quaternion(m_fX, m_fY, m_fZ, m_fW);
}

Quaternion Quaternion::QuaternionInverse()
{
	// compute the inverse of the quaternion
	Quaternion temp(*this);

	FFLOAT magnitudeSqrt = temp.QuaternionLengthSqrt();

	// same as the conjuguate but by dividing each 
	// component by the magnitude square.
	m_fX = -temp.m_fX / magnitudeSqrt;
	m_fY = -temp.m_fY / magnitudeSqrt;
	m_fZ = -temp.m_fZ / magnitudeSqrt;
	m_fW = temp.m_fW / magnitudeSqrt;

	return *this;
}

Quaternion Quaternion::QuaternionConjuguate()
{
	// compute the conjuguate of the quaternion
	// just insert opposite of the imaginary part.
	return Quaternion(-m_fX, -m_fY, -m_fZ, m_fW);
}

Quaternion Quaternion::QuaternionExponantial()
{
	// compute the exponential of the quaternion
	// First compute the absolute of the imaginary part
	FFLOAT absImagPart = sqrt(SQUARE(m_fX) + SQUARE(m_fY) + SQUARE(m_fZ));

	// then, compute the real and imaginary part of the new quaternion
	FFLOAT realPart  = exp(m_fW) * cos(absImagPart);
	FFLOAT imagPartX = exp(m_fW) * ((m_fX/absImagPart) * sin(absImagPart));
	FFLOAT imagPartY = exp(m_fW) * ((m_fY/absImagPart) * sin(absImagPart));
	FFLOAT imagPartZ = exp(m_fW) * ((m_fZ/absImagPart) * sin(absImagPart));

	return Quaternion(imagPartX, imagPartY, imagPartZ, realPart);
}

Quaternion Quaternion::QuaternionLogarithm()
{
	// compute the logarithm of the quaternion.
	// First compute the absolute of the imaginary part
	FFLOAT absImagPart = sqrt(SQUARE(m_fX) + SQUARE(m_fY) + SQUARE(m_fZ));

	// then, compute the real and imaginary part of the new quaternion
	FFLOAT realPart  = log(QuaternionLength());
	FFLOAT imagPartX = ((m_fX/absImagPart) * acos(m_fW/QuaternionLength()));
	FFLOAT imagPartY = ((m_fY/absImagPart) * acos(m_fW/QuaternionLength()));
	FFLOAT imagPartZ = ((m_fZ/absImagPart) * acos(m_fW/QuaternionLength()));

	return Quaternion(imagPartX, imagPartY, imagPartZ, realPart);
}

Quaternion Quaternion::QuaternionPowerScalar(const FFLOAT& power)
{
	// compute the quaternion at the scalar power in parameter.
	return (QuaternionLogarithm() * power).QuaternionExponantial();
}

Quaternion Quaternion::QuaternionPowerQuaternion(const Quaternion& q)
{
	// compute the quaternion at the quaternion power in parameter.
	return (QuaternionLogarithm() * q).QuaternionExponantial();
}

FFLOAT      Quaternion::QuaternionDotProduct(const Quaternion& q)
{
	// apply a dot product to the quaternion
	return ((m_fX * q.m_fX) + (m_fY * q.m_fY) + (m_fZ * q.m_fZ) + (m_fW * q.m_fW));
}

Vector3    Quaternion::QuaternionToVectorTransformAxis(const Vector3& axis)
{
	Vector3 newAxis;
	Matrix4x4 rotat;

	// create a matrix from the quaternion
	rotat = QuaternionToMatrix4Normalized();

	// then, transform the axis by the matrix
	newAxis.x((axis.getX() * rotat.get_11()) + (axis.getY() * rotat.get_21()) + (axis.getZ() * rotat.get_31()) + rotat.get_41());
	newAxis.y((axis.getX() * rotat.get_12()) + (axis.getY() * rotat.get_22()) + (axis.getZ() * rotat.get_32()) + rotat.get_42());
	newAxis.z((axis.getX() * rotat.get_13()) + (axis.getY() * rotat.get_23()) + (axis.getZ() * rotat.get_33()) + rotat.get_43());

	return newAxis;
}

Vector3    Quaternion::QuaternionToEulerNoNormalized()
{
	// for lazy programmer which were sleeping before the TV 
	// and so forgot to normalize the quaternion.
	// It will work thanks to this function.
	FFLOAT unit    = QuaternionLengthSqrt();
	FFLOAT checker = (m_fX * m_fY) + (m_fZ * m_fW);  // will test singularity points.

	if(checker > (0.499f * unit))        // singularity at north pole of the hyper sphere.
	{
		FFLOAT Heading  = 2.0f * atan2(m_fX, m_fW);
		FFLOAT Attitude = PI * 0.5f;
		FFLOAT Bank     = 0.0f;

		return Vector3(Heading, Attitude, Bank);
	}
	else if(checker < (-0.499f * unit))  // singularity at the south of the hyper sphere
	{
		FFLOAT Heading  = -2.0f * atan2(m_fX, m_fW);
		FFLOAT Attitude = -PI * 0.5f;
		FFLOAT Bank     = 0.0f;

		return Vector3(Heading, Attitude, Bank);
	}

	// else
	FFLOAT Xsqrt = SQUARE(m_fX);
	FFLOAT Ysqrt = SQUARE(m_fY);
	FFLOAT Zsqrt = SQUARE(m_fZ);
	FFLOAT Wsqrt = SQUARE(m_fW);

	FFLOAT Heading  = atan2((2.0f * m_fY * m_fW) - (2.0f * m_fX * m_fZ), Xsqrt - Ysqrt - Zsqrt + Wsqrt);
	FFLOAT Attitude = asin(2.0f * checker/unit);
	FFLOAT Bank     = atan2((2.0f * m_fX * m_fW) - (2.0f * m_fY * m_fZ), -Xsqrt + Ysqrt - Zsqrt + Wsqrt);

	return Vector3(Heading, Attitude, Bank);
}

Vector3    Quaternion::QuaternionToEulerNormalized()
{
	// we assume in this one that the quaternion is normalized.
	FFLOAT checker = (m_fX * m_fY) + (m_fZ * m_fW);  // will test singularity points.

	if(checker > 0.499f)        // singularity at north pole of the hyper sphere.
	{
		FFLOAT Heading  = 2.0f * atan2(m_fX, m_fW);
		FFLOAT Attitude = PI * 0.5f;
		FFLOAT Bank     = 0.0f;

		return Vector3(Heading, Attitude, Bank);
	}
	else if(checker < -0.499f)  // singularity at the south of the hyper sphere
	{
		FFLOAT Heading  = -2.0f * atan2(m_fX, m_fW);
		FFLOAT Attitude = -PI * 0.5f;
		FFLOAT Bank     = 0.0f;

		return Vector3(Heading, Attitude, Bank);
	}

	// else
	FFLOAT Xsqrt = SQUARE(m_fX);
	FFLOAT Ysqrt = SQUARE(m_fY);
	FFLOAT Zsqrt = SQUARE(m_fZ);

	FFLOAT Heading  = atan2((2.0f * m_fY * m_fW) - (2.0f * m_fX * m_fZ), 1.0f - (2.0f * Ysqrt) - (2.0f * Zsqrt));
	FFLOAT Attitude = asin(2.0f * checker);
	FFLOAT Bank     = atan2((2.0f * m_fX * m_fW) - (2.0f * m_fY * m_fZ), 1.0f - (2.0f * Xsqrt) - (2.0f * Zsqrt));

	return Vector3(Heading, Attitude, Bank);
}

Matrix3x3  Quaternion::QuaternionToMatrix3NoNormalized()
{
	// case where the quaternion is not normalized
	// convertion of a quaternion to a matrix 3x3.
	FFLOAT Xsqrt = SQUARE(m_fX);
	FFLOAT Ysqrt = SQUARE(m_fY);
	FFLOAT Zsqrt = SQUARE(m_fZ);
	FFLOAT Wsqrt = SQUARE(m_fW);

	// compute the inverse square length because of the non normalization
	// of the quaternion.
	// this case looks better because the sqrt root is not computed
	// (save process time)
	FFLOAT invSqrtLen = 1.0f / QuaternionLengthSqrt();

	// then create the matrix
	Matrix3x3 QuatRotMatrix;

	// do not need to create an identity one because particular
	// value will be in the main diagonal. (save process time)
	QuatRotMatrix.set_11((Xsqrt - Ysqrt - Zsqrt + Wsqrt) * invSqrtLen);
	QuatRotMatrix.set_22((-Xsqrt + Ysqrt - Zsqrt + Wsqrt) * invSqrtLen);
	QuatRotMatrix.set_33((-Xsqrt - Ysqrt + Zsqrt + Wsqrt) * invSqrtLen);

	FFLOAT mulXY = m_fX * m_fY;
	FFLOAT mulZW = m_fZ * m_fW;
	QuatRotMatrix.set_21((2.0f * (mulXY + mulZW)) * invSqrtLen);
	QuatRotMatrix.set_12((2.0f * (mulXY - mulZW)) * invSqrtLen);

	FFLOAT mulXZ = m_fX * m_fZ;
	FFLOAT mulYW = m_fY * m_fW;
	QuatRotMatrix.set_31((2.0f * (mulXZ - mulYW)) * invSqrtLen);
	QuatRotMatrix.set_13((2.0f * (mulXZ + mulYW)) * invSqrtLen);

	FFLOAT mulYZ = m_fY * m_fZ;
	FFLOAT mulXW = m_fX * m_fW;
	QuatRotMatrix.set_32((2.0f * (mulYZ + mulXW)) * invSqrtLen);
	QuatRotMatrix.set_23((2.0f * (mulYZ - mulXW)) * invSqrtLen);

	return QuatRotMatrix;
}

Matrix3x3  Quaternion::QuaternionToMatrix3Normalized()
{
	// we assume that the quaternion is normalized
	Matrix3x3 QuatRotMatrix;

	// useful precalculation (save process time)
	FFLOAT Xsqrt = SQUARE(m_fX);
	FFLOAT Ysqrt = SQUARE(m_fY);
	FFLOAT Zsqrt = SQUARE(m_fZ);
	FFLOAT Wsqrt = SQUARE(m_fW);
	FFLOAT mulXY = m_fX * m_fY;
	FFLOAT mulXZ = m_fX * m_fZ;
	FFLOAT mulXW = m_fX * m_fW;
	FFLOAT mulYZ = m_fY * m_fZ;
	FFLOAT mulYW = m_fY * m_fW;
	FFLOAT mulZW = m_fZ * m_fW;

	// now let's fill the matrix
	/*******************First row*************************/
	QuatRotMatrix.set_11(1.0f - (2.0f * Ysqrt) - (2.0f * Zsqrt));
	QuatRotMatrix.set_12((2.0f * mulXY) + (2.0f * mulZW));
	QuatRotMatrix.set_13((2.0f * mulXZ) - (2.0f * mulYW));
	/*******************Second Row************************/
	QuatRotMatrix.set_21((2.0f * mulXY) - (2.0f * mulZW));
	QuatRotMatrix.set_22(1.0f - (2.0f * Xsqrt) - (2.0f * Zsqrt));
	QuatRotMatrix.set_23((2.0f * mulYZ) + (2.0f * mulXW));
	/*******************Third row*************************/
	QuatRotMatrix.set_31((2.0f * mulXZ) + (2.0f * mulYW));
	QuatRotMatrix.set_32((2.0f * mulYZ) - (2.0f * mulXW));
	QuatRotMatrix.set_33(1.0f - (2.0f * Xsqrt) - (2.0f * Ysqrt));

	return QuatRotMatrix;
}

Matrix4x4  Quaternion::QuaternionToMatrix4NoNormalized()
{
	// case where the quaternion is not normalized
	// convertion of a quaternion to a matrix 4x4.
	FFLOAT Xsqrt = SQUARE(m_fX);
	FFLOAT Ysqrt = SQUARE(m_fY);
	FFLOAT Zsqrt = SQUARE(m_fZ);
	FFLOAT Wsqrt = SQUARE(m_fW);

	// compute the inverse square length because of the non normalization
	// of the quaternion.
	// this case looks better because the sqrt root is not computed
	// (save process time)
	FFLOAT invSqrtLen = 1.0f / QuaternionLengthSqrt();

	// then create the matrix
	Matrix4x4 QuatRotMatrix;

	// do not need to create an identity one because particular
	// value will be in the main diagonal. (save process time)
	QuatRotMatrix.set_11((Xsqrt - Ysqrt - Zsqrt + Wsqrt) * invSqrtLen);
	QuatRotMatrix.set_22((-Xsqrt + Ysqrt - Zsqrt + Wsqrt) * invSqrtLen);
	QuatRotMatrix.set_33((-Xsqrt - Ysqrt + Zsqrt + Wsqrt) * invSqrtLen);
	QuatRotMatrix.set_44(1.0f);

	FFLOAT mulXY = m_fX * m_fY;
	FFLOAT mulZW = m_fZ * m_fW;
	QuatRotMatrix.set_21((2.0f * (mulXY + mulZW)) * invSqrtLen);
	QuatRotMatrix.set_12((2.0f * (mulXY - mulZW)) * invSqrtLen);

	FFLOAT mulXZ = m_fX * m_fZ;
	FFLOAT mulYW = m_fY * m_fW;
	QuatRotMatrix.set_31((2.0f * (mulXZ - mulYW)) * invSqrtLen);
	QuatRotMatrix.set_13((2.0f * (mulXZ + mulYW)) * invSqrtLen);

	FFLOAT mulYZ = m_fY * m_fZ;
	FFLOAT mulXW = m_fX * m_fW;
	QuatRotMatrix.set_32((2.0f * (mulYZ + mulXW)) * invSqrtLen);
	QuatRotMatrix.set_23((2.0f * (mulYZ - mulXW)) * invSqrtLen);

	// fill the rest of the 4x4 matrix with 0.0f
	QuatRotMatrix.set_14(0.0f);
	QuatRotMatrix.set_24(0.0f);
	QuatRotMatrix.set_34(0.0f);
	QuatRotMatrix.set_41(0.0f);   // do not need because
	QuatRotMatrix.set_42(0.0f);   // every slot are already
	QuatRotMatrix.set_43(0.0f);   // set to zero

	return QuatRotMatrix;
}

Matrix4x4  Quaternion::QuaternionToMatrix4Normalized()
{
	// we assume that the quaternion is normalized
	Matrix4x4 QuatRotMatrix;
	Quaternion lThis = *this;

	Quaternion lQ0 = lThis + lThis;
	Quaternion lQ1 = Quaternion( lThis.getX() * lQ0.getX(), 
								 lThis.getY() * lQ0.getY(), 
								 lThis.getZ() * lQ0.getZ(), 
								 lThis.getW() * lQ0.getW() );

	Vector4 lV0 = Vector4(lQ1.getY(), lQ1.getX(), lQ1.getX(), 0.0f);
	Vector4 lV1 = Vector4(lQ1.getZ(), lQ1.getZ(), lQ1.getY(), 0.0f);
	Vector4 lR0 = Vector4( 1.0f - lV0.getX(), 
						   1.0f - lV0.getY(), 
						   1.0f - lV0.getZ(), 
						   0.0f - lV0.getW() );
	lR0 = Vector4( lR0.getX() - lV1.getX(), 
				   lR0.getY() - lV1.getY(), 
				   lR0.getZ() - lV1.getZ(), 
				   lR0.getW() - lV1.getW() );

	lV0 = Vector4( lThis.getX(), lThis.getX(), lThis.getY(), lThis.getW() );
	lV1 = Vector4( lQ0.getZ(), lQ0.getY(), lQ0.getZ(), lQ0.getW() );
	lV0 = Vector4( lV0.getX() * lV1.getX(), 
				   lV0.getY() * lV1.getY(), 
				   lV0.getZ() * lV1.getZ(), 
				   lV0.getW() * lV1.getW() );

	lV1 = Vector4( lThis.getW(), lThis.getW(), lThis.getW(), lThis.getW() );
	Vector4 lV2 = Vector4( lQ0.getY(), lQ0.getZ(), lQ0.getX(), lQ0.getW() );
	lV1 = Vector4( lV1.getX() * lV2.getX(), 
				   lV1.getY() * lV2.getY(), 
				   lV1.getZ() * lV2.getZ(), 
				   lV1.getW() * lV2.getW() );

	Vector4 lR1 = Vector4( lV0.getX() + lV1.getX(), 
						   lV0.getY() + lV1.getY(), 
						   lV0.getZ() + lV1.getZ(), 
						   lV0.getW() + lV1.getW() );
	Vector4 lR2 = Vector4( lV0.getX() - lV1.getX(), 
						   lV0.getY() - lV1.getY(), 
						   lV0.getZ() - lV1.getZ(), 
						   lV0.getW() - lV1.getW() );

	lV0 = Vector4( lR1.getY(), lR2.getX(), lR2.getY(), lR1.getZ() );
	lV1 = Vector4( lR1.getX(), lR2.getZ(), lR1.getX(), lR2.getZ() );

	// now let's fill the matrix
	// *******************First row*************************/
	QuatRotMatrix.set_11( lR0.getX() );
	QuatRotMatrix.set_12( lV0.getX() );
	QuatRotMatrix.set_13( lV0.getY() );
	QuatRotMatrix.set_14( lR0.getW() );
	// *******************Second Row************************/
	QuatRotMatrix.set_21( lV0.getZ() );
	QuatRotMatrix.set_22( lR0.getY() );
	QuatRotMatrix.set_23( lV0.getW() );
	QuatRotMatrix.set_24( lR0.getW() );
	// *******************Third row*************************/
	QuatRotMatrix.set_31( lV1.getX() );
	QuatRotMatrix.set_32( lV1.getY() );
	QuatRotMatrix.set_33( lR0.getZ() );
	QuatRotMatrix.set_34( lR0.getW() );
	// *******************Fourth row************************/
	QuatRotMatrix.set_41(0.0f);
	QuatRotMatrix.set_42(0.0f);  
	QuatRotMatrix.set_43(0.0f);  
	QuatRotMatrix.set_44(1.0f);

	// useful precalculation (save process time)
	//FFLOAT Xsqrt = SQUARE(m_fX);
	//FFLOAT Ysqrt = SQUARE(m_fY);
	//FFLOAT Zsqrt = SQUARE(m_fZ);
	//FFLOAT Wsqrt = SQUARE(m_fW);
	//FFLOAT mulXY = m_fX * m_fY;
	//FFLOAT mulXZ = m_fX * m_fZ;
	//FFLOAT mulXW = m_fX * m_fW;
	//FFLOAT mulYZ = m_fY * m_fZ;
	//FFLOAT mulYW = m_fY * m_fW;
	//FFLOAT mulZW = m_fZ * m_fW;

	//// now let's fill the matrix
	///*******************First row*************************/
	//QuatRotMatrix.set_11(1.0f - (2.0f * Ysqrt) - (2.0f * Zsqrt));
	//QuatRotMatrix.set_12((2.0f * mulXY) + (2.0f * mulZW));
	//QuatRotMatrix.set_13((2.0f * mulXZ) - (2.0f * mulYW));
	//QuatRotMatrix.set_14(0.0f);
	///*******************Second Row************************/
	//QuatRotMatrix.set_21((2.0f * mulXY) - (2.0f * mulZW));
	//QuatRotMatrix.set_22(1.0f - (2.0f * Xsqrt) - (2.0f * Zsqrt));
	//QuatRotMatrix.set_23((2.0f * mulYZ) + (2.0f * mulXW));
	//QuatRotMatrix.set_24(0.0f);
	///*******************Third row*************************/
	//QuatRotMatrix.set_31((2.0f * mulXZ) + (2.0f * mulYW));
	//QuatRotMatrix.set_32((2.0f * mulYZ) - (2.0f * mulXW));
	//QuatRotMatrix.set_33(1.0f - (2.0f * Xsqrt) - (2.0f * Ysqrt));
	//QuatRotMatrix.set_34(0.0f);
	///*******************Fourth row************************/
	//QuatRotMatrix.set_41(0.0f);
	//QuatRotMatrix.set_42(0.0f);  
	//QuatRotMatrix.set_43(0.0f);  
	//QuatRotMatrix.set_44(1.0f);

	return QuatRotMatrix;
}

Quaternion Quaternion::QuaternionLinearInterpolation(Quaternion to, FFLOAT var)
{
	// Simple Linear Interpolation of two quaternions.
	Quaternion lFrom(*this);

	// very quickly done thanks to good operator overload :-)
	return ( lFrom * (1 - var) + to * var ).QuaternionNormalize();
}

Quaternion Quaternion::QuaternionSLinearInterpolation(Quaternion to, FFLOAT var, BBOOL shortestPath)
{
	Quaternion lFrom(*this);
	Quaternion lToReturn;
	FFLOAT lDot = lFrom.QuaternionDotProduct( to );

	/*	dot = cos(theta)
		if (dot < 0), q1 and q2 are more than 90 degrees apart,
		so we can invert one to reduce spinning	*/
	if ( lDot < 0.0f )
	{
		lDot = -lDot;
		lToReturn = -to;
	} 
	else
	{
		lToReturn = to;
	}
		
	if ( lDot < 0.95f )
	{
		FFLOAT lAngle = acosf( lDot );
		return ( lFrom * sinf( lAngle * ( 1 - var ) ) + lToReturn * sinf( lAngle * var ) ) / sinf( lAngle );
	} 
	else // if the angle is small, use linear interpolation		
	{
		return lFrom.QuaternionLinearInterpolation( lToReturn, var );
	}
}

Quaternion Quaternion::QuaternionCLinearInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, FFLOAT var, BBOOL shortestPath)
{
	// cubic interpolation of four quaternions thanks to the
	// Spherical Linear Interpolation almost like 
	// the CastleJau method.
	Quaternion from(*this);

	// r1 = c(b1,b2)  b1 = c(from, to) & b2 = c(ctrlFrom, ctrlTo)
	Quaternion b1 = from.QuaternionSLinearInterpolation(to, var, shortestPath);
	Quaternion b2 = ctrlFrom.QuaternionSLinearInterpolation(ctrlTo, var, shortestPath);

	return b1.QuaternionSLinearInterpolation(b2, (2.0f * var) * (1.0f - var), shortestPath);
}

Quaternion Quaternion::QuaternionCastleJauInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, FFLOAT var)
{
	// de CastleJau Interpolation which uses
	// Simple Linear Interpolation.
	Quaternion from(*this);

	// First interpolation (base of the pyramid)
	Quaternion q0 = from.QuaternionLinearInterpolation(ctrlFrom, var);
	Quaternion q1 = ctrlFrom.QuaternionLinearInterpolation(ctrlTo, var);
	Quaternion q2 = ctrlTo.QuaternionLinearInterpolation(to, var);
	// Second round (middle of the pyramid)
	Quaternion r0 = q0.QuaternionLinearInterpolation(q1, var);
	Quaternion r1 = q1.QuaternionLinearInterpolation(q2, var);

	// and return directly the final quaternion (hat of the pyramid)
	return r0.QuaternionLinearInterpolation(r1, var);
}

Quaternion Quaternion::QuaternionBézierInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, FFLOAT var, BBOOL shortestPath)
{
	// Bézier Interpolation just by using Spherical Linear 
	// Interpolation instead of Simple Linear Interpolation.
	Quaternion from(*this);

	// First interpolation (base of the pyramid)
	Quaternion q0 = from.QuaternionSLinearInterpolation(ctrlFrom, var, shortestPath);
	Quaternion q1 = ctrlFrom.QuaternionSLinearInterpolation(ctrlTo, var, shortestPath);
	Quaternion q2 = ctrlTo.QuaternionSLinearInterpolation(to, var, shortestPath);
	// Second round (middle of the pyramid)
	Quaternion r0 = q0.QuaternionSLinearInterpolation(q1, var, shortestPath);
	Quaternion r1 = q1.QuaternionSLinearInterpolation(q2, var, shortestPath);

	// and return directly the final quaternion (hat of the pyramid)
	return r0.QuaternionSLinearInterpolation(r1, var, shortestPath);
}

VVOID Quaternion::RotateByVector(const Vector3& v)
{
	Quaternion q(0.0f, v.getX(), v.getY(), v.getZ());
	(*this) *= q;
}

VVOID Quaternion::AddScaledVector(const Vector3& v, const FFLOAT scale)
{
	Quaternion q(0.0f, v.getX() * scale, v.getY() * scale, v.getZ() * scale);
	q *= *this;

	m_fW += q.getW() * 0.5f;
	m_fX += q.getX() * 0.5f;
	m_fY += q.getY() * 0.5f;
	m_fZ += q.getZ() * 0.5f;
}

VVOID Quaternion::QuaternionRotateFromAxis(Quaternion& result, const Vector3& axis, FFLOAT angle)
{
	FFLOAT sinVal = sinf(angle * 0.5f);

	result.x(axis.getX() * sinVal);
	result.y(axis.getY() * sinVal);
	result.z(axis.getZ() * sinVal);
	result.w(cosf(angle * 0.5f));
}

Quaternion Quaternion::operator + () const
{
	return Quaternion(+this->m_fX, +this->m_fY, +this->m_fZ, +this->m_fW);
}

Quaternion Quaternion::operator - () const
{
	return Quaternion(-this->m_fX, -this->m_fY, -this->m_fZ, -this->m_fW);
}

Quaternion& Quaternion::operator += (const Quaternion& q)
{
	m_fX += q.m_fX;
	m_fY += q.m_fY;
	m_fZ += q.m_fZ;
	m_fW += q.m_fW;

	return *this;
}

Quaternion& Quaternion::operator -= (const Quaternion& q)
{
	m_fX -= q.m_fX;
	m_fY -= q.m_fY;
	m_fZ -= q.m_fZ;
	m_fW -= q.m_fW;

	return *this;
}

VVOID Quaternion::operator *= (const Quaternion& q)
{
	Quaternion t = *this;

	m_fW = t.getW() * q.getW() - t.getX() * q.getX() - t.getY() * q.getY() - t.getZ() * q.getZ();
	m_fX = t.getW() * q.getX() + t.getX() * q.getW() + t.getY() * q.getZ() - t.getZ() * q.getY();
	m_fY = t.getW() * q.getY() + t.getY() * q.getW() + t.getZ() * q.getX() - t.getX() * q.getZ();
	m_fZ = t.getW() * q.getZ() + t.getZ() * q.getW() + t.getX() * q.getY() - t.getY() * q.getX();
}

Quaternion& Quaternion::operator *= (const FFLOAT& val)
{
	m_fX *= val;
	m_fY *= val;
	m_fZ *= val;
	m_fW *= val;

	return *this;
}

Quaternion& Quaternion::operator /= (const FFLOAT& val)
{
	m_fX /= val;
	m_fY /= val;
	m_fZ /= val;
	m_fW /= val;

	return *this;
}

Quaternion Quaternion::operator + (const Quaternion& q) const
{
	Quaternion temp(*this);
	temp += q;

	return temp;
}

Quaternion Quaternion::operator - (const Quaternion& q) const
{
	Quaternion temp(*this);
	temp -= q;

	return temp;
}

Quaternion Quaternion::operator * (const Quaternion& q) const
{
	FFLOAT newW = (q.m_fW * m_fW) - (q.m_fX * m_fX) - (q.m_fY * m_fY) - (q.m_fZ * m_fZ);
	FFLOAT newX = (q.m_fW * m_fX) + (q.m_fX * m_fW) - (q.m_fY * m_fZ) + (q.m_fZ * m_fY);
	FFLOAT newY = (q.m_fW * m_fY) + (q.m_fX * m_fZ) + (q.m_fY * m_fW) - (q.m_fZ * m_fX);
	FFLOAT newZ = (q.m_fW * m_fZ) - (q.m_fX * m_fY) + (q.m_fY * m_fX) + (q.m_fZ * m_fW);

	return Quaternion(newX, newY, newZ, newW);
}

Quaternion Quaternion::operator / (const Quaternion& q) const
{
	// allow to divide two quaternions
	FFLOAT magnitudeSqrt = SQUARE(q.m_fX) + SQUARE(q.m_fY) + SQUARE(q.m_fY) + SQUARE(q.m_fW);

	FFLOAT newW = ((q.m_fW * m_fW) + (q.m_fX * m_fX) + (q.m_fY * m_fY) + (q.m_fZ * m_fZ)) / magnitudeSqrt;
	FFLOAT newX = ((q.m_fW * m_fX) - (q.m_fX * m_fW) - (q.m_fY * m_fZ) + (q.m_fZ * m_fY)) / magnitudeSqrt;
	FFLOAT newY = ((q.m_fW * m_fY) + (q.m_fX * m_fZ) - (q.m_fY * m_fW) - (q.m_fZ * m_fX)) / magnitudeSqrt;
	FFLOAT newZ = ((q.m_fW * m_fZ) - (q.m_fX * m_fY) + (q.m_fY * m_fX) - (q.m_fZ * m_fW)) / magnitudeSqrt;

	return Quaternion(newX, newY, newZ, newW);
}

Quaternion Quaternion::operator * (const FFLOAT& val) const
{
	Quaternion temp(*this);
	temp *= val;

	return temp;
}

Quaternion Quaternion::operator / (const FFLOAT& val) const
{
	Quaternion temp(*this);
	temp /= val;

	return temp;
}

BBOOL Quaternion::operator == (const Quaternion& q) const
{
	return (Equal<FFLOAT>(m_fX, q.m_fX) && Equal<FFLOAT>(m_fY, q.m_fY) && Equal<FFLOAT>(m_fZ, q.m_fZ) && Equal<FFLOAT>(m_fW, q.m_fW));
}

BBOOL Quaternion::operator != (const Quaternion& q) const
{
	return (Different<FFLOAT>(m_fX, q.m_fX) || Different<FFLOAT>(m_fY, q.m_fY) || Different<FFLOAT>(m_fZ, q.m_fZ) || Different<FFLOAT>(m_fW, q.m_fW));
}

Quaternion operator * (FFLOAT val, const Quaternion& q)
{
	Quaternion temp(q);
	temp *= val;

	return temp;
}
