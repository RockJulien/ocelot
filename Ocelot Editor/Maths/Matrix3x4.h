#ifndef MATRIX3X4_H
#define MATRIX3X4_H

//Spanish cow go "EL MOO"

#include "Vector3.h"
#include "Quaternion.h"

namespace Ocelot
{
	class Matrix3x4
	{
	public:
		Matrix3x4();
		Matrix3x4(const Matrix3x4& m);
		Matrix3x4(const FFLOAT _11, const FFLOAT _12, const FFLOAT _13, const FFLOAT _14,
			      const FFLOAT _21, const FFLOAT _22, const FFLOAT _23, const FFLOAT _24,
				  const FFLOAT _31, const FFLOAT _32, const FFLOAT _33, const FFLOAT _34);

		Vector3 Transform(const Vector3& v) const;
		Vector3 TransformInverse(const Vector3 &v) const;
		Vector3 TransformDirection(const Vector3 &v) const;
		Vector3 TransformDirectionInverse(const Vector3 &v) const;
		VVOID Matrix3x4Multiply(Matrix3x4 &out, const Matrix3x4 &mat);
		VVOID Matrix3x4Inverse(Matrix3x4 &out);
		FFLOAT GetDeterminant() const;
		VVOID SetOrientationAndPosition(Quaternion &q, const Vector3 &position);
		
		Vector3 GetAxisVector(BIGINT i) const;
		
		inline FFLOAT get11() const { return m_f11; }
		inline FFLOAT get21() const { return m_f21; }
		inline FFLOAT get31() const { return m_f31; }
		inline FFLOAT get12() const { return m_f12; }
		inline FFLOAT get22() const { return m_f22; }
		inline FFLOAT get32() const { return m_f32; }
		inline FFLOAT get13() const { return m_f13; }
		inline FFLOAT get23() const { return m_f23; }
		inline FFLOAT get33() const { return m_f33; }
		inline FFLOAT get14() const { return m_f14; }
		inline FFLOAT get24() const { return m_f24; }
		inline FFLOAT get34() const { return m_f34; }

		inline VVOID set11(const FFLOAT _11) { m_f11 = _11; }
		inline VVOID set12(const FFLOAT _12) { m_f12 = _12; }
		inline VVOID set13(const FFLOAT _13) { m_f13 = _13; }
		inline VVOID set14(const FFLOAT _14) { m_f14 = _14; }
		inline VVOID set21(const FFLOAT _21) { m_f21 = _21; }
		inline VVOID set22(const FFLOAT _22) { m_f22 = _22; }
		inline VVOID set23(const FFLOAT _23) { m_f23 = _23; }
		inline VVOID set24(const FFLOAT _24) { m_f24 = _24; }
		inline VVOID set31(const FFLOAT _31) { m_f31 = _31; }
		inline VVOID set32(const FFLOAT _32) { m_f32 = _32; }
		inline VVOID set33(const FFLOAT _33) { m_f33 = _33; }
		inline VVOID set34(const FFLOAT _34) { m_f34 = _34; }
	private:
		FFLOAT m_f11, m_f12, m_f13, m_f14;
		FFLOAT m_f21, m_f22, m_f23, m_f24;
		FFLOAT m_f31, m_f32, m_f33, m_f34;
	};
}

#endif