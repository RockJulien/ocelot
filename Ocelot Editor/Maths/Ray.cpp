#include "Ray.h"

using namespace Ocelot;

Ray::Ray()
{}

Ray::Ray(const Vector3 &o, const Vector3 &d)
	: origin(o), direction(d)
{}

Ray::Ray(const FFLOAT oX, const FFLOAT oY, const FFLOAT oZ, const FFLOAT dX, const FFLOAT dY, const FFLOAT dZ)
	: origin(oX, oY, oZ), direction(dX, dY, dZ)
{}

Vector3 Ray::operator() (const FFLOAT t)
{
	return origin + t * direction;
}
