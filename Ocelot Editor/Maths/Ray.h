#ifndef RAY_H
#define RAY_H

/*
	I've only added the bare minimum needed to do the intersection test.
	You'll have to add whatever else you need to the class.
*/

#include "Vector3.h"

namespace Ocelot
{
	class Ray
	{
	public:
		Ray();
		Ray(const Vector3 &o, const Vector3 &d);
		Ray(const FFLOAT oX, const FFLOAT oY, const FFLOAT oZ, const FFLOAT dX, const FFLOAT dY, const FFLOAT dZ);

		inline Vector3 GetOrigin() const { return origin; }
		inline Vector3 GetDirection() const { return direction; }
		inline Vector3 GetIntersectionPoint() const { return intersectionPoint; }

		inline VVOID SetOrigin(const Vector3 &o) { origin = o; }
		inline VVOID SetDirection(const Vector3 &d) { direction = d; }
		inline VVOID SetIntersectionPoint(const Vector3 &p) { intersectionPoint = p; }

		Vector3 operator() (const FFLOAT t);
		FFLOAT& operator[] (const BIGINT index);
	private:
		Vector3 origin;
		Vector3 direction;
		Vector3 intersectionPoint;
	};
}

#endif