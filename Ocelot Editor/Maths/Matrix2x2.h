#ifndef DEF_MATRIX2X2_H
#define DEF_MATRIX2X2_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Matrix2x2.h/.cpp
/
/ Description: This file provide a way to create a 2 by 2 matrix with its
/              basic contents expected for a matrix of this size in Games.
/              It will serve particularly for determinant computation for 
/              inverse calculation of a 3 by 3 matrix (by subdivision in 
/              2 by 2 ones) and could be used as simple matrix as well.
/
/ Author:      Larbi Julien & Carl Mitchell
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Utility.h"

class Matrix2x2
{
private:

	// attributes representing the matrix
	FFLOAT m_f11, m_f12;
	FFLOAT m_f21, m_f22;

public:

	// Constructors & Destructor
	Matrix2x2();								   // default cst
	Matrix2x2(const Matrix2x2& mat);               // copy cst
	Matrix2x2& operator = (const Matrix2x2&);      // assgm cst
	Matrix2x2(FFLOAT val);                          // overload cst
	Matrix2x2(DFLOAT val);						   // overload cst
	Matrix2x2(FFLOAT _11, FFLOAT _12,				   // overload cst
			  FFLOAT _21, FFLOAT _22);
	Matrix2x2(DFLOAT _11, DFLOAT _12,			   // overload cst
			  DFLOAT _21, DFLOAT _22);
	~Matrix2x2();

	// Methods
	VVOID      Matrix2x2Identity();					// create an identity matrix
	BBOOL      Matrix2x2IsIdentity();				// check if the matrix is an identity one
	VVOID      Matrix2x2Transpose(Matrix2x2& out);	// transpose the matrix
	VVOID      Matrix2x2Inverse(Matrix2x2& out);	    // compute the inverse of the matrix
	FFLOAT     Matrix2x2Determinant();				// compute the determinant of the matrix
	VVOID      Matrix2x2Mutliply(Matrix2x2& out, const Matrix2x2& mat);
	VVOID      Matrix2x2Mutliply(Matrix2x2& out, const Matrix2x2& mat1, const Matrix2x2& mat2);

	// Accessors
	inline FFLOAT get_11() const { return m_f11; }
	inline FFLOAT get_12() const { return m_f12; }
	inline FFLOAT get_21() const { return m_f21; }
	inline FFLOAT get_22() const { return m_f22; }
	inline VVOID  set_11(FFLOAT _11) { m_f11 = _11; }
	inline VVOID  set_12(FFLOAT _12) { m_f12 = _12; }
	inline VVOID  set_21(FFLOAT _21) { m_f21 = _21; }
	inline VVOID  set_22(FFLOAT _22) { m_f22 = _22; }

	// Operators overload
	/*********************Access Operats**************************/
	FFLOAT& operator () (BIGINT line, BIGINT col);
	FFLOAT  operator () (BIGINT line, BIGINT col) const;
	/*********************Cast Operators**************************/
	operator FFLOAT* ();              // for being able to pass it through the shader as a (FFLOAT*)
	operator const FFLOAT* () const;  // constant version
	/*********************Unary Operators*************************/
	Matrix2x2 operator + () const;
	Matrix2x2 operator - () const;
	/*********************Assgm Operators*************************/
	Matrix2x2& operator += (const Matrix2x2&);
	Matrix2x2& operator -= (const Matrix2x2&);
	Matrix2x2& operator *= (const Matrix2x2&);
	Matrix2x2& operator *= (const FFLOAT& val);
	Matrix2x2& operator /= (const FFLOAT& val);
	/*********************Binary Operators************************/
	Matrix2x2 operator + (const Matrix2x2&) const;
	Matrix2x2 operator - (const Matrix2x2&) const;
	Matrix2x2 operator * (const Matrix2x2&) const;
	Matrix2x2 operator * (const FFLOAT& val) const;
	Matrix2x2 operator / (const FFLOAT& val) const;
	BBOOL operator == (const Matrix2x2&) const;
	BBOOL operator != (const Matrix2x2&) const;
	friend Matrix2x2 operator * (FFLOAT, const Matrix2x2&);
};

#endif