#include "MatrixView.h"

MatrixView::MatrixView() :
m_vEye(0.0f), m_vAt(0.0f), m_vUp(0.0f)
{

}

MatrixView::~MatrixView()
{

}

MatrixView MatrixView::MatrixViewLookAtLH(const Vector3& eye, const Vector3& at, const Vector3& up)
{
	// prepare our matrices
	MatrixView transform;
	MatrixView view;
	transform.Matrix4x4Identity();
	view.Matrix4x4Identity();

	// save vectors:
	m_vEye = eye;
	m_vAt  = at;
	m_vUp  = up;

	Vector3 temp(up);

	// compute the representation of vectors enterred in parameter for each axis
	Vector3 axisZ = at - eye;
	axisZ = axisZ.Vec3Normalise();

	Vector3 axisX = temp.Vec3CrossProduct(axisZ);
	axisX = axisX.Vec3Normalise();

	Vector3 axisY = axisZ.Vec3CrossProduct(axisX);

	// now we have all our axes done, we can fill the matrix with

	// then, multiply the transform one with the view one which is
	// equal to make a single matrix with same components on axes 
	// inside but three dot product at the last row.
	//  ------------------------------------------------------------
	//  |     xaxis.x         yaxis.x           zaxis.x        0   |
	//  |     xaxis.y         yaxis.y           zaxis.y        0   |
	//  |     xaxis.z         yaxis.z           zaxis.z        0   |
	//  | -dot(xaxis,eye)  -dot(yaxis,eye)  -dot(zaxis,eye)    1   |
	//  ------------------------------------------------------------
	// like that we avoid dot products:
	transform.set_11(axisX.getX()); transform.set_12(axisY.getX()); transform.set_13(axisZ.getX());
	transform.set_21(axisX.getY()); transform.set_22(axisY.getY()); transform.set_23(axisZ.getY());
	transform.set_31(axisX.getZ()); transform.set_32(axisY.getZ()); transform.set_33(axisZ.getZ());

	view.set_41(-eye.getX()); view.set_42(-eye.getY()); view.set_43(-eye.getZ());

	// multiply
	view.Matrix4x4Mutliply(view, transform); // the view is automatically changed thanks to the member function.

	// test with dotProduct
	/*view.set_11(axisX.getX()); view.set_12(axisY.getX()); view.set_13(axisZ.getX());
	view.set_21(axisX.getY()); view.set_22(axisY.getY()); view.set_23(axisZ.getY());
	view.set_31(axisX.getZ()); view.set_32(axisY.getZ()); view.set_33(axisZ.getZ());
	view.set_41(-axisX.Vec3DotProduct(eye)); view.set_42(-axisY.Vec3DotProduct(eye)); view.set_43(-axisZ.Vec3DotProduct(eye));*/

	return view;
}

MatrixView MatrixView::MatrixViewLookAtRH(const Vector3& eye, const Vector3& at, const Vector3& up)
{
	// prepare our matrices
	MatrixView transform;
	MatrixView view;
	transform.Matrix4x4Identity();
	view.Matrix4x4Identity();

	// save vectors:
	m_vEye = eye;
	m_vAt  = at;
	m_vUp  = up;

	Vector3 temp(up);

	// compute the representation of vectors enterred in parameter for each axis
	Vector3 axisZ = eye - at;   // we inverse eye direction in RH coordinates
	axisZ = axisZ.Vec3Normalise();

	Vector3 axisX = temp.Vec3CrossProduct(axisZ);
	axisX = axisX.Vec3Normalise();

	Vector3 axisY = axisZ.Vec3CrossProduct(axisX);

	// now we have all our axes done, we can fill the matrix with


	// then, multiply the transform one with the view one which is
	// equal to make a single matrix with same components on axes 
	// inside but three dot product at the last row.
	//  ------------------------------------------------------------
	//  |    -xaxis.x         yaxis.x          -zaxis.x        0   |
	//  |    -xaxis.y         yaxis.y          -zaxis.y        0   |
	//  |    -xaxis.z         yaxis.z          -zaxis.z        0   |
	//  |  dot(xaxis,eye)  -dot(yaxis,eye)  -dot(zaxis,eye)    1   |
	//  ------------------------------------------------------------
	// like that we avoid dot products:
	// in this case, add an inversion at the X and Z axis.
	transform.set_11(-axisX.getX()); transform.set_12(axisY.getX()); transform.set_13(-axisZ.getX());
	transform.set_21(-axisX.getY()); transform.set_22(axisY.getY()); transform.set_23(-axisZ.getY());
	transform.set_31(-axisX.getZ()); transform.set_32(axisY.getZ()); transform.set_33(-axisZ.getZ());

	view.set_41(eye.getX()); view.set_42(-eye.getY()); view.set_43(-eye.getZ());

	// multiply
	view.Matrix4x4Mutliply(view, transform); // the view is automatically changed thanks to the member function.

	return view;
}