#include "Matrix2x2.h"

Matrix2x2::Matrix2x2() :
m_f11(0.0f), m_f12(0.0f),
m_f21(0.0f), m_f22(0.0f)
{
	
}

Matrix2x2::Matrix2x2(const Matrix2x2& mat) :
m_f11(mat.m_f11), m_f12(mat.m_f12),
m_f21(mat.m_f21), m_f22(mat.m_f22)
{

}

Matrix2x2& Matrix2x2::operator = (const Matrix2x2& mat)
{
	m_f11 = mat.m_f11;
	m_f12 = mat.m_f12;
	m_f21 = mat.m_f21;
	m_f22 = mat.m_f22;

	return *this;
}

Matrix2x2::Matrix2x2(FFLOAT val) :
m_f11(val), m_f12(val),
m_f21(val), m_f22(val)
{

}

Matrix2x2::Matrix2x2(DFLOAT val) :
m_f11((FFLOAT)val), m_f12((FFLOAT)val),
m_f21((FFLOAT)val), m_f22((FFLOAT)val)
{

}

Matrix2x2::Matrix2x2(FFLOAT _11, FFLOAT _12,	
					 FFLOAT _21, FFLOAT _22) :
m_f11(_11), m_f12(_12),
m_f21(_21), m_f22(_22)
{

}

Matrix2x2::Matrix2x2(DFLOAT _11, DFLOAT _12,
					 DFLOAT _21, DFLOAT _22) :
m_f11((FFLOAT)_11), m_f12((FFLOAT)_12),
m_f21((FFLOAT)_21), m_f22((FFLOAT)_22)
{

}

Matrix2x2::~Matrix2x2()
{

}

VVOID      Matrix2x2::Matrix2x2Identity()
{
	// initialize as an identity matrix with 1 along the main diagonal.
	m_f11 = 1.0f; m_f12 = 0.0f;
	m_f21 = 0.0f; m_f22 = 1.0f;
}

BBOOL      Matrix2x2::Matrix2x2IsIdentity()
{
	// check if the current matrix is an identity one
	Matrix2x2 temp(*this);
	Matrix2x2 temp2;
	temp2.Matrix2x2Identity();

	if(Equal<Matrix2x2>(temp, temp2))
		return true;
	return false;
}

VVOID      Matrix2x2::Matrix2x2Transpose(Matrix2x2& out)
{
	// transpose the matrix by changing rows with columns
	out.m_f11 = m_f11; out.m_f12 = m_f21;
	out.m_f21 = m_f12; out.m_f22 = m_f22;
}

FFLOAT     Matrix2x2::Matrix2x2Determinant()
{
	// compute the determinant of the matrix
	// it could be useful for inversing the matrix
	// or in the case of cofactors calcul.
	FFLOAT result = 0.0f;

	return result = (m_f11 * m_f22) -
					(m_f12 * m_f21);
}

VVOID      Matrix2x2::Matrix2x2Inverse(Matrix2x2& out)
{
	// first compute the determinant
	FFLOAT determinant = this->Matrix2x2Determinant();

	// if the determinant is not equal to zero,
	// the matrix inverse is possible.
	if(Different<FFLOAT>(determinant, 0.0f))
	{
		// swap components on the main diagonal
		// and give the opposite of two others
		// at their same place. Then, divide all
		// by the determinant.
		out.m_f11 = m_f22 / determinant;    // swap and divide
		out.m_f22 = m_f11 / determinant;    // swap and divide
		out.m_f12 = (-m_f12) / determinant; // opposite and divide
		out.m_f21 = (-m_f21) / determinant; // opposite and divide
	}
}

VVOID      Matrix2x2::Matrix2x2Mutliply(Matrix2x2& out, const Matrix2x2& mat)
{
	// Multiplication between two matrices.
	// each row is going to be multiplied by each 
	// element of the column where the value processed is.
	Matrix2x2 temp(*this);

	/*****************first row****************************/
	out.m_f11 = (temp.m_f11 * mat.m_f11) + (temp.m_f12 * mat.m_f21);
	out.m_f12 = (temp.m_f11 * mat.m_f21) + (temp.m_f12 * mat.m_f22);
	/*****************second row***************************/
	out.m_f21 = (temp.m_f21 * mat.m_f11) + (temp.m_f22 * mat.m_f21);
	out.m_f22 = (temp.m_f21 * mat.m_f12) + (temp.m_f22 * mat.m_f22);
}

VVOID      Matrix2x2::Matrix2x2Mutliply(Matrix2x2& out, const Matrix2x2& mat1, const Matrix2x2& mat2)
{
	Matrix2x2Mutliply(out, mat1);

	out.Matrix2x2Mutliply(out, mat2);
}

FFLOAT& Matrix2x2::operator () (BIGINT line, BIGINT col)
{
	Matrix2x2 temp(*this);
	if(line == 1)
	{
		if(col == 1)
		{
			return temp.m_f11;
		}
		else if(col == 2)
		{
			return temp.m_f12;
		}
	}
	else if(line == 2)
	{
		if(col == 1)
		{
			return temp.m_f21;
		}
		else if(col == 2)
		{
			return temp.m_f22;
		}
	}

	return temp.m_f11;    // return the first one in case of bounds error
}

FFLOAT  Matrix2x2::operator () (BIGINT line, BIGINT col) const
{
	Matrix2x2 temp(*this);
	if(line == 1)
	{
		if(col == 1)
		{
			return temp.m_f11;
		}
		else if(col == 2)
		{
			return temp.m_f12;
		}
	}
	else if(line == 2)
	{
		if(col == 1)
		{
			return temp.m_f21;
		}
		else if(col == 2)
		{
			return temp.m_f22;
		}
	}

	return temp.m_f11;    // return the first one in case of bounds error
}

Matrix2x2::operator FFLOAT* ()
{
	FFLOAT* mat2x2[4];
	mat2x2[0] = &m_f11;
	mat2x2[1] = &m_f12;
	mat2x2[2] = &m_f21;
	mat2x2[3] = &m_f22;

	return mat2x2[0];
}

Matrix2x2::operator const FFLOAT* () const
{
	const FFLOAT* mat2x2[4];
	mat2x2[0] = &m_f11;
	mat2x2[1] = &m_f12;
	mat2x2[2] = &m_f21;
	mat2x2[3] = &m_f22;

	return mat2x2[0];
}

Matrix2x2 Matrix2x2::operator + () const
{
	return Matrix2x2(+this->m_f11, +this->m_f12,
		             +this->m_f21, +this->m_f22);
}

Matrix2x2 Matrix2x2::operator - () const
{
	return Matrix2x2(-this->m_f11, -this->m_f12,
		             -this->m_f21, -this->m_f22);
}

Matrix2x2& Matrix2x2::operator += (const Matrix2x2& mat)
{
	m_f11 += mat.m_f11;
	m_f12 += mat.m_f12;
	m_f21 += mat.m_f21;
	m_f22 += mat.m_f22;

	return *this;
}

Matrix2x2& Matrix2x2::operator -= (const Matrix2x2& mat)
{
	m_f11 -= mat.m_f11;
	m_f12 -= mat.m_f12;
	m_f21 -= mat.m_f21;
	m_f22 -= mat.m_f22;

	return *this;
}

Matrix2x2& Matrix2x2::operator *= (const Matrix2x2& mat)
{
	// Multiplication between two matrices.
	Matrix2x2 temp(*this);

	// each row is going to be multiplied by each 
	// element of the column where the value processed is.
	/*****************first row****************************/
	m_f11 = (temp.m_f11 * mat.m_f11) + (temp.m_f12 * mat.m_f21);
	m_f12 = (temp.m_f11 * mat.m_f21) + (temp.m_f12 * mat.m_f22);
	/*****************second row***************************/
	m_f21 = (temp.m_f21 * mat.m_f11) + (temp.m_f22 * mat.m_f21);
	m_f22 = (temp.m_f21 * mat.m_f12) + (temp.m_f22 * mat.m_f22);

	return *this;
}

Matrix2x2& Matrix2x2::operator *= (const FFLOAT& val)
{
	m_f11 *= val;
	m_f12 *= val;
	m_f21 *= val;
	m_f22 *= val;

	return *this;
}

Matrix2x2& Matrix2x2::operator /= (const FFLOAT& val)
{
	m_f11 /= val;
	m_f12 /= val;
	m_f21 /= val;
	m_f22 /= val;

	return *this;
}

Matrix2x2 Matrix2x2::operator + (const Matrix2x2& mat) const
{
	Matrix2x2 temp(*this);
	temp += mat;

	return temp;
}

Matrix2x2 Matrix2x2::operator - (const Matrix2x2& mat) const
{
	Matrix2x2 temp(*this);
	temp -= mat;

	return temp;
}

Matrix2x2 Matrix2x2::operator * (const Matrix2x2& mat) const
{
	Matrix2x2 temp(*this);
	temp *= mat;

	return temp;
}

Matrix2x2 Matrix2x2::operator * (const FFLOAT& val) const
{
	Matrix2x2 temp(*this);
	temp *= val;

	return temp;
}

Matrix2x2 Matrix2x2::operator / (const FFLOAT& val) const
{
	Matrix2x2 temp(*this);
	temp /= val;

	return temp;
}

BBOOL Matrix2x2::operator == (const Matrix2x2& mat) const
{
	return (Equal<FFLOAT>(m_f11, mat.m_f11) && Equal<FFLOAT>(m_f12, mat.m_f12) &&
			Equal<FFLOAT>(m_f21, mat.m_f21) && Equal<FFLOAT>(m_f22, mat.m_f22));
}

BBOOL Matrix2x2::operator != (const Matrix2x2& mat) const
{
	return (Different<FFLOAT>(m_f11, mat.m_f11) || Different<FFLOAT>(m_f12, mat.m_f12) ||
			Different<FFLOAT>(m_f21, mat.m_f21) || Different<FFLOAT>(m_f22, mat.m_f22));
}

Matrix2x2 operator * (FFLOAT val, const Matrix2x2& mat)
{
	Matrix2x2 temp(mat);
	temp *= val;

	return temp;
}
