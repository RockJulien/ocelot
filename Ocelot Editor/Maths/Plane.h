#ifndef DEF_PLANE_H
#define DEF_PLANE_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Plane.h/.cpp
/   
/  Description: This Header allows us to create planes in a 
/               3D world by respecting the equation of a plane
/               Ax + By + Cz + D = 0 where A, B and C are one
/               of the normals of the plane.
/               Useful for a good view frustrum optimisation
/               algorithm.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

#include "Vector3.h"

namespace Ocelot
{
	class Plane
	{
	private:

		// Attributes
		FFLOAT m_fA;
		FFLOAT m_fB;
		FFLOAT m_fC;
		FFLOAT m_fD;

	public:

		// Constructors & Destructor
		Plane();
		Plane(FFLOAT a, FFLOAT b, FFLOAT c, FFLOAT d);
		~Plane();

		// Methods
		VVOID  PlaneNormalize();
		FFLOAT PlaneDotCoord(const Vector3& point);
		FFLOAT PlaneDot(const Vector4& point);
		FFLOAT PlaneDotNormal(const Vector3& point);
		VVOID  PlaneRescale(Plane& out, const FFLOAT& scale);
		VVOID  PlaneCreateFrom3Points(const Vector3& point1, const Vector3& point2, const Vector3& point3);

		// Accessors
		inline FFLOAT getA() const { return m_fA;}
		inline FFLOAT getB() const { return m_fB;}
		inline FFLOAT getC() const { return m_fC;}
		inline FFLOAT getD() const { return m_fD;}
		inline VVOID  A(FFLOAT a) { m_fA = a;}
		inline VVOID  B(FFLOAT b) { m_fB = b;}
		inline VVOID  C(FFLOAT c) { m_fC = c;}
		inline VVOID  D(FFLOAT d) { m_fD = d;}
		inline Vector3 GetNormal() 
		{ 
			PlaneNormalize();
			return  Vector3(m_fA, m_fB, m_fC); 
		}

		// Operators overload
		/*********************** Cast Operators ***********************/
		operator FFLOAT* ();
		operator const FFLOAT* () const;
		/*********************** unary operators **********************/
		Plane  operator + () const;
		Plane  operator - () const;
		/*********************** assgm operators **********************/
		Plane& operator *= (FFLOAT );
		Plane& operator /= (FFLOAT );
		/*********************** binary operators *********************/
		Plane  operator * (FFLOAT ) const;
		Plane  operator / (FFLOAT ) const;
		BBOOL   operator == (const Plane& ) const;
		BBOOL   operator != (const Plane& ) const;
		friend Plane operator * (FFLOAT , const Plane&);
	};
}

#endif