#include "Matrix3x3.h"

Matrix3x3::Matrix3x3() :
m_f11(0.0f), m_f12(0.0f), m_f13(0.0f),
m_f21(0.0f), m_f22(0.0f), m_f23(0.0f),
m_f31(0.0f), m_f32(0.0f), m_f33(0.0f)
{
	
}

Matrix3x3::Matrix3x3(const Matrix3x3& mat) :
m_f11(mat.m_f11), m_f12(mat.m_f12), m_f13(mat.m_f13),
m_f21(mat.m_f21), m_f22(mat.m_f22), m_f23(mat.m_f23),
m_f31(mat.m_f31), m_f32(mat.m_f32), m_f33(mat.m_f33)
{
	
}

Matrix3x3& Matrix3x3::operator = (const Matrix3x3& mat)
{
	m_f11 = mat.m_f11;
	m_f12 = mat.m_f12;
	m_f13 = mat.m_f13;
	m_f21 = mat.m_f21;
	m_f22 = mat.m_f22;
	m_f23 = mat.m_f23;
	m_f31 = mat.m_f31;
	m_f32 = mat.m_f32;
	m_f33 = mat.m_f33;

	return *this;
}

Matrix3x3::Matrix3x3(FFLOAT val) :
m_f11(val), m_f12(val), m_f13(val),
m_f21(val), m_f22(val), m_f23(val),
m_f31(val), m_f32(val), m_f33(val)
{

}

Matrix3x3::Matrix3x3(DFLOAT val) :
m_f11((FFLOAT)val), m_f12((FFLOAT)val), m_f13((FFLOAT)val),
m_f21((FFLOAT)val), m_f22((FFLOAT)val), m_f23((FFLOAT)val),
m_f31((FFLOAT)val), m_f32((FFLOAT)val), m_f33((FFLOAT)val)
{

}

Matrix3x3::Matrix3x3(FFLOAT _11, FFLOAT _12, FFLOAT _13,
			         FFLOAT _21, FFLOAT _22, FFLOAT _23,
			         FFLOAT _31, FFLOAT _32, FFLOAT _33) :
m_f11(_11), m_f12(_12), m_f13(_13),
m_f21(_21), m_f22(_22), m_f23(_23),
m_f31(_31), m_f32(_32), m_f33(_33)
{

}

Matrix3x3::Matrix3x3(DFLOAT _11, DFLOAT _12, DFLOAT _13,
					 DFLOAT _21, DFLOAT _22, DFLOAT _23,
					 DFLOAT _31, DFLOAT _32, DFLOAT _33) :
m_f11((FFLOAT)_11), m_f12((FFLOAT)_12), m_f13((FFLOAT)_13),
m_f21((FFLOAT)_21), m_f22((FFLOAT)_22), m_f23((FFLOAT)_23),
m_f31((FFLOAT)_31), m_f32((FFLOAT)_32), m_f33((FFLOAT)_33)
{

}

Matrix3x3::~Matrix3x3()
{

}

VVOID      Matrix3x3::Matrix3x3Identity()
{
	// initialize as an identity matrix with 1 along the main diagonal.
	m_f11 = 1.0f; m_f12 = 0.0f; m_f13 = 0.0f;
	m_f21 = 0.0f; m_f22 = 1.0f; m_f23 = 0.0f;
	m_f31 = 0.0f; m_f32 = 0.0f; m_f33 = 1.0f;
}

BBOOL      Matrix3x3::Matrix3x3IsIdentity()
{
	// check if the current matrix is an identity one
	Matrix3x3 temp(*this);
	Matrix3x3 temp2;
	temp2.Matrix3x3Identity();

	if(Equal<Matrix3x3>(temp, temp2))
		return true;
	return false;
}

VVOID	  Matrix3x3::Matrix3x3Transpose(Matrix3x3 &mat)
{
	Matrix3x3 temp(*this);

	// transpose the matrix by changing rows with columns
	mat.m_f11 = temp.m_f11; mat.m_f12 = temp.m_f21; mat.m_f13 = temp.m_f31;
	mat.m_f21 = temp.m_f12; mat.m_f22 = temp.m_f22; mat.m_f23 = temp.m_f32;
	mat.m_f31 = temp.m_f13; mat.m_f32 = temp.m_f23; mat.m_f33 = temp.m_f33;
}

FFLOAT     Matrix3x3::Matrix3x3Determinant()
{
	// compute the determinant of the matrix
	// it could be useful for inversing the matrix
	FFLOAT result = 0.0f;

	// following the sarrus method
	return result = (m_f11 * m_f22 * m_f33) +
					(m_f21 * m_f32 * m_f13) +
					(m_f31 * m_f12 * m_f23) -
					(m_f13 * m_f22 * m_f31) -
					(m_f23 * m_f32 * m_f11) -
					(m_f33 * m_f12 * m_f21);					
}

VVOID	  Matrix3x3::Matrix3x3Inverse(Matrix3x3 &mat)
{
	// first compute the determinant
	FFLOAT determinant = this->Matrix3x3Determinant();

	// if the determinant is not equal to zero,
	// the matrix inverse is possible.
	if(Different<FFLOAT>(determinant, 0.0f))
	{
		// compute Minors for each component of the matrix
		// for each element, eliminate other components on
		// the same row and column and create a new one 
		// with the rest for computing cofactors in a same
		// way that for the determinant.
		/*******************First row************************/
		Matrix2x2 minorMat_11(m_f22, m_f23, m_f32, m_f33);
		FFLOAT cofactor_11 = minorMat_11.Matrix2x2Determinant();
		Matrix2x2 minorMat_12(m_f21, m_f23, m_f31, m_f33);
		FFLOAT cofactor_12 = minorMat_12.Matrix2x2Determinant();
		Matrix2x2 minorMat_13(m_f21, m_f22, m_f31, m_f32);
		FFLOAT cofactor_13 = minorMat_13.Matrix2x2Determinant();
		/*******************Second Row***********************/
		Matrix2x2 minorMat_21(m_f12, m_f13, m_f32, m_f33);
		FFLOAT cofactor_21 = minorMat_21.Matrix2x2Determinant();
		Matrix2x2 minorMat_22(m_f11, m_f13, m_f31, m_f33);
		FFLOAT cofactor_22 = minorMat_22.Matrix2x2Determinant();
		Matrix2x2 minorMat_23(m_f11, m_f12, m_f31, m_f32);
		FFLOAT cofactor_23 = minorMat_23.Matrix2x2Determinant();
		/*******************Third Row************************/
		Matrix2x2 minorMat_31(m_f12, m_f13, m_f22, m_f23);
		FFLOAT cofactor_31 = minorMat_31.Matrix2x2Determinant();
		Matrix2x2 minorMat_32(m_f11, m_f13, m_f21, m_f23);
		FFLOAT cofactor_32 = minorMat_32.Matrix2x2Determinant();
		Matrix2x2 minorMat_33(m_f11, m_f12, m_f21, m_f22);
		FFLOAT cofactor_33 = minorMat_33.Matrix2x2Determinant();

		// Now we can rebuild the new matrix with cofactors without forgetting
		// to inverse values representing the middle diamond of the 3x3 matrix
		// and transpose it.
		// for calculating the sign to put before each cofactor, just use the
		// following calcul : (-1)^(line + col) or (C++ style) pow(-1.0f, (line + col)).
		mat.m_f11 = cofactor_11;  mat.m_f12 = -cofactor_12; mat.m_f13 = cofactor_13;
		mat.m_f21 = -cofactor_21; mat.m_f22 = cofactor_22; mat.m_f23 = -cofactor_23;
		mat.m_f31 = cofactor_31;  mat.m_f32 = -cofactor_32; mat.m_f33 = cofactor_33;

		mat.Matrix3x3Transpose(mat); // invert rows and columns.

		// to end up by dividing by the determinant.
		mat /= determinant;
	}
}

VVOID      Matrix3x3::Matrix3x3Mutliply(Matrix3x3& out, const Matrix3x3& mat)
{
	// Multiplication between two matrices.
	// each row is going to be multiplied by each 
	// element of the column where the value processed is.
	Matrix3x3 temp(*this);

	/*****************first row****************************/
	out.m_f11 = (temp.m_f11 * mat.m_f11) + (temp.m_f12 * mat.m_f21) + (temp.m_f13 * mat.m_f31);
	out.m_f12 = (temp.m_f11 * mat.m_f12) + (temp.m_f12 * mat.m_f22) + (temp.m_f13 * mat.m_f32);
	out.m_f13 = (temp.m_f11 * mat.m_f13) + (temp.m_f12 * mat.m_f23) + (temp.m_f13 * mat.m_f33);
	/*****************second row***************************/
	out.m_f21 = (temp.m_f21 * mat.m_f11) + (temp.m_f22 * mat.m_f21) + (temp.m_f23 * mat.m_f31);
	out.m_f22 = (temp.m_f21 * mat.m_f12) + (temp.m_f22 * mat.m_f22) + (temp.m_f23 * mat.m_f32);
	out.m_f23 = (temp.m_f21 * mat.m_f13) + (temp.m_f22 * mat.m_f23) + (temp.m_f23 * mat.m_f33);
	/*****************third row****************************/
	out.m_f31 = (temp.m_f31 * mat.m_f11) + (temp.m_f32 * mat.m_f21) + (temp.m_f33 * mat.m_f31);
	out.m_f32 = (temp.m_f31 * mat.m_f12) + (temp.m_f32 * mat.m_f22) + (temp.m_f33 * mat.m_f32);
	out.m_f33 = (temp.m_f31 * mat.m_f13) + (temp.m_f32 * mat.m_f23) + (temp.m_f33 * mat.m_f33);
}

VVOID      Matrix3x3::Matrix3x3Mutliply(Matrix3x3& out, const Matrix3x3& mat1, const Matrix3x3& mat2)
{
	// multiply with the first (view for example)
	Matrix3x3Mutliply(out, mat1);
	// then, multiply with the second (projection for example)
	out.Matrix3x3Mutliply(out, mat2);
}

VVOID      Matrix3x3::Matrix3x3MultiplyTranspose(Matrix3x3& out, const Matrix3x3& mat)
{
	// invert result of a multiplication by changing
	// rows with columns.
	Matrix3x3Mutliply(out, mat);

	// just add the transpose fonction into this one.
	out.Matrix3x3Transpose(out);
}

VVOID      Matrix3x3::Matrix3x3MultiplyTranspose(Matrix3x3& out, const Matrix3x3& mat1, const Matrix3x3& mat2)
{
	// same punition :-).
	Matrix3x3Mutliply(out, mat1, mat2);

	out.Matrix3x3Transpose(out);  // magic
}

VVOID      Matrix3x3::Matrix3x3Translation(Matrix3x3& out, const FFLOAT& Txy)
{
	// Declare a new matrix which will represent the
	// translation to apply.
	// and we will apply by multiply this new one 
	// with the matrix which called the function.
	Matrix3x3 matTranslation;
	matTranslation.Matrix3x3Identity();
	matTranslation.set_31(Txy);
	matTranslation.set_32(Txy);

	// now we apply
	Matrix3x3Mutliply(out, matTranslation);
}

VVOID      Matrix3x3::Matrix3x3Translation(Matrix3x3& out, const DFLOAT& Txy)
{
	// simple overload for lazy programmers.
	// Let's save some fingers and also the wish
	// to use, for some people a DFLOAT :-).
	Matrix3x3 matTranslation;
	matTranslation.Matrix3x3Identity();
	matTranslation.set_31((FFLOAT)Txy); // in all case, it will be a FFLOAT hahahaha.
	matTranslation.set_32((FFLOAT)Txy); // I am the devil :-).

	// now we apply
	Matrix3x3Mutliply(out, matTranslation);
}

VVOID      Matrix3x3::Matrix3x3Translation(Matrix3x3& out, const FFLOAT& Tx, const FFLOAT& Ty)
{
	// For less lazy guy who want to control every
	// variables in details. Better !!!
	Matrix3x3 matTranslation;
	matTranslation.Matrix3x3Identity();
	matTranslation.set_31(Tx); 
	matTranslation.set_32(Ty); 

	// now we apply
	Matrix3x3Mutliply(out, matTranslation);
}

VVOID      Matrix3x3::Matrix3x3Translation(Matrix3x3& out, const DFLOAT& Tx, const DFLOAT& Ty)
{
	// And again an overload with DFLOAT, sometimes
	// unfortunatly, some person forget the "f" for a FFLOAT
	// now it is fixed ;-).
	Matrix3x3 matTranslation;
	matTranslation.Matrix3x3Identity();
	matTranslation.set_31((FFLOAT)Tx); // In all case, it will be a FFLOAT hahahaha.
	matTranslation.set_32((FFLOAT)Ty); // I am the devil :-).

	// now we apply
	Matrix3x3Mutliply(out, matTranslation);
}

VVOID      Matrix3x3::Matrix3x3Scaling(Matrix3x3& out, const FFLOAT& Sxy)
{
	// Same as the translation one, we create
	// a scaling matrix for applying a size change
	// at the matrix which called the function.
	Matrix3x3 matScaling;
	matScaling.Matrix3x3Identity();
	matScaling.set_11(Sxy);
	matScaling.set_22(Sxy);

	// now we apply as previously
	Matrix3x3Mutliply(out, matScaling);
}

VVOID      Matrix3x3::Matrix3x3Scaling(Matrix3x3& out, const DFLOAT& Sxy)
{
	Matrix3x3 matScaling;
	matScaling.Matrix3x3Identity();
	matScaling.set_11((FFLOAT)Sxy);
	matScaling.set_22((FFLOAT)Sxy);

	// now we apply as previously
	Matrix3x3Mutliply(out, matScaling);
}

VVOID      Matrix3x3::Matrix3x3Scaling(Matrix3x3& out, const FFLOAT& Sx, const FFLOAT& Sy)
{
	Matrix3x3 matScaling;
	matScaling.Matrix3x3Identity();
	matScaling.set_11(Sx);
	matScaling.set_22(Sy);

	// now we apply as previously
	Matrix3x3Mutliply(out, matScaling);
}

VVOID      Matrix3x3::Matrix3x3Scaling(Matrix3x3& out, const DFLOAT& Sx, const DFLOAT& Sy)
{
	Matrix3x3 matScaling;
	matScaling.Matrix3x3Identity();
	matScaling.set_11((FFLOAT)Sx);
	matScaling.set_22((FFLOAT)Sy);

	// now we apply as previously
	Matrix3x3Mutliply(out, matScaling);
}

VVOID      Matrix3x3::Matrix3x3RotationX(Matrix3x3& out, const FFLOAT& Rx)
{
	// compute before for avoiding to compute twice.
	FFLOAT Cos = cos(Rx);
	FFLOAT Sin = sin(Rx);

	Matrix3x3 matRotationX;
	matRotationX.Matrix3x3Identity();
	matRotationX.set_22(Cos);
	matRotationX.set_23(Sin);
	matRotationX.set_32(-Sin);
	matRotationX.set_33(Cos);

	Matrix3x3Mutliply(out, matRotationX);
}

VVOID      Matrix3x3::Matrix3x3RotationX(Matrix3x3& out, const DFLOAT& Rx)
{
	// compute before for avoiding to compute twice.
	FFLOAT Cos = cos((FFLOAT)Rx);
	FFLOAT Sin = sin((FFLOAT)Rx);

	Matrix3x3 matRotationX;
	matRotationX.Matrix3x3Identity();
	matRotationX.set_22(Cos);
	matRotationX.set_23(Sin);
	matRotationX.set_32(-Sin);
	matRotationX.set_33(Cos);

	Matrix3x3Mutliply(out, matRotationX);
}

VVOID      Matrix3x3::Matrix3x3RotationY(Matrix3x3& out, const FFLOAT& Ry)
{
	// compute before for avoiding to compute twice.
	FFLOAT Cos = cos(Ry);
	FFLOAT Sin = sin(Ry);

	Matrix3x3 matRotationY;
	matRotationY.Matrix3x3Identity();
	matRotationY.set_11(Cos);
	matRotationY.set_13(-Sin);
	matRotationY.set_31(Sin);
	matRotationY.set_33(Cos);

	Matrix3x3Mutliply(out, matRotationY);
}

VVOID      Matrix3x3::Matrix3x3RotationY(Matrix3x3& out, const DFLOAT& Ry)
{
	// compute before for avoiding to compute twice.
	FFLOAT Cos = cos((FFLOAT)Ry);
	FFLOAT Sin = sin((FFLOAT)Ry);

	Matrix3x3 matRotationY;
	matRotationY.Matrix3x3Identity();
	matRotationY.set_11(Cos);
	matRotationY.set_13(-Sin);
	matRotationY.set_31(Sin);
	matRotationY.set_33(Cos);

	Matrix3x3Mutliply(out, matRotationY);
}

VVOID      Matrix3x3::Matrix3x3RotationZ(Matrix3x3& out, const FFLOAT& Rz)
{
	// compute before for avoiding to compute twice.
	FFLOAT Cos = cos(Rz);
	FFLOAT Sin = sin(Rz);

	Matrix3x3 matRotationZ;
	matRotationZ.Matrix3x3Identity();
	matRotationZ.set_11(Cos);
	matRotationZ.set_12(Sin);
	matRotationZ.set_21(-Sin);
	matRotationZ.set_22(Cos);

	Matrix3x3Mutliply(out, matRotationZ);
}

VVOID      Matrix3x3::Matrix3x3RotationZ(Matrix3x3& out, const DFLOAT& Rz)
{
	// compute before for avoiding to compute twice.
	FFLOAT Cos = cos((FFLOAT)Rz);
	FFLOAT Sin = sin((FFLOAT)Rz);

	Matrix3x3 matRotationZ;
	matRotationZ.Matrix3x3Identity();
	matRotationZ.set_11(Cos);
	matRotationZ.set_12(Sin);
	matRotationZ.set_21(-Sin);
	matRotationZ.set_22(Cos);

	Matrix3x3Mutliply(out, matRotationZ);
}

VVOID      Matrix3x3::Matrix3x3RotationGeneralized(Matrix3x3& out, const FFLOAT& Rxyz)
{
	// we could either create each different rotation matrix
	// on each axis and multiply them together, or calculate 
	// all in one generalized matrix but means more maths :-).
	// That is why I chose generalized one with maths :-)
	// R(alpha, beta, phi) = Rz(alpha)*Ry(beta)*Rx(phi) = below.
	Matrix3x3 matYawPitchRoll;
	matYawPitchRoll.Matrix3x3Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	FFLOAT CosAlpha = cos(Rxyz);
	FFLOAT SinAlpha = sin(Rxyz);
	FFLOAT CosBeta  = cos(Rxyz);
	FFLOAT SinBeta  = sin(Rxyz);
	FFLOAT CosPhi   = cos(Rxyz);
	FFLOAT SinPhi   = sin(Rxyz);

	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	Matrix3x3Mutliply(out, matYawPitchRoll);
}

VVOID      Matrix3x3::Matrix3x3RotationGeneralized(Matrix3x3& out, const DFLOAT& Rxyz)
{
	// the order is important. The way you multiply will 
	// determine the way it rotates.
	// DirectX made a mistake by starting with the "yaw"
	// of the "yawPitchRoll" for the X axis. The yaw is 
	// applied to the Z axis (e.g: avionic) and the "roll"
	// must be perform first.
	Matrix3x3 matYawPitchRoll;
	matYawPitchRoll.Matrix3x3Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	FFLOAT CosAlpha = cos((FFLOAT)Rxyz);
	FFLOAT SinAlpha = sin((FFLOAT)Rxyz);
	FFLOAT CosBeta  = cos((FFLOAT)Rxyz);
	FFLOAT SinBeta  = sin((FFLOAT)Rxyz);
	FFLOAT CosPhi   = cos((FFLOAT)Rxyz);
	FFLOAT SinPhi   = sin((FFLOAT)Rxyz);

	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	Matrix3x3Mutliply(out, matYawPitchRoll);
}

VVOID      Matrix3x3::Matrix3x3RotationGeneralized(Matrix3x3& out, const FFLOAT& Rx, const FFLOAT& Ry, const FFLOAT& Rz)
{
	// same overload of method as previously
	// but in this case, be aware of where you 
	// put "x" (Roll), "y" (Pitch) and "z" (Yaw) value.
	Matrix3x3 matYawPitchRoll;
	matYawPitchRoll.Matrix3x3Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	FFLOAT CosAlpha = cos(Rz);  // Yaw (Z)
	FFLOAT SinAlpha = sin(Rz);  // Yaw (Z)
	FFLOAT CosBeta  = cos(Ry);  // Pitch (Y)
	FFLOAT SinBeta  = sin(Ry);  // Pitch (Y)
	FFLOAT CosPhi   = cos(Rx);  // Roll (X)
	FFLOAT SinPhi   = sin(Rx);  // Roll (X)

	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	Matrix3x3Mutliply(out, matYawPitchRoll);
}

VVOID      Matrix3x3::Matrix3x3RotationGeneralized(Matrix3x3& out, const DFLOAT& Rx, const DFLOAT& Ry, const DFLOAT& Rz)
{
	Matrix3x3 matYawPitchRoll;
	matYawPitchRoll.Matrix3x3Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	FFLOAT CosAlpha = cos((FFLOAT)Rz);  // Yaw (Z)
	FFLOAT SinAlpha = sin((FFLOAT)Rz);  // Yaw (Z)
	FFLOAT CosBeta  = cos((FFLOAT)Ry);  // Pitch (Y)
	FFLOAT SinBeta  = sin((FFLOAT)Ry);  // Pitch (Y)
	FFLOAT CosPhi   = cos((FFLOAT)Rx);  // Roll (X)
	FFLOAT SinPhi   = sin((FFLOAT)Rx);  // Roll (X)

	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	Matrix3x3Mutliply(out, matYawPitchRoll);
}

VVOID      Matrix3x3::Matrix3x3RotationQuaternion(Matrix3x3& out, Quaternion q)
{
	// perform a rotation thanks to a quaternion
	// inserted as parameter.
	Matrix3x3 matRotation = q.QuaternionToMatrix3Normalized();

	// apply 
	Matrix3x3Mutliply(out, matRotation);
}

//Similar to above function, but acts on original matrix
VVOID	  Matrix3x3::Matrix3x3SetOrientation(Quaternion& q)
{
	Matrix3x3 orientation = q.QuaternionToMatrix3Normalized();

	*this = orientation;
}

Vector3	  Matrix3x3::Matrix3x3Transform(const Vector3 &v) const
{
	return Vector3(v.getX() * m_f11 + v.getY() * m_f12 + v.getZ() * m_f13,
		           v.getX() * m_f21 + v.getY() * m_f22 + v.getZ() * m_f23,
		           v.getX() * m_f31 + v.getY() * m_f32 + v.getZ() * m_f33);
}

FFLOAT& Matrix3x3::operator () (BIGINT line, BIGINT col)
{
	Matrix3x3 temp(*this);
	if(line == 1)
	{
		if(col == 1)
		{
			return temp.m_f11;
		}
		else if(col == 2)
		{
			return temp.m_f12;
		}
		else if(col == 3)
		{
			return temp.m_f13;
		}
	}
	else if(line == 2)
	{
		if(col == 1)
		{
			return temp.m_f21;
		}
		else if(col == 2)
		{
			return temp.m_f22;
		}
		else if(col == 3)
		{
			return temp.m_f23;
		}
	}
	else if(line == 3)
	{
		if(col == 1)
		{
			return temp.m_f31;
		}
		else if(col == 2)
		{
			return temp.m_f32;
		}
		else if(col == 3)
		{
			return temp.m_f33;
		}
	}

	return temp.m_f11;    // return the first one in case of bounds error
}

FFLOAT  Matrix3x3::operator () (BIGINT line, BIGINT col) const
{
	Matrix3x3 temp(*this);
	if(line == 1)
	{
		if(col == 1)
		{
			return temp.m_f11;
		}
		else if(col == 2)
		{
			return temp.m_f12;
		}
		else if(col == 3)
		{
			return temp.m_f13;
		}
	}
	else if(line == 2)
	{
		if(col == 1)
		{
			return temp.m_f21;
		}
		else if(col == 2)
		{
			return temp.m_f22;
		}
		else if(col == 3)
		{
			return temp.m_f23;
		}
	}
	else if(line == 3)
	{
		if(col == 1)
		{
			return temp.m_f31;
		}
		else if(col == 2)
		{
			return temp.m_f32;
		}
		else if(col == 3)
		{
			return temp.m_f33;
		}
	}

	return temp.m_f11;    // return the first one in case of bounds error
}

Matrix3x3::operator FFLOAT* ()
{
	FFLOAT* mat3x3[9];

	mat3x3[0] = &m_f11;
	mat3x3[1] = &m_f12;
	mat3x3[2] = &m_f13;
	mat3x3[3] = &m_f21;
	mat3x3[4] = &m_f22;
	mat3x3[5] = &m_f23;
	mat3x3[6] = &m_f31;
	mat3x3[7] = &m_f32;
	mat3x3[8] = &m_f33;

	return mat3x3[0];
}

Matrix3x3::operator const FFLOAT* () const
{
	const FFLOAT* mat3x3[9];

	mat3x3[0] = &m_f11;
	mat3x3[1] = &m_f12;
	mat3x3[2] = &m_f13;
	mat3x3[3] = &m_f21;
	mat3x3[4] = &m_f22;
	mat3x3[5] = &m_f23;
	mat3x3[6] = &m_f31;
	mat3x3[7] = &m_f32;
	mat3x3[8] = &m_f33;

	return mat3x3[0];
}

Matrix3x3 Matrix3x3::operator + () const
{
	return Matrix3x3(+this->m_f11, +this->m_f12, +this->m_f13,
					 +this->m_f21, +this->m_f22, +this->m_f23,
					 +this->m_f31, +this->m_f32, +this->m_f33);
}

Matrix3x3 Matrix3x3::operator - () const
{
	return Matrix3x3(-this->m_f11, -this->m_f12, -this->m_f13,
					 -this->m_f21, -this->m_f22, -this->m_f23,
					 -this->m_f31, -this->m_f32, -this->m_f33);
}

Matrix3x3& Matrix3x3::operator += (const Matrix3x3& mat)
{
	m_f11 += mat.m_f11;
	m_f12 += mat.m_f12;
	m_f13 += mat.m_f13;
	m_f21 += mat.m_f21;
	m_f22 += mat.m_f22;
	m_f23 += mat.m_f23;
	m_f31 += mat.m_f31;
	m_f32 += mat.m_f32;
	m_f33 += mat.m_f33;

	return *this;
}

Matrix3x3& Matrix3x3::operator -= (const Matrix3x3& mat)
{
	m_f11 -= mat.m_f11;
	m_f12 -= mat.m_f12;
	m_f13 -= mat.m_f13;
	m_f21 -= mat.m_f21;
	m_f22 -= mat.m_f22;
	m_f23 -= mat.m_f23;
	m_f31 -= mat.m_f31;
	m_f32 -= mat.m_f32;
	m_f33 -= mat.m_f33;

	return *this;
}

Matrix3x3& Matrix3x3::operator *= (const Matrix3x3& mat)
{
	// same as matrix4x4multiplication
	// Multiplication between two matrices.
	Matrix3x3 temp(*this);

	// each row is going to be multiplied by each 
	// element of the column where the value processed is.
	/*****************first row****************************/
	m_f11 = (temp.m_f11 * mat.m_f11) + (temp.m_f12 * mat.m_f21) + (temp.m_f13 * mat.m_f31);
	m_f12 = (temp.m_f11 * mat.m_f12) + (temp.m_f12 * mat.m_f22) + (temp.m_f13 * mat.m_f32);
	m_f13 = (temp.m_f11 * mat.m_f13) + (temp.m_f12 * mat.m_f23) + (temp.m_f13 * mat.m_f33);
	/*****************second row***************************/
	m_f21 = (temp.m_f21 * mat.m_f11) + (temp.m_f22 * mat.m_f21) + (temp.m_f23 * mat.m_f31);
	m_f22 = (temp.m_f21 * mat.m_f12) + (temp.m_f22 * mat.m_f22) + (temp.m_f23 * mat.m_f32);
	m_f23 = (temp.m_f21 * mat.m_f13) + (temp.m_f22 * mat.m_f23) + (temp.m_f23 * mat.m_f33);
	/*****************third row****************************/
	m_f31 = (temp.m_f31 * mat.m_f11) + (temp.m_f32 * mat.m_f21) + (temp.m_f33 * mat.m_f31);
	m_f32 = (temp.m_f31 * mat.m_f12) + (temp.m_f32 * mat.m_f22) + (temp.m_f33 * mat.m_f32);
	m_f33 = (temp.m_f31 * mat.m_f13) + (temp.m_f32 * mat.m_f23) + (temp.m_f33 * mat.m_f33);

	return *this;
}

Matrix3x3& Matrix3x3::operator *= (const FFLOAT& val)
{
	m_f11 *= val;
	m_f12 *= val;
	m_f13 *= val;
	m_f21 *= val;
	m_f22 *= val;
	m_f23 *= val;
	m_f31 *= val;
	m_f32 *= val;
	m_f33 *= val;

	return *this;
}

Matrix3x3& Matrix3x3::operator /= (const FFLOAT& val)
{
	m_f11 /= val;
	m_f12 /= val;
	m_f13 /= val;
	m_f21 /= val;
	m_f22 /= val;
	m_f23 /= val;
	m_f31 /= val;
	m_f32 /= val;
	m_f33 /= val;

	return *this;
}

Matrix3x3 Matrix3x3::operator + (const Matrix3x3& mat) const
{
	Matrix3x3 temp(*this);
	temp += mat;

	return temp;
}

Matrix3x3 Matrix3x3::operator - (const Matrix3x3& mat) const
{
	Matrix3x3 temp(*this);
	temp -= mat;

	return temp;
}

Matrix3x3 Matrix3x3::operator * (const Matrix3x3& mat) const
{
	Matrix3x3 temp(*this);
	temp *= mat;

	return temp;
}

Matrix3x3 Matrix3x3::operator * (const FFLOAT& val) const
{
	Matrix3x3 temp(*this);
	temp *= val;

	return temp;
}

Matrix3x3 Matrix3x3::operator / (const FFLOAT& val) const
{
	Matrix3x3 temp(*this);
	temp /= val;

	return temp;
}

BBOOL Matrix3x3::operator == (const Matrix3x3& mat) const
{
	return (Equal<FFLOAT>(m_f11, mat.m_f11) && Equal<FFLOAT>(m_f12, mat.m_f12) && Equal<FFLOAT>(m_f13, mat.m_f13) &&
		    Equal<FFLOAT>(m_f21, mat.m_f21) && Equal<FFLOAT>(m_f22, mat.m_f22) && Equal<FFLOAT>(m_f23, mat.m_f23) &&
			Equal<FFLOAT>(m_f31, mat.m_f31) && Equal<FFLOAT>(m_f32, mat.m_f32) && Equal<FFLOAT>(m_f33, mat.m_f33));
}

BBOOL Matrix3x3::operator != (const Matrix3x3& mat) const
{
	return (Different<FFLOAT>(m_f11, mat.m_f11) || Different<FFLOAT>(m_f12, mat.m_f12) || Different<FFLOAT>(m_f13, mat.m_f13) ||
		    Different<FFLOAT>(m_f21, mat.m_f21) || Different<FFLOAT>(m_f22, mat.m_f22) || Different<FFLOAT>(m_f23, mat.m_f23) ||
			Different<FFLOAT>(m_f31, mat.m_f31) || Different<FFLOAT>(m_f32, mat.m_f32) || Different<FFLOAT>(m_f33, mat.m_f33));
}

Matrix3x3 operator * (FFLOAT val, const Matrix3x3& mat)
{
	Matrix3x3 temp(mat);
	temp *= val;

	return temp;
}

VVOID Matrix3x3::Matrix3x3SetBasis(const Vector3& one, const Vector3& two, const Vector3& three)
{
	m_f11 = one.getX();
	m_f12 = two.getX();
	m_f13 = three.getX();
	m_f21 = one.getY();
	m_f22 = two.getY();
	m_f23 = three.getY();
	m_f31 = one.getZ();
	m_f32 = two.getZ();
	m_f33 = three.getZ();

	/*
	Transverse order
	m_f11 = one.getX();
	m_f12 = one.getY();
	m_f13 = one.getZ();
	m_f21 = two.getX();
	m_f22 = two.getY();
	m_f23 = two.getZ();
	m_f31 = three.getX();
	m_f32 = three.getY();
	m_f33 = three.getZ();
	*/
}

Vector3 Matrix3x3::Matrix3x3TransformTranspose(const Vector3& v)
{
	return Vector3(v.getX()*m_f11 + v.getY()*m_f21 + v.getZ()*m_f31, v.getX()*m_f12 + v.getY()*m_f22 + v.getZ()*m_f32, v.getX()*m_f13 + v.getY()*m_f23 + v.getZ()*m_f33);
}

VVOID Matrix3x3::Matrix3x3SetSkewSymetric(const Vector3& v)
{
	m_f11 = m_f22 = m_f33 = 0.0f;
	m_f12 = -v.getZ();
	m_f13 = v.getY();
	m_f21 = v.getZ();
	m_f23 = -v.getX();
	m_f31 = -v.getY();
	m_f32 = v.getX();
}