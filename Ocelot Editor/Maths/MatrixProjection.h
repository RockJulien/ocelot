#ifndef DEF_MATRIXPROJECTION_H
#define DEF_MATRIXPROJECTION_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   MatrixProjection.h/.cpp
/
/ Description: This file provide a way to create a 4 by 4 matrix projection
/              
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Matrix4x4.h"

class MatrixProjection : public Matrix4x4
{
private:

	// attributes of a projection matrix
	FFLOAT m_fWidth;
	FFLOAT m_fHeight;
	FFLOAT m_fNearPlaneDist;
	FFLOAT m_fFarPlaneDist;

	// extras for an off center projection
	// creating the the view frustrum.
	FFLOAT m_fLeftPlaneDist;
	FFLOAT m_fTopPlaneDist;
	FFLOAT m_fRightPlaneDist;
	FFLOAT m_fBottomPlaneDist;

	// extras for a FOV determined projection
	FFLOAT m_fFieldOfView;
	FFLOAT m_fRatio;

public:

	// Constructor & Destructor
	MatrixProjection();
	~MatrixProjection();

	// Methods
	/***********************Orthographic projection*********************/   // (2D Games)
	MatrixProjection MatrixProjectionOrthoLH(const FFLOAT& width, const FFLOAT& height, const FFLOAT& nearPlaneDist, const FFLOAT& farPlaneDist);
	MatrixProjection MatrixProjectionOrthoRH(const FFLOAT& width, const FFLOAT& height, const FFLOAT& nearPlaneDist, const FFLOAT& farPlaneDist);
	MatrixProjection MatrixProjectionOrthoExCentredLH(const FFLOAT& leftDist, const FFLOAT& topDist, const FFLOAT& rightDist, const FFLOAT& bottomDist, const FFLOAT& nearPlaneDist, const FFLOAT& farPlaneDist);
	MatrixProjection MatrixProjectionOrthoExCentredRH(const FFLOAT& leftDist, const FFLOAT& topDist, const FFLOAT& rightDist, const FFLOAT& bottomDist, const FFLOAT& nearPlaneDist, const FFLOAT& farPlaneDist);
	/***********************Perspective projection**********************/   // (3D Games)
	MatrixProjection MatrixProjectionPerspectiveLH(const FFLOAT& width, const FFLOAT& height, const FFLOAT& nearPlaneDist, const FFLOAT& farPlaneDist);
	MatrixProjection MatrixProjectionPerspectiveRH(const FFLOAT& width, const FFLOAT& height, const FFLOAT& nearPlaneDist, const FFLOAT& farPlaneDist);
	MatrixProjection MatrixProjectionPerspectiveFOVLH(const FFLOAT& fieldOfView, const FFLOAT& ratio, const FFLOAT& nearPlaneDist, const FFLOAT& farPlaneDist);
	MatrixProjection MatrixProjectionPerspectiveFOVRH(const FFLOAT& fieldOfView, const FFLOAT& ratio, const FFLOAT& nearPlaneDist, const FFLOAT& farPlaneDist);
	MatrixProjection MatrixProjectionPerspectiveExCentredLH(const FFLOAT& leftDist, const FFLOAT& topDist, const FFLOAT& rightDist, const FFLOAT& bottomDist, const FFLOAT& nearPlaneDist, const FFLOAT& farPlaneDist);
	MatrixProjection MatrixProjectionPerspectiveExCentredRH(const FFLOAT& leftDist, const FFLOAT& topDist, const FFLOAT& rightDist, const FFLOAT& bottomDist, const FFLOAT& nearPlaneDist, const FFLOAT& farPlaneDist);

	// Accessors
	FFLOAT getWidth() const { return m_fWidth; }
	FFLOAT getHeight() const { return m_fHeight; }
	FFLOAT getNearPlaneDist() const { return m_fNearPlaneDist; }
	FFLOAT getFarPlaneDist() const { return m_fFarPlaneDist; }
	FFLOAT getLeftPlaneDist() const { return m_fLeftPlaneDist; }
	FFLOAT getTopPlaneDist() const { return m_fTopPlaneDist; }
	FFLOAT getRightPlaneDist() const { return m_fRightPlaneDist; }
	FFLOAT getBottomPlaneDist() const { return m_fBottomPlaneDist; }
	FFLOAT getFOV() const { return m_fFieldOfView; }
	FFLOAT getRatio() const { return m_fRatio; }
	/*VVOID  setWidth(FFLOAT width);
	VVOID  setHeight(FFLOAT height);
	VVOID  setNearPlaneDist(FFLOAT dist);
	VVOID  setFarPlaneDist(FFLOAT dist);
	VVOID  setLeftPlaneDist(FFLOAT pos);
	VVOID  setTopPlaneDist(FFLOAT pos);
	VVOID  setRightPlaneDist(FFLOAT pos);     // not needed
	VVOID  setBottomPlaneDist(FFLOAT pos);
	VVOID  setFOV(FFLOAT angle);
	VVOID  setRatio(FFLOAT rat);*/
};

#endif