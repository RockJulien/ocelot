#ifndef DEF_MATRIX3X3_H
#define DEF_MATRIX3X3_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Matrix3x3.h/.cpp
/
/ Description: This file provide a way to create a 3 by 3 matrix with its
/              basic contents and extras for applying rotation or whatever
/              transformation expected for 2D Games as well.
/              It will serve for determinant computation for inverse 
/              calculation of a 4 by 4 matrix (by subdivision in 3 by 3 ones) 
/              and could be used as simple matrix as well.
/              The possibility to use quaternion for applying rotations has 
/              been added even if rotation is not really possible in every
/              direction.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

//#include "Utility.h"
#include "Matrix2x2.h"
#include "Quaternion.h"

class Quaternion;
class Vector3;

class Matrix3x3
{
private:

	// variables which represent the matrix
	FFLOAT m_f11, m_f12, m_f13;
	FFLOAT m_f21, m_f22, m_f23;
	FFLOAT m_f31, m_f32, m_f33;

public:

	// Constructors & Destructor
	Matrix3x3();                                   // default cst
	Matrix3x3(const Matrix3x3& mat);               // copy cst
	Matrix3x3& operator = (const Matrix3x3&);      // assgm cst
	Matrix3x3(FFLOAT val);                          // overload cst
	Matrix3x3(DFLOAT val);						   // overload cst
	Matrix3x3(FFLOAT _11, FFLOAT _12, FFLOAT _13,     // overload cst
			  FFLOAT _21, FFLOAT _22, FFLOAT _23,
			  FFLOAT _31, FFLOAT _32, FFLOAT _33);
	Matrix3x3(DFLOAT _11, DFLOAT _12, DFLOAT _13,  // overload cst
		      DFLOAT _21, DFLOAT _22, DFLOAT _23,
			  DFLOAT _31, DFLOAT _32, DFLOAT _33);
	~Matrix3x3();

	// Methods
	Vector3	  Matrix3x3Transform(const Vector3 &v) const;
	VVOID      Matrix3x3Identity();					// create an identity matrix
	BBOOL      Matrix3x3IsIdentity();				// check if the matrix is an identity one
	FFLOAT     Matrix3x3Determinant();				// compute the determinant of the matrix
	VVOID	  Matrix3x3Transpose(Matrix3x3 &mat);	// transpose the matrix
	VVOID	  Matrix3x3Inverse(Matrix3x3 &mat);		// compute the inverse of the matrix
	VVOID      Matrix3x3Mutliply(Matrix3x3& out, const Matrix3x3& mat);
	VVOID      Matrix3x3Mutliply(Matrix3x3& out, const Matrix3x3& mat1, const Matrix3x3& mat2);
	VVOID      Matrix3x3MultiplyTranspose(Matrix3x3& out, const Matrix3x3& mat);
	VVOID      Matrix3x3MultiplyTranspose(Matrix3x3& out, const Matrix3x3& mat1, const Matrix3x3& mat2);
	VVOID      Matrix3x3Translation(Matrix3x3& out, const FFLOAT& Txy);
	VVOID      Matrix3x3Translation(Matrix3x3& out, const DFLOAT& Txy);
	VVOID      Matrix3x3Translation(Matrix3x3& out, const FFLOAT& Tx, const FFLOAT& Ty);
	VVOID      Matrix3x3Translation(Matrix3x3& out, const DFLOAT& Tx, const DFLOAT& Ty);
	VVOID      Matrix3x3Scaling(Matrix3x3& out, const FFLOAT& Sxy);
	VVOID      Matrix3x3Scaling(Matrix3x3& out, const DFLOAT& Sxy);
	VVOID      Matrix3x3Scaling(Matrix3x3& out, const FFLOAT& Sx, const FFLOAT& Sy);
	VVOID      Matrix3x3Scaling(Matrix3x3& out, const DFLOAT& Sx, const DFLOAT& Sy);
	VVOID      Matrix3x3RotationX(Matrix3x3& out, const FFLOAT& Rx);
	VVOID      Matrix3x3RotationX(Matrix3x3& out, const DFLOAT& Rx);
	VVOID      Matrix3x3RotationY(Matrix3x3& out, const FFLOAT& Ry);
	VVOID      Matrix3x3RotationY(Matrix3x3& out, const DFLOAT& Ry);
	VVOID      Matrix3x3RotationZ(Matrix3x3& out, const FFLOAT& Rz);
	VVOID      Matrix3x3RotationZ(Matrix3x3& out, const DFLOAT& Rz);
	VVOID      Matrix3x3RotationGeneralized(Matrix3x3& out, const FFLOAT& Rxyz);
	VVOID      Matrix3x3RotationGeneralized(Matrix3x3& out, const DFLOAT& Rxyz);
	VVOID      Matrix3x3RotationGeneralized(Matrix3x3& out, const FFLOAT& Rx, const FFLOAT& Ry, const FFLOAT& Rz);
	VVOID      Matrix3x3RotationGeneralized(Matrix3x3& out, const DFLOAT& Rx, const DFLOAT& Ry, const DFLOAT& Rz);
	VVOID      Matrix3x3RotationQuaternion(Matrix3x3& out, Quaternion q);
	VVOID	  Matrix3x3SetOrientation(Quaternion& q);
	VVOID	  Matrix3x3SetBasis(const Vector3& one, const Vector3& two, const Vector3& three);
	Vector3	  Matrix3x3TransformTranspose(const Vector3& v);
	VVOID	  Matrix3x3SetSkewSymetric(const Vector3& v);
	// Accessors
	inline FFLOAT get_11() const { return m_f11; }
	inline FFLOAT get_12() const { return m_f12; }
	inline FFLOAT get_13() const { return m_f13; }
	inline FFLOAT get_21() const { return m_f21; }
	inline FFLOAT get_22() const { return m_f22; }
	inline FFLOAT get_23() const { return m_f23; }
	inline FFLOAT get_31() const { return m_f31; }
	inline FFLOAT get_32() const { return m_f32; }
	inline FFLOAT get_33() const { return m_f33; }
	inline VVOID  set_11(FFLOAT _11) { m_f11 = _11; }
	inline VVOID  set_12(FFLOAT _12) { m_f12 = _12; }
	inline VVOID  set_13(FFLOAT _13) { m_f13 = _13; }
	inline VVOID  set_21(FFLOAT _21) { m_f21 = _21; }
	inline VVOID  set_22(FFLOAT _22) { m_f22 = _22; }
	inline VVOID  set_23(FFLOAT _23) { m_f23 = _23; }
	inline VVOID  set_31(FFLOAT _31) { m_f31 = _31; }
	inline VVOID  set_32(FFLOAT _32) { m_f32 = _32; }
	inline VVOID  set_33(FFLOAT _33) { m_f33 = _33; }

	// Operators overload
	/*********************Access Operats**************************/
	FFLOAT& operator () (BIGINT line, BIGINT col);
	FFLOAT  operator () (BIGINT line, BIGINT col) const;
	/*********************Cast Operators**************************/
	operator FFLOAT* ();              // for being able to pass it through the shader as a (FFLOAT*)
	operator const FFLOAT* () const;  // constant version
	/*********************Unary Operators*************************/
	Matrix3x3 operator + () const;
	Matrix3x3 operator - () const;
	/*********************Assgm Operators*************************/
	Matrix3x3& operator += (const Matrix3x3&);
	Matrix3x3& operator -= (const Matrix3x3&);
	Matrix3x3& operator *= (const Matrix3x3&);
	Matrix3x3& operator *= (const FFLOAT& val);
	Matrix3x3& operator /= (const FFLOAT& val);
	/*********************Binary Operators************************/
	Matrix3x3 operator + (const Matrix3x3&) const;
	Matrix3x3 operator - (const Matrix3x3&) const;
	Matrix3x3 operator * (const Matrix3x3&) const;
	Matrix3x3 operator * (const FFLOAT& val) const;
	Matrix3x3 operator / (const FFLOAT& val) const;
	BBOOL operator == (const Matrix3x3&) const;
	BBOOL operator != (const Matrix3x3&) const;
	friend Matrix3x3 operator * (FFLOAT, const Matrix3x3&);
};

#endif