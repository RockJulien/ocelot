#include "Vector2.h"

Vector2::Vector2() :
m_fX(0.0f), m_fY(0.0f)
{

}

Vector2::Vector2(const Vector2& vec) :
m_fX(vec.getX()), m_fY(vec.getY())
{

}

Vector2& Vector2::operator = (const Vector2& vec)
{
	m_fX = vec.getX();
	m_fY = vec.getY();

	return *this;
}

Vector2::Vector2(FFLOAT xy) :
m_fX(xy), m_fY(xy)
{

}

Vector2::Vector2(DFLOAT xy) :
m_fX((FFLOAT)xy), m_fY((FFLOAT)xy)
{

}

Vector2::Vector2(FFLOAT  x, FFLOAT  y) :
m_fX(x), m_fY(y)
{

}

Vector2::Vector2(DFLOAT x, DFLOAT y) :
m_fX((FFLOAT)x), m_fY((FFLOAT)y)
{

}

Vector2::~Vector2()
{

}

FFLOAT   Vector2::Vec2Length()
{
	// calculate the length of a vector
	return sqrt((SQUARE(m_fX)) + (SQUARE(m_fY)));
}

FFLOAT   Vector2::Vec2LengthSqrt()
{
	// calculate the length square of a vector
	return (SQUARE(m_fX)) + (SQUARE(m_fY));
}

FFLOAT   Vector2::Vec2DistanceBetween(const Vector2& target)
{
	// compute the physical distance between two vectors
	FFLOAT newX = target.getX() - m_fX;
	FFLOAT newY = target.getY() - m_fY;

	return sqrt((SQUARE(newX)) + (SQUARE(newY)));
}

Vector2 Vector2::Vec2Normalise()
{
	// normalize the vector by dividing each component by the length of the vector
	FFLOAT magnitude = Vec2Length();

	if(fabsf(magnitude) > FLT_EPSILON) // prevent from dividing by 0
	{
		this->m_fX /= magnitude;
		this->m_fY /= magnitude;
	}

	return Vector2(m_fX, m_fY);
}

FFLOAT   Vector2::Vec2DotProduct(const Vector2& vec)
{
	// simple but vectors need to be normalized first for allowing this simplicity.
	return ((m_fX * vec.getX()) + (m_fY * vec.getY()));
}

Vector2 Vector2::Vec2CrossProduct(const Vector2& adjVec2)
{
	// return the perpendicular of the two vectors (more a hack)
	return Vector2(-m_fY, m_fX);
}

Vector2 Vector2::Vec2Addition(const Vector2& vecToAdd)
{
	// add two vectors together
	return Vector2(m_fX + vecToAdd.getX(), m_fY + vecToAdd.getY());
}

Vector2 Vector2::Vec2Subtraction(const Vector2& vecToSub)
{
	// subtract two vectors together
	return Vector2(m_fX - vecToSub.getX(), m_fY - vecToSub.getY());
}

Vector2 Vector2::Vec2LinearInterpolation(const Vector2& vecToJoin, FFLOAT var)
{
	// interpolate between two vectors thanks to "var".
	// if "var" equal 0, result is vecOrigine and if "var" equal 1, result is vecToJoin.
	Vector2 origine(*this);

	Vector2 result = origine + (var * (vecToJoin - origine));
	return  result;
}

Vector2 Vector2::Vec2HermiteSplineInterpolation(const Vector2& tangStartPoint, const Vector2& vecToJoin, const Vector2& tangVecToJoin, FFLOAT var)
{
	// This interpolation between two pairs of vector/tangent is also made thanks to "var" which variate from 0 to 1.
	// If "var" equal 0, result is vecOrigine and if "var" equal 1, result is vecToJoin.
	Vector2 origine(*this);

	// calculation of coefficients
	FFLOAT coef0 = (2.0f * pow(var, 3)) - (3.0f * pow(var, 2)) + 1.0f;
	FFLOAT coef1 = pow(var, 3) - (2.0f * pow(var, 2)) + var;
	FFLOAT coef2 = -(2.0f * pow(var, 3)) + (3.0f * pow(var, 2));
	FFLOAT coef3 = pow(var, 3) - pow(var, 2);

	Vector2 result = (coef0 * origine) + (coef1 * tangStartPoint) + (coef2 * vecToJoin) + (coef3 * tangVecToJoin);
	return  result;
}

Vector2 Vector2::Vec2CatmullSplineInterpolation(const Vector2& vecControl1, const Vector2& vecToJoin, const Vector2& vecControl2, FFLOAT var)
{
	// interpolation between two vectors thanks to "var".
	// Two additional vectors are for controlling on each side the shape
	// of the interpolation. If "var" equal 0, result is origine and if "var" equal 1, result is vecToJoin.
	Vector2 origine(*this);

	// computation of coefficients
	FFLOAT coef0 = -(pow(var, 3)) + (2.0f * pow(var, 2)) - var;
	FFLOAT coef1 = (3.0f * pow(var, 3)) - (5.0f * pow(var, 2)) + 2.0f;
	FFLOAT coef2 = -(3.0f * pow(var, 3)) + (4.0f * pow(var, 2)) + var;
	FFLOAT coef3 = pow(var, 3) - pow(var,2);

	Vector2 result = 0.5f * ((coef0 * vecControl1)  + (coef1 * origine) + (coef2 * vecToJoin) + (coef3 * vecControl2));
	return  result;
}

Vector2 Vector2::Vec2BarycentricInterpolation(const Vector2& vecAtBar, const Vector2& vecToJoin, FFLOAT baryVar1, FFLOAT baryVar2)
{
	// interpolation between three vectors thanks to two barycentric coordinates "baryVar1" and "baryVar2".
	// If "baryVar1" and "baryVar2" are both equal to 0, result is vecOrigine, if "baryVar1" equal 1 and "baryVar2" equal 0, result is vecAtBar,
	// and if opposite, result is vecToJoin.
	Vector2 origine(*this);

	Vector2 result = origine + (baryVar1 * (vecAtBar - origine)) + (baryVar2 * (vecToJoin - origine));
	return  result;
}

Vector2 Vector2::Vec2VecMatrixProduct(const Matrix3x3& mat)
{
	Vector2 temp;

	temp.m_fX = (this->m_fX * mat.get_11()) + (this->m_fY * mat.get_21()) + mat.get_31();
	temp.m_fY = (this->m_fX * mat.get_12()) + (this->m_fY * mat.get_22()) + mat.get_32();

	return temp;
}

Vector2  Vector2::operator + () const
{
	return Vector2(+this->m_fX, +this->m_fY);
}

Vector2  Vector2::operator - () const
{
	return Vector2(-this->m_fX, -this->m_fY);
}

Vector2& Vector2::operator += (const Vector2& vec)
{
	m_fX += vec.getX();
	m_fY += vec.getY();

	return *this;
}

Vector2& Vector2::operator -= (const Vector2& vec)
{
	m_fX -= vec.getX();
	m_fY -= vec.getY();

	return *this;
}

Vector2& Vector2::operator *= (const FFLOAT& val)
{
	m_fX *= val;
	m_fY *= val;

	return *this;
}

Vector2& Vector2::operator /= (const FFLOAT& val)
{
	m_fX /= val;
	m_fY /= val;

	return *this;
}

Vector2  Vector2::operator +  (const Vector2& vec) const
{
	Vector2 temp(*this);
	temp += vec;

	return temp;
}

Vector2  Vector2::operator -  (const Vector2& vec) const
{
	Vector2 temp(*this);
	temp -= vec;

	return temp;
}

Vector2  Vector2::operator *  (const FFLOAT& val) const
{
	Vector2 temp(*this);
	temp *= val;

	return temp;
}

Vector2  Vector2::operator /  (const FFLOAT& val) const
{
	Vector2 temp(*this);
	temp /= val;

	return temp;
}

BBOOL     Vector2::operator == (const Vector2& vec) const
{
	return (Equal<FFLOAT>(m_fX, vec.getX()) && Equal<FFLOAT>(m_fY, vec.getY()));
}

BBOOL     Vector2::operator != (const Vector2& vec) const
{
	return (Different<FFLOAT>(m_fX, vec.getX()) || Different<FFLOAT>(m_fY, vec.getY()));
}

Vector2 operator * (FFLOAT val, const Vector2& vec)
{
	Vector2 temp(vec);
	temp *= val;

	return temp;
}
