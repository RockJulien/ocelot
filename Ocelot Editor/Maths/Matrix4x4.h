#ifndef DEF_MATRIX4X4_H
#define DEF_MATRIX4X4_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Matrix4x4.h/.cpp
/
/ Description: This file provide a way to create a 4 by 4 matrix with its
/              basic contents and extras for applying rotation or whatever
/              transformation expected for 3D Games as well.
/              It will serve for Rotation, Translation, Scaling a object in
/              a 3D world and could eb used as simple matrix.
/              The possibility to use quaternion for applying rotations has 
/              been added.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

//#include "Utility.h"
#include "Quaternion.h"
#include "Vector3.h"
#include "Vector4.h"
//#include "Matrix3x3.h"

class Vector3;
class Quaternion;

class Matrix4x4
{
protected:

	// attributes representing the matrix
	FFLOAT m_f11, m_f12, m_f13, m_f14;
	FFLOAT m_f21, m_f22, m_f23, m_f24;
	FFLOAT m_f31, m_f32, m_f33, m_f34;
	FFLOAT m_f41, m_f42, m_f43, m_f44;

public:

	// Constructors & Destructor
	Matrix4x4();											  // default cst
	Matrix4x4(const Matrix4x4& mat);                          // copy cst
	Matrix4x4& operator = (const Matrix4x4&);                 // assgm cst
	Matrix4x4(FFLOAT val);                                     // overload cst
	Matrix4x4(DFLOAT val);						              // overload cst
	Matrix4x4(FFLOAT _11, FFLOAT _12, FFLOAT _13, FFLOAT _14,     // overload cst
			  FFLOAT _21, FFLOAT _22, FFLOAT _23, FFLOAT _24,
			  FFLOAT _31, FFLOAT _32, FFLOAT _33, FFLOAT _34,
			  FFLOAT _41, FFLOAT _42, FFLOAT _43, FFLOAT _44);
	Matrix4x4(DFLOAT _11, DFLOAT _12, DFLOAT _13, DFLOAT _14, // overload cst
		      DFLOAT _21, DFLOAT _22, DFLOAT _23, DFLOAT _24,
			  DFLOAT _31, DFLOAT _32, DFLOAT _33, DFLOAT _34,
			  DFLOAT _41, DFLOAT _42, DFLOAT _43, DFLOAT _44);
	~Matrix4x4();

	// Methods
	VVOID      Matrix4x4Identity();					// create an identity matrix
	BBOOL      Matrix4x4IsIdentity() const;			// check if the matrix is an identity one
	FFLOAT     Matrix4x4Determinant() const;		// compute the determinant of the matrix
	VVOID      Matrix4x4Transpose(Matrix4x4& out) const; // transpose the matrix
	VVOID      Matrix4x4Inverse(Matrix4x4& out) const;	// compute the inverse of the matrix
	VVOID      Matrix4x4Mutliply(Matrix4x4& out, const Matrix4x4& mat);
	VVOID      Matrix4x4Mutliply(Matrix4x4& out, const Matrix4x4& mat1, const Matrix4x4& mat2);
	VVOID      Matrix4x4MultiplyTranspose(Matrix4x4& out, const Matrix4x4& mat);
	VVOID      Matrix4x4MultiplyTranspose(Matrix4x4& out, const Matrix4x4& mat1, const Matrix4x4& mat2);
	VVOID      Matrix4x4Translation(Matrix4x4& out, const FFLOAT& Txyz);
	VVOID      Matrix4x4Translation(Matrix4x4& out, const DFLOAT& Txyz);
	VVOID      Matrix4x4Translation(Matrix4x4& out, const FFLOAT& Tx, const FFLOAT& Ty, const FFLOAT& Tz);
	VVOID      Matrix4x4Translation(Matrix4x4& out, const DFLOAT& Tx, const DFLOAT& Ty, const DFLOAT& Tz);
	VVOID      Matrix4x4Scaling(Matrix4x4& out, const FFLOAT& Sxyz);
	VVOID      Matrix4x4Scaling(Matrix4x4& out, const DFLOAT& Sxyz);
	VVOID      Matrix4x4Scaling(Matrix4x4& out, const FFLOAT& Sx, const FFLOAT& Sy, const FFLOAT& Sz);
	VVOID      Matrix4x4Scaling(Matrix4x4& out, const DFLOAT& Sx, const DFLOAT& Sy, const DFLOAT& Sz);
	VVOID      Matrix4x4RotationX(Matrix4x4& out, const FFLOAT& Rx);
	VVOID      Matrix4x4RotationX(Matrix4x4& out, const DFLOAT& Rx);
	VVOID      Matrix4x4RotationY(Matrix4x4& out, const FFLOAT& Ry);
	VVOID      Matrix4x4RotationY(Matrix4x4& out, const DFLOAT& Ry);
	VVOID      Matrix4x4RotationZ(Matrix4x4& out, const FFLOAT& Rz);
	VVOID      Matrix4x4RotationZ(Matrix4x4& out, const DFLOAT& Rz);
	VVOID      Matrix4x4RotationAxis(Matrix4x4& out, Vector3 axis, FFLOAT angle);
	VVOID      Matrix4x4RotationGeneralized(Matrix4x4& out, const FFLOAT& Rxyz);
	VVOID      Matrix4x4RotationGeneralized(Matrix4x4& out, const DFLOAT& Rxyz);
	VVOID      Matrix4x4RotationGeneralized(Matrix4x4& out, const FFLOAT& Rx, const FFLOAT& Ry, const FFLOAT& Rz);
	VVOID      Matrix4x4RotationGeneralized(Matrix4x4& out, const DFLOAT& Rx, const DFLOAT& Ry, const DFLOAT& Rz);
	VVOID      Matrix4x4RotationQuaternion(Matrix4x4& out, Quaternion q);
	static VVOID Matrix4x4AffineTransformation(Matrix4x4& pOut, const Vector3& pScaling, const Vector3& pRotationOrigin, const Quaternion& pRotation, const Vector3& pTranslation);
	Vector4	  Transform(const Vector4& v);

	// Accessors
	inline FFLOAT get_11() const { return m_f11; }
	inline FFLOAT get_12() const { return m_f12; }
	inline FFLOAT get_13() const { return m_f13; }
	inline FFLOAT get_14() const { return m_f14; }
	inline FFLOAT get_21() const { return m_f21; }
	inline FFLOAT get_22() const { return m_f22; }
	inline FFLOAT get_23() const { return m_f23; }
	inline FFLOAT get_24() const { return m_f24; }
	inline FFLOAT get_31() const { return m_f31; }
	inline FFLOAT get_32() const { return m_f32; }
	inline FFLOAT get_33() const { return m_f33; }
	inline FFLOAT get_34() const { return m_f34; }
	inline FFLOAT get_41() const { return m_f41; }
	inline FFLOAT get_42() const { return m_f42; }
	inline FFLOAT get_43() const { return m_f43; }
	inline FFLOAT get_44() const { return m_f44; }
	inline VVOID  set_11(FFLOAT _11) { m_f11 = _11; }
	inline VVOID  set_12(FFLOAT _12) { m_f12 = _12; }
	inline VVOID  set_13(FFLOAT _13) { m_f13 = _13; }
	inline VVOID  set_14(FFLOAT _14) { m_f14 = _14; }
	inline VVOID  set_21(FFLOAT _21) { m_f21 = _21; }
	inline VVOID  set_22(FFLOAT _22) { m_f22 = _22; }
	inline VVOID  set_23(FFLOAT _23) { m_f23 = _23; }
	inline VVOID  set_24(FFLOAT _24) { m_f24 = _24; }
	inline VVOID  set_31(FFLOAT _31) { m_f31 = _31; }
	inline VVOID  set_32(FFLOAT _32) { m_f32 = _32; }
	inline VVOID  set_33(FFLOAT _33) { m_f33 = _33; }
	inline VVOID  set_34(FFLOAT _34) { m_f34 = _34; }
	inline VVOID  set_41(FFLOAT _41) { m_f41 = _41; }
	inline VVOID  set_42(FFLOAT _42) { m_f42 = _42; }
	inline VVOID  set_43(FFLOAT _43) { m_f43 = _43; }
	inline VVOID  set_44(FFLOAT _44) { m_f44 = _44; }

	// Operators overload
	/*********************Access Operats**************************/
	FFLOAT& operator () (BIGINT line, BIGINT col);
	FFLOAT  operator () (BIGINT line, BIGINT col) const;
	/*********************Cast Operators**************************/
	operator FFLOAT* ();              // for being able to pass it through the shader as a (FFLOAT*)
	operator const FFLOAT* () const;  // constant version
	/*********************Unary Operators*************************/
	Matrix4x4 operator + () const;
	Matrix4x4 operator - () const;
	/*********************Assgm Operators*************************/
	Matrix4x4& operator += (const Matrix4x4&);
	Matrix4x4& operator -= (const Matrix4x4&);
	Matrix4x4& operator *= (const Matrix4x4&);
	Matrix4x4& operator *= (const FFLOAT& val);
	Matrix4x4& operator /= (const FFLOAT& val);
	/*********************Binary Operators************************/
	Matrix4x4 operator + (const Matrix4x4&) const;
	Matrix4x4 operator - (const Matrix4x4&) const;
	Matrix4x4 operator * (const Matrix4x4&) const;
	Matrix4x4 operator * (const FFLOAT& val) const;
	Matrix4x4 operator / (const FFLOAT& val) const;
	BBOOL operator == (const Matrix4x4&) const;
	BBOOL operator != (const Matrix4x4&) const;
	friend Matrix4x4 operator * (FFLOAT, const Matrix4x4&);
};

#endif