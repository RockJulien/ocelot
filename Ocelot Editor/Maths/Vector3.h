#ifndef DEF_VECTOR3_H
#define DEF_VECTOR3_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Vector3.h/.cpp
/
/ Description: This file provide a way to create a 3D vector with its
/              basic contents Dot, Cross products and extras such as
/              four interpolation function between vectors and product
/              with matrixes as well.
/              It will serve especially for 3D Games as point for position
/              or direction vector for lights for example.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Utility.h"
#include "Matrix3x3.h"
#include "Matrix4x4.h"
#include "Vector4.h"

class Matrix3x3;
class Matrix4x4;
class Vector4;

class Vector3
{
private:

	// Attributes
	FFLOAT m_fX;
	FFLOAT m_fY;
	FFLOAT m_fZ;

public:

	// Constructors & Destructor
	Vector3();								 // default cst
	Vector3(const Vector3&);                 // copy cst
	Vector3& operator = (const Vector3&);    // assignment cst
	Vector3(FFLOAT xyz);                      // overload cst
	Vector3(DFLOAT xyz);                     // overload cst
	Vector3(FFLOAT  x, FFLOAT  y, FFLOAT  z);   // overload cst
	Vector3(DFLOAT x, DFLOAT y, DFLOAT z);   // overload cst
	~Vector3();

	// Methods
	VVOID	 Clear();

	// Static
	static Vector3  Vec3Zero();
	static Vector3  Vec3TrueInt();
	static Vector3  Vec3Less(const Vector3& pA, const Vector3& pB);
	static Vector3  Vec3Greater(const Vector3& pA, const Vector3& pB);
	static Vector3  Vec3LessOrEqual(const Vector3& pA, const Vector3& pB);
	static Vector3  Vec3AndInt(const Vector3& pA, const Vector3& pB);
	static Vector3  Vec3OrInt(const Vector3& pA, const Vector3& pB);
	static BBOOL    Vec3EqualInt(const Vector3& pA, const Vector3& pB);
	static BBOOL    Vec3NotEqualInt(const Vector3& pA, const Vector3& pB);
	static Vector3  Vec3Sqrt(const Vector3& pVec);
	static Vector3  Vec3ReciprocalSqrt(const Vector3& pVec);

	FFLOAT   Vec3Length();
	FFLOAT   Vec3LengthSquared();
	FFLOAT   Vec3DistanceBetween(const Vector3& target);
	Vector3  Vec3Normalise();
	FFLOAT   Vec3DotProduct(const Vector3& vec);
	Vector3  Vec3CrossProduct(const Vector3& adjVec2);
	Vector3  Vec3Addition(const Vector3& vecToAdd);
	Vector3  Vec3Subtraction(const Vector3& vecToSub);
	Vector3  Vec3LinearInterpolation(const Vector3& vecToJoin, FFLOAT var);
	Vector3  Vec3HermiteSplineInterpolation(const Vector3& tangStartPoint, const Vector3& vecToJoin, const Vector3& tangVecToJoin, FFLOAT var);
	Vector3  Vec3CatmullSplineInterpolation(const Vector3& vecControl1, const Vector3& vecToJoin, const Vector3& vecControl2, FFLOAT var);
	Vector3  Vec3BarycentricInterpolation(const Vector3& vecAtBar, const Vector3& vecToJoin, FFLOAT baryVar1, FFLOAT baryVar2);
	Vector3  Vec3VecMatrixProduct(const Matrix3x3& mat);
	Vector4  Vec3VecMatrixProduct(const Matrix4x4& mat);
	Vector4  Vec3VecMatrixTransformNormal(const Matrix4x4& mat);
	static Vector3 Vec4ToVec3(const Vector4& vect);

	// Accessors
	inline FFLOAT getX() const { return m_fX; }
	inline FFLOAT getY() const { return m_fY; }
	inline FFLOAT getZ() const { return m_fZ; }
	inline VVOID  x(const FFLOAT X) { m_fX = X; }
	inline VVOID  y(const FFLOAT Y) { m_fY = Y; }
	inline VVOID  z(const FFLOAT Z) { m_fZ = Z; }
	inline VVOID  set(const BIGINT index, const FFLOAT value) { if(index == 0) { m_fX = value; } else if(index == 1) { m_fY = value; } else if(index == 2) { m_fZ = value; }  }
	// operators overload
	/*********************Cast Operators**************************/
	operator FFLOAT* ();              // for being able to pass it through the shader as a (FFLOAT*)
	operator const FFLOAT* () const;  // constant version
	/*********************** unary operators **********************/
	Vector3  operator + () const;
	Vector3  operator - () const;
	/*********************** assgm operators **********************/
	Vector3& operator += (const Vector3&);
	Vector3& operator -= (const Vector3&);
	Vector3& operator *= (const FFLOAT& val);
	Vector3& operator /= (const FFLOAT& val);
	/*********************** binary operators *********************/
	Vector3  operator +  (const Vector3&) const;
	Vector3  operator -  (const Vector3&) const;
	Vector3  operator *  (const FFLOAT& val) const;
	Vector3  operator *  (const Vector3&) const;
	Vector3  operator /  (const FFLOAT& val) const;
	BBOOL     operator == (const Vector3&) const;
	BBOOL     operator != (const Vector3&) const;
	FFLOAT    operator [] (const BIGINT index);
	friend Vector3 operator * (FFLOAT, const Vector3&);
};

#endif