#include "Color.h"

using namespace Ocelot;

Color::Color() :
m_fRed(0.0f), m_fGreen(0.0f), m_fBlue(0.0f), m_fAlpha(1.0f)
{

}

Color::Color(FFLOAT r, FFLOAT g, FFLOAT b) :
m_fRed(r), m_fGreen(g), m_fBlue(b), m_fAlpha(1.0f)
{

}

Color::Color(FFLOAT r, FFLOAT g, FFLOAT b, FFLOAT a) :
m_fRed(r), m_fGreen(g), m_fBlue(b), m_fAlpha(a)
{

}

Color::~Color()
{

}

Color::operator FFLOAT* ()
{
	FFLOAT* col[4];

	col[0] = &m_fRed;
	col[1] = &m_fGreen;
	col[2] = &m_fBlue;
	col[3] = &m_fAlpha;

	return col[0]; // return the first element in memory
}

Color::operator const FFLOAT* () const
{
	const FFLOAT* col[4];

	col[0] = &m_fRed;
	col[1] = &m_fGreen;
	col[2] = &m_fBlue;
	col[3] = &m_fAlpha;

	return col[0]; // return the first element in memory
}

Color  Color::operator + () const
{
	return Color(+m_fRed, +m_fGreen, +m_fBlue, +m_fAlpha);
}

Color  Color::operator - () const
{
	return Color(-m_fRed, -m_fGreen, -m_fBlue, -m_fAlpha);
}

Color& Color::operator += (const Color& col)
{
	m_fRed += col.m_fRed;
	if(m_fRed > 1.0f) m_fRed = 1.0f;
	m_fGreen += col.m_fGreen;
	if(m_fGreen > 1.0f) m_fGreen = 1.0f;
	m_fBlue += col.m_fBlue;
	if(m_fBlue > 1.0f) m_fBlue = 1.0f;
	m_fAlpha = Higher<FFLOAT>(m_fAlpha, col.m_fAlpha);

	return *this;
}

Color& Color::operator -= (const Color& col)
{
	m_fRed -= col.m_fRed;
	if(m_fRed < 0.0f) m_fRed = 0.0f;
	m_fGreen -= col.m_fGreen;
	if(m_fGreen < 0.0f) m_fGreen = 0.0f;
	m_fBlue -= col.m_fBlue;
	if(m_fBlue < 0.0f) m_fBlue = 0.0f;
	m_fAlpha = Higher<FFLOAT>(m_fAlpha, col.m_fAlpha);

	return *this;
}

Color& Color::operator *= (const FFLOAT& val)
{
	m_fRed *= val;
	if(m_fRed > 1.0f) m_fRed = 1.0f;
	m_fGreen *= val;
	if(m_fGreen > 1.0f) m_fGreen = 1.0f;
	m_fBlue *= val;
	if(m_fBlue > 1.0f) m_fBlue = 1.0f;
	m_fAlpha = Higher<FFLOAT>(m_fAlpha, (val * m_fAlpha));

	return *this;
}

Color& Color::operator /= (const FFLOAT& val)
{
	m_fRed /= val;
	if(m_fRed < 0.0f) m_fRed = 0.0f;
	m_fGreen /= val;
	if(m_fGreen < 0.0f) m_fGreen = 0.0f;
	m_fBlue /= val;
	if(m_fBlue < 0.0f) m_fBlue = 0.0f;
	m_fAlpha = Higher<FFLOAT>(m_fAlpha, (val * m_fAlpha));

	return *this;
}

Color  Color::operator +  (const Color& col) const
{
	Color temp(*this);
	temp += col;

	return temp;
}

Color  Color::operator -  (const Color& col) const
{
	Color temp(*this);
	temp -= col;

	return temp;
}

Color  Color::operator *  (const FFLOAT& val) const
{
	Color temp(*this);
	temp *= val;

	return temp;
}

Color  Color::operator /  (const FFLOAT& val) const
{
	Color temp(*this);
	temp /= val;

	return temp;
}

BBOOL   Color::operator == (const Color& col) const
{
	return (Equal<FFLOAT>(m_fRed, col.m_fRed) && Equal<FFLOAT>(m_fGreen, col.m_fGreen) && Equal<FFLOAT>(m_fBlue, col.m_fBlue) && Equal<FFLOAT>(m_fAlpha, col.m_fAlpha));
}

BBOOL   Color::operator != (const Color& col) const
{
	return (Different<FFLOAT>(m_fRed, col.m_fRed) || Different<FFLOAT>(m_fGreen, col.m_fGreen) || Different<FFLOAT>(m_fBlue, col.m_fBlue) || Different<FFLOAT>(m_fAlpha, col.m_fAlpha));
}

Color operator * (FFLOAT val, const Color& col)
{
	Color temp(col);
	temp *= val;

	return temp;
}
