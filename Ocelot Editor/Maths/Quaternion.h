#ifndef DEF_QUATERNION_H
#define DEF_QUATERNION_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Quaternion.h/.cpp
/
/ Description: This file provide a way to create Quaternion with its
/              basic contents and extras for applying rotation to a 3D 
/              object through a matrix on every axis we would like.
/              A way to pass from Euler to Quaternion angles, Quaternion
/              to matrices 3 by 3 or 4 by 4 has been added, so that Log,
/              Exp, and Pow function calculation of a quaternion and five
/              interpolation processes.
/              Note: for avoiding square root calculation when normalizing
/              NoNormalized or Normalized methods have been implemented.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Utility.h"
#include "Vector3.h"
#include "Matrix3x3.h"
#include "Matrix4x4.h"

class Matrix3x3;
class Matrix4x4;
class Vector3;

class Quaternion
{
private:

	// attributes of a quaternion which are like 
	// a vector4 with common methods like Dot prod
	// but will behave more like a matrix4x1.
	FFLOAT m_fX;   // imaginary part
	FFLOAT m_fY;   // imaginary part
	FFLOAT m_fZ;   // imaginary part
	FFLOAT m_fW;   // real part

public:

	// Constructors & Destructor
	Quaternion();										     // default cst
	Quaternion(const Quaternion& q);					     // copy cst
	Quaternion& operator = (const Quaternion&);		         // assgm cst
	Quaternion(FFLOAT xyzw);						             // overload cst
	Quaternion(DFLOAT xyzw);						         // overload cst
	Quaternion(FFLOAT x, FFLOAT y, FFLOAT z, FFLOAT w);	         // overload cst
	Quaternion(DFLOAT x, DFLOAT y, DFLOAT z, DFLOAT w);      // overload cst
	Quaternion(FFLOAT headingAngle, FFLOAT attitudeAngle, FFLOAT bankAngle);    // overload cst
	Quaternion(DFLOAT headingAngle, DFLOAT attitudeAngle, DFLOAT bankAngle); // overload cst
	Quaternion(const Vector3&);							     // overload cst
	Quaternion(const Matrix4x4&);						     // overload cst (could be a Matrix3x3 ? : minimum)
	Quaternion(const Matrix3x3&);						     // overload cst
	~Quaternion();

	// Methods
	Quaternion QuaternionIdentity();                         // set the quaternion with a 1 on the real part.
	FFLOAT      QuaternionLength();                           // length of the quaternion
	FFLOAT      QuaternionLengthSqrt();					     // length square of the quaternion
	Quaternion QuaternionNormalize();					     // normalize the quaternion
	Quaternion QuaternionInverse();                          // compute the inverse of the quaternion
	Quaternion QuaternionConjuguate();                       // compute the conjuguate of the quaternion.
	Quaternion QuaternionExponantial();                      // compute the exponential of the quaternion.
	Quaternion QuaternionLogarithm();                        // compute the logarithm of the quaternion.
	Quaternion QuaternionPowerScalar(const FFLOAT& power);    // compute the quaternion at the power scalar enterred as parameter.
	Quaternion QuaternionPowerQuaternion(const Quaternion&); // compute the quaternion at the power quaternion enterred as parameter.
	FFLOAT      QuaternionDotProduct(const Quaternion& q);    // apply a dot product to the quaternion.
	Vector3    QuaternionToVectorTransformAxis(const Vector3& axis);
	Vector3    QuaternionToEulerNoNormalized();			     // convert a quaternion non normalized into euler space (return a vector)
	Vector3    QuaternionToEulerNormalized();                // convert a quaternion normalized into euler space
	Matrix3x3  QuaternionToMatrix3NoNormalized();		     // return a matrix3x3 with a non normalized quaternion
	Matrix3x3  QuaternionToMatrix3Normalized();              // return a matrix3x3 with a normalized quaternion
	Matrix4x4  QuaternionToMatrix4NoNormalized();            // same but return a matrix4x4
	Matrix4x4  QuaternionToMatrix4Normalized();              // same but return a matrix4x4
	Quaternion QuaternionLinearInterpolation(Quaternion to, FFLOAT var);  // Simple Linear Interpolation
	Quaternion QuaternionSLinearInterpolation(Quaternion to, FFLOAT var, BBOOL shortestPath = true);	 // Spherical Linear Interpolation
	Quaternion QuaternionCLinearInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, FFLOAT var, BBOOL shortestPath = false);  // Cubic Linear Interpolation
	Quaternion QuaternionCastleJauInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, FFLOAT var);  // de CastleJau algorithm for interpolating
	Quaternion QuaternionBézierInterpolation(Quaternion ctrlFrom, Quaternion ctrlTo, Quaternion to, FFLOAT var, BBOOL shortestPath = true);  // Bézier Interpolation
	VVOID	   RotateByVector(const Vector3& v);
	VVOID	   AddScaledVector(const Vector3& v, const FFLOAT scale);
	VVOID       QuaternionRotateFromAxis(Quaternion& result, const Vector3& axis, FFLOAT angle);

	// Accessors
	FFLOAT getX() const { return m_fX; }
	FFLOAT getY() const { return m_fY; }
	FFLOAT getZ() const { return m_fZ; }
	FFLOAT getW() const { return m_fW; }
	VVOID  x(const FFLOAT X) { m_fX = X; }
	VVOID  y(const FFLOAT Y) { m_fY = Y; }
	VVOID  z(const FFLOAT Z) { m_fZ = Z; }
	VVOID  w(const FFLOAT W) { m_fW = W; }

	// Operators overload
	/*********************Unary Operators*************************/
	Quaternion operator + () const;
	Quaternion operator - () const;
	/*********************Assgm Operators*************************/
	VVOID		operator *= (const Quaternion&);
	Quaternion& operator += (const Quaternion&);
	Quaternion& operator -= (const Quaternion&);
	Quaternion& operator *= (const FFLOAT& val);
	Quaternion& operator /= (const FFLOAT& val);
	/*********************Binary Operators************************/
	Quaternion operator + (const Quaternion&) const;
	Quaternion operator - (const Quaternion&) const;
	Quaternion operator * (const Quaternion&) const;
	Quaternion operator / (const Quaternion&) const;
	Quaternion operator * (const FFLOAT& val) const;
	Quaternion operator / (const FFLOAT& val) const;
	BBOOL operator == (const Quaternion&) const;
	BBOOL operator != (const Quaternion&) const;
	friend Quaternion operator * (FFLOAT, const Quaternion&);
};

#endif