#include "Vector3.h"

Vector3::Vector3() :
m_fX(0.0f), m_fY(0.0f), m_fZ(0.0f)
{

}

Vector3::Vector3(const Vector3& vec) :
m_fX(vec.getX()), m_fY(vec.getY()), m_fZ(vec.getZ())
{

}

Vector3& Vector3::operator = (const Vector3& vec)
{
	m_fX = vec.getX();
	m_fY = vec.getY();
	m_fZ = vec.getZ();

	return *this;
}

Vector3::Vector3(FFLOAT xyz) :
m_fX(xyz), m_fY(xyz), m_fZ(xyz)
{

}

Vector3::Vector3(DFLOAT xyz) :
m_fX((FFLOAT)xyz), m_fY((FFLOAT)xyz), m_fZ((FFLOAT)xyz)
{

}

Vector3::Vector3(FFLOAT  x, FFLOAT  y, FFLOAT  z) :
m_fX(x), m_fY(y), m_fZ(z)
{

}

Vector3::Vector3(DFLOAT x, DFLOAT y, DFLOAT z) :
m_fX((FFLOAT)x), m_fY((FFLOAT)y), m_fZ((FFLOAT)z)
{

}

Vector3::~Vector3()
{

}

FFLOAT   Vector3::Vec3Length()
{
	// calculate the length of a vector
	return sqrt((SQUARE(m_fX)) + (SQUARE(m_fY)) + (SQUARE(m_fZ)));
}

FFLOAT   Vector3::Vec3LengthSquared()
{
	// calculate the length square of a vector
	return (SQUARE(m_fX)) + (SQUARE(m_fY)) + (SQUARE(m_fZ));
}

FFLOAT   Vector3::Vec3DistanceBetween(const Vector3& target)
{
	// compute the physical distance between two vectors
	FFLOAT newX = target.getX() - m_fX;
	FFLOAT newY = target.getY() - m_fY;
	FFLOAT newZ = target.getZ() - m_fZ;

	return sqrt((SQUARE(newX)) + (SQUARE(newY)) + (SQUARE(newZ)));
}

Vector3 Vector3::Vec3Normalise()
{
	// normalize the vector by dividing each component by the length of the vector
	FFLOAT magnitude = Vec3Length();

	if(fabsf(magnitude) > FLT_EPSILON) // prevent from dividing by 0
	{
		this->m_fX /= magnitude;
		this->m_fY /= magnitude;
		this->m_fZ /= magnitude;
	}

	return Vector3(m_fX, m_fY, m_fZ);
}

FFLOAT   Vector3::Vec3DotProduct(const Vector3& vec)
{
	// simple but vectors need to be normalized first for allowing this simplicity.
	return ((m_fX * vec.getX()) + (m_fY * vec.getY()) + (m_fZ * vec.getZ()));
}

Vector3 Vector3::Vec3CrossProduct(const Vector3& adjVec2)
{
	// return the perpendicular of the two vectors
	return Vector3((m_fY * adjVec2.m_fZ) - (m_fZ * adjVec2.m_fY), (m_fZ * adjVec2.m_fX) - (m_fX * adjVec2.m_fZ), (m_fX * adjVec2.m_fY) - (m_fY * adjVec2.m_fX));
}

Vector3 Vector3::Vec3Addition(const Vector3& vecToAdd)
{
	// add two vectors together
	return Vector3(m_fX + vecToAdd.getX(), m_fY + vecToAdd.getY(), m_fZ + vecToAdd.getZ());
}

Vector3 Vector3::Vec3Subtraction(const Vector3& vecToSub)
{
	// subtract two vectors together
	return Vector3(m_fX - vecToSub.getX(), m_fY - vecToSub.getY(), m_fZ - vecToSub.getZ());
}

Vector3 Vector3::Vec3LinearInterpolation(const Vector3& vecToJoin, FFLOAT var)
{
	// interpolate between two vectors thanks to "var".
	// if "var" equal 0, result is vecOrigine and if "var" equal 1, result is vecToJoin.
	Vector3 origine(*this);

	Vector3 result = origine + (var * (vecToJoin - origine));
	return  result;
}

Vector3 Vector3::Vec3HermiteSplineInterpolation(const Vector3& tangStartPoint, const Vector3& vecToJoin, const Vector3& tangVecToJoin, FFLOAT var)
{
	// This interpolation between two pairs of vector/tangent is also made thanks to "var" which variate from 0 to 1.
	// If "var" equal 0, result is vecOrigine and if "var" equal 1, result is vecToJoin.
	Vector3 origine(*this);

	// calculation of coefficients
	FFLOAT coef0 = (2.0f * pow(var, 3)) - (3.0f * pow(var, 2)) + 1.0f;
	FFLOAT coef1 = pow(var, 3) - (2.0f * pow(var, 2)) + var;
	FFLOAT coef2 = -(2.0f * pow(var, 3)) + (3.0f * pow(var, 2));
	FFLOAT coef3 = pow(var, 3) - pow(var, 2);

	Vector3 result = (coef0 * origine) + (coef1 * tangStartPoint) + (coef2 * vecToJoin) + (coef3 * tangVecToJoin);
	return  result;
}

Vector3 Vector3::Vec3CatmullSplineInterpolation(const Vector3& vecControl1, const Vector3& vecToJoin, const Vector3& vecControl2, FFLOAT var)
{
	// interpolation between two vectors thanks to "var".
	// Two additional vectors are for controlling on each side the shape
	// of the interpolation. If "var" equal 0, result is origine and if "var" equal 1, result is vecToJoin.
	Vector3 origine(*this);

	// computation of coefficients
	FFLOAT coef0 = -(pow(var, 3)) + (2.0f * pow(var, 2)) - var;
	FFLOAT coef1 = (3.0f * pow(var, 3)) - (5.0f * pow(var, 2)) + 2.0f;
	FFLOAT coef2 = -(3.0f * pow(var, 3)) + (4.0f * pow(var, 2)) + var;
	FFLOAT coef3 = pow(var, 3) - pow(var,2);

	Vector3 result = 0.5f * ((coef0 * vecControl1)  + (coef1 * origine) + (coef2 * vecToJoin) + (coef3 * vecControl2));
	return  result;
}

Vector3 Vector3::Vec3BarycentricInterpolation(const Vector3& vecAtBar, const Vector3& vecToJoin, FFLOAT baryVar1, FFLOAT baryVar2)
{
	// interpolation between three vectors thanks to two barycentric coordinates "baryVar1" and "baryVar2".
	// If "baryVar1" and "baryVar2" are both equal to 0, result is vecOrigine, if "baryVar1" equal 1 and "baryVar2" equal 0, result is vecAtBar,
	// and if opposite, result is vecToJoin.
	Vector3 origine(*this);

	Vector3 result = origine + (baryVar1 * (vecAtBar - origine)) + (baryVar2 * (vecToJoin - origine));
	return  result;
}

Vector3 Vector3::Vec3VecMatrixProduct(const Matrix3x3& mat)
{
	Vector3 temp;

	temp.m_fX = (m_fX * mat.get_11()) + (m_fY * mat.get_21()) + (m_fZ * mat.get_31());
	temp.m_fY = (m_fX * mat.get_12()) + (m_fY * mat.get_22()) + (m_fZ * mat.get_32());
	temp.m_fZ = (m_fX * mat.get_13()) + (m_fY * mat.get_23()) + (m_fZ * mat.get_33());
	
	return temp;
}

Vector4 Vector3::Vec3VecMatrixProduct(const Matrix4x4& mat)
{
	Vector4 temp;

	// this vector has to be a point or the result will be wrong
	temp.x((m_fX * mat.get_11()) + (m_fY * mat.get_21()) + (m_fZ * mat.get_31()) + mat.get_41());
	temp.y((m_fX * mat.get_12()) + (m_fY * mat.get_22()) + (m_fZ * mat.get_32()) + mat.get_42());
	temp.z((m_fX * mat.get_13()) + (m_fY * mat.get_23()) + (m_fZ * mat.get_33()) + mat.get_43());
	temp.w(1.0f);

	return temp;
}

Vector4 Vector3::Vec3VecMatrixTransformNormal(const Matrix4x4& mat)
{
	Vector4 temp;

	// this vector is a direction, so W = 0 and no translations.
	// Furthermore, if we want to transform a normal, we have to
	// pass the transpose of the inverse instead. (so, just inverse it,
	// because the transpose is done inside automatically).
	temp.x((m_fX * mat.get_11()) + (m_fY * mat.get_21()) + (m_fZ * mat.get_31()));
	temp.y((m_fX * mat.get_12()) + (m_fY * mat.get_22()) + (m_fZ * mat.get_32()));
	temp.z((m_fX * mat.get_13()) + (m_fY * mat.get_23()) + (m_fZ * mat.get_33()));
	temp.w(0.0f);

	return temp;
}

Vector3 Vector3::Vec4ToVec3(const Vector4& vect)
{
	return Vector3(vect.getX(), vect.getY(), vect.getZ());
}

Vector3::operator FFLOAT* ()
{
	FFLOAT* matVec3[3];

	matVec3[0] = &m_fX;
	matVec3[1] = &m_fY;
	matVec3[2] = &m_fZ;

	return matVec3[0];
}

Vector3::operator const FFLOAT* () const
{
	const FFLOAT* matVec3[3];

	matVec3[0] = &m_fX;
	matVec3[1] = &m_fY;
	matVec3[2] = &m_fZ;

	return matVec3[0];
}

Vector3  Vector3::operator + () const
{
	return Vector3(+this->m_fX, +this->m_fY, +this->m_fZ);
}

Vector3  Vector3::operator - () const
{
	return Vector3(-this->m_fX, -this->m_fY, -this->m_fZ);
}

Vector3& Vector3::operator += (const Vector3& vec)
{
	m_fX += vec.getX();
	m_fY += vec.getY();
	m_fZ += vec.getZ();

	return *this;
}

Vector3& Vector3::operator -= (const Vector3& vec)
{
	m_fX -= vec.getX();
	m_fY -= vec.getY();
	m_fZ -= vec.getZ();

	return *this;
}

Vector3& Vector3::operator *= (const FFLOAT& val)
{
	m_fX *= val;
	m_fY *= val;
	m_fZ *= val;

	return *this;
}

Vector3& Vector3::operator /= (const FFLOAT& val)
{
	m_fX /= val;
	m_fY /= val;
	m_fZ /= val;

	return *this;
}

Vector3  Vector3::operator +  (const Vector3& vec) const
{
	Vector3 temp(*this);
	temp += vec;

	return temp;
}

Vector3  Vector3::operator -  (const Vector3& vec) const
{
	Vector3 temp(*this);
	temp -= vec;

	return temp;
}

Vector3  Vector3::operator *  (const FFLOAT& val) const
{
	Vector3 temp(*this);
	temp *= val;

	return temp;
}

Vector3  Vector3::operator *  (const Vector3& vec) const
{
	return Vector3(this->m_fX * vec.m_fX, this->m_fY * vec.m_fY, this->m_fZ * vec.m_fZ);
}

Vector3  Vector3::operator /  (const FFLOAT& val) const
{
	Vector3 temp(*this);
	temp /= val;

	return temp;
}

BBOOL     Vector3::operator == (const Vector3& vec) const
{
	return (Equal<FFLOAT>(m_fX, vec.getX()) && Equal<FFLOAT>(m_fY, vec.getY()) && Equal<FFLOAT>(m_fZ, vec.getZ()));
}

BBOOL     Vector3::operator != (const Vector3& vec) const
{
	return (Different<FFLOAT>(m_fX, vec.getX()) || Different<FFLOAT>(m_fY, vec.getY()) || Different<FFLOAT>(m_fZ, vec.getZ()));
}

Vector3 operator * (FFLOAT val, const Vector3& vec)
{
	Vector3 temp(vec);
	temp *= val;

	return temp;
}

VVOID Vector3::Clear()
{
	m_fX = 0.0f;
	m_fY = 0.0f;
	m_fZ = 0.0f;
}

Vector3  Vector3::Vec3Zero()
{
	return Vector3(0.0f);
}

Vector3  Vector3::Vec3TrueInt()
{
	return Vector3((FFLOAT)0xFFFFFFFFU, (FFLOAT)0xFFFFFFFFU, (FFLOAT)0xFFFFFFFFU);
}

Vector3  Vector3::Vec3Less(const Vector3& pA, const Vector3& pB)
{
	return Vector3((pA.getX() < pB.getX()) ? (FFLOAT)0xFFFFFFFF : 0.0f,
				   (pA.getY() < pB.getY()) ? (FFLOAT)0xFFFFFFFF : 0.0f,
				   (pA.getZ() < pB.getZ()) ? (FFLOAT)0xFFFFFFFF : 0.0f);
}

Vector3  Vector3::Vec3Greater(const Vector3& pA, const Vector3& pB)
{
	return Vector3((pA.getX() > pB.getX()) ? (FFLOAT)0xFFFFFFFF : 0.0f,
				   (pA.getY() > pB.getY()) ? (FFLOAT)0xFFFFFFFF : 0.0f,
				   (pA.getZ() > pB.getZ()) ? (FFLOAT)0xFFFFFFFF : 0.0f);
}

Vector3  Vector3::Vec3LessOrEqual(const Vector3& pA, const Vector3& pB)
{
	return Vector3((pA.getX() <= pB.getX()) ? (FFLOAT)0xFFFFFFFF : 0.0f,
				   (pA.getY() <= pB.getY()) ? (FFLOAT)0xFFFFFFFF : 0.0f,
				   (pA.getZ() <= pB.getZ()) ? (FFLOAT)0xFFFFFFFF : 0.0f);
}

Vector3  Vector3::Vec3AndInt(const Vector3& pA, const Vector3& pB)
{
	return Vector3((FFLOAT)((BIGINT)pA.getX() & (BIGINT)pB.getX()),
				   (FFLOAT)((BIGINT)pA.getY() & (BIGINT)pB.getY()),
				   (FFLOAT)((BIGINT)pA.getZ() & (BIGINT)pB.getZ()));
}

Vector3  Vector3::Vec3OrInt(const Vector3& pA, const Vector3& pB)
{
	return Vector3((FFLOAT)((BIGINT)pA.getX() | (BIGINT)pB.getX()),
				   (FFLOAT)((BIGINT)pA.getY() | (BIGINT)pB.getY()),
				   (FFLOAT)((BIGINT)pA.getZ() | (BIGINT)pB.getZ()));
}

BBOOL    Vector3::Vec3EqualInt(const Vector3& pA, const Vector3& pB)
{
	return Vector3((pA.getX() == pB.getX()) ? (FFLOAT)0xFFFFFFFF : 0.0f,
				   (pA.getY() == pB.getY()) ? (FFLOAT)0xFFFFFFFF : 0.0f,
				   (pA.getZ() == pB.getZ()) ? (FFLOAT)0xFFFFFFFF : 0.0f);
}

BBOOL    Vector3::Vec3NotEqualInt(const Vector3& pA, const Vector3& pB)
{
	return ((((BIGINT)pA.getX() != (BIGINT)pB.getX()) || ((BIGINT)pA.getY() != (BIGINT)pB.getY()) || ((BIGINT)pA.getZ() != (BIGINT)pB.getZ())) != 0);
}

Vector3  Vector3::Vec3Sqrt(const Vector3& pVec)
{
	return Vector3(sqrtf(pVec.getX()), sqrtf(pVec.getY()), sqrtf(pVec.getZ()));
}

Vector3  Vector3::Vec3ReciprocalSqrt(const Vector3& pVec)
{
	return Vector3(ReciprocalSqrt(pVec.getX()), ReciprocalSqrt(pVec.getY()), ReciprocalSqrt(pVec.getZ()));
}

FFLOAT Vector3::operator [] (const BIGINT index)
{
	FFLOAT value = 0.0f;
	switch(index)
	{
	case 0:
		value = m_fX;
		break;
	case 1:
		value = m_fY;
		break;
	case 2:
		value = m_fZ;
		break;
	default:
		break;
	}
	return value;
}
