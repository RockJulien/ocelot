#ifndef DEF_VECTOR2_H
#define DEF_VECTOR2_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Vector2.h/.cpp
/
/ Description: This file provide a way to create a 2D vector with its
/              basic contents Dot, Cross products and extras such as
/              four interpolation function between vectors and product
/              with matrixes as well.
/              It will serve especially for 2D Games or for storing 
/              Textures in 3D Games for example.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Utility.h"
#include "Matrix3x3.h"

class Vector2
{
private:

	FFLOAT m_fX;
	FFLOAT m_fY;

public:

	// Constructors & Destructor
	Vector2();								 // default cst
	Vector2(const Vector2&);                 // copy cst
	Vector2& operator = (const Vector2&);    // assignment cst
	Vector2(FFLOAT xy);                       // overload cst
	Vector2(DFLOAT xy);                      // overload cst
	Vector2(FFLOAT  x, FFLOAT  y);             // overload cst
	Vector2(DFLOAT x, DFLOAT y);             // overload cst
	~Vector2();

	// Methods
	FFLOAT   Vec2Length();
	FFLOAT   Vec2LengthSqrt();
	FFLOAT   Vec2DistanceBetween(const Vector2& target);
	Vector2 Vec2Normalise();
	FFLOAT   Vec2DotProduct(const Vector2& vec);
	Vector2 Vec2CrossProduct(const Vector2& adjVec2);
	Vector2 Vec2Addition(const Vector2& vecToAdd);
	Vector2 Vec2Subtraction(const Vector2& vecToSub);
	Vector2 Vec2LinearInterpolation(const Vector2& vecToJoin, FFLOAT var);
	Vector2 Vec2HermiteSplineInterpolation(const Vector2& tangStartPoint, const Vector2& vecToJoin, const Vector2& tangVecToJoin, FFLOAT var);
	Vector2 Vec2CatmullSplineInterpolation(const Vector2& vecControl1, const Vector2& vecToJoin, const Vector2& vecControl2, FFLOAT var);
	Vector2 Vec2BarycentricInterpolation(const Vector2& vecAtBar, const Vector2& vecToJoin, FFLOAT baryVar1, FFLOAT baryVar2);
	Vector2 Vec2VecMatrixProduct(const Matrix3x3& mat);

	// Accessors
	inline FFLOAT getX() const { return m_fX; }
	inline FFLOAT getY() const { return m_fY; }
	inline VVOID  x(const FFLOAT X) { m_fX = X; }
	inline VVOID  y(const FFLOAT Y) { m_fY = Y; }

	// Operators overload
	/*********************** unary operators **********************/
	Vector2  operator + () const;
	Vector2  operator - () const;
	/*********************** assgm operators **********************/
	Vector2& operator += (const Vector2&);
	Vector2& operator -= (const Vector2&);
	Vector2& operator *= (const FFLOAT& val);
	Vector2& operator /= (const FFLOAT& val);
	/*********************** binary operators *********************/
	Vector2  operator +  (const Vector2&) const;
	Vector2  operator -  (const Vector2&) const;
	Vector2  operator *  (const FFLOAT& val) const;
	Vector2  operator /  (const FFLOAT& val) const;
	BBOOL     operator == (const Vector2&) const;
	BBOOL     operator != (const Vector2&) const;
	friend Vector2 operator * (FFLOAT, const Vector2&);
};

#endif