#ifndef DEF_UTILITY_H
#define DEF_UTILITY_H

#include <cmath>
#include <cassert>
#include <limits>
#include <stdlib.h>

#define DeletePointer(X) {if(X){delete X; X = NULL;}}
#define CUBE(X) (X) * (X) * (X)
#define SQUARE(X) (X) * (X)

typedef long long      BIGLINT;
typedef long           LINT;
typedef int            BIGINT;
typedef short          SMALLINT;
typedef float          FFLOAT;
typedef bool           BBOOL;
typedef double         DFLOAT;
typedef char           CCHAR;
typedef wchar_t        WCHAR;
typedef unsigned long  ULINT;
typedef unsigned int   UBIGINT;
typedef unsigned short USMALLINT;
typedef unsigned char  UCCHAR;
typedef void           VVOID;
typedef void*          UNKNOWN;

const FFLOAT PI        = 3.141592653589793238462f;
const FFLOAT PISQUARE  = SQUARE(PI);
const FFLOAT PICUBE    = CUBE(PI);
const FFLOAT TWOPI     = PI * 2.0f;
const FFLOAT HALFPI    = PI / 2.0f;
const FFLOAT QUARTERPI = PI / 4.0f;

const FFLOAT COS0	  = 1.0f;
const FFLOAT COSPI     = -1.0f;
const FFLOAT COSTWOPI  = 1.0f;
const FFLOAT COS30     = sqrtf(3.0f) / 2.0f;
const FFLOAT COS45	  = sqrtf(2.0f) / 2.0f;
const FFLOAT COS60	  = 0.5f;
const FFLOAT COS90	  = 0.0f;

const FFLOAT SIN0	  = 0.0f;
const FFLOAT SINPI     = 0.0f;
const FFLOAT SINTWOPI  = 0.0f;
const FFLOAT SIN30	  = 0.5f;
const FFLOAT SIN45	  = COS45;
const FFLOAT SIN60	  = COS30;
const FFLOAT SIN90	  = 1.0f;

const BIGINT     MaxInt    = (std::numeric_limits<BIGINT>::max)();
const DFLOAT  MaxDouble = (std::numeric_limits<DFLOAT>::max)();
const DFLOAT  MinDouble = (std::numeric_limits<DFLOAT>::min)();
const FFLOAT   MaxFloat  = (std::numeric_limits<FFLOAT>::max)();
const FFLOAT   MinFloat  = (std::numeric_limits<FFLOAT>::min)();
const FFLOAT   INFINITAS  = (std::numeric_limits<FFLOAT>::infinity)(); // infinity in latin

template<class T>
inline BBOOL Equal(T a, T b)
{
	if(a == b) 
		return true;
	return false;
}

template<class F>
inline BBOOL Different(F a, F b)
{
	if(a != b) 
		return true;
	return false;
}

inline BIGINT AutoRoundF(FFLOAT val)
{
	BIGINT integral   = (BIGINT)val;
	FFLOAT mantissa = val - integral;

	if(mantissa < 0.5f) return integral;

	return integral + 1;
}

inline BIGINT AutoRoundD(DFLOAT val)
{
	BIGINT integral   = (BIGINT)val;
	DFLOAT mantissa = val - integral;

	if(mantissa < 0.5f) return integral;

	return integral + 1;
}

template<class T, class K, class Q>
inline VVOID Clamp(const T& min, K& toFix, const Q& max)
{
	if((FFLOAT)min > (FFLOAT)max) return;

	if(toFix > (K)max)
		toFix = (K)max;

	if(toFix < (K)min)
		toFix = (K)min;
}

template<class T>
inline T Higher(const T& val1, const T& val2)
{
	if(val1 > val2)
		return val1;
	return val2;
}

template<class T>
inline T Higher3Val(const T& val1, const T& val2, const T& val3)
{
	return Higher<T>(Higher<T>(val1, val2), val3);
}

template<class Z>
inline Z Smaller(const Z& val1, const Z& val2)
{
	if(val1 > val2)
		return val2;
	return val1;
}

inline FFLOAT RandFloat()
{
	return (FFLOAT)(((FFLOAT)rand())/(FFLOAT)RAND_MAX);
}

// give a random FFLOAT number between val1 & val2.
inline FFLOAT RandBetween(FFLOAT val1, FFLOAT val2)
{
	return val1 + RandFloat() * ( val2 - val1 );
}

// give a random FFLOAT number between -1.0f & 1.0f.
inline FFLOAT RandUnity()
{
	return ((FFLOAT)((rand()%20000) - 10000) / 10000.0f);
}

// return a value between -1 and 1;
inline FFLOAT ClampedRand()
{
	return RandFloat() - RandFloat();
}

inline BBOOL RandBool()
{
	if(RandFloat() > 0.5f) 
		return true;
	return false;
}

inline FFLOAT DegToRad(FFLOAT deg)
{
	return TWOPI * (deg / 360.0f);
}

inline FFLOAT RadToDeg(FFLOAT rad)
{
	return (rad * 360.0f) / TWOPI;
}

inline FFLOAT Cotangent(FFLOAT radian)
{
	return 1.0f / tan(radian);
}

inline FFLOAT Cosecant(FFLOAT radian)
{
	return 1.0f / sin(radian);
}

inline FFLOAT Secant(FFLOAT radian)
{
	return 1.0f / cos(radian);
}

inline FFLOAT AngleFromXY(FFLOAT x, FFLOAT y)
{
	FFLOAT theta = 0.0f;
 
	// Quadrant I or IV
	if(x >= 0.0f) 
	{
		// If x = 0, then atanf(y/x) = +pi/2 if y > 0
		//                atanf(y/x) = -pi/2 if y < 0
		theta = atanf(y / x); // in [-pi/2, +pi/2]

		if(theta < 0.0f)
		{
			theta += 2.0f * PI; // in [0, 2*pi).
		}
	}
	// Quadrant II or III
	else
	{
		theta = atanf(y / x) + PI; // in [0, 2*pi).
	}

	return theta;
}

inline FFLOAT absf(FFLOAT f)
{
	if(f < 0.0f)
	{
		return -f;
	}
	else
	{
		return f;
	}
}

inline FFLOAT ReciprocalSqrt(FFLOAT pF) 
{
	LINT lI;     
	FFLOAT lY, lR;
	lY = pF * 0.5f;
	lI = *(LINT *)(&pF);
	lI = 0x5f3759df - (lI >> 1);
	lR = *(FFLOAT *)(&lI);
	lR = lR * (1.5f - SQUARE(lR) * lY);
	
	return lR;
}

#endif
