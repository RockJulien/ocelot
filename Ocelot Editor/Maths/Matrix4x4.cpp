#include "Matrix4x4.h"

Matrix4x4::Matrix4x4() :
m_f11(1.0f), m_f12(0.0f), m_f13(0.0f), m_f14(0.0f),
m_f21(0.0f), m_f22(1.0f), m_f23(0.0f), m_f24(0.0f),
m_f31(0.0f), m_f32(0.0f), m_f33(1.0f), m_f34(0.0f),
m_f41(0.0f), m_f42(0.0f), m_f43(0.0f), m_f44(1.0f)
{

}

Matrix4x4::Matrix4x4(const Matrix4x4& mat) :
m_f11(mat.m_f11), m_f12(mat.m_f12), m_f13(mat.m_f13), m_f14(mat.m_f14),
m_f21(mat.m_f21), m_f22(mat.m_f22), m_f23(mat.m_f23), m_f24(mat.m_f24),
m_f31(mat.m_f31), m_f32(mat.m_f32), m_f33(mat.m_f33), m_f34(mat.m_f34),
m_f41(mat.m_f41), m_f42(mat.m_f42), m_f43(mat.m_f43), m_f44(mat.m_f44)
{

}

Matrix4x4& Matrix4x4::operator = (const Matrix4x4& mat)
{
	m_f11 = mat.m_f11;
	m_f12 = mat.m_f12;
	m_f13 = mat.m_f13;
	m_f14 = mat.m_f14;
	m_f21 = mat.m_f21;
	m_f22 = mat.m_f22;
	m_f23 = mat.m_f23;
	m_f24 = mat.m_f24;
	m_f31 = mat.m_f31;
	m_f32 = mat.m_f32;
	m_f33 = mat.m_f33;
	m_f34 = mat.m_f34;
	m_f41 = mat.m_f41;
	m_f42 = mat.m_f42;
	m_f43 = mat.m_f43;
	m_f44 = mat.m_f44;

	return *this;
}

Matrix4x4::Matrix4x4(FFLOAT val) :
m_f11(val), m_f12(val), m_f13(val), m_f14(val),
m_f21(val), m_f22(val), m_f23(val), m_f24(val),
m_f31(val), m_f32(val), m_f33(val), m_f34(val),
m_f41(val), m_f42(val), m_f43(val), m_f44(val)
{

}

Matrix4x4::Matrix4x4(DFLOAT val) :
m_f11((FFLOAT)val), m_f12((FFLOAT)val), m_f13((FFLOAT)val), m_f14((FFLOAT)val),
m_f21((FFLOAT)val), m_f22((FFLOAT)val), m_f23((FFLOAT)val), m_f24((FFLOAT)val),
m_f31((FFLOAT)val), m_f32((FFLOAT)val), m_f33((FFLOAT)val), m_f34((FFLOAT)val),
m_f41((FFLOAT)val), m_f42((FFLOAT)val), m_f43((FFLOAT)val), m_f44((FFLOAT)val)
{

}

Matrix4x4::Matrix4x4(FFLOAT _11, FFLOAT _12, FFLOAT _13, FFLOAT _14,
					 FFLOAT _21, FFLOAT _22, FFLOAT _23, FFLOAT _24,
					 FFLOAT _31, FFLOAT _32, FFLOAT _33, FFLOAT _34,
					 FFLOAT _41, FFLOAT _42, FFLOAT _43, FFLOAT _44) :
m_f11(_11), m_f12(_12), m_f13(_13), m_f14(_14),
m_f21(_21), m_f22(_22), m_f23(_23), m_f24(_24),
m_f31(_31), m_f32(_32), m_f33(_33), m_f34(_34),
m_f41(_41), m_f42(_42), m_f43(_43), m_f44(_44)
{

}

Matrix4x4::Matrix4x4(DFLOAT _11, DFLOAT _12, DFLOAT _13, DFLOAT _14,
					 DFLOAT _21, DFLOAT _22, DFLOAT _23, DFLOAT _24,
					 DFLOAT _31, DFLOAT _32, DFLOAT _33, DFLOAT _34,
					 DFLOAT _41, DFLOAT _42, DFLOAT _43, DFLOAT _44) :
m_f11((FFLOAT)_11), m_f12((FFLOAT)_12), m_f13((FFLOAT)_13), m_f14((FFLOAT)_14),
m_f21((FFLOAT)_21), m_f22((FFLOAT)_22), m_f23((FFLOAT)_23), m_f24((FFLOAT)_24),
m_f31((FFLOAT)_31), m_f32((FFLOAT)_32), m_f33((FFLOAT)_33), m_f34((FFLOAT)_34),
m_f41((FFLOAT)_41), m_f42((FFLOAT)_42), m_f43((FFLOAT)_43), m_f44((FFLOAT)_44)
{

}

Matrix4x4::~Matrix4x4()
{

}

VVOID	  Matrix4x4::Matrix4x4Identity()
{
	// initialize as an identity matrix with 1 along the main diagonal.
	m_f11 = 1.0f; m_f12 = 0.0f; m_f13 = 0.0f; m_f14 = 0.0f;
	m_f21 = 0.0f; m_f22 = 1.0f; m_f23 = 0.0f; m_f24 = 0.0f;
	m_f31 = 0.0f; m_f32 = 0.0f; m_f33 = 1.0f; m_f34 = 0.0f;
	m_f41 = 0.0f; m_f42 = 0.0f; m_f43 = 0.0f; m_f44 = 1.0f;
}

BBOOL      Matrix4x4::Matrix4x4IsIdentity() const
{
	// check if the current matrix is an identity one
	Matrix4x4 temp(*this);
	Matrix4x4 temp2;
	temp2.Matrix4x4Identity();

	if(Equal<Matrix4x4>(temp, temp2))
		return true;
	return false;
}

VVOID	  Matrix4x4::Matrix4x4Transpose(Matrix4x4& out) const
{
	Matrix4x4 temp(*this);

	// transpose the matrix by changing rows with columns
	out.m_f11 = temp.m_f11; out.m_f12 = temp.m_f21; out.m_f13 = temp.m_f31; out.m_f14 = temp.m_f41;
	out.m_f21 = temp.m_f12; out.m_f22 = temp.m_f22; out.m_f23 = temp.m_f32; out.m_f24 = temp.m_f42;
	out.m_f31 = temp.m_f13; out.m_f32 = temp.m_f23; out.m_f33 = temp.m_f33; out.m_f34 = temp.m_f43;
	out.m_f41 = temp.m_f14; out.m_f42 = temp.m_f24; out.m_f43 = temp.m_f34; out.m_f44 = temp.m_f44;
}

FFLOAT     Matrix4x4::Matrix4x4Determinant() const
{
	// compute the determinant of the matrix
	// it could be useful for inversing the matrix
	FFLOAT result = 0.0f;

	return result = (((m_f11 * m_f22) - (m_f12 * m_f21)) * ((m_f33 * m_f44) - (m_f34 * m_f43))) +
					(((m_f13 * m_f21) - (m_f11 * m_f23)) * ((m_f32 * m_f44) - (m_f34 * m_f42))) +
					(((m_f11 * m_f24) - (m_f14 * m_f21)) * ((m_f32 * m_f43) - (m_f33 * m_f42))) +
					(((m_f12 * m_f23) - (m_f13 * m_f22)) * ((m_f31 * m_f44) - (m_f34 * m_f41))) +
					(((m_f14 * m_f22) - (m_f12 * m_f24)) * ((m_f31 * m_f43) - (m_f33 * m_f41))) +
					(((m_f13 * m_f24) - (m_f14 * m_f23)) * ((m_f31 * m_f42) - (m_f32 * m_f41)));
}

VVOID	  Matrix4x4::Matrix4x4Inverse(Matrix4x4& out) const
{
	// first compute the determinant
	FFLOAT determinant = this->Matrix4x4Determinant();

	// if the determinant is not equal to zero,
	// the matrix inverse is possible.
	if(Different<FFLOAT>(determinant, 0.0f))
	{
		// compute Minors for each component of the matrix
		// for each element, eliminate other components on
		// the same row and column and create a new one 
		// with the rest for computing cofactors in a same
		// way that for the determinant.
		/*******************First row*************************/
		Matrix3x3 minorMat_11(m_f22, m_f23, m_f24, 
							  m_f32, m_f33, m_f34,
							  m_f42, m_f43, m_f44);
		FFLOAT cofactor_11 = minorMat_11.Matrix3x3Determinant();
		Matrix3x3 minorMat_12(m_f21, m_f23, m_f24,
							  m_f31, m_f33, m_f34,
							  m_f41, m_f43, m_f44);
		FFLOAT cofactor_12 = minorMat_12.Matrix3x3Determinant();
		Matrix3x3 minorMat_13(m_f21, m_f22, m_f24,
							  m_f31, m_f32, m_f34,
							  m_f41, m_f42, m_f44);
		FFLOAT cofactor_13 = minorMat_13.Matrix3x3Determinant();
		Matrix3x3 minorMat_14(m_f21, m_f22, m_f23,
							  m_f31, m_f32, m_f33,
							  m_f41, m_f42, m_f43);
		FFLOAT cofactor_14 = minorMat_14.Matrix3x3Determinant();
		/********************Second Row***********************/
		Matrix3x3 minorMat_21(m_f12, m_f13, m_f14,
							  m_f32, m_f33, m_f34,
							  m_f42, m_f43, m_f44);
		FFLOAT cofactor_21 = minorMat_21.Matrix3x3Determinant();
		Matrix3x3 minorMat_22(m_f11, m_f13, m_f14,
							  m_f31, m_f33, m_f34,
							  m_f41, m_f43, m_f44);
		FFLOAT cofactor_22 = minorMat_22.Matrix3x3Determinant();
		Matrix3x3 minorMat_23(m_f11, m_f12, m_f14,
							  m_f31, m_f32, m_f34,
							  m_f41, m_f42, m_f44);
		FFLOAT cofactor_23 = minorMat_23.Matrix3x3Determinant();
		Matrix3x3 minorMat_24(m_f11, m_f12, m_f13,
							  m_f31, m_f32, m_f33,
							  m_f41, m_f42, m_f43);
		FFLOAT cofactor_24 = minorMat_24.Matrix3x3Determinant();
		/*********************Third Row***********************/
		Matrix3x3 minorMat_31(m_f12, m_f13, m_f14,
							  m_f22, m_f23, m_f24,
							  m_f42, m_f43, m_f44);
		FFLOAT cofactor_31 = minorMat_31.Matrix3x3Determinant();
		Matrix3x3 minorMat_32(m_f11, m_f13, m_f14,
							  m_f21, m_f23, m_f24,
							  m_f41, m_f43, m_f44);
		FFLOAT cofactor_32 = minorMat_32.Matrix3x3Determinant();
		Matrix3x3 minorMat_33(m_f11, m_f12, m_f14,
							  m_f21, m_f22, m_f24,
							  m_f41, m_f42, m_f44);
		FFLOAT cofactor_33 = minorMat_33.Matrix3x3Determinant();
		Matrix3x3 minorMat_34(m_f11, m_f12, m_f13,
							  m_f21, m_f22, m_f23,
							  m_f41, m_f42, m_f43);
		FFLOAT cofactor_34 = minorMat_34.Matrix3x3Determinant();
		/*********************Fourth Row**********************/
		Matrix3x3 minorMat_41(m_f12, m_f13, m_f14,
							  m_f22, m_f23, m_f24,
							  m_f32, m_f33, m_f34);
		FFLOAT cofactor_41 = minorMat_41.Matrix3x3Determinant();
		Matrix3x3 minorMat_42(m_f11, m_f13, m_f14,
							  m_f21, m_f23, m_f24,
							  m_f31, m_f33, m_f34);
		FFLOAT cofactor_42 = minorMat_42.Matrix3x3Determinant();
		Matrix3x3 minorMat_43(m_f11, m_f12, m_f14,
							  m_f21, m_f22, m_f24,
							  m_f31, m_f32, m_f34);
		FFLOAT cofactor_43 = minorMat_43.Matrix3x3Determinant();
		Matrix3x3 minorMat_44(m_f11, m_f12, m_f13,
							  m_f21, m_f22, m_f23,
							  m_f31, m_f32, m_f33);
		FFLOAT cofactor_44 = minorMat_44.Matrix3x3Determinant();

		// Now we can rebuild the new matrix with cofactors without forgetting
		// to inverse values and transpose it.
		// for calculating the sign to put before each cofactor, just use the
		// following calcul : (-1)^(line + col) or (C++ style) pow(-1.0f, (line + col)).
		out.m_f11 = cofactor_11; out.m_f12 = -cofactor_12; out.m_f13 = cofactor_13; out.m_f14 = -cofactor_14;
		out.m_f21 = -cofactor_21; out.m_f22 = cofactor_22; out.m_f23 = -cofactor_23; out.m_f24 = cofactor_24;
		out.m_f31 = cofactor_31; out.m_f32 = -cofactor_32; out.m_f33 = cofactor_33; out.m_f34 = -cofactor_34;
		out.m_f41 = -cofactor_41; out.m_f42 = cofactor_42; out.m_f43 = -cofactor_43; out.m_f44 = cofactor_44;

		out.Matrix4x4Transpose(out); // invert rows and columns.

		// to end up by dividing by the determinant.
		out /= determinant;
	}
}

VVOID	  Matrix4x4::Matrix4x4Mutliply(Matrix4x4& out, const Matrix4x4& mat)
{
	// Multiplication between two matrices.
	Matrix4x4 temp(*this);

	// each row is going to be multiplied by each 
	// element of the column where the value processed is.
	/*****************first row****************************/
	out.m_f11 = (temp.m_f11 * mat.m_f11) + (temp.m_f12 * mat.m_f21) + (temp.m_f13 * mat.m_f31) + (temp.m_f14 * mat.m_f41);
	out.m_f12 = (temp.m_f11 * mat.m_f12) + (temp.m_f12 * mat.m_f22) + (temp.m_f13 * mat.m_f32) + (temp.m_f14 * mat.m_f42);
	out.m_f13 = (temp.m_f11 * mat.m_f13) + (temp.m_f12 * mat.m_f23) + (temp.m_f13 * mat.m_f33) + (temp.m_f14 * mat.m_f43);
	out.m_f14 = (temp.m_f11 * mat.m_f14) + (temp.m_f12 * mat.m_f24) + (temp.m_f13 * mat.m_f34) + (temp.m_f14 * mat.m_f44);
	/*****************Second row***************************/
	out.m_f21 = (temp.m_f21 * mat.m_f11) + (temp.m_f22 * mat.m_f21) + (temp.m_f23 * mat.m_f31) + (temp.m_f24 * mat.m_f41);
	out.m_f22 = (temp.m_f21 * mat.m_f12) + (temp.m_f22 * mat.m_f22) + (temp.m_f23 * mat.m_f32) + (temp.m_f24 * mat.m_f42);
	out.m_f23 = (temp.m_f21 * mat.m_f13) + (temp.m_f22 * mat.m_f23) + (temp.m_f23 * mat.m_f33) + (temp.m_f24 * mat.m_f43);
	out.m_f24 = (temp.m_f21 * mat.m_f14) + (temp.m_f22 * mat.m_f24) + (temp.m_f23 * mat.m_f34) + (temp.m_f24 * mat.m_f44);
	/*****************Third row****************************/
	out.m_f31 = (temp.m_f31 * mat.m_f11) + (temp.m_f32 * mat.m_f21) + (temp.m_f33 * mat.m_f31) + (temp.m_f34 * mat.m_f41);
	out.m_f32 = (temp.m_f31 * mat.m_f12) + (temp.m_f32 * mat.m_f22) + (temp.m_f33 * mat.m_f32) + (temp.m_f34 * mat.m_f42);
	out.m_f33 = (temp.m_f31 * mat.m_f13) + (temp.m_f32 * mat.m_f23) + (temp.m_f33 * mat.m_f33) + (temp.m_f34 * mat.m_f43);
	out.m_f34 = (temp.m_f31 * mat.m_f14) + (temp.m_f32 * mat.m_f24) + (temp.m_f33 * mat.m_f34) + (temp.m_f34 * mat.m_f44);
	/*****************Fourth row***************************/
	out.m_f41 = (temp.m_f41 * mat.m_f11) + (temp.m_f42 * mat.m_f21) + (temp.m_f43 * mat.m_f31) + (temp.m_f44 * mat.m_f41);
	out.m_f42 = (temp.m_f41 * mat.m_f12) + (temp.m_f42 * mat.m_f22) + (temp.m_f43 * mat.m_f32) + (temp.m_f44 * mat.m_f42);
	out.m_f43 = (temp.m_f41 * mat.m_f13) + (temp.m_f42 * mat.m_f23) + (temp.m_f43 * mat.m_f33) + (temp.m_f44 * mat.m_f43);
	out.m_f44 = (temp.m_f41 * mat.m_f14) + (temp.m_f42 * mat.m_f24) + (temp.m_f43 * mat.m_f34) + (temp.m_f44 * mat.m_f44);
}

VVOID	  Matrix4x4::Matrix4x4Mutliply(Matrix4x4& out, const Matrix4x4& mat1, const Matrix4x4& mat2)
{
	// multiply with the first (view for example)
	Matrix4x4Mutliply(out, mat1);
	// then, multiply with the second (projection for example)
	out.Matrix4x4Mutliply(out, mat2);
}

VVOID	  Matrix4x4::Matrix4x4MultiplyTranspose(Matrix4x4& out, const Matrix4x4& mat)
{
	// invert result of a multiplication by changing
	// rows with columns.
	Matrix4x4Mutliply(out, mat);

	// just add the transpose fonction into this one.
	// will invert rows and columns
	out.Matrix4x4Transpose(out);
}

VVOID	  Matrix4x4::Matrix4x4MultiplyTranspose(Matrix4x4& out, const Matrix4x4& mat1, const Matrix4x4& mat2)
{
	// exactly the same but by using DFLOAT multiplication
	Matrix4x4Mutliply(out, mat1, mat2);

	// then swap rows and columns
	out.Matrix4x4Transpose(out);
}

VVOID	  Matrix4x4::Matrix4x4Translation(Matrix4x4& out, const FFLOAT& Txyz)
{
	// Declare a new matrix which will represent the
	// translation to apply.
	// and we will apply by multiply this new one 
	// with the matrix which called the function.
	Matrix4x4 matTranslation;
	matTranslation.Matrix4x4Identity();
	matTranslation.set_41(Txyz);
	matTranslation.set_42(Txyz);
	matTranslation.set_43(Txyz);

	// then, we apply
	Matrix4x4Mutliply(out, matTranslation);
}

VVOID	  Matrix4x4::Matrix4x4Translation(Matrix4x4& out, const DFLOAT& Txyz)
{
	// simple overload for lazy programmers.
	// Let's save some fingers and also the wish
	// to use, for some people a DFLOAT :-).
	Matrix4x4 matTranslation;
	matTranslation.Matrix4x4Identity();
	matTranslation.set_41((FFLOAT)Txyz);  // in all case, it will be a FFLOAT hahahaha.
	matTranslation.set_42((FFLOAT)Txyz);  // I am the devil :-).
	matTranslation.set_43((FFLOAT)Txyz);

	// then, we apply
	Matrix4x4Mutliply(out, matTranslation);
}

VVOID	  Matrix4x4::Matrix4x4Translation(Matrix4x4& out, const FFLOAT& Tx, const FFLOAT& Ty, const FFLOAT& Tz)
{
	// For less lazy guy who want to control every
	// variables in details. Better !!!
	Matrix4x4 matTranslation;
	matTranslation.Matrix4x4Identity();
	matTranslation.set_41(Tx);  // in all case, it will be a FFLOAT hahahaha.
	matTranslation.set_42(Ty);  // I am the devil :-).
	matTranslation.set_43(Tz);

	// then, we apply
	Matrix4x4Mutliply(out, matTranslation);
}

VVOID	  Matrix4x4::Matrix4x4Translation(Matrix4x4& out, const DFLOAT& Tx, const DFLOAT& Ty, const DFLOAT& Tz)
{
	// And again an overload with DFLOAT, sometimes
	// unfortunatly, some person forget the "f" for a FFLOAT
	// now it is fixed ;-).
	Matrix4x4 matTranslation;
	matTranslation.Matrix4x4Identity();
	matTranslation.set_41((FFLOAT)Tx);  // in all case, it will be a FFLOAT hahahaha.
	matTranslation.set_42((FFLOAT)Ty);  // I am the devil :-).
	matTranslation.set_43((FFLOAT)Tz);

	// then, we apply
	Matrix4x4Mutliply(out, matTranslation);
}

VVOID	  Matrix4x4::Matrix4x4Scaling(Matrix4x4& out, const FFLOAT& Sxyz)
{
	// Same as the translation one, we create
	// a scaling matrix for applying a size change
	// at the matrix which called the function.
	Matrix4x4 matScaling;
	matScaling.Matrix4x4Identity();
	matScaling.set_11(Sxyz);
	matScaling.set_22(Sxyz);
	matScaling.set_33(Sxyz);

	// now we apply as previously
	Matrix4x4Mutliply(out, matScaling);
}

VVOID	  Matrix4x4::Matrix4x4Scaling(Matrix4x4& out, const DFLOAT& Sxyz)
{
	// I am not going to make presentation
	// anymore ;-)
	Matrix4x4 matScaling;
	matScaling.Matrix4x4Identity();
	matScaling.set_11((FFLOAT)Sxyz);
	matScaling.set_22((FFLOAT)Sxyz);
	matScaling.set_33((FFLOAT)Sxyz);

	// now we apply as previously
	Matrix4x4Mutliply(out, matScaling);
}

VVOID	  Matrix4x4::Matrix4x4Scaling(Matrix4x4& out, const FFLOAT& Sx, const FFLOAT& Sy, const FFLOAT& Sz)
{
	Matrix4x4 matScaling;
	matScaling.Matrix4x4Identity();
	matScaling.set_11(Sx);
	matScaling.set_22(Sy);
	matScaling.set_33(Sz);

	// now we apply as previously
	Matrix4x4Mutliply(out, matScaling);
}

VVOID	  Matrix4x4::Matrix4x4Scaling(Matrix4x4& out, const DFLOAT& Sx, const DFLOAT& Sy, const DFLOAT& Sz)
{
	Matrix4x4 matScaling;
	matScaling.Matrix4x4Identity();
	matScaling.set_11((FFLOAT)Sx);
	matScaling.set_22((FFLOAT)Sy);
	matScaling.set_33((FFLOAT)Sz);

	// now we apply as previously
	Matrix4x4Mutliply(out, matScaling);
}

VVOID	  Matrix4x4::Matrix4x4RotationX(Matrix4x4& out, const FFLOAT& Rx)
{
	// Declare a new matrix which will represent the
	// rotation to apply on the X axis.
	// and we will apply by multiply this new one 
	// with the matrix which called the function.
	Matrix4x4 matRotationX;
	matRotationX.Matrix4x4Identity();

	// compute before for avoiding to compute twice.
	FFLOAT Cos = cosf(Rx);
	FFLOAT Sin = sinf(Rx);
	matRotationX.set_22(Cos);
	matRotationX.set_23(Sin);
	matRotationX.set_32(-Sin);
	matRotationX.set_33(Cos);


	// then, we apply
	Matrix4x4Mutliply(out, matRotationX);
}

VVOID	  Matrix4x4::Matrix4x4RotationX(Matrix4x4& out, const DFLOAT& Rx)
{
	Matrix4x4 matRotationX;
	matRotationX.Matrix4x4Identity();

	// compute before for avoiding to compute twice.
	FFLOAT Cos = cos((FFLOAT)Rx);
	FFLOAT Sin = sin((FFLOAT)Rx);
	matRotationX.set_22(Cos);
	matRotationX.set_23(Sin);
	matRotationX.set_32(-Sin);
	matRotationX.set_33(Cos);

	// then, we apply
	Matrix4x4Mutliply(out, matRotationX);
}

VVOID	  Matrix4x4::Matrix4x4RotationY(Matrix4x4& out, const FFLOAT& Ry)
{
	Matrix4x4 matRotationY;
	matRotationY.Matrix4x4Identity();

	// compute before for avoiding to compute twice.
	FFLOAT Cos = cosf(Ry);
	FFLOAT Sin = sinf(Ry);
	matRotationY.set_11(Cos);
	matRotationY.set_13(-Sin);
	matRotationY.set_31(Sin);
	matRotationY.set_33(Cos);

	// then, we apply
	Matrix4x4Mutliply(out, matRotationY);
}

VVOID	  Matrix4x4::Matrix4x4RotationY(Matrix4x4& out, const DFLOAT& Ry)
{
	Matrix4x4 matRotationY;
	matRotationY.Matrix4x4Identity();

	// compute before for avoiding to compute twice.
	FFLOAT Cos = cos((FFLOAT)Ry);
	FFLOAT Sin = sin((FFLOAT)Ry);
	matRotationY.set_11(Cos);
	matRotationY.set_13(-Sin);
	matRotationY.set_31(Sin);
	matRotationY.set_33(Cos);

	// then, we apply
	Matrix4x4Mutliply(out, matRotationY);
}

VVOID	  Matrix4x4::Matrix4x4RotationZ(Matrix4x4& out, const FFLOAT& Rz)
{
	Matrix4x4 matRotationZ;
	matRotationZ.Matrix4x4Identity();

	// compute before for avoiding to compute twice.
	FFLOAT Cos = cosf(Rz);
	FFLOAT Sin = sinf(Rz);
	matRotationZ.set_11(Cos);
	matRotationZ.set_12(Sin);
	matRotationZ.set_21(-Sin);
	matRotationZ.set_22(Cos);

	// then, we apply
	Matrix4x4Mutliply(out, matRotationZ);
}

VVOID	  Matrix4x4::Matrix4x4RotationZ(Matrix4x4& out, const DFLOAT& Rz)
{
	Matrix4x4 matRotationZ;
	matRotationZ.Matrix4x4Identity();

	// compute before for avoiding to compute twice.
	FFLOAT Cos = cos((FFLOAT)Rz);
	FFLOAT Sin = sin((FFLOAT)Rz);
	matRotationZ.set_11(Cos);
	matRotationZ.set_12(Sin);
	matRotationZ.set_21(-Sin);
	matRotationZ.set_22(Cos);

	// then, we apply
	Matrix4x4Mutliply(out, matRotationZ);
}

VVOID      Matrix4x4::Matrix4x4RotationAxis(Matrix4x4& out, Vector3 axis, FFLOAT angle)
{
	// rotation from an axis and an angle theta
	// equal R = I cosTheta + sinTheta [u]x + (1-cosTheta) U cross U
	// where I is an identity matrix, [u]x & U cross U are a matrix.

	FFLOAT axisX    = axis.getX();
	FFLOAT axisY    = axis.getY();
	FFLOAT axisZ    = axis.getZ();
	FFLOAT COSTheta = cosf(angle);  // compute once in my case
	FFLOAT SINTheta = sinf(angle);

	Matrix4x4 uCrossU(SQUARE(axisX), axisX * axisY, axisX * axisZ, 0.0f,
		              axisX * axisY, SQUARE(axisY), axisY * axisZ, 0.0f,
					  axisX * axisZ, axisY * axisZ, SQUARE(axisZ), 0.0f,
					  0.0f         , 0.0f         , 0.0f         , 1.0f);

	Matrix4x4 uBrakets( 0.0f , -axisZ,  axisY, 0.0f,
		                axisZ,  0.0f , -axisX, 0.0f,
					   -axisY,  axisX,  0.0f , 0.0f,
					    0.0f ,  0.0f ,  0.0f , 1.0f);

	out.Matrix4x4Identity();

	out = (out * COSTheta) + (uBrakets * SINTheta) + ((1.0f - COSTheta) * uCrossU);
}

VVOID	  Matrix4x4::Matrix4x4RotationGeneralized(Matrix4x4& out, const FFLOAT& Rxyz)
{
	// we could either create each different rotation matrix
	// on each axis and multiply them together, or calculate 
	// all in one generalized matrix but means more maths :-).
	// That is why I chose generalized one with maths :-)
	// R(alpha, beta, phi) = Rz(alpha)*Ry(beta)*Rx(phi) = below.
	Matrix4x4 matYawPitchRoll;
	matYawPitchRoll.Matrix4x4Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	FFLOAT CosAlpha = cos(Rxyz);
	FFLOAT SinAlpha = sin(Rxyz);
	FFLOAT CosBeta  = cos(Rxyz);
	FFLOAT SinBeta  = sin(Rxyz);
	FFLOAT CosPhi   = cos(Rxyz);
	FFLOAT SinPhi   = sin(Rxyz);

	// actually on internet, there is different descriptions of
	// the Yaw Pitch Roll regarding to which axis corresponds to
	// which funny word (yaw pitch roll).
	// In my case, Yaw Pitch Roll is a Z Y X transformation.
	// (come from avionic book...)
	// if problem, try Y Z X by simply concatenating three matRot
	// in this same order with the corresponding angle. 
	// For example, DirectX is X Y Z.
	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	// then, we apply
	Matrix4x4Mutliply(out, matYawPitchRoll);
}

VVOID	  Matrix4x4::Matrix4x4RotationGeneralized(Matrix4x4& out, const DFLOAT& Rxyz)
{
	// the order is important. The way you multiply will 
	// determine the way it rotates.
	// DirectX made a mistake by starting with the "yaw"
	// of the "yawPitchRoll" for the X axis. The yaw is 
	// applied to the Z axis (e.g: avionic) and the "roll"
	// must be perform first.
	Matrix4x4 matYawPitchRoll;
	matYawPitchRoll.Matrix4x4Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	FFLOAT CosAlpha = cos((FFLOAT)Rxyz);
	FFLOAT SinAlpha = sin((FFLOAT)Rxyz);
	FFLOAT CosBeta  = cos((FFLOAT)Rxyz);
	FFLOAT SinBeta  = sin((FFLOAT)Rxyz);
	FFLOAT CosPhi   = cos((FFLOAT)Rxyz);
	FFLOAT SinPhi   = sin((FFLOAT)Rxyz);

	// actually on internet, there is different descriptions of
	// the Yaw Pitch Roll regarding to which axis corresponds to
	// which funny word (yaw pitch roll).
	// In my case, Yaw Pitch Roll is a Z Y X transformation.
	// (come from avionic book...)
	// if problem, try Y Z X by simply concatenating three matRot
	// in this same order with the corresponding angle. 
	// For example, DirectX is X Y Z.
	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	// then, we apply
	Matrix4x4Mutliply(out, matYawPitchRoll);
}

VVOID	  Matrix4x4::Matrix4x4RotationGeneralized(Matrix4x4& out, const FFLOAT& Rx, const FFLOAT& Ry, const FFLOAT& Rz)
{
	// same overload of method as previously
	// but in this case, be aware of where you 
	// put "x" (Roll), "y" (Pitch) and "z" (Yaw) value.
	Matrix4x4 matYawPitchRoll;
	matYawPitchRoll.Matrix4x4Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	FFLOAT CosAlpha = cos(Rz);  // Yaw (Z)
	FFLOAT SinAlpha = sin(Rz);  // Yaw (Z)
	FFLOAT CosBeta  = cos(Ry);  // Pitch (Y)
	FFLOAT SinBeta  = sin(Ry);  // Pitch (Y)
	FFLOAT CosPhi   = cos(Rx);  // Roll (X)
	FFLOAT SinPhi   = sin(Rx);  // Roll (X)

	// actually on internet, there is different descriptions of
	// the Yaw Pitch Roll regarding to which axis corresponds to
	// which funny word (yaw pitch roll).
	// In my case, Yaw Pitch Roll is a Z Y X transformation.
	// (come from avionic book...)
	// if problem, try Y Z X by simply concatenating three matRot
	// in this same order with the corresponding angle. 
	// For example, DirectX is X Y Z.
	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	// then, we apply
	Matrix4x4Mutliply(out, matYawPitchRoll);
}

VVOID	  Matrix4x4::Matrix4x4RotationGeneralized(Matrix4x4& out, const DFLOAT& Rx, const DFLOAT& Ry, const DFLOAT& Rz)
{
	Matrix4x4 matYawPitchRoll;
	matYawPitchRoll.Matrix4x4Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	FFLOAT CosAlpha = cos((FFLOAT)Rz);  // Yaw (Z)
	FFLOAT SinAlpha = sin((FFLOAT)Rz);  // Yaw (Z)
	FFLOAT CosBeta  = cos((FFLOAT)Ry);  // Pitch (Y)
	FFLOAT SinBeta  = sin((FFLOAT)Ry);  // Pitch (Y)
	FFLOAT CosPhi   = cos((FFLOAT)Rx);  // Roll (X)
	FFLOAT SinPhi   = sin((FFLOAT)Rx);  // Roll (X)

	// actually on internet, there is different descriptions of
	// the Yaw Pitch Roll regarding to which axis corresponds to
	// which funny word (yaw pitch roll).
	// In my case, Yaw Pitch Roll is a Z Y X transformation.
	// (come from avionic book...)
	// if problem, try Y Z X by simply concatenating three matRot
	// in this same order with the corresponding angle. 
	// For example, DirectX is X Y Z.
	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	// then, we apply
	Matrix4x4Mutliply(out, matYawPitchRoll);
}

VVOID	  Matrix4x4::Matrix4x4RotationQuaternion(Matrix4x4& out, Quaternion q)
{
	// perform a rotation thanks to a quaternion
	// inserted as parameter.
	Matrix4x4 matRotation = q.QuaternionToMatrix4Normalized();

	// apply the rotation
	Matrix4x4Mutliply(out, matRotation);
}

VVOID      Matrix4x4::Matrix4x4AffineTransformation(Matrix4x4& pOut, const Vector3& pScaling, const Vector3& pRotationOrigin, const Quaternion& pRotation, const Vector3& pTranslation)
{
	Matrix4x4 lMScaling;
	Vector4   lVRotationOrigin = Vector4(pRotationOrigin.getX(), pRotationOrigin.getY(), pRotationOrigin.getZ(), 0.0f);
    Matrix4x4 lMRotation;
	Vector4   lVTranslation    = Vector4(pTranslation.getX(), pTranslation.getY(), pTranslation.getZ(), 0.0f); 

	// M = MScaling * Inverse(MRotationOrigin) * MRotation * MRotationOrigin * MTranslation;
	lMScaling.Matrix4x4Scaling(lMScaling, pScaling.getX(), pScaling.getY(), pScaling.getZ());
	lMRotation.Matrix4x4RotationQuaternion(lMRotation, pRotation);
	
	pOut = lMScaling;
	Vector4 lMPosition(pOut.get_41(), pOut.get_42(), pOut.get_43(), pOut.get_44());
	Vector4 lNewPosition = lMPosition.Vec4Subtraction(lVRotationOrigin);
	pOut.set_41(lNewPosition.getX());
	pOut.set_42(lNewPosition.getY());
	pOut.set_43(lNewPosition.getZ());
	pOut.set_44(lNewPosition.getW());
	pOut.Matrix4x4Mutliply(pOut, lMRotation);
	lMPosition = Vector4(pOut.get_41(), pOut.get_42(), pOut.get_43(), pOut.get_44());
	lNewPosition = lMPosition.Vec4Addition(lVRotationOrigin);
	lNewPosition = lNewPosition.Vec4Addition(lVTranslation);
	pOut.set_41(lNewPosition.getX());
	pOut.set_42(lNewPosition.getY());
	pOut.set_43(lNewPosition.getZ());
	pOut.set_44(lNewPosition.getW());
}

FFLOAT& Matrix4x4::operator () (BIGINT line, BIGINT col)
{
	if(line == 1)
	{
		if(col == 1)
		{
			return this->m_f11;
		}
		else if(col == 2)
		{
			return this->m_f12;
		}
		else if(col == 3)
		{
			return this->m_f13;
		}
		else if(col == 4)
		{
			return this->m_f14;
		}
	}
	else if(line == 2)
	{
		if(col == 1)
		{
			return this->m_f21;
		}
		else if(col == 2)
		{
			return this->m_f22;
		}
		else if(col == 3)
		{
			return this->m_f23;
		}
		else if(col == 4)
		{
			return this->m_f24;
		}
	}
	else if(line == 3)
	{
		if(col == 1)
		{
			return this->m_f31;
		}
		else if(col == 2)
		{
			return this->m_f32;
		}
		else if(col == 3)
		{
			return this->m_f33;
		}
		else if(col == 4)
		{
			return this->m_f34;
		}
	}
	else if(line == 4)
	{
		if(col == 1)
		{
			return this->m_f41;
		}
		else if(col == 2)
		{
			return this->m_f42;
		}
		else if(col == 3)
		{
			return this->m_f43;
		}
		else if(col == 4)
		{
			return this->m_f44;
		}
	}

	return this->m_f11;    // return the first one in case of bounds error
}

FFLOAT  Matrix4x4::operator () (BIGINT line, BIGINT col) const
{
	Matrix4x4 temp(*this);
	if(line == 1)
	{
		if(col == 1)
		{
			return temp.m_f11;
		}
		else if(col == 2)
		{
			return temp.m_f12;
		}
		else if(col == 3)
		{
			return temp.m_f13;
		}
		else if(col == 4)
		{
			return temp.m_f14;
		}
	}
	else if(line == 2)
	{
		if(col == 1)
		{
			return temp.m_f21;
		}
		else if(col == 2)
		{
			return temp.m_f22;
		}
		else if(col == 3)
		{
			return temp.m_f23;
		}
		else if(col == 4)
		{
			return temp.m_f24;
		}
	}
	else if(line == 3)
	{
		if(col == 1)
		{
			return temp.m_f31;
		}
		else if(col == 2)
		{
			return temp.m_f32;
		}
		else if(col == 3)
		{
			return temp.m_f33;
		}
		else if(col == 4)
		{
			return temp.m_f34;
		}
	}
	else if(line == 4)
	{
		if(col == 1)
		{
			return temp.m_f41;
		}
		else if(col == 2)
		{
			return temp.m_f42;
		}
		else if(col == 3)
		{
			return temp.m_f43;
		}
		else if(col == 4)
		{
			return temp.m_f44;
		}
	}

	return temp.m_f11;    // return the first one in case of bounds error
}

Matrix4x4::operator FFLOAT* ()
{
	FFLOAT* mat4x4[16];

	mat4x4[0]  = &m_f11;
	mat4x4[1]  = &m_f12;
	mat4x4[2]  = &m_f13;
	mat4x4[3]  = &m_f14;
	mat4x4[4]  = &m_f21;
	mat4x4[5]  = &m_f22;
	mat4x4[6]  = &m_f23;
	mat4x4[7]  = &m_f24;
	mat4x4[8]  = &m_f31;
	mat4x4[9]  = &m_f32;
	mat4x4[10] = &m_f33;
	mat4x4[11] = &m_f34;
	mat4x4[12] = &m_f41;
	mat4x4[13] = &m_f42;
	mat4x4[14] = &m_f43;
	mat4x4[15] = &m_f44;

	return mat4x4[0];
}

Matrix4x4::operator const FFLOAT* () const
{
	const FFLOAT* mat4x4[16];

	mat4x4[0]  = &m_f11;
	mat4x4[1]  = &m_f12;
	mat4x4[2]  = &m_f13;
	mat4x4[3]  = &m_f14;
	mat4x4[4]  = &m_f21;
	mat4x4[5]  = &m_f22;
	mat4x4[6]  = &m_f23;
	mat4x4[7]  = &m_f24;
	mat4x4[8]  = &m_f31;
	mat4x4[9]  = &m_f32;
	mat4x4[10] = &m_f33;
	mat4x4[11] = &m_f34;
	mat4x4[12] = &m_f41;
	mat4x4[13] = &m_f42;
	mat4x4[14] = &m_f43;
	mat4x4[15] = &m_f44;

	return mat4x4[0];
}

Matrix4x4 Matrix4x4::operator + () const
{
	return Matrix4x4(+this->m_f11, +this->m_f12, +this->m_f13, +this->m_f14,
					 +this->m_f21, +this->m_f22, +this->m_f23, +this->m_f24,
					 +this->m_f31, +this->m_f32, +this->m_f33, +this->m_f34,
					 +this->m_f41, +this->m_f42, +this->m_f43, +this->m_f44);
}

Matrix4x4 Matrix4x4::operator - () const
{
	return Matrix4x4(-this->m_f11, -this->m_f12, -this->m_f13, -this->m_f14,
					 -this->m_f21, -this->m_f22, -this->m_f23, -this->m_f24,
					 -this->m_f31, -this->m_f32, -this->m_f33, -this->m_f34,
					 -this->m_f41, -this->m_f42, -this->m_f43, -this->m_f44);
}

Matrix4x4& Matrix4x4::operator += (const Matrix4x4& mat)
{
	m_f11 += mat.m_f11;
	m_f12 += mat.m_f12;
	m_f13 += mat.m_f13;
	m_f14 += mat.m_f14;
	m_f21 += mat.m_f21;
	m_f22 += mat.m_f22;
	m_f23 += mat.m_f23;
	m_f24 += mat.m_f24;
	m_f31 += mat.m_f31;
	m_f32 += mat.m_f32;
	m_f33 += mat.m_f33;
	m_f34 += mat.m_f34;
	m_f41 += mat.m_f41;
	m_f42 += mat.m_f42;
	m_f43 += mat.m_f43;
	m_f44 += mat.m_f44;

	return *this;
}

Matrix4x4& Matrix4x4::operator -= (const Matrix4x4& mat)
{
	m_f11 -= mat.m_f11;
	m_f12 -= mat.m_f12;
	m_f13 -= mat.m_f13;
	m_f14 -= mat.m_f14;
	m_f21 -= mat.m_f21;
	m_f22 -= mat.m_f22;
	m_f23 -= mat.m_f23;
	m_f24 -= mat.m_f24;
	m_f31 -= mat.m_f31;
	m_f32 -= mat.m_f32;
	m_f33 -= mat.m_f33;
	m_f34 -= mat.m_f34;
	m_f41 -= mat.m_f41;
	m_f42 -= mat.m_f42;
	m_f43 -= mat.m_f43;
	m_f44 -= mat.m_f44;

	return *this;
}

Matrix4x4& Matrix4x4::operator *= (const Matrix4x4& mat)
{
	// Multiplication between two matrices.
	Matrix4x4 temp(*this);

	// each row is going to be multiplied by each 
	// element of the column where the value processed is.
	/*****************first row****************************/
	m_f11 = (temp.m_f11 * mat.m_f11) + (temp.m_f12 * mat.m_f21) + (temp.m_f13 * mat.m_f31) + (temp.m_f14 * mat.m_f41);
	m_f12 = (temp.m_f11 * mat.m_f12) + (temp.m_f12 * mat.m_f22) + (temp.m_f13 * mat.m_f32) + (temp.m_f14 * mat.m_f42);
	m_f13 = (temp.m_f11 * mat.m_f13) + (temp.m_f12 * mat.m_f23) + (temp.m_f13 * mat.m_f33) + (temp.m_f14 * mat.m_f43);
	m_f14 = (temp.m_f11 * mat.m_f14) + (temp.m_f12 * mat.m_f24) + (temp.m_f13 * mat.m_f34) + (temp.m_f14 * mat.m_f44);
	/*****************Second row***************************/
	m_f21 = (temp.m_f21 * mat.m_f11) + (temp.m_f22 * mat.m_f21) + (temp.m_f23 * mat.m_f31) + (temp.m_f24 * mat.m_f41);
	m_f22 = (temp.m_f21 * mat.m_f12) + (temp.m_f22 * mat.m_f22) + (temp.m_f23 * mat.m_f32) + (temp.m_f24 * mat.m_f42);
	m_f23 = (temp.m_f21 * mat.m_f13) + (temp.m_f22 * mat.m_f23) + (temp.m_f23 * mat.m_f33) + (temp.m_f24 * mat.m_f43);
	m_f24 = (temp.m_f21 * mat.m_f14) + (temp.m_f22 * mat.m_f24) + (temp.m_f23 * mat.m_f34) + (temp.m_f24 * mat.m_f44);
	/*****************Third row****************************/
	m_f31 = (temp.m_f31 * mat.m_f11) + (temp.m_f32 * mat.m_f21) + (temp.m_f33 * mat.m_f31) + (temp.m_f34 * mat.m_f41);
	m_f32 = (temp.m_f31 * mat.m_f12) + (temp.m_f32 * mat.m_f22) + (temp.m_f33 * mat.m_f32) + (temp.m_f34 * mat.m_f42);
	m_f33 = (temp.m_f31 * mat.m_f13) + (temp.m_f32 * mat.m_f23) + (temp.m_f33 * mat.m_f33) + (temp.m_f34 * mat.m_f43);
	m_f34 = (temp.m_f31 * mat.m_f14) + (temp.m_f32 * mat.m_f24) + (temp.m_f33 * mat.m_f34) + (temp.m_f34 * mat.m_f44);
	/*****************Fourth row***************************/
	m_f41 = (temp.m_f41 * mat.m_f11) + (temp.m_f42 * mat.m_f21) + (temp.m_f43 * mat.m_f31) + (temp.m_f44 * mat.m_f41);
	m_f42 = (temp.m_f41 * mat.m_f12) + (temp.m_f42 * mat.m_f22) + (temp.m_f43 * mat.m_f32) + (temp.m_f44 * mat.m_f42);
	m_f43 = (temp.m_f41 * mat.m_f13) + (temp.m_f42 * mat.m_f23) + (temp.m_f43 * mat.m_f33) + (temp.m_f44 * mat.m_f43);
	m_f44 = (temp.m_f41 * mat.m_f14) + (temp.m_f42 * mat.m_f24) + (temp.m_f43 * mat.m_f34) + (temp.m_f44 * mat.m_f44);

	return *this;
}

Matrix4x4& Matrix4x4::operator *= (const FFLOAT& val)
{
	m_f11 *= val;
	m_f12 *= val;
	m_f13 *= val;
	m_f14 *= val;
	m_f21 *= val;
	m_f22 *= val;
	m_f23 *= val;
	m_f24 *= val;
	m_f31 *= val;
	m_f32 *= val;
	m_f33 *= val;
	m_f34 *= val;
	m_f41 *= val;
	m_f42 *= val;
	m_f43 *= val;
	m_f44 *= val;

	return *this;
}

Matrix4x4& Matrix4x4::operator /= (const FFLOAT& val)
{
	m_f11 /= val;
	m_f12 /= val;
	m_f13 /= val;
	m_f14 /= val;
	m_f21 /= val;
	m_f22 /= val;
	m_f23 /= val;
	m_f24 /= val;
	m_f31 /= val;
	m_f32 /= val;
	m_f33 /= val;
	m_f34 /= val;
	m_f41 /= val;
	m_f42 /= val;
	m_f43 /= val;
	m_f44 /= val;

	return *this;
}

Matrix4x4 Matrix4x4::operator + (const Matrix4x4& mat) const
{
	Matrix4x4 temp(*this);
	temp += mat;

	return temp;
}

Matrix4x4 Matrix4x4::operator - (const Matrix4x4& mat) const
{
	Matrix4x4 temp(*this);
	temp -= mat;

	return temp;
}

Matrix4x4 Matrix4x4::operator * (const Matrix4x4& mat) const
{
	Matrix4x4 temp(*this);
	temp *= mat;

	return temp;
}

Matrix4x4 Matrix4x4::operator * (const FFLOAT& val) const
{
	Matrix4x4 temp(*this);
	temp *= val;

	return temp;
}

Matrix4x4 Matrix4x4::operator / (const FFLOAT& val) const
{
	Matrix4x4 temp(*this);
	temp /= val;

	return temp;
}

BBOOL Matrix4x4::operator == (const Matrix4x4& mat) const
{
	return (Equal<FFLOAT>(m_f11, mat.m_f11) && Equal<FFLOAT>(m_f12, mat.m_f12) && Equal<FFLOAT>(m_f13, mat.m_f13) && Equal<FFLOAT>(m_f14, mat.m_f14) &&
		    Equal<FFLOAT>(m_f21, mat.m_f21) && Equal<FFLOAT>(m_f22, mat.m_f22) && Equal<FFLOAT>(m_f23, mat.m_f23) && Equal<FFLOAT>(m_f24, mat.m_f24) &&
			Equal<FFLOAT>(m_f31, mat.m_f31) && Equal<FFLOAT>(m_f32, mat.m_f32) && Equal<FFLOAT>(m_f33, mat.m_f33) && Equal<FFLOAT>(m_f34, mat.m_f34) &&
			Equal<FFLOAT>(m_f41, mat.m_f41) && Equal<FFLOAT>(m_f42, mat.m_f42) && Equal<FFLOAT>(m_f43, mat.m_f43) && Equal<FFLOAT>(m_f44, mat.m_f44));
}

BBOOL Matrix4x4::operator != (const Matrix4x4& mat) const
{
	return (Different<FFLOAT>(m_f11, mat.m_f11) || Different<FFLOAT>(m_f12, mat.m_f12) || Different<FFLOAT>(m_f13, mat.m_f13) || Different<FFLOAT>(m_f14, mat.m_f14) ||
		    Different<FFLOAT>(m_f21, mat.m_f21) || Different<FFLOAT>(m_f22, mat.m_f22) || Different<FFLOAT>(m_f23, mat.m_f23) || Different<FFLOAT>(m_f24, mat.m_f24) ||
			Different<FFLOAT>(m_f31, mat.m_f31) || Different<FFLOAT>(m_f32, mat.m_f32) || Different<FFLOAT>(m_f33, mat.m_f33) || Different<FFLOAT>(m_f34, mat.m_f34) ||
			Different<FFLOAT>(m_f41, mat.m_f41) || Different<FFLOAT>(m_f42, mat.m_f42) || Different<FFLOAT>(m_f43, mat.m_f43) || Different<FFLOAT>(m_f44, mat.m_f44));
}

Matrix4x4 operator * (FFLOAT val, const Matrix4x4& mat)
{
	Matrix4x4 temp(mat);
	temp *= val;

	return temp;
}

Vector4 Matrix4x4::Transform(const Vector4& v)
{
	/*
	return Vector4(m_f11 * v.getX() + m_f12 * v.getY() + m_f13 * v.getZ() + m_f14 * v.getW(),
				   m_f21 * v.getX() + m_f22 * v.getY() + m_f23 * v.getZ() + m_f24 * v.getW(),
				   m_f31 * v.getX() + m_f32 * v.getY() + m_f33 * v.getZ() + m_f34 * v.getW(),
				   (m_f41 * 0.5f) * v.getX() + (m_f42 * 0.5f) * v.getY() + (m_f43 * 0.5f) * v.getZ() + (m_f44 * 0.5f) * v.getW());
	*/
	return Vector4(m_f11 * v.getX() + m_f21 * v.getY() + m_f31 * v.getZ() + (m_f41 * 0.5f) * v.getW(),
				   m_f12 * v.getX() + m_f22 * v.getY() + m_f32 * v.getZ() + (m_f42 * 0.5f) * v.getW(),
				   m_f13 * v.getX() + m_f23 * v.getY() + m_f33 * v.getZ() + (m_f43 * 0.5f) * v.getW(),
				   m_f14 * v.getX() + m_f24 * v.getY() + m_f34 * v.getZ() + m_f44 * v.getW());
}
