#ifndef DEF_OCELOTINPUTUTIL_H
#define DEF_OCELOTINPUTUTIL_H

#include <Windows.h>
#include <iostream>

// definition of some useful macro
#define DeletePointer(X) {if(X){delete X; X = NULL;}}
#define ReleaseID(X) {if(X){ X->Unacquire(); X->Release(); X = NULL;}}

typedef long long      BIGLINT;
typedef long           LINT;
typedef int            BIGINT;
typedef short          SMALLINT;
typedef float          FFLOAT;
typedef bool           BBOOL;
typedef double         DFLOAT;
typedef char           CCHAR;
typedef wchar_t        WCHAR;
typedef unsigned long  ULINT;
typedef unsigned int   UBIGINT;
typedef unsigned short USMALLINT;
typedef unsigned char  UCCHAR;
typedef void           VVOID;
typedef void*          UNKNOWN;

namespace Ocelot
{
	enum InputType
	{
		Mouse,
		Keyboard,
		Joystick
	};
}

#endif