#ifndef DEF_OCELOTINPUT_H
#define DEF_OCELOTINPUT_H

#include "IOcelotInputDevice.h"

namespace Ocelot
{
	class OcelotInput
	{
	private:

			// Attributes
			IOcelotInputDevice* m_pDevice;
			HINSTANCE           m_hInst;
			HMODULE             m_hDllMod;

	public:

			// Constructor & Destructor
			OcelotInput(HINSTANCE hInst);
			~OcelotInput();

			// Methods
			HRESULT createDevice();
			VVOID    release();

			// Accessors
	inline  IOcelotInputDevice* GetDevice() const { return m_pDevice;}
	inline  HINSTANCE           GetModule() const { return m_hDllMod;}
	};
}

#endif