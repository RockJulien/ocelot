#include "OcelotInput.h"

using namespace Ocelot;

OcelotInput::OcelotInput(HINSTANCE hInst) :
m_hInst(hInst), m_pDevice(NULL), m_hDllMod(NULL)
{

}

OcelotInput::~OcelotInput()
{
	release();

#if defined(DEBUG) || defined(_DEBUG)
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_WNDW);
#endif
}

HRESULT OcelotInput::createDevice()
{
#if defined(DEBUG) || defined(_DEBUG)
	
#endif

	// load the DLL from the Ocelot Input static library.
	m_hDllMod = LoadLibraryEx(L"OcelotDirectInput.dll", NULL, 0);

	if(!m_hDllMod)
	{
		// if the DLL fucked up
		MessageBox(NULL, L"Ocelot Direct Input DLL load FAILED !!!", L"Ocelot Direct Input Module ERROR !!!", MB_OK | MB_ICONERROR);
		return E_FAIL;
	}

	CREATEINPUTDEVICE ID = NULL;

	// Function who point to the DLL's create Input Device function.
	ID = (CREATEINPUTDEVICE)GetProcAddress(m_hDllMod, "createInputDevice");

	// and now launch the creation
	if(FAILED(ID(m_hDllMod, &m_pDevice)))
	{
		MessageBox(NULL, L"Ocelot CreateInputDevice function FAILED !!!", L"Ocelot Direct Input Module ERROR !!!", MB_OK | MB_ICONERROR);
		m_pDevice = NULL;
		return E_FAIL;
	}

	return S_OK;
}

VVOID    OcelotInput::release()
{
	RELEASEINPUTDEVICE RID = NULL;

	if(m_hDllMod)
		// function who points to the DLL release Input Device function.
		RID = (RELEASEINPUTDEVICE)GetProcAddress(m_hDllMod, "releaseInputDevice");

	if(m_pDevice)
		if(FAILED(RID(&m_pDevice)))
			m_pDevice = NULL;
}
