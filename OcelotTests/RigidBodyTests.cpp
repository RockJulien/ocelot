#ifndef RIGIDBODY_H
#include "../Ocelot Editor/Physics/RigidBody.h"
#include "../Ocelot Editor/Physics/RigidBody.cpp"
#include "../Ocelot Editor/Maths/Quaternion.h"
#include "../Ocelot Editor/Maths/Matrix3x4.h"
#include "../Ocelot Editor/Maths/Matrix3x4.cpp"
#endif

using namespace Ocelot;

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(RigidBodyGetMassTest)
{
	RigidBody b;

	b.SetMass(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(b.GetMass() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(RigidBodyGetInverseMassTest)
{
	RigidBody b;

	b.SetInverseMass(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(b.GetInverseMass() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(RigidBodyHasFiniteMassTest1)
{
	RigidBody b;

	b.SetMass(1.0f);

	BBOOL expectedResult = true;

	BOOST_CHECK(b.HasFiniteMass() == expectedResult);
}

BOOST_AUTO_TEST_CASE(RigidBodyHasFiniteMassTest2)
{
	RigidBody b;

	b.SetInverseMass(0.0f);

	BBOOL expectedResult = false;

	BOOST_CHECK(b.HasFiniteMass() == expectedResult);
}

BOOST_AUTO_TEST_CASE(RigidBodyGetLinearDampingTest)
{
	RigidBody b;
	b.SetLinearDamping(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(b.GetLinearDamping() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(RigidBodyGetAngularDampingTest)
{
	RigidBody b;
	b.SetAngularDamping(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(b.GetAngularDamping() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(RigidBodySetPositionTest)
{
	RigidBody b;
	b.SetPosition(Vector3(0.0f, 1.0f, 2.0f));

	Vector3 expectedResult(0.0f, 1.0f, 2.0f);

	BOOST_CHECK(b.GetRigidBodyPosition() == expectedResult);
}

BOOST_AUTO_TEST_CASE(RigidBodySetVelocityTest)
{
	RigidBody b;
	b.SetVelocity(Vector3(0.0f, 1.0f, 2.0f));

	Vector3 expectedResult(0.0f, 1.0f, 2.0f);

	BOOST_CHECK(b.GetRigidBodyVelocity() == expectedResult);
}

BOOST_AUTO_TEST_CASE(RigidBodySetAccelerationTest)
{
	RigidBody b;
	b.SetAcceleration(Vector3(0.0f, 1.0f, 2.0f));

	Vector3 expectedResult(0.0f, 1.0f, 2.0f);

	BOOST_CHECK(b.GetRigidBodyAcceleration() == expectedResult);
}

BOOST_AUTO_TEST_CASE(RigidBodySetPrevAccelerationTest)
{
	RigidBody b;
	b.SetPrevAcceleration(Vector3(0.0f, 1.0f, 2.0f));

	Vector3 expectedResult(0.0f, 1.0f, 2.0f);

	BOOST_CHECK(b.GetPrevAcceleration() == expectedResult);
}

BOOST_AUTO_TEST_CASE(RigidBodySetAngularVelocityTest)
{
	RigidBody b;
	b.SetAngularVelocity(Vector3(0.0f, 1.0f, 2.0f));

	Vector3 expectedResult(0.0f, 1.0f, 2.0f);

	BOOST_CHECK(b.GetRigidBodyAngularVelocity() == expectedResult);
}

BOOST_AUTO_TEST_CASE(RigidBodySetOrientationTest)
{
	RigidBody b;
	b.SetOrientation(Quaternion(0.0f, 0.0f, 0.0f, 1.0f));

	Quaternion expectedResult(0.0f, 0.0f, 0.0f, 1.0f);

	BOOST_CHECK(b.GetRigidBodyOrientation() == expectedResult);
}