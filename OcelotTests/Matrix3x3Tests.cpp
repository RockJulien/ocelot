#ifndef DEF_MATRIX3X3_H
#include "..\Ocelot Editor\Maths\Matrix3x3.h"
#include "..\Ocelot Editor\Maths\Matrix3x3.cpp"
#endif

#include "boost\test\unit_test.hpp"

using namespace std;

BOOST_AUTO_TEST_CASE(identityMat3x3_test)
{
	Matrix3x3 mat;
	mat.Matrix3x3Identity();

	Matrix3x3 expectedResult(1.0f, 0.0f, 0.0f,
							 0.0f, 1.0f, 0.0f,
							 0.0f, 0.0f, 1.0f);

	BOOST_CHECK(mat == expectedResult);
}

BOOST_AUTO_TEST_CASE(isIdentityMat3x3_test)
{
	Matrix3x3 mat;
	mat.Matrix3x3Identity();

	Matrix3x3 temp(1.0f, 0.0f, 0.0f,
				   0.0f, 1.0f, 0.0f,
				   0.0f, 0.0f, 1.0f);
	Matrix3x3 temp2(1.0f, 0.0f, 0.0f,
					0.0f, 3.0f, 6.0f,
					0.0f, 0.0f, 2.0f);

	BBOOL expectedResult = true;
	BBOOL expectedResult2 = false;

	BOOST_CHECK((mat == temp) == expectedResult);
	BOOST_CHECK((mat == temp2) == expectedResult2);
	BOOST_CHECK(mat.Matrix3x3IsIdentity() == expectedResult);
	BOOST_CHECK(temp2.Matrix3x3IsIdentity() == expectedResult2);
}

BOOST_AUTO_TEST_CASE(transposeMat3x3_test)
{
	Matrix3x3 mat( 2.0f,  4.0f,  6.0f,
				   8.0f, 10.0f, 12.0f,
				  14.0f, 16.0f, 18.0f);
	Matrix3x3 staySameAfterTranspose = mat;
	Matrix3x3 result;
	mat.Matrix3x3Transpose(result);

	Matrix3x3 expectedResult( 2.0f,  8.0f, 14.0f,
							  4.0f, 10.0f, 16.0f,
							  6.0f, 12.0f, 18.0f);

	BOOST_CHECK((staySameAfterTranspose == mat) == true);
	BOOST_CHECK(result == expectedResult);
}

BOOST_AUTO_TEST_CASE(inverseMat3x3_test)
{
	Matrix3x3 mat( 2.0f,  4.0f,  6.0f,
				   8.0f,  5.0f, 12.0f,
				   7.0f, 16.0f, 13.0f);
	Matrix3x3 staySameAfterInverse = mat;
	Matrix3x3 result;
	mat.Matrix3x3Inverse(result);

	/*cout << result.get_11() << " " << result.get_12() << " " << result.get_13() << " " << endl
		 << result.get_21() << " " << result.get_22() << " " << result.get_23() << " " << endl
		 << result.get_31() << " " << result.get_32() << " " << result.get_33() << " " << endl;*/

	Matrix3x3 expectedResult(-0.56696f, 0.19642f, 0.080357f,
							 -0.089285f,-0.071428f, 0.10714f,
							  0.41517f,-0.017857f,-0.098214f);

	BOOST_CHECK((staySameAfterInverse == mat) == true);
	BOOST_CHECK((result.get_11() - expectedResult.get_11()) < 0.00001f);
	BOOST_CHECK((result.get_12() - expectedResult.get_12()) < 0.00001f);
	BOOST_CHECK((result.get_13() - expectedResult.get_13()) < 0.00001f);
	BOOST_CHECK((result.get_21() - expectedResult.get_21()) < 0.00001f);
	BOOST_CHECK((result.get_22() - expectedResult.get_22()) < 0.00001f);
	BOOST_CHECK((result.get_23() - expectedResult.get_23()) < 0.00001f);
	BOOST_CHECK((result.get_31() - expectedResult.get_31()) < 0.00001f);
	BOOST_CHECK((result.get_32() - expectedResult.get_32()) < 0.00001f);
	BOOST_CHECK((result.get_33() - expectedResult.get_33()) < 0.00001f);
}

BOOST_AUTO_TEST_CASE(determinantMat3x3_test)
{
	Matrix3x3 mat( 2.0f,  4.0f,  6.0f,
				   8.0f,  5.0f, 12.0f,
				   7.0f, 16.0f, 13.0f);

	FFLOAT val = mat.Matrix3x3Determinant();

	FFLOAT expectedResult = 224.0f;

	BOOST_CHECK(fabsf(val - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(multiplyMat3x3_test)
{
	Matrix3x3 mat1( 2.0f,  4.0f,  6.0f,
				    8.0f,  5.0f, 12.0f,
				    7.0f, 16.0f, 13.0f);
	Matrix3x3 mat2(10.0f,  3.0f, 18.0f,
				    1.0f,  7.0f, 17.0f,
				    9.0f,  9.0f, 21.0f);
	Matrix3x3 stillTheSame = mat1;
	Matrix3x3 result;

	mat1.Matrix3x3Mutliply(result, mat2);

	Matrix3x3 expectedResult( 78.0f, 88.0f,230.0f,
							 193.0f,167.0f,481.0f,
							 203.0f,250.0f,671.0f);

	BOOST_CHECK(stillTheSame == mat1);
	BOOST_CHECK(result == expectedResult);
	BOOST_CHECK((mat1 * mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(translateMat3x3_test)
{
	FFLOAT Tx = 2.0f;
	FFLOAT Ty = 3.0f;

	Matrix3x3 matTrans;
	matTrans.Matrix3x3Identity();
	matTrans.Matrix3x3Translation(matTrans, Tx, Ty); 

	Matrix3x3 expectedResult;
	expectedResult.Matrix3x3Identity();
	expectedResult.set_31(Tx);  // for 2D Games
	expectedResult.set_32(Ty);

	BOOST_CHECK(Equal<Matrix3x3>(matTrans, expectedResult));
}

BOOST_AUTO_TEST_CASE(rotateMat3x3_test)
{
	FFLOAT valRx = 2.0f;
	FFLOAT Cos = cosf(valRx);
	FFLOAT Sin = sinf(valRx);

	Matrix3x3 matTrans;
	matTrans.Matrix3x3Identity();
	matTrans.Matrix3x3RotationX(matTrans, valRx); 

	Matrix3x3 expectedResult;
	expectedResult.Matrix3x3Identity();
	expectedResult.set_22(Cos);  // for 2D Games
	expectedResult.set_23(Sin);
	expectedResult.set_32(-Sin);
	expectedResult.set_33(Cos);

	BOOST_CHECK(Equal<Matrix3x3>(matTrans, expectedResult));
}

BOOST_AUTO_TEST_CASE(scaleMat3x3_test)
{
	FFLOAT Sx = 2.0f;
	FFLOAT Sy = 3.0f;

	Matrix3x3 matTrans;
	matTrans.Matrix3x3Identity();
	matTrans.Matrix3x3Scaling(matTrans, Sx, Sy); 

	Matrix3x3 expectedResult;
	expectedResult.Matrix3x3Identity();
	expectedResult.set_11(Sx);  // for 2D Games
	expectedResult.set_22(Sy);

	BOOST_CHECK(Equal<Matrix3x3>(matTrans, expectedResult));
}

BOOST_AUTO_TEST_CASE(rotationGenMat3x3_test)
{
	FFLOAT Rx = 2.0f;
	FFLOAT Ry = 3.0f;
	FFLOAT Rz = 4.0f;

	Matrix3x3 matTrans;
	matTrans.Matrix3x3Identity();
	matTrans.Matrix3x3RotationGeneralized(matTrans, Rx, Ry, Rz); 

	Matrix3x3 expectedResult;
	expectedResult.Matrix3x3Identity();

	FFLOAT CosAlpha = cos(Rz);  // Yaw (Z)
	FFLOAT SinAlpha = sin(Rz);  // Yaw (Z)
	FFLOAT CosBeta  = cos(Ry);  // Pitch (Y)
	FFLOAT SinBeta  = sin(Ry);  // Pitch (Y)
	FFLOAT CosPhi   = cos(Rx);  // Roll (X)
	FFLOAT SinPhi   = sin(Rx);  // Roll (X)

	expectedResult.set_11((CosAlpha * CosBeta)); expectedResult.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); expectedResult.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	expectedResult.set_21((SinAlpha * CosBeta)); expectedResult.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); expectedResult.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	expectedResult.set_31((-SinBeta));           expectedResult.set_32((CosBeta * SinPhi));                                    expectedResult.set_33((CosBeta * CosPhi));

	BOOST_CHECK(Equal<Matrix3x3>(matTrans, expectedResult));
}

BOOST_AUTO_TEST_CASE(doubleMultiplyMat3x3_test)
{
	Matrix3x3 mat1( 2.0f,  4.0f,  6.0f,
				    8.0f,  5.0f, 12.0f,
				    7.0f, 16.0f, 13.0f);
	Matrix3x3 mat2(10.0f,  3.0f, 18.0f,
				    1.0f,  7.0f, 17.0f,
				    9.0f,  9.0f, 21.0f);
	Matrix3x3 mat3( 1.0f,  7.0f, 19.0f,
				   15.0f, 12.0f,  3.0f,
				    1.0f,  2.0f,  6.0f);
	Matrix3x3 stillTheSame = mat1;
	Matrix3x3 stillTheSame2 = mat2;
	Matrix3x3 result;

	mat1.Matrix3x3Mutliply(result, mat2, mat3);

	Matrix3x3 expectedResult(1628.0f,2062.0f,3126.0f,
							 3179.0f,4317.0f,7054.0f,
							 4624.0f,5763.0f,8633.0f);

	BOOST_CHECK(stillTheSame == mat1);
	BOOST_CHECK(stillTheSame2 == mat2);
	BOOST_CHECK(result == expectedResult);
	BOOST_CHECK((mat1 * mat2 * mat3) == expectedResult);

}

BOOST_AUTO_TEST_CASE(addMat3x3_test)
{
	Matrix3x3 mat1( 1.0f,  2.0f,  3.0f,
				    4.0f,  5.0f,  6.0f,
				    7.0f,  8.0f,  9.0f);
	Matrix3x3 mat2(10.0f, 11.0f, 12.0f,
				   13.0f, 14.0f, 15.0f,
				   16.0f, 17.0f, 18.0f);

	Matrix3x3 expectedResult(11.0f, 13.0f, 15.0f,
							 17.0f, 19.0f, 21.0f,
							 23.0f, 25.0f, 27.0f);

	BOOST_CHECK((mat1 + mat2) == expectedResult);
	BOOST_CHECK((mat1 += mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(subMat3x3_test)
{
	Matrix3x3 mat1( 1.0f,  2.0f,  3.0f,
				    4.0f,  5.0f,  6.0f,
				    7.0f,  8.0f,  9.0f);
	Matrix3x3 mat2(10.0f, 11.0f, 12.0f,
				   13.0f, 14.0f, 15.0f,
				   16.0f, 17.0f, 18.0f);

	Matrix3x3 expectedResult(-9.0f, -9.0f, -9.0f,
							 -9.0f, -9.0f, -9.0f,
							 -9.0f, -9.0f, -9.0f);

	BOOST_CHECK((mat1 - mat2) == expectedResult);
	BOOST_CHECK((mat1 -= mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(divMat3x3_test)
{
	Matrix3x3 mat1( 1.0f,  2.0f,  3.0f,
				    4.0f,  5.0f,  6.0f,
				    7.0f,  8.0f,  9.0f);
	FFLOAT val = 2.0f;

	Matrix3x3 expectedResult( 0.5f,  1.0f,  1.5f,
							  2.0f,  2.5f,  3.0f,
							  3.5f,  4.0f,  4.5f);

	BOOST_CHECK((mat1 / val) == expectedResult);
	BOOST_CHECK((mat1 /= val) == expectedResult);
}

BOOST_AUTO_TEST_CASE(mulMat3x3_test)
{
	Matrix3x3 mat1( 2.0f,  4.0f,  6.0f,
				    8.0f,  5.0f, 12.0f,
				    7.0f, 16.0f, 13.0f);
	Matrix3x3 mat2(10.0f,  3.0f, 18.0f,
				    1.0f,  7.0f, 17.0f,
				    9.0f,  9.0f, 21.0f);

	Matrix3x3 expectedResult( 78.0f, 88.0f,230.0f,
							 193.0f,167.0f,481.0f,
							 203.0f,250.0f,671.0f);

	BOOST_CHECK((mat1 * mat2) == expectedResult);
	BOOST_CHECK((mat1 *= mat2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(mulConstMat3x3_test)
{
	Matrix3x3 mat1( 2.0f,  4.0f,  6.0f,
				    8.0f,  5.0f, 12.0f,
				    7.0f, 16.0f, 13.0f);
	FFLOAT val = 2.0f;

	Matrix3x3 expectedResult( 4.0f,  8.0f, 12.0f,
							 16.0f, 10.0f, 24.0f,
							 14.0f, 32.0f, 26.0f);

	BOOST_CHECK((mat1 * val) == expectedResult);
	BOOST_CHECK((mat1 *= val) == expectedResult);
}
