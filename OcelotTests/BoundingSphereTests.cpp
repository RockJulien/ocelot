#ifndef BOUNDINGSPHERE_H
#include "../Ocelot Editor/Collisions/BoundingSphere.h"
#include "../Ocelot Editor/Collisions/BoundingSphere.cpp"
#include "../Ocelot Editor/Geometry/Circle.h"
#include "../Ocelot Editor/Geometry/Circle.cpp"
#endif

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(BoundingSphereIntersectionTest1)
{
	BoundingSphere a;
	BoundingSphere b;

	a.SetCentre(Vector3(0.0f, 0.0f, 0.0f));
	a.SetRadius(5.0f);

	b.SetCentre(Vector3(1.0f, 1.0f, 1.0f));
	b.SetRadius(2.0f);

	BBOOL expectedResult = true;
	BBOOL actualResult = a.Intersects(b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(BoundingSphereIntersectionTest2)
{
	BoundingSphere a;
	BoundingSphere b;

	a.SetCentre(Vector3(0.0f, 0.0f, 0.0f));
	a.SetRadius(5.0f);

	b.SetCentre(Vector3(10.0f, 10.0f, 10.0f));
	b.SetRadius(2.0f);

	BBOOL expectedResult = false;
	BBOOL actualResult = a.Intersects(b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(BoundingSphereGetCentreTest1)
{
	BoundingSphere a;

	Vector3 expectedResult(1.0f,2.0f,3.0f);

	a.SetCentre(expectedResult);

	BOOST_CHECK(a.GetCentre() == expectedResult);
}

BOOST_AUTO_TEST_CASE(BoundingSphereGetCentreTest2)
{
	BoundingSphere a;

	Vector3 expectedResult(1.0f,2.0f,3.0f);

	a.SetCentre(Vector3(1.0f,2.0f,3.0f));

	BOOST_CHECK(a.GetCentre() == expectedResult);
}

BOOST_AUTO_TEST_CASE(BoundingSphereGetCentreXTest)
{
	BoundingSphere a;

	a.SetCentre(Vector3(1.0f,2.0f,3.0f));

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetCentre()[0] - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BoundingSphereGetCentreYTest)
{
	BoundingSphere a;

	a.SetCentre(Vector3(1.0f,2.0f,3.0f));

	FFLOAT expectedResult = 2.0f;

	BOOST_CHECK(fabsf(a.GetCentre()[1] - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BoundingSphereGetCentreZTest)
{
	BoundingSphere a;

	a.SetCentre(Vector3(1.0f,2.0f,3.0f));

	FFLOAT expectedResult = 3.0f;

	BOOST_CHECK(fabsf(a.GetCentre()[2] - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BoundingSphereGetRadiusTest)
{
	BoundingSphere a;

	a.SetRadius(3.0f);

	FFLOAT expectedResult = 3.0f;

	BOOST_CHECK(fabsf(a.GetRadius() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BoundingCircleIntersectionTest1)
{
	BoundingCircle a;
	BoundingCircle b;

	a.SetCentre(Vector2(0.0f,0.0f));
	a.SetRadius(10.0f);

	b.SetCentre(Vector2(1.0f, 1.0f));
	b.SetCentre(2.0f);

	BBOOL expectedResult = true;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(BoundingCircleIntersectionTest2)
{
	BoundingCircle a;
	BoundingCircle b;

	a.SetCentre(Vector2(0.0f,0.0f));
	a.SetRadius(10.0f);

	b.SetCentre(Vector2(13.0f, 13.0f));
	b.SetRadius(2.0f);

	BBOOL expectedResult = false;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(BoundingCircleGetCentreTest1)
{
	BoundingCircle a;

	Vector2 expectedResult(1.0f, 2.0f);

	a.SetCentre(expectedResult);

	BOOST_CHECK(a.GetCentre() == expectedResult);
}

BOOST_AUTO_TEST_CASE(BoundingCircleGetCentreTest2)
{
	BoundingCircle a;

	Vector2 expectedResult(1.0f, 2.0f);

	a.SetCentre(Vector2(1.0f, 2.0f));

	BOOST_CHECK(a.GetCentre() == expectedResult);
}

BOOST_AUTO_TEST_CASE(BoundingCircleGetCentreXTest)
{
	BoundingCircle a;

	a.SetCentre(Vector2(1.0f, 2.0f));

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetCentre().getX() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BoundingCircleGetCentreYTest)
{
	BoundingCircle a;

	a.SetCentre(Vector2(1.0f, 2.0f));

	FFLOAT expectedResult = 2.0f;

	BOOST_CHECK(fabsf(a.GetCentre().getY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BoundingCircleGetRadiusTest)
{
	BoundingCircle a;

	a.SetRadius(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetRadius() - expectedResult) < FLT_EPSILON);
}