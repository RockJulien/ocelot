#ifndef PARTICLESYSTEM_H
#include "../Ocelot Editor/Particle System/ParticleSystem.h"
#include "../Ocelot Editor/Particle System/ParticleSystem.cpp"
#endif

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(ParticleSystemGetMaxParticlesTest)
{
	ParticleSystem ps;

	ps.SetMaxParticles(1);

	BIGINT expectedResult = 1;

	BOOST_CHECK(ps.GetMaxParticles() == expectedResult);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetSpreadTest)
{
	ParticleSystem ps;

	ps.SetSpread(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(ps.GetSpread() == expectedResult);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetAgeTest)
{
	ParticleSystem ps;

	ps.ResetParticleSystem(); //sets age to 0

	FFLOAT expectedResult = 0.0f;

	BOOST_CHECK(fabsf(ps.GetAge() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetEyePositionTest)
{
	ParticleSystem ps;

	ps.SetEyePosition(Vector3(0.0f, 1.0f, 2.0f));

	Vector4 eyePos = ps.GetEyePosition();
	Vector3 eyePos3D(eyePos.getX(), eyePos.getY(), eyePos.getZ());
	
	Vector3 expectedResult(0.0f, 1.0f, 2.0f);

	BOOST_CHECK(eyePos3D == expectedResult);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetMeshIndexTest)
{
	ParticleSystem ps;
	ps.SetMeshIndex(1);

	BIGINT expectedResult = 1;

	BOOST_CHECK(ps.GetMeshIndex() == expectedResult);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetEmitDirectionTest)
{
	ParticleSystem ps;
	ps.SetEmitDirection(Vector3(0.0f, 1.0f, 0.0f));

	Vector4 emitDir = ps.GetEmitDirection();
	Vector3 emitDir3D(emitDir.getX(), emitDir.getY(), emitDir.getZ());

	Vector3 expectedResult(0.0f, 1.0f, 0.0f);

	BOOST_CHECK(emitDir3D == expectedResult);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetWindForceTest)
{
	ParticleSystem ps;
	ps.SetWindForce(Vector3(0.0f, 1.0f, 0.0f));

	Vector4 windForce = ps.GetWindForce();
	Vector3 windForce3D(windForce.getX(), windForce.getY(), windForce.getZ());

	Vector3 expectedResult(0.0f, 1.0f, 0.0f);

	BOOST_CHECK(windForce3D == expectedResult);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetAccelerationTest)
{
	ParticleSystem ps;
	ps.SetAcceleration(Vector3(0.0f, 1.0f, 0.0f));

	Vector4 acceleration = ps.GetAcceleration();
	Vector3 acceleration3D(acceleration.getX(), acceleration.getY(), acceleration.getZ());

	Vector3 expectedResult(0.0f, 1.0f, 0.0f);

	BOOST_CHECK(acceleration3D == expectedResult);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetWindAmplitudeTest)
{
	ParticleSystem ps;
	ps.SetWindAmplitude(Vector3(0.0f, 1.0f, 0.0f));

	Vector3 expectedResult(0.0f, 1.0f, 0.0f);

	BOOST_CHECK(ps.GetWindAmplitude() == expectedResult);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetPeriodicityTest)
{
	ParticleSystem ps;
	ps.SetWindPeriodicity(Vector3(0.0f, 1.0f, 0.0f));

	Vector3 expectedResult(0.0f, 1.0f, 0.0f);

	BOOST_CHECK(ps.GetWindPeriodicity() == expectedResult);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetEmitterOffsetTest)
{
	ParticleSystem ps;
	ps.SetEmitterOffset(Vector3(0.0f, 1.0f, 0.0f));

	Vector3 expectedResult(0.0f, 1.0f, 0.0f);

	BOOST_CHECK(ps.GetEmitterOffset() == expectedResult);
}

BOOST_AUTO_TEST_CASE(ParticleSystemGetEmitterPositionTest)
{
	ParticleSystem ps;
	ps.SetEmitterPosition(Vector3(0.0f, 1.0f, 0.0f));

	Vector3 expectedResult(0.0f, 1.0f, 0.0f);

	BOOST_CHECK(ps.GetEmitterPosition() == expectedResult);
}