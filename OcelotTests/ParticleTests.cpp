#ifndef PARTICLE_H
#include "../Ocelot Editor/Physics/Particle.h"
#include "../Ocelot Editor/Physics/Particle.cpp"
#endif

#include <boost/test/unit_test.hpp>
#include <iostream>

using std::cout;
using std::endl;

/*
 Floating point error is incredibly annoying, using FLT_EPSILON makes the BOOST_CHECKS fail.
 But when you print out the actual results and the expected results, they are IDENTICAL.
 It seems that floating point error makes some of the numbers incorrect by around +/- 0.000001f
 and that's JUST enough to skew the final error by more than the size of FLT_EPSILON.
 So we'll have to settle for 5 decimal points of precision instead of 7
 Euler integration is only a 1st order method anyway, its main weakness is the error it produces.
*/
const FFLOAT INTEGRATION_PRECISION = 0.00001f;

BOOST_AUTO_TEST_CASE(EulerIntegrationTest)
{
	Particle testParticle;

	//Set up initial conditions
	testParticle.SetPosition(Vector3(4.0f, 1.0f, 20.0f));
	testParticle.SetVelocity(Vector3(6.0f, 20.0f, 2.0f));
	testParticle.SetAcceleration(Vector3(1.0f, 1.0f, 1.0f));
	testParticle.SetDamping(0.985f);
	testParticle.SetMass(2.0f);
	testParticle.AddForce(Vector3(1.0f, 2.0f, 3.0f));

	testParticle.EulerIntegrate(1.0f);

	/*
	Calculations:
	Vector3 resultingAcc = acceleration = (1.0f, 1.0f, 1.0f)
	resultingAcc += forceAccum * inverseMass 
	= (1.0f, 1.0f, 1.0f) + (1.0f, 2.0f, 3.0f) * 0.5f 
	= (1.0f, 1.0f, 1.0f) + (0.5f, 1.0f, 1.5f)
	= (1.5f, 2.0f, 2.5f)

	position += velocity * delta
	= (4.0f, 1.0f, 20.0f) + (6.0f, 20.0f, 2.0f) * 1.0f;
	= (10.0f, 21.0f, 22.0f)

	velocity += resultingAcc * delta
	= (6.0f, 20.0f, 2.0f) + (1.5f, 2.0f, 2.5f) * 1.0f
	= (7.5f, 22.0f, 4.5f)

	velocity *= pow(damping, delta)
	= (7.5f, 22.0f, 4.5f) * 0.985f ^ 1.0f
	= (7.3875f, 21.67f, 4.4325f)
	*/

	Vector3 expectedPosition(10.0f, 21.0f, 22.0f);
	Vector3 expectedVelocity(7.3875f, 21.67f, 4.4325f);

	Vector3* pos = &(testParticle.GetPosition());
	BOOST_CHECK(abs(pos->getX() - expectedPosition.getX()) < INTEGRATION_PRECISION);
	BOOST_CHECK(abs(pos->getY() - expectedPosition.getY()) < INTEGRATION_PRECISION);
	BOOST_CHECK(abs(pos->getZ() - expectedPosition.getZ()) < INTEGRATION_PRECISION);
	
	Vector3* vel = &(testParticle.GetVelocity());
	//cout << testParticle.GetVelocity().getX() << " : " << expectedVelocity.getX() << endl;
	BOOST_CHECK(abs(vel->getX() - expectedVelocity.getX()) < INTEGRATION_PRECISION);
	BOOST_CHECK(abs(vel->getY() - expectedVelocity.getY()) < INTEGRATION_PRECISION);
	BOOST_CHECK(abs(vel->getZ() - expectedVelocity.getZ()) < INTEGRATION_PRECISION);
}

/*
BOOST_AUTO_TEST_CASE(SymplecticEulerIntegrationTest)
{
	Particle testParticle;

	//Set up initial conditions
	testParticle.SetPosition(Vector3(4.0f, 1.0f, 20.0f));
	testParticle.SetVelocity(Vector3(6.0f, 20.0f, 2.0f));
	testParticle.SetAcceleration(Vector3(1.0f, 1.0f, 1.0f));
	testParticle.SetDamping(0.985f);
	testParticle.SetMass(2.0f);
	testParticle.AddForce(Vector3(1.0f, 2.0f, 3.0f));

	testParticle.SymplecticEulerIntegrate(1.0f);

	
	Calculations:
	Vector3 resultingAcc = acceleration
	= (1.0f, 1.0f, 1.0f)

	resultingAcc += forceAccum * inverseMass
	= (1.0f, 1.0f, 1.0f) + (1.0f, 2.0f, 3.0f) * 0.5f
	= (1.0f, 1.0f, 1.0f) + (0.5f, 1.0f, 1.5f)
	= (1.5f, 2.0f, 2.5f)

	velocity += resultingAcc * delta
	= (6.0f, 20.0f, 2.0f) + (1.5f, 2.0f, 2.5f) * 1.0f
	= (7.5f, 22.0f, 4.5f)
	
	velocity *= pow(damping, delta)
	= (7.5f, 22.0f, 4.5f) * 0.985f
	= (7.3875f, 21.67f, 4.4325f)

	position += velocity * delta
	= (4.0f, 1.0f, 20.0f) + (7.3875f, 21.67f, 4.4325f)
	= (11.3875f, 22.67f, 24.4325f)
	

	Vector3 expectedPosition(11.3875f, 22.67f, 24.4325f);
	Vector3 expectedVelocity(7.3875f, 21.67f, 4.4325f);

	//cout << testParticle.GetPosition().getX() << " : " << expectedPosition.getX() << endl;
	//cout << testParticle.GetPosition().getY() << " : " << expectedPosition.getY() << endl;
	//cout << testParticle.GetPosition().getZ() << " : " << expectedPosition.getZ() << endl;
	Vector3* pos = &(testParticle.GetPosition());
	BOOST_CHECK(abs(pos->getX() - expectedPosition.getX()) < INTEGRATION_PRECISION);
	BOOST_CHECK(abs(pos->getY() - expectedPosition.getY()) < INTEGRATION_PRECISION);
	BOOST_CHECK(abs(pos->getZ() - expectedPosition.getZ()) < INTEGRATION_PRECISION);
	
	//cout << testParticle.GetVelocity().getX() << " : " << expectedVelocity.getX() << endl;
	Vector3* vel = &(testParticle.GetVelocity());
	BOOST_CHECK(abs(vel->getX() - expectedVelocity.getX()) < INTEGRATION_PRECISION);
	BOOST_CHECK(abs(vel->getY() - expectedVelocity.getY()) < INTEGRATION_PRECISION);
	BOOST_CHECK(abs(vel->getZ() - expectedVelocity.getZ()) < INTEGRATION_PRECISION);
}
*/

BOOST_AUTO_TEST_CASE(ClearAccumulatorTest)
{
	Particle testParticle;

	Vector3 expectedResult(0.0f, 0.0f, 0.0f);

	Vector3 force(1.0f, 4.0f, 5.0f);
	testParticle.AddForce(force); //Adds to force accumulator

	testParticle.ClearAccumulator(); //Sets force accumulator to zero vector

	//if ClearAccumulator() worked, forceAccum should equal expectedResult
	BOOST_CHECK(testParticle.GetForceAccum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(AddForceTest)
{
	Particle testParticle;

	Vector3 force(1.0f,2.0f,3.0f);
	testParticle.AddForce(force);

	Vector3 expectedResult = force;

	BOOST_CHECK(testParticle.GetForceAccum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(SetPositionTest1)
{
	Particle testParticle;

	Vector3 expectedResult(20.0f, 100.0f, 55.0f);
	testParticle.SetPosition(expectedResult);

	BOOST_CHECK(testParticle.GetPosition() == expectedResult);
}

BOOST_AUTO_TEST_CASE(SetPositionTest2)
{
	Particle testParticle;

	Vector3 expectedResult(20.0f, 100.0f, 55.0f);
	testParticle.SetPosition(Vector3(20.0f, 100.0f, 55.0f));

	BOOST_CHECK(testParticle.GetPosition() == expectedResult);
}

BOOST_AUTO_TEST_CASE(SetVelocityTest1)
{
	Particle testParticle;

	Vector3 expectedResult(20.0f, 100.0f, 55.0f);
	testParticle.SetVelocity(expectedResult);

	BOOST_CHECK(testParticle.GetVelocity() == expectedResult);
}

BOOST_AUTO_TEST_CASE(SetVelocityTest2)
{
	Particle testParticle;

	Vector3 expectedResult(20.0f, 100.0f, 55.0f);
	testParticle.SetVelocity(Vector3(20.0f, 100.0f, 55.0f));

	BOOST_CHECK(testParticle.GetVelocity() == expectedResult);
}

BOOST_AUTO_TEST_CASE(SetAccelerationTest1)
{
	Particle testParticle;

	Vector3 expectedResult(20.0f, 100.0f, 55.0f);
	testParticle.SetAcceleration(expectedResult);

	BOOST_CHECK(testParticle.GetAcceleration() == expectedResult);
}

BOOST_AUTO_TEST_CASE(SetAccelerationTest2)
{
	Particle testParticle;

	Vector3 expectedResult(20.0f, 100.0f, 55.0f);
	testParticle.SetAcceleration(Vector3(20.0f, 100.0f, 55.0f));

	BOOST_CHECK(testParticle.GetAcceleration() == expectedResult);
}

BOOST_AUTO_TEST_CASE(SetDampingTest)
{
	Particle testParticle;

	FFLOAT expectedResult = 0.98f;
	testParticle.SetDamping(0.98f);

	BOOST_CHECK(fabsf(testParticle.GetDamping() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(SetInverseMassTest)
{
	Particle testParticle;

	FFLOAT expectedResult = 0.5f;
	testParticle.SetInverseMass(0.5f);

	BOOST_CHECK(fabsf(testParticle.GetInverseMass() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(SetMassTest)
{
	Particle testParticle;

	FFLOAT expectedResult = 2.0f;
	testParticle.SetMass(2.0f);

	BOOST_CHECK(fabsf(testParticle.GetMass() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(FiniteMassTest)
{
	Particle testParticle;

	BBOOL expectedResult = true;
	testParticle.SetMass(2.0f);

	BOOST_CHECK(testParticle.HasFiniteMass() == expectedResult);
}