#ifndef PARTICLECONTACTS_H
#include "../Ocelot Editor/Physics/ParticleContacts.h"
#include "../Ocelot Editor/Physics/ParticleContacts.cpp"
#endif

#include <boost/test/unit_test.hpp>

using namespace Ocelot;

BOOST_AUTO_TEST_CASE(ParticleContactSeparatingVelocityTest)
{
	Particle pA;
	Particle pB;

	pA.SetVelocity(Vector3(0.0f, 10.0f, 0.0f));
	pB.SetVelocity(Vector3(0.0f, -10.0f, 0.0f));

	ParticleContact c(&pA, &pB, 1.0f, Vector3(0.0f, 1.0f, 0.0f), 0.0f);
	FFLOAT sepVel = c.SeparatingVelocity();
	FFLOAT expectedResult = 20.0f;

	BOOST_CHECK(fabsf(sepVel - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(ParticleContactClosingVelocityTest)
{
	Particle pA;
	Particle pB;

	pA.SetVelocity(Vector3(0.0f, 10.0f, 0.0f));
	pB.SetVelocity(Vector3(0.0f, -10.0f, 0.0f));

	ParticleContact c(&pA, &pB, 1.0f, Vector3(0.0f, 1.0f, 0.0f), 0.0f);
	FFLOAT closingVel = -c.SeparatingVelocity();
	FFLOAT expectedResult = -20.0f;

	BOOST_CHECK(fabsf(closingVel - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(ParticleContactGetRestitutionTest)
{
	ParticleContact c(0, 0, 1.0f, Vector3(0.0f), 0.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(c.GetRestitution() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(ParticleContactsGetPenetrationTest)
{
	ParticleContact c(0, 0, 1.0f, Vector3(0.0f), 0.0f);

	FFLOAT expectedResult = 0.0f;

	BOOST_CHECK(fabsf(c.GetPenetration() - expectedResult) < FLT_EPSILON);
}