#ifndef AABB_H
#include "../Ocelot Editor/Collisions/AABB.h"
#include "../Ocelot Editor/Collisions/AABB.cpp"
#include "../Ocelot Editor/Utilities/Color.cpp"
#include "../Ocelot Editor/Maths/Ray.cpp"
#include "../Ocelot Editor/Geometry/Line.h"
#include "../Ocelot Editor/Geometry/Line.cpp"
#include "../Ocelot Editor/ResourceManager/ResourceFactory.h"
#include "../Ocelot Editor/ResourceManager/ResourceFactory.cpp"
#include "../Ocelot Editor/Maths/MatrixProjection.cpp"
#include "../Ocelot Editor/Maths/MatrixView.cpp"
#include "../Ocelot Editor/Geometry/ADXBaseMesh.cpp"
#include "../Ocelot Editor/Geometry/OcelotBaseMesh.cpp"
#include "../Ocelot Editor/DirectXAPI/DXUtility.h"
#include "../Ocelot Editor/Camera/Camera.cpp"
#include "../Ocelot Editor/Utilities/Plane.cpp"
#endif

#include "boost\test\unit_test.hpp"

using namespace Ocelot;

BOOST_AUTO_TEST_CASE(MinMax2DIntersectionTest1)
{
	AABBMinMax2D a(0.0f, 0.0f, 13.0f, 10.0f);
	AABBMinMax2D b(9.0f, 6.0f, 20.0f, 14.0f);

	BBOOL expectedResult = true;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax2DIntersectionTest2)
{
	AABBMinMax2D a(0.0f, 0.0f, 13.0f, 10.0f);
	AABBMinMax2D b(15.0f, 7.0f, 23.0f, 14.0f);

	BBOOL expectedResult = false;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax2DGetMinimumTest1)
{
	AABBMinMax2D a;

	Vector2 expectedResult(1.0f, 2.0f);
	a.SetMinimum(expectedResult);

	BOOST_CHECK(a.GetMinimum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax2DGetMinimumTest2)
{
	AABBMinMax2D a;

	Vector2 expectedResult(1.0f, 2.0f);
	a.SetMinimum(Vector2(1.0f, 2.0f));

	BOOST_CHECK(a.GetMinimum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax2DGetMaximumTest1)
{
	AABBMinMax2D a;

	Vector2 expectedResult(1.0f, 2.0f);
	a.SetMaximum(expectedResult);

	BOOST_CHECK(a.GetMaximum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax2DGetMaximumTest2)
{
	AABBMinMax2D a;

	Vector2 expectedResult(1.0f, 2.0f);
	a.SetMaximum(Vector2(1.0f, 2.0f));

	BOOST_CHECK(a.GetMaximum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax2DGetMinXTest)
{
	AABBMinMax2D a;
	a.SetMinimum(Vector2(1.0f, 2.0f));

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetMinX() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinMax2DGetMinYTest)
{
	AABBMinMax2D a;
	a.SetMinimum(Vector2(1.0f, 2.0f));

	FFLOAT expectedResult = 2.0f;

	BOOST_CHECK(fabsf(a.GetMinY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinMax2DGetMaxXTest)
{
	AABBMinMax2D a;
	a.SetMaximum(Vector2(1.0f, 2.0f));

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetMaxX() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinMax2DGetMaxYTest)
{
	AABBMinMax2D a;
	a.SetMaximum(Vector2(1.0f, 2.0f));

	FFLOAT expectedResult = 2.0f;

	BOOST_CHECK(fabsf(a.GetMaxY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinExtent2DIntersectionTest1)
{
	AABBMinExtent2D a(0.0f, 0.0f, 13.0f, 10.0f);
	AABBMinExtent2D b(9.0f, 4.0f, 9.0f, 8.0f);

	BBOOL expectedResult = true;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinExtent2DIntersectionTest2)
{
	AABBMinExtent2D a(0.0f, 0.0f, 13.0f, 10.0f);
	AABBMinExtent2D b(15.0f, 7.0f, 2.0f, 2.0f);

	BBOOL expectedResult = false;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinExtent2DGetMinimumTest1)
{
	AABBMinExtent2D a;

	Vector2 expectedResult(1.0f, 2.0f);
	a.SetMinimum(expectedResult);

	BOOST_CHECK(a.GetMinimum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinExtent2DGetMinimumTest2)
{
	AABBMinExtent2D a;

	Vector2 expectedResult(1.0f, 2.0f);
	a.SetMinimum(Vector2(1.0f, 2.0f));

	BOOST_CHECK(a.GetMinimum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinExtent2DGetMinXTest)
{
	AABBMinExtent2D a;

	FFLOAT expectedResult = 1.0f;
	a.SetMinimum(Vector2(1.0f, 2.0f));

	BOOST_CHECK(fabsf(a.GetMinX() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinExtent2DGetMinYTest)
{
	AABBMinExtent2D a;

	FFLOAT expectedResult = 2.0f;
	a.SetMinimum(Vector2(1.0f, 2.0f));

	BOOST_CHECK(fabsf(a.GetMinY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinExtent2DGetExtentXTest)
{
	AABBMinExtent2D a;
	a.SetExtentX(1.0f);
	
	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetExtentX() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinExtent2DGetExtentYTest)
{
	AABBMinExtent2D a;
	a.SetExtentY(1.0f);
	
	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetExtentY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(CentreRadius2DIntersectionTest1)
{
	AABBCentreRadius2D a(9.0f,6.0f,2.0f,3.0f);
	AABBCentreRadius2D b(12.0f,7.0f,2.0f,2.0f);

	BBOOL expectedResult = true;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(CentreRadius2DIntersectionTest2)
{
	AABBCentreRadius2D a(9.0f,6.0f,2.0f,3.0f);
	AABBCentreRadius2D b(2.0f,2.0f,2.0f,2.0f);

	BBOOL expectedResult = false;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(CentreRadius2DGetCentreTest1)
{
	AABBCentreRadius2D a;

	a.SetCentre(Vector2(1.0f, 2.0f));

	Vector2 expectedResult(1.0f, 2.0f);

	BOOST_CHECK(a.GetCentre() == expectedResult);
}

BOOST_AUTO_TEST_CASE(CentreRadius2DGetCentreTest2)
{
	AABBCentreRadius2D a;

	a.SetCentre(Vector2(1.0f, 2.0f));

	Vector2 expectedResult(1.0f, 2.0f);

	BOOST_CHECK(a.GetCentre() == expectedResult);
}

BOOST_AUTO_TEST_CASE(CentreRadius2DGetCentreXTest)
{
	AABBCentreRadius2D a;

	a.SetCentre(Vector2(1.0f, 2.0f));

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetCentreX() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(CentreRadius2DGetCentreYTest)
{
	AABBCentreRadius2D a;

	a.SetCentre(Vector2(1.0f, 2.0f));

	FFLOAT expectedResult = 2.0f;

	BOOST_CHECK(fabsf(a.GetCentreY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(CentreRadius2DGetRadiusXTest)
{
	AABBCentreRadius2D a;

	a.SetXRadius(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetXRadius() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(CentreRadius2DGetRadiusYTest)
{
	AABBCentreRadius2D a;

	a.SetYRadius(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetYRadius() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinMax3DIntersectionTest1)
{
	AABBMinMax3D a(11.0f,6.0f,0.0f,18.0f,12.0f,10.0f);
	AABBMinMax3D b(16.0f,9.0f,2.0f,21.0f,14.0f,8.0f);

	BBOOL expectedResult = true;
	BBOOL actualResult = a.Intersects(b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax3DIntersectionTest2)
{
	AABBMinMax3D a(11.0f,6.0f,0.0f,18.0f,12.0f, 10.0f);
	
	AABBMinMax3D b(2.0f,3.0f,0.0f,7.0f,8.0f,10.0f);

	BBOOL expectedResult = false;
	BBOOL actualResult = a.Intersects(b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax3DGetMinimumTest1)
{
	AABBMinMax3D a;

	Vector3 expectedResult(1.0f, 1.0f, 2.0f);
	a.SetMinimum(expectedResult);

	BOOST_CHECK(a.GetMinimum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax3DGetMinimumTest2)
{
	AABBMinMax3D a;

	Vector3 expectedResult(1.0f, 1.0f, 2.0f);
	a.SetMinimum(1.0f, 1.0f, 2.0f);

	BOOST_CHECK(a.GetMinimum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax3DGetMaximumTest1)
{
	AABBMinMax3D a;

	Vector3 expectedResult(1.0f, 1.0f, 2.0f);
	a.SetMaximum(expectedResult);

	BOOST_CHECK(a.GetMaximum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax3DGetMaximumTest2)
{
	AABBMinMax3D a;

	Vector3 expectedResult(1.0f, 1.0f, 2.0f);
	a.SetMaximum(1.0f, 1.0f, 2.0f);

	BOOST_CHECK(a.GetMaximum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinMax3DGetMinXTest)
{
	AABBMinMax3D a;

	a.SetMinimum(1.0f, 2.0f, 3.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetMinX() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinMax3DGetMinYTest)
{
	AABBMinMax3D a;

	a.SetMinimum(1.0f, 2.0f, 3.0f);

	FFLOAT expectedResult = 2.0f;

	BOOST_CHECK(fabsf(a.GetMinY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinMax3DGetMinZTest)
{
	AABBMinMax3D a;

	a.SetMinimum(1.0f, 2.0f, 3.0f);

	FFLOAT expectedResult = 3.0f;

	BOOST_CHECK(fabsf(a.GetMinZ() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinMax3DGetMaxXTest)
{
	AABBMinMax3D a;

	a.SetMaximum(1.0f, 2.0f, 3.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetMaxX() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinMax3DGetMaxYTest)
{
	AABBMinMax3D a;

	a.SetMaximum(1.0f, 2.0f, 3.0f);

	FFLOAT expectedResult = 2.0f;

	BOOST_CHECK(fabsf(a.GetMaxY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinMax3DGetMaxZTest)
{
	AABBMinMax3D a;

	a.SetMaximum(1.0f, 2.0f, 3.0f);

	FFLOAT expectedResult = 3.0f;

	BOOST_CHECK(fabsf(a.GetMaxZ() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinExtent3DIntersectionTest1)
{
	AABBMinExtent3D a(9.0f,6.0f,0.0f,6.0f,5.0f,10.0f);
	AABBMinExtent3D b(13.0f,9.0f,2.0f,5.0f,4.0f,6.0f);

	BBOOL expectedResult = true;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinExtent3DIntersectionTest2)
{
	AABBMinExtent3D a(9.0f,6.0f,0.0f,6.0f,5.0f,10.0f);
	AABBMinExtent3D b(2.0f,3.0f,0.0f,5.0f,4.0f,10.0f);

	BBOOL expectedResult = false;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinExtent3DGetMinimumTest1)
{
	AABBMinExtent3D a;

	Vector3 expectedResult(1.0f, 2.0f, 3.0f);
	a.SetMinimum(expectedResult);

	BOOST_CHECK(a.GetMinimum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinExtent3DGetMinimumTest2)
{
	AABBMinExtent3D a;

	Vector3 expectedResult(1.0f, 2.0f, 3.0f);
	a.SetMinimum(Vector3(1.0f, 2.0f, 3.0f));

	BOOST_CHECK(a.GetMinimum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(MinExtent3DGetMinXTest)
{
	AABBMinExtent3D a;
	a.SetMinimum(Vector3(1.0f, 2.0f, 3.0f));

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetMinimum()[0] - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinExtent3DGetMinYTest)
{
	AABBMinExtent3D a;
	a.SetMinimum(Vector3(1.0f, 2.0f, 3.0f));

	FFLOAT expectedResult = 2.0f;

	BOOST_CHECK(fabsf(a.GetMinimum().getY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinExtent3DGetMinZTest)
{
	AABBMinExtent3D a;
	a.SetMinimum(Vector3(1.0f, 2.0f, 3.0f));

	FFLOAT expectedResult = 3.0f;

	BOOST_CHECK(fabsf(a.GetMinimum().getZ() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinExtent3DGetExtentXTest)
{
	AABBMinExtent3D a;

	a.SetExtentX(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetExtentX() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinExtent3DGetExtentYTest)
{
	AABBMinExtent3D a;

	a.SetExtentY(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetExtentY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(MinExtent3DGetExtentZTest)
{
	AABBMinExtent3D a;

	a.SetExtentZ(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetExtentZ() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(CentreRadius3DIntersectionTest1)
{
	AABBCentreRadius3D a(10.0f,7.0f,0.0f,3.0f,2.0f,5.0f);
	AABBCentreRadius3D b(12.0f,8.0f,0.0f,1.0f,2.0f,3.0f);

	BBOOL expectedResult = true;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(CentreRadius3DIntersectionTest2)
{
	AABBCentreRadius3D a(10.0f,7.0f,0.0f,3.0f,2.0f,5.0f);
	AABBCentreRadius3D b(2.0f,4.0f,0.0f,2.0f,2.0f,2.0f);

	BBOOL expectedResult = false;
	BBOOL actualResult = a.Intersects(&b);

	BOOST_CHECK(actualResult == expectedResult);
}

BOOST_AUTO_TEST_CASE(CentreRadius3DGetCentreTest1)
{
	AABBCentreRadius3D a;

	Vector3 expectedResult(1.0f, 2.0f, 3.0f);
	a.SetCentre(expectedResult);

	BOOST_CHECK(a.GetCentre() == expectedResult);
}

BOOST_AUTO_TEST_CASE(CentreRadius3DGetCentreTest2)
{
	AABBCentreRadius3D a;

	Vector3 expectedResult(1.0f, 2.0f, 3.0f);
	a.SetCentre(Vector3(1.0f, 2.0f, 3.0f));

	BOOST_CHECK(a.GetCentre() == expectedResult);
}

BOOST_AUTO_TEST_CASE(CentreRadius3DGetCentreXTest)
{
	AABBCentreRadius3D a;
	a.SetCentre(Vector3(1.0f, 2.0f, 3.0f));

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetCentre().getX() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(CentreRadius3DGetCentreYTest)
{
	AABBCentreRadius3D a;
	a.SetCentre(Vector3(1.0f, 2.0f, 3.0f));

	FFLOAT expectedResult = 2.0f;

	BOOST_CHECK(fabsf(a.GetCentre().getY() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(CentreRadius3DGetCentreZTest)
{
	AABBCentreRadius3D a;
	a.SetCentre(Vector3(1.0f, 2.0f, 3.0f));

	FFLOAT expectedResult = 3.0f;

	BOOST_CHECK(fabsf(a.GetCentre().getZ() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(CentreRadius3DGetXRadiusTest)
{
	AABBCentreRadius3D a;

	a.SetXRadius(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetXRadius() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(CentreRadius3DGetYRadiusTest)
{
	AABBCentreRadius3D a;

	a.SetYRadius(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetYRadius() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(CentreRadius3DGetZRadiusTest)
{
	AABBCentreRadius3D a;

	a.SetZRadius(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(a.GetZRadius() - expectedResult) < FLT_EPSILON);
}