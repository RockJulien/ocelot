
#ifndef DEF_VECTOR2_H
#include "..\Ocelot Editor\Maths\Vector2.h"
#include "..\Ocelot Editor\Maths\Vector2.cpp"
#endif

#include "..\OcelotInput\OcelotInput.h"
#pragma comment(lib, "OcelotInput.lib")

#include <boost/test/unit_test.hpp>

using namespace std;

BOOST_AUTO_TEST_CASE(lengthVec_test)
{
	Vector2 vec(3.0f, 4.0f);

	FFLOAT expected = 5.0f;

	FFLOAT val = vec.Vec2Length();

	cout << val << endl;

	BOOST_CHECK(abs(expected - val) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(lengthSqrtVec_test)
{
	Vector2 vec(3.0f, 4.0f);

	FFLOAT expected = 25.0f;

	FFLOAT val = vec.Vec2LengthSqrt();

	cout << val << endl;

	BOOST_CHECK(abs(expected - val) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(distBetweenVec_test)
{
	Vector2 vec1(0.0f, 4.0f);
	Vector2 vec2(2.0f, 2.0f);

	FFLOAT expectedResult = sqrt(8.0f);

	FFLOAT val = vec1.Vec2DistanceBetween(vec2);

	cout << val << endl;

	BOOST_CHECK(fabsf(val - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(normalizeVec_test)
{
	Vector2 vec(3.0f, 4.0f);

	Vector2 expectedResult(0.6f, 0.8f);

	BOOST_CHECK(vec.Vec2Normalise() == expectedResult);
}

BOOST_AUTO_TEST_CASE(dotProdVec_test)
{
	Vector2 vec1(1.0f, 2.0f);
	Vector2 vec2(4.0f, 1.0f);

	// need to normalize first
	vec1.Vec2Normalise(); // L2.236067977499 X0.4472135955 Y0.8944271910
	vec2.Vec2Normalise(); // L4.123105625617 X0.9701425001 Y0.2425356250

	FFLOAT expectedResult = 0.65079137340325f; // 0.43386091561708 + 0.21693045778617

	FFLOAT val  = vec1.Vec2DotProduct(vec2);

	cout << val << endl;

	BOOST_CHECK(abs(expectedResult - val) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(crossProdVec_test)
{
	Vector2 vec1(1.0f, 2.0f);

	Vector2 expectedResult(-2.0f, 1.0f);

	BOOST_CHECK(vec1.Vec2CrossProduct(vec1) == expectedResult);
}

BOOST_AUTO_TEST_CASE(addVec_test)
{
	
	Vector2 vec1(1.0f, 21.0f);
	Vector2 vec2(2.0f, 1.0f);

	Vector2 expectedResult(3.0f, 22.0f);
	

	BOOST_CHECK(vec1.Vec2Addition(vec2) == expectedResult);
	BOOST_CHECK(vec1 + vec2 == expectedResult);
	BOOST_CHECK((vec1 += vec2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(subVec_test)
{
	
	Vector2 vec1(1.0f, 21.0f);
	Vector2 vec2(2.0f, 1.0f);

	Vector2 expectedResult(-1.0f, 20.0f);
	

	BOOST_CHECK(vec1.Vec2Subtraction(vec2) == expectedResult);
	BOOST_CHECK(vec1 - vec2 == expectedResult);
	BOOST_CHECK((vec1 -= vec2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(divVec_test)
{
	
	Vector2 vec1(1.0f, 21.0f);
	FFLOAT   val = 2.0f;

	Vector2 expectedResult(0.5f, 10.5f);
	
	BOOST_CHECK(vec1 / val == expectedResult);
	BOOST_CHECK((vec1 /= val) == expectedResult);
}

BOOST_AUTO_TEST_CASE(mulVec_test)
{
	
	Vector2 vec1(1.0f, 21.0f);
	FFLOAT   val = 2.0f;

	Vector2 expectedResult(2.0f, 42.0f);
	
	BOOST_CHECK(vec1 * val == expectedResult);
	BOOST_CHECK((vec1 *= val) == expectedResult);
}

BOOST_AUTO_TEST_CASE(equVec_test)
{
	
	Vector2 vec1(1.0f, 21.0f);
	Vector2 vec2(1.0f, 21.0f);
	Vector2 vec3(21.0f, 1.0f);

	BBOOL expectedResult = true;
	BBOOL expectedResult2 = false;
	
	BOOST_CHECK(Equal<Vector2>(vec1, vec2) == expectedResult);
	BOOST_CHECK(Equal<Vector2>(vec1, vec3) == expectedResult2);
	BOOST_CHECK(Equal<Vector2>(vec2, vec3) == expectedResult2);

	BOOST_CHECK((vec1 == vec2) == expectedResult);
	BOOST_CHECK((vec1 == vec3) == expectedResult2);
	BOOST_CHECK((vec2 == vec3) == expectedResult2);
}

BOOST_AUTO_TEST_CASE(diffVec_test)
{
	
	Vector2 vec1(1.0f, 21.0f);
	Vector2 vec2(1.0f, 21.0f);
	Vector2 vec3(21.0f, 1.0f);

	BBOOL expectedResult = true;
	BBOOL expectedResult2 = false;
	
	BOOST_CHECK(Different<Vector2>(vec1, vec3) == expectedResult);
	BOOST_CHECK(Different<Vector2>(vec2, vec3) == expectedResult);
	BOOST_CHECK(Different<Vector2>(vec1, vec2) == expectedResult2);

	BOOST_CHECK((vec1 != vec3) == expectedResult);
	BOOST_CHECK((vec2 != vec3) == expectedResult);
	BOOST_CHECK((vec1 != vec2) == expectedResult2);
}