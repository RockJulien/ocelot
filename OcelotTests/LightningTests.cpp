#ifndef LIGHTNING_H
#include "../Ocelot Editor/Particle System/Lightning.h"
#include "../Ocelot Editor/Particle System/Lightning.cpp"
#endif

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(LightningEyePositionTest)
{
	Lightning l;
	l.SetEyePosition(Vector3(0.0f,1.0f,2.0f));

	Vector3 expectedResult(0.0f, 1.0f, 2.0f);

	BOOST_CHECK(l.GetEyePosition() == expectedResult);
}

BOOST_AUTO_TEST_CASE(LightningGetStartPointTest)
{
	Lightning l;
	l.SetStartPoint(Vector3(0.0f, 1.0f, 2.0f));

	Vector3 expectedResult(0.0f, 1.0f, 2.0f);

	BOOST_CHECK(l.GetStartPoint() == expectedResult);
}

BOOST_AUTO_TEST_CASE(LightningGetEndPointTest)
{
	Lightning l;
	l.SetEndPoint(Vector3(0.0f, 1.0f, 2.0f));

	Vector3 expectedResult(0.0f, 1.0f, 2.0f);

	BOOST_CHECK(l.GetEndPoint() == expectedResult);
}

BOOST_AUTO_TEST_CASE(LightningGetNumIterationsTest)
{
	Lightning l;
	l.SetNumIterations(1);

	BIGINT expectedResult = 1;

	BOOST_CHECK(l.GetNumIterations() == expectedResult);
}

BOOST_AUTO_TEST_CASE(LightningGetColorTest)
{
	Lightning l;
	l.SetColor(Ocelot::Color(0.0f, 1.0f, 1.0f, 1.0f));

	Ocelot::Color expectedResult = OcelotColor::CYAN;

	BOOST_CHECK(l.GetColor() == expectedResult);
}

BOOST_AUTO_TEST_CASE(LightningGetDurationTest)
{
	Lightning l;
	l.SetDuration(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(l.GetDuration() == expectedResult);
}