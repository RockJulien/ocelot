
#ifndef DEF_VECTOR3_H
#include "..\Ocelot Editor\Maths\Vector3.h"
#include "..\Ocelot Editor\Maths\Vector3.cpp"
#include "..\Ocelot Editor\Maths\Vector4.cpp"
#endif

#include <boost/test/unit_test.hpp>
#include <iostream>

using namespace std;

BOOST_AUTO_TEST_CASE(lengthVec3_test)
{
	Vector3 vec(3.0f, 4.0f, 5.0f);

	FFLOAT expected = 7.0710678118654f;

	FFLOAT val = vec.Vec3Length();

	cout << val << endl;

	BOOST_CHECK(abs(expected - val) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(lengthSqrtVec3_test)
{
	Vector3 vec(3.0f, 4.0f, 5.0f);

	FFLOAT expected = 49.99999999f;

	FFLOAT val = vec.Vec3LengthSquared();

	cout << val << endl;

	BOOST_CHECK(abs(expected - val) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(distBetweenVec3_test)
{
	Vector3 vec1(0.0f, 4.0f, 2.0f);
	Vector3 vec2(2.0f, 2.0f, 1.0f);

	FFLOAT expectedResult = sqrt(9.0f);

	FFLOAT val = vec1.Vec3DistanceBetween(vec2);

	cout << val << endl;

	BOOST_CHECK(fabsf(val - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(normalizeVec3_test)
{
	Vector3 vec(3.0f, 4.0f, 5.0f);

	Vector3 expectedResult(0.424264068711f, 0.56568542494923f, 0.707106781186547f);

	BOOST_CHECK(vec.Vec3Normalise() == expectedResult);
}

BOOST_AUTO_TEST_CASE(dotProdVec3_test)
{
	Vector3 vec1(1.0f, 2.0f, 3.0f);
	Vector3 vec2(4.0f, 1.0f, 2.0f);

	// need to normalize first
	vec1.Vec3Normalise(); 
	vec2.Vec3Normalise();

	FFLOAT expectedResult = 0.69985410543f; 

	FFLOAT val  = vec1.Vec3DotProduct(vec2);

	cout << val << endl;

	BOOST_CHECK(abs(expectedResult - val) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(crossProdVec3_test)
{
	Vector3 vec1(1.0f, 2.0f, 3.0f);
	Vector3 vec2(4.0f, 1.0f, 2.0f);

	Vector3 expectedResult(1.0f, 10.0f, -7.0f);

	BOOST_CHECK(vec1.Vec3CrossProduct(vec2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(addVec3_test)
{
	
	Vector3 vec1(1.0f, 21.0f, 5.0f);
	Vector3 vec2(2.0f, 1.0f, 10.0f);

	Vector3 expectedResult(3.0f, 22.0f, 15.0f);
	

	BOOST_CHECK(vec1.Vec3Addition(vec2) == expectedResult);
	BOOST_CHECK(vec1 + vec2 == expectedResult);
	BOOST_CHECK((vec1 += vec2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(subVec3_test)
{
	
	Vector3 vec1(1.0f, 21.0f, 5.0f);
	Vector3 vec2(2.0f, 1.0f, 10.0f);

	Vector3 expectedResult(-1.0f, 20.0f, -5.0f);
	

	BOOST_CHECK(vec1.Vec3Subtraction(vec2) == expectedResult);
	BOOST_CHECK(vec1 - vec2 == expectedResult);
	BOOST_CHECK((vec1 -= vec2) == expectedResult);
}

BOOST_AUTO_TEST_CASE(divVec3_test)
{
	
	Vector3 vec1(1.0f, 21.0f, 5.0f);
	FFLOAT   val = 2.0f;

	Vector3 expectedResult(0.5f, 10.5f, 2.5f);
	
	BOOST_CHECK(vec1 / val == expectedResult);
	BOOST_CHECK((vec1 /= val) == expectedResult);
}

BOOST_AUTO_TEST_CASE(mulVec3_test)
{
	
	Vector3 vec1(1.0f, 21.0f, 5.0f);
	FFLOAT   val = 2.0f;

	Vector3 expectedResult(2.0f, 42.0f, 10.0f);
	
	BOOST_CHECK(vec1 * val == expectedResult);
	BOOST_CHECK((vec1 *= val) == expectedResult);
}

BOOST_AUTO_TEST_CASE(equVec3_test)
{
	
	Vector3 vec1(1.0f, 21.0f, 5.0f);
	Vector3 vec2(1.0f, 21.0f, 5.0f);
	Vector3 vec3(21.0f, 5.0f, 1.0f);

	BBOOL expectedResult = true;
	BBOOL expectedResult2 = false;
	
	BOOST_CHECK(Equal<Vector3>(vec1, vec2) == expectedResult);
	BOOST_CHECK(Equal<Vector3>(vec1, vec3) == expectedResult2);
	BOOST_CHECK(Equal<Vector3>(vec2, vec3) == expectedResult2);

	BOOST_CHECK((vec1 == vec2) == expectedResult);
	BOOST_CHECK((vec1 == vec3) == expectedResult2);
	BOOST_CHECK((vec2 == vec3) == expectedResult2);
}

BOOST_AUTO_TEST_CASE(diffVec3_test)
{
	
	Vector3 vec1(1.0f, 21.0f, 5.0f);
	Vector3 vec2(1.0f, 21.0f, 5.0f);
	Vector3 vec3(21.0f, 5.0f, 1.0f);

	BBOOL expectedResult = true;
	BBOOL expectedResult2 = false;
	
	BOOST_CHECK(Different<Vector3>(vec1, vec3) == expectedResult);
	BOOST_CHECK(Different<Vector3>(vec2, vec3) == expectedResult);
	BOOST_CHECK(Different<Vector3>(vec1, vec2) == expectedResult2);

	BOOST_CHECK((vec1 != vec3) == expectedResult);
	BOOST_CHECK((vec2 != vec3) == expectedResult);
	BOOST_CHECK((vec1 != vec2) == expectedResult2);
}