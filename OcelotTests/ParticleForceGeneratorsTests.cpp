
#ifndef PARTICLEFORCEGENERATORS_H
#include "../Ocelot Editor/Physics/ParticleForceGenerators.h"
#include "../Ocelot Editor/Physics/ParticleForceGenerators.cpp"
#endif

#ifndef PARTICLE_H
#include "../Ocelot Editor/Physics/Particle.h"
#endif
 
#include <boost/test/unit_test.hpp>


BOOST_AUTO_TEST_CASE(GravityGenUpdateForceTest)
{
	IParticleForceRegistry*reg = PForceRegistry;
	reg->Clear();

	ParticleGravityGenerator gravGen(Vector3(0.0f,9.81f,0.0f));
	
	Particle p;
	p.SetMass(1.0f);

	reg->Add(&p, &gravGen);

	//Simulate a simple time step
	reg->UpdateForces(1.0f);
	
	//F = mg
	Vector3 expectedResult(Vector3(0.0f,9.81f,0.0f) * p.GetMass()); // = (0.0f, 9.81f, 0.0f) since mass is 1.0f

	BOOST_CHECK(p.GetForceAccum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(GravityGenGetGravityTest1)
{
	ParticleGravityGenerator gravGen;
	gravGen.SetGravity(Vector3(0.0f, 9.81f, 0.0f));

	Vector3 expectedResult(0.0f, 9.81f, 0.0f);

	BOOST_CHECK(gravGen.GetGravity() == expectedResult);
}

BOOST_AUTO_TEST_CASE(GravityGenGetGravityTest2)
{
	ParticleGravityGenerator gravGen;
	gravGen.SetGravity(0.0f, 9.81f, 0.0f);

	Vector3 expectedResult(0.0f, 9.81f, 0.0f);

	BOOST_CHECK(gravGen.GetGravity() == expectedResult);
}

BOOST_AUTO_TEST_CASE(DragUpdateForceTest)
{
	ParticleDragGenerator pDrag(1.0f, 1.0f);

	Particle p;
	p.SetVelocity(Vector3(1.0f, 2.0f, 2.0f));

	IParticleForceRegistry *reg = PForceRegistry;
	reg->Clear();
	reg->Add(&p, &pDrag);

	reg->UpdateForces(1.0f);

	/*Calculations
	vel = (1.0f, 2.0f, 2.0f)
	=> speed = 3 m/s

	dragCoeff = 1.0f * 3.0f + 1.0f * 3.0f * 3.0f
	= 3 + 9 = 12

	(1.0f, 2.0f, 2.0f) norm'd = (1/3, 2/3, 2/3)
	
	drag force = (1/3, 2/3, 2/3) * -12
	= (-4, -8, -8)
	*/
	Vector3 expectedResult(-4.0f, -8.0f, -8.0f);

	BOOST_CHECK(p.GetForceAccum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(DragGetSpeedTest)
{
	ParticleDragGenerator pDrag(1.0f, 1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(pDrag.GetSpeedCoefficient() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(DragGetSpeedSquareTest)
{
	ParticleDragGenerator pDrag(1.0f, 1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(pDrag.GetSpeedSquareCoefficient() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(SpringUpdateForceTest)
{
	Particle a;
	Particle b;

	a.SetPosition(Vector3(3.0f, 2.0f, 3.0f));
	b.SetPosition(Vector3(2.0f, 0.0f, 1.0f));

	ParticleSpringGenerator pSpring(&b, 1.0f, 1.0f);

	IParticleForceRegistry* reg = PForceRegistry;
	reg->Clear();
	reg->Add(&a, &pSpring);

	reg->UpdateForces(1.0f);

	/* Calculations
	(3, 2, 3) - (2, 0, 1)
	= (1, 2, 2)

	spring magnitude = |3 - 1| * k = 2

	(1, 2, 2) norm'd = (1/3, 2/3, 2/3)
	spring force = (1/3, 2/3, 2/3) * 2
	= (2/3, 4/3, 4/3)
	*/

	Vector3 expectedResult(2.0f/3.0f, 4.0f/3.0f, 4.0f/3.0f);

	BOOST_CHECK(a.GetForceAccum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(SpringGetOtherTest)
{
	Particle a;

	ParticleSpringGenerator pSpring(&a, 1.0f, 1.0f);

	Particle* expectedResult = &a;

	BOOST_CHECK(pSpring.GetOther() == expectedResult);
}

BOOST_AUTO_TEST_CASE(SpringGetSpringConstantTest)
{
	Particle a;

	ParticleSpringGenerator pSpring(&a, 1.0f, 1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(pSpring.GetSpringConstant() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(SpringGetRestLengthTest)
{
	Particle a;

	ParticleSpringGenerator pSpring(&a, 1.0f, 1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(pSpring.GetRestLength() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(AnchoredSpringUpdateForceTest)
{
	ParticleAnchoredSpringGenerator pSpring(Vector3(1.0f,1.0f,1.0f), 1.0f, 1.0f);
	
	Particle p;
	p.SetPosition(Vector3(2.0f, 3.0f, 3.0f));

	IParticleForceRegistry* reg = PForceRegistry;
	reg->Clear();
	reg->Add(&p, &pSpring);

	reg->UpdateForces(1.0f);
	
	/* Calculations
	(2,3,3) - (1,1,1) = (1,2,2)

	spring anchor force magnitude = (1 - 3) * 1
	= - 2

	(1,2,2) norm'd = (1/3,2/3,2/3)
	anchor force = (1/3,2/3,2/3) * -2
	= (-2/3, -4/3, -4/3)
	*/

	Vector3 expectedResult(-2.0f/3.0f, -4.0f/3.0f, -4.0f/3.0f);

	BOOST_CHECK(p.GetForceAccum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(AnchoredSpringGetAnchorTest)
{
	ParticleAnchoredSpringGenerator paSpring(Vector3(1.0f, 1.0f, 1.0f), 1.0f, 1.0f);

	Vector3 expectedResult(1.0f, 1.0f, 1.0f);

	BOOST_CHECK(paSpring.GetAnchor() == expectedResult);
}

BOOST_AUTO_TEST_CASE(AnchoredSpringGetSpringConstantTest)
{
	ParticleAnchoredSpringGenerator paSpring;
	paSpring.SetSpringConstant(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(paSpring.GetSpringConstant() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(AnchoredSpringGetRestLengthTest)
{
	ParticleAnchoredSpringGenerator paSpring;
	paSpring.SetRestLength(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(paSpring.GetRestLength() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BungeeUpdateForceTest)
{
	Particle a;
	Particle b;

	a.SetPosition(Vector3(1.0f, 2.0f, 3.0f));
	b.SetPosition(Vector3(0.0f, 0.0f, 1.0f));

	ParticleBungeeGenerator bungee(&b, 1.0f, 1.0f);

	IParticleForceRegistry* reg = PForceRegistry;
	reg->Clear();
	reg->Add(&a, &bungee);

	reg->UpdateForces(1.0f);

	/*
	(1,2,3) - (0,0,1) = (1,2,2)

	(1,2,2) mag = 3
	3 > rest length so we have a force

	force magnitude = 1 * (1 - 3)
	= -2

	(1,2,2) norm'd = (1/3,2/3,2/3)
	bungee force (1/3,2/3,2/3) * -2
	= (-2/3, -4/3, -4/3)
	*/

	Vector3 expectedResult(-2.0f/3.0f, -4.0f/3.0f, -4.0f/3.0f);

	BOOST_CHECK(a.GetForceAccum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(BungeeGetOtherTest)
{
	Particle a;

	ParticleBungeeGenerator bungee;
	bungee.SetOther(&a);

	Particle* expectedResult = &a;

	BOOST_CHECK(bungee.GetOther() == expectedResult);
}

BOOST_AUTO_TEST_CASE(BungeeGetSpringConstantTest)
{
	ParticleBungeeGenerator bungee;
	bungee.SetSpringConstant(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(bungee.GetSpringConstant() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BungeeGetRestLengthTest)
{
	ParticleBungeeGenerator bungee;
	bungee.SetRestLength(1.0f);

	FFLOAT expectedResult = 1.0f;

	BOOST_CHECK(fabsf(bungee.GetRestLength() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BuoyancyUpdateForceTest1)
{
	ParticleBuoyancyGenerator b(10.0f, 100.0f, 20.0f, 1000.0f);

	Particle a;
	a.SetPosition(Vector3(4.0f, 7.0f, 3.0f));

	IParticleForceRegistry* reg = PForceRegistry;
	reg->Clear();
	reg->Add(&a, &b);

	reg->UpdateForces(1.0f);

	/* Calculations
	depth = 7

	depth is less than max height and water depth so there
	is a force

	at current depth, particle is fully submerged.
	=> full force instead of partial force.
	force = (0,1000*100,0)
	= (0, 100000, 0)
	*/

	Vector3 expectedResult(0.0f, 100000.0f, 0.0f);

	BOOST_CHECK(a.GetForceAccum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(BuoyancyUpdateForceTest2)
{
	ParticleBuoyancyGenerator b(10.0f, 100.0f, 15.0f, 1000.0f);

	Particle a;
	a.SetPosition(Vector3(4.0f, 7.0f, 3.0f));

	IParticleForceRegistry* reg = PForceRegistry;
	reg->Clear();
	reg->Add(&a, &b);

	reg->UpdateForces(1.0f);

	/* Calculations
	depth = 7

	depth is less than max height and water depth so there
	is a force

	at current depth, particle is partially submerged.
	
	force = (0, 1000 * 100 * (7 - 10 - 15)/2 * 10, 0)
	= 100000 * (-18/2) * 10
	= (0, -9000000, 0)
	*/

	Vector3 expectedResult(0.0f, -9000000.0f, 0.0f);

	BOOST_CHECK(a.GetForceAccum() == expectedResult);
}

BOOST_AUTO_TEST_CASE(BouyancyGetMaxDepthTest)
{
	ParticleBuoyancyGenerator b;
	b.SetMaxDepth(10.0f);

	FFLOAT expectedResult = 10.0f;

	BOOST_CHECK(fabsf(b.GetMaxDepth() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BuoyancyGetVolumeTest)
{
	ParticleBuoyancyGenerator b;
	b.SetVolume(100.0f);

	FFLOAT expectedResult = 100.0f;

	BOOST_CHECK(fabsf(b.GetVolume() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BuoyancyGetWaterHeightTest)
{
	ParticleBuoyancyGenerator b;
	b.SetWaterHeight(10.0f);

	FFLOAT expectedResult = 10.0f;

	BOOST_CHECK(fabsf(b.GetWaterHeight() - expectedResult) < FLT_EPSILON);
}

BOOST_AUTO_TEST_CASE(BuoyancyGetLiquidDensityTest)
{
	ParticleBuoyancyGenerator b;
	b.SetLiquidDensity(1000.0f);

	FFLOAT expectedResult = 1000.0f;

	BOOST_CHECK(fabsf(b.GetLiquidDensity() - expectedResult) < FLT_EPSILON);
}
