#ifndef DEF_OCELOTDIRECTAUDIO_H
#define DEF_OCELOTDIRECTAUDIO_H

#include "OcelotAudioUtil.h"
#include "..\OcelotAudio\IOcelotAudioDevice.h"
#include "..\Ocelot Editor\Maths\Vector3.cpp"
#include "..\Ocelot Editor\Maths\Vector4.cpp"
#include "WaveFile.h"

#define INPUTCHANNELS 1  //nb of sources
#define OUTPUTCHANNELS 8 //nb max of destination channels supported

namespace Audio
{
			struct GameAudioState
			{
				// XAudio2
				IXAudio2*               pAudio;
				IXAudio2MasteringVoice* pMasterVoice;
				IXAudio2SourceVoice*    pSource;
				IXAudio2SubmixVoice*    pSubMix;
				IUnknown*               pReverbEffect;
				BYTE*                   pData;
				
				// 3D info
				X3DAUDIO_HANDLE         instance;
				INT                     iNbFrameToApplyAudio;
				DWORD                   iChannelMask;
				UINT                    iNbChannels;
				X3DAUDIO_DSP_SETTINGS   dspSettings;
				X3DAUDIO_LISTENER       listener;
				X3DAUDIO_EMITTER        emitter;
				X3DAUDIO_CONE           emitCone;
				Vector3                 vListenerPos;
				Vector3                 vEmitterPos;
				FFLOAT                   fListenerAngle;
				BBOOL                    bListenerCone;
				BBOOL                    bInnerRadius;
				BBOOL                    bRedirectLFE;
				BBOOL                    bInitialized;
				FFLOAT                   fEmitterAzimuths[INPUTCHANNELS];
				FFLOAT                   fMatCoefficients[INPUTCHANNELS * OUTPUTCHANNELS];
			};

	class OcelotDirectAudio : public IOcelotAudioDevice
	{
	private:

			// Attributes
			GameAudioState m_audioState;
			BBOOL           m_b3DSound;
			const CCHAR*    m_cMusicName;

			// private methods
			VVOID log(CCHAR*, ...);

	public:

			// Constructor & Destructor
			OcelotDirectAudio(HINSTANCE hDllMod);
			~OcelotDirectAudio();

			// Methods
			HRESULT initialize(BBOOL);
			HRESULT startSound();
			HRESULT loadSound(const CCHAR*, BBOOL bLoop = false);
			HRESULT playSound(UINT, FFLOAT delta);
			VVOID    stopSound();
			HRESULT setReverb(ReverbEffect);
			HRESULT trigger();
			VVOID    setListener(Vector3 pos, Vector3 dir, Vector3 up, Vector3 v);
			VVOID    setSoundPosition(Vector3, UINT);
			VVOID    setSoundDirection(Vector3, Vector3 v, UINT);
			VVOID    setSoundMaxDistance(const FFLOAT& dist, UINT s);
			VVOID    release();

			// Accessors
	inline  GameAudioState getAudioState() const { return m_audioState;}
	};
}

#endif