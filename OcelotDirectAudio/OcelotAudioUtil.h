#ifndef DEF_OCELOTAUDIOUTIL_H
#define DEF_OCELOTAUDIOUTIL_H

#include <XAudio2.h>     // audio header for win32 & XBox360
#include <XAudio2fx.h>
#include <X3DAudio.h>
#include <stdio.h>
#include "..\OcelotAudio\AudioUtilities.h"

// definition of some useful macro
#define DeleteArray(X) {if(X){delete[] X; X = NULL;}}
#define DeletePointer(X) {if(X){delete X; X = NULL;}}
#define ReleaseCOM(X) {if(X){ X->Release(); X = NULL;}}

#define NB_PRESETS 30

// speed of the sound in meter per second
const FFLOAT SpeedOfSound = X3DAUDIO_SPEED_OF_SOUND;
const size_t vecOffset = sizeof(FFLOAT) * 3;

// for decreasing/increasing sound regarding to the position of the emitter regarding to the listener.
static const X3DAUDIO_CONE listenerDirectionalCone = {X3DAUDIO_PI * 5.0f / 6.0f, X3DAUDIO_PI * 11.0f / 6.0f, 1.0f, 0.75f, 0.0f, 0.25f, 0.708f, 1.0f};

// LFE level distance curve
static const X3DAUDIO_DISTANCE_CURVE_POINT emitterLFECurvePoints[3] = {0.0f, 1.0f, 0.25f, 0.0f, 1.0f, 0.0f};
static const X3DAUDIO_DISTANCE_CURVE       emitterLFECurve          = {(X3DAUDIO_DISTANCE_CURVE_POINT*)&emitterLFECurvePoints[0], 3};

// Reverb send level distance curve
static const X3DAUDIO_DISTANCE_CURVE_POINT emitterReverbCurvePoints[3] = {0.0f, 0.5f, 0.75f, 1.0f, 1.0f, 0.0f};
static const X3DAUDIO_DISTANCE_CURVE       emitterReverbCurve          = {(X3DAUDIO_DISTANCE_CURVE_POINT*)&emitterReverbCurvePoints[0], 3};

XAUDIO2FX_REVERB_I3DL2_PARAMETERS presetParams[NB_PRESETS] =
{
	XAUDIO2FX_I3DL2_PRESET_FOREST,
    XAUDIO2FX_I3DL2_PRESET_DEFAULT,
    XAUDIO2FX_I3DL2_PRESET_GENERIC,
    XAUDIO2FX_I3DL2_PRESET_PADDEDCELL,
    XAUDIO2FX_I3DL2_PRESET_ROOM,
    XAUDIO2FX_I3DL2_PRESET_BATHROOM,
    XAUDIO2FX_I3DL2_PRESET_LIVINGROOM,
    XAUDIO2FX_I3DL2_PRESET_STONEROOM,
    XAUDIO2FX_I3DL2_PRESET_AUDITORIUM,
    XAUDIO2FX_I3DL2_PRESET_CONCERTHALL,
    XAUDIO2FX_I3DL2_PRESET_CAVE,
    XAUDIO2FX_I3DL2_PRESET_ARENA,
    XAUDIO2FX_I3DL2_PRESET_HANGAR,
    XAUDIO2FX_I3DL2_PRESET_CARPETEDHALLWAY,
    XAUDIO2FX_I3DL2_PRESET_HALLWAY,
    XAUDIO2FX_I3DL2_PRESET_STONECORRIDOR,
    XAUDIO2FX_I3DL2_PRESET_ALLEY,
    XAUDIO2FX_I3DL2_PRESET_CITY,
    XAUDIO2FX_I3DL2_PRESET_MOUNTAINS,
    XAUDIO2FX_I3DL2_PRESET_QUARRY,
    XAUDIO2FX_I3DL2_PRESET_PLAIN,
    XAUDIO2FX_I3DL2_PRESET_PARKINGLOT,
    XAUDIO2FX_I3DL2_PRESET_SEWERPIPE,
    XAUDIO2FX_I3DL2_PRESET_UNDERWATER,
    XAUDIO2FX_I3DL2_PRESET_SMALLROOM,
    XAUDIO2FX_I3DL2_PRESET_MEDIUMROOM,
    XAUDIO2FX_I3DL2_PRESET_LARGEROOM,
    XAUDIO2FX_I3DL2_PRESET_MEDIUMHALL,
    XAUDIO2FX_I3DL2_PRESET_LARGEHALL,
    XAUDIO2FX_I3DL2_PRESET_PLATE,
};

HRESULT FindMediaFileCch(CCHAR* destPath, INT cchDest, CCHAR* fileName)
{
	BBOOL found = false;

	if((fileName == NULL) || (fileName[0] == 0) || (destPath == NULL) || (cchDest < 10))
		return E_INVALIDARG;

	// get the exe name, and the exe path.
	CCHAR exePath[MAX_PATH] = {0};
	CCHAR exeName[MAX_PATH] = {0};
	CCHAR* lastSlash = NULL;
	GetModuleFileNameA(NULL, exePath, MAX_PATH);
	exePath[MAX_PATH - 1] = 0;
	lastSlash = strrchr(exePath, TEXT('\\'));

	if(lastSlash)
	{
		strcpy_s(exeName, MAX_PATH, &lastSlash[1]);

		// chop the exe name from the exe path.
		(*lastSlash) = 0;

		// chop the .exe from the exe name.
		lastSlash = strrchr(exeName, TEXT('.'));
		if(lastSlash)
			(*lastSlash) = 0;
	}

	strcpy_s(destPath, cchDest, fileName);
	if(GetFileAttributesA(destPath) != 0xFFFFFFFF)
		return S_OK;

	// search all parent directories starting at .\
	// and using the fileName as leaf name.
	CCHAR leafName[MAX_PATH] = {0};
	strcpy_s(leafName, MAX_PATH, fileName);

	CCHAR fullPath[MAX_PATH]     = {0};
	CCHAR fullFileName[MAX_PATH] = {0};
	CCHAR search[MAX_PATH]       = {0};
	CCHAR* filePart				= NULL;

	GetFullPathNameA(".", MAX_PATH, fullPath, &filePart);
	if(!filePart)
		return E_FAIL;

	while((filePart != NULL) && ((*filePart) != '\0'))
	{
		sprintf_s(fullFileName, MAX_PATH, "%s\\%s", fullPath, leafName);
		if(GetFileAttributesA(fullFileName) != 0xFFFFFFFF)
		{
			strcpy_s(destPath, cchDest, fullFileName);
			found = true;
			break;
		}

		sprintf_s(fullFileName, MAX_PATH, "%s\\%s\\%s", fullPath, exeName, leafName);
		if(GetFileAttributesA(fullFileName) != 0xFFFFFFFF)
		{
			strcpy_s(destPath, cchDest, fullFileName);
			found = true;
			break;
		}

		sprintf_s(search, MAX_PATH, "%s\\..", fullPath);
		GetFullPathNameA(search, MAX_PATH, fullPath, &filePart);
	}

	if(found)
		return S_OK;

	// if error, return the file as the path with an error code as well.
	strcpy_s(destPath, cchDest, fileName);
	return HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND);
}

#endif