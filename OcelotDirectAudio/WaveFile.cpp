#include "WaveFile.h"

using namespace Audio;

WaveFile::WaveFile()
{
	m_pWaveFormat  = NULL;
	m_hmmio        = NULL;
	m_pBuffer      = NULL;
	m_pLog         = NULL;
	m_iWaveSize    = 0;
	m_bReadFromMem = false;

	// debug log file initialization
	m_pLog = fopen("OcelotWaveFileLog.txt", "w");
	log("SUCCEESS. Wait for an opening !!!");
}

WaveFile::~WaveFile()
{
	close();

	if(!m_bReadFromMem)
		if(m_pWaveFormat)
		{
			delete[] m_pWaveFormat;
			m_pWaveFormat = NULL;
		}

	fclose(m_pLog);
}

VVOID    WaveFile::log(CCHAR* str, ...)
{
	CCHAR  str2[256];
	CCHAR* args;

	args = (CCHAR*)&str + sizeof(str);
	vsprintf(str2, str, args);
	fprintf(m_pLog, "Starting OcelotWAVEFILE Debug Log: ");
	fprintf(m_pLog, str2);
	fprintf(m_pLog, "\n");
	fflush(m_pLog);
}

HRESULT WaveFile::readMMIO()
{
	MMCKINFO      ckInf; // chunk info.
	PCMWAVEFORMAT wF;    // structure for loading into

	log("Start Read MMIO !!!");
	memset(&ckInf, 0, sizeof(ckInf));

	m_pWaveFormat = NULL;

	if(mmioDescend(m_hmmio, &m_ckRiff, NULL, 0) != 0)
	{
		log("ERROR: MmioDescend function FAILED !!!");
		return E_FAIL;
	}

	// verify if it is a valid WAVE file.
	if((m_ckRiff.ckid != FOURCC_RIFF) || (m_ckRiff.fccType != mmioFOURCC('W', 'A', 'V', 'E')))
	{
		log("ERROR: MmioFOURCC function FAILED !!!");
		return E_FAIL;
	}

	// search the input file for the "fmt" chunk.
	ckInf.ckid = mmioFOURCC('f', 'm', 't', ' ');
	if(mmioDescend(m_hmmio, &ckInf, &m_ckRiff, MMIO_FINDCHUNK) != 0)
	{
		log("ERROR: MmioDescend for the format Chunk FAILED !!!");
		return E_FAIL;
	}

	// Because teh format should be as large as <PCMWAVEFORMAT>,
	// so if extra parameters, ignore them.
	if(ckInf.cksize < (LINT)sizeof(PCMWAVEFORMAT))
	{
		log("ERROR: Size of the Wave format invalid !!!");
		return E_FAIL;
	}

	// Read teh format chunk into <wF>
	if(mmioRead(m_hmmio, (HPSTR)&wF, sizeof(wF)) != sizeof(wF))
	{
		log("ERROR: MmioRead function FAILED !!!");
		return E_FAIL;
	}

	if(wF.wf.wFormatTag == WAVE_FORMAT_PCM)
	{
		m_pWaveFormat = (WAVEFORMATEX*)new CCHAR[sizeof(WAVEFORMATEX)];
		if(!m_pWaveFormat)
		{
			log("Initialization of WaveFormat FAILED !!!");
			return E_FAIL;
		}

		memcpy(m_pWaveFormat, &wF, sizeof(wF));
		m_pWaveFormat->cbSize = 0;
	}
	else
	{
		// Read in length of extra bytes.
		USMALLINT extraBytes = 0;
		if(mmioRead(m_hmmio, (CCHAR*)&extraBytes, sizeof(USMALLINT)) != sizeof(USMALLINT))
		{
			log("ERROR: MmioRead function FAILED !!!");
			return E_FAIL;
		}

		m_pWaveFormat = (WAVEFORMATEX*)new CCHAR[sizeof(WAVEFORMATEX) + extraBytes];
		if(!m_pWaveFormat)
		{
			log("ERROR: New operator/stack memory allocation FAILED !!!");
			return E_FAIL;
		}

		// copy the bytes from the pcm structure to the waveFormat structure.
		memcpy(m_pWaveFormat, &wF, sizeof(wF));
		if(m_pWaveFormat)
			m_pWaveFormat->cbSize = extraBytes;

		// Now, read those extra bytes into the structure and if extraAlloc != 0.
		if(mmioRead(m_hmmio, (CCHAR*)(((UCCHAR*)&(m_pWaveFormat->cbSize)) + sizeof(USMALLINT)), extraBytes) != extraBytes)
		{
			if(m_pWaveFormat)
			{
				delete m_pWaveFormat;
				m_pWaveFormat = NULL;
			}
			log("ERROR: MmioRead function FAILED !!!");
			return E_FAIL;
		}
	}

	// Ascend the input file out of the format chunk.
	if(mmioAscend(m_hmmio, &ckInf, 0) != 0)
	{
		if(m_pWaveFormat)
		{
			delete m_pWaveFormat;
			m_pWaveFormat = NULL;
		}
		log("ERROR: MmioAscend function FAILED !!!");
		return E_FAIL;
	}

	log("Read MMIO SUCCEEDED !!!");
	return S_OK;
}

HRESULT WaveFile::writeMMIO(WAVEFORMATEX* pWaveFormatDest)
{
	ULINT factChunk;  // contains the actual fact chunk.
	MMCKINFO      ckOut1;

	memset(&ckOut1, 0, sizeof(ckOut1));

	factChunk = (ULINT)-1;

	// create the output file RIFF chunk of form type WAVE.
	m_ckRiff.fccType = mmioFOURCC('W', 'A', 'V', 'E');
	m_ckRiff.cksize  = 0;

	if(mmioCreateChunk(m_hmmio, &m_ckRiff, MMIO_CREATERIFF) != 0)
	{
		log("ERROR: MmioCreateChunk function for the RIFF FAILED !!!");
		return E_FAIL;
	}

	// now create the format chunk.
	m_ck.ckid   = mmioFOURCC('f', 'm', 't', ' ');
	m_ck.cksize = sizeof(PCMWAVEFORMAT);

	if(mmioCreateChunk(m_hmmio, &m_ck, 0) != 0)
	{
		log("ERROR: MmioCreateChunk function for the FMT FAILED");
		return E_FAIL;
	}

	// write the PCMWAVEFORMAT structure to the FMT chunk if its that type.
	if(pWaveFormatDest->wFormatTag == WAVE_FORMAT_PCM)
	{
		if((UINT)mmioWrite(m_hmmio, (HPSTR)pWaveFormatDest, sizeof(PCMWAVEFORMAT)) != sizeof(PCMWAVEFORMAT))
		{
			log("ERROR: MmioWrite function for writting format FAILED !!!");
			return E_FAIL;
		}
	}
	else
	{
		// write the variable length size.
		if((UINT)mmioWrite(m_hmmio, (HPSTR)pWaveFormatDest, sizeof(*pWaveFormatDest)) != sizeof(PCMWAVEFORMAT))
		{
			log("ERROR: MmioWrite function for writting the size FAILED !!!");
			return E_FAIL;
		}
	}

	// Ascend out of the format chunk back into the RIFF chunk.
	if(mmioAscend(m_hmmio, &m_ck, 0) != 0)
	{
		log("ERROR: MmioAscend function FAILED !!!");
		return E_FAIL;
	}

	// then, create the fact chunk
	ckOut1.ckid   = mmioFOURCC('f', 'a', 'c', 't');
	ckOut1.cksize = 0;

	if(mmioCreateChunk(m_hmmio, &ckOut1, 0) != 0)
	{
		log("ERROR: MmioCreateChunk function FAILED !!!");
		return E_FAIL;
	}

	if(mmioWrite(m_hmmio, (HPSTR)&factChunk, sizeof(factChunk)) != sizeof(factChunk))
	{
		log("ERROR: MmioWrite function FAILED !!!");
		return E_FAIL;
	}

	// for ending up by ascending out the fact chunk.
	if(mmioAscend(m_hmmio, &ckOut1, 0) != 0)
	{
		log("ERROR: MmioAscend function FAILED !!!");
		return E_FAIL;
	}

	log("Write MMIO SUCCESSFULLY !!!");
	return S_OK;
}

HRESULT WaveFile::open(CCHAR* strFileName, WAVEFORMATEX* fT, ULINT flags)
{
	// reset some useful variables
	m_iFlags       = flags;
	m_bReadFromMem = false;

	log("Start Open File From external Source !!!");

	if(m_iFlags == READ)
	{
		if(!strFileName)
			return E_INVALIDARG;

		// release the format array
		if(m_pWaveFormat)
		{
			delete[] m_pWaveFormat;
			m_pWaveFormat = NULL;
		}

		m_hmmio = mmioOpenA(strFileName, NULL, MMIO_ALLOCBUF | MMIO_READ);

		if(!m_hmmio)
		{
			HRSRC         hResInfo;
			HGLOBAL       hResData;
			ULINT iSize;
			UNKNOWN         pRes;

			// if NULL, load it as a resource because FAILED
			if(!(hResInfo = FindResourceA(NULL, strFileName, "WAVE")))
				if(!(hResInfo = FindResourceA(NULL, strFileName, "WAV")))
				{
					log("ERROR: Find Resource FAILED !!!");
					return E_FAIL;
				}

			if(!(hResData = LoadResource(GetModuleHandle(NULL), hResInfo)))
			{
				log("ERROR: Load Resource FAILED !!!");
				return E_FAIL;
			}

			if((iSize = SizeofResource(GetModuleHandle(NULL), hResInfo)) == 0)
			{
				log("ERROR: Get Size of the Resource FAILED !!!");
				return E_FAIL;
			}

			if(!(pRes = LockResource(hResData)))
			{
				log("ERROR: Lock the Resource FAILED !!!");
				return E_FAIL;
			}

			m_pBuffer = new CCHAR[iSize];
			if(!m_pBuffer)
			{
				log("OUT OF MEMORY: New operator FAILED !!!");
				return E_OUTOFMEMORY;
			}

			memcpy(m_pBuffer, pRes, iSize);

			MMIOINFO mmioInfo;
			ZeroMemory(&mmioInfo, sizeof(mmioInfo));
			mmioInfo.fccIOProc = FOURCC_MEM;
			mmioInfo.cchBuffer = iSize;
			mmioInfo.pchBuffer = (CCHAR*)m_pBuffer;

			m_hmmio = mmioOpen(NULL, &mmioInfo, MMIO_ALLOCBUF | MMIO_READ);
		}

		if(FAILED(readMMIO()))
		{
			// will fail if it is not a WAVE file.
			mmioClose(m_hmmio, 0);
			log("ERROR: Read MMIO FAILED !!!");
			return E_FAIL;
		}

		if(FAILED(resetFile()))
		{
			log("ERROR: Reset of the File FAILED !!!");
			return E_FAIL;
		}

		// after the reset, store the new size of teh wave file.
		m_iWaveSize = m_ck.cksize;
	}
	else
	{
		m_hmmio = mmioOpenA(strFileName, NULL, MMIO_ALLOCBUF | MMIO_READWRITE | MMIO_CREATE);

		if(!m_hmmio)
		{
			log("ERROR: MmioOpen function FAILED !!!");
			return E_FAIL;
		}

		if(FAILED(writeMMIO(fT)))
		{
			mmioClose(m_hmmio, 0);
			log("ERROR: Write MMIO function FAILED !!!");
			return E_FAIL;
		}

		if(FAILED(resetFile()))
		{
			log("ERROR: Reset of the File FAILED !!!");
			return E_FAIL;
		}
	}

	log("Open File From Path SUCCEEDED !!!");
	return S_OK;
}

HRESULT WaveFile::openFromMemory(UCCHAR* data, ULINT dataSize, WAVEFORMATEX* fT, ULINT flags)
{

	log("Start Openning From Memory !!!");

	m_pWaveFormat  = fT;
	m_iDataSize    = dataSize;
	m_pData        = data;
	m_pCurrentData = m_pData;
	m_bReadFromMem = true;

	if(flags != READ)
	{
		log("WARNING: Not In Read Mode !!!");
		return E_NOTIMPL;
	}

	log("File From Memory Openned !!!");
	return S_OK;
}

HRESULT WaveFile::readFile(UCCHAR* buffer, ULINT sizeToRead, ULINT* sizeRead)
{
	log("Reading file !!!");

	if(m_bReadFromMem)
	{
		if(!m_pCurrentData)
		{
			log("ERROR: m_pCurrentData not initialized !!!");
			return CO_E_NOTINITIALIZED;
		}

		if(sizeRead)
			(*sizeRead) = 0;

		if((CCHAR*)(m_pCurrentData + sizeToRead) > (CCHAR*)(m_pData + m_iDataSize))
			sizeToRead = m_iDataSize - (ULINT)(m_pCurrentData - m_pData);

		CopyMemory(buffer, m_pCurrentData, sizeToRead);

		if(sizeRead)
			(*sizeRead) = sizeToRead;

		log("Read File SUCCEEDED !!!");
		return S_OK;
	}
	else
	{
		// current status of m_hmmio
		MMIOINFO mmioInfoIn;

		if(!m_hmmio)
		{
			log("ERROR: m_hmmio not initialized !!!");
			return CO_E_NOTINITIALIZED;
		}

		if((buffer == NULL) || (sizeRead == NULL))
		{
			log("ERROR: Invalid Argument/buffer or dataRead NULL !!!");
			return E_INVALIDARG;
		}

		(*sizeRead) = 0;

		if(mmioGetInfo(m_hmmio, &mmioInfoIn, 0) != 0)
		{
			log("ERROR: MmioGetInfo function FAILED !!!");
			return E_FAIL;
		}

		UINT dataIn = sizeToRead;
		if(dataIn > m_ck.cksize)
			dataIn = m_ck.cksize;

		m_ck.cksize -= dataIn;

		for(ULINT cT = 0; cT < dataIn; cT++)
		{
			// copy bytes from the I/O to the Buffer.
			if(mmioInfoIn.pchNext == mmioInfoIn.pchEndRead)
			{
				if(mmioAdvance(m_hmmio, &mmioInfoIn, MMIO_READ) != 0)
				{
					log("ERROR: MmioAdvance function FAILED !!!");
					return E_FAIL;
				}

				if(mmioInfoIn.pchNext == mmioInfoIn.pchEndRead)
				{
					log("ERROR: MmioInfoIn.pchNext Invalid !!!");
					return E_FAIL;
				}
			}

			// actual copy
			*((CCHAR*)buffer + cT) = *((CCHAR*)mmioInfoIn.pchNext);
			mmioInfoIn.pchNext++;
		}

		if(mmioSetInfo(m_hmmio, &mmioInfoIn, 0) != 0)
		{
			log("ERROR: MmioSetInfo function FAILED !!!");
			return E_FAIL;
		}

		(*sizeRead) = dataIn;

		log("File Read SUCCESSFULLY !!!");
		return S_OK;
	}
}

HRESULT WaveFile::writeFile(UBIGINT sizeToWrite, UCCHAR* dataToWrite, UBIGINT* sizeWrote)
{
	UINT cT;

	if(m_bReadFromMem)
	{
		log("ERROR: Not reading from memory !!!");
		return E_NOTIMPL;
	}

	if(!m_hmmio)
	{
		log("ERROR: m_hmmio not initialized !!!");
		return CO_E_NOTINITIALIZED;
	}

	if((sizeWrote == NULL) || (dataToWrite == NULL))
	{
		log("ERROR: Invalid argument/size wrote or data !!!");
		return E_INVALIDARG;
	}

	(*sizeWrote) = 0;

	for(cT = 0; cT < sizeToWrite; cT++)
	{
		if(m_mmioInfoOut.pchNext == m_mmioInfoOut.pchEndWrite)
		{
			m_mmioInfoOut.dwFlags |= MMIO_DIRTY;
			if(mmioAdvance(m_hmmio, &m_mmioInfoOut, MMIO_WRITE) != 0)
			{
				log("ERROR: MmioAdvance function FAILED !!!");
				return E_FAIL;
			}
		}

		*((CCHAR*)m_mmioInfoOut.pchNext) = *((CCHAR*)dataToWrite + cT);
		(CCHAR*)m_mmioInfoOut.pchNext++;
		(*sizeWrote)++;
	}

	log("File written SUCCESSFULLY !!!");
	return S_OK;
}

HRESULT WaveFile::resetFile()
{
	log("Reset the file !!!");
	if(m_bReadFromMem)
		m_pCurrentData = m_pData;
	else
	{
		if(!m_hmmio)
		{
			log("ERROR: m_hmmio not initialized !!!");
			return CO_E_NOTINITIALIZED;
		}

		if(m_iFlags == READ)
		{
			// Seek to the data.
			if(mmioSeek(m_hmmio, m_ckRiff.dwDataOffset + sizeof(FOURCC), SEEK_SET) == -1)
			{
				log("ERROR: MmioSeek function FAILED !!!");
				return E_FAIL;
			}

			// search the input file for the data chunk.
			m_ck.ckid   = mmioFOURCC('d', 'a', 't', 'a');
			if(mmioDescend(m_hmmio, &m_ck, &m_ckRiff, MMIO_FINDCHUNK) != 0)
			{
				log("ERROR: MmioDescend function FAILED !!!");
				return E_FAIL;
			}
		}
		else
		{
			// create the 'data' chunk that holds the waveform samples.
			m_ck.ckid   = mmioFOURCC('d', 'a', 't', 'a');
			m_ck.cksize = 0;

			if(mmioCreateChunk(m_hmmio, &m_ck, 0) != 0)
			{
				log("ERROR: MmioCreateChunk function FAILED !!!");
				return E_FAIL;
			}

			if(mmioGetInfo(m_hmmio, &m_mmioInfoOut, 0) != 0)
			{
				log("ERROR: MmioGetInfo function FAILED !!!");
				return E_FAIL;
			}
		}
	}

	log("Reset File SUCCEEDED !!!");
	return S_OK;
}

HRESULT WaveFile::close()
{
	log("Closing File !!!");

	if(m_iFlags == READ)
	{
		if(m_hmmio != NULL)
		{
			mmioClose(m_hmmio, 0);
			m_hmmio = NULL;
		}

		if(m_pBuffer)
		{
			delete[] m_pBuffer;
			m_pBuffer = NULL;
		}
	}
	else
	{
		m_mmioInfoOut.dwFlags |= MMIO_DIRTY;

		if(!m_hmmio)
		{
			log("ERROR: m_hmmio not initialized !!!");
			return CO_E_NOTINITIALIZED;
		}

		if(mmioSetInfo(m_hmmio, &m_mmioInfoOut, 0) != 0)
		{
			log("ERROR: MmioSetInfo function FAILED !!!");
			return E_FAIL;
		}

		// Ascend the output file out of the data chunk
		// the chunk size of the data chunk will be written.
		if(mmioAscend(m_hmmio, &m_ck, 0) != 0)
		{
			log("ERROR: MmioAscend function FAILED !!!");
			return E_FAIL;
		}

		// instead
		if(mmioAscend(m_hmmio, &m_ckRiff, 0) != 0)
		{
			log("ERROR: MmioAscend function FAILED !!!");
			return E_FAIL;
		}

		mmioSeek(m_hmmio, 0, SEEK_SET);

		if((INT)mmioDescend(m_hmmio, &m_ckRiff, NULL, 0) != 0)
		{
			log("ERROR: MmioDescend function FAILED !!!");
			return E_FAIL;
		}

		m_ck.ckid = mmioFOURCC('f', 'a', 'c', 't');

		if(mmioDescend(m_hmmio, &m_ck, &m_ckRiff, MMIO_FINDCHUNK) == 0)
		{
			ULINT samples = 0;
			mmioWrite(m_hmmio, (HPSTR)&samples, sizeof(ULINT));
			mmioAscend(m_hmmio, &m_ck, 0);
		}

		// Ascend the output file out of the RIFF chunk.
		// will write teh chunk size of the RIFF chunk.
		if(mmioAscend(m_hmmio, &m_ckRiff, 0) != 0)
		{
			log("ERROR: MmioAscend function FAILED !!!");
			return E_FAIL;
		}

		mmioClose(m_hmmio, 0);
		m_hmmio = NULL;
	}

	log("File Successfully closed !!!");
	return S_OK;
}
