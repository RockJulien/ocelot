#include "OcelotDirectAudio.h"

using namespace Audio;

HRESULT createAudioDevice(HINSTANCE hDllMod, IOcelotAudioDevice** pInterface)
{
	if(!(*pInterface))
	{
		(*pInterface) = new OcelotDirectAudio(hDllMod);
		return S_OK;
	}
	return E_FAIL;
}

HRESULT releaseAudioDevice(IOcelotAudioDevice** pInterface)
{
	if(!(*pInterface))
		return E_FAIL;

	delete (*pInterface);
	(*pInterface) = NULL;
	return S_OK;
}

OcelotDirectAudio::OcelotDirectAudio(HINSTANCE hDllMod)
{
	m_hDllMod      = hDllMod;
	m_pLog         = NULL;
	m_bRunning     = false;
	m_bLogFileOpen = false;
	m_b3DSound     = false;

	// debug log file initialization
	m_pLog = fopen("OcelotAudioDeviceLog.txt", "w");
	log("SUCCEESS. Need to initialize now !!!");
}

OcelotDirectAudio::~OcelotDirectAudio()
{
	release();
}

VVOID OcelotDirectAudio::log(CCHAR* str, ...)
{
	CCHAR  str2[256];
	CCHAR* args;

	args = (CCHAR*)&str + sizeof(str);
	vsprintf(str2, str, args);
	fprintf(m_pLog, "Starting OcelotDA Debug Log: ");
	fprintf(m_pLog, str2);
	fprintf(m_pLog, "\n");

	if(m_bLogFileOpen)
		fflush(m_pLog);
}

HRESULT OcelotDirectAudio::initialize(BBOOL b3DSound)
{
	log("Starting initialization !!!");
	m_bLogFileOpen = true;
	m_b3DSound     = b3DSound;
	ZeroMemory(&m_audioState, sizeof(GameAudioState));

	// initialize COM
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	UINT32 iFlags = 0;
#ifdef _DEBUG
	iFlags |= XAUDIO2_DEBUG_ENGINE;
#endif

	if(FAILED(XAudio2Create(&m_audioState.pAudio, iFlags)))
	{
		log("ERROR: Creation of the Audio FAILED !!!");
		return E_FAIL;
	}

	// create the mastering voice
	if(FAILED(m_audioState.pAudio->CreateMasteringVoice(&m_audioState.pMasterVoice)))
	{
		ReleaseCOM(m_audioState.pAudio);
		log("ERROR: Creation of the Mastering voice FAILED !!!");
		return E_FAIL;
	}

	// stop here for just having a global music in game.
	// check device details for being sure it is supported.
	XAUDIO2_DEVICE_DETAILS details;
	if(FAILED(m_audioState.pAudio->GetDeviceDetails(0, &details)))
	{
		ReleaseCOM(m_audioState.pAudio);
		log("ERROR: Get Audio details FAILED !!!");
		return E_FAIL;
	}

	if(details.OutputFormat.Format.nChannels > OUTPUTCHANNELS)
	{
		ReleaseCOM(m_audioState.pAudio);
		log("ERROR: OutputChannels number incompatible !!!");
		return E_FAIL;
	}

	// if everything is okay.
	m_audioState.iChannelMask = details.OutputFormat.dwChannelMask;
	m_audioState.iNbChannels  = details.OutputFormat.Format.nChannels;

	// then create the reverb effect
	iFlags = 0;
#ifdef _DEBUG
	iFlags |= XAUDIO2FX_DEBUG;
#endif

	if(FAILED(XAudio2CreateReverb(&m_audioState.pReverbEffect, iFlags)))
	{
		ReleaseCOM(m_audioState.pAudio);
		log("ERROR: Creation of Reverb FAILED !!!");
		return E_FAIL;
	}

	// create the submix voice.
	// run the reverb in mono mode (will save CPU overhead)
	XAUDIO2_EFFECT_DESCRIPTOR effects[] = {{m_audioState.pReverbEffect, true, 1}};
	XAUDIO2_EFFECT_CHAIN effectChain    = {1, effects};

	if(FAILED(m_audioState.pAudio->CreateSubmixVoice(&m_audioState.pSubMix, INPUTCHANNELS, details.OutputFormat.Format.nSamplesPerSec, 0, 0, NULL, &effectChain)))
	{
		ReleaseCOM(m_audioState.pAudio);
		ReleaseCOM(m_audioState.pReverbEffect);
		log("ERROR: Creation of the Submix Voice FAILED !!!");
		return E_FAIL;
	}

	// set the FX parameters for creating cool effect such as place resonning particularity.
	XAUDIO2FX_REVERB_PARAMETERS native;
	ReverbConvertI3DL2ToNative(&presetParams[0], &native);
	m_audioState.pSubMix->SetEffectParameters(0, &native, sizeof(native));

	// and finally initialize the 3DAudio for allowing surrounding sounds effect.
	X3DAudioInitialize(details.OutputFormat.dwChannelMask, SpeedOfSound, m_audioState.instance);

	m_audioState.vListenerPos   = Vector3(0.0f);
	m_audioState.vEmitterPos    = Vector3(0.0f);
	m_audioState.fListenerAngle = 0;
	m_audioState.bListenerCone  = true;
	m_audioState.bInnerRadius   = true;
	m_audioState.bRedirectLFE   = ((details.OutputFormat.dwChannelMask & SPEAKER_LOW_FREQUENCY) != 0);

	X3DAUDIO_VECTOR temp;
	temp.x = m_audioState.vListenerPos.getX();
	temp.y = m_audioState.vListenerPos.getY();
	temp.z = m_audioState.vListenerPos.getZ();
	m_audioState.listener.Position    = temp;

	temp.x = 0.0f;
	temp.y = 0.0f;
	temp.z = 1.0f;  // Z Axis
	m_audioState.listener.OrientFront = temp;

	temp.x = 0.0f;
	temp.y = 1.0f;  // Y axis
	temp.z = 0.0f;
	m_audioState.listener.OrientTop   = temp;

	m_audioState.listener.pCone = (X3DAUDIO_CONE*)&listenerDirectionalCone;

	// determine the cone for the emitter.
	m_audioState.emitter.pCone  = &m_audioState.emitCone;
	m_audioState.emitter.pCone->InnerAngle  = 0.0f;
	m_audioState.emitter.pCone->OuterAngle  = 0.0f;
	m_audioState.emitter.pCone->InnerVolume = 0.0f;
	m_audioState.emitter.pCone->OuterVolume = 1.0f;
	m_audioState.emitter.pCone->InnerLPF    = 0.0f;
	m_audioState.emitter.pCone->OuterLPF    = 1.0f;
	m_audioState.emitter.pCone->InnerReverb = 0.0f;
	m_audioState.emitter.pCone->OuterReverb = 1.0f;

	X3DAUDIO_VECTOR tempE;
	tempE.x = m_audioState.vEmitterPos.getX();
	tempE.y = m_audioState.vEmitterPos.getY();
	tempE.z = m_audioState.vEmitterPos.getZ();
	m_audioState.emitter.Position    = tempE;

	tempE.x = 0.0f;
	tempE.y = 0.0f;
	tempE.z = 1.0f;
	m_audioState.emitter.OrientFront = tempE;

	tempE.x = 0.0f;
	tempE.y = 1.0f;
	tempE.z = 0.0f;
	m_audioState.emitter.OrientTop   = tempE;

	m_audioState.emitter.ChannelCount  = INPUTCHANNELS;
	m_audioState.emitter.ChannelRadius = 1.0f;
	m_audioState.emitter.pChannelAzimuths = m_audioState.fEmitterAzimuths;

	// use of the inner radius smooth transitions as a sound travels in every direction
	// regarding to the listener.
	m_audioState.emitter.InnerRadius      = 2.0f;
	m_audioState.emitter.InnerRadiusAngle = X3DAUDIO_PI * 0.25f;
	m_audioState.emitter.pVolumeCurve     = (X3DAUDIO_DISTANCE_CURVE*)&X3DAudioDefault_LinearCurve;
	m_audioState.emitter.pLFECurve        = (X3DAUDIO_DISTANCE_CURVE*)&emitterLFECurve;
	m_audioState.emitter.pLPFDirectCurve  = NULL;
	m_audioState.emitter.pLPFReverbCurve  = NULL;
	m_audioState.emitter.pReverbCurve     = (X3DAUDIO_DISTANCE_CURVE*)&emitterReverbCurve;
	m_audioState.emitter.CurveDistanceScaler = 100.0f;
	m_audioState.emitter.DopplerScaler       = 1.0f;

	m_audioState.dspSettings.SrcChannelCount = INPUTCHANNELS;
	m_audioState.dspSettings.DstChannelCount = m_audioState.iNbChannels;
	m_audioState.dspSettings.pMatrixCoefficients = m_audioState.fMatCoefficients;
	log("3D Sound parameters initialized !!!");

	// ending up with an aknowledgement
	log("Initialization SUCCEEDED !!!");
	m_audioState.bInitialized = true;
	m_bRunning = true;
	return S_OK;
}

HRESULT OcelotDirectAudio::startSound()
{
	if(!m_audioState.bInitialized)
		return E_FAIL;

	m_bRunning = true;
	return m_audioState.pAudio->StartEngine();
}

HRESULT OcelotDirectAudio::loadSound(const CCHAR* name, BBOOL bLoop)
{
	// store the music name for replaying and only
	// reload on the fly when needed.
	m_cMusicName = name;

	if(!m_audioState.bInitialized)
	{
		log("ERROR: Audio not initialized/ Can't load a sound !!!");
		return E_FAIL;
	}

	if(m_audioState.pSource)
	{
		m_audioState.pSource->Stop();
		m_audioState.pSource->DestroyVoice();
		m_audioState.pSource = NULL;
		log("Source Voice Destroyed !!!");
	}

	// look for media.
	CCHAR filePath[MAX_PATH];
	CCHAR waveFilePath[MAX_PATH];

	strcpy_s(waveFilePath, MAX_PATH, "..\\Resources\\");
	strcat_s(waveFilePath, MAX_PATH, name);

	if(FAILED(FindMediaFileCch(filePath, MAX_PATH, waveFilePath)))
	{
		log("ERROR: No Media File Found !!!");
		return E_FAIL;
	}

	// Read in the Wave File.
	WaveFile music;
	if(FAILED(music.open(filePath, NULL, READ)))
	{
		log("ERROR: Cannot open the Wave File !!!");
		return E_FAIL;
	}

	// Get the format of the Wave File.
	WAVEFORMATEX* pWF      = music.getFormat();

	// Compute how many bytes and samples are present.
	ULINT waveSize = music.getSize();

	// Read the sample data into the memory.
	DeleteArray(m_audioState.pData);
	m_audioState.pData = new BYTE[waveSize];
	if(!m_audioState.pData)
	{
		log("ERROR: New operator/allocation in stack memory FAILED !!!");
		return E_FAIL;
	}

	if(FAILED(music.readFile(m_audioState.pData, waveSize, &waveSize)))
	{
		log("ERROR: Cannot Read the File !!!");
		return E_FAIL;
	}

	// And play the music using source voice that send
	// to both the submix and mastering voice.
	XAUDIO2_SEND_DESCRIPTOR sendDesc[2];
	sendDesc[0].Flags        = XAUDIO2_SEND_USEFILTER;  // direct path
	sendDesc[0].pOutputVoice = m_audioState.pMasterVoice;
	sendDesc[1].Flags        = XAUDIO2_SEND_USEFILTER;  // reverb path
	sendDesc[1].pOutputVoice = m_audioState.pSubMix;
	const XAUDIO2_VOICE_SENDS sendList = {2, sendDesc};

	// then, create the source voice.
	if(FAILED(m_audioState.pAudio->CreateSourceVoice(&m_audioState.pSource, pWF, 0, 2.0f, NULL, &sendList)))
	{
		log("ERROR: Creation of the source voice FAILED !!!");
		return E_FAIL;
	}

	// Submit the Wave data thanks to a buffer structure.
	XAUDIO2_BUFFER buff = {0};
	buff.pAudioData = m_audioState.pData;
	buff.Flags      = XAUDIO2_END_OF_STREAM;
	buff.AudioBytes = waveSize;

	if(bLoop)
		buff.LoopCount  = XAUDIO2_LOOP_INFINITE;  // play all the time.
	else
	{
		buff.LoopCount  = 0; // play once.
		buff.LoopBegin  = 0; // forced to 0 (because of LoopCount = 0).
		buff.LoopLength = 0; // forced to 0 (because of LoopCount = 0).
		buff.PlayBegin  = 0; // start from the first sample.
	}


	if(FAILED(m_audioState.pSource->SubmitSourceBuffer(&buff)))
	{
		log("ERROR: Submition of buffer FAILED !!!");
		return E_FAIL;
	}

	if(FAILED(m_audioState.pSource->Start()))
	{
		log("ERROR: Cannot start to read music !!!");
		return E_FAIL;
	}

	m_audioState.iNbFrameToApplyAudio = 0;
	m_bRunning = true;
	log("Sound loaded SUCCEESSFULLY and Ready!!!");
	return S_OK;
}

HRESULT OcelotDirectAudio::playSound(UINT iId, FFLOAT delta)
{
	if(!m_audioState.bInitialized)
		return S_FALSE;
	
	if(m_b3DSound)
	{
		if(m_audioState.iNbFrameToApplyAudio == 0)
		{
			Vector3 deltaTemp;

			// compute listener orientation in x/z plane
			if((m_audioState.vListenerPos.getX() != m_audioState.listener.Position.x) || (m_audioState.vListenerPos.getZ() != m_audioState.listener.Position.z))
			{
				deltaTemp = m_audioState.vListenerPos - Vector3(m_audioState.listener.Position.x, m_audioState.listener.Position.y, m_audioState.listener.Position.z);
				m_audioState.fListenerAngle = atan2f(deltaTemp.getX(), deltaTemp.getZ());
				deltaTemp.y(0.0f);
				deltaTemp.Vec3Normalise();

				// reset new orientation
				m_audioState.listener.OrientFront.x = deltaTemp.getX();
				m_audioState.listener.OrientFront.y = 0.0f;
				m_audioState.listener.OrientFront.z = deltaTemp.getZ();
			}

			// set the listener cone parameter if choosen.
			if(m_audioState.bListenerCone)
				m_audioState.listener.pCone = (X3DAUDIO_CONE*)&listenerDirectionalCone;
			else
				m_audioState.listener.pCone = NULL;

			// set the inner radius if choosen.
			if(m_audioState.bInnerRadius)
			{
				m_audioState.emitter.InnerRadius      = 2.0f;
				m_audioState.emitter.InnerRadiusAngle = X3DAUDIO_PI * 0.25f;
			}
			else
			{
				m_audioState.emitter.InnerRadius      = 0.0f;
				m_audioState.emitter.InnerRadiusAngle = 0.0f;
			}

			if(delta > 0.0f)
			{
				Vector3 lVelo = deltaTemp / delta;
				m_audioState.listener.Position.x = m_audioState.vListenerPos.getX();
				m_audioState.listener.Position.y = m_audioState.vListenerPos.getY();
				m_audioState.listener.Position.z = m_audioState.vListenerPos.getZ();
				X3DAUDIO_VECTOR temp;
				temp.x = lVelo.getX();
				temp.y = lVelo.getY();
				temp.z = lVelo.getZ();
				m_audioState.listener.Velocity   = temp;

				Vector3 eVelo = (m_audioState.vEmitterPos - Vector3(m_audioState.emitter.Position.x, m_audioState.emitter.Position.y, m_audioState.emitter.Position.z)) / delta;
				m_audioState.emitter.Position.x = m_audioState.vEmitterPos.getX();
				m_audioState.emitter.Position.y = m_audioState.vEmitterPos.getY();
				m_audioState.emitter.Position.z = m_audioState.vEmitterPos.getZ();
				temp.x = eVelo.getX();
				temp.y = eVelo.getY();
				temp.z = eVelo.getZ();
				m_audioState.emitter.Velocity   = temp;
			}

			DWORD iFlags = X3DAUDIO_CALCULATE_MATRIX | X3DAUDIO_CALCULATE_DOPPLER |
						   X3DAUDIO_CALCULATE_LPF_DIRECT | X3DAUDIO_CALCULATE_LPF_REVERB |
						   X3DAUDIO_CALCULATE_REVERB;

			// set the route to the LFE destination channel if choosen.
			if(m_audioState.bRedirectLFE)
				iFlags |= X3DAUDIO_CALCULATE_REDIRECT_TO_LFE;

			X3DAudioCalculate(m_audioState.instance, &m_audioState.listener, &m_audioState.emitter, iFlags, &m_audioState.dspSettings);

			IXAudio2SourceVoice* theVoice = m_audioState.pSource;
			if(theVoice)
			{
				theVoice->SetFrequencyRatio(m_audioState.dspSettings.DopplerFactor);
				theVoice->SetOutputMatrix(m_audioState.pMasterVoice, INPUTCHANNELS, m_audioState.iNbChannels, m_audioState.fMatCoefficients);
				theVoice->SetOutputMatrix(m_audioState.pSubMix, 1, 1, &m_audioState.dspSettings.ReverbLevel);

				XAUDIO2_FILTER_PARAMETERS filterParametersDirect = {LowPassFilter, 2.0f * sinf(X3DAUDIO_PI / 6.0f * m_audioState.dspSettings.LPFDirectCoefficient), 1.0f};
				theVoice->SetOutputFilterParameters(m_audioState.pMasterVoice, &filterParametersDirect);
				XAUDIO2_FILTER_PARAMETERS filterParametersReverb = {LowPassFilter, 2.0f * sinf(X3DAUDIO_PI / 6.0f * m_audioState.dspSettings.LPFReverbCoefficient), 1.0f};
				theVoice->SetOutputFilterParameters(m_audioState.pSubMix, &filterParametersReverb);
			}
		}
	}

	m_audioState.iNbFrameToApplyAudio++;
	m_audioState.iNbFrameToApplyAudio &= 1;

	return S_OK;
}

VVOID    OcelotDirectAudio::stopSound()
{
	if(!m_audioState.bInitialized)
		return;

	m_bRunning = false;
	return m_audioState.pAudio->StopEngine();
}

HRESULT OcelotDirectAudio::setReverb(ReverbEffect iReverb)
{
	if(!m_audioState.bInitialized)
	{
		log("ERROR: Can't set Reverb/Audio not initialized !!!");
		return S_FALSE;
	}

	if(iReverb > NB_PRESETS)
	{
		log("ERROR: Index of Reverb incorrect/Up to maximum possible !!!");
		return E_FAIL;
	}

	if(m_audioState.pSubMix)
	{
		XAUDIO2FX_REVERB_PARAMETERS native;
		ReverbConvertI3DL2ToNative(&presetParams[iReverb], &native);
		m_audioState.pSubMix->SetEffectParameters(0, &native, sizeof(native));
		log("Set new reverb SUCCEEDED !!!");
	}

	return S_OK;
}

HRESULT OcelotDirectAudio::trigger()
{
	return loadSound(m_cMusicName);
}

VVOID    OcelotDirectAudio::setListener(Vector3 pos, Vector3 dir, Vector3 up, Vector3 vel)
{
	m_audioState.vListenerPos = pos;
}

VVOID    OcelotDirectAudio::setSoundPosition(Vector3 pos, UINT iID)
{
	m_audioState.vEmitterPos = pos;
}

VVOID    OcelotDirectAudio::setSoundDirection(Vector3 dir, Vector3 v, UINT iID)
{
	memcpy(&(m_audioState.emitter.OrientFront), &dir, vecOffset);
	memcpy(&(m_audioState.emitter.Velocity),    &v  , vecOffset);
}

VVOID    OcelotDirectAudio::setSoundMaxDistance(const FFLOAT& dist, UINT s)
{
	m_audioState.emitter.CurveDistanceScaler = dist;
}

VVOID    OcelotDirectAudio::release()
{
	log("Shutting down the Direct Audio !!!");

	// if m_bRunning, uninitialize COM
	if(m_bRunning)
	{
		if(!m_audioState.bInitialized)
		return;

		if(m_audioState.pSource)
		{
			m_audioState.pSource->DestroyVoice();
			m_audioState.pSource = NULL;
		}

		if(m_audioState.pSubMix)
		{
			m_audioState.pSubMix->DestroyVoice();
			m_audioState.pSubMix = NULL;
		}

		if(m_audioState.pMasterVoice)
		{
			m_audioState.pMasterVoice->DestroyVoice();
			m_audioState.pMasterVoice = NULL;
		}

		m_audioState.pAudio->StopEngine();
		ReleaseCOM(m_audioState.pAudio);
		ReleaseCOM(m_audioState.pReverbEffect);
		DeleteArray(m_audioState.pData);

		CoUninitialize();
		m_audioState.bInitialized = false;
	}

	log("Deconnection succeeded !!!");
	fclose(m_pLog);
}
