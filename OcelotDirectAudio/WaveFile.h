#ifndef DEF_WAVEFILE_H
#define DEF_WAVEFILE_H

#include <Windows.h>
#include <stdio.h>
#include "..\OcelotAudio\AudioUtilities.h"

#pragma warning (disable : 4996)

namespace Audio
{

#define READ 1
#define WRITE 2

	class WaveFile
	{
	protected:

			// Attributes
			WAVEFORMATEX*  m_pWaveFormat;
			HMMIO          m_hmmio;
			MMCKINFO       m_ck;
			MMCKINFO       m_ckRiff;
			MMIOINFO       m_mmioInfoOut;
			ULINT		   m_iFlags;
			ULINT		   m_iWaveSize;
			ULINT		   m_iDataSize;
			UCCHAR*	       m_pData;
			UCCHAR*		   m_pCurrentData;
			CCHAR*         m_pBuffer;
			BBOOL          m_bReadFromMem;
			FILE*          m_pLog;

			// private methods
			HRESULT readMMIO();
			HRESULT writeMMIO(WAVEFORMATEX* pWaveFormatDest);
			VVOID   log(CCHAR*, ...);

	public:

			// Constructor & Destructor
			WaveFile();
			~WaveFile();

			// Methods
			HRESULT open(CCHAR* strFileName, WAVEFORMATEX* fT, ULINT flags);
			HRESULT openFromMemory(UCCHAR* data, ULINT dataSize, WAVEFORMATEX* fT, ULINT flags);
			HRESULT readFile(UCCHAR* buffer, ULINT sizeToRead, ULINT* sizeRead);
			HRESULT writeFile(UBIGINT sizeToWrite, UCCHAR* dataToWrite, UBIGINT* sizeWrote);
			HRESULT resetFile();
			HRESULT close();

			// Accessors
	inline  WAVEFORMATEX* getFormat() const { return m_pWaveFormat;}
	inline  ULINT getSize() const { return m_iWaveSize;}
	};
}

#endif