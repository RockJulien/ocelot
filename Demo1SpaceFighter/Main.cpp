#include "Main.h"
#include <ctime>
#include "Geometry\CubeMap.h"
#include "Geometry\Cube.h"
#include "Geometry\Sphere.h"
#include "ResourceManager\ResourceFactory.h"

#pragma warning (disable : 4099)

using namespace Ocelot;

float SpaceFighterDemo::sGameTime = 0.0f;

BIGINT WINAPI WinMain( HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPSTR lpCmdLine,
					   BIGINT nShowCmd )
{

	SpaceFighterDemo app;
	app.initApp(hInstance, SW_HIDE, L"SpaceFighterDemo");
	

	return app.run();
}

SpaceFighterDemo::SpaceFighterDemo() : 
DXOcelotApp(), mCamera(NULL), mElapsedTime(0.0f), mCubmap(nullptr), mSun(nullptr),
mWireFrame(false), mBounded(false), mCentralCursor(NULL), mTransparentBlendState(NULL)
{
	srand(static_cast<UBIGINT>(time(0)));
}

SpaceFighterDemo::~SpaceFighterDemo()
{
	clean3DDevice();

	DeletePointer(mCamera);
	DeletePointer(mPlayer);
	DeletePointer(mCentralCursor);

	if 
		( this->mPlanets.size() > 0 )
	{
		std::vector<IOcelotMesh*>::iterator lMeshIter = this->mPlanets.begin();
		for 
			( ; lMeshIter != this->mPlanets.end(); lMeshIter++ )
		{
			DeletePointer((*lMeshIter));
		}
		this->mPlanets.clear();
	}

	DeletePointer(this->mCubmap);
	DeletePointer(this->mSun);

	if(AudioMng)
		AudioMng->release();
}

BIGINT     SpaceFighterDemo::run()
{
	// the main loop
	MSG lMsg = {0};

	this->m_timer.resetTimer();

	while 
		( lMsg.message != WM_QUIT )
	{
		if 
			( PeekMessage( &lMsg, NULL, 0, 0, PM_REMOVE ) )
		{												  

			// translate keystroke messages into the right format
			TranslateMessage(&lMsg);

			// send the message to the WindowProc
			DispatchMessage(&lMsg);
		}
		else
		{
			if
				( this->m_bPaused )
			{
				Sleep(50);
			}
			else
			{
				this->m_timer.tick();
				sGameTime = this->m_timer.getGameTime();
				this->update(this->m_timer.getDeltaTime());
				this->renderFrame();
			}
		}
	}

	// clean up DirectX COM
	clean3DDevice();

	return lMsg.wParam;
}

VVOID    SpaceFighterDemo::update(FFLOAT deltaTime)
{
	DXOcelotApp::update(deltaTime);
	this->mElapsedTime = deltaTime;

	// update the user impact
	this->handleInput();

	// update the audio device
	HR(AudioMng->update(deltaTime));

	// refresh frustum planes.
	this->mCamera->computeFrustumPlane();

	if 
		( this->mBounded )
	{
		this->mPlayer->ShowBound();
	}
	else
	{
		this->mPlayer->HideBound();
	}

	// update the player
	this->mPlayer->update(deltaTime);

	if 
		(this->mCentralCursor)
	{
		this->mCentralCursor->update(deltaTime);
	}

	if 
		( this->mCubmap )
	{
		this->mCubmap->update(deltaTime);
	}

	if 
		( this->mSun )
	{
		if
			( this->mBounded )
		{
			this->mSun->ShowBound();
		}
		else
		{
			this->mSun->HideBound();
		}

		this->mSun->update(deltaTime);

		// Update sun texture moves.
		// Tile sun texture.
		Matrix4x4 lSunScale;
		lSunScale.Matrix4x4Scaling( lSunScale, 0.5f, 0.5f, 0.0f );
		this->mSunTexOffset.y(this->mSunTexOffset.getY() + 0.0005f * deltaTime);
		this->mSunTexOffset.x(this->mSunTexOffset.getX() + 0.002f * deltaTime);
		Matrix4x4 lSunOffset;
		lSunOffset.Matrix4x4Translation(lSunOffset, this->mSunTexOffset.getX(), this->mSunTexOffset.getY(), 0.0f);

		// Combine scale and translation.
		this->mSunTexTransform = lSunScale * lSunOffset;

		// Set the new sun texture transform.
		this->mSun->SetTextureTransform( this->mSunTexTransform );
	}

	// update planets
	for 
		( UBIGINT lCurrMesh = 0; lCurrMesh < (UBIGINT)this->mPlanets.size(); lCurrMesh++ )
	{
		IOcelotMesh* lStaticMesh = this->mPlanets[lCurrMesh];
		if 
			( this->mBounded )
		{
			lStaticMesh->ShowBound();
		}
		else
		{
			lStaticMesh->HideBound();
		}

		// give a natural rotation to planets.
		IMeshEditor* lEditor = lStaticMesh->StartEdition();
		lEditor->RotateY( RadToDeg(0.05f * deltaTime) );
		DeletePointer(lEditor);
		lStaticMesh->update(deltaTime);
	}
}

VVOID    SpaceFighterDemo::renderFrame()
{
	// render basic DirectX elements
	DXOcelotApp::renderFrame();

	m_device->OMSetDepthStencilState(0, 0);
	FFLOAT blendFactor[] = {0.0f, 0.0f, 0.0f, 0.0f};
	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	if 
		( this->mWireFrame )
	{
		m_device->RSSetState( this->mWireFrameMode );
	}

	if
		(this->mCentralCursor)
	{
		this->mCentralCursor->renderMesh();
	}

	if 
		( this->mCubmap )
	{
		this->mCubmap->renderMesh();
	}

	if 
		( this->mSun )
	{
		this->mSun->renderMesh();
	}

	for 
		( UBIGINT lCurrMesh = 0; lCurrMesh < (UBIGINT)this->mPlanets.size(); lCurrMesh++ )
	{
		this->mPlanets[lCurrMesh]->renderMesh();
	}

	// render the player
	this->mPlayer->renderMesh();

	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	m_swapChain->Present( 1, 0 ); // swap with the back buffer and sync at the monitors refresh rate
}

VVOID    SpaceFighterDemo::initApp(HINSTANCE pInstance, BIGINT pShowCmd, LPWSTR pWindowName)
{
	DXOcelotApp::initApp(pInstance, pShowCmd, pWindowName);

	// mode wireframe
	D3D10_RASTERIZER_DESC rsDesc;
	ZeroMemory(&rsDesc, sizeof(D3D10_RASTERIZER_DESC));
	rsDesc.FillMode = D3D10_FILL_WIREFRAME;
	rsDesc.CullMode = D3D10_CULL_BACK;
    rsDesc.FrontCounterClockwise = false;
	HR(m_device->CreateRasterizerState(&rsDesc, &mWireFrameMode));

	// TransparentBS
	D3D10_BLEND_DESC lBlendState;
	ZeroMemory(&lBlendState, sizeof(D3D10_BLEND_DESC));

	lBlendState.BlendEnable[0] = TRUE;
	lBlendState.SrcBlend = D3D10_BLEND_SRC_ALPHA;
	lBlendState.DestBlend = D3D10_BLEND_INV_SRC_ALPHA;
	lBlendState.BlendOp = D3D10_BLEND_OP_ADD;
	lBlendState.SrcBlendAlpha = D3D10_BLEND_ONE;
	lBlendState.DestBlendAlpha = D3D10_BLEND_ZERO;
	lBlendState.BlendOpAlpha = D3D10_BLEND_OP_ADD;
	lBlendState.RenderTargetWriteMask[0] = D3D10_COLOR_WRITE_ENABLE_ALL;

	HR(this->m_device->CreateBlendState(&lBlendState, &this->mTransparentBlendState));

	// init the audio device.
	// for now, add whatever song you wnat here. just the short name
	// by putting them first in the Resources folder.
	AudioMng->initialize(m_hAppInstance);

	// init the Resource Manager
	ResFactory->SetDevice(m_device);
	HR(ResFactory->CreateTextures());

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3(0.0f, 50.0f, -12000.0f),
							   Vector3(0.0f, 0.0f, 1.0f),
							   (FFLOAT)m_iWindowWidth / m_iWindowHeight,
							   8000.0f );

	this->mCamera = new OcelotCamera(lCamInfo);

	// Create the game central cursor.
	OverlayInfo lCursorInfo( this->m_device, 
							 this->mCamera, 
							 L"GameCursor.png", 
							 Vector2(0.0f, 0.0f), 
							 Vector2(0.1f, 0.1f) );
	this->mCentralCursor = new GameCursor( lCursorInfo );
	this->mCentralCursor->createInstance();

	// Create the light  
	this->mLight = new OcelotLight( Vector3(0.0f, 0.0f, 0.0f), // eye    
									Vector3(0.0f, 0.0f, 1.0f),    // dir    
									Vector3(1.0f, 0.0f, 0.0f),    // atten  
									OcelotColor::DARKGREY,		  // ambient   
									OcelotColor::LIGHTGREY,       // diffuse  
									OcelotColor::GREY,			  // spec  
									30000.0f,					  // range 
									64.0f,						  // spotPow 
									1.0f );						  // specPow

	// Create the fog.
	this->mFog = new OcelotFog( 20.0f,
								30000.0f, 
								OcelotColor::BLACK );

	// load world data from XML file.
	this->loadXMLData();

	CubeMapInfo infoCM( this->m_device,
						this->mCamera,
						8000,
						L"Space2.dds",
						L"..\\Effects\\CubeMap.fx" );

	this->mCubmap = new CubeMap( infoCM );
	this->mCubmap->createInstance();

	SphereInfo lSunInfo( this->m_device,
						 this->mCamera,
						 L"planetLava.jpg",
						 L"..\\Effects\\Sun.fx",
						 this->mLight,
						 this->mFog,
						 Vector3(0.0f),
						 3000.0f,
						 40,
						 40 );
	this->mSun = new Sphere( lSunInfo );
	this->mSun->SetUseTransform( true ); // For sun texture moves over frames (Before to create the instance to init shader var)
	this->mSun->createInstance();


	// ship test
	SpaceShipInfo lSSI( this->m_device, 
						this->mCamera,
						20.0, // Angular Accel of 20 deg.
						this->mLight,
						this->mFog,
						1.0f );

	this->mPlayer = new PlayerSpaceShip( lSSI );
	this->mPlayer->SetSpeedLim( 2000.0 );
	this->mPlayer->createInstance();

	OcelotCameraInfo lInfo = this->mCamera->Info();
	Vector3 lEye = lInfo.Eye();
	this->mPlayer->SetPosition( lEye.getX(), lEye.getY(), lEye.getZ() );

	// init default meshes extracted from file
	for 
		( UBIGINT lCurrMesh = 0; lCurrMesh < (UBIGINT)this->mPlanets.size(); lCurrMesh++ )
	{
		this-mPlanets[lCurrMesh]->createInstance();
	}

	// trigger sounds for ambient and input control
	HR(AudioMng->createSound(L"Robotical.wav", false, true)); // ambient song
	AudioMng->stopASound(L"Robotical.wav");
	HR(AudioMng->createSound(L"rocketFire.wav", false, false)); // triggered song
	AudioMng->stopASound(L"rocketFire.wav");
	HR(AudioMng->createSound(L"LazerFire.wav", false, false));  // triggered song
	AudioMng->stopASound(L"LazerFire.wav");
	HR(AudioMng->createSound(L"spaceShipEngine.wav", false, true));  // space ship engine
	HR(AudioMng->createSound(L"RailJet.wav", false, true)); // ambient song

	// show the window
	ShowWindow(m_mainWindowInstance, SW_SHOW);
}

VVOID    SpaceFighterDemo::handleInput()
{
	if
		( InMng->isPressedAndLocked( OCELOT_RCTRL ) )
	{
		HR(AudioMng->startASound(L"LazerFire.wav"));
		HR(AudioMng->restartASound(L"LazerFire.wav"));
		this->mPlayer->fireLazer();
	}

	if
		( InMng->isPressedAndLocked( OCELOT_RSHIFT ) )
	{
		HR(AudioMng->startASound(L"rocketFire.wav"));
		HR(AudioMng->restartASound(L"rocketFire.wav"));
		this->mPlayer->fireMissile(NULL);
	}

	if 
		( InMng->isPressedAndLocked( OCELOT_B ) )
	{
		this->mBounded = !this->mBounded;
	}

	if 
		( InMng->isPressedAndLocked( OCELOT_W ) )
	{
		this->mWireFrame = !this->mWireFrame;
	}

	if
		( this->mPlayer != NULL )
	{
		if
			( InMng->isPressed(Keyboard, OCELOT_NPPLUS) )
		{
			this->mPlayer->Accelerate();
		}
		if
			( InMng->isPressed(Keyboard, OCELOT_NPMINUS) )
		{
			this->mPlayer->Decelerate();
		}
		if
			(InMng->isPressed(Keyboard, OCELOT_SPACE))
		{
			this->mPlayer->Stop();
		}
		if
			(InMng->isPressed(Keyboard, OCELOT_LEFT))
		{
			this->mPlayer->Move(PlayerSpaceShip::Direction::Left);
		}
		if
			(InMng->isPressed(Keyboard, OCELOT_RIGHT))
		{
			this->mPlayer->Move(PlayerSpaceShip::Direction::Right);
		}
		if
			(InMng->isPressed(Keyboard, OCELOT_UP))
		{
			this->mPlayer->Move(PlayerSpaceShip::Direction::Up);
		}
		if
			(InMng->isPressed(Keyboard, OCELOT_DOWN))
		{
			this->mPlayer->Move(PlayerSpaceShip::Direction::Down);
		}
	}
}

VVOID    SpaceFighterDemo::clean3DDevice()
{
	DXOcelotApp::clean3DDevice();
}

VVOID    SpaceFighterDemo::resize()
{
	DXOcelotApp::resize();

	// refresh the aspect of the camera if the
	// window is resized.
	if
		(this->mCamera)
	{
		FFLOAT lAspect = (FFLOAT)m_iWindowWidth / m_iWindowHeight;
		OcelotCameraInfo lInfo = this->mCamera->Info();
		lInfo.SetAspect(lAspect);  // aspect ratio
		this->mCamera->SetInfo(lInfo);
	}

	if 
		(this->mCentralCursor)
	{
		this->mCentralCursor->Info().OnResize(this->m_iWindowWidth, this->m_iWindowHeight);
	}
}

VVOID    SpaceFighterDemo::loadXMLData()
{
	OcelotXMLFile xmlfile("XML\\GameData.xml");
	OcelotXMLDoc doc;
	try
	{
		doc.parse<rapidxml::parse_no_data_nodes>(xmlfile.data());
	}
	catch(rapidxml::parse_error e)
	{}

	loadXMLMeshData(doc);
	loadXMLCameraData(doc);
}

VVOID    SpaceFighterDemo::loadXMLMeshData(rapidxml::xml_document<>& doc)
{
	OcelotXMLNode *root = doc.first_node("spaceShip");
	OcelotXMLNode *meshesData = root->first_node("meshes");

	BBOOL enabled = atoi(meshesData->first_attribute("enabled")->value()) == 1;
	if(!enabled) 
	{
		return;
	}
	BIGINT count = 0;
	for
		( OcelotXMLNode *currentMesh = meshesData->first_node(); currentMesh; currentMesh = currentMesh->next_sibling())
	{
		BBOOL enabled = atoi(currentMesh->first_attribute("enabled")->value()) == 1;
		if(!enabled) 
		{
			count++;
			continue;
		}

		string type = currentMesh->first_node()->value();
		if
			( type == "sphere" )
		{
			Vector3 pos     = OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x","y","z");
			Vector3 scale   = OcelotXML::ExtractVector3(currentMesh->first_node("scale"),"sx","sy","sz");
			FFLOAT radius    = (FFLOAT)atof(currentMesh->first_attribute("radius")->value());
			BIGINT slices      = atoi(currentMesh->first_attribute("slices")->value());
			BIGINT rings       = atoi(currentMesh->first_attribute("rings")->value());
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			SphereInfo SI(  this->m_device,
							this->mCamera,
							fileNameW,
							L"..\\Effects\\BumpMapping.fx",
							this->mLight,
							this->mFog,
							pos,
							radius,
							rings,
							slices );
			Sphere* lNewSphere = new Sphere( SI );
			this->mPlanets.push_back( lNewSphere );
			lNewSphere->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
			count++;
		} 
		else if
			( type == "cube" )
		{
			Vector3 pos     = OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x","y","z");
			Vector3 radius  = OcelotXML::ExtractVector3(currentMesh->first_node("radius"), "x", "y", "z");
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			CubeInfo CI(this->m_device,
						this->mCamera,
						fileNameW,
						L"..\\Effects\\BumpMapping.fx",
						this->mLight,
						this->mFog,
						pos,
						radius);
			Cube* lNewCube = new Cube(CI);
			this->mPlanets.push_back( lNewCube );
			lNewCube->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
			count++;
		}
	}
}

VVOID    SpaceFighterDemo::loadXMLCameraData(rapidxml::xml_document<>& doc)
{
	OcelotXMLNode *root = doc.first_node("spaceShip");
	OcelotXMLNode *camerasData = root->first_node("cameras");

	BBOOL enabled = atoi(camerasData->first_attribute("enabled")->value()) == 1;
	if(!enabled) 
	{
		return;
	}
}

LRESULT SpaceFighterDemo::AppProc(UBIGINT message,WPARAM wParam,LPARAM lParam)
{
	// add extra button behaviour here
	Vector2 mousePosition;

	FFLOAT dx = 0.0f;
	FFLOAT dy = 0.0f;

	switch(message)
	{
		case WM_LBUTTONDOWN:
			if(wParam & MK_LBUTTON)
			{
				SetCapture(m_mainWindowInstance);

				this->mOldMousePos.x((FFLOAT)LOWORD(lParam));
				this->mOldMousePos.y((FFLOAT)HIWORD(lParam));
			}
			return 0;
			break;

		case WM_LBUTTONUP:
			ReleaseCapture();
			return 0;
			break;

		case WM_MOUSEMOVE:
			if(wParam & MK_LBUTTON)
			{
				mousePosition.x((FFLOAT)LOWORD(lParam));
				mousePosition.y((FFLOAT)HIWORD(lParam));

				FFLOAT lToRad = PI / 180.0f;
				// Make each pixel correspond to a quarter of a degree.
				FFLOAT lDx = (0.25f * static_cast<FFLOAT>(mousePosition.getX() - this->mOldMousePos.getX())) * lToRad;
				FFLOAT lDy = (0.25f * static_cast<FFLOAT>(mousePosition.getY() - this->mOldMousePos.getY())) * lToRad;

				// add extra camera controler stuff here
				/*this->mController->pitch( lDy );
				this->mController->yaw( lDx );*/

				this->mOldMousePos = mousePosition;
			}
			return 0;
	}

	// then pass to the basic Proc.
	return DXOcelotApp::AppProc(message, wParam, lParam);
}
