#ifndef DEF_GAMECURSOR_H
#define DEF_GAMECURSOR_H

#include "Geometry\OcelotDXMesh.h"

								class OverlayInfo : public Ocelot::BaseDXData
								{
								private:

									// Attributes
									Ocelot::ResViewPtr mTexture;
									Vector2			   mWindowSize;
									Vector2			   mScreenPosition;
									Vector2			   mScale;

								public:

									// Constructor & Destructor
									OverlayInfo();
									OverlayInfo(const Ocelot::DevPtr pDevice, const Ocelot::CamPtr pCamera, const WSTRING& pTexturePath, Vector2 pScreenPos, Vector2 pScale);
									~OverlayInfo();

									VVOID OnResize(BIGINT pWidth, BIGINT pHeight);

									// Accessors
									inline Ocelot::ResViewPtr Texture() const { return this->mTexture; }
									inline Vector2    ScreenPosition() const { return this->mScreenPosition; }
									inline Vector2    Scale() const { return this->mScale; }
									inline Vector2	  WindowSize() const { return this->mWindowSize; }
									inline VVOID      SetTexture(const Ocelot::ResViewPtr pTexture) { this->mTexture = pTexture; }
									inline VVOID      SetScreenPosition(const Vector2& pScreenPosition) { this->mScreenPosition = pScreenPosition; }
									inline VVOID      SetScale(const Vector2& pScale) { this->mScale = pScale; }
									inline VVOID	  SetWindowSize(const Vector2& pWindowSize) { this->mWindowSize = pWindowSize; }
								};

class GameCursor : public Ocelot::OcelotDXMesh
{
private:

	// Attributes
	OverlayInfo      mInfo;
	Ocelot::BlendPtr mTransparentBlendState;
	Ocelot::TexPtr   m_fxTexture;
	Ocelot::MatPtr   m_fxWorldViewProj;

	// Private Methods
	VVOID  RefreshWorldBounds();

public:

	// Constructor & Destructor
	GameCursor();
	GameCursor(const OverlayInfo& pInfo);
	~GameCursor();

	// Methods per frame.
	VVOID  update(FFLOAT deltaTime);
	BIGINT updateEffectVariables();
	BIGINT renderMesh();

	// Methods to use once.
	BIGINT initializeLayout();
	BIGINT initializeEffect();
	BIGINT initializeVertices();
	BIGINT initializeIndices();
	VVOID  initializeEffectVariables();
	VVOID  releaseInstance();

	// Accessors
	inline OverlayInfo& Info() { return this->mInfo; }
};

#endif