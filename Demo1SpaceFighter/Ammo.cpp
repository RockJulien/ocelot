#include "Ammo.h"
#include "Main.h"
#include "Particle System/Lightning.h"
#include "Particle System/ParticleEffectsFactory.h"

using namespace Ocelot;

inline BIGINT Sign(const Vector3& dir, const Vector3& toReach)
{
	if((dir.getZ() * toReach.getX()) > (dir.getX() * toReach.getZ()))
		return -1; // anti-clockwise
	else
		return 1;  // clockwise
}

Ammo::Ammo() :
ABaseSpaceShip(AmmoInfo()), mIsFlying(false), mLightning(nullptr), mSmoke(nullptr), mFlame(nullptr)
{
	this->mTheAmmo = new Sphere(this->mInfo.GetSphereInfo());
	this->mTheAmmo->createInstance();
}

Ammo::Ammo(AmmoInfo pInfo) :
ABaseSpaceShip(pInfo), mIsFlying(false), mLightning(nullptr), mSmoke(nullptr), mFlame(nullptr)
{
	SphereInfo& lAmmoShapeInfo = this->mInfo.GetSphereInfo();
	this->mTheAmmo = new Sphere( lAmmoShapeInfo );
	this->mTheAmmo->createInstance();

	DevPtr lDevice = this->mInfo.Device();
	CamPtr lCamera = this->mInfo.GetCamera();
	if 
		( this->mInfo.IsLazer() )
	{
		LightningInfo lInfo(lDevice,
							lCamera,
							L"..\\Effects\\LightningEffect.fx",
							ResFactory->GetResource(L"lbolt.dds"),
							this->Position(),
							this->Position(),
							OcelotColor::RED,
							1,
							10.0f,
							0.00005f);

		this->mLightning = new Lightning(lInfo);
		this->mLightning->createInstance();
	}
	else
	{
		ParticleSystemInfo lFlameInfo;
		lFlameInfo.device = lDevice;
		lFlameInfo.camera = lCamera;
		lFlameInfo.maxnumparticles = 500;
		lFlameInfo.spread = 200.0f;
		lFlameInfo.texture = ResFactory->GetResource(L"flame.dds");

		this->mFlame = ParticleFXFactory->CreateParticleEffect(PE_FIRE, lFlameInfo);
		this->mFlame->SetMeshIndex(-1);
		this->mFlame->SetEmitterOffset(Vector3(0.0f, 0.0f, lAmmoShapeInfo.Radius()));

		ParticleSystemInfo lSmokeInfo;
		lSmokeInfo.device = lDevice;
		lSmokeInfo.camera = lCamera;
		lSmokeInfo.maxnumparticles = 500;
		lSmokeInfo.spread = 200.0f;
		lSmokeInfo.texture = ResFactory->GetResource(L"smoke.dds");

		this->mSmoke = ParticleFXFactory->CreateParticleEffect(PE_SMOKE, lSmokeInfo);
		this->mSmoke->SetMeshIndex(-1);
		this->mSmoke->SetEmitterOffset(Vector3(0.0f, 0.0f, lAmmoShapeInfo.Radius()));
		this->mSmoke->SetEmitDirection(Vector3(0.0f, 1.0f, 0.0f));
	}
}

Ammo::~Ammo()
{
	releaseInstance();
}

VVOID    Ammo::update(FFLOAT deltaTime)
{
	CamPtr lCamera = this->mInfo.GetCamera();

	// refresh variables in the shader file
	HR( this->updateEffectVariables() );

	if 
		( this->mIsFlying )
	{
		this->updateMovements(deltaTime);
	}

	Vector3 lNewPosition = this->Position();
	this->mTheAmmo->SetPosition(lNewPosition.getX(), lNewPosition.getY(), lNewPosition.getZ());
	this->mTheAmmo->update(deltaTime);

	this->SetVisible(lCamera->cullMesh(lNewPosition.getX(), lNewPosition.getY(), lNewPosition.getZ(), this->mInfo.GetSphereInfo().Radius()));

	// Update particle effects only if visible.
	if 
		( this->IsVisible() && 
		  this->mIsFlying )
	{
		if 
			( this->mInfo.IsLazer() )
		{
			this->mLightning->Info().SetStartpoint(this->mShipPosition);
			this->mLightning->Info().SetEndpoint(lNewPosition);
			this->mLightning->SetTime(SpaceFighterDemo::sGameTime);
			this->mLightning->update(deltaTime);
		}
		else
		{

			this->mSmoke->SetEmitterPosition(lNewPosition);
			this->mSmoke->SetEmitDirection(lNewPosition - this->mTargetPos);
			this->mSmoke->SetAcceleration(Vector3(this->mSpeedLimit));
			this->mSmoke->Update(deltaTime, SpaceFighterDemo::sGameTime);
			this->mFlame->SetEmitterPosition(lNewPosition);
			this->mFlame->SetEmitDirection(lNewPosition - this->mTargetPos);
			this->mFlame->SetAcceleration(Vector3(this->mSpeedLimit));
			this->mFlame->Update(deltaTime, SpaceFighterDemo::sGameTime);
		}
	}

	ABaseSpaceShip::update(deltaTime);
}

BIGINT   Ammo::updateEffectVariables()
{
	HRESULT lHr = S_OK;

	this->mTheAmmo->updateEffectVariables();

	return (BIGINT)lHr;
}

VVOID    Ammo::updateMovements(FFLOAT deltaTime)
{
	Vector3 force(0.0f);

	force = seek(mTargetPos);
	mVelocity += force * deltaTime;
	Vector3 lPosition = this->Position();
	Vector3 lNewPosition = lPosition + mVelocity * deltaTime;
	this->SetPosition( lNewPosition.getX(), lNewPosition.getY(), lNewPosition.getZ() );

	if
		( mVelocity.Vec3LengthSquared() > 0.00000001f )
	{
		Vector3 temp = mTargetPos;
		temp.Vec3Normalise();
		SetDir(temp);
	}
}

BIGINT   Ammo::renderMesh()
{
	if 
		( this->IsVisible() &&
		  this->mIsFlying )
	{
		if
			(this->mInfo.IsLazer())
		{
			this->mLightning->renderMesh();
		}
		else
		{
			this->mSmoke->Render();
			this->mFlame->Render();
		}

		HR(mTheAmmo->renderMesh());
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT Ammo::initializeLayout()
{
	return this->mTheAmmo->initializeLayout();
}

BIGINT Ammo::initializeEffect()
{
	return this->mTheAmmo->initializeEffect();
}

BIGINT Ammo::initializeVertices()
{
	return this->mTheAmmo->initializeVertices();
}

BIGINT Ammo::initializeIndices()
{
	return this->mTheAmmo->initializeIndices();
}

VVOID  Ammo::initializeEffectVariables()
{
	this->mTheAmmo->initializeEffectVariables();
}

VVOID  Ammo::releaseInstance()
{
	DeletePointer(mTheAmmo);
	DeletePointer(mLightning);
	DeletePointer(mFlame);
	DeletePointer(mSmoke);
}

Vector3   Ammo::seek(Vector3 target)
{
	Vector3 lDesiredVel = target - this->Position();
	lDesiredVel.Vec3Normalise();
	lDesiredVel *= mSpeedLimit;

	return (lDesiredVel - mVelocity);
}

VVOID     Ammo::reset()
{
	mVelocity     = Vector3(0.0f);
	mTargetPos    = this->Position();
	mAcceleration = Vector3(0.0f);
	Vector3 tempDir = mInfo.GetCamera()->Info().LookAt();
	tempDir.Vec3Normalise();
	SetDir(tempDir);
}

VVOID     Ammo::rotate(FFLOAT pInDegreeX, FFLOAT pInDegreeY, FFLOAT pInDegreeZ)
{
	IMeshEditor* lEditor = this->mTheAmmo->StartEdition();
	lEditor->RotateX(pInDegreeX);
	lEditor->RotateY(pInDegreeY);
	lEditor->RotateZ(pInDegreeZ);
	DeletePointer(lEditor);
}

VVOID     Ammo::RefreshWorldBounds()
{

}

/*******************************************************************************************************************************************/
AmmoInfo::AmmoInfo() :
BaseEntityInfo(), mSphereInfo(), mIsLazer(false)
{

}

AmmoInfo::AmmoInfo(const SphereInfo& pSphereInfo, FFLOAT pMass, BBOOL pIsLazer) :
BaseEntityInfo(pSphereInfo.Device(), pSphereInfo.GetCamera(), pMass), mSphereInfo(pSphereInfo), mIsLazer(pIsLazer)
{

}

AmmoInfo::~AmmoInfo()
{

}
