#ifndef DEF_MAIN_H
#define DEF_MAIN_H

#include "DirectXAPI\DXOcelotApp.h"
#include "..\Ocelot Editor\XML\OcelotRapidXML.h"
#include "Camera\OcelotCameraFactory.h"
#include "GameCursor.h"
#include "AudioManager\AudioManager.h"
#include "PlayerSpaceShip.h"

using namespace Ocelot;

BIGINT WINAPI WinMain( HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPSTR lpCmdLine,
					   BIGINT nShowCmd );

class SpaceFighterDemo : public DXOcelotApp
{
private:

	// Attributes
	GameCursor*				  mCentralCursor;
	OcelotLight*			  mLight; // Global light.
	OcelotFog*				  mFog;
	ID3D10RasterizerState*	  mWireFrameMode;
	ID3D10BlendState*		  mTransparentBlendState;
	Matrix4x4				  mSunTexTransform;
	Vector2					  mSunTexOffset;
	Sphere*					  mSun;
	IOcelotMesh*			  mCubmap;
	std::vector<IOcelotMesh*> mPlanets;
	PlayerSpaceShip*          mPlayer;
	OcelotCamera*             mCamera;
	Vector2                   mOldMousePos;
	FFLOAT                    mElapsedTime;
	BIGINT                    mTerrainIndex;
	BBOOL                     mBounded;
	BBOOL                     mWireFrame;

public:

	// Constructor & Destructor
	SpaceFighterDemo();
	~SpaceFighterDemo();

	// game time property
	static float sGameTime;

	// Methods
	BIGINT   run();
	VVOID    update(FFLOAT deltaTime);
	VVOID    renderFrame();
	VVOID    initApp(HINSTANCE hInstance, BIGINT nShowCmd, LPWSTR windowName);
	VVOID    handleInput();
	VVOID    clean3DDevice();
	VVOID    resize();
	LRESULT  AppProc(UINT message, WPARAM wParam, LPARAM lParam);

	// XML Load data methods
	VVOID    loadXMLData();
	VVOID    loadXMLMeshData(rapidxml::xml_document<>& doc);
	VVOID    loadXMLCameraData(rapidxml::xml_document<>& doc);
};

#endif
