#ifndef DEF_ABASESPACESHIP_H
#define DEF_ABASESPACESHIP_H

#include "ABaseGameEntity.h"

const FFLOAT cLifeAllowed = 100.0f;
const FFLOAT cSpeedMax    = 50.0f;
const FFLOAT cForceMax    = 50.0f;

namespace Ocelot
{
	template<class TInfo>
	class ABaseSpaceShip : public ABaseGameEntity<TInfo>
	{
	protected:

		// Attributes
		FFLOAT  mLife;
		FFLOAT  mSpeedLimit;
		FFLOAT  mForceLimit;
		Vector3 mDirection;
		Vector3 mSideVector;
		Vector3 mVelocity;
		Vector3 mAcceleration;
		Vector3 mTargetPos;

		// Constructor
		ABaseSpaceShip(TInfo pInfo);

	public:

		// Destructor
		virtual ~ABaseSpaceShip();

		// Methods
		virtual VVOID update(FFLOAT deltaTime);

		// Accessors
		inline  FFLOAT   Life() const { return mLife; }
		inline  FFLOAT   SpeedLim() const { return mSpeedLimit; }
		inline  FFLOAT   ForceLim() const { return mForceLimit; }
		inline  Vector3  Dir() const { return mDirection; }
		inline  Vector3  Sid() const { return mSideVector; }
		inline  Vector3  Vel() const { return mVelocity; }
		inline  Vector3  Acc() const { return mAcceleration; }
		inline  Vector3  Target() const { return mTargetPos; }
		inline  VVOID    Repair(FFLOAT life) { mLife = life; }
		inline  VVOID    SetSpeedLim(FFLOAT newSpeedLim) { mSpeedLimit = newSpeedLim; }
		inline  VVOID    SetForceLim(FFLOAT newForceLim) { mForceLimit = newForceLim; }
		inline  VVOID    SetDir(Vector3 newDir) { mDirection = newDir; mSideVector = mDirection.Vec3CrossProduct(Vector3(0.0f, 1.0f, 0.0f)); }
		inline  VVOID    SetVel(Vector3 newVel) { mVelocity = newVel; }
		inline  VVOID    SetAcc(Vector3 newAcc) { mAcceleration = newAcc; }
		inline  VVOID    SetTargetPos(Vector3 target) { mTargetPos = target; }
	};

template<class TInfo>
ABaseSpaceShip<TInfo>::ABaseSpaceShip(TInfo pInfo) :
ABaseGameEntity<TInfo>(pInfo), mLife(cLifeAllowed), mSpeedLimit(cSpeedMax),
mForceLimit(cForceMax), mVelocity(0.0f), mAcceleration(0.0f)
{

}

template<class TInfo>
ABaseSpaceShip<TInfo>::~ABaseSpaceShip()
{

}

template<class TInfo>
VVOID ABaseSpaceShip<TInfo>::update(FFLOAT deltaTime)
{
	mElapsedTime = deltaTime;

	OcelotDXMesh::update(deltaTime);
}

}

#endif