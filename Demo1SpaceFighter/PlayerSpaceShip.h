#ifndef DEF_PLAYERSPACESHIP_H
#define DEF_PLAYERSPACESHIP_H

#include "ABaseSpaceShip.h"
#include "DataStructures\VectorExt.h"
#include "Camera\OcelotCameraFactory.h"
#include "Ammo.h"

const UINT NbLazer   = 40;
const UINT NbMissile = 20;

									class SpaceShipInfo : public Ocelot::BaseEntityInfo
									{
									private:

										// Attributes
										float					mAngularAcceleration;
										Ocelot::OcelotLight*    mLight;
										Ocelot::OcelotFog*      mFog;

									public:

										// Constructors & Destructor
										SpaceShipInfo();
										SpaceShipInfo(Ocelot::DevPtr pDevice, Ocelot::CamPtr pCamera, float pAngularAccel, Ocelot::OcelotLight* pLight, Ocelot::OcelotFog* pFog, FFLOAT pMass);
										~SpaceShipInfo();

										// Accessors
										inline float					AngularAccel() const { return mAngularAcceleration; }
										inline Ocelot::OcelotLight*     Light() const { return mLight; }
										inline Ocelot::OcelotFog*       Fog() const { return mFog; }
										inline VVOID					SetAngularAccel(float pAngularAccel) { this->mAngularAcceleration = pAngularAccel; }
										inline VVOID					SetLight(Ocelot::OcelotLight* light) { mLight = light; }
										inline VVOID					SetFog(Ocelot::OcelotFog* fog) { mFog = fog; }

									};

class PlayerSpaceShip : public Ocelot::ABaseSpaceShip<SpaceShipInfo>
{
private:

			// Attributes
			Ocelot::IOcelotMesh*	 mMesh;
			Ocelot::VectorExt<Ammo*> mMissiles;
			Ocelot::VectorExt<Ammo*> mLazers;
			Ocelot::IOcelotCameraController* mController;
			FFLOAT					 mSpeed;
			UBIGINT					 mLazersFired;
			UBIGINT					 mMissilesFired;

			VVOID UpdateMovement();
			VVOID RefreshWorldBounds();

public:

			// Nested classes
			enum Direction
			{
				Up,
				Down,
				Left,
				Right
			};

			// Constructor & Destructor
			PlayerSpaceShip(SpaceShipInfo pInfo);
			~PlayerSpaceShip();

			// Methods per frame.
			VVOID  update(FFLOAT deltaTime);
			BIGINT updateEffectVariables();
			BIGINT renderMesh();

			// Methods to use once.
	virtual BIGINT createInstance();
			BIGINT initializeLayout();
			BIGINT initializeEffect();
			BIGINT initializeVertices();
			BIGINT initializeIndices();
			VVOID  initializeEffectVariables();
			VVOID  releaseInstance();

			// Gameplay Methods
			VVOID Move(Direction pDirection);
			VVOID Accelerate();
			VVOID Decelerate();
			VVOID Stop();
			VVOID fireLazer();
			VVOID fireMissile(Vector3* const target);

			// Accessors
	inline  const Ocelot::VectorExt<Ammo*>& Missiles() const { return mMissiles; }
	inline  const Ocelot::VectorExt<Ammo*>& Lazers() const { return mLazers; }
};

#endif