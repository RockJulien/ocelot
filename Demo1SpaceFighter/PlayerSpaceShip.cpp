#include "PlayerSpaceShip.h"
#include "MeshLoaders\OBJ\OBJMeshLoader.h"
#include "MeshLoaders\OBJ\OBJMeshLoader2.h"
#include <sstream>

using namespace Ocelot;
using namespace std;

PlayerSpaceShip::PlayerSpaceShip(SpaceShipInfo pInfo) :
ABaseSpaceShip(pInfo), mLazers(), mMissiles(), mController(NULL),
mLazersFired(0), mMissilesFired(0), mMesh(NULL), mSpeed(20.0f)
{
	this->mDirection = pInfo.GetCamera()->Info().LookAt();
}

PlayerSpaceShip::~PlayerSpaceShip()
{
	releaseInstance();
}

BIGINT PlayerSpaceShip::createInstance()
{
	// Embbed the controller
	this->mController = ControlFact->createController( this->mInfo.GetCamera(), CAMERA_CONTROLLER_FLYER, this->mSpeed );

	// Create the spaceship
	OBJMeshLoader lLoader;
	this->mMesh = lLoader.LoadFromFile( this->mInfo.Device(), 
										this->mInfo.GetCamera(), 
										"..\\Models\\CityPatrolVehicle.obj" );
	
	this->mMesh->createInstance();

	// Pass lights (TO DO: Lights must be managed by the scene and not pass to each object at creation time.
	MeshGeometry* lCast = dynamic_cast<MeshGeometry*>( this->mMesh );
	if 
		( lCast != NULL )
	{
		lCast->Info().SetLight( this->mInfo.Light() );
		lCast->Info().SetFog( this->mInfo.Fog() );
	}

	Vector3 lPos = this->mInfo.GetCamera()->Info().Eye();
	float xpos = lPos.getX();
	float ypos = lPos.getY() - 1.0f;
	float zpos = lPos.getZ() + 1.0f;
	lPos.x(xpos);
	lPos.y(ypos);
	lPos.z(zpos);

	// Create weapons
	SphereInfo lSphereInfo( this->mInfo.Device(), 
							this->mInfo.GetCamera(), 
							L"lazer.png", 
							L"..\\Effects\\BumpMapping.fx", 
							this->mInfo.Light(), 
							this->mInfo.Fog(), 
							lPos, 
							10.0f );
	AmmoInfo lAmmoInfo( lSphereInfo, 1.0f, true );

	//create lazer ammo.
	for 
		( UBIGINT currLazer = 0; currLazer < NbLazer; currLazer++ )
	{
		this->mLazers.Add( new Ammo( lAmmoInfo ) );
		this->mLazers[currLazer]->createInstance();
		this->mLazers[currLazer]->SetPosition(xpos, ypos, zpos);
		this->mLazers[currLazer]->SetSpeedLim(5000.0f);
	}

	lAmmoInfo.SetIsLazer(false);
	lAmmoInfo.GetSphereInfo().SetTexturePath( L"missileText.jpg" );
	lAmmoInfo.GetSphereInfo().SetRadius( 25.0f );  // bigger radius

	// create misile ammo.
	for 
		( UBIGINT currMissile = 0; currMissile < NbMissile; currMissile++ )
	{
		this->mMissiles.Add( new Ammo( lAmmoInfo ) );
		this->mMissiles[currMissile]->createInstance();
		this->mMissiles[currMissile]->SetPosition(xpos, ypos, zpos);
		this->mMissiles[currMissile]->SetSpeedLim(3000.0f);
	}

	return ABaseSpaceShip::createInstance();
}

VVOID PlayerSpaceShip::update(FFLOAT deltaTime)
{
	CamPtr lCamera = this->mInfo.GetCamera();

	// update the controller
	this->mController->update( deltaTime );

	// Update movement
	this->UpdateMovement();

	ABaseSpaceShip::update(deltaTime);

	// Aply an object space rotation.
	Matrix4x4 lRotation;
	lRotation.Matrix4x4RotationGeneralized(lRotation, -this->mOrientation.getX(), -this->mOrientation.getY(), this->mOrientation.getZ());
	Matrix4x4 lWorldInverse;
	this->m_matWorld.Matrix4x4Inverse(lWorldInverse);
	lWorldInverse.Matrix4x4Mutliply(lWorldInverse, lRotation);
	lWorldInverse.Matrix4x4Inverse(this->m_matWorld);

	// refresh variables in the shader file
	HR( this->updateEffectVariables() );

	if
		( this->mMesh != NULL )
	{
		Matrix4x4 lMeshOffset;
		lMeshOffset.Matrix4x4Translation(lMeshOffset, 0.0f, -300.0f, 500.0f); // Camera to Ship arm.
		this->mMesh->SetWorld(lMeshOffset * this->m_matWorld); // Scene graph principle.
	}

	const Matrix4x4* lMeshWorld = this->mMesh->World();
	Vector3 lThisPosition(lMeshWorld->get_41(), lMeshWorld->get_42(), lMeshWorld->get_43());
	FFLOAT xpos = lThisPosition.getX();
	FFLOAT ypos = lThisPosition.getY();
	FFLOAT zpos = lThisPosition.getZ();

	for
		(UBIGINT currLazer = 0; currLazer < NbLazer; currLazer++)
	{
		// If not flying, remain to the spaceship position
		if
			( this->mLazers[currLazer]->isFlying() == false )
		{
			// Keep it embedded.
			this->mLazers[currLazer]->SetPosition(xpos, ypos, zpos);
			this->mLazers[currLazer]->SetShipPosition(lThisPosition);
		}
		else
		{
			float lDistance = this->mLazers[currLazer]->Position().Vec3DistanceBetween(this->Position());
			if 
				(lDistance > 15000.0f) // If far from the ship, bring it back to the ship.
			{
				this->mLazers[currLazer]->setFlying(false);
			}
		}

		this->mLazers[currLazer]->update(deltaTime);
	}

	for
		(UBIGINT currMissile = 0; currMissile < NbMissile; currMissile++)
	{
		// If not flying, remain to the spaceship position
		if
			( this->mMissiles[currMissile]->isFlying() == false )
		{
			this->mMissiles[currMissile]->SetPosition(xpos, ypos, zpos);
			this->mMissiles[currMissile]->SetShipPosition(lThisPosition);
		}
		else
		{
			float lDistance = this->mMissiles[currMissile]->Position().Vec3DistanceBetween(this->Position());
			if
				(lDistance > 15000.0f) // If far from the ship, bring it back to the ship.
			{
				this->mMissiles[currMissile]->setFlying(false);
			}
		}

		this->mMissiles[currMissile]->update(deltaTime);
	}
}

BIGINT PlayerSpaceShip::updateEffectVariables()
{
	if 
		( this->mMesh != NULL )
	{
		this->mMesh->updateEffectVariables();
	}

	for
		( UBIGINT lCurrLazer = 0; lCurrLazer < NbLazer; lCurrLazer++ )
	{
		this->mLazers[ lCurrLazer ]->updateEffectVariables();
	}

	for
		( UBIGINT lCurrMissile = 0; lCurrMissile < NbMissile; lCurrMissile++ )
	{
		this->mMissiles[ lCurrMissile ]->updateEffectVariables();
	}

	return 1;
}

BIGINT PlayerSpaceShip::renderMesh()
{
	if 
		( this->mMesh != NULL )
	{
		this->mMesh->renderMesh();
	}

	for
		( UBIGINT currLazer = 0; currLazer < NbLazer; currLazer++ )
	{
		if 
			( this->mLazers[currLazer]->IsVisible())
		{
			this->mLazers[currLazer]->renderMesh();
			if (m_bRenderBounds)
				this->mLazers[currLazer]->Mesh()->ShowBound();
			else
				this->mLazers[currLazer]->Mesh()->HideBound();
		}
	}

	for
		( UBIGINT currMissile = 0; currMissile < NbMissile; currMissile++ )
	{
		if (this->mMissiles[currMissile]->IsVisible())
		{
			this->mMissiles[currMissile]->renderMesh();
			if (m_bRenderBounds)
				this->mMissiles[currMissile]->Mesh()->ShowBound();
			else
				this->mMissiles[currMissile]->Mesh()->HideBound();
		}
	}

	return OcelotDXMesh::renderMesh();
}

BIGINT PlayerSpaceShip::initializeLayout()
{
	if
		( this->mMesh != NULL )
	{
		this->mMesh->initializeLayout();
	}

	for
		( UBIGINT lCurrLazer = 0; lCurrLazer < NbLazer; lCurrLazer++ )
	{
		this->mLazers[ lCurrLazer ]->initializeLayout();
	}

	for
		( UBIGINT lCurrMissile = 0; lCurrMissile < NbMissile; lCurrMissile++ )
	{
		this->mMissiles[ lCurrMissile ]->initializeLayout();
	}

	return 1;
}

BIGINT PlayerSpaceShip::initializeEffect()
{
	if
		( this->mMesh != NULL )
	{
		this->mMesh->initializeEffect();
	}

	for
		( UBIGINT lCurrLazer = 0; lCurrLazer < NbLazer; lCurrLazer++ )
	{
		this->mLazers[ lCurrLazer ]->initializeEffect();
	}

	for
		( UBIGINT lCurrMissile = 0; lCurrMissile < NbMissile; lCurrMissile++ )
	{
		this->mMissiles[ lCurrMissile ]->initializeEffect();
	}

	return 1;
}

BIGINT PlayerSpaceShip::initializeVertices()
{
	if
		( this->mMesh != NULL )
	{
		this->mMesh->initializeVertices();
	}

	for
		( UBIGINT lCurrLazer = 0; lCurrLazer < NbLazer; lCurrLazer++ )
	{
		this->mLazers[ lCurrLazer ]->initializeVertices();
	}

	for
		( UBIGINT lCurrMissile = 0; lCurrMissile < NbMissile; lCurrMissile++ )
	{
		this->mMissiles[ lCurrMissile ]->initializeVertices();
	}

	return 1;
}

BIGINT PlayerSpaceShip::initializeIndices()
{
	if
		( this->mMesh != NULL )
	{
		this->mMesh->initializeIndices();
	}

	for
		( UBIGINT lCurrLazer = 0; lCurrLazer < NbLazer; lCurrLazer++ )
	{
		this->mLazers[ lCurrLazer ]->initializeIndices();
	}

	for
		( UBIGINT lCurrMissile = 0; lCurrMissile < NbMissile; lCurrMissile++ )
	{
		this->mMissiles[ lCurrMissile ]->initializeIndices();
	}

	return 1;
}

VVOID  PlayerSpaceShip::initializeEffectVariables()
{
	if
		( this->mMesh != NULL )
	{
		this->mMesh->initializeEffectVariables();
	}

	for
		( UBIGINT lCurrLazer = 0; lCurrLazer < NbLazer; lCurrLazer++ )
	{
		this->mLazers[ lCurrLazer ]->initializeEffectVariables();
	}

	for
		( UBIGINT lCurrMissile = 0; lCurrMissile < NbMissile; lCurrMissile++ )
	{
		this->mMissiles[ lCurrMissile ]->initializeEffectVariables();
	}
}

VVOID  PlayerSpaceShip::releaseInstance()
{
	DeletePointer( this->mController );
	DeletePointer( this->mMesh );

	if
		(this->mLazers.Size() != 0)
	{
		VectorIter<Ammo*> lLazerIter = this->mLazers.Iterator();
		for
			(; lLazerIter.NotEnd(); lLazerIter.MoveNext())
		{
			Ammo* lToDelete = lLazerIter.Current();
			DeletePointer(lToDelete);
		}
		this->mLazers.RemoveAll();
	}

	if
		(this->mMissiles.Size() != 0)
	{
		VectorIter<Ammo*> lMissileIter = this->mMissiles.Iterator();
		for
			(; lMissileIter.NotEnd(); lMissileIter.MoveNext())
		{
			Ammo* lToDelete = lMissileIter.Current();
			DeletePointer(lToDelete);
		}
		this->mMissiles.RemoveAll();
	}
}

VVOID PlayerSpaceShip::Move(Direction pDirection)
{
	float lAngularAccel  = this->mInfo.AngularAccel();
	double lAngularSpeed = DegToRad( lAngularAccel * this->mElapsedTime );
	
	switch 
		( pDirection )
	{
	case Direction::Up:
		{
			this->mOrientation.x( this->mOrientation.getX() + lAngularSpeed );
			this->mController->pitch( -lAngularSpeed );
		}
		break;
	case Direction::Down:
		{
			this->mOrientation.x( this->mOrientation.getX() - lAngularSpeed );
			this->mController->pitch( lAngularSpeed );
		}
		break;
	case Direction::Left:
		{
			this->mOrientation.y( this->mOrientation.getY() + lAngularSpeed );
			this->mController->yaw( -lAngularSpeed );
		}
		break;
	case Direction::Right:
		{
			this->mOrientation.y( this->mOrientation.getY() - lAngularSpeed );
			this->mController->yaw( lAngularSpeed );
		}
		break;
	}
	
	//this->m_matRotation = lRotMatrix * this->m_matRotation;
}

VVOID PlayerSpaceShip::Accelerate()
{
	FFLOAT lSpeedToAdd =this->mSpeedLimit / 20.0f;
	this->mSpeed += lSpeedToAdd;

	if 
		( this->mSpeed > this->mSpeedLimit)
	{
		this->mSpeed = this->mSpeedLimit;
	}

	static_cast<OcelotFlyerController*>(this->mController)->SetSpeed(this->mSpeed);

	// Just for taking ship Accel when a missile is fired.
	for
		(UBIGINT currLazer = 0; currLazer < NbLazer; currLazer++)
	{
		// If not flying, remain to the spaceship position
		if
			(this->mLazers[currLazer]->isFlying() == false)
		{
			this->mLazers[currLazer]->SetSpeedLim(this->mLazers[currLazer]->SpeedLim() + lSpeedToAdd);
		}
	}

	for
		(UBIGINT currMissile = 0; currMissile < NbMissile; currMissile++)
	{
		// If not flying, remain to the spaceship position
		if
			(this->mMissiles[currMissile]->isFlying() == false)
		{
			this->mMissiles[currMissile]->SetSpeedLim(this->mMissiles[currMissile]->SpeedLim() + lSpeedToAdd);
		}
	}
}

VVOID PlayerSpaceShip::Decelerate()
{
	FFLOAT lSpeedToAdd = this->mSpeedLimit / 20.0f;
	this->mSpeed -= lSpeedToAdd;

	if
		( this->mSpeed < 0.0f )
	{
		this->mSpeed = 0.0f;
	}

	static_cast<OcelotFlyerController*>(this->mController)->SetSpeed(this->mSpeed);

	// Just for taking ship decel when a missile is fired.
	for
		(UBIGINT currLazer = 0; currLazer < NbLazer; currLazer++)
	{
		// If not flying, remain to the spaceship position
		if
			(this->mLazers[currLazer]->isFlying() == false)
		{
			this->mLazers[currLazer]->SetSpeedLim(this->mLazers[currLazer]->SpeedLim() - lSpeedToAdd);
		}
	}

	for
		(UBIGINT currMissile = 0; currMissile < NbMissile; currMissile++)
	{
		// If not flying, remain to the spaceship position
		if
			(this->mMissiles[currMissile]->isFlying() == false)
		{
			this->mMissiles[currMissile]->SetSpeedLim(this->mMissiles[currMissile]->SpeedLim() - lSpeedToAdd);
		}
	}
}

VVOID PlayerSpaceShip::Stop()
{
	this->mSpeed = 0.0f;
	static_cast<OcelotFlyerController*>(this->mController)->SetSpeed(this->mSpeed);
}

VVOID PlayerSpaceShip::UpdateMovement()
{
	this->m_matRotation.Matrix4x4Identity();
	this->m_matTranslation.Matrix4x4Identity();
	CamPtr lCamera = this->mInfo.GetCamera();
	Vector3 lEye   = lCamera->Info().Eye();
	this->m_matTranslation.Matrix4x4Translation( this->m_matTranslation, lEye.getX(), lEye.getY(), lEye.getZ() );
	//this->m_matRotation.Matrix4x4RotationGeneralized(this->m_matRotation, this->mOrientation.getX(), this->mOrientation.getY(), this->mOrientation.getZ());

	this->mController->move( Ocelot::Direction::Forward );
}

VVOID PlayerSpaceShip::fireLazer()
{
	OcelotCameraInfo lInfo = this->mInfo.GetCamera()->Info();
	Vector3 lForward = lInfo.LookAt();
	Vector3 target = lInfo.Eye() + (lForward * 30000.0f);

	if(mLazersFired < NbLazer)
	{
		// fire a lazer.
		this->mLazers[mLazersFired]->SetTargetPos(target);
		this->mLazers[mLazersFired]->setFlying(true);
	}
	// increment
	mLazersFired++;

	BBOOL reload = false;
	if(mLazersFired == NbLazer)
	{
		for(UBIGINT index = 0; index < NbLazer; index++)
		{
			if(!this->mLazers[index]->isFlying())
			{
				mLazersFired--;
				reload = false;
				// fire a lazer.
				this->mLazers[index]->SetTargetPos(target);
				this->mLazers[index]->setFlying(true);
			}
			else
				reload = true;
		}
	}

	if(reload)
	{
		for(UBIGINT index = 0; index < NbLazer; index++)
		{
			this->mLazers[index]->reset();
			this->mLazers[index]->setFlying(false);
		}
		mLazersFired = 0;
	}
}

VVOID PlayerSpaceShip::fireMissile(Vector3* const target)
{
	Vector3 lTarget;
	if 
		( target == NULL )
	{
		OcelotCameraInfo lInfo = this->mInfo.GetCamera()->Info();
		Vector3 lForward = lInfo.LookAt();
		lTarget = Vector3(lInfo.Eye() + (lForward * 30000.0f));
	}
	else
	{
		lTarget = *target;
	}

	if(mMissilesFired < NbMissile)
	{
		// fire a lazer.
		this->mMissiles[mMissilesFired]->SetTargetPos(lTarget);
		this->mMissiles[mMissilesFired]->setFlying(true);
	}
	// increment
	mMissilesFired++;

	BBOOL reload = false;
	if(mMissilesFired == NbMissile)
	{
		for(UBIGINT index = 0; index < NbMissile; index++)
		{
			if(!this->mMissiles[index]->isFlying())
			{
				reload = false;
				// fire a lazer.
				this->mMissiles[index]->SetTargetPos(lTarget);
				this->mMissiles[index]->setFlying(true);
			}
			else
				reload = true;
		}
	}

	if(reload)
	{
		for (UBIGINT index = 0; index < NbMissile; index++)
		{
			this->mMissiles[index]->reset();
			this->mMissiles[index]->setFlying(false);
		}
		mMissilesFired = 0;
	}
}

VVOID PlayerSpaceShip::RefreshWorldBounds()
{

}

/**************************************************************************************************************************************/
SpaceShipInfo::SpaceShipInfo() :
BaseEntityInfo(), mAngularAcceleration(20.0), mLight(NULL), mFog(NULL)
{

}

SpaceShipInfo::SpaceShipInfo(DevPtr pDevice, CamPtr pCamera, float pAngularAccel, OcelotLight* pLight, OcelotFog* pFog, FFLOAT pMass) :
BaseEntityInfo(pDevice, pCamera, pMass), mAngularAcceleration(pAngularAccel), mLight(pLight), mFog(pFog)
{

}

SpaceShipInfo::~SpaceShipInfo()
{

}
