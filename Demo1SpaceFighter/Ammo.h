#ifndef DEF_AMMO_H
#define DEF_AMMO_H

#include "ABaseSpaceShip.h"
#include "Geometry\Sphere.h"

// Forward declaration(s)
namespace Ocelot
{
	class Lightning;
	class ParticleSystem;
}

						class AmmoInfo : public Ocelot::BaseEntityInfo
						{
						private:

							// Attributes
							Ocelot::SphereInfo mSphereInfo;
							BBOOL			   mIsLazer;

						public:

							// Constructors & Destructor
							AmmoInfo();
							AmmoInfo(const Ocelot::SphereInfo& pSphereInfo, FFLOAT pMass, BBOOL pIsLazer = false);
							~AmmoInfo();

							// Accessors
							inline Ocelot::SphereInfo&  GetSphereInfo() { return this->mSphereInfo; }
							inline BBOOL IsLazer() const
							{
								return this->mIsLazer;
							}
							inline VVOID SetIsLazer(BBOOL pIsLazer)
							{
								this->mIsLazer = pIsLazer;
							}
						};

class Ammo : public Ocelot::ABaseSpaceShip<AmmoInfo>
{
private:

	// Attributes
	Ocelot::IOcelotMesh* mTheAmmo;
	Ocelot::Lightning*	 mLightning;
	Ocelot::ParticleSystem* mFlame;
	Ocelot::ParticleSystem* mSmoke;
	Vector3				 mShipPosition;
	BBOOL				 mIsFlying;

	VVOID updateMovements(FFLOAT deltaTime);
	VVOID RefreshWorldBounds();

public:

	// Constructors & Destructor
	Ammo();
	Ammo(AmmoInfo inf);
	~Ammo();

	// Methods per frame.
	VVOID  update(FFLOAT deltaTime);
	BIGINT updateEffectVariables();
	BIGINT renderMesh();

	// Methods to use once.
	BIGINT initializeLayout();
	BIGINT initializeEffect();
	BIGINT initializeVertices();
	BIGINT initializeIndices();
	VVOID  initializeEffectVariables();
	VVOID  releaseInstance();

	// Gameplay Methods
	VVOID    rotate(FFLOAT pInDegreeX, FFLOAT pInDegreeY, FFLOAT pInDegreeZ);
	Vector3  seek(Vector3 target);
	VVOID    reset();

	// Accessors
	inline Ocelot::IOcelotMesh* Mesh() const { return mTheAmmo;}
	inline BBOOL         isFlying() const { return mIsFlying;}
	inline VVOID         setFlying(BBOOL state) { mIsFlying = state;}
	inline VVOID		 SetShipPosition(const Vector3& pPosition)
	{
		this->mShipPosition = pPosition;
	}
};

#endif