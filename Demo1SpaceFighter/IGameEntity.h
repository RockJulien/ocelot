#ifndef DEF_IGAMEENTITY_H
#define DEF_IGAMEENTITY_H

namespace Ocelot
{
	class IGameEntity // interface
	{
	public:

		// Destructor
		IGameEntity(){}
		virtual ~IGameEntity(){}

		// Methods
		virtual VVOID update(FFLOAT deltaTime) = 0;
		virtual VVOID render() = 0; // add methods useful for every entities which could inherit from that one.
	};
}

#endif