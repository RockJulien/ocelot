#ifndef DEF_ABASEGAMEENTITY_H
#define DEF_ABASEGAMEENTITY_H

#include <Windows.h>
#include <cassert>
#include "Geometry\OcelotDXMesh.h"
#include "IGameEntity.h"

namespace Ocelot
{

						class BaseEntityInfo : public BaseDXData
						{
						protected:

									// Attributes
									FFLOAT   mMass;

						public:

									BaseEntityInfo();
									BaseEntityInfo(DevPtr pDevice, CamPtr pCamera, FFLOAT pMass);
							virtual ~BaseEntityInfo();

									// Accessors
							inline  FFLOAT   Mass() const { return this->mMass; }
							inline  VVOID    SetMass(FFLOAT pMass) { this->mMass = pMass; }
						};

	template<class TInfo>
	class ABaseGameEntity : public OcelotDXMesh
	{
	protected:

				// Attributes
				UBIGINT        mId;
				static UBIGINT mNextValidId;
				FFLOAT         mElapsedTime;
				TInfo		   mInfo;

				// private method
				VVOID    setId(UBIGINT pTheId);

				// Constructors
				ABaseGameEntity(UBIGINT pId);
				ABaseGameEntity(TInfo pInfo);

	public:

				// Destructor
		virtual ~ABaseGameEntity();

				// Accessors
		inline  UBIGINT          ID() const { return mId; }
		static  inline  UBIGINT  NextID() { return mNextValidId; }
		inline  TInfo&			 BaseInfo() { return this->mInfo; }
	};

template<class TInfo>
UBIGINT ABaseGameEntity<TInfo>::mNextValidId = 0; // set the static next valid id to 0 at the start.

template<class TInfo>
ABaseGameEntity<TInfo>::ABaseGameEntity(TInfo pInfo) :
mElapsedTime(0.0f), mInfo(pInfo)
{
	setId( ABaseGameEntity<TInfo>::NextID() );
}

template<class TInfo>
ABaseGameEntity<TInfo>::ABaseGameEntity(UBIGINT pId) :
mElapsedTime(0.0f), mInfo()
{
	setId( pId );
}

template<class TInfo>
ABaseGameEntity<TInfo>::~ABaseGameEntity()
{

}

template<class TInfo>
VVOID ABaseGameEntity<TInfo>::setId(UBIGINT pTheId)
{
	// assert that the new ID is greater or equal to the Next one.
	assert( (pTheId >= ABaseGameEntity<TInfo>::mNextValidId) && "ABaseGameEntity<TInfo>::setId>: incompatible ID" );

	// if Okay
	mId = pTheId;
	mNextValidId = mId + 1;
}

}

#endif