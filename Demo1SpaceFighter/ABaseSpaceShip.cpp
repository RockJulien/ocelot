#include "ABaseSpaceShip.h"

using namespace Ocelot;

ABaseSpaceShip::ABaseSpaceShip(BaseEntityInfo pInfo) :
ABaseGameEntity(pInfo), m_fLife(LifeAllowed), m_fSpeedLimit(SpeedMax),
m_fForceLimit(ForceMax), m_vVelocity(0.0f), m_vAcceleration(0.0f),
m_vTargetPos(pInfo.Position())
{

}

ABaseSpaceShip::~ABaseSpaceShip()
{

}

VVOID ABaseSpaceShip::update(FFLOAT deltaTime)
{
	m_fElapsedTime = deltaTime;
}
