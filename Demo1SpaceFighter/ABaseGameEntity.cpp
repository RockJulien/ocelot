#include "ABaseGameEntity.h"

using namespace Ocelot;

/******************************************************************************************************************************************************************/
BaseEntityInfo::BaseEntityInfo() :
BaseDXData(), mMass(1.0f)
{

}

BaseEntityInfo::BaseEntityInfo(DevPtr pDevice, CamPtr pCamera, FFLOAT mass) : 
BaseDXData(pDevice, pCamera), mMass(mass)
{

}

BaseEntityInfo::~BaseEntityInfo()
{

}

