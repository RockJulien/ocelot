#ifndef TORCH_H
#define TORCH_H

#include "Geometry/Cylinder.h"
#include "Particle System/ParticleEffectsFactory.h"
#include "ResourceManager/ResourceFactory.h"

namespace Ocelot
{
	class Torch
	{
	public:
		Torch();
		~Torch();

		VVOID CreateTorch(ID3D10Device* p_device, OcelotCamera* camera, OcelotLight* pLight, OcelotFog* pFog);
		VVOID Update(const FFLOAT delta, const FFLOAT gameTime);
		VVOID Render();
		VVOID Release();

		inline Vector3 GetPosition() const { return stick->Position(); }
		inline VVOID SetPosition(const Vector3& pos) { stick->SetPosition(pos.getX(), pos.getY(), pos.getZ());  smoke->SetEmitterPosition(stick->Position() + smoke->GetEmitterOffset()); flame->SetEmitterPosition(stick->Position() + flame->GetEmitterOffset()); }

		inline VVOID TurnOffTorch() { showEffects = false; }
		inline VVOID TurnOnTorch() { showEffects = true; }
		inline BBOOL IsLit() const { return showEffects; }

	private:

		Cylinder* stick;
		ParticleSystem* flame;
		ParticleSystem* smoke;

		OcelotCamera* cam;

		BBOOL showEffects;
	};
}

#endif