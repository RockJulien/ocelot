#ifndef DEF_BASEGAMEENTITY_H
#define DEF_BASEGAMEENTITY_H

#include <Windows.h>
#include <cassert>
#include "Maths\Vector3.h"

#define NID BaseGameEntity::NextID()

							struct BaseEntityInfo
							{
								BaseEntityInfo() : Mass(1.0f), BRad(1.0f), Pos(0.0f){}
								BaseEntityInfo(FFLOAT mass, FFLOAT rad, Vector3 pos) : Mass(mass), BRad(rad), Pos(pos){}

								FFLOAT   Mass;
								FFLOAT   BRad;
								Vector3 Pos;
							};

class BaseGameEntity
{
protected:

		// Attributes
		UINT           m_iId;
static  UINT           m_iNextValidId;
		FFLOAT          m_fElapsedTime;
		BaseEntityInfo m_baseInfo;

		// private method
		VVOID    setId(UINT theId);

		// private cst
		BaseGameEntity(UINT id);

public:

		// Constructor & Destructor
		BaseGameEntity();
		BaseGameEntity(BaseEntityInfo inf);
virtual ~BaseGameEntity();

		// Methods
virtual VVOID update(FFLOAT deltaTime) = 0;
virtual VVOID render()                = 0;

		// Accessors
inline  UINT            ID() const { return m_iId;}
static  inline  UINT    NextID() { return m_iNextValidId;}
inline  BaseEntityInfo  BaseInfo() const { return m_baseInfo;}
inline  VVOID            ResetInf(BaseEntityInfo inf) { m_baseInfo = inf;}
inline  VVOID            setPos(Vector3 pos) { m_baseInfo.Pos = pos;}
};

#endif
