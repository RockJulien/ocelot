#include "Humanoid.h"

using namespace Ocelot;
using namespace std;

Humanoid::Humanoid() : 
MovingEntity(), m_bWalk(false), m_bInMovement(false), m_bEndSequence(false),
m_fSlowdowner(timeToWait), m_iSequenceCounter(0), m_fTimeCounter(0.0f),
m_RotationTrack(0.0f)
{

}

Humanoid::Humanoid(HUMAN_ATTRIBUTES info) :
MovingEntity(info.baseInfo), m_bWalk(false), m_bInMovement(false), m_bEndSequence(false),
m_fSlowdowner(timeToWait), m_iSequenceCounter(0), m_humanInfo(info), m_fTimeCounter(0.0f),
m_RotationTrack(0.0f)
{

}

Humanoid::~Humanoid()
{
	if(SG)
		SG->destroy();
}

VVOID Humanoid::createEntity()
{
	wifstream stream("HumanInfo.crea");

	// don't care about first informative stuff.
	wstring buffer;
	buffer.resize(50);
	stream >> buffer;

	// store the number of members first.
	UINT nbMems = 0;
	stream >> nbMems;

	// order is important for the auto build process.
	m_members.putBack(new Sphere(m_humanInfo.headInfo));
	m_members.putBack(new Cylinder(m_humanInfo.leftShoulderInfo));
	m_members.putBack(new Cylinder(m_humanInfo.rightShoulderInfo));
	m_members.putBack(new Sphere(m_humanInfo.leftShoulderJointInfo));
	m_members.putBack(new Sphere(m_humanInfo.rightShoulderJointInfo));
	m_members.putBack(new Cylinder(m_humanInfo.leftArmInfo));
	m_members.putBack(new Cylinder(m_humanInfo.rightArmInfo));
	m_members.putBack(new Sphere(m_humanInfo.leftArmJointInfo));
	m_members.putBack(new Sphere(m_humanInfo.rightArmJointInfo));
	m_members.putBack(new Cylinder(m_humanInfo.leftForwardArmInfo));
	m_members.putBack(new Cylinder(m_humanInfo.rightForwardArmInfo));
	m_members.putBack(new Cylinder(m_humanInfo.trunkTopInfo));
	m_members.putBack(new Sphere(m_humanInfo.middleTrunkJointInfo));
	m_members.putBack(new Cylinder(m_humanInfo.trunkBottomInfo));
	m_members.putBack(new Sphere(m_humanInfo.bottomTrunkJointInfo));
	m_members.putBack(new Cylinder(m_humanInfo.hipInfo));
	m_members.putBack(new Sphere(m_humanInfo.leftHipsJointInfo));
	m_members.putBack(new Sphere(m_humanInfo.rightHipsJointInfo));
	m_members.putBack(new Cylinder(m_humanInfo.topLeftLegInfo));
	m_members.putBack(new Cylinder(m_humanInfo.topRightLegInfo));
	m_members.putBack(new Sphere(m_humanInfo.leftLegJointInfo));
	m_members.putBack(new Sphere(m_humanInfo.rightLegJointInfo));
	m_members.putBack(new Cylinder(m_humanInfo.bottomLeftLegInfo));
	m_members.putBack(new Cylinder(m_humanInfo.bottomRightLegInfo));

	for(UINT currMesh = 0; currMesh < nbMems; currMesh++)
		HR(m_members.researchByIndex(currMesh)->createInstance());

	FFLOAT camOffset = 50.0f;

	/**********************************************************  HIGHER PART  *******************************************************************/
	// head group
	TranslationNode* HEADGroupT  = new TranslationNode(0.0f, ((m_humanInfo.headInfo.Radius() * 2.0f) + (5.0f * 2.0f)),  -camOffset);
	RotationNode*    HEADGroupR  = new RotationNode(0.0f);
	ScalingNode*     HEADGroupS  = new ScalingNode(1.0f);
	HEADGroupT->addChild(HEADGroupR);
	HEADGroupS->addChild(HEADGroupT);

	/**********************************************************  TRUNK PART  ********************************************************************/
	// trunk group
	TranslationNode* TRUNKGroupT = new TranslationNode(0.0f, 5.0f,  -camOffset);
	RotationNode*    TRUNKGroupR = new RotationNode(0.0f);
	ScalingNode*     TRUNKGroupS = new ScalingNode(1.0f);
	TRUNKGroupT->addChild(TRUNKGroupR);
	TRUNKGroupS->addChild(TRUNKGroupT);

	/**********************************************************  LOWER PART  ********************************************************************/
	// lower group
	TranslationNode* LOWERGroupT = new TranslationNode(0.0f, -10.0f,  -camOffset);
	RotationNode*    LOWERGroupR = new RotationNode(0.0f);
	ScalingNode*     LOWERGroupS = new ScalingNode(1.0f);
	LOWERGroupT->addChild(LOWERGroupR);
	LOWERGroupS->addChild(LOWERGroupT);

	for(UINT currMem = 0; currMem < nbMems; currMem++)
	{
		// don't care about first info
		if(currMem == 0)
			stream >> buffer >> buffer >> buffer >> buffer;

		// entity of the head
		FFLOAT memRadius = 0.0f, temp1 = 0.0f, temp2 = 0.0f, temp3 = 0.0f;
		stream >> buffer >> memRadius >> temp1 >> temp2 >> temp3;
		ScalingNode*     memberS    = new ScalingNode(temp1, temp2, temp3);
		stream >> temp1 >> temp2 >> temp3;
		TranslationNode* memberT    = new TranslationNode(temp1, temp2, temp3);
		stream >> temp1 >> temp2 >> temp3;
		RotationNode*    memberR    = new RotationNode(DegToRad(temp1), DegToRad(temp2), DegToRad(temp3));
		GeometryNode*    memberGeom = new GeometryNode(m_members.researchByIndex(currMem));

		memberR->addChild(memberGeom);
		memberT->addChild(memberR);
		memberS->addChild(memberT);

		if(currMem < 1) // prepared if addition of extra facial components later
		{
			// link
			HEADGroupR->addChild(memberS);
		}

		if((currMem > 0) && (currMem < 15))
		{
			if(currMem == 3) // left shoulder joint
			{
				TRUNKGroupR->getList().researchByIndex(0)->getList().FirstData()->getList().FirstData()->addChild(memberS); // leftShoulder access
			}
			else if(currMem == 4) // right shoulder joint
			{
				TRUNKGroupR->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS); // rightShoulder access
			}
			else if(currMem == 5) //left arm
			{
				TRUNKGroupR->getList().researchByIndex(0)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS);
			}
			else if(currMem == 6) // right arm
			{
				TRUNKGroupR->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS);
			}
			else if(currMem == 7) // left arm joint
			{
				TRUNKGroupR->getList().researchByIndex(0)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS);
			}
			else if(currMem == 8) // right arm joint
			{
				TRUNKGroupR->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS);
			}
			else if(currMem == 9) // left forward arm
			{
				TRUNKGroupR->getList().researchByIndex(0)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS);
			}
			else if(currMem == 10) // right forward arm
			{
				TRUNKGroupR->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS);
			}
			else
			{
				// link the rest
				TRUNKGroupR->addChild(memberS);
			}
		}

		if((currMem > 14))
		{
			
			if(currMem == 18) // left upper leg
			{
				LOWERGroupR->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS); // left hip joint access
			}
			else if(currMem == 19) // right upper leg
			{
				LOWERGroupR->getList().researchByIndex(2)->getList().FirstData()->getList().FirstData()->addChild(memberS); // right hip joint access
			}
			else if(currMem == 20) // left knee joint
			{
				LOWERGroupR->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS);
			}
			else if(currMem == 21) // right knee joint
			{
				LOWERGroupR->getList().researchByIndex(2)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS);
			}
			else if(currMem == 22) // left lower leg
			{
				LOWERGroupR->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS);
			}
			else if(currMem == 23) // right lower leg
			{
				LOWERGroupR->getList().researchByIndex(2)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->addChild(memberS);
			}
			else
			{
				// link the rest
				LOWERGroupR->addChild(memberS);
			}
		}
	}

	// ends up
	/**********************************************************  SCENE GRAPH  *******************************************************************/
	RotationNode* humanRotate = new RotationNode(0.0f, 0.0f, 0.0f);
	humanRotate->addChild(HEADGroupS);
	humanRotate->addChild(TRUNKGroupS);
	humanRotate->addChild(LOWERGroupS);
	SG->addNode(humanRotate);

	stream.close();
}

VVOID Humanoid::update(FFLOAT deltaTime)
{
	MovingEntity::update(deltaTime);

	// animation 
	m_fTimeCounter += deltaTime;

	SG->update(deltaTime);

	// handle inputs
	if(m_bInMovement)
	{
		SG->Root()->setTranslation(m_baseInfo.Pos);
		m_bInMovement = false;
	}
}

VVOID Humanoid::render()
{
	for(UINT currMesh = 0; currMesh < (UINT)m_members.getSize(); currMesh++)
		HR(m_members.researchByIndex(currMesh)->renderMesh());
}

VVOID Humanoid::walk()
{
	if(!m_bWalk)
	{
		/******************************************moving the head*******************************************/
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LookingAngles", m_headXangles, 1);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LookingAngles", m_headYangles, 2);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LookingAngles", m_headZangles, 3);

		/******************************************moving left arm*******************************************/
		// shoulder joint
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftArmAngles", m_leftShoulderXangles, 2);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftArmAngles", m_leftShoulderYangles, 3);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftArmAngles", m_leftShoulderZangles, 4);
		// elbow joint
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftArmAngles", m_leftElbowXangles, 6);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftArmAngles", m_leftElbowYangles, 7);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftArmAngles", m_leftElbowZangles, 8);

		/******************************************moving right arm******************************************/
		// shoulder joint
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightArmAngles", m_rightShoulderXangles, 2);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightArmAngles", m_rightShoulderYangles, 3);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightArmAngles", m_rightShoulderZangles, 4);
		// elbow joint
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightArmAngles", m_rightElbowXangles, 6);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightArmAngles", m_rightElbowYangles, 7);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightArmAngles", m_rightElbowZangles, 8);

		/******************************************moving left leg*******************************************/
		// hip joint
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftLegAngles", m_leftHipXangles, 2);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftLegAngles", m_leftHipYangles, 3);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftLegAngles", m_leftHipZangles, 4);
		// knee joint
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftLegAngles", m_leftKneeXangles, 6);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftLegAngles", m_leftKneeYangles, 7);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//LeftLegAngles", m_leftKneeZangles, 8);

		/******************************************moving right leg******************************************/
		// hip joint
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightLegAngles", m_rightHipXangles, 2);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightLegAngles", m_rightHipYangles, 3);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightLegAngles", m_rightHipZangles, 4);
		// knee joint
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightLegAngles", m_rightKneeXangles, 6);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightLegAngles", m_rightKneeYangles, 7);
		FSMng->searchRightInformationInFile(L"WalkAngleInfo.crea", L"//RightLegAngles", m_rightKneeZangles, 8);
		
		m_bWalk = true;
	}

	// start the sequence read from the file for every joint concerned by its movement.
	if((m_bInMovement == true) && (m_bEndSequence == false))
	{
		if(m_fTimeCounter > m_fSlowdowner)
		{
			if(m_iSequenceCounter == 0)
				m_iSequenceCounter = 1;

			applyChangeToLeftShoulderJoint(Vector3((FFLOAT)_wtof(m_leftShoulderXangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_leftShoulderYangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_leftShoulderZangles[m_iSequenceCounter].c_str())));
			applyChangeToRightShoulderJoint(Vector3((FFLOAT)_wtof(m_rightShoulderXangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_rightShoulderYangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_rightShoulderZangles[m_iSequenceCounter].c_str())));
			applyChangeToLeftElbowJoint(Vector3((FFLOAT)_wtof(m_leftElbowXangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_leftElbowYangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_leftElbowZangles[m_iSequenceCounter].c_str())));
			applyChangeToRightElbowJoint(Vector3((FFLOAT)_wtof(m_rightElbowXangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_rightElbowYangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_rightElbowZangles[m_iSequenceCounter].c_str())));
			applyChangeToLeftHipJoint(Vector3((FFLOAT)_wtof(m_leftHipXangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_leftHipYangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_leftHipZangles[m_iSequenceCounter].c_str())));
			applyChangeToRightHipJoint(Vector3((FFLOAT)_wtof(m_rightHipXangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_rightHipYangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_rightHipZangles[m_iSequenceCounter].c_str())));
			applyChangeToLeftKneeJoint(Vector3((FFLOAT)_wtof(m_leftKneeXangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_leftKneeYangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_leftKneeZangles[m_iSequenceCounter].c_str())));
			applyChangeToRightKneeJoint(Vector3((FFLOAT)_wtof(m_rightKneeXangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_rightKneeYangles[m_iSequenceCounter].c_str()), (FFLOAT)_wtof(m_rightKneeZangles[m_iSequenceCounter].c_str())));

			m_iSequenceCounter++;

			if(m_iSequenceCounter == (m_leftElbowXangles.size())) // does not matter the array we peek, all same size for synchronized sequences.
			{
				// reinitialization
				m_iSequenceCounter = 0;
				
			}

			m_fTimeCounter = 0.0f;
		}
	}
}

VVOID Humanoid::applyChangeToLeftShoulderJoint(Vector3 angles)
{
	FFLOAT radAngleX = DegToRad(angles.getX());
	FFLOAT radAngleY = DegToRad(angles.getY());
	FFLOAT radAngleZ = DegToRad(angles.getZ());

	// apply rotation to the left shoulder
	SG->Root()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(0)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->applyRotat(radAngleX, radAngleY, radAngleZ);
}

VVOID Humanoid::applyChangeToRightShoulderJoint(Vector3 angles)
{
	FFLOAT radAngleX = DegToRad(angles.getX());
	FFLOAT radAngleY = DegToRad(angles.getY());
	FFLOAT radAngleZ = DegToRad(angles.getZ());

	//apply the rotation to the right shoulder
	SG->Root()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->applyRotat(radAngleX, radAngleY, radAngleZ);
}

VVOID Humanoid::applyChangeToLeftElbowJoint(Vector3 angles)
{
	FFLOAT radAngleX = DegToRad(angles.getX());
	FFLOAT radAngleY = DegToRad(angles.getY());
	FFLOAT radAngleZ = DegToRad(angles.getZ());

	// apply the rotation to the left elbow
	SG->Root()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(0)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->applyRotat(radAngleX, radAngleY, radAngleZ);
}

VVOID Humanoid::applyChangeToRightElbowJoint(Vector3 angles)
{
	FFLOAT radAngleX = DegToRad(angles.getX());
	FFLOAT radAngleY = DegToRad(angles.getY());
	FFLOAT radAngleZ = DegToRad(angles.getZ());

	// apply the rotation to the right elbow
	SG->Root()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->applyRotat(radAngleX, radAngleY, radAngleZ);
}

VVOID Humanoid::applyChangeToLeftHipJoint(Vector3 angles)
{
	FFLOAT radAngleX = DegToRad(angles.getX());
	FFLOAT radAngleY = DegToRad(angles.getY());
	FFLOAT radAngleZ = DegToRad(angles.getZ());

	// apply the rotation to the left hip
	SG->Root()->getList().FirstData()->getList().researchByIndex(2)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->applyRotat(radAngleX, radAngleY, radAngleZ);
}

VVOID Humanoid::applyChangeToRightHipJoint(Vector3 angles)
{
	FFLOAT radAngleX = DegToRad(angles.getX());
	FFLOAT radAngleY = DegToRad(angles.getY());
	FFLOAT radAngleZ = DegToRad(angles.getZ());

	// apply the rotation to the right hip.
	SG->Root()->getList().FirstData()->getList().researchByIndex(2)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(2)->getList().FirstData()->getList().FirstData()->applyRotat(radAngleX, radAngleY, radAngleZ);
}

VVOID Humanoid::applyChangeToLeftKneeJoint(Vector3 angles)
{
	FFLOAT radAngleX = DegToRad(angles.getX());
	FFLOAT radAngleY = DegToRad(angles.getY());
	FFLOAT radAngleZ = DegToRad(angles.getZ());

	// apply the rotation to the left knee.
	SG->Root()->getList().FirstData()->getList().researchByIndex(2)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->applyRotat(radAngleX, radAngleY, radAngleZ);
}

VVOID Humanoid::applyChangeToRightKneeJoint(Vector3 angles)
{
	FFLOAT radAngleX = DegToRad(angles.getX());
	FFLOAT radAngleY = DegToRad(angles.getY());
	FFLOAT radAngleZ = DegToRad(angles.getZ());

	// apply the rotation to the right knee.
	SG->Root()->getList().FirstData()->getList().researchByIndex(2)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(2)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->getList().researchByIndex(1)->getList().FirstData()->getList().FirstData()->applyRotat(radAngleX, radAngleY, radAngleZ);
}

VVOID Humanoid::rotate(const Vector3& rotat)
{
	// rotate the human.
	SG->Root()->getList().FirstData()->applyRotat(rotat.getX(), rotat.getY(), rotat.getZ());
}

VVOID Humanoid::rotateCam(Vector3 rotCam)
{
	// we just care about two components even if actually only the Y rotation is neede.
	FFLOAT xAngle = m_RotationTrack.getX();
	FFLOAT yAngle = m_RotationTrack.getY();
	xAngle += rotCam.getX();
	yAngle += rotCam.getY();
	m_RotationTrack.x(xAngle);
	m_RotationTrack.y(yAngle);
	SG->Root()->getList().FirstData()->applyRotat(xAngle, yAngle, DegToRad(180.0f));
}

VVOID Humanoid::rescale(Vector3 scale)
{
	// apply the scale at every child after the master rotation
	// will resize the entire body.
	for(UINT currGroup = 0; currGroup < 3; currGroup++)
		SG->Root()->getList().FirstData()->getList().researchByIndex(currGroup)->applyScale(scale.getX(), scale.getY(), scale.getZ());
}

VVOID Humanoid::resetPosition(const Vector3& pos)
{
	// reset the position of the humanoid.
	SG->Root()->setTranslation(pos.getX(), pos.getY(), pos.getZ());
	m_baseInfo.Pos = pos;
}
