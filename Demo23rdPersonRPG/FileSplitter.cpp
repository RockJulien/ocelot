#include "FileSplitter.h"

using namespace std;

FileSplitter::~FileSplitter()
{

}

FileSplitter* FileSplitter::instance()
{
	static FileSplitter instance;

	return &instance;
}

VVOID  FileSplitter::splitWithBlanks(vector<string>& result, string toSplit)
{
	istringstream iSS(toSplit);
	copy(istream_iterator<string>(iSS), istream_iterator<string>(), back_inserter<vector<string>>(result));
}

VVOID  FileSplitter::splitWithDelimiter(vector<wstring>& result, const wstring& toSplit, const CCHAR& delimiter)
{
	wstring::size_type start = 0;
	wstring::size_type stop  = toSplit.find(delimiter);

	while(stop != wstring::npos)
	{
		result.push_back(toSplit.substr(start, (stop - start)));
		start = stop++;
		stop  = toSplit.find(delimiter, stop);

		if(stop == wstring::npos)
			result.push_back(toSplit.substr(start, toSplit.length()));
	}
}

VVOID  FileSplitter::searchRightInformationInFile(const wstring& fileName, const wstring& toSearch, vector<wstring>& info, UINT linesToPass)
{
	const wstring path = L"C:/Users/l000460b/Desktop/Sample Project/Terrain/";
	wstring finalName  = /*path +*/ fileName;
	wifstream theStream(finalName.c_str());

	wstring temp;
	while(getline(theStream, temp))
	{
		if(temp == toSearch)
			break;
	}

	// go to the line expected when right position founded.
	if(linesToPass != 0)
		// reachs the right line and grabs info
		for(UINT currLine = 0; currLine < linesToPass; currLine++)
			getline(theStream, temp);
	
	// split information
	splitWithDelimiter(info, temp, ' ');

	// close when finished!!!
	theStream.close();
}

VVOID  FileSplitter::eraseBlanks(wstring& noMoreBlanks)
{
	noMoreBlanks.erase(remove(noMoreBlanks.begin(), noMoreBlanks.end(), ' '), noMoreBlanks.end());
}
