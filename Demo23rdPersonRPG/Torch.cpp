#include "Torch.h"

using namespace Ocelot;

Torch::Torch()
{
	stick = nullptr;
}

Torch::~Torch()
{
}

VVOID Torch::Release()
{
	
}

VVOID Torch::CreateTorch(ID3D10Device* p_device, OcelotCamera* _camera, OcelotLight* pLight, OcelotFog* pFog)
{
	cam = _camera;

	CylinderInfo cInfo( p_device, _camera, L"chene.jpg", L"..\\Effects\\BumpMapping.fx", pLight, pFog, Vector3(0.0f, 0.0f, 0.0f), 1, 5 );
	
	stick = new Cylinder(cInfo);
	stick->createInstance();
	stick->SetPhysicsUse(false);
	
	ParticleSystemInfo fInfo;
	fInfo.device = p_device;
	fInfo.camera = _camera;

	fInfo.maxnumparticles = 500;
	fInfo.spread = 1.0f;

	std::string firefileName = "flame.dds";
	std::wstring firefileNameW(firefileName.begin(), firefileName.end());
	fInfo.texture = ResFactory->GetResource(firefileNameW);

	flame = ParticleFXFactory->CreateParticleEffect( PE_FIRE, fInfo );

	flame->SetMeshIndex(-1);
	flame->SetEmitterOffset(Vector3(0.0f, cInfo.Height() * 0.5f, 0.0f));
	flame->SetAcceleration(Vector3(0.0f, 7.8f, 0.0f));
	flame->SetWindAmplitude(Vector3(-5.5f, 1.0f, 1.0f));
	flame->SetWindPeriodicity(Vector3(0.25f, 0.0f, 0.0f));

	ParticleSystemInfo sInfo;
	sInfo.device = p_device;
	sInfo.camera = _camera;

	sInfo.maxnumparticles = 500;
	sInfo.spread = 0.4f;

	std::string smokefileName = "smoke.dds";
	std::wstring smokefileNameW(smokefileName.begin(), smokefileName.end());
	sInfo.texture = ResFactory->GetResource(smokefileNameW);

	smoke = ParticleFXFactory->CreateParticleEffect(PE_SMOKE, sInfo);

	smoke->SetMeshIndex(-1);
	smoke->SetEmitterOffset(Vector3(0.0f, cInfo.Height() * 0.5f, 0.0f));
	smoke->SetEmitDirection(Vector3(0.0f, 1.0f, 0.0f));
	smoke->SetAcceleration(Vector3(0.0f, 7.8f, 0.0f));
	smoke->SetWindAmplitude(Vector3(-0.5f, 1.0f, 1.0f));
	smoke->SetWindPeriodicity(Vector3(0.250f, 1.0f, 0.0f));
}

VVOID Torch::Update(const FFLOAT delta, const FFLOAT gameTime)
{
	stick->update(delta);
	if(showEffects)
	{
		smoke->Update(delta, gameTime);
		flame->Update(delta, gameTime);
	}
}

VVOID Torch::Render()
{
	stick->renderMesh();

	cam->computeFrustumPlane();

	// frustrum culling 
	Vector3 lFlamePos = flame->GetEmitterPosition();
	showEffects = cam->cullMesh(lFlamePos.getX(), lFlamePos.getY(), lFlamePos.getZ(), 2.0f);

	if
		( showEffects )
	{
		smoke->Render();
		flame->Render();
	}
}