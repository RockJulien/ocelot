#ifndef DEF_MOVINGENTITY_H
#define DEF_MOVINGENTITY_H

#include "BaseGameEntity.h"

const FFLOAT LifeAllowed = 100.0f;
const FFLOAT SpeedMax    = 50.0f;
const FFLOAT ForceMax    = 50.0f;

class MovingEntity : public BaseGameEntity
{
protected:

		// Attributes
		FFLOAT   m_fLife;
		FFLOAT   m_fSpeedLimit;
		FFLOAT   m_fForceLimit;
		Vector3 m_vDirection;
		Vector3 m_vSideVector;
		Vector3 m_vVelocity;
		Vector3 m_vAcceleration;
		Vector3 m_vTargetPos;

public:

		// Constructor & Destructor
		MovingEntity();
		MovingEntity(BaseEntityInfo inf);
		~MovingEntity();

		// Methods
virtual VVOID createEntity();
virtual VVOID update(FFLOAT deltaTime);
virtual VVOID render();

		// Accessors
inline  FFLOAT   Life() const { return m_fLife;}
inline  FFLOAT   SpeedLim() const { return m_fSpeedLimit;}
inline  FFLOAT   ForceLim() const { return m_fForceLimit;}
inline  Vector3 Dir() const { return m_vDirection;}
inline  Vector3 Sid() const { return m_vSideVector;}
inline  Vector3 Vel() const { return m_vVelocity;}
inline  Vector3 Acc() const { return m_vAcceleration;}
inline  Vector3 Target() const { return m_vTargetPos;}
inline  VVOID    Repair(FFLOAT life) { m_fLife = life;}
inline  VVOID    SetSpeedLim(FFLOAT newSpeedLim) { m_fSpeedLimit = newSpeedLim;}
inline  VVOID    SetForceLim(FFLOAT newForceLim) { m_fForceLimit = newForceLim;}
inline  VVOID    SetDir(Vector3 newDir) { m_vDirection = newDir; m_vSideVector = m_vDirection.Vec3CrossProduct(Vector3(0.0f, 1.0f, 0.0f));}
inline  VVOID    SetVel(Vector3 newVel) { m_vVelocity = newVel;}
inline  VVOID    SetAcc(Vector3 newAcc) { m_vAcceleration = newAcc;}
inline  VVOID    SetTargetPos(Vector3 target) { m_vTargetPos = target;}
};

#endif