#include "Main.h"
#include "Maths\Vector4.h"
#include "MeshEditor\MeshEditor.h"
#include "Utilities\PickPair.h"
#include "Geometry\Cylinder.h"
#include "Geometry\Sphere.h"
#include "Geometry\Terrain.h"
#include "Geometry\Cube.h"
#include "Geometry\PyraCylinder.h"

using namespace Ocelot;

BIGINT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, BIGINT nShowCmd)
{
	ThirdPersonRPGDemo app;
	app.initApp(hInstance, SW_SHOW, L"Third Person RPG Demo");
	
	return app.run();
}

ThirdPersonRPGDemo::ThirdPersonRPGDemo()
	: DXOcelotApp(), mCamera(NULL), mController(NULL)
{	 
	srand(static_cast<UBIGINT>(time(0)));
}

ThirdPersonRPGDemo::~ThirdPersonRPGDemo()
{
	clean3DDevice();

	BIGINT lNumTorches = (BIGINT)torchVector.size();
	for 
		( BIGINT i = 0; i < lNumTorches; ++i )
	{
		torchVector[i]->Release();
	}
	torchVector.clear();

	tFile.close();

	if(InMng)
		InMng->release();

	if(AudioMng)
		AudioMng->release();
}

BIGINT ThirdPersonRPGDemo::run()
{
	MSG msg = {0};

	m_timer.resetTimer();

	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))      
		{												  
			// translate keystroke messages into the right format
			TranslateMessage(&msg);

			// send the message to the WindowProc
			DispatchMessage(&msg);
		}
		else
		{
			if(m_bPaused)
			{
				Sleep(50);
			}
			else
			{
				m_timer.tick();
				update(m_timer.getDeltaTime());
				renderFrame();
			}
		}
	}
	// clean up DirectX COM
	clean3DDevice();
	return msg.wParam;
}

VVOID ThirdPersonRPGDemo::update(FFLOAT deltaTime)
{
	DXOcelotApp::update(deltaTime);

	InMng->update();

	handleInput();

	HR(AudioMng->update(deltaTime));
	
	OcelotCameraInfo lInfo = this->mCamera->Info();
	Vector3 camPos = lInfo.Eye();
	BIGINT corner = WhichCorner(camPos.getX(), camPos.getZ());
	FFLOAT camHeight;
	FFLOAT xPosG = corner % 2 == 0 ? camPos.getX() + (terrains[corner]->getTerrainWidth() * 0.5f) : camPos.getX() - (terrains[corner]->getTerrainWidth() * 0.5f);
	FFLOAT zPosG = corner < 2 ?  camPos.getZ() + (terrains[corner]->getTerrainDepth() * 0.5f) : camPos.getZ() - (terrains[corner]->getTerrainDepth() * 0.5f);
	camHeight = terrains[corner]->heightAtPosition(xPosG, zPosG);
	if(!_finite(camHeight))
	{
		camHeight = previousHeight;
	}
	if(!flyCam)
	{
		camPos.y(camHeight + 10.0f);
	}
	lInfo.SetEye(camPos);
	this->mCamera->SetInfo(lInfo);
	
	// Update the controller
	this->mController->update( deltaTime );

	previousHeight = camHeight;
	//3D sound can only handle one sound position at a time so find the torch
	//closest to the player and set that to be the sound emitter
	BIGINT numTorches = (BIGINT)torchPositions.size();
	BIGINT closestIndex = 0;
	FFLOAT closestDistance = FLT_MAX;
	for(BIGINT i = 0; i < numTorches; ++i)
	{
		FFLOAT dist = (torchPositions[i] - camPos).Vec3LengthSquared();
		if(dist < closestDistance)
		{
			closestDistance = dist;
			closestIndex = i;
		}
	}

	lInfo = this->mCamera->Info();
	Vector3 newPos = lInfo.Eye();
	m_player->setPos(Vector3(-newPos.getX(), -newPos.getY() + 5.0f, -newPos.getZ()));
	
	m_player->setInMove(true);
	// update the player.
	m_player->update(deltaTime);
	
	AudioMng->Device()[1]->setSoundPosition(torchPositions[closestIndex], 0);
	AudioMng->Device()[1]->setListener(newPos, Vector3(0.0f), Vector3(0.0f), Vector3(0.0f));

	BIGINT numStaticMeshes = static_meshes.size();
	for(BIGINT currMesh = 0; currMesh < numStaticMeshes; currMesh++)
	{
		static_meshes[currMesh]->update(deltaTime);
	}

	BIGINT numParticleSystems = particleSystems.size();
	for(BIGINT i = 0; i < numParticleSystems; ++i)
	{
		IParticleSystem* currentPS = particleSystems[i];
		BIGINT mIndex = currentPS->GetMeshIndex();
		currentPS->SetEmitterPosition((mIndex == -1? particleEffectPositions[i] : static_meshes[mIndex]->Position()) + currentPS->GetEmitterOffset());

		FFLOAT dist = newPos.Vec3DistanceBetween(currentPS->GetEmitterPosition());
		if( dist > 100.0f) // create a radius checking if player is near for a emmitter source for drawing particles.
			showParticles[i] = false;
		else
			showParticles[i] = true;
		
		if(showParticles[i])
		{
			currentPS->Update(deltaTime, m_timer.getGameTime());
		}
	}

	
	for(BIGINT i = 0; i < numTorches; ++i)
	{
		torchVector[i]->Update(deltaTime, m_timer.getGameTime());
	}
}

VVOID ThirdPersonRPGDemo::renderFrame()
{
	DXOcelotApp::renderFrame();

	m_device->OMSetDepthStencilState(0, 0);
	FFLOAT blendFactor[] = {0.0f, 0.0f, 0.0f, 0.0f};
	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	std::vector<IOcelotMesh*>::iterator end = static_meshes.end();
	std::vector<IOcelotMesh*>::iterator it;
	for(it = static_meshes.begin(); it != end; ++it)
	{
		(*it)->renderMesh();
	}

	// render the player
	m_player->render();

	for(BIGINT i = 0; i < (BIGINT)particleSystems.size(); ++i)
	{
		if(showParticles[i])
			particleSystems[i]->Render();
	}

	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	
	BIGINT numTorches = (BIGINT)torchVector.size();
	for (BIGINT i = 0; i < numTorches; ++i)
	{
		torchVector[i]->Render();
	}
	
	
	m_device->OMSetBlendState(0, blendFactor, 0xffffffff);

	/*
	RECT R = {10,10,0,0};
	m_device->RSSetState(0);
	m_font->DrawTextW(0, scoreString.c_str(), -1, &R, DT_NOCLIP, DXColor::OcelotColToDXCol(OcelotColor::WHITE)); 
	*/
	m_swapChain->Present(1,0);   
}

VVOID ThirdPersonRPGDemo::initApp(HINSTANCE pInstance, BIGINT pShowCmd, LPWSTR pWindowName)
{
	DXOcelotApp::initApp(pInstance, pShowCmd, pWindowName);

	tFile.open("pos.txt");
	flyCam = false;

	// init the audio device.
	// for now, add whatever song you wnat here. just the short name
	// by putting them first in the Resources folder.
	AudioMng->initialize(m_hAppInstance);

	// init the Resource Manager
	ResFactory->SetDevice(m_device);
	HR(ResFactory->CreateTextures());

	// Initialize the camera.
	OcelotCameraInfo lCamInfo(Vector3(-74.47f, 10.0f, 219.28f),
							  Vector3(75.0f, 0.0f, 200.0f),
							  (FFLOAT)m_iWindowWidth / m_iWindowHeight,
							  400.0f);
	this->mCamera = new OcelotCamera(lCamInfo);

	// And create the controller
	this->mController = ControlFact->createController(this->mCamera, CAMERA_CONTROLLER_FLYER);
	
	WSTRING lDefTextPath[3];
	lDefTextPath[0] = L"Dirt.jpg";
	lDefTextPath[1] = L"Grass.jpg";
	lDefTextPath[2] = L"Rock.jpg";

	// Create common textures for saving memory (will be the same).
	this->mTerrainMaterial = new TriPlanarBumpMaterial();
	ResFactory->LoadTriPlanarMaterial( lDefTextPath, *this->mTerrainMaterial );

	// Create the light  
	mLight = new OcelotLight(Vector3(-300.0f, 100.0f, -180.0f), // eye    
							 Vector3(0.0f, 0.0f, 1.0f),   // dir    
							 Vector3(1.0f, 0.0f, 0.0f),   // atten  
							 OcelotColor::DARKGREY,		  // ambient   
							 OcelotColor::LIGHTGREY,      // diffuse  
							 OcelotColor::GREY,			  // spec  
							 1000.0f,					  // range 
							 64.0f,						  // spotPow 
							 1.0f);						  // specPow
	
	// Create the fog.
	mFog = new OcelotFog(5.0f, 
						 140.0f);

	CubeMapInfo infoCM(this->m_device, 
					   this->mCamera, 
					   5000.0f, 
					   L"WaterHill.dds", 
					   L"..\\Effects\\CubeMap.fx");
	infoCM.SetTesselation( 2 );

	static_meshes.push_back(new CubeMap(infoCM));

	OcelotXMLFile xmlfile("XML\\OcelotGameData.xml");
	try
	{
		rpgGameData.parse<rapidxml::parse_no_data_nodes>(xmlfile.data());
	}
	catch(rapidxml::parse_error e)
	{}
	LoadXMLMeshData(rpgGameData);

	std::vector<IOcelotMesh*>::iterator it;
	std::vector<IOcelotMesh*>::iterator end = static_meshes.end();
	for(it = static_meshes.begin(); it != end; ++it)
		(*it)->createInstance();

	loadOtherXMLData();

	// init the player.
	initTheGuy();

	AudioMng->createSound(L"ariely.wav", false, true);
	AudioMng->createSound(L"firesound.wav", true, true);
	AudioMng->Device()[1]->setSoundMaxDistance(15.0f, 0);

	// show the window
	ShowWindow(m_mainWindowInstance, SW_SHOW);
}

VVOID ThirdPersonRPGDemo::initTheGuy()
{
	HUMAN_ATTRIBUTES HA;
	CylinderInfo CI( m_device, mCamera, L"floor.jpg", L"..\\Effects\\BumpMapping.fx", this->mLight, this->mFog, Vector3(0.0f), 2, 5 );
	SphereInfo SI( m_device, mCamera, L"chene.jpg", L"..\\Effects\\BumpMapping.fx", this->mLight, this->mFog, Vector3(0.0f), 4 );
	
	//SI.scale         = Vector3(1.0f);
	HA.baseInfo.BRad = 5.0f;
	HA.baseInfo.Mass = 1.0f;
	HA.headInfo          = SI;
	HA.leftShoulderInfo  = CI;
	HA.rightShoulderInfo = CI;
	SI.SetRadius( 2.5f );  // rad for joints
	SI.SetTexturePath( L"floor.jpg" );   // texture for joints (same than arms).
	HA.leftShoulderJointInfo  = SI;
	HA.rightShoulderJointInfo = SI;
	HA.leftArmInfo            = CI;
	HA.rightArmInfo           = CI;
	HA.leftArmJointInfo       = SI;
	HA.rightArmJointInfo      = SI;
	HA.leftForwardArmInfo     = CI;
	HA.rightForwardArmInfo    = CI;
	HA.trunkTopInfo           = CI;
	HA.middleTrunkJointInfo   = SI;
	HA.trunkBottomInfo        = CI;
	HA.bottomTrunkJointInfo   = SI;
	CI.SetTexturePath( L"snow.dds" );  // change the texture for the legs
	SI.SetTexturePath( L"snow.dds" );  // same than legs.
	HA.hipInfo                = CI;
	HA.leftHipsJointInfo      = SI;
	HA.rightHipsJointInfo     = SI;
	HA.topLeftLegInfo         = CI;
	HA.topRightLegInfo        = CI;
	HA.leftLegJointInfo       = SI;
	HA.rightLegJointInfo      = SI;
	HA.bottomLeftLegInfo      = CI;
	HA.bottomRightLegInfo     = CI;

	m_player = new Humanoid(HA);
	m_player->createEntity();
	m_player->resetPosition(-mCamera->Info().Eye()); // start pos for test.
	m_player->rotate(Vector3(0.0f, 0.0f, DegToRad(180.0f))); // correct the inversion of the world due to scene graph transformation;
	m_player->rescale(Vector3(8.0f));    // remember the inversion, rescale in smaller size means increase these values.
	m_player->setInMove(true);
}

VVOID ThirdPersonRPGDemo::handleInput()
{
	//Switch between a camera that follows the terrain to one that flys freely
	if(InMng->isPressedAndLocked(OCELOT_F))
	{
		flyCam = !flyCam;
	}
	//Just for debugging, press space to store your current x,z position
	//into a file called pos.txt.
	//Use those coords to know where to place the torches
	//Easiest to do it this way rather than just trying random positions till they
	//look nice.
	if(InMng->isPressedAndLocked(OCELOT_SPACE))
	{
		OcelotCameraInfo lInfo = mCamera->Info();
		tFile << lInfo.Eye().getX() << "," << lInfo.Eye().getZ() << "\n";
	}

	if(InMng->isPressed(Keyboard, OCELOT_UP))
	{
		m_player->setInMove(true);
		m_player->walk();
		this->mController->move(Direction::Forward);
	}

	if(InMng->isPressed(Keyboard, OCELOT_LEFT))
	{
		m_player->setInMove(true);
		m_player->walk();
		this->mController->move(Direction::LeftSide);
	}
	
	if(InMng->isPressed(Keyboard, OCELOT_RIGHT))
	{
		m_player->setInMove(true);
		m_player->walk();
		this->mController->move(Direction::RightSide);
	}

	if(InMng->isPressed(Keyboard, OCELOT_DOWN))
	{
		m_player->setInMove(true);
		m_player->walk();
		this->mController->move(Direction::Backward);
	}
}

VVOID ThirdPersonRPGDemo::clean3DDevice()
{
	DXOcelotApp::clean3DDevice();

	DeletePointer(this->mCamera);
	DeletePointer(this->mController);
	DeletePointer(this->m_player);
}

VVOID ThirdPersonRPGDemo::resize()
{
	DXOcelotApp::resize();

	// refresh the aspect of the camera if the
	// window is resized.
	if
		( this->mCamera )
	{
		FFLOAT lAspect = (FFLOAT)m_iWindowWidth / m_iWindowHeight;
		OcelotCameraInfo lInfo = this->mCamera->Info();
		lInfo.SetAspect(lAspect);  // aspect ratio
		this->mCamera->SetInfo(lInfo);
	}
}

VVOID ThirdPersonRPGDemo::loadOtherXMLData()
{
	LoadXMLParticleEffectsData(rpgGameData);
	LoadXMLTorches(rpgGameData);
}

LRESULT ThirdPersonRPGDemo::AppProc(UINT message,WPARAM wParam,LPARAM lParam)
{
	Vector2 mousePosition;

	FFLOAT dx = 0.0f;
	FFLOAT dy = 0.0f;

	switch(message)
	{
	case WM_LBUTTONDOWN:
		if(wParam & MK_LBUTTON)
		{
			SetCapture(m_mainWindowInstance);

			m_vOldMousePos.x((FFLOAT)LOWORD(lParam));
			m_vOldMousePos.y((FFLOAT)HIWORD(lParam));
		}
		return 0;
		break;

	case WM_RBUTTONDOWN:
		return 0;
		break;

	case WM_LBUTTONUP:
		ReleaseCapture();
		return 0;
		break;

	case WM_MOUSEMOVE:
		if(wParam & MK_LBUTTON)
		{
			mousePosition.x((FFLOAT)LOWORD(lParam));
			mousePosition.y((FFLOAT)HIWORD(lParam));

			FFLOAT lToRad = PI / 180.0f;
			// Make each pixel correspond to a quarter of a degree.
			FFLOAT lDx = (0.25f * static_cast<FFLOAT>(mousePosition.getX() - this->m_vOldMousePos.getX())) * lToRad;
			FFLOAT lDy = (0.25f * static_cast<FFLOAT>(mousePosition.getY() - this->m_vOldMousePos.getY())) * lToRad;

			// add extra camera controler stuff here
			this->mController->pitch(lDy);
			this->mController->yaw(lDx);

			m_vOldMousePos = mousePosition;

			m_player->rotateCam(Vector3(0.0f, -lDx, 0.0f));
		}
		return 0;
	}
	return DXOcelotApp::AppProc(message, wParam, lParam);
}

IOcelotMesh* ThirdPersonRPGDemo::underMouse(Vector2 curs)
{
	// algorithm which determine which mesh is under 
	// the cursor of the mouse by unprojecting.
	Matrix4x4 lMatProj = mCamera->Proj();
	Matrix4x4 lMatView = mCamera->View();

	Vector3 lEye = this->mCamera->Info().Eye();

	// determine the ray thanks to screen position
	// in view space.
	FFLOAT lDirX = (+2.0f * curs.getX() / m_iWindowWidth - 1.0f) / lMatProj.get_11();
	FFLOAT lDirY = (-2.0f * curs.getY() / m_iWindowHeight + 1.0f) / lMatProj.get_22();

	Vector3 lOrigine(0.0f, 0.0f, 0.0f);
	Vector3 lRayDir(lDirX, lDirY, 1.0f);

	// Get the inverse view for transforming mesh world to local.
	Matrix4x4 lMatViewInverse;
	lMatView.Matrix4x4Inverse(lMatViewInverse);

	const IOcelotMesh* lClosest = NULL;
	size_t lCount = this->static_meshes.size();
	// transform from world space to local space meshes
	if
		(lCount != 0)
	{
		std::vector<PickPair> lMatches;
		for
			(UBIGINT lCurrMesh = 0; lCurrMesh < (UBIGINT)lCount - 1; lCurrMesh++)
		{
			IOcelotMesh* lCurrMeshInstance = this->static_meshes[lCurrMesh];

			Matrix4x4 lCurrMeshInvWorld;
			const Matrix4x4* lCurrMeshWorld = lCurrMeshInstance->World();
			lCurrMeshWorld->Matrix4x4Inverse(lCurrMeshInvWorld);

			Matrix4x4 lCurrMeshLocal;
			lMatViewInverse.Matrix4x4Mutliply(lCurrMeshLocal, lCurrMeshInvWorld);

			// apply the local world to origine and rayDir vec.
			Vector3 lLocalOrigine = Vector3::Vec4ToVec3(lOrigine.Vec3VecMatrixProduct(lCurrMeshLocal));
			Vector3 lLocalRayDir = Vector3::Vec4ToVec3(lRayDir.Vec3VecMatrixTransformNormal(lCurrMeshLocal));
			lLocalRayDir.Vec3Normalise();

			Ray lRay(lLocalOrigine, lLocalRayDir);

			// now determine which mesh this ray intersect
			// and return it if exists.
			const IRayIntersectable* lIntersectable = lCurrMeshInstance->LocalBounds();
			if
				(lIntersectable != NULL &&
				lIntersectable->IntersectsAndFindPoint(lRay))
			{
				lMatches.push_back(PickPair(lCurrMeshInstance, lRay.GetIntersectionPoint()));
			}
		}

		FFLOAT lMinDistance = MaxFloat;
		// Retrieve the closest from the camera and return it.
		std::vector<PickPair>::const_iterator lIter = lMatches.begin();
		for
			(; lIter != lMatches.end(); lIter++)
		{
			FFLOAT lCurrDistance = lEye.Vec3DistanceBetween(lIter->IntersectedPoint());
			if
				(lMinDistance > lCurrDistance)
			{
				lMinDistance = lCurrDistance;
				lClosest = lIter->Picked();
			}
		}
	}

	return const_cast<IOcelotMesh*>(lClosest);
}

VVOID ThirdPersonRPGDemo::ResolveContacts(const FFLOAT deltaTime)
{
	/*
	contacts.clear();
	for (BIGINT i = 0; i < (BIGINT)moving_meshes.size() - 1; ++i)
	{
		IOcelotMesh* objA = moving_meshes[i];
		for(BIGINT j = i + 1; j < (BIGINT)moving_meshes.size(); ++j)
		{
			IOcelotMesh* objB = moving_meshes[j];

			if(!objA->UsesPhysics() && !objB->UsesPhysics())
			{
				continue;
			}

			if(!objA->setWorldBounds() || !objB->setWorldBounds())
			{
				continue;
			}

			AABBMinMax3D boundsA(objA->GetMinW(), objA->GetMaxW());
			AABBMinMax3D boundsB(objB->GetMinW(), objB->GetMaxW());

			if(boundsA.Intersects(boundsB))
			{
				Vector3 contactNormal = (objA->getPosition() - objB->getPosition()).Vec3Normalise();
				if(!gameFinished) score += 1;
				AudioMng->restartASound(L"ping.wav");
				contacts.push_back(new ParticleContact(ParticleContact(objA->GetParticle(), objB->GetParticle(), 0.6f, contactNormal, boundsA.CalculatePenetration(boundsB) * deltaTime)));
			}
		}
	}
	contactResolver->ResolveContacts(contacts, deltaTime);
	*/
}

VVOID ThirdPersonRPGDemo::LoadXMLMeshData(OcelotXMLDoc& doc)
{
	OcelotXMLNode *root = doc.first_node("ocelot");
	OcelotXMLNode *meshesData = root->first_node("meshes");

	BBOOL enabled = atoi(meshesData->first_attribute("enabled")->value()) == 1;
	if(!enabled) 
	{
		return;
	}

	for(OcelotXMLNode *currentMesh = meshesData->first_node(); currentMesh; currentMesh = currentMesh->next_sibling())
	{
		BBOOL enabled = atoi(currentMesh->first_attribute("enabled")->value()) == 1;
		if(!enabled) 
		{
			continue;
		}

		string type = currentMesh->first_node()->value();
		
		if(type == "terrain")
		{
			BIGINT rows = atoi(currentMesh->first_attribute("rows")->value());
			BIGINT cols = atoi(currentMesh->first_attribute("cols")->value());
			FFLOAT width = (FFLOAT)atof(currentMesh->first_attribute("width")->value());
			FFLOAT depth = (FFLOAT)atof(currentMesh->first_attribute("depth")->value());
			string fileName = currentMesh->first_node("texture")->first_attribute("name")->value();
			wstring fileNameW(fileName.begin(), fileName.end());
			std::string mapName = currentMesh->first_node("heightMap")->first_attribute("name")->value();
		    std:wstring lWMapName(mapName.begin(), mapName.end());
			BBOOL useMap = atoi(currentMesh->first_node("useHeightMap")->value()) == 1;
			BBOOL useMultiTexturing = atoi(currentMesh->first_node("useMultiTexture")->value()) == 1;

			TerrainInfo TI( this->m_device,
							this->mCamera,
							L"..\\Effects\\TriPlanarBumpMapping.fx",
							this->mLight,
							this->mFog,
							this->mTerrainMaterial,
							Vector3(0.0f),
							cols,
							rows,
							depth,
							width);

			/*if(useMultiTexturing)
			{	
				std::vector<wstring> names;
				UINT count = 0;
				for(OcelotXMLNode *currentTexture = currentMesh->first_node("multiTextures")->first_node("texture"); currentTexture; currentTexture = currentTexture->next_sibling())
				{
					string fileName = currentTexture->first_attribute("name")->value();
					wstring fileNameW(fileName.begin(), fileName.end());
					names.push_back(fileNameW);
					count++;
				}
				TI.nbTexture = count;
				TI.textureNames = names;
			}*/

			static_meshes.push_back(new Terrain(TI));
			static_meshes.back()->SetPhysicsUse(atoi(currentMesh->first_node("physics")->value()) == 1);
			terrains.push_back(reinterpret_cast<Terrain*>(static_meshes.back()));
			if(terrains.back() && useMap)
			{
				Terrain* lBack = terrains.back();
				lBack->setHeightMapUse(true);
				lBack->setHeightMapName(lWMapName);
				Vector3 signs = OcelotXML::ExtractVector3(currentMesh->first_node("worldTranslation"), "x", "y", "z");
				IMeshEditor* lEditor = lBack->StartEdition();
				lEditor->TranslateX(signs.getX() * ((width - 1.0f) * 0.5f));
				lEditor->TranslateY(signs.getY() * 1.0f);
				lEditor->TranslateZ(signs.getZ() * ((depth - 1.0f) * 0.5f));
				DeletePointer(lEditor);
			}
			//terrains.back()->setMultiTextured(useMultiTexturing);
		} 
	}
}

VVOID ThirdPersonRPGDemo::LoadXMLParticleEffectsData(OcelotXMLDoc& doc)
{
	OcelotXMLNode *root = doc.first_node("ocelot");
	OcelotXMLNode *particleSystemsData = root->first_node("particleSystems");

	BBOOL enabled = atoi(particleSystemsData->first_attribute("enabled")->value()) == 1;
	if(!enabled) 
	{
		return;
	}

	for(OcelotXMLNode *currentMesh = particleSystemsData->first_node(); currentMesh; currentMesh = currentMesh->next_sibling())
	{
		BBOOL enabled = atoi(currentMesh->first_attribute("enabled")->value()) == 1;
		if(!enabled) 
		{
			continue;
		}

		ParticleSystemInfo info;

		info.device = m_device;
		info.camera = mCamera;

		info.maxnumparticles = atoi(currentMesh->first_attribute("numParticles")->value());
		info.spread = (FFLOAT)atof(currentMesh->first_attribute("spread")->value());

		std::string fileName = currentMesh->first_attribute("textureFileName")->value();
		std::wstring fileNameW(fileName.begin(), fileName.end());
		info.texture = ResFactory->GetResource(fileNameW);

		std::string typeS = currentMesh->first_node("type")->value();
		BIGINT type;
		if(typeS == "rain")
		{
			type = PE_RAIN;
		} else if(typeS == "snow")
		{
			type = PE_SNOW;
		} else if(typeS == "smoke")
		{
			type = PE_SMOKE;
		} else if(typeS == "fire")
		{
			type = PE_FIRE;
		}

		particleSystems.push_back(ParticleFXFactory->CreateParticleEffect(ParticleEffectType(type), info));

		IParticleSystem* ps = particleSystems.back();
		ps->SetMeshIndex(atoi(currentMesh->first_node("emitterPosition")->first_attribute("meshIndex")->value()));
		ps->SetEmitterOffset(OcelotXML::ExtractVector3(currentMesh->first_node("emitterPosition")->first_node("offset"), "x", "y", "z"));
		ps->SetAcceleration(OcelotXML::ExtractVector3(currentMesh->first_node("acceleration"), "x","y","z"));
		ps->SetWindAmplitude(OcelotXML::ExtractVector3(currentMesh->first_node("windForce")->first_node("amplitude"), "x", "y", "z"));
		ps->SetWindPeriodicity(OcelotXML::ExtractVector3(currentMesh->first_node("windForce")->first_node("periodicity"), "x", "y", "z"));
		particleEffectPositions.push_back(ps->GetEmitterOffset());

		showParticles.push_back(true);
	}
}

VVOID ThirdPersonRPGDemo::LoadXMLTorches(OcelotXMLDoc& doc)
{
	OcelotXMLNode *root = doc.first_node("ocelot");
	OcelotXMLNode *torchData = root->first_node("torches");

	
	for(OcelotXMLNode *currentMesh = torchData->first_node("torchObject"); currentMesh; currentMesh = currentMesh->next_sibling())
	{
		Vector3 pos = OcelotXML::ExtractVector3(currentMesh->first_node("position"), "x","y","z");
		BIGINT corner = WhichCorner(pos.getX(), pos.getZ());
		FFLOAT xPosG = corner % 2 == 0 ? pos.getX() + (terrains[corner]->getTerrainWidth() * 0.5f) : pos.getX() - (terrains[corner]->getTerrainWidth() * 0.5f);
		FFLOAT zPosG = corner < 2 ?  pos.getZ() + (terrains[corner]->getTerrainDepth() * 0.5f) : pos.getZ() - (terrains[corner]->getTerrainDepth() * 0.5f);
		FFLOAT torchHeight = terrains[corner]->heightAtPosition(xPosG, zPosG);
		pos.y(torchHeight);
		torchPositions.push_back(pos);
	}

	
	BIGINT numTorches = (BIGINT)torchPositions.size();
	torchVector.reserve(numTorches);
	for(BIGINT i = 0; i < numTorches; ++i)
	{
		torchVector.push_back(new Torch());
		torchVector.back()->CreateTorch(m_device, mCamera, mLight, mFog);
		torchVector.back()->SetPosition(torchPositions[i]);
	}
}

BIGINT ThirdPersonRPGDemo::WhichCorner(const FFLOAT posX, const FFLOAT posZ)
{
	if(posX <= 0.0f && posX > -(terrains[0]->getTerrainWidth()) && posZ <= 0.0f && posZ > -(terrains[0]->getTerrainDepth()))
	{
		return 0;
	}
	if(posX >= 0.0f && posX < terrains[1]->getTerrainWidth() && posZ <= 0.0f && posZ > -(terrains[1]->getTerrainDepth()))
	{
		return 1;
	}
	if(posX >= 0.0f && posX < terrains[3]->getTerrainWidth() && posZ >= 0.0f && posZ < terrains[3]->getTerrainDepth())
	{
		return 3;
	}
	if(posX <= 0.0f && posX > -(terrains[0]->getTerrainWidth()) && posZ >= 0.0f && posZ < terrains[3]->getTerrainDepth())
	{
		return 2;
	}
	return 4;
}