#ifndef DEF_FILESPLITTER_H
#define DEF_FILESPLITTER_H

#include <Windows.h>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include "..\Ocelot Editor\Maths\Utility.h"

#define FSMng FileSplitter::instance()

class FileSplitter
{
private:

		// Attributes


		// Constructor
		FileSplitter(){}

		// Constructor of cpy and assgnt
	    FileSplitter(const FileSplitter&);
		FileSplitter& operator = ( const FileSplitter&);
public:
	
		// Destructor
		~FileSplitter();

		// singleton
static  FileSplitter* instance();

		// Methods
		VVOID  splitWithBlanks(std::vector<std::string>& result, std::string toSplit);
		VVOID  splitWithDelimiter(std::vector<std::wstring>& result, const std::wstring& toSplit, const CCHAR& delimiter);
		VVOID  searchRightInformationInFile(const std::wstring& fileName, const std::wstring& toSearch, std::vector<std::wstring>& info, UINT linesToPass = 0);
		VVOID  eraseBlanks(std::wstring& noMoreBlanks);

		// accessors

};

#endif