#include "BaseGameEntity.h"

UINT BaseGameEntity::m_iNextValidId = 0; // set the static next valid id to 0 at the start.

BaseGameEntity::BaseGameEntity() : 
m_fElapsedTime(0.0f), m_baseInfo()
{
	setId(NID);
}

BaseGameEntity::BaseGameEntity(BaseEntityInfo inf) :
m_fElapsedTime(0.0f), m_baseInfo(inf)
{
	setId(NID);
}

BaseGameEntity::BaseGameEntity(UINT id) : 
m_fElapsedTime(0.0f), m_baseInfo()
{
	setId(id);
}

BaseGameEntity::~BaseGameEntity()
{

}

VVOID BaseGameEntity::setId(UINT theId)
{
	// assert that the new ID is greater or equal to the Next one.
	assert((theId >= m_iNextValidId) && "<BaseGameEntity::setId>: incompatible ID");

	// if Okay
	m_iId = theId;
	m_iNextValidId = m_iId + 1;
}