#include "MovingEntity.h"

MovingEntity::MovingEntity() :
BaseGameEntity(), m_fLife(LifeAllowed), m_fSpeedLimit(SpeedMax),
m_fForceLimit(ForceMax), m_vVelocity(0.0f), m_vAcceleration(0.0f),
m_vTargetPos(0.0f)
{
	
}

MovingEntity::MovingEntity(BaseEntityInfo inf) :
BaseGameEntity(inf), m_fLife(LifeAllowed), m_fSpeedLimit(SpeedMax),
m_fForceLimit(ForceMax), m_vVelocity(0.0f), m_vAcceleration(0.0f),
m_vTargetPos(inf.Pos)
{

}

MovingEntity::~MovingEntity()
{

}

VVOID MovingEntity::createEntity()
{

}

VVOID MovingEntity::update(FFLOAT deltaTime)
{
	m_fElapsedTime = deltaTime;
}

VVOID MovingEntity::render()
{

}