#ifndef DEF_MAIN_H
#define DEF_MAIN_H

#include "DirectXAPI\DXOcelotApp.h"
#include "..\Ocelot Editor\XML\OcelotRapidXML.h"
#include "Camera\OcelotCameraFactory.h"
#include "Geometry\CubeMap.h"
#include "Geometry\Terrain.h"
#include "AudioManager\AudioManager.h"
#include "InputManager\InputManager.h"
#include "Particle System\ParticleEffectsFactory.h"
#include "House.h"
#include "Humanoid.h"
#include "Torch.h"
#include <ctime>
#include <float.h>
#include <fstream>

using namespace Ocelot;

BIGINT WINAPI WinMain( HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	BIGINT nShowCmd );

class ThirdPersonRPGDemo : public DXOcelotApp
{
public:
	ThirdPersonRPGDemo();
	~ThirdPersonRPGDemo();

	VVOID update(FFLOAT deltaTime);

	VVOID renderFrame();
	VVOID initApp(HINSTANCE hInstance, BIGINT nShowCmd, LPWSTR windowName);
	VVOID clean3DDevice();
	VVOID resize();
	LRESULT AppProc(UINT message,WPARAM wParam,LPARAM lParam);

	VVOID loadOtherXMLData();
	VVOID initTheGuy();
	VVOID handleInput();
	BIGINT run();

	IOcelotMesh* underMouse(Vector2 curs);
	VVOID ResolveContacts(const FFLOAT delta);
	VVOID LoadXMLMeshData(OcelotXMLDoc& doc);
	VVOID LoadXMLParticleEffectsData(OcelotXMLDoc& doc);
	VVOID LoadXMLTorches(OcelotXMLDoc& doc);
	BIGINT WhichCorner(const FFLOAT posX, const FFLOAT posZ);
private:
	//Holds the torches loaded from XML
	std::vector<Ocelot::Torch*> torchVector;
	//If we want to have 3D for all torches (i.e. play 3D sound for the closest torch)
	//We need to store their positions
	std::vector<Vector3> torchPositions;

	OcelotCamera*              mCamera;
	IOcelotCameraController*   mController;
	CubeMap*				   mCubeMap;
	TriPlanarBumpMaterial*     mTerrainMaterial;
	OcelotLight*			   mLight; // Global light.
	OcelotFog*				   mFog;

	FFLOAT previousHeight;
	Vector2 m_vOldMousePos;

	std::vector<IOcelotMesh*> static_meshes;
	std::vector<Terrain*> terrains;
	std::vector<IParticleSystem*> particleSystems;
	std::vector<BBOOL> showParticles;

	//Since the particle effect systems aren't just following the camera, we
	//unfortunately have to store their positions
	std::vector<Vector3> particleEffectPositions;

	//ADXBaseMesh* m_selectedMesh;

	//Pointer to terrain mesh. The mesh itself is still held
	//in the static_meshes array but this pointer lets us access
	//the terrain in the array (since the array holds generic meshes, we
	//don't know which one is the terrain) so we can use the terrains functions
	//like getting height at (x,z) coordinates at any point in the application

	OcelotXMLDoc rpgGameData;

	BBOOL flyCam;
	BBOOL m_bVisible;
	//Used for debugging torch positions, see the handleInput in main.cpp for more info
	std::ofstream tFile;

	Humanoid* m_player;
};

#endif

