#ifndef DEF_HOUSE_H
#define DEF_HOUSE_H

#include "Geometry\Cube.h"
#include "BaseGameEntity.h"

class House : public BaseGameEntity
{
private:

	// Attributes

public:

	// Constructor & Destructor
	House();
	~House();

	// Methods


	// Accessors

};

#endif