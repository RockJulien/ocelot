#ifndef DEF_HUMANNOID_H
#define DEF_HUMANNOID_H

#include "Geometry\Cylinder.h"
#include "DataStructures\MyLinkedList.h"
#include "DataStructures\SceneGraph.h"
#include "Geometry\Sphere.h"
#include "MovingEntity.h"
#include "FileSplitter.h"
#include <vector>

const FFLOAT m_gJointRadius  = 2.5f;
const FFLOAT m_gMemberHeight = 5.0f;
const FFLOAT timeToWait = 0.03f;

struct HUMAN_ATTRIBUTES
{
	Ocelot::SphereInfo   headInfo;				  	                 //////
	Ocelot::CylinderInfo leftShoulderInfo;                           //////
	Ocelot::CylinderInfo rightShoulderInfo;                          //////
	Ocelot::CylinderInfo leftArmInfo;                             /////  /////
	Ocelot::CylinderInfo leftForwardArmInfo;                      //   //   //
	Ocelot::CylinderInfo rightArmInfo;                            //   //   //
	Ocelot::CylinderInfo rightForwardArmInfo;                     //   //   //
	Ocelot::CylinderInfo trunkTopInfo;								 //
	Ocelot::CylinderInfo trunkBottomInfo;                              //
	Ocelot::CylinderInfo hipInfo;                                  //////////
	Ocelot::CylinderInfo topLeftLegInfo;                           //      //
	Ocelot::CylinderInfo bottomLeftLegInfo;                        //      //
	Ocelot::CylinderInfo topRightLegInfo;                          //      //
	Ocelot::CylinderInfo bottomRightLegInfo;                       //      //
	Ocelot::SphereInfo   leftShoulderJointInfo;
	Ocelot::SphereInfo   rightShoulderJointInfo;
	Ocelot::SphereInfo   leftArmJointInfo;
	Ocelot::SphereInfo   rightArmJointInfo;
	Ocelot::SphereInfo   middleTrunkJointInfo;
	Ocelot::SphereInfo   bottomTrunkJointInfo;
	Ocelot::SphereInfo   leftHipsJointInfo;
	Ocelot::SphereInfo   rightHipsJointInfo;
	Ocelot::SphereInfo   leftLegJointInfo;
	Ocelot::SphereInfo   rightLegJointInfo;
	BaseEntityInfo       baseInfo;
};

class Humanoid : public MovingEntity
{
private:

		// Attributes
		MyLinkedList<Ocelot::IOcelotMesh*> m_members;
		HUMAN_ATTRIBUTES                   m_humanInfo;

		// useful for animation
		BBOOL   m_bWalk;
		BBOOL   m_bInMovement;
		BBOOL   m_bEndSequence;
		UINT   m_iSequenceCounter;
		FFLOAT  m_fTimeCounter;
		FFLOAT  m_fSlowdowner;
		Vector3 m_RotationTrack;

		std::vector<wstring> m_headXangles;
		std::vector<wstring> m_headYangles;
		std::vector<wstring> m_headZangles;
		std::vector<wstring> m_leftShoulderXangles;
		std::vector<wstring> m_leftShoulderYangles;
		std::vector<wstring> m_leftShoulderZangles;
		std::vector<wstring> m_leftElbowXangles;
		std::vector<wstring> m_leftElbowYangles;
		std::vector<wstring> m_leftElbowZangles;
		std::vector<wstring> m_rightShoulderXangles;
		std::vector<wstring> m_rightShoulderYangles;
		std::vector<wstring> m_rightShoulderZangles;
		std::vector<wstring> m_rightElbowXangles;
		std::vector<wstring> m_rightElbowYangles;
		std::vector<wstring> m_rightElbowZangles;
		std::vector<wstring> m_leftHipXangles;
		std::vector<wstring> m_leftHipYangles;
		std::vector<wstring> m_leftHipZangles;
		std::vector<wstring> m_leftKneeXangles;
		std::vector<wstring> m_leftKneeYangles;
		std::vector<wstring> m_leftKneeZangles;
		std::vector<wstring> m_rightHipXangles;
		std::vector<wstring> m_rightHipYangles;
		std::vector<wstring> m_rightHipZangles;
		std::vector<wstring> m_rightKneeXangles;
		std::vector<wstring> m_rightKneeYangles;
		std::vector<wstring> m_rightKneeZangles;

public:

		// Constructor & Destructor
		Humanoid();
		Humanoid(HUMAN_ATTRIBUTES info);
		~Humanoid();

		// Methods
		VVOID createEntity();
		VVOID update(FFLOAT deltaTime);
		VVOID render();
		VVOID walk();
		VVOID applyChangeToLeftShoulderJoint(Vector3 angles);
		VVOID applyChangeToRightShoulderJoint(Vector3 angles);
		VVOID applyChangeToLeftElbowJoint(Vector3 angles);
		VVOID applyChangeToRightElbowJoint(Vector3 angles);
		VVOID applyChangeToLeftHipJoint(Vector3 angles);
		VVOID applyChangeToRightHipJoint(Vector3 angles);
		VVOID applyChangeToLeftKneeJoint(Vector3 angles);
		VVOID applyChangeToRightKneeJoint(Vector3 angles);
		VVOID rotate(const Vector3& rotat);
		VVOID rotateCam(Vector3 rotCam);
		VVOID rescale(Vector3 scale);
		VVOID resetPosition(const Vector3& pos);

		// Accessors
inline  Vector3 getPos() const { return m_baseInfo.Pos;}
inline  VVOID    setPos(Vector3 newPos) { m_baseInfo.Pos = newPos;}
inline  VVOID    setWalk(BBOOL walk) { m_bWalk = walk;}
inline  VVOID    setInMove(BBOOL inMov) { m_bInMovement = inMov;}
inline  BBOOL    isMoving() const { return m_bInMovement;}
};

#endif